var API_PATH = 'http://' + window.location.hostname + ':4001',
    API_EXPORTS_PATH = API_PATH + '/exports',
    APIBasePath = API_PATH + '/', BASE_PATH = {
    'OMD': 'http://localhost:4004/OMD/home',
    'OCD': 'http://localhost:4004/OCD/home',
    'DMMP': 'http://localhost:4004/DMMP/home',
    'CFS': 'http://localhost:4004/CFS/home',
    'AAD': 'http://localhost:4004/AAD/home',
    'NRP': 'http://localhost:4004/NRP',
    'NOIMG': 'http://localhost:4004/assets/no-image-preview.jpg'
}, DEFAULT_DIRECTORY = (window.location.pathname.split('/'))[1], DIRECTORY_404 = "home";
