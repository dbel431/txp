require('events').EventEmitter.prototype._maxListeners = 0;
require('../config.js');
// ---------------------------------------------------------
// ---------------------------------------------------------
// MONGOOSE
// ---------------------------------------------------------
// ---------------------------------------------------------
var mongoose = require('mongoose');
var DataTable = require('mongoose-datatable');
var mongooseDataTablesConditionHandlers = require('./modules/core/mongoose-datatables/condition-handlers.js');
DataTable.configure({
    // verbose: true,
    // debug: true,
});
DataTable.HANDLERS.String = mongooseDataTablesConditionHandlers.myStringHandler;
DataTable.HANDLERS.Boolean = mongooseDataTablesConditionHandlers.myBooleanHandler;
mongoose.plugin(DataTable.init);
var connectionUrl = process.env.APP_DB_USER ? "mongodb://" + encodeURIComponent(process.env.APP_DB_USER) + ':' + encodeURIComponent(process.env.APP_DB_PASS) + '@' + encodeURIComponent(process.env.APP_DB_HOST) + ":" + encodeURIComponent(process.env.APP_DB_PORT) + "/" + encodeURIComponent(process.env.APP_DB) : "mongodb://" + encodeURIComponent(process.env.APP_DB_HOST) + ":" + encodeURIComponent(process.env.APP_DB_PORT) + "/" + encodeURIComponent(process.env.APP_DB);
mongoose.connect(connectionUrl);
// ---------------------------------------------------------
// ---------------------------------------------------------
// EXPRESS
// ---------------------------------------------------------
// ---------------------------------------------------------
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
// ---------------------------------------------------------
// ---------------------------------------------------------
// Logging
// ---------------------------------------------------------
// ---------------------------------------------------------
var winston = require('winston');
var logger = new winston.Logger({
    transports: [
        new winston.transports.File({
            level: process.env.LOG_LEVEL || 'warning',
            filename: './log/app.log',
            handleExceptions: true,
            json: true,
            maxsize: 5242880,
            maxFiles: 5,
            colorize: false
        })
    ],
    exitOnError: false
});
logger.stream = {
    write: function(message, encoding) {
        logger.info(message);
    }
};
app.use(require("morgan")("combined", { "stream": logger.stream }));
// ---------------------------------------------------------
// ---------------------------------------------------------
// CORS
// ---------------------------------------------------------
// ---------------------------------------------------------
app.use(require('cors')());
// ---------------------------------------------------------
// ---------------------------------------------------------
// Body Parser
// ---------------------------------------------------------
// ---------------------------------------------------------
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({
    limit: '50mb',
    extended: true
}));
// ---------------------------------------------------------
// ---------------------------------------------------------
// Treasured Works APP
// ---------------------------------------------------------
// ---------------------------------------------------------
app.use('/exports', express.static(__dirname + '/exports'));
// ---------------------------------------------------------
// ---------------------------------------------------------
// load app/log
var log = require('./modules/log/log.router.js');
app.use('/log', log);
// load app/acl
var acl = require('./modules/acl/acl.router.js');
app.use('/acl', acl);
// load app/mailbox
var mailbox = require('./modules/mailbox/mailbox.router.js');
app.use('/mailbox', mailbox);
// load app/user
var user = require('./modules/user/user.router.js');
app.use('/user', user);
// load app/role
var role = require('./modules/role/role.router.js');
app.use('/role', role);
// load app/notification
// var notification = require('./modules/notification/notification.router.js');
// app.use('/notification', notification);
// load app/todo
var todo = require('./modules/todo/todo.router.js');
app.use('/todo', todo);
// load app/security-question
var securityQuestion = require('./modules/security-question/security-question.router.js');
app.use('/security-question', securityQuestion);
// load app/ref-code-type
var refCode = require('./modules/ref-code/refCode.router.js');
app.use('/ref-code', refCode);
// load app/organization
var organization = require('./modules/organization/organization.router.js');
app.use('/organization', organization);
// load app/batch-job-type
var batchJobType = require('./modules/batch-job/batch-job-type.router.js');
app.use('/batch-job-type', batchJobType);
// load app/batch-job
var batchJob = require('./modules/batch-job/batch-job.router.js');
app.use('/batch-job', batchJob);
// load app/web-user
var webUser = require('./modules/web-user/web-user.router.js');
app.use('/web-user', webUser);
// load app/web-request
var webRequest = require('./modules/web-request/web-request.router.js');
app.use('/web-request', webRequest);
// load app/emailTemplate
var mailTemplate = require('./modules/mail-template/mail-template.router.js');
app.use('/mail-template', mailTemplate);
// load app/subscription-package
var subscriptionPackage = require('./modules/subscription-package/subscription-package.router.js');
app.use('/subscription-package', subscriptionPackage);
app.use('/package', subscriptionPackage);
// load app/package
// var package = require('./modules/package/package.router.js');
// app.use('/package', package);
// var mailTemplate = require('./modules/email-template/email-template.router.js');
// app.use('/email-template', emailTemplate);

var menu = require('./web_modules/menu/menu.router.js');
app.use('/menu', menu);

var webData = require('./web_modules/web-data/web-data.router.js');
app.use('/web-data', webData);


// var webpackage = require('./web_modules/package/package.router.js');


var productsServices = require('./web_modules/products-services/products-services.router.js');
app.use('/products-services', productsServices);


var customerFeedback = require('./web_modules/customer-feedback/customer-feedback.router.js');
app.use('/customer-feedback', customerFeedback);

// ---------------------------------------------------------
// listen
app.listen(process.env.APP_PORT, function() {
    console.log('Listening...');
    if (process.env.RESTART_CRON_JOBS) {
        var batchJobService = require('./modules/batch-job/batch-job.service.js');
        batchJobService.restartIncompleteJobs();
    }
});
