module.exports = function(grunt) {

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        nodemon: {
            dev: {
                script: 'server.js',
                options: {
                    nodeArgs: ['--max-old-space-size=3096']
                }
            }
        }
    });

    grunt.registerTask('default', ['nodemon']);

}
