var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var mst_email_templatesSchema = new Schema({

    // "_id": ObjectId,
    "subject": String,
    "title": String, //Greetings
    "bodyText": String,
    "signature": String,
    "belongsTo": {
        "identifier": String, // 1.    Comments and Feedback Submission 
        "directories": [{
            "type": Schema.Types.ObjectId,
            "ref": 'MST_RefCodeValue'
        }],
    },
    "active": Boolean,
    "deleted": Boolean,
     "created": {
        "by": {
            type: Schema.Types.ObjectId,
            ref: 'TXN_User',
        },
        "at": {
            type: Date,
            default: Date.now
        }
    },
    "updated": {
        "by": {
            type: Schema.Types.ObjectId,
            ref: 'TXN_User'
        },
        "at": {
            type: Date,
            default: Date.now
        }
    }
});



module.exports = mongoose.model('mst_email_templates', mst_email_templatesSchema);
