
var express = require('express');
var emailTemplate = require('./email-template.controller.js');

var temp = express.Router();
temp.get('/',emailTemplate.get);
temp.get('/txn',emailTemplate.txn);
temp.post('/',emailTemplate.add);
temp.post('/addtransaction',emailTemplate.addtransaction);
temp.put('/',emailTemplate.update);
temp.put('/puttransaction',emailTemplate.puttransaction);

module.exports = temp;
