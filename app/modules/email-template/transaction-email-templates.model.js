var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var txn_email_transactionSchema = new Schema({
    // "_id": ObjectId,
    "templateId": {
        "type": Schema.Types.ObjectId,
        "ref": 'mst_emailTemplates'
    }, //Ref
    "to": [{
        "userId": { //User ids
            "type": Schema.Types.ObjectId,
            "ref": 'txn_webuser'
        },
        "userName": String,
        "userEmail": String
    }],
    "cc": [{
        "userId": { //User ids
            "type": Schema.Types.ObjectId,
            "ref": 'txn_webuser'
        },
        "userName": String,
        "userEmail": String
    }],
    "bcc": [{
        "userId": { //User ids
            "type": Schema.Types.ObjectId,
            "ref": 'txn_webuser'
        },
        "userName": String,
        "userEmail": String
    }],
    "fromEmail": String, //Email id
    "FromName": String, //
    "flags": {
        "sent": Boolean,
        "isAttachment": Boolean,
    },
    "note": String,
    "retryAttempt": Number,
    "currentAttempt": Number,
    "createdDate": Date,
    "sendDate": Date,
    "active": Boolean,
    "deleted": Boolean,
    "created": {
        "by": {
            type: Schema.Types.ObjectId,
            ref: 'TXN_User',
        },
        "at": {
            type: Date,
            default: Date.now
        }
    },
    "updated": {
        "by": {
            type: Schema.Types.ObjectId,
            ref: 'TXN_User'
        },
        "at": {
            type: Date,
            default: Date.now
        }
    }

});
module.exports = mongoose.model('txn_email_transaction', txn_email_transactionSchema);