var MstEmail = require('./master-templates.model.js');
var TxnEmail = require('./transaction-email-templates.model.js');
module.exports = {
    get: function(query, callback) {
        //var by = { by: undefined };
        //if (query.created) by.by = query.created.by || undefined;
        //MstEmail.find(query,callback);
        console.log('animals_animals',query);
        MstEmail.find(query).exec(function(err, animals) {
          return  callback(null, animals);
        });
        
        
    },
    txn: function(query, callback) {
        var by = { by: undefined };
        if (query.created) by.by = query.created.by || undefined;
        TxnEmail.create(query, function(err, TxnEmail) {
            //return callback(err);
        });

    },
    add: function(query, callback) {
        console.log(query);
        var by = { by: undefined };
        if (query.created) by.by = query.created.by || undefined;
        MstEmail.create(query, function(err, MstEmail) {
            return callback(null, MstEmail);
        });

    },
    addtransaction: function(query, callback) {
        // 	    var by = { by: undefined };
        //        if (query.created) by.by = query.created.by || undefined;
        //             TxnEmail.create(query, function(err, TxnEmail)
        //             {
        //             		//return callback(err);
        //             });

        // },
        var by = { by: undefined };
        if (query.created) by.by = query.created.by || undefined;
        TxnEmail.create(query, function(err, TxnEmail) {
            return callback(null, TxnEmail);
        });

    },
    update: function(query, callback) {
    	console.log(query);
       var by = { by: undefined };
        if (query.updated) by.by = query.updated.by || undefined;
        MstEmail.findByIdAndUpdate(query.id, query, function(err, MstEmail) {
            return callback(null, MstEmail);
        });

    },
    puttransaction: function(query, callback) {
    	console.log(query);
       var by = { by: undefined };
        if (query.updated) by.by = query.updated.by || undefined;
        TxnEmail.findByIdAndUpdate(query.id, query, function(err, TxnEmail) {
            return callback(null, TxnEmail);
        });

    },
};
