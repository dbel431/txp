var coreQueryBuilderService = require('../core/core.query-builder.service.js');
var emailTemplate = require('./email-template.service.js');

module.exports = {
    get:function(req,res){

        var query = coreQueryBuilderService.buildQuery(req);
   
        var EmailTemplate = emailTemplate.get(query,function(err, emailTemplateData) {
                if (err) return res.status(400).send(err);
                return  res.send('emailTemplateData');
              //   return '456';
  
            });
        //res.send(EmailTemplate);

    },
    txn:function(req,res){
        var query={'note':'pan'};
        var EmailTemplate = emailTemplate.txn(query,function(err, emailTemplateData) {
                if (err) return res.status(400).send(err);
                return res.send(emailTemplateData);
            });
        res.send(EmailTemplate);
    },
    add:function(req,res){
        var query = coreQueryBuilderService.buildQuery(req);
        var EmailTemplate = emailTemplate.add(query,function(err, emailTemplateData) {
                if (err) return res.status(400).send(err);
                return res.send(emailTemplateData);

            });
        
    },
    addtransaction:function(req,res){
    //     var query={'note':'addtransaction data'};
    //     var EmailTemplate = emailTemplate.addtransaction(query,function(err, emailTemplateData) {
    //             if (err) return res.status(400).send(err);
    //             return res.send(emailTemplateData);
    //         });
    //     res.send(EmailTemplate);
    // },
      var query = coreQueryBuilderService.buildQuery(req);
      console.log(query);
        var EmailTemplate = emailTemplate.addtransaction(query,function(err, emailTemplateData) {
                if (err) return res.status(400).send(err);
                return res.send(emailTemplateData);

            });
        
    },
    update:function(req,res){
        //var query = coreQueryBuilderService.buildQuery(req);
      
        var EmailTemplate = emailTemplate.update(req.body,function(err, emailTemplateData) {
                if (err) return res.status(400).send(err);
                return res.send(emailTemplateData);

            });
        
    },
    puttransaction:function(req,res){
        //var query = coreQueryBuilderService.buildQuery(req);
      
        var EmailTemplate = emailTemplate.puttransaction(req.body,function(err, emailTemplateData) {
                if (err) return res.status(400).send(err);
                return res.send(emailTemplateData);

            });
        
    },
};
