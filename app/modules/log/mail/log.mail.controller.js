var coreQueryBuilderService = require('../../core/core.query-builder.service.js');
var mailerService = require('../../core/mailer.service.js');

module.exports = {
    get: function(req, res) {
        var query = coreQueryBuilderService.buildQuery(req);
        mailerService.getLog(query, function(err, mailLogs) {
            if (err) return res.status(400).send(err);
            return res.send(mailLogs);
        });
    }
};
