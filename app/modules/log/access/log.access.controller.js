var coreQueryBuilderService = require('../../core/core.query-builder.service.js');
var accessLogService = require('../../core/access.log.service.js');

module.exports = {
    get: function(req, res) {
        var query = coreQueryBuilderService.buildQuery(req);
        accessLogService.get(query, function(err, accessLogs) {
            if (err) return res.status(400).send(err);
            return res.send(accessLogs);
        });
    }
};
