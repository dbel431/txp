var express = require('express');
var log = express.Router();

var accessLogController = require('./access/log.access.controller.js');
var mailLogController = require('./mail/log.mail.controller.js');

log.get('/access', accessLogController.get);
log.get('/mail', mailLogController.get);

module.exports = log;
