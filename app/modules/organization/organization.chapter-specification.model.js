var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TXN_ChapterSpecificationSchema = new Schema({
    "directoryId": {
        type: Schema.Types.ObjectId,
        ref: 'MST_RefCodeValue'
    },
    "organizationId": {
        type: Schema.Types.ObjectId,
        ref: 'TXN_Organization'
    },
    "specificationType": String,
    "listingType": {
        type: Schema.Types.ObjectId,
        ref: 'MST_RefCodeValue'
    },
    "code": {
        type: Schema.Types.ObjectId,
        ref: 'MST_RefCodeValue'
    },
    "name": String,
    "sequence_number": Number,
    "activeFlag": Boolean,
    active: {
        type: Boolean,
        default: true
    },
    deleted: {
        type: Boolean,
        default: false
    },
    "created": {
        "by": {
            "type": Schema.Types.ObjectId,
            "ref": 'TXN_User'
        },
        "at": Date
    },
    "updated": {
        "by": {
            "type": Schema.Types.ObjectId,
            "ref": 'TXN_User'
        },
        "at": Date
    }
});

TXN_ChapterSpecificationSchema.index({ sequence_number: 1, _id: 1 });
TXN_ChapterSpecificationSchema.index({ sequence_number: -1, _id: 1 });
TXN_ChapterSpecificationSchema.index({ sequence_number: -1, _id: -1 });

module.exports = mongoose.model('TXN_ChapterSpecification', TXN_ChapterSpecificationSchema);
