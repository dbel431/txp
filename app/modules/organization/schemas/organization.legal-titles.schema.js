var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = new Schema({
    "sequenceNo": Number,
    "legalTitle": String,
    "city": { //
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "cityName": String,
    "state": { //
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "stateName": String,
    "deleted": {
        type: Boolean,
        default: false
    },
}).post('init', function(doc) {
    if (!doc._id) doc._id = mongoose.Types.ObjectId();
    return doc;
});
