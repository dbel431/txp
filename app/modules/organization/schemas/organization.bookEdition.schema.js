var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = new Schema({
    "edition": Number,
    "mustField": Boolean,
    "nomSource": String,
    "editionRespond": String,
    "status": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "statusValue": String,
    "killComment": String,
    "editorialComment": String,
    "researchComment": String,
    "permanentComment": String,
    "deleted": {
        type: Boolean,
        default: false
    },
}).post('init', function(doc) {
    if (!doc._id) doc._id = mongoose.Types.ObjectId();
    return doc;
});
