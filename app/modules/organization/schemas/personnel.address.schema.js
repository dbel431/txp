var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = new Schema({
    "addressType": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "street1": String,
    "street2": String,
    "city": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "cityName": String,
    "state": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "stateName": String,
    "zip": String,
    "country": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "countryName": String,
    "county": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "countyName": String,
    "deleted": {
        "type": Boolean,
        "default": false
    },
}).post('init', function(doc) {
    if (!doc._id) doc._id = mongoose.Types.ObjectId();
    return doc;
});
