var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = new Schema({
    "contactType": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "contactTypeName": String,
    "sequenceNo": Number,
    "addressSequenceNo": Number,
    "contactDetails": String,
    "extension": String,
    "printFlag": Boolean,
    "mailOptOut": Boolean,
    "nixieIndex": Boolean,
    "nonUSA": Boolean,
    "note": String,
    "deleted": {
        "type": Boolean,
        "default": false
    },
}).post('init', function(doc) {
    if (!doc._id) doc._id = mongoose.Types.ObjectId();
    return doc;
});
