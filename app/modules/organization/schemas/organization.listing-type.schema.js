var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = new Schema({
    "listing": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "listingName": String,
    "listingActive": Boolean,
    "sectionNumber": String,
    "listingSectionCode": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "listingSectionName": String,
    "deleted": {
        "type": Boolean,
        "default": false
    }
}).post('init', function(doc) {
    if (!doc._id) doc._id = mongoose.Types.ObjectId();
    return doc;
});
