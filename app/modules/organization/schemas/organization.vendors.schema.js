var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = new Schema({
    "vendorType": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "sequenceNumber": Number,
    "name": String,
    "postBox": String,
    "street": String,
    "city": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "cityName": String,
    "stateName": String,
    "state": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "zip": String,
    "deleted": {
        "type": Boolean,
        "default": false
    }
}).post('init', function(doc) {
    if (!doc._id) doc._id = mongoose.Types.ObjectId();
    return doc;
});
