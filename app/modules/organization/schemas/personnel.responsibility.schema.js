var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = new Schema({ //found in CFS only
    "responsibilityCode": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "responsibilityDescription": String,
    "sdaaIndex": Boolean,
    "dcaPubIndex": Boolean,
    "sdaIndex": Boolean,
    "dcaIpcIndex": Boolean,
    "acfIndex": Boolean,
    "dcaPreviousIndex": Boolean,
    "iadIndex": Boolean,
    "iagIndex": Boolean,
    active: {
        type: Boolean,
        default: true
    },
    "deleted": {
        "type": Boolean,
        "default": false
    },
}).post('init', function(doc) {
    if (!doc._id) doc._id = mongoose.Types.ObjectId();
    return doc;
});
