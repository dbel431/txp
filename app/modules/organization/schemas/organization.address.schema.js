var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = new Schema({
    "addressType": { //
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "mailingIndex": Boolean, //
    "sequenceNo": Number,
    "street1": String, //
    "postBox": String,
    "street2": String, //
    "country": { //
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "countryName": String, //
    "state": { //
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "stateName": String, //
    "city": { //
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "cityName": String, //
    "zip": String, // 
    "location": {
        "type": [Number],
        "index": '2d'
    },
    // For OCD County
    "county": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "countyName": String,
    "contactPerson": String,
    "organizationName": String,
    "mailOptOut": Boolean,
    "nonUSA": Boolean,
    "header": String,
    "printFlag": Boolean,
    "sortSequenceNo": Number,
    // New added fields 11 June 2016
    "comments": String,
    "nixieIndex": Boolean,
    "nixieDate": String,
    "nixieComments": String,
    "deleted": {
        type: Boolean,
        default: false
    },
    "considerThisAddress": Boolean
}).post('init', function(doc) {
    if (!doc._id) doc._id = mongoose.Types.ObjectId();
    return doc;
});
