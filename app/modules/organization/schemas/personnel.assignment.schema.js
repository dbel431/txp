var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = new Schema({
    "sequenceNo": Number,
    "orgId": {
        "type": Schema.Types.ObjectId,
        "ref": 'TXN_Organization'
    },
    "orgName": String,
    "assignTypeId": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "assignType": String,
    "title": String,
    "response": Boolean,
    "deleted": {
        "type": Boolean,
        "default": false
    },
}).post('init', function(doc) {
    if (!doc._id) doc._id = mongoose.Types.ObjectId();
    return doc;
});
