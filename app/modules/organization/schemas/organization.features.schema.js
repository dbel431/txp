var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = new Schema({
    "sequenceNo": Number,
    "featureType": String,
    "code": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "codeName": String,
    "name": String,
    "classif_code": String,
    // New added fields 11 June 2016
    "majorIndex": String,
    "indexIndication": String,
    "deleted": {
        type: Boolean,
        default: false
    },
}).post('init', function(doc) {
    if (!doc._id) doc._id = mongoose.Types.ObjectId();
    return doc;
});
