var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = new Schema({
    "sequenceNo": Number,
    "note": String,
    "noteType": String,
    "deleted": {
        "type": Boolean,
        "default": false
    },
    //for OCD
    "italic": Boolean,
    "bold": Boolean,
    "smallCap": Boolean,
}).post('init', function(doc) {
    if (!doc._id) doc._id = mongoose.Types.ObjectId();
    return doc;
});
