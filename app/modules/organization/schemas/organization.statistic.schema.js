var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = new Schema({
    "statisticCategory": String, //ORG Statistics/DIO Statistics/Archive Statistics
    "sequenceNo": Number,
    "statisticType": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "statisticTypeName": String,
    "value": String,
    "text": String,
    "year": String,
    "deleted": {
        type: Boolean,
        default: false
    },
}).post('init', function(doc) {
    if (!doc._id) doc._id = mongoose.Types.ObjectId();
    return doc;
});
