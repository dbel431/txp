var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = new Schema({ //found in CFS only
    "officerTypeId": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "OfficerTypeName": String,
    "listingType": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "listingTypeName": String,
    active: {
        type: Boolean,
        default: true
    },
    "deleted": {
        "type": Boolean,
        "default": false
    },
    "text": String,
}).post('init', function(doc) {
    if (!doc._id) doc._id = mongoose.Types.ObjectId();
    return doc;
});
