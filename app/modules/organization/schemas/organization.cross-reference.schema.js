var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = new Schema({
    "text": String,
    "sortName": String,
    "crossRefId": String,
}).post('init', function(doc) {
    if (!doc._id) doc._id = mongoose.Types.ObjectId();
    return doc;
});
