var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = new Schema({
    "name": String,
    "startDate": Date,
    "endDate": Date,
    "showDate": String,
    "suffix": String,
    "deleted": {
        type: Boolean,
        default: false
    },
}).post('init', function(doc) {
    if (!doc._id) doc._id = mongoose.Types.ObjectId();
    return doc;
});
