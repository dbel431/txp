var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = new Schema({
    "sequenceNo": Number,
    "noteType": String,
    "note": String,
    "italic": Boolean,
    "bold": Boolean,
    "smallCap": Boolean,
    "deleted": {
        "type": Boolean,
        "default": false
    },
}).post('init', function(doc) {
    if (!doc._id) doc._id = mongoose.Types.ObjectId();
    return doc;
});
