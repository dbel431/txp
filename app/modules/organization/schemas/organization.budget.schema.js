var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = new Schema({
    "code": { //
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "codeName": String,
    "value": String,

    "deleted": {
        type: Boolean,
        default: false
    },
}).post('init', function(doc) {
    if (!doc._id) doc._id = mongoose.Types.ObjectId();
    return doc;
});
