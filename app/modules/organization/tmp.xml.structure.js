module.exports = [{
    "nodeId": 1,
    "label": "component",
    "name": "component",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 0,
    "parentNodeId": 0,
    "sequenceNo": 1,
    "attributes": { "key": "type", "value": "CFS-Regular" }
}, {
    "nodeId": 2,
    "label": "Sec",
    "name": "sec",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 1,
    "parentNodeId": 1,
    "sequenceNo": 1,
    "repeatFor": "onlyListingTypes",
    "repeatConditions": { "key": "_id", "value": "577e24b8c19c4090a8aee1ec" }
}, {
    "nodeId": 3,
    "label": "ChapterName",
    "name": "chaptername",
    "nodeType": "data",
    "patternId": "VC",
    "level": 2,
    "parentNodeId": 2,
    "sequenceNo": 1,
    "data": { "model": "codeValue" }
}, {
    "nodeId": 4,
    "label": "Org",
    "name": "org",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 2,
    "parentNodeId": 2,
    "sequenceNo": 2,
    "repeatFor": "organizations",
    "repeatConditions": { "key": "workflowStatus", "value": "57283b4214dde6a43b46a7bb" }
}, {
    "nodeId": 5,
    "label": "OrgInfo",
    "name": "orginfo",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 3,
    "parentNodeId": 4,
    "sequenceNo": 1
}, {
    "nodeId": 6,
    "label": "OrganizationID",
    "name": "orgid",
    "nodeType": "data",
    "patternId": "VC",
    "level": 4,
    "parentNodeId": 5,
    "sequenceNo": 1,
    "data": { "model": "org_id" }
}, {
    "nodeId": 7,
    "label": "OrganizationName",
    "name": "orgname",
    "nodeType": "data",
    "patternId": "VC",
    "level": 4,
    "parentNodeId": 5,
    "sequenceNo": 2,
    "data": { "model": "name" }
}, {
    "nodeId": 8,
    "label": "AddressInfo",
    "name": "addressinfo",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 3,
    "parentNodeId": 4,
    "sequenceNo": 2
}, {
    "nodeId": 9,
    "label": "Address",
    "name": "address",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 4,
    "parentNodeId": 8,
    "sequenceNo": 1,
    "repeatFor": "address"
}, {
    "nodeId": 10,
    "label": "AddressStreet1",
    "name": "addressstreet1",
    "nodeType": "data",
    "patternId": "VC",
    "level": 5,
    "parentNodeId": 9,
    "sequenceNo": 1,
    "data": { "model": "street1" }
}, {
    "nodeId": 11,
    "label": "AddressStreet2",
    "name": "addressrstreet2",
    "nodeType": "data",
    "patternId": "VC",
    "level": 5,
    "parentNodeId": 9,
    "sequenceNo": 2,
    "data": { "model": "street2" }
}, {
    "nodeId": 12,
    "label": "AddressCity",
    "name": "addressrcityname",
    "nodeType": "data",
    "patternId": "VC",
    "level": 5,
    "parentNodeId": 9,
    "sequenceNo": 3,
    "data": { "model": "cityName" }
}, {
    "nodeId": 13,
    "label": "AddressStateTerritory",
    "name": "addressstatename",
    "nodeType": "data",
    "patternId": "VC",
    "level": 5,
    "parentNodeId": 9,
    "sequenceNo": 4,
    "data": { "model": "stateName" }
}, {
    "nodeId": 14,
    "label": "AddressStateTerritoryAbbr",
    "name": "addressstateterritoryabbr",
    "nodeType": "data",
    "patternId": "VC",
    "level": 5,
    "parentNodeId": 9,
    "sequenceNo": 5,
    "data": { "model": "state", "valuePath": "description" }
}, {
    "nodeId": 15,
    "label": "AddressZip",
    "name": "addresszip",
    "nodeType": "data",
    "patternId": "VC",
    "level": 5,
    "parentNodeId": 9,
    "sequenceNo": 6,
    "data": { "model": "zip" }
}, {
    "nodeId": 16,
    "label": "ContactInfo",
    "name": "contactinfo",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 4,
    "parentNodeId": 4,
    "sequenceNo": 3
}, {
    "nodeId": 17,
    "label": "ContactPhoneNo",
    "name": "contactphone",
    "nodeType": "data",
    "patternId": "VC",
    "level": 5,
    "parentNodeId": 16,
    "sequenceNo": 1,
    "data": { "model": "contact.phoneNumbers", "isNested": true }
}, {
    "nodeId": 18,
    "label": "ContactFax",
    "name": "contactfax",
    "nodeType": "data",
    "patternId": "VC",
    "level": 5,
    "parentNodeId": 16,
    "sequenceNo": 2,
    "data": { "model": "contact.faxNumbers", "isNested": true }
}, {
    "nodeId": 19,
    "label": "ContactPrimaryEmail",
    "name": "contactemailpri",
    "nodeType": "data",
    "patternId": "VC",
    "level": 5,
    "parentNodeId": 16,
    "sequenceNo": 3,
    "data": { "model": "contact.emails.primary", "isNested": true }
}, {
    "nodeId": 20,
    "label": "ContactWebsites",
    "name": "contactemailsec",
    "nodeType": "data",
    "patternId": "VC",
    "level": 5,
    "parentNodeId": 16,
    "sequenceNo": 4,
    "data": { "model": "contact.websites", "isNested": true }
}, {
    "nodeId": 21,
    "label": "KeyPersonnelInfo",
    "name": "keypersinfo",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 4,
    "parentNodeId": 4,
    "sequenceNo": 4
}, {
    "nodeId": 22,
    "label": "KeyPersonnel",
    "name": "keypers",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 5,
    "parentNodeId": 21,
    "sequenceNo": 1,
    "repeatFor": "personnel"
}, {
    "nodeId": 23,
    "label": "KeyPersonnelOfficersGroup",
    "name": "keypersofficersgrp",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 22,
    "sequenceNo": 1
}, {
    "nodeId": 24,
    "label": "KeyPersonnelOfficers",
    "name": "keypersofficers",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 23,
    "sequenceNo": 1,
    "repeatFor": "officers"
}, {
    "nodeId": 25,
    "label": "KeyPersonnelOfficersTitle",
    "name": "keypersofficerstypetitle",
    "nodeType": "data",
    "patternId": "VC",
    "level": 8,
    "parentNodeId": 24,
    "sequenceNo": 1,
    "data": { "model": "OfficerTypeName" }
}, {
    "nodeId": 26,
    "label": "KeyPersonnelPrefix",
    "name": "keyperspref",
    "nodeType": "data",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 22,
    "sequenceNo": 2,
    "data": { "model": "name.prefix", "isNested": true }
}, {
    "nodeId": 27,
    "label": "KeyPersonnelFirstName",
    "name": "keypersnamefir",
    "nodeType": "data",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 22,
    "sequenceNo": 3,
    "data": { "model": "name.first", "isNested": true }
}, {
    "nodeId": 28,
    "label": "KeyPersonnelMiddleName",
    "name": "keypersnamemid",
    "nodeType": "data",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 22,
    "sequenceNo": 4,
    "data": { "model": "name.middle", "isNested": true }
}, {
    "nodeId": 29,
    "label": "KeyPersonnelLastName",
    "name": "keypersnamelast",
    "nodeType": "data",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 22,
    "sequenceNo": 5,
    "data": { "model": "name.last", "isNested": true }
}, {
    "nodeId": 30,
    "label": "KeyPersonnelSuffix",
    "name": "keyperssuffix",
    "nodeType": "data",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 22,
    "sequenceNo": 6,
    "data": { "model": "name.suffix", "isNested": true }
}, {
    "nodeId": 31,
    "label": "KeyPersonnelTitle",
    "name": "keyperstitle",
    "nodeType": "data",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 22,
    "sequenceNo": 7,
    "data": { "model": "titleMasterName" }
}, {
    "nodeId": 32,
    "label": "KeyPersonnelPhoneNo",
    "name": "keypersphone",
    "nodeType": "data",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 22,
    "sequenceNo": 8,
    "data": { "model": "phoneNumbers" }
}, {
    "nodeId": 33,
    "label": "ChapterSpecificationInfo",
    "name": "chapterspeciInfo",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 4,
    "parentNodeId": 4,
    "sequenceNo": 5
}, {
    "nodeId": 34,
    "label": "ServicesOfferedInfo",
    "name": "serviceofferedinfo",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 5,
    "parentNodeId": 33,
    "sequenceNo": 1,
    "repeatFor": "CSSERVICE OFFERED"
}, {
    "nodeId": 35,
    "label": "ServicesOfferedTitle",
    "name": "serviceofferedtitle",
    "nodeType": "data",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 34,
    "sequenceNo": 1,
    "data": { "model": "codeType" }
}, {
    "nodeId": 36,
    "label": "ServiceOffered",
    "name": "serviceoffered",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 34,
    "sequenceNo": 2,
    "repeatFor": "records"
}, {
    "nodeId": 37,
    "label": "ChapterSpecificationSpecificationTypeCodeName",
    "name": "chapterspeccodename",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 36,
    "sequenceNo": 1,
    "data": { "model": "codeName" }
}, {
    "nodeId": 38,
    "label": "ChapterSpecificationSpecificationTypeName",
    "name": "chapterspecname",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 36,
    "sequenceNo": 2,
    "data": { "model": "name" }
}, {
    "nodeId": 39,
    "label": "FinancingPreferredInfo",
    "name": "financingpreferredinfo",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 5,
    "parentNodeId": 33,
    "sequenceNo": 2,
    "repeatFor": "CSFINANCING PREFERRED"
}, {
    "nodeId": 40,
    "label": "FinancingPreferredTitle",
    "name": "financingpreferredtitle",
    "nodeType": "data",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 39,
    "sequenceNo": 1,
    "data": { "model": "codeType" }
}, {
    "nodeId": 41,
    "label": "FinancingPreferred",
    "name": "financingpreferred",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 39,
    "sequenceNo": 2,
    "repeatFor": "records"
}, {
    "nodeId": 42,
    "label": "ChapterSpecificationSpecificationTypeCodeName",
    "name": "chapterspeccodename",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 41,
    "sequenceNo": 1,
    "data": { "model": "codeName" }
}, {
    "nodeId": 43,
    "label": "ChapterSpecificationSpecificationTypeName",
    "name": "chapterspecname",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 41,
    "sequenceNo": 2,
    "data": { "model": "name" }
}, {
    "nodeId": 44,
    "label": "MinimumOperatingDataInfo",
    "name": "minimumoperatingdatainfo",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 5,
    "parentNodeId": 33,
    "sequenceNo": 3,
    "repeatFor": "CSMINIMUM OPERATING DATA REQUIRED TO CONSIDER FINANCING"
}, {
    "nodeId": 45,
    "label": "MinimumOperatingDataTitle",
    "name": "minumunoperationdatatitle",
    "nodeType": "data",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 44,
    "sequenceNo": 1,
    "data": { "model": "codeType" }
}, {
    "nodeId": 46,
    "label": "MinimumOperatingData",
    "name": "minimumoperatingdata",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 44,
    "sequenceNo": 2,
    "repeatFor": "records"
}, {
    "nodeId": 47,
    "label": "ChapterSpecificationSpecificationTypeCodeName",
    "name": "chapterspeccodename",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 46,
    "sequenceNo": 1,
    "data": { "model": "codeName" }
}, {
    "nodeId": 48,
    "label": "ChapterSpecificationSpecificationTypeName",
    "name": "chapterspecname",
    "nodeType": "data",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 46,
    "sequenceNo": 2,
    "data": { "model": "name" }
}, {
    "nodeId": 49,
    "label": "IndustaryPreferenceInfo",
    "name": "industarypreferenceinfo",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 5,
    "parentNodeId": 33,
    "sequenceNo": 4,
    "repeatFor": "CSINDUSTRY PREFERENCE"
}, {
    "nodeId": 50,
    "label": "IndustaryPreferenceTitle",
    "name": "industrypreferencetitle",
    "nodeType": "data",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 49,
    "sequenceNo": 1,
    "data": { "model": "codeType" }
}, {
    "nodeId": 51,
    "label": "IndustaryPreference",
    "name": "industarypreference",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 49,
    "sequenceNo": 2,
    "repeatFor": "records"
}, {
    "nodeId": 52,
    "label": "ChapterSpecificationSpecificationTypeCodeName",
    "name": "chapterspeccodename",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 51,
    "sequenceNo": 1,
    "data": { "model": "codeName" }
}, {
    "nodeId": 53,
    "label": "ChapterSpecificationSpecificationTypeName",
    "name": "chapterspecname",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 51,
    "sequenceNo": 2,
    "data": { "model": "name" }
}, {
    "nodeId": 54,
    "label": "FirmPrefersNotToInvestInInfo",
    "name": "firmprefersnottoinvestIninfo",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 5,
    "parentNodeId": 33,
    "sequenceNo": 5,
    "repeatFor": "CSFIRM PREFERS NOT TO INVEST IN"
}, {
    "nodeId": 55,
    "label": "FirmPrefersNotToInvestInTitle",
    "name": "firmprefersnottoinvestin",
    "nodeType": "data",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 54,
    "sequenceNo": 1,
    "data": { "model": "codeType" }
}, {
    "nodeId": 56,
    "label": "FirmPrefersNotToInvestIn",
    "name": "firmprefersnottoinvestIn",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 54,
    "sequenceNo": 2,
    "repeatFor": "records"
}, {
    "nodeId": 57,
    "label": "ChapterSpecificationSpecificationTypeCodeName",
    "name": "chapterspeccodename",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 56,
    "sequenceNo": 1,
    "data": { "model": "codeName" }
}, {
    "nodeId": 58,
    "label": "ChapterSpecificationSpecificationTypeName",
    "name": "chapterspecname",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 56,
    "sequenceNo": 2,
    "data": { "model": "name" }
}, {
    "nodeId": 59,
    "label": "FeeStructureAndMethodCompensationInfo",
    "name": "feestructureandmethodcompensationinfo",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 5,
    "parentNodeId": 33,
    "sequenceNo": 6,
    "repeatFor": "CSFEE STRUCTURE OR METHOD OF COMPENSATION"
}, {
    "nodeId": 60,
    "label": "FeeStructureAndMethodCompensationTitle",
    "name": "feestructureandmethodofcompensation",
    "nodeType": "data",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 59,
    "sequenceNo": 1,
    "data": { "model": "codeType" }
}, {
    "nodeId": 61,
    "label": "FeeStructureAndMethodCompensation",
    "name": "feestructureandmethodcompensation",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 59,
    "sequenceNo": 2,
    "repeatFor": "records"
}, {
    "nodeId": 62,
    "label": "ChapterSpecificationSpecificationTypeCodeName",
    "name": "chapterspeccodename",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 61,
    "sequenceNo": 1,
    "data": { "model": "codeName" }
}, {
    "nodeId": 63,
    "label": "ChapterSpecificationSpecificationTypeName",
    "name": "chapterspecname",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 61,
    "sequenceNo": 2,
    "data": { "model": "name" }
}, {
    "nodeId": 64,
    "label": "ExitCriteriaInfo",
    "name": "exitcriteriainfo",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 5,
    "parentNodeId": 33,
    "sequenceNo": 7,
    "repeatFor": "CSEXIT CRITERIA"
}, {
    "nodeId": 65,
    "label": "ExitCriteriaTitle",
    "name": "exitcriteriatitle",
    "nodeType": "data",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 64,
    "sequenceNo": 1,
    "data": { "model": "codeType" }
}, {
    "nodeId": 66,
    "label": "ExitCriteria",
    "name": "exitcriteria",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 64,
    "sequenceNo": 2,
    "repeatFor": "records"
}, {
    "nodeId": 67,
    "label": "ChapterSpecificationSpecificationTypeCodeName",
    "name": "chapterspeccodename",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 66,
    "sequenceNo": 1,
    "data": { "model": "codeName" }
}, {
    "nodeId": 68,
    "label": "ChapterSpecificationSpecificationTypeName",
    "name": "chapterspecname",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 66,
    "sequenceNo": 2,
    "data": { "model": "name" }
}, {
    "nodeId": 69,
    "label": "GeoGraphicalPreferenceInfo",
    "name": "geographicalpreferenceinfo",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 5,
    "parentNodeId": 33,
    "sequenceNo": 8,
    "repeatFor": "CSGEOGRAPHICAL PREFERENCES"
}, {
    "nodeId": 70,
    "label": "GeoGraphicalTitle",
    "name": "geographicaltitle",
    "nodeType": "data",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 69,
    "sequenceNo": 1,
    "data": { "model": "codeType" }
}, {
    "nodeId": 71,
    "label": "GeoGraphicalPreference",
    "name": "geographicalpreference",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 69,
    "sequenceNo": 2,
    "repeatFor": "records"
}, {
    "nodeId": 72,
    "label": "ChapterSpecificationSpecificationTypeCodeName",
    "name": "chapterspeccodename",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 71,
    "sequenceNo": 1,
    "data": { "model": "codeName" }
}, {
    "nodeId": 73,
    "label": "ChapterSpecificationSpecificationTypeName",
    "name": "chapterspecname",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 71,
    "sequenceNo": 2,
    "data": { "model": "name" }
}, {
    "nodeId": 74,
    "label": "EstablishInfo",
    "name": "establishinfo",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 4,
    "parentNodeId": 4,
    "sequenceNo": 9
}, {
    "nodeId": 75,
    "label": "EstablishedYear",
    "name": "establishyear",
    "nodeType": "data",
    "patternId": "VC",
    "level": 5,
    "parentNodeId": 74,
    "sequenceNo": 1,
    "data": { "model": "established.year", "isNested": true }
}, {
    "nodeId": 76,
    "label": "InvesmentPortfolioSizeInfo",
    "name": "invesmentportfoliosizeinfo",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 5,
    "parentNodeId": 33,
    "sequenceNo": 10,
    "repeatFor": "CSINVESTMENT PORTFOLIO SIZE"
}, {
    "nodeId": 77,
    "label": "InvesmentPortfolioSizeTitle",
    "name": "invesmentportfoliosizetitle",
    "nodeType": "data",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 76,
    "sequenceNo": 1,
    "data": { "model": "codeType" }
}, {
    "nodeId": 78,
    "label": "InvesmentPortfolioSize",
    "name": "invesmentportfoliosize",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 76,
    "sequenceNo": 2,
    "repeatFor": "records"
}, {
    "nodeId": 79,
    "label": "ChapterSpecificationSpecificationTypeCodeName",
    "name": "chapterspeccodename",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 78,
    "sequenceNo": 1,
    "data": { "model": "codeName" }
}, {
    "nodeId": 80,
    "label": "ChapterSpecificationSpecificationTypeName",
    "name": "chapterspecname",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 78,
    "sequenceNo": 2,
    "data": { "model": "name" }
}, {
    "nodeId": 81,
    "label": "MainSourceOfCapitalInfo",
    "name": "mainsourceofcapitalinfo",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 5,
    "parentNodeId": 33,
    "sequenceNo": 11,
    "repeatFor": "CSMAIN SOURCES OF CAPITAL"
}, {
    "nodeId": 82,
    "label": "MainSourceOfCapitalTitle",
    "name": "mainsourceofcapitaltitle",
    "nodeType": "data",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 81,
    "sequenceNo": 1,
    "data": { "model": "codeType" }
}, {
    "nodeId": 83,
    "label": "MainSourceOfCapital",
    "name": "mainsourceofcapital",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 81,
    "sequenceNo": 2,
    "repeatFor": "records"
}, {
    "nodeId": 84,
    "label": "ChapterSpecificationSpecificationTypeCodeName",
    "name": "chapterspeccodename",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 83,
    "sequenceNo": 1,
    "data": { "model": "codeName" }
}, {
    "nodeId": 85,
    "label": "ChapterSpecificationSpecificationTypeName",
    "name": "chapterspecname",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 83,
    "sequenceNo": 2,
    "data": { "model": "name" }
}, {
    "nodeId": 86,
    "label": "FundsAvailableForInvesmentOrLoansInfo",
    "name": "fundsavailableforinvesmentorloansinfo",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 5,
    "parentNodeId": 33,
    "sequenceNo": 12,
    "repeatFor": "CSFUNDS AVAILABLE FOR INVESTMENTS OR LOANS"
}, {
    "nodeId": 87,
    "label": "FundsAvailableForInvesmentOrLoansTitle",
    "name": "fundsavailableforinvesmentorloanstitle",
    "nodeType": "data",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 86,
    "sequenceNo": 1,
    "data": { "model": "codeType" }
}, {
    "nodeId": 88,
    "label": "FundsAvailableForInvesmentOrLoans",
    "name": "fundsavailableforinvesmentorloans",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 86,
    "sequenceNo": 2,
    "repeatFor": "records"
}, {
    "nodeId": 89,
    "label": "ChapterSpecificationSpecificationTypeCodeName",
    "name": "chapterspeccodename",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 88,
    "sequenceNo": 1,
    "data": { "model": "codeName" }
}, {
    "nodeId": 90,
    "label": "ChapterSpecificationSpecificationTypeName",
    "name": "chapterspecname",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 88,
    "sequenceNo": 2,
    "data": { "model": "name" }
}, {
    "nodeId": 91,
    "label": "MinimumSizeInvesmentInfo",
    "name": "minimumsizeinvesmentinfo",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 5,
    "parentNodeId": 33,
    "sequenceNo": 13,
    "repeatFor": "CSMINIMUM SIZE INVESTMENT"
}, {
    "nodeId": 92,
    "label": "MinimumSizeInvesmentTitle",
    "name": "minimumsizeinvesmenttitle",
    "nodeType": "data",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 91,
    "sequenceNo": 1,
    "data": { "model": "codeType" }
}, {
    "nodeId": 93,
    "label": "MinimumSizeInvesment",
    "name": "minimumsizeinvesment",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 91,
    "sequenceNo": 2,
    "repeatFor": "records"
}, {
    "nodeId": 94,
    "label": "ChapterSpecificationSpecificationTypeCodeName",
    "name": "chapterspeccodename",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 93,
    "sequenceNo": 1,
    "data": { "model": "codeName" }
}, {
    "nodeId": 95,
    "label": "ChapterSpecificationSpecificationTypeName",
    "name": "chapterspecname",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 93,
    "sequenceNo": 2,
    "data": { "model": "name" }
}, {
    "nodeId": 96,
    "label": "PreferredSizeInvesmentInfo",
    "name": "preferredsizeinvesmentinfo",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 5,
    "parentNodeId": 33,
    "sequenceNo": 14,
    "repeatFor": "CSPREFERRED SIZE INVESTMENT"
}, {
    "nodeId": 97,
    "label": "PreferredSizeInvesmentTitle",
    "name": "preferredsizeinvesmenttitle",
    "nodeType": "data",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 96,
    "sequenceNo": 1,
    "data": { "model": "codeType" }
}, {
    "nodeId": 98,
    "label": "PreferredSizeInvesment",
    "name": "preferredsizeinvesment",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 96,
    "sequenceNo": 2,
    "repeatFor": "records"
}, {
    "nodeId": 99,
    "label": "ChapterSpecificationSpecificationTypeCodeName",
    "name": "chapterspeccodename",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 98,
    "sequenceNo": 1,
    "data": { "model": "codeName" }
}, {
    "nodeId": 100,
    "label": "ChapterSpecificationSpecificationTypeName",
    "name": "chapterspecname",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 98,
    "sequenceNo": 2,
    "data": { "model": "name" }
}, {
    "nodeId": 101,
    "label": "MaximumSizeInvesmentInOneCompanyInfo",
    "name": "maximumsizeinvesmentinonecompinfo",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 5,
    "parentNodeId": 33,
    "sequenceNo": 15,
    "repeatFor": "CSMAXIMUM SIZE INVESTMENT IN ONE COMPANY"
}, {
    "nodeId": 102,
    "label": "MaximumSizeInvesmentInOneCompanyTitle",
    "name": "maximumsizeinvesmentinonecomptitle",
    "nodeType": "data",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 101,
    "sequenceNo": 1,
    "data": { "model": "codeType" }
}, {
    "nodeId": 103,
    "label": "MaximumSizeInvesmentInOneCompany",
    "name": "maximumsizeinvesmentinonecomp",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 101,
    "sequenceNo": 2,
    "repeatFor": "records"
}, {
    "nodeId": 104,
    "label": "ChapterSpecificationSpecificationTypeCodeName",
    "name": "chapterspeccodename",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 103,
    "sequenceNo": 1,
    "data": { "model": "codeName" }
}, {
    "nodeId": 105,
    "label": "ChapterSpecificationSpecificationTypeName",
    "name": "chapterspecname",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 103,
    "sequenceNo": 2,
    "data": { "model": "name" }
}, {
    "nodeId": 106,
    "label": "FirmWillTakeActionRollAsInfo",
    "name": "firmwilltakeactionroleasinfo",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 5,
    "parentNodeId": 33,
    "sequenceNo": 16,
    "repeatFor": "CSFIRM WILL TAKE ACTIVE ROLE AS"
}, {
    "nodeId": 107,
    "label": "FirmWillTakeActionRollAsTitle",
    "name": "firmwilltakeactionroleastitle",
    "nodeType": "data",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 106,
    "sequenceNo": 1,
    "data": { "model": "codeType" }
}, {
    "nodeId": 108,
    "label": "FirmWillTakeActionRollAs",
    "name": "firmwilltakeactionroleas",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 106,
    "sequenceNo": 2,
    "repeatFor": "records"
}, {
    "nodeId": 109,
    "label": "ChapterSpecificationSpecificationTypeCodeName",
    "name": "chapterspeccodename",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 108,
    "sequenceNo": 1,
    "data": { "model": "codeName" }
}, {
    "nodeId": 110,
    "label": "ChapterSpecificationSpecificationTypeName",
    "name": "chapterspecname",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 108,
    "sequenceNo": 2,
    "data": { "model": "name" }
}, {
    "nodeId": 111,
    "label": "PortfolioRelationshipInfo",
    "name": "portfoliorelationshipinfo",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 5,
    "parentNodeId": 33,
    "sequenceNo": 17,
    "repeatFor": "CSPORTFOLIO RELATIONSHIP"
}, {
    "nodeId": 112,
    "label": "PortfolioRelationshipTitle",
    "name": "portfoliorelationshiptitle",
    "nodeType": "data",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 111,
    "sequenceNo": 1,
    "data": { "model": "codeType" }
}, {
    "nodeId": 113,
    "label": "PortfolioRelationship",
    "name": "portfoliorelationship",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 111,
    "sequenceNo": 2,
    "repeatFor": "records"
}, {
    "nodeId": 114,
    "label": "ChapterSpecificationSpecificationTypeCodeName",
    "name": "chapterspeccodename",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 113,
    "sequenceNo": 1,
    "data": { "model": "codeName" }
}, {
    "nodeId": 115,
    "label": "ChapterSpecificationSpecificationTypeName",
    "name": "chapterspecname",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 113,
    "sequenceNo": 2,
    "data": { "model": "name" }
}, {
    "nodeId": 116,
    "label": "AvgNumberOfDealsCompletedAnnuallyInfo",
    "name": "avgnumberofdealscompletdannuallyinfo",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 5,
    "parentNodeId": 33,
    "sequenceNo": 18,
    "repeatFor": "CSAVERAGE NUMBER OF DEALS COMPLETED ANNUALLY"
}, {
    "nodeId": 117,
    "label": "AvgNumberOfDealsCompletedAnnuallyTitle",
    "name": "avgnumberofdealscompletdannuallytitle",
    "nodeType": "data",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 116,
    "sequenceNo": 1,
    "data": { "model": "codeType" }
}, {
    "nodeId": 118,
    "label": "AvgNumberOfDealsCompletedAnnually",
    "name": "avgnumberofdealscompletdannually",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 116,
    "sequenceNo": 2,
    "repeatFor": "records"
}, {
    "nodeId": 119,
    "label": "ChapterSpecificationSpecificationTypeCodeName",
    "name": "chapterspeccodename",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 118,
    "sequenceNo": 1,
    "data": { "model": "codeName" }
}, {
    "nodeId": 120,
    "label": "ChapterSpecificationSpecificationTypeName",
    "name": "chapterspecname",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 118,
    "sequenceNo": 2,
    "data": { "model": "name" }
}, {
    "nodeId": 121,
    "label": "AvgAmountInvestedAnnuallyInfo",
    "name": "avgamountinvestedannuallyinfo",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 5,
    "parentNodeId": 33,
    "sequenceNo": 19,
    "repeatFor": "CSAVERAGE AMOUNT INVESTED ANNUALLY"
}, {
    "nodeId": 122,
    "label": "AvgAmountInvestedAnnuallyTitle",
    "name": "avgamountinvestedannuallytitle",
    "nodeType": "data",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 121,
    "sequenceNo": 1,
    "data": { "model": "codeType" }
}, {
    "nodeId": 123,
    "label": "AvgAmountInvestedAnnually",
    "name": "avgamountinvestedannually",
    "nodeType": "structural",
    "patternId": "VC",
    "level": 6,
    "parentNodeId": 121,
    "sequenceNo": 2,
    "repeatFor": "records"
}, {
    "nodeId": 124,
    "label": "ChapterSpecificationSpecificationTypeCodeName",
    "name": "chapterspeccodename",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 123,
    "sequenceNo": 1,
    "data": { "model": "codeName" }
}, {
    "nodeId": 125,
    "label": "ChapterSpecificationSpecificationTypeName",
    "name": "chapterspecname",
    "nodeType": "data",
    "patternId": "VC",
    "level": 7,
    "parentNodeId": 123,
    "sequenceNo": 2,
    "data": { "model": "name" }
}];
