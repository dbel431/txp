var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TXN_ExhibitionSchema = new Schema({
    "orgId": {
        type: Schema.Types.ObjectId,
        ref: 'TXN_Organization'
    },
    "directoryId": {
        type: Schema.Types.ObjectId,
        ref: 'MST_RefCodeValue'
    },
    "sequenceNumber": Number,
    "exhibitName": String,
    "showDate": String,
    "startDate": Date,
    "endDate": Date,
    "startDateText": String,
    "endDateText": String,
    "travelFlag": String,
    active: {
        type: Boolean,
        default: true
    },
    deleted: {
        type: Boolean,
        default: false
    },
    "created": {
        "by": {
            "type": Schema.Types.ObjectId,
            "ref": 'TXN_User'
        },
        "at": Date
    },
    "updated": {
        "by": {
            "type": Schema.Types.ObjectId,
            "ref": 'TXN_User'
        },
        "at": Date
    }
});

TXN_ExhibitionSchema.index({ sequenceNumber: 1, _id: 1 });
TXN_ExhibitionSchema.index({ sequenceNumber: -1, _id: 1 });
TXN_ExhibitionSchema.index({ sequenceNumber: -1, _id: -1 });

module.exports = mongoose.model('TXN_Exhibition', TXN_ExhibitionSchema);
