module.exports = {
    "AUN": {
        xmlArtSchoolIds: ['576285a6c19ce4590952a8d5', '576285a6c19ce4590952a8d8', '576285a6c19ce4590952a8db'],
        xmlCounterLimit: 10,
        // xmlCountrySort: { codeValue: -1 },
        xmlOrgSortBy: { sortName: 1 },
        // xmlOrgDataMaps: {
        //     'onlineSales': {
        //         "D": "Direct online sales",
        //         "I": "Indirect online sales"
        //     },
        //     'expenses': {
        //         "0": null,
        //         0: null
        //     },
        //     'income': {
        //         "0": null,
        //         0: null
        //     },
        //     'attendance.method': {
        //         "A": "accurate",
        //         "E": "estimated"
        //     },
        //     'attendance.count': function(x) {
        //         if (isNaN(parseFloat(x))) return x;
        //         var parts = x.toString().split(".");
        //         parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        //         return parts.join(".");
        //     }
        // },
        xmlOrgIndexFlag: false,
        xmlOrgLimit: null,
        xmlOrgSkip: null,
        xmlPersonnelReps: null,
        xmlPersonnelSkip: null,
        xmlAddrPrefOnly: false,
        xmlAddrPref1: "Physical Address",
        xmlAddrPref2: "Mailing Address",
        abbrCC: function(cc) {
            return {
                "Organization/Association": "O",
                "Museum": "M",
                "Library": "L",
                "Corporation": "C",
                "X-Ref": "x-ref",
                "em-dash": "em-dash",
                "indented em-dash": "indented em-dash",
            }[cc];
        }
    },
    "ACN": {
        xmlArtSchoolIds: ['576285a6c19ce4590952a8d5', '576285a6c19ce4590952a8d8', '576285a6c19ce4590952a8db'],
        xmlCounterLimit: 10,
        xmlOrgSortBy: { sortName: 1 },
        xmlAddrPref1: "Physical Address",
        xmlAddrPref2: "Mailing Address",
        abbrCC: function(cc) {
            return {
                "Organization/Association": "O",
                "Museum": "M",
                "Library": "L",
                "Corporation": "C",
                "X-Ref": "x-ref",
                "em-dash": "em-dash",
                "indented em-dash": "indented em-dash",
            }[cc];
        }
    },
    "AUM": {
        xmlArtSchoolIds: ['576285a6c19ce4590952a8d5', '576285a6c19ce4590952a8d8', '576285a6c19ce4590952a8db'],
        xmlCounterLimit: 200,
        xmlOrgSortBy: { sortName: 1 },
        xmlAddrPref1: "Physical Address",
        xmlAddrPref2: "Mailing Address",
        abbrCC: function(cc) {
            return {
                "Organization/Association": "A",
                "Museum": "M",
                "Library": "L",
                "Corporation": "C",
                "X-Ref": "x-ref",
                "em-dash": "em-dash",
                "indented em-dash": "indented em-dash",
            }[cc];
        }
    },
    "ACM": {
        xmlArtSchoolIds: ['576285a6c19ce4590952a8d5', '576285a6c19ce4590952a8d8', '576285a6c19ce4590952a8db'],
        xmlCounterLimit: 100,
        xmlOrgSortBy: { sortName: 1 },
        xmlAddrPref1: "Physical Address",
        xmlAddrPref2: "Mailing Address",
        abbrCC: function(cc) {
            return {
                "Organization/Association": "A",
                "Museum": "M",
                "Library": "L",
                "Corporation": "C",
                "X-Ref": "x-ref",
                "em-dash": "em-dash",
                "indented em-dash": "indented em-dash",
            }[cc];
        }
    },
    "AAM": {
        xmlArtSchoolIds: ['576285a6c19ce4590952a8d5', '576285a6c19ce4590952a8d8', '576285a6c19ce4590952a8db'],
        xmlCounterLimit: 150,
        xmlOrgSortBy: { sortName: 1 },
        xmlAddrPref1: "Physical Address",
        xmlAddrPref2: "Mailing Address",
        abbrCC: function(cc) {
            return {
                "Organization/Association": "A",
                "Museum": "M",
                "Library": "L",
                "Corporation": "C",
                "X-Ref": "x-ref",
                "em-dash": "em-dash",
                "indented em-dash": "indented em-dash",
            }[cc];
        }
    },
    "AUS": {
        xmlArtSchoolIds: ['576285a6c19ce4590952a8d5', '576285a6c19ce4590952a8d8', '576285a6c19ce4590952a8db'],
        xmlCounterLimit: 200,
        xmlOrgSortBy: { sortName: 1 },
        xmlAddrPref1: "Physical Address",
        xmlAddrPref2: "Mailing Address",
        abbrCC: function(cc) {
            return {
                "X-Ref": "x-ref",
                "em-dash": "em-dash",
                "indented em-dash": "indented em-dash",
            }[cc];
        }
    },
    "ACS": {
        xmlArtSchoolIds: ['576285a6c19ce4590952a8d5', '576285a6c19ce4590952a8d8', '576285a6c19ce4590952a8db'],
        xmlCounterLimit: 20,
        xmlOrgSortBy: { sortName: 1 },
        xmlAddrPref1: "Physical Address",
        xmlAddrPref2: "Mailing Address",
        abbrCC: function(cc) {
            return {
                "X-Ref": "x-ref",
                "em-dash": "em-dash",
                "indented em-dash": "indented em-dash",
            }[cc];
        }
    },
    "AAS": {
        xmlArtSchoolIds: ['576285a6c19ce4590952a8d5', '576285a6c19ce4590952a8d8', '576285a6c19ce4590952a8db'],
        xmlCounterLimit: 15,
        xmlOrgSortBy: { sortName: 1 },
        xmlAddrPref1: "Physical Address",
        xmlAddrPref2: "Mailing Address",
        abbrCC: function(cc) {
            return {
                "X-Ref": "x-ref",
                "em-dash": "em-dash",
                "indented em-dash": "indented em-dash",
            }[cc];
        }
    },
    "SUB": {
        xmlArtSchoolIds: ['576285a6c19ce4590952a8d5', '576285a6c19ce4590952a8d8', '576285a6c19ce4590952a8db'],
        xmlCounterLimit: 200,
        xmlOrgSortBy: { sortName: 1 },
        xmlOrgIndexFlag: false,
        xmlAddrPrefOnly: true,
        xmlAddrPref1: "Physical Address",
        xmlAddrPref2: "Mailing Address",
        abbrCC: function(cc) {
            return {}[cc];
        }
    },
    "PER": {
        xmlArtSchoolIds: ['576285a6c19ce4590952a8d5', '576285a6c19ce4590952a8d8', '576285a6c19ce4590952a8db'],
        xmlCounterLimit: 1500,
        xmlOrgSortBy: { sortName: 1 },
        xmlOrgIndexFlag: false,
        xmlPersonnelReps: 1500,
        xmlPersonnelSkip: 0,
        xmlAddrPrefOnly: true,
        xmlAddrPref1: "Physical Address",
        xmlAddrPref2: "Mailing Address",
        abbrCC: function(cc) {
            return {}[cc];
        }
    },
    "ORG": {
        xmlArtSchoolIds: ['576285a6c19ce4590952a8d5', '576285a6c19ce4590952a8d8', '576285a6c19ce4590952a8db'],
        xmlCounterLimit: 10,
        xmlOrgSortBy: { sortNameIndex: 1 },
        xmlOrgIndexFlag: true,
        xmlAddrPrefOnly: true,
        xmlAddrPref1: "Physical Address",
        xmlAddrPref2: "Mailing Address",
        abbrCC: function(cc) {
            return {
                "Organization/Association": "A",
                "Museum": "M",
                "Library": "L"
            }[cc];
        }
    },
    // OMD 57189b3e24d8bc65f4123bbf
    "IBS": {
        xmlCounterLimit: 1000,
        xmlOrgSortBy: { sortMajorName: 1 },
        xmlOrgDataMaps: {
            'onlineSales': {
                "D": "Direct online sales",
                "I": "Indirect online sales"
            },
            'expenses': {
                "0": null,
                0: null
            },
            'income': {
                "0": null,
                0: null
            },
            'attendance.method': {
                "A": "accurate",
                "E": "estimated"
            },
            'attendance.count': function(x) {
                if (isNaN(parseFloat(x))) return x;
                var parts = x.toString().split(".");
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                return parts.join(".");
            }
        },
        xmlAddrPref1: "Physical Address",
        xmlAddrPref2: "Mailing Address"
    },
    "IIBC": {
        xmlCounterLimit: 50,
        xmlOrgSortBy: { sortMajorName: 1 },
        xmlAddrPrefOnly: true,
        xmlAddrPref1: "Physical Address",
        xmlAddrPref2: "Mailing Address"
    },
    "ITI": {
        xmlCounterLimit: 10,
        xmlOrgSortBy: { sortMajorName: 1 },
        xmlAddrPrefOnly: true,
        xmlAddrPref1: "Physical Address",
        xmlAddrPref2: "Mailing Address"
    },
};
