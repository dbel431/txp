var mongoose = require('mongoose');
var OrganizationFormerNamesSchema = require('./schemas/organization.former-names.schema.js');
var OrganizationAddressSchema = require('./schemas/organization.address.schema.js');
var OrganizationFeaturesSchema = require('./schemas/organization.features.schema.js');
var OrganizationResearchSchema = require('./schemas/organization.research.schema.js');
var OrganizationPublicationSchema = require('./schemas/organization.publication.schema.js');
var OrganizationNotesSchema = require('./schemas/organization.notes.schema.js');
var OrganizationListingTypeSchema = require('./schemas/organization.listing-type.schema.js');
var OrganizationMembersSchema = require('./schemas/organization.members.schema.js');
var OrganizationVendorsSchema = require('./schemas/organization.vendors.schema.js');
// For OCD
var OrganizationOCDContactSchema = require('./schemas/organization.ocdContact.schema.js');
var OrganizationLegalTitlesSchema = require('./schemas/organization.legal-titles.schema.js');
//var OrganizationCrossReferenceSchema = require('./schemas/organization.cross-reference.schema.js');
var OrganizationStatisticSchema = require('./schemas/organization.statistic.schema.js');
var OrganizationStathistSchema = require('./schemas/organization.stathist.schema.js');
var OrganizationDirectMarketingBudgetDisbursalSchema = require('./schemas/organization.budget.schema.js');
var OrganizationBookEditionSchema = require('./schemas/organization.bookEdition.schema.js');
var Schema = mongoose.Schema;

var TXN_OrganizationSchema = new Schema({
    "parentId": {
        "type": Schema.Types.ObjectId,
        "ref": 'TXN_Organization'
    },
    "directoryId": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "org_id": String,
    "orgIdNumber": Number, // for sorting numerically (mongoose-datatables)
    "sequenceNo": Number,
    "sub_nbr": String,
    "subNo": Number,
    //"additionalInfo": String,
    "additionalInformation": String,
    "nameSuffix": String, // all
    "name": String, // all
    "sortName": String, //all Sorting only
    "sortNameIndex": String, //orgIndex Sorting only
    "shortName": String, // all
    "sortMajorName": String, // all
    "sortMinorName": String, // AAD
    "subName": String, // all
    "aamId": String,
    "established": {
        "detail": String,
        "year": String
    },
    "minorNameIndexIndication": Boolean,
    "entityType": String,
    "timing": String,
    "membership": [String],
    "attendance": {
        "count": String,
        "countNumber": Number,
        "method": String
    },
    "totalAdvBudgetAmount": Number,
    "congressDistrict": String,
    "flags": {
        "archdiocese": Boolean,
        "taxExempt": Boolean,
        "giftShop": Boolean,
        "nameSuppress": Boolean,
        "wrapIndex": Boolean,
        "income": Boolean,
        "handicapped": Boolean,
        "accredited": Boolean,
        "firstMail": Boolean,
        "mailer": String,
        "secondMail": Boolean,
        "otherSrc": Boolean,
        "nrm": Boolean,
        "icom": Boolean,
        "aam": Boolean,
        "americanAssociationMuseum": Boolean,
        "participateLeaseOthers": Boolean,
        "thirdParty": Boolean,
        "jointlyWithCommission": Boolean,
        "annualContractualExpenses": Boolean,
        "shortTermCashFund": Boolean,
        "zeroCashBalance": Boolean,
        "lineAccessAvailable": Boolean,
        "dmaCode": Boolean,
        "catalogOnline": Boolean //This field is not present in AAD,CFS,OMD
    },
    "handicapped": String,
    "formerNames": [OrganizationFormerNamesSchema],
    "bookEdition": [OrganizationBookEditionSchema],
    "attention": String,
    "address": [OrganizationAddressSchema],
    "contact": {
        "contactType": String,
        "phoneNumbers": String,
        "officeNumbers": String,
        "faxNumbers": String,

        "emails": {
            "primary": String,
            "secondary": String
        },
        "websites": String,
        "social": String,
        "printFlag": Boolean,
        "suppress": String,

        //AAD Only
        "wats": String,
        "others": String,
        "telexNumbers": String,
        "cable": String,
        "fts": String,
        "twx": String,
        "pager": String,
        "mobile": String,
    },
    //for ocd
    "header": {
        "preText": String,
        "bold": Boolean,
        "italic": Boolean,
        "center": Boolean,
        "largeFont": Boolean,
        "underline": Boolean,
        "county": String,
        "state": String
    },
    "ocdContact": [OrganizationOCDContactSchema],
    // "personnel": [{
    //     "type": Schema.Types.ObjectId,
    //     "ref": 'TXN_Personnel'
    // }],
    "volunteer": {
        "totalPartTimePaid": Number,
        "totalPartTimeUnpaid": Number,
        "totalFullTimePaid": Number,
        "totalFullTimeUnpaid": Number,
        "totalIntern": Number
    },
    "volunteerHours": String,
    "features": [OrganizationFeaturesSchema],
    "research": [OrganizationResearchSchema],
    "stathist": [OrganizationStathistSchema],
    "publication": [OrganizationPublicationSchema],
    "activities": {
        "freeText": String, // for OMD
        "sequenceNumber": Number,
        "educationDept": String,
        "lecturesAwards": String,
        "extensionDept": String,
        "bookTravelingExhib": String,
        "origiTravelingExhib": String,
        "museumShop": String,
        "juniorMuseum": String,
        // New added fields 11 June 2016
        "scholarships": String,
    },
    "income": String,
    "expenses": String,
    "purchases": String,
    "governing": String,


    // AAD
    "circulation": String,
    "classificationCode": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "sectionCode": [{
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    }],
    //"bookEdition": [Number],
    "feeStructure": String,
    "useParentAddress": Boolean,
    "useParentContact": Boolean,
    "galleryDescription": String,
    "purchases": String,
    "artSchool": [{
        "scholarship": Boolean,
        "fellowships": Boolean,
        "assistantships": Boolean,
        "grants": Boolean,
        "loans": Boolean,
        "workStudy": Boolean,
        "financialAid": Boolean,
        "coursesOffered": String,
        "enrollment": String,
        "tuition": String,

        // changes for UI
        // "adult_hobby_class": String,
        "adultHobbyClass": String,

        "childrenClasses": String,
        "summerSchool": String,
        "instructors": String,
        "control": String,
        "classTime": String,
        "entranceRequired": String,
        "degreeGranted": String,
        "scholarshipFellowships": String
    }],


    // CFS
    "partner": String,
    "members": [{
        "name": String,
        "listingType": {
            "type": Schema.Types.ObjectId,
            "ref": 'MST_RefCodeValue'
        },
        "active": Boolean
    }],
    "assets": String,
    "lendingLimit": String,
    "ownedBy": String,
    "fundsAvailable": String,
    "privatePlacementPortfolio": String,
    "privatePlacementFunds": String,
    "loanPortfolioOutstanding": String,
    "minLoanHandled": String,
    "originalCostLeasingPortfolio": String,
    "grossReceivableLeasingPortfolio": String,
    "avgEquityLease": String,
    "primaryEquipementLeased": String,
    "preferredLeaseTerm": String,
    "yearsLeasing": String,
    "leadBank": String,
    "totalAssetsLeadBank": String,
    "clientMixBuyer": String,
    "clientMixSeller": String,
    "averageBuyerRepresented": String,
    "averageSellerRepresented": String,
    "averageDealsClosedThreeYear": String,
    "dealSizeHandled": String,
    "parentName": String,
    "minimumDealSizeHandled": String,
    "pensionManagementYear": String,
    "pensionAssetsManagement": String,
    "pensionReportInterval": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "masterTrustClient": String,
    "masterTrustAssets": String,
    "clientReceiveReportInterval": String,
    "compensationMethod": String,
    "numberOfOffices": {
        "us": Number,
        "wordwide": Number
    },
    "averageAnnualRevenues": String,
    "averageAnnualCorporateAssignment": String,
    "annualRevenues": {
        "us": String,
        "wordwide": String
    },
    "feePercentage": {
        "professionalService": Number,
        "commision": Number
    },
    "officesCountries": Number,
    "overseasAffiliates": Number,
    "subsidiaryOf": String,
    "specialStrength": String,
    //CFS Chnaged by sagar
    // "portfolioManagersOnStaff": Number,
    "professionalStaff": {
        "portfolioManagersOnStaff": Number,
        "marketingDepartment": Number,
        "developmentDepartment": Number,
        "technicalDepartment": Number
    },
    "ownerCountry": String,
    "comments": [String],

    // Added by Swapnil Sonawane 26/05/2016 

    "parentAddress": String,
    "parentAssets": String,
    //Added on 17/08/2016
    "averagePrivatePlacementParticipant": String,
    "loanCeiling": String,
    "averagePortfolioMaturity": String,
    "intermediaryLoanPercentage": String,


    // DMMP

    "advanceBillType": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "totalAdvertisingBudget": String,
    "primaryMarket": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "accountPreference": String,
    "dmaCode": String,
    "conductBusiness": String,
    "onlineSales": String, // Direct/Indirect
    "directMarketingBudget": String,
    "directMarketingBudgetDisbursal": [{ // create code type for direct marketing ad heads
        "code": {
            "type": Schema.Types.ObjectId,
            "ref": 'MST_RefCodeValue'
        },
        "value": String,
    }],
    "annualMailed": Number,
    "employees": String,
    // "annualRevenues": [{
    //     "us": Number,
    //     "wordwide": Number
    // }],
    "notes": [OrganizationNotesSchema],
    /*"listin gType": [{
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    }],*/
    "logo": String,
    "listingType": [OrganizationListingTypeSchema],
    "members": [OrganizationMembersSchema],
    "vendors": [OrganizationVendorsSchema],
    "directMarketingBudgetDisbursal": [OrganizationDirectMarketingBudgetDisbursalSchema],


    //-- Denormalized Data

    "personnel": [{
        "type": Schema.Types.ObjectId,
        "ref": 'TXN_Personnel',
    }],
    "exhibition": [{
        "type": Schema.Types.ObjectId,
        "ref": 'TXN_Exhibition'
    }],
    "chapterSpecification": [{
        "type": Schema.Types.ObjectId,
        "ref": 'TXN_ChapterSpecification'
    }],

    // For DMMP 18/08/2016
    "dmaId": String,
    "mailingList": {
        "mailListAvailableIndex": Boolean,
        "emailListAvailableIndex": Boolean,
        "prefix": String,
        "first": String,
        "middle": String,
        "last": String,
        "suffix": String,
        "MailingContact": String,
        "extension": String,
        "postBox": String,
        "street1": String,
        "city": {
            "type": Schema.Types.ObjectId,
            "ref": 'MST_RefCodeValue'
        },
        "cityName": String,
        "state": {
            "type": Schema.Types.ObjectId,
            "ref": 'MST_RefCodeValue'
        },
        "stateName": String,
        "zip": String,
        "email": String
    },

    //For OCD
    "dioceseType": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "dioceseTypeName": String,
    "abbrevationName": String,
    "latinName": String,
    "squareMiles": String,
    "supplementType": String,
    "mastheadImage": String,
    "dioProvince": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "dioProvinceName": String,
    "mailerType": String,
    "outstandingIssues": String,
    "parishShrine": {
        "ethnicityType1": {
            "type": Schema.Types.ObjectId,
            "ref": 'MST_RefCodeValue'
        },
        "ethnicityType1Name": String,
        "ethnicityText1": String,
        "ethnicityType2": {
            "type": Schema.Types.ObjectId,
            "ref": 'MST_RefCodeValue'
        },
        "ethnicityType2Name": String,
        "ethnicityText2": String,
        "parishStatus": {
            "type": Schema.Types.ObjectId,
            "ref": 'MST_RefCodeValue'
        },
        "cemeteries": String,
        "jointCemeteries": String,
        "spanishMass": Boolean
    },
    "EINumber": String,
    "purpose": String,
    "placementCity": String,
    "akaDbaIndex": String,
    "akaDbaName": String,
    "kennedyId": String,
    "locale": String,
    "crossRefId": String,
    "legalTitles": [OrganizationLegalTitlesSchema],
    // "crossReference": [OrganizationCrossReferenceSchema],
    "sales": {
        "nixie": Boolean,
        "name1": String,
        "name2": String
    },
    "statistic": [OrganizationStatisticSchema],
    "organizationOffice": {
        "name": String,
        "additionalInfo": String
    },
    "religiousOrder": {
        "religiousType": {
            "type": Schema.Types.ObjectId,
            "ref": 'MST_RefCodeValue'
        },
        "religiousTypeName": String,
        "kennedyId": String,
        "secondaryName": String,
        "tertiaryName": String,
        "pontificalOrDiocese": String,
        "initials": String
    },
    "school": {
        "schoolType": {
            "type": Schema.Types.ObjectId,
            "ref": 'MST_RefCodeValue'
        },
        "schoolTypeName": String,
        "consolidated": Boolean,
        "college": Boolean,
        "gradeTypeStart": {
            "type": Schema.Types.ObjectId,
            "ref": 'MST_RefCodeValue'
        },
        "gradeTypeEnd": {
            "type": Schema.Types.ObjectId,
            "ref": 'MST_RefCodeValue'
        }
    },
    "classificationCodeName": String,

    //--

    "workflowStatus": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "status": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "statusId": Boolean,
    "versionId": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "active": {
        "type": Boolean,
        "default": true
    },
    "deleted": {
        "type": Boolean,
        "default": false
    },
    "created": {
        "by": {
            "type": Schema.Types.ObjectId,
            "ref": 'TXN_User'
        },
        "at": Date
    },
    "updated": {
        "by": {
            "type": Schema.Types.ObjectId,
            "ref": 'TXN_User'
        },
        "at": Date
    }
});

TXN_OrganizationSchema.index({ directoryId: 1, _id: 1 });
TXN_OrganizationSchema.index({ orgIdNumber: 1, _id: 1 });
TXN_OrganizationSchema.index({ orgIdNumber: -1, _id: 1 });
TXN_OrganizationSchema.index({ orgIdNumber: -1, _id: -1 });
TXN_OrganizationSchema.index({ sortName: 1 });
TXN_OrganizationSchema.index({ sortNameIndex: 1 });
TXN_OrganizationSchema.index({ sortMajorName: 1 });
TXN_OrganizationSchema.index({ name: 1, _id: 1 });
TXN_OrganizationSchema.index({ name: -1, _id: 1 });
TXN_OrganizationSchema.index({ name: -1, _id: -1 });
TXN_OrganizationSchema.index({ 'address.street1': 1, _id: 1 });
TXN_OrganizationSchema.index({ 'address.street1': -1, _id: 1 });
TXN_OrganizationSchema.index({ 'address.street1': -1, _id: -1 });
TXN_OrganizationSchema.index({ 'address.cityName': 1, _id: 1 });
TXN_OrganizationSchema.index({ 'address.cityName': -1, _id: 1 });
TXN_OrganizationSchema.index({ 'address.cityName': -1, _id: -1 });
TXN_OrganizationSchema.index({ 'address.stateName': 1, _id: 1 });
TXN_OrganizationSchema.index({ 'address.stateName': -1, _id: 1 });
TXN_OrganizationSchema.index({ 'address.stateName': -1, _id: -1 });
TXN_OrganizationSchema.index({ 'address.countryName': 1, _id: 1 });
TXN_OrganizationSchema.index({ 'address.countryName': -1, _id: 1 });
TXN_OrganizationSchema.index({ 'address.countryName': -1, _id: -1 });
TXN_OrganizationSchema.index({ 'address.countyName': 1, _id: 1 });
TXN_OrganizationSchema.index({ 'address.countyName': -1, _id: 1 });
TXN_OrganizationSchema.index({ 'address.countyName': -1, _id: -1 });

module.exports = mongoose.model('TXN_Organization', TXN_OrganizationSchema);
