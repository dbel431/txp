var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SummaryReportCountSchema = new Schema({
    "summaryType": String,
    "directoryId": [{
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    }],
    "recordType": String,
    "sourceCount": Number,
    "migratedCount": Number,
    "targetCount": Number
});

module.exports = mongoose.model('summary_count', SummaryReportCountSchema);
