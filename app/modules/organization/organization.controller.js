var async = require('async');
var iconv = require('iconv-lite');
var json2csv = require('json2csv');
var xmlbuilder = require('xmlbuilder');
var fs = require('fs');
var path = require('path');
var dateFormat = require('dateformat');
var coreQueryBuilderService = require('../core/core.query-builder.service.js');
var refCodeTypeService = require('../ref-code/refCodeType.service.js');
var refCodeValueService = require('../ref-code/refCodeValue.service.js');
var organizationService = require('./organization.service.js');
var xmlStructureService = require('../xml/xml.structure.service.js');
var fileLogService = require('../core/file-log.service.js');

var RECORDINDEX = 0,
    COLUMNINDEX = 0,
    COLUMNMAX = 1,
    EXCLUDEPARENTS = {},
    PREVRECORDID;

module.exports = {
    // get
    getChapterSpecificationData: function(req, res) {
        var dt = req.body.dataTableQuery;
        var query = coreQueryBuilderService.buildQuery(req);
        var xqr=req.body.conditions;
            organizationService.getChapterSpecificationData(xqr, function(err, data) {
            var query = {};
            if(data.length != 0){
            query['_id'] = { $in: data[0].ids };
            organizationService.dataTable({
                dataTableQuery: dt,
                conditions: query
            }, function(err, organizations) {
                if (err) return res.status(400).send(err);
                return res.send(organizations);
            });    
            }
            else
            {
                res.send({ "draw": 1, "recordsTotal": 0, "recordsFiltered": 0, "data": [] });   
            }
            
        });
    },
    get: function(req, res, next) {
        req.queryErrors = [];
        var query = coreQueryBuilderService.buildQuery(req);
        organizationService.get(query, function(err, organizations) {
            if (err) return res.status(400).send(err);
            if (!organizations) req.queryErrors.push({ message: 'Could not fetch Organizations' });
            else req.organization = (discardDeleted(organizations))[0];
            next();
        });
    },
    getPersonnelList: function(req, res, next) {
        var query = coreQueryBuilderService.buildQuery(req);
        organizationService.getPersonnelList(query, function(err, personnel) {
            if (err) return res.status(400).send(err);
            if (!personnel) return res.status(400).send({ message: 'Could not fetch Personnel' });
            return res.send(personnel);
        });
    },
    getComplete: function(req, res) {
        return res.send({ data: req.organization, errors: req.queryErrors });
    },
    // export
    xmlImportStructure: function(req, res) {
        var xmlStructure = require('./tmp.xml.structure.js');
        xmlStructureService.addMany(xmlStructure, function(err, data) {
            res.send({ err: err, data: data.length });
        });
    },
    exportToXMLGetStructure: function(req, res, next) {
        req.xmlErrors = [];

        // @TODO: Mock. Need to change during actual implementation
        req.query = {
            patternId: "SUB",
            // directoryId: "57189b3e24d8bc65f4123bbf", // OMD
            directoryId: "57189c7024d8bc65f4123bc0", // AAD
        };

        var patternId = req.query.patternId,
            config = require('./xml.config.js')[patternId];
        req.query = {
            _where: {
                directoryId: req.query.directoryId,
                active: true,
                deleted: false
            }
        };
        var query = {
            directoryId: req.query._where.directoryId,
            patternId: patternId,
            active: true,
            deleted: false
        };

        Object.assign(req, config);

        req.xmlStructure = [];
        req.xmlStructureMap = {};
        req.xmlQueryExt = {};
        console.log('XML: Fetching structure...');
        xmlStructureService.get(query, function(err, xmlStructure) {
            if (err) req.xmlErrors.push(err);
            if (!xmlStructure) req.xmlErrors.push({ message: "Could not fetch XML Structure" });
            req.xmlStructure = xmlStructure;
            req.xmlStructure.forEach(function(element, index) {
                req.xmlStructureMap[element._id] = element;
                req.xmlStructureMap[element._id].children = [];
                if (element.parentNode && req.xmlStructureMap[element.parentNode._id] && req.xmlStructureMap[element.parentNode._id].children) {
                    req.xmlStructureMap[element.parentNode._id].children.push(element);
                    req.xmlStructureMap[element.parentNode._id].children.sort(function(f1, f2) {
                        return (f1.sequenceNo - f2.sequenceNo);
                    });
                }
                if (element.repeatFor && element.repeatConditions && element.repeatConditions.length > 0) {
                    if (!req.xmlQueryExt[element.repeatFor]) req.xmlQueryExt[element.repeatFor] = {};
                    element.repeatConditions.forEach(function(rc) {
                        req.xmlQueryExt[element.repeatFor][rc.key] = parseQueryExt(rc.value);
                    });
                }
                if (element.replicateNode && req.xmlStructureMap[element._id]) req.xmlStructureMap[element._id].replicate = element.replicateNode._id;
            });
            next();
        });
    },
    exportToXML: function(req, res, next) {
        req.xmlFilename = 'export-as-xml-' + (new Date()).getTime() + '.xml'
        var xmlJson = {},
            rootNode, excludeSections = [],
            fetchingOrganizations = false,
            hasTraversedChildren, lastRootChild, levelOffset = 0,
            working = false,
            chunkSize = 20,
            funcMap = {
                sectionCodes: {
                    func: getSectionsForXML,
                    queryExtKey: {
                        'organizations': [{ value: null, key: "parentId" }, { value: "_id", key: 'sectionCode' }]
                    }
                },
                organizations: {
                    func: getOrganizationForXML
                },
                allOrgs: {
                    func: getOrgsForXML
                },
                alphabeticalOrgs: {
                    func: getAlphabeticalOrgsForXML
                },
                countries: {
                    func: getCountriesForXML,
                    queryExtKey: {
                        'cities': { value: "_id", key: "address.country" },
                        'stateOrgs': { value: "_id", key: "address.country" },
                        'cityOrgs': { value: "_id", key: "address.country" },
                    }
                },
                states: {
                    func: getStatesForXML,
                    queryExtKey: {
                        'cities': { value: "_id", key: "address.state" },
                        'cityOrgs': { value: "_id", key: "address.state" },
                        'allOrgs': [
                            { value: "_id", key: "address.state" }
                        ]
                    }
                },
                cities: {
                    func: getCitiesForXML,
                    queryExtKey: {
                        'organizations': [
                            { value: null, key: "parentId" },
                            { value: "_id", key: "address.city" }
                        ]
                    }
                },
                codedSubjects: {
                    func: getCodedSubjectsForXML,
                    queryExtKey: {
                        'allOrgs': { value: "ids", key: "_id" }
                    }
                },
                collections: {
                    func: getCollectionsForXML,
                    queryExtKey: {
                        'allOrgs': { value: "ids", key: "_id" }
                    }
                },
                personnel: {
                    func: getPersonnelForXML,
                    queryExtKey: {
                        'allOrgs': { value: "orgId", key: "_id" }
                    }
                },
                onlyListingTypes: {
                    func: getOnlyListingTypesForXML,
                    queryExtKey: {
                        'organizations': [
                            { value: "active", key: "listingType.listingActive" },
                            { value: "_id", key: "listingType.listing" }
                        ]
                    }
                },
                listingTypes: {
                    func: getListingTypesForXML
                },
                listingSectionCodes: {
                    queryExtKey: {
                        'organizations': [
                            { value: null, key: "parentId" },
                            { value: "active", key: "listingType.listingActive" },
                            { value: "_id", key: "listingType.listingSectionCode" }
                        ]
                    }
                },
                categories: {
                    func: getOrgCategoriesForXML
                },
                subCategories: {
                    queryExtKey: {
                        'allOrgs': [
                            { value: "_id", key: "features.code" }
                        ]
                    }
                }
            };
        req.xmlStructure.forEach(function(element, index) {
            if (!element.parentNode) {
                rootNode = element;
            }
        });
        try {
            console.log('XML: Creating ROOT node...');
            createElement(rootNode, true, null, req.xmlQueryExt);
        } catch (error) {
            console.log('In catch', error);
        }
        setTimeout(function() {
            var prevOffset, cnt = 0;
            var int = setInterval(function() {
                if (!working && !fetchingOrganizations && prevOffset == levelOffset) {
                    cnt++;
                    console.log('Completed possibility..', (req.xmlCounterLimit - cnt) + " sec remaining..");
                    if (cnt == req.xmlCounterLimit) {
                        console.log('Writing file...');
                        fs.writeFile(process.env.EXPORT_AS_CSV_PATH + '/' + req.xmlFilename, iconv.encode(xmlbuilder.create(cleanJson(xmlJson), {
                            skipNullAttributes: true
                        }).end({
                            pretty: true,
                            indent: '  ',
                            newline: '\n',
                            allowEmpty: false
                        }), 'UTF-16'), function(err) {
                            if (err) xmlErrors.push(err);
                            req.resData = { xmlFilename: req.xmlFilename };
                            console.log('XML done...');
                        });
                        clearInterval(int);
                        next();
                    }
                } else cnt = 0;
                prevOffset = levelOffset;
            }, 500);
        }, 500);

        function createElement(element, isRootNode, data, queryExt, callback) {
            hasTraversedChildren = !isRootNode;
            levelOffset++;
            var xmlElement = isRootNode ? xmlJson : {},
                proxy, dontPrint = false;
            xmlElement[element.label] = {};
            if (req.xmlStructureMap[element._id].replicate) {
                proxy = element.label;
                element = req.xmlStructureMap[req.xmlStructureMap[element._id].replicate];
            }
            if (element.attributes && element.attributes.length > 0) {
                element.attributes.forEach(function(attribute, index) {
                    xmlElement[(proxy || element.label)][('@' + attribute.key)] = attribute.value;
                });
            }
            if (data && element.data && element.nodeType == 'data') {
                var text;
                if (element.data.isNested && element.data.model.split('.').length > 1) {
                    var text = data;
                    element.data.model.split('.').forEach(function(s) {
                        text = (text[s] || "");
                    });
                } else text = (data[element.data.model] || "");
                if (element.data.valuePath) text = (text[element.data.valuePath] || "");
                if (text && text.constructor === Array && text.length > 0) text = text.join(' ');
                if (text && text != "") {
                    if (typeof text === 'string' && text.indexOf("indented em-dash") >= 0) xmlElement[(proxy || element.label)]['#raw'] = (text || "").replace(/indented em-dash/gi, "&emsp;&mdash;");
                    else if (typeof text === 'string' && text.indexOf("em-dash") >= 0) xmlElement[(proxy || element.label)]['#raw'] = (text || "").replace(/em-dash/gi, "&mdash;");
                    else if ([
                            "ArtSchoolEntranceRequired",
                            "ArtSchoolTuition",
                            "ArtSchoolAdultHobbyClasses",
                            "ArtSchoolChildrenClasses",
                            "ArtSchoolSummerSchool"
                        ].indexOf(element.label) >= 0) {
                        // standard entity conversions
                        text = (text || "").replace(/</g, "&lt;");
                        text = (text || "").replace(/>/g, "&gt;");
                        text = (text || "").replace(/'/g, "&apos;");
                        text = (text || "").replace(/"/g, "&quot;");
                        text = (text || "").replace(/&/g, "&amp;");
                        // custom entity conversions
                        text = (text || "").replace(/_/g, "&mdash;");

                        xmlElement[(proxy || element.label)]['#raw'] = (text || "");
                    } else xmlElement[(proxy || element.label)]['#text'] = (text || "");
                }
            }
            if (req.xmlStructureMap[element._id] && req.xmlStructureMap[element._id].children && req.xmlStructureMap[element._id].children.length > 0) {
                req.xmlStructureMap[element._id].children.forEach(function(child, index) {
                    if (child.nodeType == 'structural' && child.repeatFor) {
                        xmlElement[(proxy || element.label)][child.label] = [];
                        if (child.repeatConditions && child.repeatConditions.length > 0) {
                            child.repeatConditions.forEach(function(repeatCondition) {
                                if (!queryExt[child.repeatFor]) queryExt[child.repeatFor] = {};
                                if (!queryExt[child.repeatFor][repeatCondition.key]) queryExt[child.repeatFor][repeatCondition.key] = repeatCondition.value;
                            });
                        }
                        if (data && data[child.repeatFor]) {
                            createChildElements(null, data[child.repeatFor]);
                        } else if (funcMap[child.repeatFor]) {
                            funcMap[child.repeatFor]['func'](createChildElements, queryExt, (element.label == 'Countries'));
                        }

                        function createChildElements(err, newData) {
                            if (child.label == 'Org' && newData) {
                                console.log('recieved : ', newData.length);
                                newData.forEach(function(organization) {
                                    if (organization.org_id == "101050") {
                                        console.log(organization.shortName, organization.minorNameIndexIndication);
                                    }
                                });
                            }
                            if (child.label == 'KeyPersonnel' && newData) {
                                console.log('recieved pers: ', newData.length);
                            }
                            working = true;
                            if (err) req.xmlErrors.push(err);
                            else if (newData && newData.length > 0) {
                                newData.forEach(function(elemData) {
                                    var queryExtn = queryExt;
                                    if (funcMap[child.repeatFor] && funcMap[child.repeatFor].queryExtKey) {
                                        //queryExtn = {};
                                        Object.keys(funcMap[child.repeatFor].queryExtKey).forEach(function(qek) {
                                            if (!queryExtn[qek]) queryExtn[qek] = {};
                                            if (funcMap[child.repeatFor].queryExtKey[qek].constructor === Array) {
                                                funcMap[child.repeatFor].queryExtKey[qek].forEach(function(q) {
                                                    queryExtn[qek][q.key] = elemData && q.value && elemData[q.value] ? (elemData[q.value]['$in'] || elemData[q.value].toString()) : (q.value ? q.value['$in'] : q.value);
                                                });
                                            } else {
                                                var q = funcMap[child.repeatFor].queryExtKey[qek];
                                                queryExtn[qek][q.key] = elemData && q.value && elemData[q.value] ? (elemData[q.value]['$in'] || elemData[q.value].toString()) : (q.value ? q.value['$in'] : q.value);
                                            }
                                        });
                                    }
                                    working = true;
                                    createElement(child, false, elemData, queryExtn, function(err, childElem) {
                                        if (err) req.xmlErrors.push(err);
                                        working = false;
                                        if (childElem && Object.keys(childElem).length > 0) xmlElement[(proxy || element.label)][child.label].push(childElem);
                                    });
                                });
                                working = false;
                            } else {
                                delete xmlElement[(proxy || element.label)][child.label];
                            }
                        }
                    } else {
                        createElement(child, false, data, queryExt, function(err, elem) {
                            working = false;
                            if (elem && Object.keys(elem).length > 0) xmlElement[(proxy || element.label)][child.label] = elem;
                        });
                    }
                });
            }
            levelOffset--;
            if (!isRootNode) {
                return callback(null, xmlElement[(proxy || element.label)]);
            }
        }

        function getSectionsForXML(callback, queryExt) {
            var query = {
                'belongsTo.identifier': 'SECTION_CODE',
                'belongsTo.directories': req.query._where.directoryId,
                active: true,
                deleted: false,
            };
            refCodeTypeService.get(query, function(err, codeTypes) {
                if (err) return callback(err);
                if (!codeTypes) return callback({ message: 'Could not fetch RefCodeType' });
                var nextQuery = {
                    active: true,
                    deleted: false,
                };
                if (queryExt['sectionCodes']) {
                    Object.keys(queryExt['sectionCodes']).forEach(function(key, index) {
                        nextQuery[key] = queryExt['sectionCodes'][key];
                    });
                }
                refCodeValueService.get(nextQuery, function(err, sections) {
                    if (err) return callback(err);
                    if (!sections) return callback({ message: 'Could not fetch Sections' });
                    return callback(null, sections);
                });
            });
        }

        function getCountriesForXML(callback, queryExt) {
            fetchingOrganizations = true;
            var Organization = organizationService.orgModel;
            var orgMatch = {
                    "directoryId": organizationService.mongoose.Types.ObjectId(req.query._where.directoryId),
                    "active": true,
                    "deleted": false
                },
                agMatch = {};
            if (queryExt["organizations"] && Object.keys(queryExt["organizations"]).length > 0) {
                Object.keys(queryExt["organizations"]).forEach(function(key) {
                    orgMatch[key] = parseQueryExt(queryExt["organizations"][key]);
                });
            } else if (queryExt["allOrgs"] && Object.keys(queryExt["allOrgs"]).length > 0) {
                Object.keys(queryExt["allOrgs"]).forEach(function(key) {
                    orgMatch[key] = parseQueryExt(queryExt["allOrgs"][key]);
                });
            }
            Object.keys(orgMatch).forEach(function(key) {
                if (key.indexOf('.') >= 0) {
                    var splits = key.split(".");
                    if (splits.length == 2 && organizationService.orgModel.schema && organizationService.orgModel.schema.tree[splits[0]] && organizationService.orgModel.schema.tree[splits[0]].constructor === Array) {
                        if (!orgMatch[splits[0]]) orgMatch[splits[0]] = { $elemMatch: {} };
                        orgMatch[splits[0]]["$elemMatch"][splits[1]] = orgMatch[key];
                        delete orgMatch[key];
                    }
                }
            });
            console.log(orgMatch);
            Organization.aggregate([{
                $match: orgMatch
            }, {
                $unwind: "$address"
            }, {
                $match: {
                    'address.city': { $ne: null }
                }
            }, {
                $group: {
                    _id: false,
                    ids: { $addToSet: '$address.country' }
                }
            }]).exec(function(err, op) {
                console.log(err, op);
                if (err) return callback(err);
                if (op && op[0] && op[0].ids && op[0].ids.length > 0) {
                    if (op[0].ids.indexOf(null) >= 0) op[0].ids.splice(op[0].ids.indexOf(null), 1);
                    var query = {
                        _where: {
                            _id: { $in: op[0].ids },
                            active: true,
                            deleted: false,
                        },
                        _sort: req.xmlCountrySort
                    };
                    refCodeValueService.get(query, function(err, countries) {
                        if (err) return callback(err);
                        if (!countries) return callback({ message: 'Could not fetch Countries' });
                        fetchingOrganizations = false;
                        return callback(null, countries);
                    });
                } else return callback(null, []);
            });
        }

        function getStatesForXML(callback, queryExt) {
            fetchingOrganizations = true;
            var Organization = organizationService.orgModel;
            var orgMatch = {
                    "directoryId": organizationService.mongoose.Types.ObjectId(req.query._where.directoryId),
                    "active": true,
                    "deleted": false
                },
                agMatch = { 'address.city': { $ne: null } };
            if (queryExt["organizations"] && Object.keys(queryExt["organizations"]).length > 0) {
                Object.keys(queryExt["organizations"]).forEach(function(key) {
                    orgMatch[key] = parseQueryExt(queryExt["organizations"][key]);
                });
            } else if (queryExt["allOrgs"] && Object.keys(queryExt["allOrgs"]).length > 0) {
                Object.keys(queryExt["allOrgs"]).forEach(function(key) {
                    orgMatch[key] = parseQueryExt(queryExt["allOrgs"][key]);
                });
            }
            if (queryExt["stateOrgs"] && Object.keys(queryExt["stateOrgs"]).length > 0) {
                Object.keys(queryExt["stateOrgs"]).forEach(function(key) {
                    agMatch[key] = parseQueryExt(queryExt["stateOrgs"][key]);
                });
            }
            if (queryExt["address"] && Object.keys(queryExt["address"]).length > 0) {
                Object.keys(queryExt["address"]).forEach(function(key) {
                    agMatch["address." + key] = parseQueryExt(queryExt["address"][key]);
                });
            }
            Object.keys(orgMatch).forEach(function(key) {
                if (key.indexOf('.') >= 0) {
                    var splits = key.split(".");
                    if (splits.length == 2 && organizationService.orgModel.schema && organizationService.orgModel.schema.tree[splits[0]] && organizationService.orgModel.schema.tree[splits[0]].constructor === Array) {
                        if (!orgMatch[splits[0]]) orgMatch[splits[0]] = { $elemMatch: {} };
                        orgMatch[splits[0]]["$elemMatch"][splits[1]] = orgMatch[key];
                        delete orgMatch[key];
                    }
                }
            });
            console.log(orgMatch, agMatch);
            Organization.aggregate([{
                $match: orgMatch
            }, {
                $unwind: "$address"
            }, {
                $match: agMatch
            }, {
                $group: {
                    _id: '$address.state',
                    count: { $addToSet: "$_id" }
                }
            }]).exec(function(err, opo) {
                console.log(err, op);
                if (err) return callback(err);
                var op;
                if (opo && opo.length > 0) {
                    op = [{
                        ids: [],
                        counts: {}
                    }];
                    opo.forEach(function(o) {
                        if (op[0].ids.indexOf(o._id) < 0) op[0].ids.push(o._id);
                        op[0].counts[o._id] = o.count.length;
                    });
                }
                if (op && op[0] && op[0].ids && op[0].ids.length > 0) {
                    if (op[0].ids.indexOf(null) >= 0) op[0].ids.splice(op[0].ids.indexOf(null), 1);
                    var query = {
                        _id: { $in: op[0].ids },
                        active: true,
                        deleted: false,
                    };
                    refCodeValueService.get(query, function(err, states) {
                        if (err) return callback(err);
                        if (!states) return callback({ message: 'Could not fetch States' });
                        var territories = [],
                            newStates = [];
                        states.forEach(function(state, index) {
                            state.listingCount = op[0].counts[state._id];
                            if (state.isUSTerritory) territories.push(state);
                            else newStates.push(state);
                        });
                        newStates = newStates.concat(territories);
                        callback(null, newStates);
                        fetchingOrganizations = false;
                    });
                } else return callback(null, []);
            });
        }

        function getCitiesForXML(callback, queryExt, countryFlag) {
            fetchingOrganizations = true;
            var Organization = organizationService.orgModel;
            var agStr = (countryFlag ? 'address.country' : 'address.state'),
                agId;
            agId = '$' + agStr;
            var orgMatch = {
                    "directoryId": organizationService.mongoose.Types.ObjectId(req.query._where.directoryId),
                    "active": true,
                    "deleted": false
                },
                agMatch = {};
            agMatch[agStr] = organizationService.mongoose.Types.ObjectId(queryExt["cities"][agStr]);
            if (queryExt["organizations"] && Object.keys(queryExt["organizations"]).length > 0) {
                Object.keys(queryExt["organizations"]).forEach(function(key) {
                    orgMatch[key] = parseQueryExt(queryExt["organizations"][key]);
                });
            } else if (queryExt["allOrgs"] && Object.keys(queryExt["allOrgs"]).length > 0) {
                Object.keys(queryExt["allOrgs"]).forEach(function(key) {
                    orgMatch[key] = parseQueryExt(queryExt["allOrgs"][key]);
                });
            }
            if (queryExt["cityOrgs"] && Object.keys(queryExt["cityOrgs"]).length > 0) {
                Object.keys(queryExt["cityOrgs"]).forEach(function(key) {
                    agMatch[key] = parseQueryExt(queryExt["cityOrgs"][key]);
                });
            }
            if (queryExt["address"] && Object.keys(queryExt["address"]).length > 0) {
                Object.keys(queryExt["address"]).forEach(function(key) {
                    agMatch["address." + key] = parseQueryExt(queryExt["address"][key]);
                });
            }
            Object.keys(orgMatch).forEach(function(key) {
                if (key.indexOf('.') >= 0) {
                    var splits = key.split(".");
                    if (splits.length == 2 && organizationService.orgModel.schema && organizationService.orgModel.schema.tree[splits[0]] && organizationService.orgModel.schema.tree[splits[0]].constructor === Array) {
                        if (!orgMatch[splits[0]]) orgMatch[splits[0]] = { $elemMatch: {} };
                        orgMatch[splits[0]]["$elemMatch"][splits[1]] = orgMatch[key];
                        delete orgMatch[key];
                    }
                }
            });
            console.log(orgMatch);
            Organization.aggregate([{
                $match: orgMatch
            }, {
                $unwind: "$address"
            }, {
                $match: agMatch
            }, {
                $group: {
                    _id: agId,
                    ids: { $addToSet: '$address.city' }
                }
            }]).exec(function(err, op) {
                console.log(err, op);
                if (err) return callback(err);
                if (op && op[0] && op[0].ids && op[0].ids.length > 0) {
                    if (op[0].ids.indexOf(null) >= 0) op[0].ids.splice(op[0].ids.indexOf(null), 1);
                    var query = {
                        _id: { $in: op[0].ids },
                        active: true,
                        deleted: false,
                    };
                    // if (!countryFlag) query.parentId = op[0]._id;
                    refCodeValueService.get(query, function(err, cities) {
                        if (err) return callback(err);
                        if (!cities) return callback({ message: 'Could not fetch Cities' });
                        callback(null, cities);
                        fetchingOrganizations = false;
                    });
                } else return callback(null, []);
            });
        }

        function getCodedSubjectsForXML(callback, queryExt) {
            fetchingOrganizations = true;
            var Organization = organizationService.orgModel,
                orgMatch = {
                    "directoryId": organizationService.mongoose.Types.ObjectId(req.query._where.directoryId),
                    "active": true,
                    "deleted": false
                };
            if (queryExt["organizations"] && Object.keys(queryExt["organizations"]).length > 0) {
                Object.keys(queryExt["organizations"]).forEach(function(key) {
                    orgMatch[key] = parseQueryExt(queryExt["organizations"][key]);
                });
            } else if (queryExt["allOrgs"] && Object.keys(queryExt["allOrgs"]).length > 0) {
                Object.keys(queryExt["allOrgs"]).forEach(function(key) {
                    orgMatch[key] = parseQueryExt(queryExt["allOrgs"][key]);
                });
            }
            Object.keys(orgMatch).forEach(function(key) {
                if (key.indexOf('.') >= 0) {
                    var splits = key.split(".");
                    if (splits.length == 2 && organizationService.orgModel.schema && organizationService.orgModel.schema.tree[splits[0]] && organizationService.orgModel.schema.tree[splits[0]].constructor === Array) {
                        if (!orgMatch[splits[0]]) orgMatch[splits[0]] = { $elemMatch: {} };
                        orgMatch[splits[0]]["$elemMatch"][splits[1]] = orgMatch[key];
                        delete orgMatch[key];
                    }
                }
            });
            console.log(orgMatch);
            refCodeTypeService.get({
                codeType: "CODED SUBJECTS",
                active: true,
                deleted: false
            }, function(err, refCodeTypes) {
                if (err) return callback(err);
                if (!refCodeTypes) return callback({ message: 'Could not fetch Code type' });
                refCodeValueService.get({
                    codeTypeId: refCodeTypes[0]._id,
                    // classif_code: '888',
                    subjectIndexPrintFlag: true,
                    active: true,
                    deleted: false
                }, function(err, refCodeValues) {
                    if (err) return callback(err);
                    if (!refCodeValues) return callback({ message: 'Could not fetch Subjects' });
                    var subjectIds = [];
                    refCodeValues.forEach(function(subject) {
                        if (subjectIds.indexOf(subject.codeValue) < 0)
                            subjectIds.push(subject.codeValue);
                    });
                    console.log("subjectIds", subjectIds, subjectIds.length);
                    Organization.aggregate([{
                        $match: orgMatch
                    }, {
                        $unwind: '$features'
                    }, {
                        $match: {
                            "features.featureType": /CODED SUBJECT/i,
                            'features.codeName': { $in: subjectIds },
                            'features.indexIndication': { $ne: 'Y' }
                        }
                    }, {
                        $group: {
                            _id: '$features.codeName',
                            ids: { $addToSet: '$_id' }
                        }
                    }, {
                        $sort: {
                            _id: 1
                        }
                    }]).exec(function(err, op) {
                        if (err) return callback(err);
                        if (op && op.length > 0) {
                            op.forEach(function(o) {
                                if (o.ids && o.ids.length > 0) o.ids = { $in: o.ids };
                                if (o._id) o.codeValue = o._id;
                                o.sortName = o.codeValue.toLowerCase();
                            });
                            op.sort(function(a, b) {
                                if (a.sortName < b.sortName) return -1;
                                if (a.sortName > b.sortName) return 1;
                                return 0;
                            });
                        }
                        callback(null, (op || []));
                        fetchingOrganizations = false;
                    });
                });
            });
        }

        function getCollectionsForXML(callback, queryExt) {
            fetchingOrganizations = true;
            var Organization = organizationService.orgModel,
                orgMatch = {
                    "directoryId": organizationService.mongoose.Types.ObjectId(req.query._where.directoryId),
                    "active": true,
                    "deleted": false
                };
            if (queryExt["organizations"] && Object.keys(queryExt["organizations"]).length > 0) {
                Object.keys(queryExt["organizations"]).forEach(function(key) {
                    orgMatch[key] = parseQueryExt(queryExt["organizations"][key]);
                });
            } else if (queryExt["allOrgs"] && Object.keys(queryExt["allOrgs"]).length > 0) {
                Object.keys(queryExt["allOrgs"]).forEach(function(key) {
                    orgMatch[key] = parseQueryExt(queryExt["allOrgs"][key]);
                });
            }
            Object.keys(orgMatch).forEach(function(key) {
                if (key.indexOf('.') >= 0) {
                    var splits = key.split(".");
                    if (splits.length == 2 && organizationService.orgModel.schema && organizationService.orgModel.schema.tree[splits[0]] && organizationService.orgModel.schema.tree[splits[0]].constructor === Array) {
                        if (!orgMatch[splits[0]]) orgMatch[splits[0]] = { $elemMatch: {} };
                        orgMatch[splits[0]]["$elemMatch"][splits[1]] = orgMatch[key];
                        delete orgMatch[key];
                    }
                }
            });
            console.log(orgMatch);
            Organization.aggregate([{
                $match: orgMatch
            }, {
                $unwind: '$features'
            }, {
                $match: {
                    "features.featureType": /COLLECTION/i,
                    'features.name': { $ne: null },
                    'features.indexIndication': { $eq: 'Y' }
                }
            }, {
                $group: {
                    _id: '$features.name',
                    ids: { $addToSet: '$_id' }
                }
            }, {
                $sort: {
                    _id: 1
                }
            }]).exec(function(err, op) {
                if (err) return callback(err);
                if (op && op.length > 0) {
                    op.forEach(function(o) {
                        if (o.ids && o.ids.length > 0) o.ids = { $in: o.ids };
                        if (o._id) o.codeValue = o._id;
                        o.sortName = o.codeValue.toLowerCase();
                    });
                    op.sort(function(a, b) {
                        if (a.sortName < b.sortName) return -1;
                        if (a.sortName > b.sortName) return 1;
                        return 0;
                    });
                }
                callback(null, (op || []));
                fetchingOrganizations = false;
            });
        }

        function getPersonnelForXML(callback, queryExt) {
            fetchingOrganizations = true;
            var Organization = organizationService.orgModel,
                orgMatch = {
                    "directoryId": organizationService.mongoose.Types.ObjectId(req.query._where.directoryId),
                    "active": true,
                    "deleted": false
                };
            if (queryExt["organizations"] && Object.keys(queryExt["organizations"]).length > 0) {
                Object.keys(queryExt["organizations"]).forEach(function(key) {
                    orgMatch[key] = parseQueryExt(queryExt["organizations"][key]);
                });
            } else if (queryExt["allOrgs"] && Object.keys(queryExt["allOrgs"]).length > 0) {
                Object.keys(queryExt["allOrgs"]).forEach(function(key) {
                    orgMatch[key] = parseQueryExt(queryExt["allOrgs"][key]);
                });
            }
            Object.keys(orgMatch).forEach(function(key) {
                if (key.indexOf('.') >= 0) {
                    var splits = key.split(".");
                    if (splits.length == 2 && organizationService.orgModel.schema && organizationService.orgModel.schema.tree[splits[0]] && organizationService.orgModel.schema.tree[splits[0]].constructor === Array) {
                        if (!orgMatch[splits[0]]) orgMatch[splits[0]] = { $elemMatch: {} };
                        orgMatch[splits[0]]["$elemMatch"][splits[1]] = orgMatch[key];
                        delete orgMatch[key];
                    }
                }
            });
            console.log(orgMatch);
            Organization.aggregate([{
                $match: orgMatch
            }, {
                $project: {
                    'personnel': 1
                }
            }, {
                $unwind: '$personnel'
            }, {
                $match: {
                    'personnel': { $ne: null }
                }
            }, {
                $group: {
                    _id: null,
                    ids: { $addToSet: '$personnel' }
                }
            }]).exec(function(err, op) {
                if (err) return callback(err);
                if (op && op[0]) {
                    var personnelIds = op[0].ids;
                    var query = {
                        _where: {
                            "_id": { $in: personnelIds },
                            active: true,
                            deleted: false,
                            "name.last": { $nin: [null, ""] },
                            "printFlag": true
                        },
                        _select: {
                            orgId: 1,
                            name: 1,
                            title: 1,
                            titleMasterName: 1,
                            sortName: {
                                "$concat": [
                                    { "$toLower": "$name.last" }, ",",
                                    { "$toLower": "$name.first" }, ",",
                                    { "$toLower": "$titleMasterName" }
                                ]
                            }
                        },
                        _sort: { 'sortName': 1 },
                        // _limit: chunkSize,
                        // _skip: req.xmlPersonnelSkip
                    };
                    var Personnel = organizationService.persModel;
                    Personnel.count(query._where, function(err, persCount) {
                        if (err) return callback(err);
                        if (persCount <= 0) return callback(null, []);
                        // var seriesOps = [],
                        //     repeat = ((req.xmlPersonnelReps * chunkSize) + req.xmlPersonnelSkip) > persCount ? ((parseInt(((persCount - req.xmlPersonnelSkip) / chunkSize))) + 1) : req.xmlPersonnelReps;
                        // // for (var reps = parseInt((persCount / chunkSize)); reps >= 0; reps--) {
                        // for (var reps = repeat; reps > 0; reps--) {
                        //     seriesOps.push(function(cb) {
                        Personnel.aggregate([
                            { $match: query._where },
                            // { $limit: query._limit },
                            // { $skip: query._skip },
                            { $project: query._select },
                            { $sort: query._sort },
                        ]).exec(function(err, personnel) {
                            if (err) req.xmlErrors.push(err);
                            else if (!personnel) req.xmlErrors.push({ message: 'Could not fetch Personnel' });
                            callback(null, personnel);
                            fetchingOrganizations = false;
                        });
                        //         query._skip += chunkSize;
                        //     });
                        // }
                        // console.log('jobs: ', seriesOps.length);
                        // async.series(seriesOps, function(err, personnelGroups) {
                        //     personnelGroups.forEach(function(personnelGroup) {
                        //         callback(null, personnelGroup);
                        //     });
                        //     fetchingOrganizations = false;
                        // });
                    });
                }
            });
        }

        function getListingTypesForXML(callback, queryExt) {
            fetchingOrganizations = true;
            var Organization = organizationService.orgModel;
            var agMatch = {
                    "listingType.listingActive": true
                },
                orgQueryExt = (queryExt["organizations"] || queryExt["allOrgs"]);
            if (queryExt["listingTypes"] && queryExt["listingTypes"]["_id"]) agMatch["listingType.listing"] = organizationService.mongoose.Types.ObjectId(queryExt["listingTypes"]["_id"]);
            Organization.aggregate([{
                $match: {
                    "directoryId": organizationService.mongoose.Types.ObjectId(req.query._where.directoryId),
                    active: true,
                    deleted: false,
                    "status": organizationService.mongoose.Types.ObjectId(orgQueryExt["status"])
                }
            }, {
                $project: {
                    listingType: 1
                }
            }, {
                $unwind: '$listingType'
            }, {
                $match: agMatch
            }, {
                $group: {
                    _id: '$listingType.listing',
                    listingSectionCode: { $addToSet: '$listingType.listingSectionCode' }
                }
            }, {
                $lookup: {
                    "from": "mst_refcodevalues",
                    "localField": "_id",
                    "foreignField": "_id",
                    "as": "listingMaster"
                }
            }, {
                $unwind: '$listingSectionCode'
            }, {
                $lookup: {
                    "from": "mst_refcodevalues",
                    "localField": "listingSectionCode",
                    "foreignField": "_id",
                    "as": "listingSectionCodeMaster"
                }
            }, {
                $unwind: '$listingMaster'
            }, {
                $unwind: '$listingSectionCodeMaster'
            }, {
                $project: {
                    listingMaster: 1,
                    listingSectionCodeMaster: 1
                }
            }, {
                $group: {
                    _id: '$listingMaster',
                    listingSectionCodes: { $addToSet: '$listingSectionCodeMaster' }
                }
            }]).exec(function(err, op) {
                if (err) return callback(err);
                if (op && op.length > 0) {
                    op.forEach(function(o, index) {
                        var oo = o._id;
                        oo.listingSectionCodes = o.listingSectionCodes;
                        if (oo.listingSectionCodes && oo.listingSectionCodes.length > 0) {
                            oo.listingSectionCodes.sort(function(f1, f2) {
                                var n1 = (f1.sequenceNo && !isNaN(parseInt(f1.sequenceNo)) ? parseInt(f1.sequenceNo) : 0),
                                    n2 = (f2.sequenceNo && !isNaN(parseInt(f2.sequenceNo)) ? parseInt(f2.sequenceNo) : 0);
                                return n1 - n2;
                            });
                        }
                        op[index] = oo;
                    });
                    op.sort(function(f1, f2) {
                        var n1 = (f1.partNbr && !isNaN(parseInt(f1.partNbr)) ? parseInt(f1.partNbr) : 0),
                            n2 = (f2.partNbr && !isNaN(parseInt(f2.partNbr)) ? parseInt(f2.partNbr) : 0);
                        return n1 - n2;
                    });
                }
                callback(null, (op || []));
                fetchingOrganizations = false;
            });
        }

        function getOnlyListingTypesForXML(callback, queryExt) {
            fetchingOrganizations = true;
            var Organization = organizationService.orgModel;
            var agMatch = {
                    "listingType.listingActive": true
                },
                orgMatch = {
                    "directoryId": organizationService.mongoose.Types.ObjectId(req.query._where.directoryId),
                    active: true,
                    deleted: false
                };
            if (queryExt["onlyListingTypes"]["_id"]) agMatch["listingType.listing"] = organizationService.mongoose.Types.ObjectId(queryExt["onlyListingTypes"]["_id"]);
            if (queryExt["organizations"]["status"]) orgMatch["status"] = organizationService.mongoose.Types.ObjectId(queryExt["organizations"]["status"]);
            if (queryExt["organizations"]["workflowStatus"]) orgMatch["workflowStatus"] = organizationService.mongoose.Types.ObjectId(queryExt["organizations"]["workflowStatus"])
            Organization.aggregate([{
                $match: orgMatch
            }, {
                $project: {
                    listingType: 1
                }
            }, {
                $unwind: '$listingType'
            }, {
                $match: agMatch
            }, {
                $group: {
                    _id: '$listingType.listing',
                    orgIds: { $addToSet: '$_id' }
                }
            }, {
                $lookup: {
                    "from": "mst_refcodevalues",
                    "localField": "_id",
                    "foreignField": "_id",
                    "as": "listingType"
                }
            }, {
                $unwind: '$listingType'
            }, {
                $project: {
                    listingType: 1
                }
            }]).exec(function(err, op) {
                if (err) return callback(err);
                if (op && op.length > 0) {
                    op.forEach(function(o, index) {
                        op[index] = o.listingType;
                    });
                    op.sort(function(f1, f2) {
                        var n1 = (f1.rank && !isNaN(parseInt(f1.rank)) ? parseInt(f1.rank) : 0),
                            n2 = (f2.rank && !isNaN(parseInt(f2.rank)) ? parseInt(f2.rank) : 0);
                        return n1 - n2;
                    });
                }
                callback(null, (op || []));
                fetchingOrganizations = false;
            });
        }

        function getOrgCategoriesForXML(callback, queryExt) {
            fetchingOrganizations = true;
            var Organization = organizationService.orgModel;
            var orgMatch = {
                    "directoryId": organizationService.mongoose.Types.ObjectId(req.query._where.directoryId),
                    "active": true,
                    "deleted": false
                },
                agMatch = { "features.featureType": "CATEGORY" };
            if (queryExt["organizations"] && Object.keys(queryExt["organizations"]).length > 0) {
                Object.keys(queryExt["organizations"]).forEach(function(key) {
                    orgMatch[key] = parseQueryExt(queryExt["organizations"][key]);
                });
            } else if (queryExt["allOrgs"] && Object.keys(queryExt["allOrgs"]).length > 0) {
                Object.keys(queryExt["allOrgs"]).forEach(function(key) {
                    orgMatch[key] = parseQueryExt(queryExt["allOrgs"][key]);
                });
            }
            if (queryExt["categories"] && Object.keys(queryExt["categories"]).length > 0) {
                Object.keys(queryExt["categories"]).forEach(function(key) {
                    agMatch[key] = parseQueryExt(queryExt["categories"][key]);
                });
            }
            Organization.aggregate([{
                $match: orgMatch
            }, {
                $unwind: "$features"
            }, {
                $match: agMatch
            }, {
                $group: {
                    _id: false,
                    ids: { $addToSet: "$features.code" }
                }
            }]).exec(function(err, op) {
                if (err) return callback(err);
                if (op && op[0] && op[0].ids && op[0].ids.length > 0) {
                    if (op[0].ids.indexOf(null) >= 0) op[0].ids.splice(op[0].ids.indexOf(null), 1);
                    var query = {
                        _id: { $in: op[0].ids },
                        active: true,
                        deleted: false,
                    };
                    refCodeValueService.get(query, function(err, categories) {
                        if (err) return callback(err);
                        if (!categories) return callback({ message: 'Could not fetch Categories' });
                        var newCategories = [];
                        categories.forEach(function(category, index) {
                            if (category.parentId) {
                                if (newCategories.indexOf(category.parentId) < 0) newCategories.push(category.parentId);
                                if (!newCategories[(newCategories.length - 1)].subCategories) newCategories[(newCategories.length - 1)].subCategories = [];
                                newCategories[(newCategories.length - 1)].subCategories.push(category);
                                if (!newCategories[(newCategories.length - 1)].subCategoryListings) newCategories[(newCategories.length - 1)].subCategoryListings = [];
                                newCategories[(newCategories.length - 1)].subCategoryListings.push(category);
                            } else {
                                if (newCategories.indexOf(category) < 0) newCategories.push(category);
                            }
                        });
                        newCategories.forEach(function(category, index) {
                            if (category.subCategories.length == 0) category.subCategories = [{ _id: category._id, codeValue: "" }];
                            category.subCategories.sort(function(a, b) {
                                if (a.codeValue < b.codeValue) return -1;
                                if (a.codeValue > b.codeValue) return 1;
                                return 0;
                            });
                            category.subCategoryListings.sort(function(a, b) {
                                if (a.codeValue < b.codeValue) return -1;
                                if (a.codeValue > b.codeValue) return 1;
                                return 0;
                            });
                        });
                        newCategories.sort(function(a, b) {
                            if (a.codeValue < b.codeValue) return -1;
                            if (a.codeValue > b.codeValue) return 1;
                            return 0;
                        });
                        callback(null, newCategories);
                        fetchingOrganizations = false;
                    });
                } else return callback(null, []);
            });

        }

        function getOrgsForXML(callback, queryExt) {
            var query = coreQueryBuilderService.buildQuery(req);
            fetchingOrganizations = true;
            var qe = queryExt['allOrgs'];
            if (qe) {
                Object.keys(qe).forEach(function(key, index) {
                    query._where[key] = parseQueryExt(qe[key] && qe[key].constructor === Array ? { $in: qe[key] } : qe[key]);
                });
                Object.keys(query._where).forEach(function(key) {
                    if (key.indexOf('.') >= 0) {
                        var splits = key.split(".");
                        if (splits.length == 2 && organizationService.orgModel.schema && organizationService.orgModel.schema.tree[splits[0]] && organizationService.orgModel.schema.tree[splits[0]].constructor === Array) {
                            if (!query._where[splits[0]]) query._where[splits[0]] = { $elemMatch: {} };
                            query._where[splits[0]]["$elemMatch"][splits[1]] = query._where[key];
                            delete query._where[key];
                        }
                    }
                });
            }
            query._sort = req.xmlOrgSortBy;
            query._select = { name: 1, org_id: 1, shortName: 1, address: 1, sectionCode: 1, classificationCode: 1, listingType: 1, parentId: 1, bookEdition: 1 };
            var Organization = organizationService.orgModel;
            Organization.find(query._where)
                .populate({ path: 'address.addressType', match: { active: true, deleted: false } })
                .populate({ path: 'address.state', match: { active: true, deleted: false } })
                .populate({ path: 'classificationCode', match: { active: true, deleted: false } })
                .populate({ path: 'sectionCode', match: { active: true, deleted: false } })
                .select(query._select)
                .sort(query._sort)
                .exec(function(err, organizations) {
                    if (err) return callback(err);
                    if (!organizations) return callback({ message: 'Could not fetch Organizations' });
                    callback(null, sanitizeOrgData(discardDeleted(organizations), !req.xmlOrgIndexFlag));
                    fetchingOrganizations = false;
                });
        }

        function getAlphabeticalOrgsForXML(callback, queryExt) {
            var query = coreQueryBuilderService.buildQuery(req);
            fetchingOrganizations = true;
            var qe = queryExt['alphabeticalOrgs'];
            if (qe) {
                Object.keys(qe).forEach(function(key, index) {
                    query._where[key] = parseQueryExt(qe[key] && qe[key].constructor === Array ? { $in: qe[key] } : qe[key]);
                });
                Object.keys(query._where).forEach(function(key) {
                    if (key.indexOf('.') >= 0) {
                        var splits = key.split(".");
                        if (splits.length == 2 && organizationService.orgModel.schema && organizationService.orgModel.schema.tree[splits[0]] && organizationService.orgModel.schema.tree[splits[0]].constructor === Array) {
                            if (!query._where[splits[0]]) query._where[splits[0]] = { $elemMatch: {} };
                            query._where[splits[0]]["$elemMatch"][splits[1]] = query._where[key];
                            delete query._where[key];
                        }
                    }
                });
            }
            query._sort = req.xmlOrgSortBy;
            query._select = { org_id: 1, name: 1, address: 1, sortMajorName: 1, bookEdition: 1 };
            if (req.xmlOrgLimit) query._limit = req.xmlOrgLimit;
            if (req.xmlOrgSkip) query._skip = req.xmlOrgSkip;
            console.log("Org query: ", query);
            var Organization = organizationService.orgModel;
            Organization.find(query._where)
                .populate({ path: 'address.addressType', match: { active: true, deleted: false } })
                .populate({ path: 'address.state', match: { active: true, deleted: false } })
                .select(query._select)
                .sort(query._sort)
                .exec(function(err, organizations) {
                    if (err) return callback(err);
                    if (!organizations) return callback({ message: 'Could not fetch Organizations' });
                    var orgs = sanitizeOrgData(discardDeleted(organizations), !req.xmlOrgIndexFlag),
                        alphaOrgs = [],
                        alphaOrgsMap = {};
                    orgs.forEach(function(org) {
                        var alphabet = org.sortMajorName[0].toUpperCase();
                        if (!alphaOrgsMap[alphabet]) alphaOrgsMap[alphabet] = [];
                        alphaOrgsMap[alphabet].push(org);
                    });
                    Object.keys(alphaOrgsMap).forEach(function(alphabet) {
                        alphaOrgs.push({
                            alphabet: alphabet,
                            orgs: alphaOrgsMap[alphabet]
                        });
                        console.log({
                            alphabet: alphabet,
                            orgs: alphaOrgsMap[alphabet].length
                        });
                    });
                    callback(null, alphaOrgs);
                    fetchingOrganizations = false;
                });
        }

        function getOrganizationForXML(callback, queryExt) {
            var query = coreQueryBuilderService.buildQuery(req);
            fetchingOrganizations = true;
            var qe = queryExt['organizations'],
                addrQe = queryExt['address'];
            // if (qe && qe.parentId) delete qe.parentId;
            if (qe) {
                Object.keys(qe).forEach(function(key, index) {
                    query._where[key] = parseQueryExt(qe[key] && qe[key].constructor === Array ? { $in: qe[key] } : qe[key]);
                });
                Object.keys(addrQe).forEach(function(key, index) {
                    query._where["address." + key] = parseQueryExt(addrQe[key]);
                });
                Object.keys(query._where).forEach(function(key) {
                    if (key.indexOf('.') >= 0) {
                        var splits = key.split(".");
                        if (splits.length == 2 && organizationService.orgModel.schema && organizationService.orgModel.schema.tree[splits[0]] && organizationService.orgModel.schema.tree[splits[0]].constructor === Array) {
                            if (!query._where[splits[0]]) query._where[splits[0]] = { $elemMatch: {} };
                            query._where[splits[0]]["$elemMatch"][splits[1]] = query._where[key];
                            delete query._where[key];
                        }
                    }
                });
            }
            // query._where.parentId = null;
            if (query._where._id) delete query._where._id;
            // query._sort = { name: 1, _id: 1 };
            query._sort = req.xmlOrgSortBy;
            if (req.xmlOrgIndexFlag) {
                query._select = { parentId: 1, name: 1, org_id: 1, shortName: 1, address: 1, sectionCode: 1, classificationCode: 1, minorNameIndexIndication: 1, listingType: 1, bookEdition: 1 };
            }
            if (req.xmlOrgLimit) query._limit = req.xmlOrgLimit;
            if (req.xmlOrgSkip) query._skip = req.xmlOrgSkip;
            console.log("Org query: ", query);
            organizationService.get(query, function(err, organizations) {
                if (err) return callback(err);
                if (!organizations) return callback({ message: 'Could not fetch Organizations' });
                var orgMap = {},
                    parallelOps = [];
                organizations = sanitizeOrgData(discardDeleted(organizations), !req.xmlOrgIndexFlag);
                organizations.forEach(function(org, index) {
                    orgMap[org._id.toString()] = index;
                    organizations[index].children = [];
                });
                if (organizations && organizations.length > 0) {
                    for (var reps = parseInt((organizations.length / chunkSize)), start = 0; reps >= 0; reps--) {
                        parallelOps.push(function(cb) {
                            var orgIds = [];
                            organizations.slice(start, (start += chunkSize)).forEach(function(org) {
                                orgIds.push(org._id);
                            });
                            var childQuery = {
                                _where: {
                                    parentId: { '$in': orgIds },
                                    active: true,
                                    deleted: false
                                },
                                _sort: { sub_nbr: 1 }
                            };
                            if (qe["status"]) childQuery._where.status = qe["status"];
                            if (qe["bookEdition"]) childQuery._where.bookEdition = qe["bookEdition"];
                            if (qe["workflowStatus"]) childQuery._where.workflowStatus = qe["workflowStatus"];
                            if (req.xmlOrgIndexFlag) childQuery._select = { parentId: 1, shortName: 1, sectionCode: 1, classificationCode: 1, address: 1 };
                            fetchingOrganizations = true;
                            organizationService.get(childQuery, function(err, children) {
                                if (err) req.xmlErrors.push(err);
                                else if (!children) req.xmlErrors.push({ message: 'Could not fetch child Organizations' });
                                else {
                                    var childOrgsSanitized = sanitizeOrgData(discardDeleted(children), req.xmlOrgIndexFlag);
                                    childOrgsSanitized.forEach(function(childOrg) {
                                        if (childOrg.parentId) {
                                            delete childOrg.name;
                                            var parentIndex = orgMap[childOrg.parentId._id.toString()];
                                            organizations[parentIndex].children.push(childOrg);
                                        }
                                    });
                                }
                                return cb();
                            });
                        });
                    }
                    console.log('jobs: ', parallelOps.length);
                    async.parallel(parallelOps, function() {
                        fetchingOrganizations = false;
                        callback(null, sanitizeOrgData(discardDeleted(organizations), !req.xmlOrgIndexFlag));
                    });
                } else callback(null, []);
            });
        }

        function sanitizeOrgData(organizations, allowMNII) {
            if (organizations && organizations.length > 0) {
                organizations.forEach(function(organization, index) {
                    var phyAddrPresent = false,
                        phyAddrSameCity = false;
                    if (!allowMNII && req.xmlOrgIndexFlag && organization.parentId && !organization.minorNameIndexIndication) {
                        // organizations.remove(organization);
                        organizations.splice(index, 1);
                    } else {
                        if (req.xmlOrgIndexFlag && !organization.parentId && organization.shortName) {
                            organization.parentMinorName = organization.shortName;
                            delete organization.shortName;
                        }
                        if (req.xmlOrgDataMaps) {
                            Object.keys(req.xmlOrgDataMaps).forEach(function(key) {
                                if (key.indexOf(".") >= 0) {
                                    var splits = key.split("."),
                                        root = (splits.splice(0, 1))[0];
                                    organization[root] = changeNestedValue(organization[root], splits);

                                    function changeNestedValue(data, splits) {
                                        if (data) {
                                            var split = (splits.splice(0, 1))[0];
                                            if (splits.length > 0) {
                                                data[split] = changeNestedValue(data[split], splits);
                                            } else if (data[split]) {
                                                switch (typeof req.xmlOrgDataMaps[key]) {
                                                    case 'function':
                                                        {
                                                            data[split] = req.xmlOrgDataMaps[key](data[split]);
                                                            break;
                                                        }
                                                    default:
                                                        {
                                                            if (typeof req.xmlOrgDataMaps[key][data[split]] != 'undefined') data[split] = req.xmlOrgDataMaps[key][data[split]];
                                                            break;
                                                        }
                                                }
                                            }
                                        }
                                        return data;
                                    }
                                } else if (organization[key]) {
                                    switch (typeof req.xmlOrgDataMaps[key]) {
                                        case 'function':
                                            {
                                                organization[key] = req.xmlOrgDataMaps[key](organization[key]);
                                                break;
                                            }
                                        default:
                                            {
                                                if (typeof req.xmlOrgDataMaps[key][organization[key]] != 'undefined') organization[key] = req.xmlOrgDataMaps[key][organization[key]];
                                                break;
                                            }
                                    }
                                }
                            });
                        }

                        if (organization.classificationCode)
                            organization.classificationCode.abbr = req.abbrCC(organization.classificationCode.codeValue);

                        if (organization.artSchool && organization.artSchool[0])
                            organization.artSchool = organization.artSchool[0];

                        if (organization.personnel && organization.personnel.length > 0) {
                            if (req.xmlQueryExt['personnel'] && Object.keys(req.xmlQueryExt['personnel']).length > 0) {
                                organization.personnel.forEach(function(personnel, index) {
                                    Object.keys(req.xmlQueryExt['personnel']).forEach(function(qe) {
                                        if (personnel[qe] != req.xmlQueryExt['personnel'][qe]) delete organization.personnel[index];
                                    });
                                });
                                organization.personnel.clean(undefined);
                            }
                        }

                        if (organization.research && organization.research.length > 0) {
                            if (req.xmlQueryExt['research'] && Object.keys(req.xmlQueryExt['research']).length > 0) {
                                organization.research.forEach(function(research, index) {
                                    Object.keys(req.xmlQueryExt['research']).forEach(function(qe) {
                                        if (research[qe] != req.xmlQueryExt['research'][qe]) delete organization.research[index];
                                    });
                                });
                                organization.research.clean(undefined);
                            }
                        }

                        if (organization.listingType && organization.listingType.length > 0) {
                            if (req.xmlQueryExt['listingType'] && Object.keys(req.xmlQueryExt['listingType']).length > 0) {
                                organization.listingType.forEach(function(listingType, index) {
                                    Object.keys(req.xmlQueryExt['listingType']).forEach(function(qe) {
                                        if (listingType[qe] != req.xmlQueryExt['listingType'][qe]) delete organization.listingType[index];
                                    });
                                });
                                organization.listingType.clean(undefined);
                            }
                        }

                        if (organization.directMarketingBudgetDisbursal && organization.directMarketingBudgetDisbursal.length > 0) {
                            organization.directMarketingBudgetDisbursal.sort(function(a, b) {
                                if (a.codeName < b.codeName) return -1;
                                if (a.codeName > b.codeName) return 1;
                                return 0;
                            });
                        }

                        if (organization.features && organization.features.length > 0) {
                            var featureGroups = {};
                            organization.features.forEach(function(feature, index) {
                                if (!featureGroups[feature.featureType]) featureGroups[feature.featureType] = [];
                                if (!(feature.indexIndication && feature.indexIndication == 'Y'))
                                    featureGroups[feature.featureType].push(feature);
                            });

                            if (featureGroups['LIBRARY_HOLDING']) {
                                featureGroups['LIBRARY_HOLDING'].sort(function(a, b) {
                                    if (a.codeName < b.codeName) return -1;
                                    if (a.codeName > b.codeName) return 1;
                                    return 0;
                                });
                            }

                            if (featureGroups['COURSES OFFERED']) {
                                featureGroups['COURSES OFFERED'].forEach(function(course, index) {
                                    featureGroups['COURSES OFFERED'][index].codeName = featureGroups['COURSES OFFERED'][index].codeName == '' ? null : featureGroups['COURSES OFFERED'][index].codeName;
                                    featureGroups['COURSES OFFERED'][index].name = featureGroups['COURSES OFFERED'][index].name == '' ? null : featureGroups['COURSES OFFERED'][index].name;
                                    featureGroups['COURSES OFFERED'][index].sortName = (featureGroups['COURSES OFFERED'][index].codeName || featureGroups['COURSES OFFERED'][index].name || "").toUpperCase();
                                });
                                featureGroups['COURSES OFFERED'].sort(function(a, b) {
                                    if (a.sortName < b.sortName) return -1;
                                    if (a.sortName > b.sortName) return 1;
                                    return 0;
                                });
                            }

                            Object.keys(featureGroups).forEach(function(featureType, index) {
                                if (featureGroups[featureType] && featureGroups[featureType].length > 0) {
                                    organization['FG' + featureType] = [{
                                        featureType: featureType,
                                        features: featureGroups[featureType]
                                    }];
                                }
                            });
                        }

                        //for chapter specification
                        if (organization.chapterSpecification && organization.chapterSpecification.length > 0) {
                            var chapterspecificationGroups = {};
                            organization.chapterSpecification.forEach(function(chapterSpecification, index) {
                                if (!chapterspecificationGroups[chapterSpecification.specificationType]) chapterspecificationGroups[chapterSpecification.specificationType] = [];
                                chapterspecificationGroups[chapterSpecification.specificationType].push(chapterSpecification);
                            });

                            Object.keys(chapterspecificationGroups).forEach(function(specificationType, index) {
                                if (chapterspecificationGroups[specificationType] && chapterspecificationGroups[specificationType].length > 0) {
                                    if (!organization['CS' + specificationType]) organization['CS' + specificationType] = [{
                                        codeType: specificationType,
                                        records: chapterspecificationGroups[specificationType]
                                    }];
                                }
                            });
                        }
                        //For Key personnel CFS
                        if (organization.personnel && organization.personnel.length > 0) {
                            var personnelGroups = {};
                            organization.personnel.forEach(function(personnel, index) {
                                personnel.officers.forEach(function(officers, index) {
                                    if (!personnelGroups[officers.OfficerTypeName]) personnelGroups[officers.OfficerTypeName] = [];
                                    personnelGroups[officers.OfficerTypeName].push(personnel);
                                })
                            });

                            Object.keys(personnelGroups).forEach(function(OfficerTypeName, index) {
                                if (personnelGroups[OfficerTypeName] && personnelGroups[OfficerTypeName].length > 0) {
                                    if (!organization['PS' + OfficerTypeName]) organization['PS' + OfficerTypeName] = [];
                                    organization['PS' + OfficerTypeName][0] = {
                                        codeType: OfficerTypeName,
                                        records: personnelGroups[OfficerTypeName]

                                    };
                                } else if (organization['PS' + OfficerTypeName]) delete organization['PS' + OfficerTypeName];
                            });
                        }

                        // Sort Address by "Physical Address"
                        if (organization.address && organization.address.length > 0) {
                            if (req.xmlAddrPrefOnly) {
                                if (req.xmlQueryExt['address'] && Object.keys(req.xmlQueryExt['address']).length > 0) {
                                    organization.address.forEach(function(address, index) {
                                        Object.keys(req.xmlQueryExt['address']).forEach(function(qe) {
                                            if (address[qe] != req.xmlQueryExt['address'][qe]) delete organization.address[index];
                                        });
                                    });
                                    organization.address.clean(undefined);
                                }
                            } else {
                                var sortAddress = [];
                                organization.address.forEach(function(address, index) {
                                    if (address.addressType && address.addressType.description == req.xmlAddrPref1) {
                                        sortAddress.splice(0, 0, address);
                                    } else {
                                        sortAddress.push(address);
                                    }
                                });

                                organization.address = sortAddress;
                            }
                        }

                        if (organization.parentId == null) {
                            if (organization.sectionCode && organization.sectionCode[0]) {
                                organization.artSchoolFlag = (req.xmlArtSchoolIds.indexOf(organization.sectionCode[0]._id.toString()) < 0 ? "" : "(S)");
                                organization.sectionFlag = organization.classificationCode ? organization.classificationCode.abbr : null;
                            }
                            if (organization.sectionFlag && organization.sectionFlag.constructor !== Array) organization.sectionFlag = organization.sectionFlag ? [organization.sectionFlag] : [];
                            if (organization && organization.children && organization.children.length > 0) {
                                organization.children.forEach(function(child, index) {
                                    if (child.sectionCode && child.sectionCode[0]) {
                                        child.sectionFlag = child.classificationCode ? child.classificationCode.abbr : null;
                                    }
                                    if (organization.sectionFlag && child.sectionFlag && organization.sectionFlag.indexOf(child.sectionFlag) < 0) organization.sectionFlag.push(child.sectionFlag);
                                });
                            }
                            if (organization.sectionFlag && organization.sectionFlag.length > 0) organization.sectionFlag = "(" + organization.sectionFlag.join(', ') + ")";

                            // if (req.xmlAddrPrefOnly && phyAddrPresent && !phyAddrSameCity) organizations.splice(index, 1)
                        } else delete organization.sectionFlag;

                    }

                });
            }
            return organizations;
        }


    },
    exportToXMLComplete: function(req, res) {
        return res.send({ data: req.xmlFilename, errors: req.xmlErrors });
    },
    exportToExcelCount: function(req, res, next) {
        console.log('in count');
        var query = coreQueryBuilderService.buildQuery(req);
        if (query && query.limit) return next();
        else {
            organizationService.exportToExcelCount({
                directoryId: query.where.directoryId,
                active: true,
                deleted: false
            }, function(err, count) {
                if (err) return res.status(400).send(err);
                req.query.limit = count;
                return next();
            });
        }
    },
    exportToExcel: function(req, res, next) {
        var exportErrors = [],
            query = coreQueryBuilderService.buildQuery(req),
            offset = 50,
            skip = parseInt(query.skip || 0),
            limit = parseInt(query.limit) + skip,
            limitRounds = parseInt(query.limit / offset),
            parallelOps = [],
            nextLimit, nextSkip, abort = false,
            filename = 'export-as-csv-' + (new Date()).getTime() + '.csv',
            excelFields = [{
                label: 'Sr. No.',
                value: 'srno'
            }],
            selectedFields = query.select;
        console.log(limit);
        for (var i = 0; i < selectedFields.length; i++) {
            excelFields.push({
                label: selectedFields[i].label,
                value: selectedFields[i].name
            });
        }
        var srno = 1;
        for (var limitCounter = 0; limitCounter <= limitRounds; limitCounter++) {
            parallelOps.push(function(mainCallback) {
                var builtFields = buildSelectForExport(query.select);
                var newQuery = {
                    select: builtFields,
                    where: {
                        active: true,
                        deleted: false
                    },
                    sort: { orgIdNumber: 1, _id: 1 }
                };
                for (var i in query.where) newQuery.where[i] = query.where[i];
                newQuery.skip = nextSkip !== undefined ? ((nextSkip + offset) > limit ? (limit - nextSkip) : (nextSkip + offset)) : skip;
                newQuery.limit = nextLimit !== undefined ? ((newQuery.skip + offset) > limit ? (limit - newQuery.skip) : offset) : ((query.limit - offset) < 0 ? query.limit : offset);
                console.log(newQuery.skip, newQuery.limit);
                if (!abort && newQuery.limit > 0) {
                    organizationService.exportToExcel(newQuery, function(err, organizations) {
                        if (err) exportErrors.push(err);
                        if (organizations && organizations.length > 0) {
                            var newData = [];
                            var firstIteration = (srno == 1);
                            organizations = discardDeleted(organizations);
                            for (var i = 0; i < organizations.length; i++, srno++) {
                                var orgData = organizations[i];
                                var maxRowsNeeded = getMaxRowsNeeded(orgData, selectedFields);
                                RECORDINDEX = COLUMNINDEX = 0;
                                for (var j = 0; j < maxRowsNeeded; j++) {
                                    COLUMNINDEX++;
                                    var row = getRow(orgData, selectedFields, j);
                                    if (j == 0) row.srno = srno;
                                    newData.push(row);
                                }
                            }
                            console.log('preparing csv data ', newData.length);
                            json2csv({ data: newData, fields: excelFields, hasCSVColumnTitle: firstIteration }, function(err, csv) {
                                if (err) exportErrors.push(err);
                                if (!firstIteration) csv = "\n" + csv;
                                fs.appendFile(process.env.EXPORT_AS_CSV_PATH + '/' + filename, csv, function(err) {
                                    if (err) exportErrors.push(err);
                                    return mainCallback(null);
                                });
                            });
                        } else {
                            console.log('error cond', err, (organizations ? organizations.length : ''));
                            mainCallback(null);
                        }
                    });
                } else {
                    console.log('finished... aborted: ', abort);
                    mainCallback(null);
                }
                nextLimit = newQuery.limit;
                nextSkip = newQuery.skip;
                abort = ((nextSkip + offset) > limit);
            });
        }
        async.series(parallelOps, function(err, results) {
            console.log('out callback');
            // fileLogService.log(process.env.EXPORT_AS_CSV_PATH + '/' + filename, )
            req.resData = { csvFile: filename, errors: exportErrors };
            return next();
        });
    },
    exportToExcelComplete: function(req, res) {
        res.send(req.resData);
    },
    // list
    list: function(req, res) {
        var query = coreQueryBuilderService.buildQuery(req);
        organizationService.get(query, function(err, organizations) {
            if (err) return res.status(400).send(err);
                return res.send(organizations);
        });
    },
    getOrganizationList: function(req, res) {
        coreQueryBuilderService.buildQuery(req);
        var a = req.body.conditions,
            dt = req.body.dataTableQuery;
        req.body = a;
        if (a.mname) {
            var dquery = {};
            dquery.name = a.mname;
            dquery.directoryId = a.directoryId;
            organizationService.orgNameMatch(dquery, function(err, personnel) {
                if (err) {
                    res.status(400).send(err);
                } else {
                    if (personnel.length != 0) {
                        var query = {};
                        query['_id'] = { $in: personnel[0].ids };
                        organizationService.dataTable({
                            dataTableQuery: dt,
                            conditions: query
                        }, function(err, organizations) {
                            if (err) return res.status(400).send(err);
                            return res.send(organizations);
                        });
                    } else {
                        res.send({ "draw": 1, "recordsTotal": 0, "recordsFiltered": 0, "data": [] });
                    }
                }
            });
        } else {
            var query = coreQueryBuilderService.buildQuery(req);
            organizationService.dataTable({
                dataTableQuery: dt,
                conditions: query
            }, function(err, organizations) {
                if (err) return res.status(400).send(err);
                return res.send(organizations);
            });
        }

    },
    getChapterSpecificationListForSearch: function(req, res) {

        var a = req.body.conditions,
            dt = req.body.dataTableQuery;

        if (a.obj.personnel == true) {
            delete a.obj.personnel;

            req.body = a.obj6;
            var query = coreQueryBuilderService.buildQuery(req);


            organizationService.getChapterSpecificationOnAggregate(query, function(err, chapterSpecification) {
                req.body = a.obj1;
                var query = coreQueryBuilderService.buildQuery(req);
                if (err) {
                    query = { _id: null };
                } else {
                    query['orgId'] = { $in: (chapterSpecification.length > 0) ? chapterSpecification[0].ids : [] };
                }
                organizationService.getOrgIdFromPersonnel(query,
                    function(err, personnel) {
                        req.body = a.obj;
                        var query = coreQueryBuilderService.buildQuery(req);
                        if (err) {
                            query = { _id: null };
                        } else {
                            query['_id'] = { $in: (personnel.length > 0) ? personnel[0].ids : [] };

                        }
                        organizationService.dataTable({
                            dataTableQuery: dt,
                            conditions: query
                        }, function(err, organizations) {
                            if (err) return res.status(400).send(err);
                            return res.send(organizations);
                        });
                    });
            });
        } else {

            delete a.obj.personnel;

            req.body = a.obj6;
            var query = coreQueryBuilderService.buildQuery(req);
            organizationService.getChapterSpecificationOnAggregate(query, function(err, chapterSpecification) {
                req.body = a.obj;
                var query = coreQueryBuilderService.buildQuery(req);
                if (err) {
                    query = { _id: null };
                } else {
                    query['_id'] = { $in: (chapterSpecification.length > 0) ? chapterSpecification[0].ids : [] };
                }

                organizationService.dataTable({
                    dataTableQuery: dt,
                    conditions: query
                }, function(err, organizations) {
                    if (err) return res.status(400).send(err);
                    return res.send(organizations);
                });
            });
        }

    },
    getExhibitionList: function(req, res) {
        var a = req.body.conditions;
        if (a.eFlag) {
            var qr = {};
            qr.org = a.org;
            qr.exhi = a.exhi;
            var query = coreQueryBuilderService.buildQuery(req);
            delete query.conditions.eFlag;
            organizationService.getOrgtoExhi(qr, function(err, exhibition) {
                if (exhibition.length == 0) {
                    organizationService.getOrgAggregate(qr, function(err, exhibition) {
                        if (exhibition.length == 0) {
                            res.send({ "draw": 1, "recordsTotal": 0, "recordsFiltered": 0, "data": [] });
                        } else {

                            var query = {};
                            query['_id'] = { $in: exhibition[0].ids };
                            organizationService.dataTable({
                                dataTableQuery: req.body.dataTableQuery,
                                conditions: query
                            }, function(err, organizations) {
                                return res.send(organizations);
                            });
                        }

                    });

                } else {
                    var query = {};
                    query['_id'] = { $in: exhibition[0].ids };
                    organizationService.dataTable({
                        dataTableQuery: req.body.dataTableQuery,
                        conditions: query
                    }, function(err, organizations) {
                        return res.send(organizations);
                    });
                }

            });
            return 0;
        }
        var a = req.body.conditions,
            dt = req.body.dataTableQuery;
        if (a.data == 'exhibitNameAAD') {
            var query = coreQueryBuilderService.buildQuery(req);
            organizationService.getExhibitionListData(query.conditions.query, function(err, exhibition) {
                var query = {};
                query['_id'] = { $in: exhibition[0].ids };
                organizationService.dataTable({
                    dataTableQuery: req.body.dataTableQuery,
                    conditions: query
                }, function(err, organizations) {
                    return res.send(organizations);
                });
            });
        }
        if (a.obj)
            if (a.obj.personnel == true) {
                delete a.obj.personnel;

                req.body = a.obj2;
                var query = coreQueryBuilderService.buildQuery(req);
                organizationService.getExhibitionList(query, function(err, exhibition) {

                    req.body = a.obj1;
                    var query = coreQueryBuilderService.buildQuery(req);
                    if (err) {
                        query = { _id: null };
                    } else {
                        query['orgId'] = { $in: (exhibition.length > 0) ? exhibition[0].ids : [] };

                    }
                    organizationService.getOrgIdFromPersonnel(query,
                        function(err, personnel) {

                            req.body = a.obj;
                            var query = coreQueryBuilderService.buildQuery(req);
                            if (err) {
                                query = { _id: null };
                            } else {
                                query['_id'] = { $in: (personnel.length > 0) ? personnel[0].ids : [] };
                            }
                            organizationService.dataTable({
                                dataTableQuery: dt,
                                conditions: query
                            }, function(err, organizations) {
                                if (err) return res.status(400).send(err);
                                return res.send(organizations);
                            });
                        });
                });


            } else {
                delete a.obj.personnel;

                req.body = a.obj2;
                var query = coreQueryBuilderService.buildQuery(req);
                organizationService.getExhibitionList(query, function(err, exhibition) {

                    req.body = a.obj;
                    var query = coreQueryBuilderService.buildQuery(req);
                    if (err) {
                        query = { _id: null };
                    } else {
                        query['_id'] = { $in: (exhibition.length > 0) ? exhibition[0].ids : [] };

                    }
                    organizationService.dataTable({
                        dataTableQuery: dt,
                        conditions: query
                    }, function(err, organizations) {
                        if (err) return res.status(400).send(err);
                        return res.send(organizations);
                    });
                });
            }
    },
    getOrgIdFromPersonnel: function(req, res) {
        var a = req.body.conditions,
            dt = req.body.dataTableQuery;


        if (req.body.persone == 1) {
            var qr = coreQueryBuilderService.buildQuery(req);
            console.log("qr.conditions",qr.conditions);
            organizationService.getOrgFromPer(qr.conditions, function(err, org) {
                if (org.length != 0) {                    
                    var query = {};
                    query['_id'] = { $in: org[0].ids };
                    organizationService.dataTable({
                        dataTableQuery: dt,
                        conditions: query
                    }, function(err, organizations) {
                        if (err) return res.status(400).send(err);
                        return res.send(organizations);
                    });

                } else {
                    return res.send({ "draw": 1, "recordsTotal": 0, "recordsFiltered": 0, "data": [] });
                }
            });
        } else {
            req.body = a.obj1;
            var query = coreQueryBuilderService.buildQuery(req);
            organizationService.getOrgIdFromPersonnel(query,
                function(err, personnel) {
                console.log("organizations_QUERY_RES",JSON.stringify(personnel));    
                    req.body = a.obj;
                    var query = coreQueryBuilderService.buildQuery(req);
                    if (err) {
                        query = { _id: null };
                    } else {
                        query['_id'] = { $in: (personnel.length > 0) ? personnel[0].ids : [] };
                    }
                    organizationService.dataTable({
                        dataTableQuery: dt,
                        conditions: query
                    }, function(err, organizations) {
                        if (err) return res.status(400).send(err);
                        return res.send(organizations);
                    });
                });

        }

    },
    dataTable: function(req, res) {
        var query = coreQueryBuilderService.buildQuery(req);
        organizationService.dataTable(query, function(err, organizations) {
            if (err) return res.status(400).send(err);
            return res.send(organizations);
        });
    },
    // add
    add: function(req, res, next) {
        var organization = req.body;
        req.queryErrors = [];
        var orgOthers = {
            personnel: organization.personnel,
            exhibition: organization.exhibition,
            chapterSpecification: organization.chapterSpecification,
        };
        delete organization.personnel;
        delete organization.exhibition;
        delete organization.chapterSpecification;
        organizationService.add(organization, function(err, organization) {
            if (err) return res.status(400).send(err);
            if (organization && organization._id) {
                req.body._id = organization._id;
                req.body.personnel = orgOthers.personnel;
                req.body.exhibition = orgOthers.exhibition;
                req.body.chapterSpecification = orgOthers.chapterSpecification;
                next();
            } else return res.status(400).send({ message: 'Organization not added' });
        });
    },
    addPersonnel: function(req, res, next) {
        var personnel = req.body.personnel;
        var removePersonnel = req.body.removePersonnel;
        if (personnel && personnel.length > 0) {
            organizationService.upsertPersonnel(req.body._id, personnel, removePersonnel, req.body.created, function(err, personnel) {
                if (err) req.queryErrors.push(err);
                next();
            });
        } else next();
    },
    addExhibition: function(req, res, next) {
        var exhibition = req.body.exhibition;
        var removeExhibition = req.body.removeExhibition;
        if (exhibition && exhibition.length > 0) {
            organizationService.upsertExhibition(req.body._id, exhibition, removeExhibition, req.body.created, function(err, exhibition) {
                if (err) req.queryErrors.push(err);
                next();
            });
        } else next();
    },
    addChapterSpecification: function(req, res, next) {
        var chapterSpecification = req.body.chapterSpecification;
        var removeChapterSpecification = req.body.removeChapterSpecification;
        if (chapterSpecification && chapterSpecification.length > 0) {
            organizationService.upsertChapterSpecification(req.body._id, chapterSpecification, removeChapterSpecification, req.body.created, function(err, chapterSpecification) {
                if (err) req.queryErrors.push(err);
                next();
            });
        } else next();
    },
    addVersion: function(req, res, next) {
        var version = req.body.version;
        if (version && version.length > 0) {
            req.recentOrgVersionId = version[(version.length - 1)].status;
            organizationService.upsertVersion([req.body._id], version, req.body.updated, function(err, version) {
                if (err) req.queryErrors.push(err); /*some change are need*/
                next();
            });
        } else next();
    },
    addComplete: function(req, res) {
        return res.send({ data: req.body, errors: req.queryErrors });
    },
    // update
    update: function(req, res, next) {
        var organization = req.body;
        var orgOthers = {
            personnel: organization.personnel,
            exhibition: organization.exhibition,
            chapterSpecification: organization.chapterSpecification,
        };
        delete organization.personnel;
        delete organization.exhibition;
        delete organization.chapterSpecification;
        req.queryErrors = [];
        organizationService.update(organization, function(err, organization) {
            if (err) req.queryErrors.push(err);
            req.body.personnel = orgOthers.personnel;
            req.body.exhibition = orgOthers.exhibition;
            req.body.chapterSpecification = orgOthers.chapterSpecification;
            next();
        });
    },
    updatePersonnel: function(req, res, next) {
        var personnel = req.body.personnel;
        var removePersonnel = req.body.removePersonnel;
        if ((personnel && personnel.length > 0) || (removePersonnel && removePersonnel.length > 0)) {
            organizationService.upsertPersonnel(req.body._id, personnel, removePersonnel, req.body.updated, function(err, personnel) {
                if (err) req.queryErrors.push(err);
                next();
            });
        } else next();
    },
    updateExhibition: function(req, res, next) {
        var exhibition = req.body.exhibition;
        var removeExhibition = req.body.removeExhibition;
        if ((exhibition && exhibition.length > 0) || (removeExhibition && removeExhibition.length > 0)) {
            organizationService.upsertExhibition(req.body._id, exhibition, removeExhibition, req.body.updated, function(err, exhibition) {
                if (err) req.queryErrors.push(err);
                next();
            });
        } else next();
    },
    updateChapterSpecification: function(req, res, next) {
        var chapterSpecification = req.body.chapterSpecification;
        var removeChapterSpecification = req.body.removeChapterSpecification;
        if ((chapterSpecification && chapterSpecification.length > 0) || (removeChapterSpecification && removeChapterSpecification.length > 0)) {
            organizationService.upsertChapterSpecification(req.body._id, chapterSpecification, removeChapterSpecification, req.body.updated, function(err, chapterSpecification) {
                if (err) req.queryErrors.push(err);
                next();
            });
        } else next();
    },
    updateVersion: function(req, res) {
        var version = req.body.version;
        if (version) {
            organizationService.upsertVersion(req.body.id, version, req.body.updated, function(err, version) {
                if (err) return res.status(400).send(err);
                return res.send(version);
            });
        } else
            return res.status(400).send({ message: 'Could not get Version details!' });
    },
    updateComplete: function(req, res) {
        return res.send({ data: req.body, errors: req.queryErrors });
    },
    updateOrganizationLatestVersion: function(req, res, next) {
        var organization = req.body;
        organization.versionId = req.recentOrgVersionId;
        organizationService.update(organization, function(err, organization) {
            if (err) req.queryErrors.push(err);
            next();
        });
    },
    // delete
    remove: function(req, res) {
        organizationService.remove(req.body, function(err, organization) {
            if (err) return res.status(400).send(err);
            return res.send(organization);
        });
    },
    // uncategorized
    getSummaryReportCount: function(req, res) {
        var query = coreQueryBuilderService.buildQuery(req);
        /* var query = {
            "summaryType": "SAGAR",
            "recordType": "dIKSHA",
            "sourceCount": 10,
            "targetCount": 10
        };*/


        organizationService.getSummaryReportCount(query, function(err, summaryReportCount) {
            if (err) return res.status(400).send(err);
            return res.send(summaryReportCount);
        });
    },
    getExhibitionCount: function(req, res) {
        var query = coreQueryBuilderService.buildQuery(req);
        query.active = true;
        query.deleted = false;
        organizationService.getExhibitionCount(query, function(err, count) {

            if (err) return res.status(400).send(err);
            return res.send({ count: count });
        });
    },
    getActivitiesCount: function(req, res) {
        var query = coreQueryBuilderService.buildQuery(req);
        query.active = true;
        query.deleted = false;
        query.activities = { $exists: true };
        organizationService.getActivitiesCount(query, function(err, count) {

            if (err) return res.status(400).send(err);
            return res.send({ count: count });
        });
    },
    getOrganizationCount: function(req, res) {
        var query = coreQueryBuilderService.buildQuery(req);
        query.active = true;
        query.deleted = false;
        organizationService.getOrganizationCount(query, function(err, count) {
            if (err) return res.status(400).send(err);
            return res.send({ count: count });
        });
    },
    // OCD
    getOrganizationCountOCDUSDio: function(req, res, next) {
        var directoryId = (coreQueryBuilderService.buildQuery(req)).directoryId;
        var query = { "classificationCodeName": "US Diocese" };
        query.directoryId = directoryId;
        query.active = true;
        query.deleted = false;
        organizationService.getOrganizationCount(query, function(err, count) {
            req.resultObject["US Diocese"] = count;
            next();
        });
    },
    getOrganizationCountOCDWorldDio: function(req, res, next) {
        var directoryId = (coreQueryBuilderService.buildQuery(req)).directoryId;
        var query = { "classificationCodeName": "World Diocese" };
        query.directoryId = directoryId;
        query.active = true;
        query.deleted = false;
        organizationService.getOrganizationCount(query, function(err, count) {
            req.resultObject["World Diocese"] = count;
            next();
        });
    },
    getOrganizationCountOCDOrg: function(req, res, next) {
        var directoryId = (coreQueryBuilderService.buildQuery(req)).directoryId;
        var query = {
            "classificationCodeName": {
                $in: ["Base/Generic Organization", "Religious Order Organization", "Curia Organization", "Parish", "Residence", "School", "High School", "Catechesis Religious Program", "College", "Seminary", "Shrine", "Oratory", "Mission", "Station", "Chapel", "Church", "Hospital", "Special Care Facility", "Personal Prelature", "Convent", "Monastery", "Propogation of the Faith", "Nursing Home/Homes for the Aged", "Campus Ministry/Newman Center", "Catholic Charity", "Retreat House/Renewal Center", "Rectory", "Missionary Activities", "Child Care", "Cemetery", "Endowment"]
            }
        };
        query.directoryId = directoryId;
        query.active = true;
        query.deleted = false;
        organizationService.getOrganizationCount(query, function(err, count) {
            req.resultObject["Organization"] = count;
            next();
        });
    },
    getOrganizationCountOCDRelOrdOrg: function(req, res, next) {
        var directoryId = (coreQueryBuilderService.buildQuery(req)).directoryId;
        var query = { "classificationCodeName": "Religious Order Organization" };
        query.directoryId = directoryId;
        query.active = true;
        query.deleted = false;
        organizationService.getOrganizationCount(query, function(err, count) {
            req.resultObject["Religious Order Organization"] = count;
            next();
        });
    },
    getOrganizationCountOCDOrgCuria: function(req, res, next) {
        var directoryId = (coreQueryBuilderService.buildQuery(req)).directoryId;
        var query = { "classificationCodeName": "Curia Organization" };
        query.directoryId = directoryId;
        query.active = true;
        query.deleted = false;
        organizationService.getOrganizationCount(query, function(err, count) {
            req.resultObject["Curia Organization"] = count;
            next();
        });
    },
    getOrganizationCountOCDSchool: function(req, res, next) {
        var directoryId = (coreQueryBuilderService.buildQuery(req)).directoryId;
        var query = { "classificationCodeName": "School" };
        query.directoryId = directoryId;
        query.active = true;
        query.deleted = false;
        organizationService.getOrganizationCount(query, function(err, count) {
            req.resultObject["School"] = count;
            next();
        });
    },
    getOrganizationCountOCDParish: function(req, res, next) {
        var directoryId = (coreQueryBuilderService.buildQuery(req)).directoryId;
        var query = { "classificationCodeName": "Parish" };
        query.directoryId = directoryId;
        query.active = true;
        query.deleted = false;
        organizationService.getOrganizationCount(query, function(err, count) {
            req.resultObject["Parish"] = count;
            next();
        });
    },
    // ---
    getPersonnelCount: function(req, res) {
        var query = coreQueryBuilderService.buildQuery(req);
        // query.active = true;
        query.deleted = false;
        organizationService.getPersonnelCount(query, function(err, count) {
            if (err) return res.status(400).send(err);
            return res.send({ count: count });
        });
    },
    getFacilitiesCount: function(req, res) {
        var query = coreQueryBuilderService.buildQuery(req);
        query.active = true;
        query.deleted = false;
        query.features = { $exists: true };

        organizationService.getFacilitiesCount(query, function(err, count) {

            if (err) return res.status(400).send(err);
            return res.send({ count: count });
        });
    },
    getCollectionCount: function(req, res) {
        var query = coreQueryBuilderService.buildQuery(req);
        query.active = true;
        query.deleted = false;
        query.features = { $exists: true };
        organizationService.getCollectionCount(query, function(err, count) {

            if (err) return res.status(400).send(err);
            return res.send({ count: count });
        });
    },
    getPersonnel: function(req, res, next) {
        var query = {
            where: {
                orgId: req.organization._id,
                //  active: true,
                deleted: false,
            },
            sort: {
                sequenceNumber: 1,
                _id: 1
            }
        };
        organizationService.getPersonnel(query, function(err, personnel) {
            if (err) req.queryErrors.push(err);
            if (!personnel) req.queryErrors.push({ message: 'Could not fetch Personnel' });
            req.organization['personnel'] = personnel;
            next();
        });
    },
    getExhibition: function(req, res, next) {
        var query = {
            where: {
                orgId: req.organization._id,
                active: true,
                deleted: false,
            },
            sort: {
                sequenceNumber: 1,
                _id: 1
            }
        };
        organizationService.getExhibition(query, function(err, exhibition) {
            if (err) req.queryErrors.push(err);
            if (!exhibition) req.queryErrors.push({ message: 'Could not fetch Exhibitions' });
            req.organization['exhibition'] = exhibition;
            next();
        });
    },
    getChapterSpecification: function(req, res, next) {
        var query = {
            where: {
                organizationId: req.organization._id,
                active: true,
                deleted: false,
            },
            sort: {
                sequence_number: 1,
                _id: 1
            }
        };
        organizationService.getChapterSpecification(query, function(err, chapterSpecification) {
            if (err) req.queryErrors.push(err);
            if (!chapterSpecification) req.queryErrors.push({ message: 'Could not fetch Chapter Specification' });
            req.organization['chapterSpecification'] = chapterSpecification;
            next();
        });
    },
    getVersion: function(req, res) {
        var query = coreQueryBuilderService.buildQuery(req);
        query.active = true;
        organizationService.getVersion(query, function(err, versions) {
            if (err) return res.status(400).send(err);
            return res.send(versions);
        });
    }
};


function buildSelectForExport(selectedFields) {
    var newSelect = {
        organization: [],
        personnel: [],
        exhibition: [],
        chapterSpecification: []
    };
    for (var i = 0; i < selectedFields.length; i++) {
        if (newSelect[selectedFields[i].base].indexOf(selectedFields[i].ngModel) < 0)
            newSelect[selectedFields[i].base].push(selectedFields[i].ngModel);
    }
    return newSelect;
}

function getMaxRowsNeeded(organization, selectedFields) {
    var maxRowsNeeded = 1,
        max = {};

    for (var i = 0; i < selectedFields.length; i++) {
        var splitString = selectedFields[i].ngModel.split('.');
        var nestField, nestArray, data = organization;
        if (nestField = (selectedFields[i].arrayPath))
            for (var k = 0; k < splitString.length; k++) {
                if (nestField == (splitString.slice(0, k)).join('.')) {
                    if (nestArray = (selectedFields[i].nestedArrayPath)) {
                        if (!max[nestField]) max[nestField] = [];
                        for (var l = 0; l < data.length; l++) {
                            if (!max[nestField][l]) max[nestField][l] = 1;
                            var finalLength = data[l] && data[l][nestArray] ? data[l][nestArray].length : 0;
                            max[nestField][l] = finalLength > max[nestField][l] ? finalLength : max[nestField][l];
                        }
                        var sumMax = 0;
                        for (var m = 0; m < max[nestField].length; m++) {
                            sumMax += max[nestField][m];
                        }
                        maxRowsNeeded = (sumMax > maxRowsNeeded) ? sumMax : maxRowsNeeded;
                    } else {
                        var finalLength = data ? data.length : 0;
                        maxRowsNeeded = finalLength > maxRowsNeeded ? finalLength : maxRowsNeeded;
                    }
                }
                data = data ? data[splitString[k]] : '';
            };
    }

    return maxRowsNeeded;
}

function discardDeleted(organizations) {
    for (var g = 0; g < organizations.length; g++) {
        var organization = organizations[g],
            fn = [];
        if (organization.formerNames) {
            for (var i = 0; i < organization.formerNames.length; i++) {
                if (!organization.formerNames[i].deleted) fn.push(organization.formerNames[i]);
            }
            organization.formerNames = fn;
        }
        if (organization.bookEdition) {
            fn = [];
            for (var i = 0; i < organization.bookEdition.length; i++) {
                if (!organization.bookEdition[i].deleted) fn.push(organization.bookEdition[i]);
            }
            organization.bookEdition = fn;
        }
        if (organization.address) {
            fn = [];
            for (var i = 0; i < organization.address.length; i++) {
                if (!organization.address[i].deleted) fn.push(organization.address[i]);
            }
            organization.address = fn;
        }
        if (organization.features) {
            fn = [];
            for (var i = 0; i < organization.features.length; i++) {
                if (!organization.features[i].deleted) fn.push(organization.features[i]);
            }
            organization.features = fn;
        }
        if (organization.research) {
            fn = [];
            for (var i = 0; i < organization.research.length; i++) {
                if (!organization.research[i].deleted) fn.push(organization.research[i]);
            }
            organization.research = fn;
        }
        if (organization.publication) {
            fn = [];
            for (var i = 0; i < organization.publication.length; i++) {
                if (!organization.publication[i].deleted) fn.push(organization.publication[i]);
            }
            organization.publication = fn;
        }
        if (organization.listingType) {
            fn = [];
            for (var i = 0; i < organization.listingType.length; i++) {
                if (!organization.listingType[i].deleted) fn.push(organization.listingType[i]);
            }
            organization.listingType = fn;
        }
        if (organization.directMarketingBudgetDisbursal) {
            fn = [];
            for (var i = 0; i < organization.directMarketingBudgetDisbursal.length; i++) {
                if (!organization.directMarketingBudgetDisbursal[i].deleted) fn.push(organization.directMarketingBudgetDisbursal[i]);
            }
            organization.directMarketingBudgetDisbursal = fn;
        }
        if (organization.notes) {
            fn = [];
            for (var i = 0; i < organization.notes.length; i++) {
                if (!organization.notes[i].deleted) fn.push(organization.notes[i]);
            }
            organization.notes = fn;
        }
        if (organization.vendors) {
            fn = [];
            for (var i = 0; i < organization.vendors.length; i++) {
                if (!organization.vendors[i].deleted) fn.push(organization.vendors[i]);
            }
            organization.vendors = fn;
        }
        if (organization.personnel && organization.personnel.length > 0) {
            for (var h = 0; h < organization.personnel.length; h++) {
                if (organization.personnel[h].address) {
                    fn = [];
                    for (var i = 0; i < organization.personnel[h].address.length; i++) {
                        if (!organization.personnel[h].address[i].deleted) fn.push(organization.personnel[h].address[i]);
                    }
                    organization.personnel[h].address = fn;
                }
                if (organization.personnel[h].ocdDegree) {
                    fn = [];
                    for (var i = 0; i < organization.personnel[h].ocdDegree.length; i++) {
                        if (!organization.personnel[h].ocdDegree[i].deleted) fn.push(organization.personnel[h].ocdDegree[i]);
                    }
                    organization.personnel[h].ocdDegree = fn;
                }
                if (organization.personnel[h].ocdContact) {
                    fn = [];
                    for (var i = 0; i < organization.personnel[h].ocdContact.length; i++) {
                        if (!organization.personnel[h].ocdContact[i].deleted) fn.push(organization.personnel[h].ocdContact[i]);
                    }
                    organization.personnel[h].ocdContact = fn;
                }
                if (organization.personnel[h].notes) {
                    fn = [];
                    for (var i = 0; i < organization.personnel[h].notes.length; i++) {
                        if (!organization.personnel[h].notes[i].deleted) fn.push(organization.personnel[h].notes[i]);
                    }
                    organization.personnel[h].notes = fn;
                }
                if (organization.personnel[h].assignment) {
                    fn = [];
                    for (var i = 0; i < organization.personnel[h].assignment.length; i++) {
                        if (!organization.personnel[h].assignment[i].deleted) fn.push(organization.personnel[h].assignment[i]);
                    }
                    organization.personnel[h].assignment = fn;
                }
                if (organization.personnel[h].responsibilityCodes) {
                    fn = [];
                    for (var i = 0; i < organization.personnel[h].responsibilityCodes.length; i++) {
                        if (!organization.personnel[h].responsibilityCodes[i].deleted) fn.push(organization.personnel[h].responsibilityCodes[i]);
                    }
                    organization.personnel[h].responsibilityCodes = fn;
                }
                if (organization.personnel[h].officers) {
                    fn = [];
                    for (var i = 0; i < organization.personnel[h].officers.length; i++) {
                        if (!organization.personnel[h].officers[i].deleted) fn.push(organization.personnel[h].officers[i]);
                    }
                    organization.personnel[h].officers = fn;
                }
            }
        }
        if (organization.legalTitles) {
            fn = [];
            for (var i = 0; i < organization.legalTitles.length; i++) {
                if (!organization.legalTitles[i].deleted) fn.push(organization.legalTitles[i]);
            }
            organization.legalTitles = fn;
        }
        if (organization.statistic) {
            fn = [];
            for (var i = 0; i < organization.statistic.length; i++) {
                if (!organization.statistic[i].deleted) fn.push(organization.statistic[i]);
            }
            organization.statistic = fn;
        }
        if (organization.ocdContact) {
            fn = [];
            for (var i = 0; i < organization.ocdContact.length; i++) {
                if (!organization.ocdContact[i].deleted) fn.push(organization.ocdContact[i]);
            }
            organization.ocdContact = fn;
        }
        organizations[g] = organization;
    }
    return organizations;
}

function getRow(data, fields, rowIndex) {
    var row = {};

    for (var i = 0; i < fields.length; i++) {
        row[fields[i].name] = getColumn(data, fields[i], rowIndex);
    }

    if (COLUMNINDEX >= COLUMNMAX) {
        RECORDINDEX++;
        COLUMNINDEX = 0;
        COLUMNMAX = 1;
    }

    return row;
}

function getColumn(data, field, rowIndex) {
    var splitString = field.ngModel.split('.');
    if (field.isNested && field.arrayPath) {
        data = getCell(data, field, rowIndex);
    } else if (rowIndex == 0) {
        for (var k = 0; k < splitString.length; k++) {
            data = data ? data[splitString[k]] : "";
        }
    } else data = "";

    return getActualData(data, field, rowIndex);;
}

function getCell(data, field, rowIndex) {
    var nestFieldPath = field.arrayPath,
        nestArrayPath = field.nestedArrayPath;
    field = field.ngModel.split('.');

    for (var k = 0; k < field.length; k++) {
        if (data) {
            if (nestFieldPath == (field.slice(0, k)).join('.')) {
                if (EXCLUDEPARENTS[nestFieldPath] = (EXCLUDEPARENTS[nestFieldPath] || !!(nestArrayPath))) {
                    if (nestArrayPath == field[k]) {
                        data = data[RECORDINDEX] ? data[RECORDINDEX][field[k]] : "";
                        COLUMNMAX = (data && COLUMNMAX < data.length) ? data.length : COLUMNMAX;
                        data = data ? data[(COLUMNINDEX - 1)] : "";
                    } else if (COLUMNINDEX == 1)
                        data = data[RECORDINDEX] ? data[RECORDINDEX][field[k]] : "";
                    else data = "";
                } else data = data[rowIndex] ? data[rowIndex][field[k]] : "";
            } else if (field.length == 1)
                data = data[field[k]][rowIndex];
            else
                data = data[field[k]];
        }
    }
    return data;
}

function getActualData(data, field, rowIndex) {
    var valueField = field ? field.valuePath : false;
    if (valueField) {
        splitString = valueField.split('.');
        for (var k = 0; k < splitString.length; k++) {
            if (data) data = data[splitString[k]];
        }
    }
    if (field.isIndexedArray && data && data.constructor === Array) {
        data = data[rowIndex];
    }
    if (field.isDate && data) {
        try {
            data = dateFormat(new Date(data), field.dateFormat);
        } catch (e) {
            data = data;
        }
    }
    if (field.hasOptions && field.hasOptions.length > 0) {
        var label = '';
        for (var m = field.hasOptions.length - 1; m >= 0; m--) {
            if (field.hasOptions[m].value === data) {
                label = field.hasOptions[m].label;
                break;
            }
        }
        data = label;
    }
    return sanitizeForCSV(data);
}

function sanitizeForCSV(data) {
    if ((data && data != '') || (data == 0) || (data == '0')) {
        try {
            data = decodeURIComponent(escape(data));
        } catch (e) {
            console.log(e);
        }
        var arr = CSVtoArray("'" + data.toString().replace(/'/g, "\\'") + "'");
        if (arr && arr.constructor === Array && arr.length > 0) data = arr[0];
    } else data = "";
    return data;
}

function CSVtoArray(text) {
    var re_valid = /^\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*(?:,\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*)*$/;
    var re_value = /(?!\s*$)\s*(?:'([^'\\]*(?:\\[\S\s][^'\\]*)*)'|"([^"\\]*(?:\\[\S\s][^"\\]*)*)"|([^,'"\s\\]*(?:\s+[^,'"\s\\]+)*))\s*(?:,|$)/g;
    // Return NULL if input string is not well formed CSV string.
    if (!re_valid.test(text)) return null;
    var a = []; // Initialize array to receive values.
    text.replace(re_value, // "Walk" the string using replace with callback.
        function(m0, m1, m2, m3) {
            // Remove backslash from \' in single quoted values.
            if (m1 !== undefined) a.push(m1.replace(/\\'/g, "'"));
            // Remove backslash from \" in double quoted values.
            else if (m2 !== undefined) a.push(m2.replace(/\\"/g, '"'));
            else if (m3 !== undefined) a.push(m3);
            return ''; // Return empty string.
        });
    // Handle special case of empty last value.
    if (/,\s*$/.test(text)) a.push('');
    return a;
};

function cleanJson(json) {
    if (json === null || typeof json === 'undefined') {
        delete json;
    } else if (json.constructor === Array) {
        if (json.length == 0) {
            delete json;
        } else {
            json.forEach(cleanJson);
        }
    } else if ((typeof json).toLowerCase() == 'object') {
        if (Object.keys(json).length == 0) {
            delete json;
        } else {
            Object.keys(json).forEach(function(key) {
                var value = json[key];
                if (value.constructor === Array && value.length == 0) {
                    delete json[key];
                } else if ((typeof value).toLowerCase() == 'object' && Object.keys(value).length == 0) {
                    delete json[key];
                } else json[key] = cleanJson(value);
            });
        }
    }
    return json;
}

function parseQueryExt(value) {
    if (value && typeof value != 'number') {
        value = (typeof value == 'string' && value.indexOf("{") >= 0) ? JSON.parse(value) : value;
        try {
            var id;
            if (value['$in'] && value['$in'].length > 0) {
                value['$in'].forEach(function(v, index) {
                    value['$in'][index] = organizationService.mongoose.Types.ObjectId(v.toString());
                });
            } else if (value['$nin'] && value['$nin'].length > 0) {
                value['$nin'].forEach(function(v, index) {
                    value['$nin'][index] = organizationService.mongoose.Types.ObjectId(v.toString());
                });
            } else {
                value = organizationService.mongoose.Types.ObjectId(value.toString());
            }
        } catch (e) {}
    }
    return value;
}

Array.prototype.clean = function(deleteValue) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == deleteValue) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
};
