var express = require('express');
var organization = express.Router();

var organizationController = require('./organization.controller.js');

organization.get('/list', organizationController.list);

organization.get('/', organizationController.get, /*organizationController.getPersonnel, organizationController.getExhibition, organizationController.getChapterSpecification, organizationController.getVersion,*/ organizationController.getComplete);

organization.post('/getChapterSpecificationData', organizationController.getChapterSpecificationData);
organization.get('/version', organizationController.getVersion);

organization.post('/data-table', organizationController.dataTable);

//Added by Swapnil Sonawane 31/07/2016 for Advanced Search Functionality
organization.get('/exhibition/list', organizationController.getExhibitionList);
organization.get('/personnel/list', organizationController.getOrgIdFromPersonnel);
organization.get('/org-list',organizationController.getOrganizationList);
organization.get('/chapter-specification',organizationController.getChapterSpecificationListForSearch);


// organization.post('/export-to-excel', organizationController.exportToExcel, organizationController.exportToExcelPersonnel, organizationController.exportToExcelExhibition, organizationController.exportToExcelChapterSpecification, organizationController.exportToExcelComplete);

organization.post('/export-to-xml', organizationController.exportToXMLGetStructure, organizationController.exportToXML, organizationController.exportToXMLComplete);
organization.post('/export-to-xml/import-structure', organizationController.xmlImportStructure);

organization.post('/export-to-excel', organizationController.exportToExcelCount, organizationController.exportToExcel, organizationController.exportToExcelComplete);

organization.post('/', organizationController.add, organizationController.addPersonnel, organizationController.addExhibition, organizationController.addChapterSpecification, organizationController.addComplete);

organization.post('/version', organizationController.addVersion, organizationController.updateOrganizationLatestVersion, organizationController.addComplete);

organization.put('/', organizationController.update, organizationController.updatePersonnel,
    organizationController.updateExhibition,
    organizationController.updateChapterSpecification,
    organizationController.updateComplete);

organization.put('/version', organizationController.updateVersion);

organization.get('/exhibition/count', organizationController.getExhibitionCount);
organization.get('/personnel/count', organizationController.getPersonnelCount);
organization.get('/exhibition/getPersonnel',organizationController.getPersonnel);
organization.get('/activities/count', organizationController.getActivitiesCount);
organization.get('/facilities/count', organizationController.getFacilitiesCount);
organization.get('/collection/count', organizationController.getCollectionCount);
organization.get('/organization/count', organizationController.getOrganizationCount);

organization.get('/organization/count-ocd', function(req, res, next) {
        req.resultObject = {};
        next();
    },
    organizationController.getOrganizationCountOCDUSDio,
    organizationController.getOrganizationCountOCDWorldDio,
    organizationController.getOrganizationCountOCDOrg,
    organizationController.getOrganizationCountOCDRelOrdOrg,
    organizationController.getOrganizationCountOCDOrgCuria,
    organizationController.getOrganizationCountOCDSchool,
    organizationController.getOrganizationCountOCDParish,
    function(req, res) {
        res.send(req.resultObject);
    });

// organization.get('/organization/organization-count', organizationController.getOrganizationCount);
organization.get('/summary-report-count', organizationController.getSummaryReportCount);

organization.post('/personnel/list', organizationController.getPersonnelList);

/*organization.put('/updateExhibition',organizationController.updateExhibition,
      organizationController.updateComplete);
*/
organization.put('/delete', organizationController.remove);


module.exports = organization;
