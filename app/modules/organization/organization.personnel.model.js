var mongoose = require('mongoose');
var PersonnelAssignmentSchema = require('./schemas/personnel.assignment.schema.js');
var PersonnelNotesSchema = require('./schemas/personnel.notes.schema.js');
var PersonnelAddressSchema = require('./schemas/personnel.address.schema.js');
var PersonnelOfficersSchema = require('./schemas/personnel.officers.schema.js');
var PersonnelOCDDegreeSchema = require('./schemas/personnel.ocdDegree.schema.js');
var PersonnelOCDContactSchema = require('./schemas/personnel.ocdContact.schema.js');
var PersonnelResponsibilityCodesSchema = require('./schemas/personnel.responsibility.schema.js');
var Schema = mongoose.Schema;

var TXN_PersonnelSchema = new Schema({
    "orgId": [{
        type: Schema.Types.ObjectId,
        ref: 'TXN_Organization'
    }],
    "directoryId": {
        type: Schema.Types.ObjectId,
        ref: 'MST_RefCodeValue'
    },
    "orgUserId": {
        "type": Schema.Types.ObjectId,
        "ref": 'TXN_User'
    },
    "titleMaster": {
        type: Schema.Types.ObjectId,
        ref: 'MST_RefCodeValue'
    },
    // New added fields 11 June 2016
    "titleMasterName": String,

    "title": String,
    "titleType": String,
    "sequenceNumber": Number,
    "employee_id": String,
    "personnel_id": String,
    "name": {
        "prefix": String,
        "first": String,
        "middle": String,
        "last": String,
        "suffix": String
    },
    "phoneNumbers": String,
    "faxNumbers": String,
    "website": String, //New field
    "service": String,
    "membership": String,
    "attention": Boolean,
    "chairman": Boolean,
    "dmmpContact": Boolean,
    "printFlag": Boolean, // If TRUE, the personnel will be print from PDF
    //for OCD
    "personType": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "webSite": String,
    "religiousOrderInitials": String,
    "retired": Boolean,
    "very": Boolean,
    "homeDiocese": String,
    "homeNation": String,
    "status": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "diedFlag": Boolean,
    "died": {
        "day": String,
        "month": String,
        "year": String
    },
    "ordination": {
        "ordination_day": String,
        "ordination_month": String,
        "ordination_year": String
    },
    "assignment": [PersonnelAssignmentSchema],
    "notes": [PersonnelNotesSchema],
    "ocdDegree": [PersonnelOCDDegreeSchema],
    "ocdContact": [PersonnelOCDContactSchema],
    //18/08/2016
    "responsibilityCodes": [PersonnelResponsibilityCodesSchema],
    // New added fields 11 June 2016
    "degree": String,
    "suppress": String,
    "contactPersonIndex": String,
    "indexIndication": String,

    "phoneNumbers": String,
    "faxNumbers": String,
    "website": String, //New field

    "service": String,
    "membership": String,
    "attention": Boolean,

    "description": String,
    "address": [PersonnelAddressSchema],
    "email": {
        "primary": String,
        "secondary": String
    },
    "officers": [PersonnelOfficersSchema],
    "activeFlag": Boolean,
    active: {
        type: Boolean,
        default: true
    },
    deletedFlag: {
        type: Boolean,
        default: false
    },
    deleted: {
        type: Boolean,
        default: false
    },
    "created": {
        "by": {
            "type": Schema.Types.ObjectId,
            "ref": 'TXN_User'
        },
        "at": Date
    },
    "updated": {
        "by": {
            "type": Schema.Types.ObjectId,
            "ref": 'TXN_User'
        },
        "at": Date
    }
}).post('init', function(doc) {
    if (doc.orgId && doc.orgId.constructor !== Array) doc.orgId = [doc.orgId];
    return doc;
});

TXN_PersonnelSchema.index({ sequenceNumber: 1, _id: 1 });
TXN_PersonnelSchema.index({ sequenceNumber: -1, _id: 1 });
TXN_PersonnelSchema.index({ sequenceNumber: -1, _id: -1 });

TXN_PersonnelSchema.index({ 'name.prefix': 1 });
TXN_PersonnelSchema.index({ 'name.prefix': -1 });
TXN_PersonnelSchema.index({ 'name.first': 1 });
TXN_PersonnelSchema.index({ 'name.first': -1 });
TXN_PersonnelSchema.index({ 'name.middle': 1 });
TXN_PersonnelSchema.index({ 'name.middle': -1 });
TXN_PersonnelSchema.index({ 'name.last': 1 });
TXN_PersonnelSchema.index({ 'name.last': 1, 'name.first': 1, 'name.middle': 1 });
TXN_PersonnelSchema.index({ 'name.last': 1, 'name.first': 1, 'titleMasterName': 1 });
TXN_PersonnelSchema.index({ 'name.last': -1 });
TXN_PersonnelSchema.index({ 'name.suffix': 1 });
TXN_PersonnelSchema.index({ 'name.suffix': -1 });

module.exports = mongoose.model('TXN_Personnel', TXN_PersonnelSchema);
