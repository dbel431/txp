var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TXN_OrgVersionSchema = new Schema({
    "organization": {
        type: Schema.Types.ObjectId,
        ref: 'TXN_Organization'
    },
    "status": {
        type: Schema.Types.ObjectId,
        ref: 'MST_RefCodeValue'
    },
    "data": Schema.Types.Mixed,
    "comments": String,
    active: {
        type: Boolean,
        default: true
    },
    deleted: {
        type: Boolean,
        default: false
    },
    "created": {
        "by": {
            "type": Schema.Types.ObjectId,
            "ref": 'TXN_User'
        },
        "at": Date
    }
});

module.exports = mongoose.model('TXN_OrgVersion', TXN_OrgVersionSchema);
