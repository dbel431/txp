var aclActionService = require('./acl.action.service.js');

module.exports = {
    get: function(req, res) {
        var query = {};
        if (req.query) {
            if (req.query.id) {
                query._id = req.query.id;
                delete req.query.id;
            }
            for (var i in req.query) {
                query[i] = req.query[i];
            }
        }
        if (req.body) {
            if (req.body.id) {
                query._id = req.body.id;
                delete req.body.id;
            }
            for (var i in req.body) {
                query[i] = req.body[i];
            }
        }
        aclActionService.get(query, function(err, aclActions) {
            if (err) return res.status(400).send(err);
            return res.send(aclActions);
        });
    },
    add: function(req, res) {
        aclActionService.add(req.body, function(err, aclAction) {
            if (err) return res.status(400).send(err);
            return res.send(aclAction);
        });
    },
    update: function(req, res) {
        aclActionService.update(req.body, function(err, aclAction) {
            if (err) return res.status(400).send(err);
            return res.send(aclAction);
        });
    }
};
