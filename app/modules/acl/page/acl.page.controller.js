var aclPageService = require('./acl.page.service.js');

module.exports = {
    get: function(req, res) {
        var query = {};
        if (req.query) {
            if (req.query.id) {
                query._id = req.query.id;
                delete req.query.id;
            }
            for (var i in req.query) {
                query[i] = req.query[i];
            }
        }
        if (req.body) {
            if (req.body.id) {
                query._id = req.body.id;
                delete req.body.id;
            }
            for (var i in req.body) {
                query[i] = req.body[i];
            }
        }
        aclPageService.get(query, function(err, aclPages) {
            if (err) return res.status(400).send(err);
            return res.json(aclPages);
        });
    },
    add: function(req, res) {
        aclPageService.add(req.body, function(err, aclPage) {
            if (err) return res.status(400).send(err);
            return res.send(aclPage);
        });
    },
    update: function(req, res) {
        aclPageService.update(req.body, function(err, aclPage) {
            if (err) return res.status(400).send(err);
            return res.send(aclPage);
        });
    }
};
