var express = require('express');
var acl = express.Router();

var aclModuleController = require('./module/acl.module.controller.js');
var aclPageController = require('./page/acl.page.controller.js');
var aclActionController = require('./action/acl.action.controller.js');

acl.get('/module', aclModuleController.get);
acl.post('/module', aclModuleController.add);
acl.put('/module', aclModuleController.update);

acl.get('/page', aclPageController.get);
acl.post('/page', aclPageController.add);
acl.put('/page', aclPageController.update);

acl.get('/action', aclActionController.get);
acl.post('/action', aclActionController.add);
acl.put('/action', aclActionController.update);

module.exports = acl;
