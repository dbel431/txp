var aclModuleService = require('./acl.module.service.js');

module.exports = {
    get: function(req, res) {
        var query = {};
        if (req.query) {
            if (req.query.id) {
                query._id = req.query.id;
                delete req.query.id;
            }
            for (var i in req.query) {
                query[i] = req.query[i];
            }
        }
        if (req.body) {
            if (req.body.id) {
                query._id = req.body.id;
                delete req.body.id;
            }
            for (var i in req.body) {
                query[i] = req.body[i];
            }
        }
        aclModuleService.get(query, function(err, aclModules) {
            if (err) return res.status(400).send(err);
            return res.send(aclModules);
        });
    },
    add: function(req, res) {
        aclModuleService.add(req.body, function(err, aclModule) {
            if (err) return res.status(400).send(err);
            return res.send(aclModule);
        });
    },
    update: function(req, res) {
        aclModuleService.update(req.body, function(err, aclModule) {
            if (err) return res.status(400).send(err);
            return res.send(aclModule);
        });
    }
};
