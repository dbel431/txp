var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MST_SecurityQuestionSchema = new Schema({
    question: {
        type: String,
        required: true
    },
    active: {
        type: Boolean,
        default: true
    },
    deleted: {
        type: Boolean,
        default: false
    },
    created: {
        by: {
            type: Schema.Types.ObjectId,
            ref: 'TXN_User',
        },
        at: {
            type: Date,
            default: Date.now
        }
    },
    updated: {
        by: {
            type: Schema.Types.ObjectId,
            ref: 'TXN_User'
        },
        at: {
            type: Date,
            default: Date.now
        }
    }
});

MST_SecurityQuestionSchema.index({ question: 1, _id: 1 });

module.exports = mongoose.model('MST_SecurityQuestion', MST_SecurityQuestionSchema);
