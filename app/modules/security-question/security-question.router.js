var express = require('express');
var securityQuestion = express.Router();

var securityQuestionController = require('./security-question.controller.js');

securityQuestion.get('/', securityQuestionController.get);
securityQuestion.post('/', securityQuestionController.add);
securityQuestion.put('/', securityQuestionController.update);
securityQuestion.put('/delete', securityQuestionController.remove);
module.exports = securityQuestion;
