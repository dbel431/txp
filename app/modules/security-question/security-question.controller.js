var securityQuestionService = require('./security-question.service.js');

module.exports = {
    get: function(req, res) {
        var query = {};
        if (req.query) {
            if (req.query.id) {
                query._id = req.query.id;
                delete req.query.id;
            }
            for (var i in req.query) {
                query[i] = req.query[i];
            }
        }
        if (req.body) {
            if (req.body.id) {
                query._id = req.body.id;
                delete req.body.id;
            }
            for (var i in req.body) {
                query[i] = req.body[i];
            }
        }
        securityQuestionService.get(query, function(err, securityQuestions) {
            if (err) return res.status(400).send(err);
            return res.send(securityQuestions);
        });
    },
    add: function(req, res) {
        securityQuestionService.add(req.body, function(err, securityQuestion) {
            if (err) return res.status(400).send(err);
            return res.send(securityQuestion);
        });
    },
    update: function(req, res) {
        securityQuestionService.update(req.body, function(err, securityQuestion) {
            if (err) return res.status(400).send(err);
            return res.send(securityQuestion);
        });
    },
    remove: function(req, res) {
        securityQuestionService.remove(req.body, function(err, securityQuestion) {
            if (err) return res.status(400).send(err);
            return res.send(securityQuestion);
        });
    }
};
