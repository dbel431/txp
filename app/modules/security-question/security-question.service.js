var SecurityQuestion = require('./security-question.model.js');
var Audit = require('../core/audit.log.service.js');
var Exception = require('../core/exception.log.service.js');

module.exports = {
    get: function(query, callback) {
        if (typeof query.deleted == 'undefined') query.deleted = false;
        SecurityQuestion.find(query).sort({ question: 1, _id: 1 }).exec(callback);
    },
    add: function(securityQuestionData, callback) {
        var by = { by: undefined };
        if (securityQuestionData.created) by.by = securityQuestionData.created.by || undefined;
        SecurityQuestion.create(securityQuestionData, function(err, securityQuestion) {
            if (err) {
                console.log(err);
                Exception.log('SECURITY_QUESTION', 'ADD', 'Security Question add Error', err, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            if (!securityQuestion) return callback(null, false, { message: 'Security Question not found' });
            Audit.log('SECURITY_QUESTION', 'ADD', 'Security Question added', securityQuestion, by.by, function(err) {
                if (err) console.log(err);
            });
            return callback(null, securityQuestion);
        });
    },
    update: function(securityQuestionData, callback) {
        var by = { by: undefined };
        if (securityQuestionData.updated) by.by = securityQuestionData.updated.by || undefined;
        SecurityQuestion.findByIdAndUpdate(securityQuestionData._id, securityQuestionData, function(err, securityQuestion) {
            if (err) {
                console.log(err);
                Exception.log('SECURITY_QUESTION', 'UPDATE', 'Security Question Update Error', err, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            Audit.log('SECURITY_QUESTION', 'UPDATE', 'Security Question Updated', securityQuestion, by.by, function(err) {
                if (err) console.log(err);
            });
            return callback(null, securityQuestion);
        });
    },
    remove: function(securityQuestionData, callback) {
        var by = { by: undefined };
        if (securityQuestionData.updated) by.by = securityQuestionData.updated.by || undefined;
        securityQuestionData.deleted = true;
        SecurityQuestion.findByIdAndUpdate(securityQuestionData._id, securityQuestionData, function(err, securityQuestion) {
            if (err) {
                console.log(err);
                Exception.log('SECURITY_QUESTION', 'DELETE', 'Security Question Delete Error', err, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            Audit.log('SECURITY_QUESTION', 'DELETE', 'Security Question Deleted', securityQuestion, by.by, function(err) {
                if (err) console.log(err);
            });
            return callback(null, securityQuestion);
        });
    }
};
