var SubscriptionPackage = require('./subscription-package.model.js');
var Audit = require('../core/audit.log.service.js');
var Exception = require('../core/exception.log.service.js');

module.exports = {
    get: function(query, callback) {
        if (typeof query.deleted == 'undefined') query.deleted = false;
        SubscriptionPackage.find(query).sort({ packageName: 1, _id: 1 }).exec(callback);
    },
    add: function(subscriptionPackageData, callback) {
        var by = { by: undefined };
        if (subscriptionPackageData.created) by.by = subscriptionPackageData.created.by || undefined;
        SubscriptionPackage.create(subscriptionPackageData, function(err, subscriptionPackage) {
            if (err) {
                console.log(err);
                Exception.log('ROLE', 'ADD', 'Subscription Package add Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            if (!subscriptionPackage) return callback(null, false, { message: 'Subscription Package not found' });
            Audit.log('ROLE', 'ADD', 'Subscription Package added', subscriptionPackage, by.by, function(err) { if(err) console.log(err); });
            return callback(null, subscriptionPackage);
        });
    },
    dataTable: function(query, callback) {
        console.log("zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");
        var options = { conditions: query.conditions || {} };
        if (!options.conditions.deleted) options.conditions.deleted = false;
        SubscriptionPackage.dataTable(query, options, callback);
    },
    update: function(subscriptionPackageData, callback) {
        var by = { by: undefined };
        if (subscriptionPackageData.updated) by.by = subscriptionPackageData.updated.by || undefined;
        SubscriptionPackage.findByIdAndUpdate(subscriptionPackageData._id, subscriptionPackageData, function(err, subscriptionPackage) {
            if (err) {
                console.log(err);
                Exception.log('ROLE', 'UPDATE', 'Subscription Package Update Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            Audit.log('ROLE', 'UPDATE', 'Subscription Package Updated', subscriptionPackage, by.by, function(err) { if(err) console.log(err); });
            return callback(null, subscriptionPackage);
        });
    },
    remove: function(subscriptionPackageData, callback) {
        var by = { by: undefined };
        if (subscriptionPackageData.updated) by.by = subscriptionPackageData.updated.by || undefined;
        subscriptionPackageData.deleted = true;
        SubscriptionPackage.findByIdAndUpdate(subscriptionPackageData._id, subscriptionPackageData, function(err, subscriptionPackage) {
            if (err) {
                console.log(err);
                Exception.log('ROLE', 'DELETE', 'Subscription Package Delete Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            Audit.log('ROLE', 'DELETE', 'Subscription Package Deleted', subscriptionPackage, by.by, function(err) { if(err) console.log(err); });
            return callback(null, subscriptionPackage);
        });
    }
};
