var express = require('express');
var subscriptionPackage = express.Router();

var subscriptionPackageController = require('./subscription-package.controller.js');

subscriptionPackage.get('/', subscriptionPackageController.get);
subscriptionPackage.get('/subscriber-type', subscriptionPackageController.getSubscriberTypes);
subscriptionPackage.post('/data-table', subscriptionPackageController.dataTable);
subscriptionPackage.post('/list', subscriptionPackageController.get);
subscriptionPackage.post('/', subscriptionPackageController.add);
subscriptionPackage.put('/', subscriptionPackageController.update);
subscriptionPackage.put('/delete', subscriptionPackageController.remove);
module.exports = subscriptionPackage;
