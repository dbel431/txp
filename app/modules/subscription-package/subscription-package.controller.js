var coreQueryBuilderService = require('../core/core.query-builder.service.js');
var WebDataService = require('../../web_modules/web-data/web-data.service.js');
var subscriptionPackageService = require('./subscription-package.service.js');

module.exports = {
    get: function(req, res) {
        var query = coreQueryBuilderService.buildQuery(req);
        subscriptionPackageService.get(query, function(err, subscriptionPackages) {
            if (err) return res.status(400).send(err);
            return res.send(subscriptionPackages);
        });
    },
    getSubscriberTypes: function(req, res) {
        var query = coreQueryBuilderService.buildQuery(req);
        WebDataService.get(query, function(err, subscriberTypes) {
            if (err) return res.status(400).send(err);
            return res.send(subscriberTypes);
        });
    },
    dataTable: function(req, res) {
        var query = coreQueryBuilderService.buildQuery(req);
        subscriptionPackageService.dataTable(query, function(err, subscriptionPackages) {
            if (err) return res.status(400).send(err);
            return res.send(subscriptionPackages);
        });
    },
    add: function(req, res) {
        subscriptionPackageService.add(req.body, function(err, subscriptionPackage) {
            if (err) return res.status(400).send(err);
            return res.send(subscriptionPackage);
        });
    },
    update: function(req, res) {
        subscriptionPackageService.update(req.body, function(err, subscriptionPackage) {
            if (err) return res.status(400).send(err);
            return res.send(subscriptionPackage);
        });
    },
    remove: function(req, res) {
        subscriptionPackageService.remove(req.body, function(err, subscriptionPackage) {
            if (err) return res.status(400).send(err);
            return res.send(subscriptionPackage);
        });
    }
};
