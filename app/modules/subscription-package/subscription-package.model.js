var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TXN_SubscriptionPackageSchema = new Schema({
    "directoryId": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "directoryName": String,
    "packageName": String,
    "packageDescription": String,
    "packageCost": Number,
    // "searDataFileds": [{
    //     "label": String,
    //     "base": String,
    //     "ngModel": String,
    //     "name": String,
    //     "dataType": String
    // }],
    "active": {
        type: Boolean,
        default: true
    },
    "deleted": {
        type: Boolean,
        default: false
    },
    "created": {
        "by": {
            "type": Schema.Types.ObjectId,
            "ref": 'TXN_WebUser'
        },
        "at": {
            "type": Date,
            "default": Date.now
        }
    },
    "updated": {
        "by": {
            "type": Schema.Types.ObjectId,
            "ref": 'TXN_WebUser'
        },
        "at": {
            "type": Date,
            "default": Date.now
        }
    }
});

TXN_SubscriptionPackageSchema.index({ packageName: 1, _id: 1 });

module.exports = mongoose.model('TXN_SubscriptionPackage', TXN_SubscriptionPackageSchema);
