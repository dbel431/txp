var todoService = require('./todo.service.js');

module.exports = {
    get: function(req, res) {
        var query = {};
        if (req.query) {
            if (req.query.id) {
                query._id = req.query.id;
                delete req.query.id;
            }
            for (var i in req.query) {
                query[i] = req.query[i];
            }
        }
        if (req.body) {
            if (req.body.id) {
                query._id = req.body.id;
                delete req.body.id;
            }
            for (var i in req.body) {
                query[i] = req.body[i];
            }
        }
        todoService.get(query, function(err, todos) {
            if (err) return res.status(400).send(err);
            return res.send(todos);
        });
    },
    add: function(req, res) {
        todoService.add(req.body, function(err, todo) {
            if (err) return res.status(400).send(err);
            return res.send(todo);
        });
    },
    update: function(req, res) {
        todoService.update(req.body, function(err, todo) {
            if (err) return res.status(400).send(err);
            return res.send(todo);
        });
    },
    remove: function(req, res) {
        todoService.remove(req.body, function(err, todo) {
            if (err) return res.status(400).send(err);
            return res.send(todo);
        });
    }
};
