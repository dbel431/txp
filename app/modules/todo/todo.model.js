var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TXN_ToDoSchema = new Schema({
    task: {
        type: String,
        required: true
    },
    completed: {
        type: Boolean,
        default: false
    },
    deleted: {
        type: Boolean,
        default: false
    },
    created: {
        by: {
            type: Schema.Types.ObjectId,
            ref: 'TXN_User',
        },
        at: {
            type: Date,
            default: Date.now
        }
    },
    updated: {
        by: {
            type: Schema.Types.ObjectId,
            ref: 'TXN_User'
        },
        at: {
            type: Date,
            default: Date.now
        }
    }
});

module.exports = mongoose.model('TXN_ToDo', TXN_ToDoSchema);
