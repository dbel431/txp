var ToDo = require('./todo.model.js');
var Audit = require('../core/audit.log.service.js');
var Exception = require('../core/exception.log.service.js');

module.exports = {
    get: function(query, callback) {
        if (typeof query.deleted == 'undefined') query.deleted = false;
        ToDo.find(query, callback);
    },
    add: function(todoData, callback) {
        var by = { by: undefined };
        if (todoData.created) by.by = todoData.created.by || undefined;
        ToDo.create(todoData, function(err, todo) {
            if (err) {
                console.log(err);
                Exception.log('TODO', 'ADD', 'ToDo add Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            if (!todo) return callback(null, false, { message: 'ToDo not found' });
            Audit.log('TODO', 'ADD', 'ToDo added', todo, by.by, function(err) { if(err) console.log(err); });
            return callback(null, todo);
        });
    },
    update: function(todoData, callback) {
        var by = { by: undefined };
        if (todoData.updated) by.by = todoData.updated.by || undefined;
        ToDo.findByIdAndUpdate(todoData._id, todoData, function(err, todo) {
            if (err) {
                console.log(err);
                Exception.log('TODO', 'UPDATE', 'ToDo Update Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            Audit.log('TODO', 'UPDATE', 'ToDo Updated', todo, by.by, function(err) { if(err) console.log(err); });
            return callback(null, todo);
        });
    },
    remove: function(todoData, callback) {
        var by = { by: undefined };
        if (todoData.updated) by.by = todoData.updated.by || undefined;
        todoData.deleted = true;
        ToDo.findByIdAndUpdate(todoData._id, todoData, function(err, todo) {
            if (err) {
                console.log(err);
                Exception.log('TODO', 'DELETE', 'ToDo Delete Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            Audit.log('TODO', 'DELETE', 'ToDo Deleted', todo, by.by, function(err) { if(err) console.log(err); });
            return callback(null, todo);
        });
    }
};
