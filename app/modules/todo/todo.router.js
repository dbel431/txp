var express = require('express');
var todo = express.Router();

var todoController = require('./todo.controller.js');

todo.get('/', todoController.get);
todo.post('/', todoController.add);
todo.put('/', todoController.update);
todo.put('/delete', todoController.remove);
module.exports = todo;