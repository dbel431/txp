var express = require('express');
var batchJob = express.Router();

var batchJobController = require('./batch-job.controller.js');

batchJob.get('/', batchJobController.get);
batchJob.post('/data-table', batchJobController.dataTable);
batchJob.post('/force-start-all-jobs', batchJobController.forceStartAllJobs);
batchJob.post('/', batchJobController.add);
batchJob.put('/', batchJobController.update);
batchJob.put('/delete', batchJobController.remove);

module.exports = batchJob;
