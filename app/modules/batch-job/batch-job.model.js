var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TXN_BatchJobSchema = new Schema({
    "type": {
        "type": Schema.Types.ObjectId,
        // ref: 'MST_BatchJobType',
        "ref": 'MST_RefCodeValue',
    },
    "name": String,
    "description": String,
    "configuration": {
        "interval": String,
        "cronTime": Date,
        "expectedFinish": Date
    },
    "data": Schema.Types.Mixed,
    "result": Schema.Types.Mixed,
    // invites: [{
    //     type: Schema.Types.ObjectId,
    //     ref: 'TXN_User'
    // }],
    "running": {
        "type": Boolean,
        "default": false
    },
    "runAt": Date,
    "completed": {
        "type": Boolean,
        "default": false
    },
    "completedAt": Date,
    "retryComplete": {
        "type": Boolean,
        "default": false
    },
    "failed": {
        "type": Boolean,
        "default": false
    },
    "active": {
        "type": Boolean,
        "default": true
    },
    "deleted": {
        "type": Boolean,
        "default": false
    },
    "created": {
        "by": {
            "type": Schema.Types.ObjectId,
            "ref": 'TXN_User',
            "default": null
        },
        "at": {
            "type": Date,
            "default": Date.now
        }
    },
    "updated": {
        "by": {
            "type": Schema.Types.ObjectId,
            "ref": 'TXN_User',
            "default": null
        },
        "at": {
            "type": Date,
            "default": Date.now
        }
    }
});

module.exports = mongoose.model('TXN_BatchJob', TXN_BatchJobSchema);
