var util = require('util');
var CronJob = require('../core/cron-job/cron-job.js');
var BatchJob = require('./batch-job.model.js');
var Audit = require('../core/audit.log.service.js');
var Exception = require('../core/exception.log.service.js');

module.exports = {
    get: function(query, callback) {
        if (typeof query.deleted == 'undefined') query.deleted = false;
        BatchJob
            .find(query)
            .populate({
                path: 'type',
                match: {
                    active: true,
                    deleted: false
                }
            })
            .exec(callback);
    },
    dataTable: function(query, callback) {
        var options = { conditions: query.conditions || {} };
        if (!options.conditions.deleted) options.conditions.deleted = false;
        if (!options.select) options.select = ['completed'];
        options.handlers = {
            'running': function(field, search, opt) {
                var value = search,
                    conds = {
                        'running': function() {
                            opt.conditions['running'] = true;
                        },
                        'completed': function() {
                            opt.conditions['completed'] = true;
                        },
                        'idle': function() {
                            opt.conditions['running'] = false;
                            opt.conditions['completed'] = false;
                        }
                    };
                if (opt.conditions && opt.conditions.running) delete opt.conditions.running;
                if (value.length == 1) {
                    conds[value[0]]();
                }
            }
        };
        BatchJob.dataTable(query.dataTableQuery, options, callback);
    },
    add: function(batchJobData, callback) {
        var by = { by: undefined };
        if (batchJobData.created) by.by = batchJobData.created.by || undefined;
        BatchJob.create(batchJobData, function(err, batchJob) {
            if (err) {
                console.log(err);
                Exception.log('BATCH_JOB', 'ADD', 'Batch Job add Error', err, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            Audit.log('BATCH_JOB', 'ADD', 'Batch Job added', batchJob, by.by, function(err) {
                if (err) console.log(err);
            });
            BatchJob.populate(batchJob, {
                path: 'type',
                match: {
                    active: true,
                    deleted: false
                }
            }, function(err, batchJobPopulated) {

                var cronTime = CronJob.timers[batchJobData.type.codeValue]['create']();

                if (!batchJobPopulated.configuration) batchJobPopulated.configuration = {};
                batchJobPopulated.configuration.cronTime = cronTime;

                batchJobPopulated.save(function(err, batchJobUpdated) {
                    if (err) {
                        console.log(err);
                        Exception.log('BATCH_JOB', 'UPDATE', 'Batch Job update Error', err, by.by, function(err) {
                            if (err) console.log(err);
                        });
                    }
                    Audit.log('BATCH_JOB', 'UPDATE', 'Batch Job updated', batchJobUpdated, by.by, function(err) {
                        if (err) console.log(err);
                    });

                    var job = CronJob.handlers[batchJobPopulated.type.codeValue](batchJobUpdated);
                    job.start();
                    return callback(null, batchJob);
                });
            });
        });
    },
    update: function(batchJobData, callback) {
        var by = { by: undefined };
        if (batchJobData.updated) by.by = batchJobData.updated.by || undefined;
        BatchJob.findByIdAndUpdate(batchJobData._id, batchJobData, function(err, batchJob) {
            if (err) {
                console.log(err);
                Exception.log('BATCH_JOB', 'UPDATE', 'Batch Job update Error', err, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            Audit.log('BATCH_JOB', 'UPDATE', 'Batch Job updated', batchJob, by.by, function(err) {
                if (err) console.log(err);
            });
            return callback(null, batchJob);
        });
    },
    remove: function(batchJobData, callback) {
        var by = { by: undefined };
        if (batchJobData.updated) by.by = batchJobData.updated.by || undefined;
        batchJobData.deleted = true;
        BatchJob.findByIdAndUpdate(batchJobData._id, batchJobData, function(err, batchJob) {
            if (err) {
                console.log(err);
                Exception.log('BATCH_JOB', 'DELETE', 'Batch Job delete Error', err, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            Audit.log('BATCH_JOB', 'DELETE', 'Batch Job deleted', batchJob, by.by, function(err) {
                if (err) console.log(err);
            });
            return callback(null, batchJob);
        });
    },
    restartIncompleteJobs: function() {
        console.log('Restarting all Incomplete Jobs...');
        var query = {
            completed: false,
            retryComplete: false,
            failed: false,
            active: true,
            deleted: false
        };
        this.get(query, function(err, batchJobs) {
            if (err) {
                console.log('FAILED ', err);
                return;
            }
            if (batchJobs.length == 0) {
                console.log('No incomplete jobs in the queue... OK');
                return;
            }
            batchJobs.forEach(function(batchJob) {
                var job = CronJob.handlers[batchJob.type.codeValue](batchJob, true);
                job.start();
            });
        });
    }
}
