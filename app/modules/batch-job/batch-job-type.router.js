var express = require('express');
var batchJobType = express.Router();

var batchJobTypeController = require('./batch-job-type.controller.js');

batchJobType.get('/', batchJobTypeController.get);
batchJobType.post('/', batchJobTypeController.add);
batchJobType.put('/', batchJobTypeController.update);
batchJobType.put('/delete', batchJobTypeController.remove);

module.exports = batchJobType;
