var coreQueryBuilderService = require('../core/core.query-builder.service.js');
var batchJobService = require('./batch-job.service.js');

module.exports = {
    get: function(req, res) {
        var query = coreQueryBuilderService.buildQuery(req);
        batchJobService.get(query, function(err, batchJobs) {
            if (err) return res.status(400).send(err);
            return res.send(batchJobs);
        });
    },
    dataTable: function(req, res) {
        var query = coreQueryBuilderService.buildQuery(req);
        batchJobService.dataTable(query, function(err, batchJobs) {
            if (err) return res.status(400).send(err);
            batchJobs.data.forEach(function(batchJob, index) {
                batchJob.running = batchJob.running ? 'Running' : (batchJob.completed ? 'Completed' : 'Idle');
                delete batchJob.completed;
                batchJobs.data[index] = batchJob;
            });
            return res.send(batchJobs);
        });
    },
    forceStartAllJobs: function(req, res) {
        batchJobService.restartIncompleteJobs();
        return res.send({ ok: 'ok' });
    },
    add: function(req, res) {
        batchJobService.add(req.body, function(err, batchJob) {
            if (err) return res.status(400).send(err);
            return res.send(batchJob);
        });
    },
    update: function(req, res) {
        batchJobService.update(req.body, function(err, batchJob) {
            if (err) return res.status(400).send(err);
            return res.send(batchJob);
        });
    },
    remove: function(req, res) {
        batchJobService.remove(req.body, function(err, batchJob) {
            if (err) return res.status(400).send(err);
            return res.send(batchJob);
        });
    }
};
