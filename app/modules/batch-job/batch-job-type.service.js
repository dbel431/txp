var BatchJobType = require('./batch-job-type.model.js');

module.exports = {
    get: function(query, callback) {
        if (typeof query.deleted == 'undefined') query.deleted = false;
        BatchJobType.find(query).exec(callback);
    },
    add: function(batchJobTypeData, callback) {
        var by = { by: undefined };
        if (batchJobTypeData.created) by.by = batchJobTypeData.created.by || undefined;
        BatchJobType.create(batchJobTypeData, function(err, batchJobType) {
            if (err) {
                console.log(err);
                Exception.log('BATCH_JOB_TYPE', 'ADD', 'Batch Job Type add Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            Audit.log('BATCH_JOB_TYPE', 'ADD', 'Batch Job Type added', batchJobType, by.by, function(err) { if(err) console.log(err); });
            return callback(null, batchJobType);
        });
    },
    update: function(batchJobType, callback) {
        var by = { by: undefined };
        if (batchJobTypeData.updated) by.by = batchJobTypeData.updated.by || undefined;
        BatchJobType.findByIdAndUpdate(batchJobType._id, batchJobType, function(err, job) {
            if (err) {
                console.log(err);
                Exception.log('BATCH_JOB_TYPE', 'UPDATE', 'Batch Job Type update Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            Audit.log('BATCH_JOB_TYPE', 'UPDATE', 'Batch Job Type updated', batchJobType, by.by, function(err) { if(err) console.log(err); });
            return callback(null, batchJobType);
        });
    },
    remove: function(batchJobType, callback) {
        var by = { by: undefined };
        if (batchJobTypeData.updated) by.by = batchJobTypeData.updated.by || undefined;
        batchJobTypeData.deleted = true;
        BatchJobType.findByIdAndUpdate(batchJobType._id, batchJobType, function(err, job) {
            if (err) {
                console.log(err);
                Exception.log('BATCH_JOB_TYPE', 'DELETE', 'Batch Job Type delete Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            Audit.log('BATCH_JOB_TYPE', 'DELETE', 'Batch Job Type deleted', batchJobType, by.by, function(err) { if(err) console.log(err); });
            return callback(null, batchJobType);
        });
    }
}
