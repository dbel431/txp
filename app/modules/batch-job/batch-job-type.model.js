var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TXN_BatchJobTypeSchema = new Schema({
    name: String,
    active: {
        type: Boolean,
        default: true
    },
    deleted: {
        type: Boolean,
        default: false
    },
    created: {
        by: {
            type: Schema.Types.ObjectId,
            ref: 'TXN_User',
            default: null
        },
        at: {
            type: Date,
            default: Date.now
        }
    },
    updated: {
        by: {
            type: Schema.Types.ObjectId,
            ref: 'TXN_User',
            default: null
        },
        at: {
            type: Date,
            default: Date.now
        }
    }
});

module.exports = mongoose.model('TXN_BatchJobType', TXN_BatchJobTypeSchema);
