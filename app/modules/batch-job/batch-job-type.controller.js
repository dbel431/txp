var coreQueryBuilderService = require('../core/core.query-builder.service.js');
var batchJobTypeService = require('./batch-job-type.service.js');

module.exports = {
    get: function(req, res) {
        var query = coreQueryBuilderService.buildQuery(req);
        batchJobTypeService.get(query, function(err, batchJobTypes) {
            if (err) return res.status(400).send(err);
            return res.send(batchJobTypes);
        });
    },
    add: function(req, res) {
        batchJobTypeService.add(req.body, function(err, batchJobType) {
            if (err) return res.status(400).send(err);
            return res.send(batchJobType);
        });
    },
    update: function(req, res) {
        batchJobTypeService.update(req.body, function(err, batchJobType) {
            if (err) return res.status(400).send(err);
            return res.send(batchJobType);
        });
    },
    remove: function(req, res) {
        batchJobTypeService.remove(req.body, function(err, batchJobType) {
            if (err) return res.status(400).send(err);
            return res.send(batchJobType);
        });
    }
};
