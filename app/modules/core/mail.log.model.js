var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ZZ_MailLogSchema = new Schema({
    mailType: String,
    from: String, // sender address
    to: String, // list of receivers
    subject: String, // Subject line
    text: String, // plaintext body
    html: String,
    delivered: Date,
    retry: Date,
    created: {
        by: {
            type: Schema.Types.ObjectId,
            ref: 'TXN_User',
            default: null
        },
        at: {
            type: Date,
            default: Date.now
        }
    }
});

module.exports = mongoose.model('ZZ_MailLog', ZZ_MailLogSchema);
