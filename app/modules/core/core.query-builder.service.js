module.exports = {
    buildQuery: function (req) {
        var query = {};
        if (req.query) {
            if (req.query.id) {
                query._id = req.query.id;
                delete req.query.id;
            }
            for (var i in req.query) {
                if (req.query[i]) query[i] = evaluate(req.query[i]);
                else query[i] = req.query[i];
            }
        }
        if (req.body) {
            if (req.body.id) {
                query._id = req.body.id;
                delete req.body.id;
            }
            for (var i in req.body) {
                if (req.body[i]) query[i] = evaluate(req.body[i]);
                else query[i] = req.body[i];
            }
        }
        if (query._id && query._id.constructor === Array) {
            query._id = {
                $in: query._id
            };
        }
        return query;
    }
}

function evaluate(object) {
    if (object && object.constructor === Array) {
        for (var i = 0; i < object.length; i++) {
            object[i] = evaluate(object[i]);
        }
    } else if (object && typeof object == 'object' && Object.keys(object).length > 0) {
        if (Object.keys(object).indexOf('_eval') < 0) {
            for (var key in object) {
                object[key] = evaluate(object[key]);
            }
        } else switch (object['_eval']) {
            case 'regex':
                {
                    object = new RegExp(RegExp.escape(object['value']), 'i');
                    break;
                }
        }
    }
    return object;
}
