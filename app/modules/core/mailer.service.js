var nodemailer = require('nodemailer');
var MailLog = require('./mail.log.model.js');

var poolConfig = {
    pool: true,
    host: 'smtp.gmail.com',
    port: 465,
    secure: true, // use SSL
    auth: {
        user: 'treasuredwork@gmail.com',
        pass: '112233@tw'
    }
};

var transporter = nodemailer.createTransport(poolConfig);


transporter.verify(function(error, success) {
    if (error) {
        console.log(error);
    } else {
        console.log('Server is ready to take our messages');
    }
});

module.exports = {
    sendMail: function(from, to, subject, text, html, type, by) {
        var mailOptions = {
            from: from, // sender address
            to: to, // list of receivers
            subject: subject, // Subject line
            text: text, // plaintext body
            html: html // html body
                // html: { path: '' } // html body
        };
        var mailData = {
            mailType: type,
            from: from, // sender address
            to: to, // list of receivers
            subject: subject, // Subject line
            text: text, // plaintext body
            html: html,
            created: { by: by, at: new Date() }
        };
        MailLog.create(mailData, function(err, mail) {
            if (err) {
                console.log('Mail Error', err);
                Exception.log('MAIL LOG', 'INSERT', 'Mail Log insert Error', err, by, function(err) {
                    if (err) console.log(err);
                });
                return false;
            }
            transporter.sendMail(mailOptions, function(err, info) {
                if (err) {
                    console.log('Mail Error', err);
                    Exception.log('MAIL', 'SEND', 'Mail send Error', err, by, function(err) {
                        if (err) console.log(err);
                    });
                    return false;
                } else {
                    mail.delivered = new Date();
                    mail.save(function(err) {
                        if (err) {
                            console.log('Mail Error', err);
                            Exception.log('MAIL', 'UPDATE', 'Mail Log update Error', err, by, function(err) {
                                if (err) console.log(err);
                            });
                        }
                    })
                }
            });
        });
    },
    retrySend: function(mailOptions, mail) {
        mail.retry = new Date();
        mail.save(function(err) {
            if (err) {
                console.log('Mail Error', err);
                Exception.log('MAIL', 'UPDATE', 'Mail Log update Error', err, by, function(err) {
                    if (err) console.log(err);
                });
                return false;
            }
            transporter.sendMail(mailOptions, function(err, info) {
                if (err) {
                    console.log('Mail Error', err);
                    Exception.log('MAIL', 'SEND', 'Mail send Error', err, by, function(err) {
                        if (err) console.log(err);
                    });
                    return false;
                } else {
                    mail.delivered = new Date();
                    mail.save(function(err) {
                        if (err) {
                            console.log('Mail Error', err);
                            Exception.log('MAIL', 'UPDATE', 'Mail Log update Error', err, by, function(err) {
                                if (err) console.log(err);
                            });
                        }
                    });
                }
            });
        });
    },
    getLog: function(query, callback) {
        MailLog.find(query)
            .populate({
                path: 'created.by',
                match: {
                    active: true,
                    deleted: false
                }
            })
            .exec(callback);
    }
};
