var FileLog = require('../core/file-log.model.js');

module.exports = {

    log: function(file, by, callback) {
        FileLog.create({
            file: file,
            created: {
                by: by
            }
        }, function(err, log) {
            if (err) {
                console.log(err);
                return callback(err);
            }
            return callback(false, log);
        });
    }

}
