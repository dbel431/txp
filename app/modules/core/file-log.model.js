var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TXN_FileLogSchema = new Schema({
    file: String,
    created: {
        by: {
            type: Schema.Types.ObjectId,
            ref: 'TXN_User',
            default: null
        },
        at: {
            type: Date,
            default: Date.now
        }
    }
});

module.exports = mongoose.model('TXN_FileLog', TXN_FileLogSchema);
