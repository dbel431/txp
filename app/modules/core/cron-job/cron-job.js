var CronJob = require('cron').CronJob;
var Audit = require('../audit.log.service.js');
var Exception = require('../exception.log.service.js');

var OrganizationController = require('../../organization/organization.controller.js');
var MailerService = require('../../core/mailer.service.js');
var WebUserService = require('../../web-user/web-user.service.js');

var timers = {
    'DIRECTORY DATA DOWNLOADS': {
        create: function() {
            var today = new Date();
            today.setHours(CRON_JOB_TIMERS.others.hours);
            today.setMinutes(CRON_JOB_TIMERS.others.minutes);
            today.setSeconds(0);
            return today;
        },
        expectedFinish: function(start) {
            return new Date(start + CRON_JOB_TIMERS.others.finishThreshold);
        },
        retry: function() {
            return new Date(Date.now() + CRON_JOB_TIMERS.others.retryThreshold);
        }
    },
    'MAIL RETRY': {
        create: function() {
            return CRON_JOB_TIMERS.scheduled.time;
        },
        expectedFinish: function(start) {
            return new Date(start + CRON_JOB_TIMERS.scheduled.finishThreshold);
        },
        retry: function() {
            return new Date(Date.now() + CRON_JOB_TIMERS.scheduled.retryThreshold);
        }
    },
    'BULK MAIL': {
        create: function() {
            var today = new Date();
            today.setHours(CRON_JOB_TIMERS.others.hours);
            today.setMinutes(CRON_JOB_TIMERS.others.minutes);
            today.setSeconds(0);
            return today;
        },
        expectedFinish: function(start) {
            return new Date(start + CRON_JOB_TIMERS.others.finishThreshold);
        },
        retry: function() {
            return new Date(Date.now() + CRON_JOB_TIMERS.others.retryThreshold);
        }
    }
};

module.exports = {
    timers: timers,
    handlers: {
        'DIRECTORY DATA DOWNLOADS': function(batchJob, retry) {
            var res = {
                responseCode: 200,
                response: {},
                job: null,
                status: function(responseCode) {
                    this.responseCode = responseCode;
                    return this;
                },
                send: function(obj) {
                    console.log('JOB ERROR: ', obj);
                    if (this.responseCode != 200) this.job.stop();
                    return this;
                }
            };
            var req = {};
            var cronTime = batchJob.configuration ? new Date(batchJob.configuration.cronTime) : timers[batchJob.type.codeValue]['create']();
            if (retry || cronTime.getTime() < Date.now()) cronTime = timers[batchJob.type.codeValue]['retry']();

            var job = new CronJob({
                cronTime: cronTime,
                onTick: function() {
                    console.log('JOB STARTED');
                    batchJob.running = true;
                    batchJob.runAt = cronTime;
                    batchJob.configuration.expectedFinish = timers['DIRECTORY DATA DOWNLOADS']['expectedFinish'](cronTime);
                    batchJob.completed = false;
                    batchJob.updated = {
                        at: Date.now()
                    };
                    batchJob.save(function(err) {
                        if (err) {
                            console.log(err);
                            Exception.log('BATCH_JOB', 'UPDATE', 'Batch Job update Error', err, undefined, function(err) {
                                if (err) console.log(err);
                            });
                            return false;
                        }
                        Audit.log('BATCH_JOB', 'UPDATE', 'Batch Job updated', batchJob, undefined, function(err) {
                            if (err) console.log(err);
                        });

                        var exportConfig = {
                            select: batchJob.data.selectedFields,
                            where: {
                                directoryId: batchJob.data.directoryId
                            }
                        };
                        req = { query: exportConfig };
                        res.job = job;
                        OrganizationController.exportToExcelCount(req, res, function() {
                            OrganizationController.exportToExcel(req, res, function() {
                                job.stop();
                            })
                        });
                    });
                },
                onComplete: function() {
                    console.log('JOB COMPLETE');
                    batchJob.running = false;
                    batchJob.completed = true;
                    batchJob.completedAt = Date.now();
                    if (retry) batchJob.retryComplete = true;
                    batchJob.result = {
                        errors: req.resData.exportErrors,
                        filename: req.resData.csvFile
                    };
                    if (req.resData.exportErrors) batchJob.failed = (req.resData.exportErrors.length > 0);
                    batchJob.updated = {
                        at: Date.now()
                    };
                    batchJob.save(function(err) {
                        if (err) {
                            console.log(err);
                            Exception.log('BATCH_JOB', 'UPDATE', 'Batch Job update Error', err, undefined, function(err) {
                                if (err) console.log(err);
                            });
                            return false;
                        }
                        Audit.log('BATCH_JOB', 'UPDATE', 'Batch Job updated', batchJob, undefined, function(err) {
                            if (err) console.log(err);
                        });
                    });
                }
            });
            return job;
        },
        'MAIL RETRY': function(batchJob, retry) {
            var cronTime = batchJob.configuration ? batchJob.configuration.interval : timers[batchJob.type.codeValue]['create']();

            var job = new CronJob({
                cronTime: cronTime,
                onTick: function() {
                    console.log('JOB STARTED');
                    batchJob.running = true;
                    batchJob.runAt = cronTime;
                    batchJob.configuration.expectedFinish = timers['MAIL RETRY']['expectedFinish'](cronTime);
                    batchJob.completed = false;
                    batchJob.updated = {
                        at: Date.now()
                    };
                    batchJob.save(function(err) {
                        if (err) {
                            console.log(err);
                            Exception.log('BATCH_JOB', 'UPDATE', 'Batch Job update Error', err, undefined, function(err) {
                                if (err) console.log(err);
                            });
                            return false;
                        }
                        Audit.log('BATCH_JOB', 'UPDATE', 'Batch Job updated', batchJob, undefined, function(err) {
                            if (err) console.log(err);
                        });

                        MailerService.getLog({
                            delivered: { $exists: false },
                            retry: { $exists: false }
                        }, function(err, mails) {
                            if (err) {
                                console.log(err);
                                Exception.log('MAIL LOG', 'GET', 'Mail Log get Error', err, undefined, function(err) {
                                    if (err) console.log(err);
                                });
                                return false;
                            }
                            mails.forEach(function(mail) {
                                MailerService.retrySend({
                                    from: mail.from, // sender address
                                    to: mail.to, // list of receivers
                                    subject: mail.subject, // Subject line
                                    text: mail.text, // plaintext body
                                    html: mail.html
                                }, mail);
                            });
                        });

                    });
                },
                onComplete: function() {
                    console.log('JOB COMPLETE');
                    batchJob.running = false;
                    batchJob.completed = true;
                    batchJob.completedAt = Date.now();
                    if (retry) batchJob.retryComplete = true;
                    batchJob.updated = {
                        at: Date.now()
                    };
                    batchJob.save(function(err) {
                        if (err) {
                            console.log(err);
                            Exception.log('BATCH_JOB', 'UPDATE', 'Batch Job update Error', err, undefined, function(err) {
                                if (err) console.log(err);
                            });
                            return false;
                        }
                        Audit.log('BATCH_JOB', 'UPDATE', 'Batch Job updated', batchJob, undefined, function(err) {
                            if (err) console.log(err);
                        });
                    });
                }
            });
            return job;
        },
        'BULK MAIL': function(batchJob, retry) {
            var cronTime = batchJob.configuration ? new Date(batchJob.configuration.cronTime) : timers[batchJob.type.codeValue]['create']();
            if (retry || cronTime.getTime() < Date.now()) cronTime = timers[batchJob.type.codeValue]['retry']();

            var job = new CronJob({
                cronTime: cronTime,
                onTick: function() {
                    console.log('JOB STARTED');
                    batchJob.running = true;
                    batchJob.runAt = cronTime;
                    batchJob.configuration.expectedFinish = timers['BULK MAIL']['expectedFinish'](cronTime);
                    batchJob.completed = false;
                    batchJob.updated = {
                        at: Date.now()
                    };
                    batchJob.save(function(err) {
                        if (err) {
                            console.log(err);
                            Exception.log('BATCH_JOB', 'UPDATE', 'Batch Job update Error', err, undefined, function(err) {
                                if (err) console.log(err);
                            });
                            return false;
                        }
                        Audit.log('BATCH_JOB', 'UPDATE', 'Batch Job updated', batchJob, undefined, function(err) {
                            if (err) console.log(err);
                        });

                        var query = {
                            directoryId: batchJob.data.directoryId,
                            active: true,
                            deleted: false
                        };
                        WebUserService.get(query, function(err, webUsers) {
                            if (err) {
                                console.log(err);
                                Exception.log('WEB USER', 'GET', 'Web User get Error', err, undefined, function(err) {
                                    if (err) console.log(err);
                                });
                                return false;
                            }

                            webUsers.forEach(function(webUser) {
                                MailerService.sendMail(batchJob.data.from, webUser.auth.email, batchJob.data.subject, '', batchJob.data.body);
                            });

                            job.stop();

                        });

                    });
                },
                onComplete: function() {
                    console.log('JOB COMPLETE');
                    batchJob.running = false;
                    batchJob.completed = true;
                    batchJob.completedAt = Date.now();
                    if (retry) batchJob.retryComplete = true;
                    batchJob.updated = {
                        at: Date.now()
                    };
                    batchJob.save(function(err) {
                        if (err) {
                            console.log(err);
                            Exception.log('BATCH_JOB', 'UPDATE', 'Batch Job update Error', err, undefined, function(err) {
                                if (err) console.log(err);
                            });
                            return false;
                        }
                        Audit.log('BATCH_JOB', 'UPDATE', 'Batch Job updated', batchJob, undefined, function(err) {
                            if (err) console.log(err);
                        });
                    });
                }
            });
            return job;
        }
    }
};
