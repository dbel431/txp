var XMLStructure = require('./xml.structure.model.js');
var Audit = require('../core/audit.log.service.js');
var Exception = require('../core/exception.log.service.js');

module.exports = {
    get: function(query, callback) {
        var select, limit, sort = { level: 1 };
        if (query._select) select = query._select;
        if (query._limit) limit = query._limit;
        if (query._sort) sort = query._sort;
        if (query._where) query = query._where;
        if (typeof query.deleted == 'undefined') query.deleted = false;
        var dbQuery = XMLStructure
            .find(query)
            .populate({ path: 'directoryId', match: { active: true, deleted: false } })
            .populate({ path: 'parentNode', match: { active: true, deleted: false } })
            .populate({ path: 'replicateNode', match: { active: true, deleted: false } });
        if (select) dbQuery.select(select);
        if (sort) dbQuery.sort(sort);
        if (limit) dbQuery.limit(limit);
        dbQuery.exec(callback);
    },
    add: function(xmlStructureData, callback) {
        var by = { by: undefined };
        if (xmlStructureData.created) by.by = xmlStructureData.created.by || undefined;
        XMLStructure.create(xmlStructureData, function(err, xmlStructure) {
            if (err) {
                console.log(err);
                Exception.log('XML STRUCTURE', 'ADD', 'XML Structure add Error', err, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            if (!xmlStructure) return callback(null, false, { message: 'XML Structure not found' });
            Audit.log('XML STRUCTURE', 'ADD', 'XML Structure added', xmlStructure, by.by, function(err) {
                if (err) console.log(err);
            });
            return callback(null, xmlStructure);
        });
    },
    addMany: function(xmlStructureData, callback) {
        XMLStructure.insertMany(xmlStructureData, function(err, xmlStructure) {
            if (err) return callback(err);
            if (!xmlStructure) return callback(null, false, { message: 'XML Structure not found' });
            return callback(null, xmlStructure);
        });
    },
    dataTable: function(query, callback) {
        var options = { conditions: query.conditions || {} };
        if (!options.conditions.deleted) options.conditions.deleted = false;
        XMLStructure.dataTable(query, options, callback);
    },
    update: function(xmlStructureData, callback) {
        var by = { by: undefined };
        if (xmlStructureData.updated) by.by = xmlStructureData.updated.by || undefined;
        XMLStructure.findByIdAndUpdate(xmlStructureData._id, xmlStructureData, function(err, xmlStructure) {
            if (err) {
                console.log(err);
                Exception.log('XML STRUCTURE', 'UPDATE', 'XML Structure Update Error', err, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            Audit.log('XML STRUCTURE', 'UPDATE', 'XML Structure Updated', xmlStructure, by.by, function(err) {
                if (err) console.log(err);
            });
            return callback(null, xmlStructure);
        });
    },
    remove: function(xmlStructureData, callback) {
        var by = { by: undefined };
        if (xmlStructureData.updated) by.by = xmlStructureData.updated.by || undefined;
        xmlStructureData.deleted = true;
        XMLStructure.findByIdAndUpdate(xmlStructureData._id, xmlStructureData, function(err, xmlStructure) {
            if (err) {
                console.log(err);
                Exception.log('XML STRUCTURE', 'DELETE', 'XMLStructure Delete Error', err, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            Audit.log('XML STRUCTURE', 'DELETE', 'XMLStructure Deleted', xmlStructure, by.by, function(err) {
                if (err) console.log(err);
            });
            return callback(null, xmlStructure);
        });
    }
};
