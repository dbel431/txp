var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var TXN_XMLStructureSchema = new Schema({
    "directoryId": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "directoryName": String,
    "nodeId": Number,
    "label": String,
    "name": String,
    "nodeType": {
        "type": String,
        "enum": ['structural', 'data']
    },
    "patternId": String,
    "level": Number,
    "parentNodeId": Number,
    "parentNode": {
        "type": Schema.Types.ObjectId,
        "ref": 'TXN_XMLStructure'
    },
    "sequenceNo": Number,
    "attributes": [{
        "key": String,
        "value": String
    }],
    "replicateNode": {
        "type": Schema.Types.ObjectId,
        "ref": 'TXN_XMLStructure'
    },
    "replicateNodeId": Number,
    "repeatFor": String,
    "repeatConditions": [{
        "key": String,
        "value": Schema.Types.Mixed
    }],
    "data": {
        "model": String,
        "valuePath": String,
        "isNested": Boolean
    },
    "active": {
        type: Boolean,
        default: true
    },
    "deleted": {
        type: Boolean,
        default: false
    },
    "created": {
        "by": {
            "type": Schema.Types.ObjectId,
            "ref": 'TXN_User'
        },
        "at": {
            type: Date,
            default: Date.now
        }
    },
    "updated": {
        "by": {
            "type": Schema.Types.ObjectId,
            "ref": 'TXN_User'
        },
        "at": {
            type: Date,
            default: Date.now
        }
    }
});

module.exports = mongoose.model('TXN_XMLStructure', TXN_XMLStructureSchema);
