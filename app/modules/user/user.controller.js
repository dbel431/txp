var userService = require('./user.service.js');
var coreQueryBuilderService = require('../core/core.query-builder.service.js');

function sanitizeUser(user) {
    if (user.constructor === Array) {
        for (var i in user) {
            user[i] = sanitizeUser(user[i]);
        }
    } else {
        if (user.user && user.user.auth) {
            user.user = sanitizeUser(user.user);
        }
        if (user.created && ('by' in user.created) && user.created.by && user.created.by.auth) {
            user.created.by = sanitizeUser(user.created.by);
        }
        if (user.updated && ('by' in user.updated) && user.updated.by && user.updated.by.auth) {
            user.updated.by = sanitizeUser(user.updated.by);
        }
        if (user.auth) {
            user.auth.password = undefined;
            delete user.auth.password;
            if (user.auth.securityQuestion) {
                user.auth.securityQuestion.answer = undefined;
                delete user.auth.securityQuestion.answer;
            }
        }
    }
    return user;
}

module.exports = {
    token: function(req, res) {
        var token = req.body;
        userService.token(token, function(err, user, info) {
            if (err) return res.status(400).send(err);
            if (!user) return res.status(403).send(info);
            return res.send(sanitizeUser(user));
        });
    },
    resetPasswordToken: function(req, res) {
        var resetPasswordToken = req.body;
        userService.resetPasswordToken(resetPasswordToken, function(err, user, info) {
            if (err) return res.status(400).send(err);
            if (!user) return res.status(403).send(info);
            return res.send(sanitizeUser(user));
        });
    },
    login: function(req, res) {
        var credentials = req.body;
        userService.login(credentials, function(err, user, info) {
            if (err) return res.status(400).send(err);
            if (!user) return res.status(403).send(info);
            return res.send(sanitizeUser(user));
        });
    },
    resetPassword: function(req, res) {
        var credentials = req.body;
        userService.resetPassword(credentials, function(err, user, info) {
            if (err) return res.status(400).send(err);
            if (!user) return res.status(403).send(info);
            return res.send(sanitizeUser(user));
        });
    },
    resetPasswordManual: function(req, res) {
        var user = req.body;
        userService.resetPasswordManual(user, function(err, user, info) {
            if (err) return res.status(400).send(err);
            if (!user) return res.status(403).send(info);
            return res.send(sanitizeUser(user));
        });
    },
    logout: function(req, res) {
        var token = req.body;
        userService.logout(token, function(err, loggedout) {
            if (err) return res.status(400).send(err);
            return res.send(loggedout);
        });
    },
    get: function(req, res) {
        var query = coreQueryBuilderService.buildQuery(req);
        userService.get(query, function(err, users, info) {
            if (err) return res.status(400).send(err);
            return res.send(sanitizeUser(users));
        });
    },
    dataTable: function(req, res) {
        var query = coreQueryBuilderService.buildQuery(req);
        userService.dataTable(query, function(err, users) {
            if (err) return res.status(400).send(err);
            return res.send(users);
        });
    },
    add: function(req, res) {
        userService.add(req.body, function(err, user, info) {
            if (err) return res.status(400).send(err);
            if (!user) return res.status(403).send(info);
            return res.send(sanitizeUser(user));
        });
    },
    update: function(req, res) {
        userService.update(req.body, function(err, user, info) {
            if (err) return res.status(400).send(err);
            if (!user) return res.status(403).send(info);
            return res.send(sanitizeUser(user));
        });
    },
    remove: function(req, res) {
        userService.remove(req.body, function(err, user, info) {
            if (err) return res.status(400).send(err);
            if (!user) return res.status(403).send(info);
            return res.send(sanitizeUser(user));
        });
    },
    verifySecurityQuestion: function(req, res) {
        userService.verifySecurityQuestion(req.body, function(err, user, info) {
            if (err) return res.status(400).send(err);
            if (!user) return res.status(403).send(info);
            return res.send(sanitizeUser(user));
        });
    }
};
