var bcrypt = require('bcrypt-nodejs');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TXN_UserTokensSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'TXN_User'
    },
    token: {
        type: String,
        unique: true
    },
    secret: String,
    remember: Boolean,
    expireAt: {
        type: Date,
        default: Date.now,
        index: true,
        expires: 1
    }
});

// methods ======================
// generating a hash
TXN_UserTokensSchema.methods.generateHash = function(secret) {
    var token = bcrypt.hashSync((secret + this.user), bcrypt.genSaltSync(8), null);
    return token;
};

// refresh token ttl
TXN_UserTokensSchema.methods.newTokenTTL = function() {
    return (new Date((new Date()).getTime() + (this.remember ? (14 * 24 * 60 * 60 * 1000) : (30 * 60 * 1000))));
};

// checking if token is valid
TXN_UserTokensSchema.methods.validToken = function(secret) {
    var valid = bcrypt.compareSync((secret + this.user._id), this.token);
    return valid;
};

module.exports = mongoose.model('TXN_UserTokens', TXN_UserTokensSchema);
