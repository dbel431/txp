var express = require('express');
var user = express.Router();

var userController = require('./user.controller.js');

user.post('/token', userController.token);
user.post('/reset-password-token', userController.resetPasswordToken);
user.post('/login', userController.login);
user.post('/reset-password', userController.resetPassword);
user.put('/reset-password-manual', userController.resetPasswordManual);
user.post('/verify-security-question', userController.verifySecurityQuestion);
user.post('/logout', userController.logout);
user.get('/', userController.get);
user.post('/data-table', userController.dataTable);
user.post('/', userController.add);
user.put('/', userController.update);
user.put('/delete', userController.remove);
module.exports = user;
