var User = require('./user.model.js');
var UserTokens = require('./user.tokens.model.js');
var UserResetPasswordTokens = require('./user.reset-password-tokens.model.js');
var UserPersonalDetails = require('./user.personal.details.model.js');
var Audit = require('../core/audit.log.service.js');
var Exception = require('../core/exception.log.service.js');
var AccessLog = require('../core/access.log.service.js');
var Mailer = require('../core/mailer.service.js');

module.exports = {
    token: function(userToken, callback) {
        UserTokens.findOne({
            token: userToken.token
        }).populate({
            path: 'user',
            match: {
                active: true,
                deleted: false
            },
            populate: [{
                path: 'role',
                match: {
                    active: true,
                    deleted: false
                }
            }, {
                path: 'directory',
                match: {
                    active: true,
                    deleted: false
                }
            }]
        }).sort({ 'auth.email': 1, _id: 1 }).exec(function(err, token) {
            if (err) {
                console.log(err);
                return callback(err);
            }
            if (!(token && token.user && token.validToken(userToken.secret))) return callback(null, false, {
                message: 'Invalid/Expired Token'
            });
            if ((new Date(token.user.accessDate)) > Date.now()) return callback(null, false, {
                message: 'Access is yet to be given to you! Please contact the Administrator!'
            });
            if ((new Date(token.user.expiryDate)) < Date.now()) return callback(null, false, {
                message: 'Access has been expired! Please contact the Administrator!'
            });
            token.expireAt = token.newTokenTTL();
            token.save(function(err) {
                if (err) {
                    console.log(err);
                    return callback(err);
                }
                return callback(null, token.user);
            });
        });
    },
    resetPasswordToken: function(userToken, callback) {
        UserResetPasswordTokens.findOne({
            token: userToken.token
        }).populate({
            path: 'user',
            match: {
                active: true,
                deleted: false
            }
        }).exec(function(err, token) {
            if (err) {
                console.log(err);
                return callback(err);
            }
            if (!(token && token.user && token.validToken(userToken.secret))) return callback(null, false, {
                message: 'Invalid/Expired Password Reset Token'
            });
            token.expireAt = token.newTokenTTL();
            token.save(function(err) {
                if (err) {
                    console.log(err);
                    return callback(err);
                }
                return callback(null, token.user);
            });
        });
    },
    login: function(credentials, callback) {
        User.findOne({
            'auth.email': credentials.email,
            'active': true,
            'deleted': false
        }, function(err, user) {
            if (err) {
                console.log(err);
                return callback(err);
            }
            if (!user) return callback(null, false, {
                message: 'Incorrect Username'
            });
            if (!user.validPassword(credentials.password)) return callback(null, false, {
                message: 'Incorrect Password'
            });
            if ((new Date(user.accessDate)) > Date.now()) return callback(null, false, {
                message: 'Access is yet to be given to you! Please contact the Administrator!'
            });
            if ((new Date(user.expiryDate)) < Date.now()) return callback(null, false, {
                message: 'Access has been expired! Please contact the Administrator!'
            });
            UserTokens.remove({
                user: user._id,
                secret: credentials.secret
            }, function(err) {
                if (err) {
                    console.log(err);
                    return callback(err);
                }
                var newToken = {
                    user: user._id,
                    secret: credentials.secret,
                    remember: credentials.remember
                };
                UserTokens.create(newToken, function(err, token) {
                    if (err) {
                        console.log(err);
                        return callback(err);
                    }
                    if (!token) return callback(null, false, {
                        message: 'Could not generate token!'
                    });
                    token.token = token.generateHash(credentials.secret);
                    token.expireAt = token.newTokenTTL();
                    token.save(function(err) {
                        if (err) {
                            Exception.log('USER', 'LOGIN', 'User login Error', err, token.user, function(err) {
                                if (err) console.log(err);
                            });
                            return callback(err);
                        }

                        AccessLog.log(token.user, 'LOGIN', function(err) {
                            if (err) console.log(err);
                        });

                        return callback(null, {
                            email: user.auth.email,
                            token: token.token
                        });
                    });
                });
            });
        });
    },
    resetPassword: function(credentials, callback) {
        User.findOne({
            'auth.email': credentials.email,
            'active': true,
            'deleted': false
        }, function(err, user) {
            if (err) {
                console.log(err);
                Exception.log('USER', 'GET', 'User get Error', err, undefined, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            if (!user) return callback(null, false, { message: 'No user found' });
            var randomPassword = user.randomPassword();
            user.auth.password = user.generateHash(randomPassword);
            user.save(function(err, user) {
                if (err) {
                    console.log(err);
                    Exception.log('USER', 'GET', 'User get Error', err, undefined, function(err) {
                        if (err) console.log(err);
                    });
                    return callback(err);
                }
                Audit.log('USER', 'RESET_PASSWORD', 'User password reset', user, undefined, function(err) {
                    if (err) console.log(err);
                });

                Mailer.sendMail("Treasured Works: Directory Management System", user.auth.email, 'PASSWORD RESET', '', '<h2><p>Your new Password :' + randomPassword + ' </p></h2><p>Regards </p><p>Treasured Works </p>');

                return callback(null, user);
            });
        });
    },
    resetPasswordManual: function(userData, callback) {
        User.findById(userData._id, function(err, user) {
            if (err) {
                console.log(err);
                Exception.log('USER', 'GET', 'User get Error', err, undefined, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            if (!user) return callback(null, false, { message: 'No user found' });
            user.auth.password = user.generateHash(userData.auth.password);
            user.save(function(err, user) {
                if (err) {
                    console.log(err);
                    Exception.log('USER', 'GET', 'User get Error', err, undefined, function(err) {
                        if (err) console.log(err);
                    });
                    return callback(err);
                }
                Audit.log('USER', 'RESET_PASSWORD_MANUAL', 'User password reset manually', user, undefined, function(err) {
                    if (err) console.log(err);
                });

                // Mailer.sendMail("Treasured Works 👥", user.auth.email, 'PASSWORD RESET MANUALLY ✔', '', '<h2><p>Your new Password :' + userData.auth.password + ' </p></h2><p>Regards </p><p>Treasured Works </p>');

                return callback(null, user);
            });
        });
    },
    logout: function(userToken, callback) {
        UserTokens.findOne({
            token: userToken.token
        }).exec(function(err, token) {
            if (err) {
                console.log(err);
                Exception.log('USER_TOKEN', 'GET', 'User token get Error', err, token.user, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            if (!token) return callback(null, false, { message: 'No token found' });
            UserTokens.remove({
                token: userToken.token
            }, function(err) {
                if (err) {
                    console.log(err);
                    Exception.log('USER_TOKEN', 'DELETE', 'User token delete Error', err, token.user, function(err) {
                        if (err) console.log(err);
                    });
                    return callback(err);
                }
                AccessLog.log(token.user, 'LOGOUT', function(err) {
                    if (err) console.log(err);
                });
                return callback(null, {
                    loggedOut: true
                });
            });
        });
    },
    get: function(query, callback) {
        if (typeof query.deleted == 'undefined') query.deleted = false;
        User.find(query)
            .populate('role')
            .populate('directory')
            .populate('created.by')
            .populate('auth.securityQuestion.question')
            .exec(callback);
    },
    dataTable: function(query, callback) {
        var util = require('util');
        var options = { conditions: query.conditions || {} };
        if (!options.conditions.deleted) options.conditions.deleted = false;
        options.handlers = {
            'details.personal': function(field, search, opt) {
                var value = [];
                search.forEach(function(chunks) {
                    chunks.split(' ').forEach(function(chunk) {
                        if (util.isRegExp(chunk)) {
                            value.push(chunk);
                        } else {
                            value.push(new RegExp(RegExp.escape(chunk.trim()), 'i'));
                        }
                    });
                });
                options.conditions = options.conditions || {};
                options.conditions['$or'] = options.conditions['$or'] || [];
                options.conditions['$or'].push({ 'details.personal.name.first': { '$in': value } });
                options.conditions['$or'].push({ 'details.personal.name.middle': { '$in': value } });
                options.conditions['$or'].push({ 'details.personal.name.last': { '$in': value } });
            },
            'auth.email': function(field, search, opt) {
                var value = [];
                search.forEach(function(chunks) {
                    chunks.split(' ').forEach(function(chunk) {
                        if (util.isRegExp(chunk)) {
                            value.push(chunk);
                        } else {
                            value.push(new RegExp(RegExp.escape(chunk.trim()), 'i'));
                        }
                    });
                });
                options.conditions = options.conditions || {};
                options.conditions['$or'] = options.conditions['$or'] || [];
                options.conditions['$or'].push({ 'auth.email': { '$in': value } });
            }
        };
        User.dataTable(query, options, callback);
    },
    add: function(userData, callback) {
        var by = { by: undefined };
        if (userData.created) by.by = userData.created.by || undefined;
        var user = new User(userData);
        user.auth.password = user.auth.password = user.generateHash(userData.auth.password);
        if (userData.auth.securityQuestion && userData.auth.securityQuestion.answer)
            userData.auth.securityQuestion.answer = user.generateHash(userData.auth.securityQuestion.answer);
        user.save(function(err, user) {
            if (err) {
                console.log(err);
                Exception.log('USER', 'ADD', 'User add Error', err, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            if (!user) return callback(null, false, { message: 'No user found' })
            Audit.log('USER', 'ADD', 'User added', user, by.by, function(err) {
                if (err) console.log(err);
            });

            Mailer.sendMail("Treasured Works: Directory Management System", user.auth.email, 'Welcome to Treasured Works: Directory Management System', '', '<h2>Greetings user!</h2><p>Welcome to Treasured Works: Directory Management System</p><p>Your Username :' + user.auth.email + ' </p><p>Your Password :' + userData.auth.password + ' </p> <p>Regards </p><p>Treasured Works </p>');

            return callback(null, user);
        });
    },
    update: function(userData, callback) {
        var by = { by: undefined };
        if (userData.updated) by.by = userData.updated.by || undefined;
        User.findById(userData._id, function(err, user) {
            if (err) {
                console.log(err);
                Exception.log('USER', 'GET', 'User get Error', err, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            if (!user) return callback(null, false, { message: 'No user found' });
            if (userData.auth) {
                if (userData.auth.email) user.auth.email = userData.auth.email;
                if (userData.auth.password) user.auth.password = user.generateHash(userData.auth.password.trim());
                if (userData.auth.securityQuestion && userData.auth.securityQuestion.question)
                    user.auth.securityQuestion.question = userData.auth.securityQuestion.question;
                if (userData.auth.securityQuestion && userData.auth.securityQuestion.answer)
                    user.auth.securityQuestion.answer = user.generateHash(userData.auth.securityQuestion.answer);
            }
            if (userData.avatar) user.avatar = userData.avatar;
            if (userData.role) user.role = userData.role;
            if (userData.details) user.details = userData.details;
            if (userData.accessDate) user.accessDate = userData.accessDate;
            if (userData.expiryDate) user.expiryDate = userData.expiryDate;
            if (userData.quickLinks) user.quickLinks = userData.quickLinks;
            if (userData.directory) user.directory = userData.directory;
            if (typeof userData.active != 'undefined') user.active = userData.active;
            if (by.by) user.updated.by = by.by;
            user.updated.at = new Date();
            user.save(function(err, user) {
                if (err) {
                    console.log(err);
                    Exception.log('USER', 'UPDATE', 'User update Error', err, by.by, function(err) {
                        if (err) console.log(err);
                    });
                    return callback(err);
                }
                Audit.log('USER', 'UPDATE', 'User updated', user, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(null, user);
            });
        });
    },
    remove: function(userData, callback) {
        var by = { by: undefined };
        if (userData.updated) by.by = userData.updated.by || undefined;
        userData.deleted = true;
        User.findByIdAndUpdate(userData._id, userData, function(err, user) {
            if (err) {
                console.log(err);
                Exception.log('USER', 'DELETE', 'User delete Error', err, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            Audit.log('USER', 'DELETE', 'User deleted', user, by.by, function(err) {
                if (err) console.log(err);
            });
            return callback(null, user);
        });
    },
    verifySecurityQuestion: function(userData, callback) {
        User.findById(userData._id, function(err, user) {
            if (err) {
                console.log(err);
                Exception.log('USER', 'GET', 'User get Error', err, undefined, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            if (!user.validStringHash(userData.auth.securityQuestion.answer, user.auth.securityQuestion.answer))
                return callback(null, false, { message: 'Answer Incorrect!' });
            UserResetPasswordTokens.remove({
                user: user._id,
                secret: userData.secret
            }, function(err) {
                if (err) {
                    console.log(err);
                    return callback(err);
                }
                var newToken = {
                    user: user._id,
                    secret: userData.secret
                };
                UserResetPasswordTokens.create(newToken, function(err, token) {
                    if (err) {
                        console.log(err);
                        return callback(err);
                    }
                    if (!token) return callback(null, false, {
                        message: 'Could not generate token!'
                    });
                    token.token = token.generateHash(userData.secret);
                    token.expireAt = token.newTokenTTL();
                    token.save(function(err) {
                        if (err) {
                            Exception.log('USER', 'RESET_PASSWORD_TOKEN', 'User reset password token Error', err, token.user, function(err) {
                                if (err) console.log(err);
                            });
                            return callback(err);
                        }

                        return callback(null, {
                            token: token.token
                        });
                    });
                });
            });
        });
    }
};
