var bcrypt = require('bcrypt-nodejs');
var randomstring = require("randomstring");
var mongoose = require('mongoose');
var TXN_UserPersonalDetailsSchema = require('./user.personal.details.model.js')
var Schema = mongoose.Schema;

var TXN_UserSchema = new Schema({
    avatar: String,
    auth: {
        email: {
            type: String,
            required: true,
            lowercase: true
        },
        password: {
            type: String,
            required: true
        },
        securityQuestion: {
            question: {
                type: Schema.Types.ObjectId,
                ref: 'MST_SecurityQuestion'
            },
            answer: String
        }
    },
    role: {
        type: Schema.Types.ObjectId,
        ref: 'TXN_Role'
    },
    details: {
        // personalization: {
        //     theme: String
        // },
        personal: TXN_UserPersonalDetailsSchema
    },
    directory: {
        type: Schema.Types.ObjectId,
        ref: 'MST_RefCodeValue'
    },
    accessDate: Date,
    expiryDate: Date,
    quickLinks: Schema.Types.Mixed,
    active: {
        type: Boolean,
        default: true
    },
    deleted: {
        type: Boolean,
        default: false
    },
    created: {
        by: {
            type: Schema.Types.ObjectId,
            ref: 'TXN_User'
        },
        at: {
            type: Date,
            default: Date.now
        }
    },
    updated: {
        by: {
            type: Schema.Types.ObjectId,
            ref: 'TXN_User'
        },
        at: {
            type: Date,
            default: Date.now
        }
    }
});

TXN_UserSchema.index({ 'auth.email': 1, _id: 1 });

// methods ======================
// generating a hash
TXN_UserSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if hash is valid
TXN_UserSchema.methods.validStringHash = function(string, hash) {
    return bcrypt.compareSync(string, hash);
};

// checking if password is valid
TXN_UserSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.auth.password);
};

// randomize password
TXN_UserSchema.methods.randomPassword = function() {
    String.prototype.replaceAt = function(index, character) {
        return this.substr(0, index) + character + this.substr(index + character.length);
    }
    var newString = randomstring.generate(10);
    newString = newString.replaceAt(4, "$");
    newString = newString.replaceAt(8, "*");
    // newString[8] = "*";
    return newString;
};


module.exports = mongoose.model('TXN_User', TXN_UserSchema);
