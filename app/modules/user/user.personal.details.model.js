var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TXN_UserPersonalDetailsSchema = new Schema({
    salute: {
        type: String,
        lowercase: true,
        enum: ['mr', 'ms', 'mrs', 'dr']
    },
    name: {
        first: String,
        middle: String,
        last: String
    },
    dob: Date,
    gender: {
        type: String,
        lowercase: true,
        enum: ['male', 'female', 'transgender']
    }
});

// module.exports = mongoose.model('TXN_UserPersonalDetails', TXN_UserPersonalDetailsSchema);
module.exports = TXN_UserPersonalDetailsSchema;
