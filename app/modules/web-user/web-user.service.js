var WebUser = require('./web-user.model.js');
var WebUserTokens = require('./web-user.tokens.model.js');
var Mailer = require('../core/mailer.service.js');
var Audit = require('../core/audit.log.service.js');
var Exception = require('../core/exception.log.service.js');
var AccessLog = require('../core/access.log.service.js');


module.exports = {

    token: function(userToken, callback) {
        WebUserTokens.findOne({
            token: userToken.token
        }).populate({
            path: 'user',
            match: {
                active: true,
                deleted: false
            }
        }).exec(function(err, token) {
            if (err) {
                console.log(err);
                return callback(err);
            }
            if (!(token && token.user && token.validToken(userToken.secret))) return callback(null, false, {
                message: 'Invalid/Expired Token',
                expireToken: true
            });
            if ((new Date(token.user.accessDate)) > Date.now()) return callback(null, false, {
                message: 'Access is yet to be given to you! Please contact the Administrator!',
                expireToken: true
            });
            var maxDate = (getMaxDate(token.user));
            if (!maxDate || maxDate < Date.now()) return callback(null, false, {
                // message: 'Access has been expired! Please contact the Administrator!',
                message: 'Your subscription has ended or not yet started! Please contact the Administrator!',
                expireToken: true
            });
            token.expireAt = token.newTokenTTL();
            token.save(function(err) {
                if (err) {
                    console.log(err);
                    return callback(err);
                }
                return callback(null, token.user);
            });
        });
    },
    login: function(credentials, callback) {
        console.log("credentials.auth.emmail", credentials.auth.email);
        WebUser.findOne({
            'directoryId': credentials.directoryId,
            'auth.email': credentials.auth.email,
            'active': true,
            'deleted': false
        }, function(err, user) {
            if (err) {
                console.log("In Error");
                console.log(err);
                return callback(err);
            }
            console.log('1');
            if (!user) {

                console.log("In If of Incorrect Username");
                return callback(null, false, {
                    message: 'Incorrect Username'
                });
            }
            if (user && user.auth && !user.auth.registered) {
                console.log("In If of user not registered");
                return callback(null, false, {
                    message: 'Please Sign Up before you Login!'
                });
            }
            console.log('2', user.generateHash('reset@123'));
            if (!user.validPassword(credentials.auth.password)) {
                console.log("In If of Incorrect Password");
                return callback(null, false, {
                    message: 'Incorrect Password'
                });
            }
            console.log('3');
            if ((new Date(user.accessDate)) > Date.now()) {
                console.log("In If of Access is yet to be given to you! Please contact the Administrator");
                return callback(null, false, {
                    message: 'Access is yet to be given to you! Please contact the Administrator!'
                });
            }
            var maxDate = (getMaxDate(user));
            console.log('4');
            if (!maxDate || maxDate < Date.now()) {
                console.log("In If of Access has been expired! Please contact the Administrator!");
                return callback(null, false, {
                    // message: 'Access has been expired! Please contact the Administrator!',
                    message: 'Your subscription has ended or not yet started! Please contact the Administrator!',
                });
            }
            console.log('5');
            WebUserTokens.remove({
                user: user._id,
                secret: credentials.auth.secret
            }, function(err) {
                if (err) {
                    console.log(err);
                    return callback(err);
                }
                console.log("\nIn WebUserTokens.remove");
                var newToken = {
                    user: user._id,
                    secret: credentials.auth.secret,
                    remember: credentials.auth.remember
                };
                console.log("\nnewToken :", newToken);
                WebUserTokens.create(newToken, function(err, token) {
                    if (err) {
                        console.log(err);
                        return callback(err);
                    }
                    if (!token) return callback(null, false, {
                        message: 'Could not generate token!'
                    });
                    console.log("\ncredentials.auth.secret", credentials.auth.secret);
                    token.token = token.generateHash(credentials.auth.secret);
                    console.log("\ntoken.token :", token.token);
                    token.expireAt = token.newTokenTTL();
                    console.log("\ntoken.expireAt", token.expireAt);
                    token.save(function(err) {
                        console.log("\nIn token.save");
                        if (err) {
                            console.log("In token.save err");
                            Exception.log('USER', 'LOGIN', 'User login Error', err, token.user, function(err) {
                                if (err) console.log(err);
                            });
                            return callback(err);
                        }
                        console.log("\ntoken.user", token.user);
                        AccessLog.log(token.user, 'LOGIN', function(err) {
                            if (err) console.log(err);
                        });
                        console.log("\nIn AccessLog function");
                        return callback(null, {
                            email: user,
                            token: token.token,
                            requestPasswordChange: (!user.auth.lastPasswordChangeDate)
                        });
                    });
                });
            });
        });
    },
    logout: function(query, callback) {
        if (typeof query.deleted == 'undefined') query.deleted = false;
        WebUserTokens.findOne(query, function(err, webUserToken) {
            if (err) {
                Exception.log('WEB USER TOKEN', 'REMOVE', 'Web User Token remove Error', err, token.user, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            WebUserTokens.findByIdAndRemove(webUserToken._id, callback);
        });
    },
    get: function(query, callback) {
        if (typeof query.deleted == 'undefined') query.deleted = false;
        WebUser.find(query)
            // .populate({ path: 'package', match: { active: true, deleted: false } })
            .sort({ completeName: 1, _id: 1 }).exec(callback);
    },
    add: function(webUserData, callback) {
        var by = { by: undefined };
        if (webUserData.created) by.by = webUserData.created.by || undefined;
        WebUser.findOne({
            'directoryId': webUserData.directoryId,
            'auth.email': webUserData.auth.email,
            'active': true,
            'deleted': false
        }).exec(function(err, webUser) {
            if (err) {
                console.log(err);
                Exception.log('WEB USER', 'GET', 'Web User get Error', err, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            var orderData = webUserData.orders;
            delete webUserData.orders;
            if (!webUser) {
                webUserData.orders = (webUserData.viaAdmin && orderData.constructor === Array) ? orderData : [orderData];
                WebUser.create(webUserData, function(err, webUser) {
                    if (err) {
                        console.log(err);
                        Exception.log('WEB USER', 'ADD', 'Web User add Error', err, by.by, function(err) {
                            if (err) console.log(err);
                        });
                        return callback(err);
                    }
                    if (!webUser) return callback(null, false, { message: 'Web User could not be created' });
                    Audit.log('WEB USER', 'ADD', 'Web User added', webUser, by.by, function(err) {
                        if (err) console.log(err);
                    });
                    return finalizeOrder(webUser);
                });
            } else {
                webUser.orders.push(orderData);
                webUserData.orders = webUser.orders;
                webUserData.auth.password = webUser.auth.password;
                WebUser.findByIdAndUpdate(webUserData._id, webUserData, function(err, user) {
                    if (err) {
                        console.log(err);
                        Exception.log('WEB USER', 'UPDATE', 'Web User update Error', err, by.by, function(err) {
                            if (err) console.log(err);
                        });
                        return callback(err);
                    }
                    if (!user) return callback(null, false, { message: 'Web User could not be updated' });
                    Audit.log('WEB USER', 'UPDATE', 'Web User updated', user, by.by, function(err) {
                        if (err) console.log(err);
                    });
                    return finalizeOrder(user);
                });
            }

            function finalizeOrder(webUser) {
                if (webUserData.auth && webUserData.auth.email) {
                    orderPlacedMails(webUserData);
                }
                return callback(null, webUser);
            }

        });
    },
    dataTable: function(query, callback) {
        var options = { conditions: query.conditions || {} };
        if (!options.conditions.deleted) options.conditions.deleted = false;
        // if (!options.select) options.select = ['auth.registered'];
        WebUser.dataTable(query.dataTableQuery, options, callback);
    },
    update: function(webUserData, callback) {
        var by = { by: undefined };
        if (typeof webUserData.updatedOrderIndex != 'undefined')
            var updatedOrder = webUserData.orders[webUserData.updatedOrderIndex];
        if (webUserData.updated) by.by = webUserData.updated.by || undefined;
        WebUser.findById(webUserData._id, function(err, webUser) {
            if (err) {
                console.log(err);
                Exception.log('WEB USER', 'GET', 'Web User Fetch Error', err, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            if (webUserData.auth && !webUserData.auth.password) webUserData.auth.password = webUser.auth.password;
            else webUserData.auth.lastPasswordChangeDate = new Date();
            WebUser.findByIdAndUpdate(webUserData._id, webUserData, function(err, user) {
                if (err) {
                    console.log(err);
                    Exception.log('WEB USER', 'UPDATE', 'Web User Update Error', err, by.by, function(err) {
                        if (err) console.log(err);
                    });
                    return callback(err);
                }
                Audit.log('WEB USER', 'UPDATE', 'Web User Updated', user, by.by, function(err) {
                    if (err) console.log(err);
                });
                if (updatedOrder && updatedOrder.confirmed.flag) {
                    confirmationMails(webUserData, updatedOrder);
                }
                return callback(null, user);
            });
            return callback(null, webUser);
        });
    },
    changePassword: function(webUserData, callback) {
        var by = { by: undefined };
        if (webUserData.updated) by.by = webUserData.updated.by || undefined;
        WebUser.findById(webUserData._id, function(err, webUser) {
            if (err) {
                console.log(err);
                Exception.log('WEB USER', 'GET', 'Web User Fetch Error', err, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            if (!webUser.validPassword(webUserData.auth.currentPassword || '')) {
                console.log("In If of Incorrect Password");
                return callback(null, false, {
                    message: 'Incorrect Password'
                });
            }
            webUser.auth.password = webUser.generateHash(webUserData.auth.password);
            webUser.auth.lastPasswordChangeDate = new Date();
            webUser.save(function(err, user) {
                if (err) {
                    console.log(err);
                    Exception.log('WEB USER', 'UPDATE PASSWORD', 'Web User Password Update Error', err, by.by, function(err) {
                        if (err) console.log(err);
                    });
                    return callback(err);
                }
                Audit.log('WEB USER', 'UPDATE PASSWORD', 'Web User Password Updated', user, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(null, webUser);
            });
        });
    },
    confirmSubscription: function(webUserData, callback) {
        console.log("confirmSubscription webUserData : ", webUserData);
        var by = { by: undefined };
        if (webUserData.updated) by.by = webUserData.updated.by || undefined;
        WebUser.findById(webUserData._id, function(err, webUser) {
            if (err) {
                console.log(err);
                Exception.log('WEB USER', 'GET', 'Web User Fetch Error', err, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            if (!webUser.auth) webUser.auth = {};
            if (!webUser.auth.registered) {
                var registrationCode = webUser.randomPassword();
                var randomPassword = webUser.randomPassword();
                console.log('rc: ', registrationCode);
                console.log('rp: ', randomPassword);
                if (webUserData.auth && webUserData.auth.email) {
                    accountCreationMails(webUserData, randomPassword, registrationCode);
                }

                webUserData.auth.registered = false;
                webUserData.auth.lastPasswordChangeDate = undefined;
                webUserData.auth.registrationCode = registrationCode;
                webUserData.auth.password = webUser.generateHash(randomPassword);
            } else {
                webUserData.auth.password = webUser.auth.password;
            }
            WebUser.findByIdAndUpdate(webUserData._id, webUserData, function(err) {
                if (err) {
                    console.log(err);
                    Exception.log('WEB USER', 'UPDATE', 'Web User Update Error', err, by.by, function(err) {
                        if (err) console.log(err);
                    });
                    return callback(err);
                }
                Audit.log('WEB USER', 'UPDATE', 'Web User Updated', webUser, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(null, webUser);
            });
        });
    },
    remove: function(webUserData, callback) {
        var by = { by: undefined };
        if (webUserData.updated) by.by = webUserData.updated.by || undefined;
        webUserData.deleted = true;
        WebUser.findByIdAndUpdate(webUserData._id, webUserData, function(err, webUser) {
            if (err) {
                console.log(err);
                Exception.log('WEB USER', 'DELETE', 'Web User Delete Error', err, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            Audit.log('WEB USER', 'DELETE', 'Web User Deleted', webUser, by.by, function(err) {
                if (err) console.log(err);
            });
            return callback(null, webUser);
        });
    },
    forgotPassword: function(credentials, callback) {
        console.log("forgotPassword credentials : ", credentials);
        WebUser.findOne({
            'directoryId': credentials.directoryId,
            'auth.email': credentials.auth.email,
            'active': true,
            'deleted': false
        }, function(err, webUser) {
            if (err) {
                console.log(err);
                Exception.log('USER', 'GET', 'WebUser get Error', err, undefined, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            if (!webUser) return callback(null, false, { message: 'Incorrect Email' });
            var randomPassword = webUser.randomPassword();
            webUser.auth.password = webUser.generateHash(randomPassword);
            webUser.auth.lastPasswordChangeDate = undefined;
            webUser.save(function(err, webUser) {
                if (err) {
                    console.log(err);
                    Exception.log('USER', 'GET', 'WebUser get Error', err, undefined, function(err) {
                        if (err) console.log(err);
                    });
                    return callback(err);
                }
                Audit.log('USER', 'RESET_PASSWORD', 'WebUser password reset', webUser, undefined, function(err) {
                    if (err) console.log(err);
                });
                if (webUser.directoryId == '57189cd924d8bc65f4123bc3')
                    Mailer.sendMail("Official Catholic Directory", webUser.auth.email, 'NRP: Password Recovery', '', '<h2>Greetings <b>' + (webUser.completeName || 'user') + '</b>!</h2><p>We received a request to reset the password associated with this email address. If you made this request, please go to website and log in using below credentials<br><br>Username: ' + webUser.auth.email + '<br>Password: ' + randomPassword + '<br><br></p><br><br><p>Best wishes,<br><strong>National Register Publishing</strong><br>Customer Service<br>Treasured Works, LLC / National Register Publishing,<br>430 Mountain Avenue Suite 403, New Providence, NJ 07974.<br>Phone - 908-517-0780<br>Fax - 908-608-3012<br>Email - <a href="mailto:Sales@nrpdirect.com">Sales@nrpdirect.com</a></p>');
                if (webUser.directoryId == "57189b3e24d8bc65f4123bbf")
                    Mailer.sendMail("Official Museum Directory", webUser.auth.email, 'NRP: Password Recovery', '', '<h2>Greetings <b>' + (webUser.completeName || 'user') + '</b>!</h2><p>We received a request to reset the password associated with this email address. If you made this request, please go to website and log in using below credentials<br><br>Username: ' + webUser.auth.email + '<br>Password: ' + randomPassword + '<br><br></p><br><br><p>Best wishes,<br><strong>National Register Publishing</strong><br>Customer Service<br>Treasured Works, LLC / National Register Publishing,<br>430 Mountain Avenue Suite 403, New Providence, NJ 07974.<br>Phone - 908-517-0780<br>Fax - 908-608-3012<br>Email - <a href="mailto:Sales@nrpdirect.com">Sales@nrpdirect.com</a></p>');
                if (webUser.directoryId == "57189ccf24d8bc65f4123bc2")
                    Mailer.sendMail("Direct Marketing Market Place", webUser.auth.email, 'NRP: Password Recovery', '', '<h2>Greetings <b>' + (webUser.completeName || 'user') + '</b>!</h2><p>We received a request to reset the password associated with this email address. If you made this request, please go to website and log in using below credentials<br><br>Username: ' + webUser.auth.email + '<br>Password: ' + randomPassword + '<br><br></p><br><br><p>Best wishes,<br><strong>National Register Publishing</strong><br>Customer Service<br>Treasured Works, LLC / National Register Publishing,<br>430 Mountain Avenue Suite 403, New Providence, NJ 07974.<br>Phone - 908-517-0780<br>Fax - 908-608-3012<br>Email - <a href="mailto:Sales@nrpdirect.com">Sales@nrpdirect.com</a></p>');
                if (webUser.directoryId == "57189c7024d8bc65f4123bc0")
                    Mailer.sendMail("American Art Directory", webUser.auth.email, 'NRP: Password Recovery', '', '<h2>Greetings <b>' + (webUser.completeName || 'user') + '</b>!</h2><p>We received a request to reset the password associated with this email address. If you made this request, please go to website and log in using below credentials<br><br>Username: ' + webUser.auth.email + '<br>Password: ' + randomPassword + '<br><br></p><br><br><p>Best wishes,<br><strong>National Register Publishing</strong><br>Customer Service<br>Treasured Works, LLC / National Register Publishing,<br>430 Mountain Avenue Suite 403, New Providence, NJ 07974.<br>Phone - 908-517-0780<br>Fax - 908-608-3012<br>Email - <a href="mailto:Sales@nrpdirect.com">Sales@nrpdirect.com</a></p>');
                if (webUser.directoryId == "57189cc224d8bc65f4123bc1")
                    Mailer.sendMail("Corporate Finance Sourcebook", webUser.auth.email, 'NRP: Password Recovery', '', '<h2>Greetings <b>' + (webUser.completeName || 'user') + '</b>!</h2><p>We received a request to reset the password associated with this email address. If you made this request, please go to website and log in using below credentials<br><br>Username: ' + webUser.auth.email + '<br>Password: ' + randomPassword + '<br><br></p><br><br><p>Best wishes,<br><strong>National Register Publishing</strong><br>Customer Service<br>Treasured Works, LLC / National Register Publishing,<br>430 Mountain Avenue Suite 403, New Providence, NJ 07974.<br>Phone - 908-517-0780<br>Fax - 908-608-3012<br>Email - <a href="mailto:Sales@nrpdirect.com">Sales@nrpdirect.com</a></p>');
                return callback(null, webUser);
            });
        });
    },
    generateAndInsertPassword: function(webUserData, callback) {
        console.log("generateAndInsertPassword webUserData : ", webUserData);
        var by = { by: undefined };
        if (webUserData.updated) by.by = webUserData.updated.by || undefined;
        WebUser.findById(webUserData._id, function(err, webUser) {
            if (err) {
                console.log(err);
                Exception.log('WEB USER', 'GET', 'Web User Fetch Error', err, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            if (!webUser.auth) webUser.auth = {};
            var randomPassword = webUser.randomPassword();
            if (webUserData.auth && webUserData.auth.email) {
                if (webUserData.directoryId == '57189cd924d8bc65f4123bc3')
                    Mailer.sendMail("Official Catholic Directory", webUserData.auth.email, 'Welcome to Official Catholic Directory', '', '<h2>Greetings <b>' + (webUserData.completeName || 'user') + '</b>!</h2><p>Welcome to Official Catholic Directory</p><p>Your OTP is authenticated. Now you can login with following Username and Password.<br><br><b>Username: ' + webUserData.auth.email + '</b><br><b>Password: ' + randomPassword + '</b></p><p>Regards, </p><p>Treasured Works </p>');
                if (webUserData.directoryId == "57189b3e24d8bc65f4123bbf")
                    Mailer.sendMail("Official Museum Directory", webUserData.auth.email, 'Welcome to Official Museum Directory', '', '<h2>Greetings <b>' + (webUserData.completeName || 'user') + '</b>!</h2><p>Welcome to Official Museum Directory</p><p>Your OTP is authenticated. Now you can login with following Username and Password.<br><br><b>Username: ' + webUserData.auth.email + '</b><br><b>Password: ' + randomPassword + '</b></p><p>Regards, </p><p>Treasured Works </p>');
                if (webUserData.directoryId == "57189ccf24d8bc65f4123bc2")
                    Mailer.sendMail("Direct Marketing Market Place", webUserData.auth.email, 'Welcome to Direct Marketing Market Place', '', '<h2>Greetings <b>' + (webUserData.completeName || 'user') + '</b>!</h2><p>Welcome to Direct Marketing Market Place</p><p>Your OTP is authenticated. Now you can login with following Username and Password.<br><br><b>Username: ' + webUserData.auth.email + '</b><br><b>Password: ' + randomPassword + '</b></p><p>Regards, </p><p>Treasured Works </p>');
                if (webUserData.directoryId == "57189c7024d8bc65f4123bc0")
                    Mailer.sendMail("American Art Directory", webUserData.auth.email, 'Welcome to American Art Directory', '', '<h2>Greetings <b>' + (webUserData.completeName || 'user') + '</b>!</h2><p>Welcome to American Art Directory</p><p>Your OTP is authenticated. Now you can login with following Username and Password.<br><br><b>Username: ' + webUserData.auth.email + '</b><br><b>Password: ' + randomPassword + '</b></p><p>Regards, </p><p>Treasured Works </p>');
                if (webUserData.directoryId == "57189cc224d8bc65f4123bc1")
                    Mailer.sendMail("Corporate Finance Sourcebook", webUserData.auth.email, 'Welcome to Corporate Finance Sourcebook', '', '<h2>Greetings <b>' + (webUserData.completeName || 'user') + '</b>!</h2><p>Welcome to Corporate Finance Sourcebook</p><p>Your OTP is authenticated. Now you can login with following Username and Password.<br><br><b>Username: ' + webUserData.auth.email + '</b><br><b>Password: ' + randomPassword + '</b></p><p>Regards, </p><p>Treasured Works </p>');
            }

            console.log("randomPassword", randomPassword);
            console.log("generateHash", webUser.generateHash(randomPassword));
            webUser.auth.password = webUser.generateHash(randomPassword);
            webUser.auth.registered = true;
            webUser.save(function(err) {
                if (err) {
                    console.log(err);
                    Exception.log('WEB USER', 'UPDATE', 'Web User Update Error', err, by.by, function(err) {
                        if (err) console.log(err);
                    });
                    return callback(err);
                }
                Audit.log('WEB USER', 'UPDATE', 'Web User Updated', webUser, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(null, webUser);
            });
        });
    }

};

function getMaxDate(user) {
    var maxDate;
    for (var i = 0; i < user.orders.length; i++) {
        var order = user.orders[i];
        if (order.confirmed && order.confirmed.flag && order.subscription && order.subscription.to)
            if (maxDate) maxDate = (maxDate > (new Date(order.subscription.to))) ? maxDate : new Date(order.subscription.to);
            else maxDate = new Date(order.subscription.to);
    }
    return (maxDate ? (new Date(maxDate.getTime() + (1000 * 60 * 60 * 24))) : false);
}


function confirmationMails(webUserData, updatedOrder) {
    if (webUserData.directoryId == '57189cd924d8bc65f4123bc3')
        Mailer.sendMail("Official Catholic Directory", webUserData.auth.email, 'NRP: Order Confirmation', '', '<h2>Greetings <b>' + (webUserData.completeName || 'user') + '</b>!</h2><p>This is to inform you that the order that you had placed for the package <b>' + updatedOrder.packageName + '</b> has been confirmed.<br>If you have any question about our product/advertisement services, we invite you to call us immediately at 908-517-0780.</p><br><br>DO NOT Respond to this email.<br>To respond choose here info@nrpdirect.com<br><br><p>Regards,<br><strong>National Register Publishing</strong><br>Customer Service<br>Treasured Works, LLC / National Register Publishing,<br>430 Mountain Avenue Suite 403, New Providence, NJ 07974.<br>Phone - 908-517-0780<br>Fax - 908-608-3012<br>Email - <a href="mailto:Sales@nrpdirect.com">Sales@nrpdirect.com</a></p>');
    if (webUserData.directoryId == "57189b3e24d8bc65f4123bbf")
        Mailer.sendMail("Official Museum Directory", webUserData.auth.email, 'NRP: Order Confirmation', '', '<h2>Greetings <b>' + (webUserData.completeName || 'user') + '</b>!</h2><p>This is to inform you that the order that you had placed for the package <b>' + updatedOrder.packageName + '</b> has been confirmed.<br>If you have any question about our product/advertisement services, we invite you to call us immediately at 908-517-0780.</p><br><br>DO NOT Respond to this email.<br>To respond choose here info@nrpdirect.com<br><br><p>Regards,<br><strong>National Register Publishing</strong><br>Customer Service<br>Treasured Works, LLC / National Register Publishing,<br>430 Mountain Avenue Suite 403, New Providence, NJ 07974.<br>Phone - 908-517-0780<br>Fax - 908-608-3012<br>Email - <a href="mailto:Sales@nrpdirect.com">Sales@nrpdirect.com</a></p>');
    if (webUserData.directoryId == "57189ccf24d8bc65f4123bc2")
        Mailer.sendMail("Direct Marketing Market Place", webUserData.auth.email, 'NRP: Order Confirmation', '', '<h2>Greetings <b>' + (webUserData.completeName || 'user') + '</b>!</h2><p>This is to inform you that the order that you had placed for the package <b>' + updatedOrder.packageName + '</b> has been confirmed.<br>If you have any question about our product/advertisement services, we invite you to call us immediately at 908-517-0780.</p><br><br>DO NOT Respond to this email.<br>To respond choose here info@nrpdirect.com<br><br><p>Regards,<br><strong>National Register Publishing</strong><br>Customer Service<br>Treasured Works, LLC / National Register Publishing,<br>430 Mountain Avenue Suite 403, New Providence, NJ 07974.<br>Phone - 908-517-0780<br>Fax - 908-608-3012<br>Email - <a href="mailto:Sales@nrpdirect.com">Sales@nrpdirect.com</a></p>');
    if (webUserData.directoryId == "57189c7024d8bc65f4123bc0")
        Mailer.sendMail("American Art Directory", webUserData.auth.email, 'NRP: Order Confirmation', '', '<h2>Greetings <b>' + (webUserData.completeName || 'user') + '</b>!</h2><p>This is to inform you that the order that you had placed for the package <b>' + updatedOrder.packageName + '</b> has been confirmed.<br>If you have any question about our product/advertisement services, we invite you to call us immediately at 908-517-0780.</p><br><br>DO NOT Respond to this email.<br>To respond choose here info@nrpdirect.com<br><br><p>Regards,<br><strong>National Register Publishing</strong><br>Customer Service<br>Treasured Works, LLC / National Register Publishing,<br>430 Mountain Avenue Suite 403, New Providence, NJ 07974.<br>Phone - 908-517-0780<br>Fax - 908-608-3012<br>Email - <a href="mailto:Sales@nrpdirect.com">Sales@nrpdirect.com</a></p>');
    if (webUserData.directoryId == "57189cc224d8bc65f4123bc1")
        Mailer.sendMail("Corporate Finance Sourcebook", webUserData.auth.email, 'NRP: Order Confirmation', '', '<h2>Greetings <b>' + (webUserData.completeName || 'user') + '</b>!</h2><p>This is to inform you that the order that you had placed for the package <b>' + updatedOrder.packageName + '</b> has been confirmed.<br>If you have any question about our product/advertisement services, we invite you to call us immediately at 908-517-0780.</p><br><br>DO NOT Respond to this email.<br>To respond choose here info@nrpdirect.com<br><br><p>Regards,<br><strong>National Register Publishing</strong><br>Customer Service<br>Treasured Works, LLC / National Register Publishing,<br>430 Mountain Avenue Suite 403, New Providence, NJ 07974.<br>Phone - 908-517-0780<br>Fax - 908-608-3012<br>Email - <a href="mailto:Sales@nrpdirect.com">Sales@nrpdirect.com</a></p>');
}

function accountCreationMails(webUserData, randomPassword, registrationCode) {
    if (webUserData.directoryId == '57189cd924d8bc65f4123bc3')
        Mailer.sendMail("Official Catholic Directory", webUserData.auth.email, 'NRP: Welcome to Official Catholic Directory', '', '<h2>Greetings <b>' + (webUserData.completeName || 'user') + '</b>!</h2><p>Thanks for purchasing our product.  You are authenticated now in our system.<br>If you have any question about our product/advertisement services, we invite you to call us immediately at 908-517-0780.<br>You can login with following Username and Password. First sign up requires change of password provided below.<br><br>Username: ' + webUserData.auth.email + '<br>Password: ' + randomPassword + (registrationCode ? '<br>Registration Code: ' + registrationCode : "") + '<br><br>Once again Thank you for providing opportunity to serve you.</p><br><br>DO NOT Respond to this email.<br>To respond choose here info@nrpdirect.com<br><br><p>Best wishes,<br><strong>National Register Publishing</strong><br>Customer Service<br>Treasured Works, LLC / National Register Publishing,<br>430 Mountain Avenue Suite 403, New Providence, NJ 07974.<br>Phone - 908-517-0780<br>Fax - 908-608-3012<br>Email - <a href="mailto:Sales@nrpdirect.com">Sales@nrpdirect.com</a></p>');
    if (webUserData.directoryId == "57189b3e24d8bc65f4123bbf")
        Mailer.sendMail("Official Museum Directory", webUserData.auth.email, 'NRP: Welcome to Official Museum Directory', '', '<h2>Greetings <b>' + (webUserData.completeName || 'user') + '</b>!</h2><p>Thanks for purchasing our product.  You are authenticated now in our system.<br>If you have any question about our product/advertisement services, we invite you to call us immediately at 908-517-0780.<br>You can login with following Username and Password. First sign up requires change of password provided below.<br><br>Username: ' + webUserData.auth.email + '<br>Password: ' + randomPassword + (registrationCode ? '<br>Registration Code: ' + registrationCode : "") + '<br><br>Once again Thank you for providing opportunity to serve you.</p><br><br>DO NOT Respond to this email.<br>To respond choose here info@nrpdirect.com<br><br><p>Best wishes,<br><strong>National Register Publishing</strong><br>Customer Service<br>Treasured Works, LLC / National Register Publishing,<br>430 Mountain Avenue Suite 403, New Providence, NJ 07974.<br>Phone - 908-517-0780<br>Fax - 908-608-3012<br>Email - <a href="mailto:Sales@nrpdirect.com">Sales@nrpdirect.com</a></p>');
    if (webUserData.directoryId == "57189ccf24d8bc65f4123bc2")
        Mailer.sendMail("Direct Marketing Market Place", webUserData.auth.email, 'NRP: Welcome to Direct Marketing Market Place', '', '<h2>Greetings <b>' + (webUserData.completeName || 'user') + '</b>!</h2><p>Thanks for purchasing our product.  You are authenticated now in our system.<br>If you have any question about our product/advertisement services, we invite you to call us immediately at 908-517-0780.<br>You can login with following Username and Password. First sign up requires change of password provided below.<br><br>Username: ' + webUserData.auth.email + '<br>Password: ' + randomPassword + (registrationCode ? '<br>Registration Code: ' + registrationCode : "") + '<br><br>Once again Thank you for providing opportunity to serve you.</p><br><br>DO NOT Respond to this email.<br>To respond choose here info@nrpdirect.com<br><br><p>Best wishes,<br><strong>National Register Publishing</strong><br>Customer Service<br>Treasured Works, LLC / National Register Publishing,<br>430 Mountain Avenue Suite 403, New Providence, NJ 07974.<br>Phone - 908-517-0780<br>Fax - 908-608-3012<br>Email - <a href="mailto:Sales@nrpdirect.com">Sales@nrpdirect.com</a></p>');
    if (webUserData.directoryId == "57189c7024d8bc65f4123bc0")
        Mailer.sendMail("American Art Directory", webUserData.auth.email, 'NRP: Welcome to American Art Directory', '', '<h2>Greetings <b>' + (webUserData.completeName || 'user') + '</b>!</h2><p>Thanks for purchasing our product.  You are authenticated now in our system.<br>If you have any question about our product/advertisement services, we invite you to call us immediately at 908-517-0780.<br>You can login with following Username and Password. First sign up requires change of password provided below.<br><br>Username: ' + webUserData.auth.email + '<br>Password: ' + randomPassword + (registrationCode ? '<br>Registration Code: ' + registrationCode : "") + '<br><br>Once again Thank you for providing opportunity to serve you.</p><br><br>DO NOT Respond to this email.<br>To respond choose here info@nrpdirect.com<br><br><p>Best wishes,<br><strong>National Register Publishing</strong><br>Customer Service<br>Treasured Works, LLC / National Register Publishing,<br>430 Mountain Avenue Suite 403, New Providence, NJ 07974.<br>Phone - 908-517-0780<br>Fax - 908-608-3012<br>Email - <a href="mailto:Sales@nrpdirect.com">Sales@nrpdirect.com</a></p>');
    if (webUserData.directoryId == "57189cc224d8bc65f4123bc1")
        Mailer.sendMail("Corporate Finance Sourcebook", webUserData.auth.email, 'NRP: Welcome to Corporate Finance Sourcebook', '', '<h2>Greetings <b>' + (webUserData.completeName || 'user') + '</b>!</h2><p>Thanks for purchasing our product.  You are authenticated now in our system.<br>If you have any question about our product/advertisement services, we invite you to call us immediately at 908-517-0780.<br>You can login with following Username and Password. First sign up requires change of password provided below.<br><br>Username: ' + webUserData.auth.email + '<br>Password: ' + randomPassword + (registrationCode ? '<br>Registration Code: ' + registrationCode : "") + '<br><br>Once again Thank you for providing opportunity to serve you.</p><br><br>DO NOT Respond to this email.<br>To respond choose here info@nrpdirect.com<br><br><p>Best wishes,<br><strong>National Register Publishing</strong><br>Customer Service<br>Treasured Works, LLC / National Register Publishing,<br>430 Mountain Avenue Suite 403, New Providence, NJ 07974.<br>Phone - 908-517-0780<br>Fax - 908-608-3012<br>Email - <a href="mailto:Sales@nrpdirect.com">Sales@nrpdirect.com</a></p>');
}

function orderPlacedMails(webUserData) {
    if (webUserData.directoryId == '57189cd924d8bc65f4123bc3')
        Mailer.sendMail("Official Catholic Directory", webUserData.auth.email, 'NRP: Order Submission confirmation', '', '<h2>Greetings <b>' + (webUserData.completeName || 'user') + '</b>!</h2><p>Thanks for choosing us. Our representative will contact you soon.<br>Mean while if you have any question about our product/advertisement services, we invite you to call us immediately at 908-517-0780.<br>Once again Thank you for providing us to serve you.</p><br><br>DO NOT Respond to this email.<br>To respond choose here info@nrpdirect.com<br><br><p>Regards,<br><strong>National Register Publishing</strong><br>Customer Service<br>Treasured Works, LLC / National Register Publishing,<br>430 Mountain Avenue Suite 403, New Providence, NJ 07974.<br>Phone - 908-517-0780<br>Fax - 908-608-3012<br>Email - <a href="mailto:Sales@nrpdirect.com">Sales@nrpdirect.com</a></p>');
    if (webUserData.directoryId == "57189b3e24d8bc65f4123bbf")
        Mailer.sendMail("Official Museum Directory", webUserData.auth.email, 'NRP: Order Submission confirmation', '', '<h2>Greetings <b>' + (webUserData.completeName || 'user') + '</b>!</h2><p>Thanks for choosing us. Our representative will contact you soon.<br>Mean while if you have any question about our product/advertisement services, we invite you to call us immediately at 908-517-0780.<br>Once again Thank you for providing us to serve you.</p><br><br>DO NOT Respond to this email.<br>To respond choose here info@nrpdirect.com<br><br><p>Regards,<br><strong>National Register Publishing</strong><br>Customer Service<br>Treasured Works, LLC / National Register Publishing,<br>430 Mountain Avenue Suite 403, New Providence, NJ 07974.<br>Phone - 908-517-0780<br>Fax - 908-608-3012<br>Email - <a href="mailto:Sales@nrpdirect.com">Sales@nrpdirect.com</a></p>');
    if (webUserData.directoryId == "57189ccf24d8bc65f4123bc2")
        Mailer.sendMail("Direct Marketing Market Place", webUserData.auth.email, 'NRP: Order Submission confirmation', '', '<h2>Greetings <b>' + (webUserData.completeName || 'user') + '</b>!</h2><p>Thanks for choosing us. Our representative will contact you soon.<br>Mean while if you have any question about our product/advertisement services, we invite you to call us immediately at 908-517-0780.<br>Once again Thank you for providing us to serve you.</p><br><br>DO NOT Respond to this email.<br>To respond choose here info@nrpdirect.com<br><br><p>Regards,<br><strong>National Register Publishing</strong><br>Customer Service<br>Treasured Works, LLC / National Register Publishing,<br>430 Mountain Avenue Suite 403, New Providence, NJ 07974.<br>Phone - 908-517-0780<br>Fax - 908-608-3012<br>Email - <a href="mailto:Sales@nrpdirect.com">Sales@nrpdirect.com</a></p>');
    if (webUserData.directoryId == "57189c7024d8bc65f4123bc0")
        Mailer.sendMail("American Art Directory", webUserData.auth.email, 'NRP: Order Submission confirmation', '', '<h2>Greetings <b>' + (webUserData.completeName || 'user') + '</b>!</h2><p>Thanks for choosing us. Our representative will contact you soon.<br>Mean while if you have any question about our product/advertisement services, we invite you to call us immediately at 908-517-0780.<br>Once again Thank you for providing us to serve you.</p><br><br>DO NOT Respond to this email.<br>To respond choose here info@nrpdirect.com<br><br><p>Regards,<br><strong>National Register Publishing</strong><br>Customer Service<br>Treasured Works, LLC / National Register Publishing,<br>430 Mountain Avenue Suite 403, New Providence, NJ 07974.<br>Phone - 908-517-0780<br>Fax - 908-608-3012<br>Email - <a href="mailto:Sales@nrpdirect.com">Sales@nrpdirect.com</a></p>');
    if (webUserData.directoryId == "57189cc224d8bc65f4123bc1")
        Mailer.sendMail("Corporate Finance Sourcebook", webUserData.auth.email, 'NRP: Order Submission confirmation', '', '<h2>Greetings <b>' + (webUserData.completeName || 'user') + '</b>!</h2><p>Thanks for choosing us. Our representative will contact you soon.<br>Mean while if you have any question about our product/advertisement services, we invite you to call us immediately at 908-517-0780.<br>Once again Thank you for providing us to serve you.</p><br><br>DO NOT Respond to this email.<br>To respond choose here info@nrpdirect.com<br><br><p>Regards,<br><strong>National Register Publishing</strong><br>Customer Service<br>Treasured Works, LLC / National Register Publishing,<br>430 Mountain Avenue Suite 403, New Providence, NJ 07974.<br>Phone - 908-517-0780<br>Fax - 908-608-3012<br>Email - <a href="mailto:Sales@nrpdirect.com">Sales@nrpdirect.com</a></p>');
}
