var express = require('express');
var webUser = express.Router();

var webUserController = require('./web-user.controller.js');

webUser.get('/', webUserController.get);
webUser.post('/data-table', webUserController.dataTable);
webUser.post('/list', webUserController.get);
webUser.post('/', webUserController.add);
webUser.post('/register', webUserController.register);
webUser.put('/', webUserController.update);
webUser.put('/confirm-subscription', webUserController.confirmSubscription);
webUser.put('/delete', webUserController.remove);
webUser.post('/token', webUserController.token);
webUser.post('/login', webUserController.login);
webUser.post('/logout', webUserController.logout);
webUser.put('/forgot-password', webUserController.forgotPassword);
webUser.put('/change-password', webUserController.changePassword);

module.exports = webUser;
