var bcrypt = require('bcrypt-nodejs');
var randomstring = require("randomstring");
var mongoose = require('mongoose');
var WebUserOrdersSchema = require('./web-user.orders.schema.js');
var Schema = mongoose.Schema;

var TXN_WebUserSchema = new Schema({
    "directoryId": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "auth": {
        "email": String,
        "password": String,
        "registrationCode": String,
        "registered": {
            type: Boolean,
            default: false
        },
        "lastPasswordChangeDate": Date
    },
    "orders": [WebUserOrdersSchema],
    "completeName": String,
    "name": {
        "first": String,
        "middle": String,
        "last": String
    },
    "title": String,
    "institution": String,
    "address": String,
    "address2": String,
    "city": String,
    "state": String,
    "country": String,
    "zip": String,
    "phoneNumbers": String,
    "faxNumbers": String,
    "sourceOfInfo": String,
    "social": {
        "facebook": String,
        "twitter": String,
        "linkedIn": String
    },
    "active": {
        type: Boolean,
        default: true
    },
    "deleted": {
        type: Boolean,
        default: false
    },
    "accessDate": Date,
    "expiryDate": Date,
    "created": {
        "by": {
            "type": Schema.Types.ObjectId,
            "ref": 'TXN_WebUser'
        },
        "at": {
            "type": Date,
            "default": Date.now
        }
    },
    "updated": {
        "by": {
            "type": Schema.Types.ObjectId,
            "ref": 'TXN_WebUser'
        },
        "at": {
            "type": Date,
            "default": Date.now
        }
    }
}).post('init', function(doc) {
    doc.orders.sort(function(fn1, fn2) {
        return (fn2.recieved - fn1.recieved);
    });
    return doc;
});

TXN_WebUserSchema.index({ completeName: 1, _id: 1 });


// methods ======================
// generating a hash
TXN_WebUserSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if hash is valid
TXN_WebUserSchema.methods.validStringHash = function(string, hash) {
    return bcrypt.compareSync(string, hash);
};

// checking if password is valid
TXN_WebUserSchema.methods.validPassword = function(password) {
    console.log('here', password, this.auth.password);
    return (this.auth && this.auth.password) ? bcrypt.compareSync(password, this.auth.password) : false;
};

// randomize password
TXN_WebUserSchema.methods.randomPassword = function() {
    String.prototype.replaceAt = function(index, character) {
        return this.substr(0, index) + character + this.substr(index + character.length);
    }
    var newString = randomstring.generate(10);
    newString = newString.replaceAt(4, "$");
    newString = newString.replaceAt(8, "*");
    // newString[8] = "*";
    return newString;
};

module.exports = mongoose.model('TXN_WebUser', TXN_WebUserSchema);
