var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = new Schema({
    "typeName": String,
    "qty": Number,
    "ppu": Number,
    "package": {
        "type": Schema.Types.ObjectId,
        "ref": 'TXN_SubscriptionPackage'
    },
    "packageName": String,
    "subscription": {
        "from": Date,
        "to": Date
    },
    "recieved": {
        type: Date,
        default: Date.now
    },
    "confirmed": {
        "flag": {
            type: Boolean,
            default: false
        },
        "by": {
            "type": Schema.Types.ObjectId,
            "ref": 'TXN_User'
        },
        at: Date
    }

}).post('init', function(doc) {
    if (!doc._id) doc._id = mongoose.Types.ObjectId();
    return doc;
});
