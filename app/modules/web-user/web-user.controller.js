var coreQueryBuilderService = require('../core/core.query-builder.service.js');
var webUserService = require('./web-user.service.js');
var Mailer = require('../core/mailer.service.js');
var WebUser = require('./web-user.model.js');

function sanitizeUser(user) {
    if (user.constructor === Array) {
        for (var i in user) {
            user[i] = sanitizeUser(user[i]);
        }
    } else {
        if (user.user && user.user.auth) {
            user.user = sanitizeUser(user.user);
        }
        if (user.created && ('by' in user.created) && user.created.by && user.created.by.auth) {
            user.created.by = sanitizeUser(user.created.by);
        }
        if (user.updated && ('by' in user.updated) && user.updated.by && user.updated.by.auth) {
            user.updated.by = sanitizeUser(user.updated.by);
        }
        if (user.auth) {
            user.auth.password = undefined;
            delete user.auth.password;
            if (user.auth.securityQuestion) {
                user.auth.securityQuestion.answer = undefined;
                delete user.auth.securityQuestion.answer;
            }
        }
    }
    return user;
}

function loginFunction(req, res) {
    var credentials = req.body;
    console.log("In login controller credentials :", credentials);
    webUserService.login(credentials, function(err, user, info) {
        if (err) return res.status(400).send(err);
        if (!user) return res.status(403).send(info);
        return res.send(sanitizeUser(user));
    });
}

module.exports = {
    token: function(req, res) {
        var token = req.body;
        console.log(" In token controller token :", token);
        webUserService.token(token, function(err, user, info) {
            if (err) return res.status(400).send(err);
            if (!user) return res.status(403).send(info);
            return res.send(sanitizeUser(user));
        });
    },
    login: loginFunction,
    logout: function(req, res) {
        var query = coreQueryBuilderService.buildQuery(req);
        webUserService.logout(query, function(err, user, info) {
            if (err) return res.status(400).send(err);
            if (!user) return res.status(403).send(info);
            return res.send(sanitizeUser(user));
        });
    },
    get: function(req, res) {
        var query = coreQueryBuilderService.buildQuery(req);
        webUserService.get(query, function(err, webUsers) {
            if (err) return res.status(400).send(err);
            return res.send(sanitizeUser(webUsers));
        });
    },
    dataTable: function(req, res) {
        var query = coreQueryBuilderService.buildQuery(req);
        webUserService.dataTable(query, function(err, webUsers) {
            if (err) return res.status(400).send(err);
            return res.send(webUsers);
        });
    },
    add: function(req, res) {
        console.log("req.body:", req.body);
        webUserService.add(req.body, function(err, webUser) {
            if (err) return res.status(400).send(err);
            return res.send(sanitizeUser(webUser));
        });
    },
    update: function(req, res) {
        webUserService.update(req.body, function(err, webUser) {
            if (err) return res.status(400).send(err);
            return res.send(sanitizeUser(webUser));
        });
    },
    confirmSubscription: function(req, res) {
        webUserService.confirmSubscription(req.body, function(err, webUser) {
            if (err) return res.status(400).send(err);
            return res.send(sanitizeUser(webUser));
        });
    },
    remove: function(req, res) {
        webUserService.remove(req.body, function(err, webUser) {
            if (err) return res.status(400).send(err);
            return res.send(sanitizeUser(webUser));
        });
    },
    forgotPassword: function(req, res) {
        console.log("\nIn Reset Password Controller");
        var credentials = req.body;
        webUserService.forgotPassword(credentials, function(err, webUser, info) {
            if (err) return res.status(400).send(err);
            if (!webUser) return res.status(403).send(info);
            return res.send(sanitizeUser(webUser));
        });
    },
    changePassword: function(req, res) {
        console.log("\nIn Manual Reset Password Controller");
        webUserService.changePassword(req.body, function(err, webUser, info) {
            if (err) return res.status(400).send(err);
            if (!webUser) return res.status(400).send(info);
            return res.send(sanitizeUser(webUser));
        });
    },
    register: function(req, res) {
        var query = coreQueryBuilderService.buildQuery(req);
        console.log('query for validate otp', query);
        webUserService.get({
            directoryId: query.directoryId,
            'auth.email': query.auth.email,
            'auth.registrationCode': query.auth.registrationCode,
            active: true,
            deleted: false
        }, function(err, webUsers) {
            if (err) return res.status(400).send(err);
            console.log("\n\n webUsers : ", webUsers);
            if (webUsers && webUsers[0]) {
                console.log("validate");
                if ((new Date(webUsers[0].accessDate)) > Date.now()) {
                    console.log("In If of Access is yet to be given to you! Please contact the Administrator");
                    return res.status(400).send({
                        message: 'Access is yet to be given to you! Please contact the Administrator!'
                    });
                }
                var maxDate = (getMaxDate(webUsers[0]));
                console.log('4');
                if (!maxDate || maxDate < Date.now()) {
                    console.log("In If of Access has been expired! Please contact the Administrator!");
                    return res.status(400).send({
                        // message: 'Access has been expired! Please contact the Administrator!',
                        message: 'Your subscription has ended or not yet started! Please contact the Administrator!',
                    });
                }
                if (!webUsers[0].validPassword(query.auth.currentPassword)) return res.status(403).send({ message: "Invalid password!" });
                else if (webUsers[0].auth.registered) return res.status(400).send({ message: "This user has already registered!" });
                else {
                    webUsers[0].auth.password = webUsers[0].generateHash(query.auth.password);
                    webUsers[0].auth.registered = true;
                    webUsers[0].auth.lastPasswordChangeDate = new Date();
                    webUsers[0].save(function(err, user) {
                        if (err) {
                            console.log(err);
                            return res.status(400).send(err);
                        }
                        loginFunction(req, res);
                    });
                };
            } else {
                console.log("In Web User controller Record is not Found(empty responce)");
                return res.status(400).send({ 'message': 'Invalid Email and/or Registration code!' });
            }
        });
    }
};

function getMaxDate(user) {
    var maxDate;
    for (var i = 0; i < user.orders.length; i++) {
        var order = user.orders[i];
        if (order.confirmed && order.confirmed.flag && order.subscription && order.subscription.to)
            if (maxDate) maxDate = (maxDate > (new Date(order.subscription.to))) ? maxDate : new Date(order.subscription.to);
            else maxDate = new Date(order.subscription.to);
    }
    return (maxDate ? (new Date(maxDate.getTime() + (1000 * 60 * 60 * 24))) : false);
}