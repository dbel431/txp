var Role = require('./role.model.js');
var Audit = require('../core/audit.log.service.js');
var Exception = require('../core/exception.log.service.js');

module.exports = {
    get: function(query, callback) {
        if (typeof query.deleted == 'undefined') query.deleted = false;
        Role.find(query).sort({ name: 1, _id: 1 }).exec(callback);
    },
    add: function(roleData, callback) {
        var by = { by: undefined };
        if (roleData.created) by.by = roleData.created.by || undefined;
        Role.create(roleData, function(err, role) {
            if (err) {
                console.log(err);
                Exception.log('ROLE', 'ADD', 'Role add Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            if (!role) return callback(null, false, { message: 'Role not found' });
            Audit.log('ROLE', 'ADD', 'Role added', role, by.by, function(err) { if(err) console.log(err); });
            return callback(null, role);
        });
    },
    dataTable: function(query, callback) {
        var options = { conditions: query.conditions || {} };
        if (!options.conditions.deleted) options.conditions.deleted = false;
        Role.dataTable(query, options, callback);
    },
    update: function(roleData, callback) {
        var by = { by: undefined };
        if (roleData.updated) by.by = roleData.updated.by || undefined;
        Role.findByIdAndUpdate(roleData._id, roleData, function(err, role) {
            if (err) {
                console.log(err);
                Exception.log('ROLE', 'UPDATE', 'Role Update Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            Audit.log('ROLE', 'UPDATE', 'Role Updated', role, by.by, function(err) { if(err) console.log(err); });
            return callback(null, role);
        });
    },
    remove: function(roleData, callback) {
        var by = { by: undefined };
        if (roleData.updated) by.by = roleData.updated.by || undefined;
        roleData.deleted = true;
        Role.findByIdAndUpdate(roleData._id, roleData, function(err, role) {
            if (err) {
                console.log(err);
                Exception.log('ROLE', 'DELETE', 'Role Delete Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            Audit.log('ROLE', 'DELETE', 'Role Deleted', role, by.by, function(err) { if(err) console.log(err); });
            return callback(null, role);
        });
    }
};
