var express = require('express');
var role = express.Router();

var roleController = require('./role.controller.js');

role.get('/', roleController.get);
role.post('/data-table', roleController.dataTable);
role.post('/', roleController.add);
role.put('/', roleController.update);
role.put('/delete', roleController.remove);
module.exports = role;