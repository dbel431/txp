var Event = require('./notification.event.model.js');
var Audit = require('../../core/audit.log.service.js');
var Exception = require('../../core/exception.log.service.js');

module.exports = {
    get: function(query, callback) {
        if (typeof query.deleted == 'undefined') query.deleted = false;
        Event.find(query, callback);
    },
    add: function(eventData, callback) {
        var by = { by: undefined };
        if (eventData.created) by.by = eventData.created.by || undefined;
        Event.create(eventData, function(err, event) {
            if (err) {
                console.log(err);
                Exception.log('Event', 'ADD', 'Event add Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            if (!event) return callback(null, false, { message: 'Event not found' });
            Audit.log('Event', 'ADD', 'Event added', event, by.by, function(err) { if(err) console.log(err); });
            return callback(null, event);
        });
    },
    update: function(eventData, callback) {
        var by = { by: undefined };
        if (eventData.updated) by.by = eventData.updated.by || undefined;
        Event.findByIdAndUpdate(eventData._id, eventData, function(err, event) {
            if (err) {
                console.log(err);
                Exception.log('Event', 'UPDATE', 'Event Update Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            Audit.log('Event', 'UPDATE', 'Event Updated', event, by.by, function(err) { if(err) console.log(err); });
            return callback(null, event);
        });
    },
    remove: function(eventData, callback) {
        var by = { by: undefined };
        if (eventData.updated) by.by = eventData.updated.by || undefined;
        eventData.deleted = true;
        Event.findByIdAndUpdate(eventData._id, eventData, function(err, event) {
            if (err) {
                console.log(err);
                Exception.log('Event', 'DELETE', 'Event delete Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            Audit.log('Event', 'DELETE', 'Event deleted', event, by.by, function(err) { if(err) console.log(err); });
            return callback(null, event);
        });
    }
};