var eventService = require('./notification.event.service.js');

module.exports = {
    get: function(req, res) {
        var query = {};
        if (req.query) {
            if (req.query.id) {
                query._id = req.query.id;
                delete req.query.id;
            }
            for (var i in req.query) {
                query[i] = req.query[i];
            }
        }
        if (req.body) {
            if (req.body.id) {
                query._id = req.body.id;
                delete req.body.id;
            }
            for (var i in req.body) {
                query[i] = req.body[i];
            }
        }
        eventService.get(query, function(err, events) {
            if (err) return res.status(400).send(err);
            return res.send(events);
        });
    },
    add: function(req, res) {
        eventService.add(req.body, function(err, event) {
            if (err) return res.status(400).send(err);
            return res.send(event);
        });
    },
    update: function(req, res) {
        eventService.update(req.body, function(err, event) {
            if (err) return res.status(400).send(err);
            return res.send(event);
        });
    },
    remove: function(req, res) {
        eventService.remove(req.body, function(err, event) {
            if (err) return res.status(400).send(err);
            return res.send(event);
        });
    }
};
