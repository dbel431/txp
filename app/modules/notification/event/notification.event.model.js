var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TXN_EventSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
        required: true,
    },
    active: {
        type: Boolean,
        default: true
    },
    deleted: {
        type: Boolean,
        default: false
    },
    created: {
        by: {
            type: Schema.Types.ObjectId,
            ref: 'TXN_User',
        },
        at: {
            type: Date,
            default: Date.now
        }
    },
    updated: {
        by: {
            type: Schema.Types.ObjectId,
            ref: 'TXN_User',
        },
        at: {
            type: Date,
            default: Date.now
        }
    }
});

module.exports = mongoose.model('TXN_Event', TXN_EventSchema);
