var express = require('express');
var notification = express.Router();

var notificationEventController = require('./event/notification.event.controller.js');
var notificationTemplateController = require('./template/notification.template.controller.js');

notification.get('/event', notificationEventController.get);
notification.post('/event', notificationEventController.add);
notification.put('/event', notificationEventController.update);
notification.put('/event/delete', notificationEventController.remove);

notification.get('/template', notificationTemplateController.get);
notification.post('/template', notificationTemplateController.add);
notification.put('/template', notificationTemplateController.update);
notification.put('/template/delete', notificationTemplateController.remove);

module.exports = notification;
