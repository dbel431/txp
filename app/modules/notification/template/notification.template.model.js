var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TXN_TemplateSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
        required: true,
    },
    event: {
        type: Schema.Types.ObjectId,
        ref: 'TXN_Event',
    },
    active: {
        type: Boolean,
        default: true
    },
    deleted: {
        type: Boolean,
        default: true
    },
    created: {
        by: {
            type: Schema.Types.ObjectId,
            ref: 'TXN_User',
        },
        at: {
            type: Date,
            default: Date.now
        }
    },
    updated: {
        by: {
            type: Schema.Types.ObjectId,
            ref: 'TXN_User',
        },
        at: {
            type: Date,
            default: Date.now
        }
    }
});

module.exports = mongoose.model('TXN_Template', TXN_TemplateSchema);
