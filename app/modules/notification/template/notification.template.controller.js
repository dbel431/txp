var templateService = require('./notification.template.service.js');

module.exports = {
    get: function(req, res) {
        var query = {};
        if (req.query) {
            if (req.query.id) {
                query._id = req.query.id;
                delete req.query.id;
            }
            for (var i in req.query) {
                query[i] = req.query[i];
            }
        }
        if (req.body) {
            if (req.body.id) {
                query._id = req.body.id;
                delete req.body.id;
            }
            for (var i in req.body) {
                query[i] = req.body[i];
            }
        }
        templateService.get(query, function(err, templates) {
            if (err) return res.status(400).send(err);
            return res.send(templates);
        });
    },
    add: function(req, res) {
        templateService.add(req.body, function(err, template) {
            if (err) return res.status(400).send(err);
            return res.send(template);
        });
    },
    update: function(req, res) {
        templateService.update(req.body, function(err, template) {
            if (err) return res.status(400).send(err);
            return res.send(template);
        });
    },
    remove: function(req, res) {
        templateService.remove(req.body, function(err, template) {
            if (err) return res.status(400).send(err);
            return res.send(template);
        });
    }
};
