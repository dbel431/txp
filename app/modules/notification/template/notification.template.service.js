var Template = require('./notification.template.model.js');
var Audit = require('../../core/audit.log.service.js');
var Exception = require('../../core/exception.log.service.js');

module.exports = {
    get: function(query, callback) {
        if (typeof query.deleted == 'undefined') query.deleted = false;
        Template.find(query)
            .populate({
                path: 'event',
                match: {
                    active: true,
                    deleted: false
                }
            })
            .exec(callback);;
    },
    add: function(templateData, callback) {
        var by = { by: undefined };
        if (templateData.created) by.by = templateData.created.by || undefined;
        Template.create(templateData, function(err, template) {
            if (err) {
                console.log(err);
                Exception.log('Template', 'ADD', 'Template add Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            if (!template) return callback(null, false, { message: 'Template not found' });
            Audit.log('Template', 'ADD', 'Template added', template, by.by, function(err) { if(err) console.log(err); });
            return callback(null, template);
        });
    },
    update: function(templateData, callback) {
        var by = { by: undefined };
        if (templateData.updated) by.by = templateData.updated.by || undefined;
        Template.findByIdAndUpdate(templateData._id, templateData, function(err, template) {
            if (err) {
                console.log(err);
                Exception.log('Template', 'UPDATE', 'Template Update Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            Audit.log('Template', 'UPDATE', 'Template Updated', template, by.by, function(err) { if(err) console.log(err); });
            return callback(null, template);
        });
    },
    remove: function(templateData, callback) {
        var by = { by: undefined };
        if (templateData.updated) by.by = templateData.updated.by || undefined;
        templateData.deleted = true;

        Template.findByIdAndUpdate(templateData._id, templateData, function(err, template) {
            if (err) {
                console.log(err);
                Exception.log('Template', 'DELETE', 'Template delete Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            Audit.log('Template', 'DELETE', 'Template deleted', template, by.by, function(err) { if(err) console.log(err); });
            return callback(null, template);
        });
    }
};