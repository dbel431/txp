var RefCodeType = require('./refCodeType.model.js');
var Audit = require('../core/audit.log.service.js');
var Exception = require('../core/exception.log.service.js');

module.exports = {
    get: function(query, callback) {
        if (typeof query.deleted == 'undefined') query.deleted = false;
            RefCodeType.find(query).sort({ codeType: 1, _id: 1 }).exec(callback);
            //RefCodeType.find(query).limit(0).exec(callback);
    },
    add: function(refCodeTypeData, callback) {
        var by = { by: undefined };
        if (refCodeTypeData.from) by.by = refCodeTypeData.from || undefined;
        RefCodeType.create(refCodeTypeData, function(err, refCodeType) {
            if (err) {
                console.log(err);
                Exception.log('REF_CODE_TYPE', 'ADD', 'RefCodeType add Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            if (!refCodeType) return callback(null, false, { message: 'RefCodeType not found' });
            Audit.log('REF_CODE_TYPE', 'ADD', 'RefCodeType added', refCodeType, by.by, function(err) { if(err) console.log(err); });

            return callback(null, refCodeType);
        });
    },
    update: function(refCodeTypeData, callback) {
        var by = { by: undefined };
        if (refCodeTypeData.updated) by.by = refCodeTypeData.updated.by || undefined;
        RefCodeType.findByIdAndUpdate(refCodeTypeData._id, refCodeTypeData, function(err, refCodeType) {
            if (err) {
                console.log(err);
                Exception.log('REF_CODE_TYPE', 'UPDATE', 'RefCodeType Update Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            Audit.log('REF_CODE_TYPE', 'UPDATE', 'RefCodeType Updated', refCodeType, by.by, function(err) { if(err) console.log(err); });
            return callback(null, refCodeType);
        });
    },
    remove: function(refCodeTypeData, callback) {
        var by = { by: undefined };
        if (refCodeTypeData.updated) by.by = refCodeTypeData.updated.by || undefined;
        refCodeTypeData.deleted = true;
        RefCodeType.findByIdAndUpdate(refCodeTypeData._id, refCodeTypeData, function(err, refCodeType) {
            if (err) {
                console.log(err);
                Exception.log('REF_CODE_TYPE', 'DELETE', 'RefCodeType Delete Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            Audit.log('REF_CODE_TYPE', 'DELETE', 'RefCodeType Deleted', refCodeType, by.by, function(err) { if(err) console.log(err); });
            return callback(null, refCodeType);
        });
    }
};
