var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MST_RefCodeTypeSchema = new Schema({
    "codeType": String,
    "description": String,
    // apna cheej
    "belongsTo": {
        "identifier": String,
        "directories": [{
            "type": Schema.Types.ObjectId,
            "ref": 'MST_RefCodeValue'
        }]
    },
    // apna cheej khatam
    active: {
        type: Boolean,
        default: true
    },
    deleted: {
        type: Boolean,
        default: false
    },
    "created": {
        "by": {
            "type": Schema.Types.ObjectId,
            "ref": 'TXN_User'
        },
        "at": Date
    },
    "updated": {
        "by": {
            "type": Schema.Types.ObjectId,
            "ref": 'TXN_User'
        },
        "at": Date
    }
});

MST_RefCodeTypeSchema.index({ codeType: 1, _id: 1 });

module.exports = mongoose.model('MST_RefCodeType', MST_RefCodeTypeSchema);
