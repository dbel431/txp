var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MST_RefCodeValueSchema = new Schema({
    "parentId": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "directoryId": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "countyId": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeType'
    },
    "codeTypeId": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeType'
    },
    "codeValue": String,
    "shortName": String,
    "description": String,
    "sequenceNo": Number,
    "isUSTerritory": Boolean,
    // apna cheej
    "belongsTo": {
        "identifier": String,
        "directory": {
            "type": Schema.Types.ObjectId,
            "ref": 'MST_RefCodeType'
        },
        "directories": [{
            "type": Schema.Types.ObjectId,
            "ref": 'MST_RefCodeType'
        }]
    },
    // apna cheej khatam
    active: {
        type: Boolean,
        default: true
    },
    deleted: {
        type: Boolean,
        default: false
    },
    "created": {
        "by": {
            "type": Schema.Types.ObjectId,
            "ref": 'TXN_User'
        },
        "at": Date
    },
    "updated": {
        "by": {
            "type": Schema.Types.ObjectId,
            "ref": 'TXN_User'
        },
        "at": Date
    }
});

MST_RefCodeValueSchema.index({ codeValue: 1, description: 1, _id: 1 });

module.exports = mongoose.model('MST_RefCodeValue', MST_RefCodeValueSchema);
