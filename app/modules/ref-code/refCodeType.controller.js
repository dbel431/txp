var refCodeTypeService = require('./refCodeType.service.js');

module.exports = {
    get: function(req, res) {
        var query = {};
        
        if (req.query) {
            if (req.query.id) {
                query._id = req.query.id;
                delete req.query.id;
            }
            for (var i in req.query) {
                query[i] = req.query[i];
            }
        }
        if (req.body) {
            if (req.body.id) {
                query._id = req.body.id;
                delete req.body.id;
            }
            for (var i in req.body) {
                query[i] = req.body[i];
            }
        }
         
        refCodeTypeService.get(query, function(err, refCodeTypes) {
            if (err) return res.status(400).send(err);
            return res.send(refCodeTypes);
        });
    },
    add: function(req, res) {
        refCodeTypeService.add(req.body, function(err, refCodeType) {
            if (err) return res.status(400).send(err);
            return res.send(refCodeType);
        });
    },
    update: function(req, res) {
        refCodeTypeService.update(req.body, function(err, refCodeType) {
            if (err) return res.status(400).send(err);
            return res.send(refCodeType);
        });
    },
    remove: function(req, res) {
        refCodeTypeService.remove(req.body, function(err, refCodeType) {
            if (err) return res.status(400).send(err);
            return res.send(refCodeType);
        });
    }
};
