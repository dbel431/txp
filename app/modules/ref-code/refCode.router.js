var express = require('express');
var refCode = express.Router();

var refCodeTypeController = require('./refCodeType.controller.js');
var refCodeValueController = require('./refCodeValue.controller.js');

refCode.get('/type', refCodeTypeController.get);
refCode.post('/type', refCodeTypeController.add);
refCode.put('/type', refCodeTypeController.update);
refCode.put('/type/delete', refCodeTypeController.remove);

refCode.get('/value', refCodeValueController.get);
refCode.get('/count', refCodeValueController.getCount);
refCode.post('/value', refCodeValueController.add);
refCode.put('/value', refCodeValueController.update);
refCode.put('/value/delete', refCodeValueController.remove);

module.exports = refCode;
