var coreQueryBuilderService = require('../core/core.query-builder.service.js');
var refCodeValueService = require('./refCodeValue.service.js');

module.exports = {
    get: function(req, res) {
        var query = coreQueryBuilderService.buildQuery(req);
        refCodeValueService.get(query, function(err, refCodeValues) {
            if (err) return res.status(400).send(err);
            return res.send(refCodeValues);
        });
    },
    add: function(req, res) {
        refCodeValueService.add(req.body, function(err, refCodeValue) {
            if (err) return res.status(400).send(err);
            return res.send(refCodeValue);
        });
    },
    update: function(req, res) {
        refCodeValueService.update(req.body, function(err, refCodeValue) {
            if (err) return res.status(400).send(err);
            return res.send(refCodeValue);
        });
    },
    remove: function(req, res) {
        refCodeValueService.remove(req.body, function(err, refCodeValue) {
            if (err) return res.status(400).send(err);
            return res.send(refCodeValue);
        });
    },
    getCount: function(req, res) {
         var query = coreQueryBuilderService.buildQuery(req);
         //console.log("query  " + query);
        refCodeValueService.getCount(query, function(err, count) {
            //console.log("count aagya " + count);
            if (err) return res.status(400).send(err);
            return res.send({count : count});
        });
    }
};
