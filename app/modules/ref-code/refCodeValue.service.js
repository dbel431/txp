var RefCodeValue = require('./refCodeValue.model.js');
var Audit = require('../core/audit.log.service.js');
var Exception = require('../core/exception.log.service.js');

module.exports = {
    get: function (query, callback) {
        console.log(query);
        var select, limit, sort = { codeValue: 1, description: 1, _id: 1 };
        if (query._select) select = query._select;
        if (query._limit) limit = query._limit;
        if (query._sort) sort = query._sort;
        if (query._where) query = query._where;
        if (typeof query.deleted == 'undefined') query.deleted = false;
        var dbQuery = RefCodeValue.find(query).populate({
            path: 'parentId',
            match: {
                active: true,
                deleted: false
            },
            populate: {
                path: 'parentId',
                match: {
                    active: true,
                    deleted: false
                }
            }
        }).populate({
            path: 'codeTypeId',
            match: {
                active: true,
                deleted: false
            }
        });

        if (select) dbQuery.select(select);
        if (sort) dbQuery.sort(sort);
        if (limit) dbQuery.limit(limit);

        dbQuery.exec(callback);
    },
    getCount: function (query, callback) {
        if (typeof query.deleted == 'undefined') query.deleted = false;
        RefCodeValue.count(query).exec(callback);
    },
    add: function (refCodeValueData, callback) {
        var by = { by: undefined };
        if (refCodeValueData.from) by.by = refCodeValueData.from || undefined;
        RefCodeValue.create(refCodeValueData, function (err, refCodeValue) {
            if (err) {
                console.log(err);
                Exception.log('REF_CODE_VALUE', 'ADD', 'RefCodeValue add Error', err, by.by, function (err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            if (!refCodeValue) return callback(null, false, { message: 'RefCodeValue not found' });
            Audit.log('REF_CODE_VALUE', 'ADD', 'RefCodeValue added', refCodeValue, by.by, function (err) {
                if (err) console.log(err);
            });

            return callback(null, refCodeValue);
        });
    },
    update: function (refCodeValueData, callback) {
        var by = { by: undefined };
        if (refCodeValueData.updated) by.by = refCodeValueData.updated.by || undefined;
        RefCodeValue.findByIdAndUpdate(refCodeValueData._id, refCodeValueData, function (err, refCodeValue) {
            if (err) {
                console.log(err);
                Exception.log('REF_CODE_VALUE', 'UPDATE', 'RefCodeValue Update Error', err, by.by, function (err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            Audit.log('REF_CODE_VALUE', 'UPDATE', 'RefCodeValue Updated', refCodeValue, by.by, function (err) {
                if (err) console.log(err);
            });
            return callback(null, refCodeValue);
        });
    },
    remove: function (refCodeValueData, callback) {
        var by = { by: undefined };
        if (refCodeValueData.updated) by.by = refCodeValueData.updated.by || undefined;
        refCodeValueData.deleted = true;
        RefCodeValue.findByIdAndUpdate(refCodeValueData._id, refCodeValueData, function (err, refCodeValue) {
            if (err) {
                console.log(err);
                Exception.log('REF_CODE_VALUE', 'DELETE', 'RefCodeValue Delete Error', err, by.by, function (err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            Audit.log('REF_CODE_VALUE', 'DELETE', 'RefCodeValue Deleted', refCodeValue, by.by, function (err) {
                if (err) console.log(err);
            });
            return callback(null, refCodeValue);
        });
    }

};
