var express = require('express');
var webRequest = express.Router();

var webRequestController = require('./web-request.controller.js');

webRequest.get('/', webRequestController.get);
webRequest.post('/data-table', webRequestController.dataTable);
webRequest.post('/', webRequestController.add);
webRequest.put('/', webRequestController.update);
webRequest.put('/delete', webRequestController.remove);
module.exports = webRequest;