var coreQueryBuilderService = require('../core/core.query-builder.service.js');
var webRequestService = require('./web-request.service.js');

module.exports = {
    get: function(req, res) {
        var query = coreQueryBuilderService.buildQuery(req);
        webRequestService.get(query, function(err, webRequests) {
            if (err) return res.status(400).send(err);
            return res.send(webRequests);
        });
    },
    dataTable: function(req, res) {
        var query = coreQueryBuilderService.buildQuery(req);
        webRequestService.dataTable(query, function(err, webRequests) {
            if (err) return res.status(400).send(err);
            return res.send(webRequests);
        });
    },
    add: function(req, res) {
        webRequestService.add(req.body, function(err, webRequest) {
            if (err) return res.status(400).send(err);
            return res.send(webRequest);
        });
    },
    update: function(req, res) {
        webRequestService.update(req.body, function(err, webRequest) {
            if (err) return res.status(400).send(err);
            return res.send(webRequest);
        });
    },
    remove: function(req, res) {
        webRequestService.remove(req.body, function(err, webRequest) {
            if (err) return res.status(400).send(err);
            return res.send(webRequest);
        });
    }
};
