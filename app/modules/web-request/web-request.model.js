var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TXN_WebRequestSchema = new Schema({
    "requestType": String,
    "directoryId": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "direcoryName": String,
    "name": {
        "prefix": String,
        "first": String,
        "last": String,
        "middle": String,
        "suffix": String
    },
    "email": String,
    "title": String,
    "organization": String,
    "organizationtype": String,
    "address": String,
    "city": String,
    "state": String,
    "zip": String,
    "needs": String,
    "hearaboutus": String,
    "vendors": String,
    "comments": String,
    "subject": String,
    "details": String,
    "reasons": String,
    "phone": String,
    "friendsEmail": String,
    "emailStatus": Boolean,
    "data": Schema.Types.Mixed,
    "progress": {
        type: String,
        default: '1'
    },
    "adminComments": [{
        prevProgress: String,
        by: {
            type: Schema.Types.ObjectId,
            ref: 'TXN_User',
        },
        at: {
            type: Date,
            default: Date.now
        },
        comment: String
    }],
    "active": {
        type: Boolean,
        default: true
    },
    "deleted": {
        type: Boolean,
        default: false
    },
    "created": {
        "by": {
            type: Schema.Types.ObjectId,
            ref: 'TXN_User',
        },
        "at": {
            type: Date,
            default: Date.now
        }
    },
    "updated": {
        "by": {
            type: Schema.Types.ObjectId,
            ref: 'TXN_User'
        },
        "at": {
            type: Date,
            default: Date.now
        }
    }
});

module.exports = mongoose.model('TXN_WebRequest', TXN_WebRequestSchema);
