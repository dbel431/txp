var WebRequest = require('./web-request.model.js');
var Audit = require('../core/audit.log.service.js');
var Exception = require('../core/exception.log.service.js');
var Mailer = require('../core/mailer.service.js');

module.exports = {
    get: function(query, callback) {
        if (typeof query.deleted == 'undefined') query.deleted = false;
        WebRequest.find(query).populate({
            path: 'adminComments.by',
            match: { active: true, deleted: false }
        }).sort({ name: 1, _id: 1 }).exec(callback);
    },
    add: function(webRequestData, callback) {
        var by = { by: undefined };
        if (webRequestData.created) by.by = webRequestData.created.by || undefined;
        WebRequest.create(webRequestData, function(err, webRequest) {
            if (err) {
                console.log(err);
                Exception.log('WEB REQUEST', 'ADD', 'Web Request add Error', err, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            if (!webRequest) return callback(null, false, { message: 'Web Request not found' });
            Audit.log('WEB REQUEST', 'ADD', 'Web Request added', webRequest, by.by, function(err) {
                if (err) console.log(err);
            });
            if (webRequestData.requestType == "Comment") {

                // if (webRequestData.directoryId == '57189cd924d8bc65f4123bc3') {
                //     var name = webRequestData.name.first;
                //     var email = "Greetings " + name + "! <br><br> You recently gave us some really helpful comments about our product and Services. We really appreciate the time you <br>took to help us to improve our product/services.<br><br> If required our representative will contact you soon.  <br><br> Thanks!!<br><br> Best wishes, <br> National Register Publishing<br> <i>Customer Service <br> Treasured Works, LLC / National Register Publishing, <br> 430 Mountain Avenue Suite 403, New Providence, NJ 07974.<br> Phone - 908-517-0780 <br> Fax - 908-608-3012<br> Email -Sales@nrpdirect.com<br></i>";
                //     Mailer.sendMail("Treasured Works: Directory Management System", webRequestData.email, 'NRP: New Comments and Feedback Submission', '', email);
                // } else
                
                    var name = webRequestData.name.first;
                var email = "Greetings " + name + "! <br><br> You recently gave us some really helpful comments about our product and Services. We really appreciate the time you <br>took to help us to improve our product/services.<br><br> If required our representative will contact you soon.  <br><br> Thanks!!<br><br>DO NOT Respond to this email.<br>To respond choose here info@nrpdirect.com<br><br> Best wishes, <br> National Register Publishing<br> <i>Customer Service <br> Treasured Works, LLC / National Register Publishing, <br> 430 Mountain Avenue Suite 403, New Providence, NJ 07974.<br> Phone - 908-517-0780 <br> Fax - 908-608-3012<br> Email -Sales@nrpdirect.com<br></i>";
                Mailer.sendMail("Treasured Works: Directory Management System", webRequestData.email, 'NRP: Comments and Feedback Submission', '', email);
            } else if (webRequestData.requestType == "Email A Friend") {
                var name = webRequestData.name.first;

                var friendsEmail = "Greetings " + name + "! <br><br>" + webRequestData.detailsReq + "<br><br> Thanks again!!<br><br>DO NOT Respond to this email.<br>To respond choose here info@nrpdirect.com<br><br> Regards, <br> National Register Publishing<br> <i>Customer Service <br> Treasured Works, LLC / National Register Publishing, <br> 430 Mountain Avenue Suite 403, New Providence, NJ 07974.<br> Phone - 908-517-0780 <br> Fax - 908-608-3012<br> Email -Sales@nrpdirect.com<br></i>";
                Mailer.sendMail("Treasured Works: Directory Management System", webRequestData.friendsEmail, 'NRP: Listing Confirmation', '', friendsEmail);
                // Mailer.sendMail("Treasured Works: Directory Management System", webRequestData.friendsEmail, webRequestData.subject, '', webRequestData.details + ' <h2><p>Thanks for joining us!</p></h2><p>Regards,<br>Treasured Works.</p>');
            } else if (webRequestData.requestType == "Special Offers") {
                var name = webRequestData.email;
                var email = "Greetings " + name + "! <br><br> You have been successfully added to our mailing list, keeping you up-to-date with our latest news.<br> You can also hear about our latest developments by following Aorlowska@nrpdirect.com <br><br>DO NOT Respond to this email.<br>To respond choose here info@nrpdirect.com<br><br> Regards, <br> National Register Publishing<br> <i>Customer Service <br> Treasured Works, LLC / National Register Publishing, <br> 430 Mountain Avenue Suite 403, New Providence, NJ 07974.<br> Phone - 908-517-1116, ext. 410<br> Fax - 908-608-3012<br> Email -Aorlowska@nrpdirect.com<br></i>";
                //Mailer.sendMail("Treasured Works: Directory Management System", webRequestData.email, 'Treasured Works – Special Offer Subscription', '', '<h2><p>Thanks for joining us!</p></h2><p>Regards,<br>Treasured Works.</p>');
                Mailer.sendMail("Treasured Works: Directory Management System", webRequestData.email, 'NRP: Special Offer Subscription', '', email);
                //Mailer.sendMail("Treasured Works: Directory Management System", webRequestData.email, 'Treasured Works', '', '<h2><p>Your request has been submitted!</p></h2><p>Regards,<br>Treasured Works.</p>');
            } else if (webRequestData.requestType == "Request A Datacard") {
                //Mailer.sendMail("Treasured Works: Directory Management System", webRequestData.email, 'Treasured Works', '', '<h2><p>Thank you for your inquiry!</p></h2><p>Regards,<br>Treasured Works.</p>');
                var name = webRequestData.name.first;
                var email = "Greetings " + name + "! <br><br> Thank you for your inquiry! <br><br> A list representative will contact you soon. <br><br> Mean while if you have any question about our other product services, we invite you to call us immediately at 908-517-0780. <br><br>DO NOT Respond to this email.<br>To respond choose here info@nrpdirect.com<br><br> Regards, <br> National Register Publishing<br> <i>Customer Service <br> Treasured Works, LLC / National Register Publishing, <br> 430 Mountain Avenue Suite 403, New Providence, NJ 07974.<br> Phone - 908-517-1116, ext. 410 <br> Fax - 908-608-3012<br> Email -Aorlowska@nrpdirect.com<br></i>";
                Mailer.sendMail("Treasured Works: Directory Management System", webRequestData.email, 'NRP: Datacard Request Submission Confirmation', '', email);

            } else if (webRequestData.requestType == "Create Request For Information") {
                var name = webRequestData.name.first;
                var email = "Greetings " + name + "! <br><br> Thanks for request. <br><br> We have forwarded your request for information to service provider. <br><br> You will be contacted soon by service provider’s representative. <br><br>DO NOT Respond to this email.<br>To respond choose here info@nrpdirect.com<br><br> Regards, <br> National Register Publishing<br> <i>Customer Service <br> Treasured Works, LLC / National Register Publishing, <br> 430 Mountain Avenue Suite 403, New Providence, NJ 07974.<br> Phone - 908-517-0780 <br> Fax - 908-608-3012<br> Email -Sales@nrpdirect.com<br></i>";
                Mailer.sendMail("Treasured Works: Directory Management System", webRequestData.email, 'NRP: Thanks for showing interest in our Product and Services', '', email);

                // for (var i = 0; i < webRequestData.data.length; i++) {
                //     console.log(webRequestData.data[i].companyEmail);
                //     // Mailer.sendMail("Treasured Works: Directory Management System", webRequestData.email, 'Treasured Works', '', '<h2><p>Your request has been submitted!</p></h2><p>Regards,<br>Treasured Works.</p>');
                // };
                // Mailer.sendMail("Treasured Works: Directory Management System", webRequestData.email, 'Treasured Works', '', '<h2><p>Your request has been submitted!</p></h2><p>Regards,<br>Treasured Works.</p>');
            } else if (webRequestData.requestType == "NRP Inquiry") {
                //Mailer.sendMail("Treasured Works: Directory Management System", webRequestData.email, 'Treasured Works', '', '<h2><p>Your request has been submitted!</p></h2><p>Regards,<br>Treasured Works.</p>');
                var name = webRequestData.name.first;
                var email = "Greetings " + name + "! <br><br> You recently gave us some really helpful comments about our product and Services. We really appreciate the time you <br>took to help us to improve our product/services.<br><br> If required our representative will contact you soon.  <br><br> Thanks again!!<br><br>DO NOT Respond to this email.<br>To respond choose here info@nrpdirect.com<br><br> Best wishes, <br> National Register Publishing<br> <i>Customer Service <br> Treasured Works, LLC / National Register Publishing, <br> 430 Mountain Avenue Suite 403, New Providence, NJ 07974.<br> Phone - 908-517-0780 <br> Fax - 908-608-3012<br> Email -Sales@nrpdirect.com<br></i>";
                Mailer.sendMail("Treasured Works: Directory Management System", webRequestData.email, 'NRP: Comments and Feedbacks Submission', '', email);

            } else {
                //Mailer.sendMail("Treasured Works: Directory Management System", webRequestData.email, 'Treasured Works', '', '<h2><p>We will get back to you!</p></h2><p>Regards,<br>Treasured Works.</p>');
                var name = webRequestData.name.first;
                var email = "Greetings " + name + "! <br><br> Thanks for choosing us to advertise your product on our website/print media. Our representative will <br>contact you soon. <br><br> Mean while if you have any question about our product/advertisement services, we invite you to call us <br> immediately at 908-517-0780. <br><br> Once again Thank you for business opportunity. <br><br>DO NOT Respond to this email.<br>To respond choose here info@nrpdirect.com<br><br> Regards, <br> National Register Publishing<br> <i>Customer Service <br> Treasured Works, LLC / National Register Publishing, <br> 430 Mountain Avenue Suite 403, New Providence, NJ 07974.<br> Phone - 908-517-0780 <br> Fax - 908-608-3012<br> Email -Sales@nrpdirect.com<br></i>";
                Mailer.sendMail("Treasured Works: Directory Management System", webRequestData.email, 'NRP: Thanks for showing interest to Advertise with Us', '', email);
            }
            return callback(null, webRequest);
        });
    },
    dataTable: function(query, callback) {
        var options = { conditions: query.conditions || {} };
        if (!options.conditions.deleted) options.conditions.deleted = false;
        WebRequest.dataTable(query.dataTableQuery, options, callback);
    },
    update: function(webRequestData, callback) {
        var by = { by: undefined };
        if (webRequestData.updated) by.by = webRequestData.updated.by || undefined;
        WebRequest.findByIdAndUpdate(webRequestData._id,webRequestData, function(err, webRequest) {
            if (err) {






                
                console.log(err);
                Exception.log('WEB REQUEST', 'UPDATE', 'Web Request Update Error', err, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            Audit.log('WEB REQUEST', 'UPDATE', 'Web Request Updated', webRequest, by.by, function(err) {
                if (err) console.log(err);
            });
            return callback(null, webRequest);
        });
    },
    remove: function(webRequestData, callback) {
        var by = { by: undefined };
        if (webRequestData.updated) by.by = webRequestData.updated.by || undefined;
        webRequestData.deleted = true;
        WebRequest.findByIdAndUpdate(webRequestData._id, webRequestData, function(err, webRequest) {
            if (err) {
                console.log(err);
                Exception.log('WEB REQUEST', 'DELETE', 'Web Request Delete Error', err, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            Audit.log('WEB REQUEST', 'DELETE', 'Web Request Deleted', webRequest, by.by, function(err) {
                if (err) console.log(err);
            });
            return callback(null, webRequest);
        });
    }
};
