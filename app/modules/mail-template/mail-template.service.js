var MailTemplate = require('./mail-template.model.js');
var Audit = require('../core/audit.log.service.js');
var Exception = require('../core/exception.log.service.js');

module.exports = {
    get: function(query, callback) {
        if (typeof query.deleted == 'undefined') query.deleted = false;
        MailTemplate.find(query).sort({ name: 1, _id: 1 }).exec(callback);
    },
    add: function(mailTemplateData, callback) {
        var by = { by: undefined };
        if (mailTemplateData.created) by.by = mailTemplateData.created.by || undefined;
        MailTemplate.create(mailTemplateData, function(err, mailTemplate) {
            if (err) {
                console.log(err);
                Exception.log('MAIL TEMPLATE', 'ADD', 'Mail Template add Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            if (!mailTemplate) return callback(null, false, { message: 'MailTemplate not found' });
            Audit.log('MAIL TEMPLATE', 'ADD', 'Mail Template added', mailTemplate, by.by, function(err) { if(err) console.log(err); });
            return callback(null, mailTemplate);
        });
    },
    dataTable: function(query, callback) {
        var options = { conditions: query.conditions || {} };
        if (!options.conditions.deleted) options.conditions.deleted = false;
        MailTemplate.dataTable(query, options, callback);
    },
    update: function(mailTemplateData, callback) {
        var by = { by: undefined };
        if (mailTemplateData.updated) by.by = mailTemplateData.updated.by || undefined;
        MailTemplate.findByIdAndUpdate(mailTemplateData._id, mailTemplateData, function(err, mailTemplate) {
            if (err) {
                console.log(err);
                Exception.log('MAIL TEMPLATE', 'UPDATE', 'Mail Template Update Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            Audit.log('MAIL TEMPLATE', 'UPDATE', 'Mail Template Updated', mailTemplate, by.by, function(err) { if(err) console.log(err); });
            return callback(null, mailTemplate);
        });
    },
    remove: function(mailTemplateData, callback) {
        var by = { by: undefined };
        if (mailTemplateData.updated) by.by = mailTemplateData.updated.by || undefined;
        mailTemplateData.deleted = true;
        MailTemplate.findByIdAndUpdate(mailTemplateData._id, mailTemplateData, function(err, mailTemplate) {
            if (err) {
                console.log(err);
                Exception.log('MAIL TEMPLATE', 'DELETE', 'Mail Template Delete Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            Audit.log('MAIL TEMPLATE', 'DELETE', 'Mail Template Deleted', mailTemplate, by.by, function(err) { if(err) console.log(err); });
            return callback(null, mailTemplate);
        });
    }
};
