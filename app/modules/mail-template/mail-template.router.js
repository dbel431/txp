var express = require('express');
var mailTemplate = express.Router();

var mailTemplateController = require('./mail-template.controller.js');

mailTemplate.get('/', mailTemplateController.get);
mailTemplate.post('/data-table', mailTemplateController.dataTable);
mailTemplate.post('/', mailTemplateController.add);
mailTemplate.put('/', mailTemplateController.update);
mailTemplate.put('/delete', mailTemplateController.remove);
module.exports = mailTemplate;