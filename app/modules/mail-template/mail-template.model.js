var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TXN_MailTemplateSchema = new Schema({
    name: String,
    from: String,
    subject: String,
    body: String,
    active: {
        type: Boolean,
        default: true
    },
    deleted: {
        type: Boolean,
        default: false
    },
    created: {
        by: {
            type: Schema.Types.ObjectId,
            ref: 'TXN_User',
            default: null
        },
        at: {
            type: Date,
            default: Date.now
        }
    },
    updated: {
        by: {
            type: Schema.Types.ObjectId,
            ref: 'TXN_User',
            default: null
        },
        at: {
            type: Date,
            default: Date.now
        }
    }
});

module.exports = mongoose.model('TXN_MailTemplate', TXN_MailTemplateSchema);
