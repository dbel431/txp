var coreQueryBuilderService = require('../core/core.query-builder.service.js');
var mailTemplateService = require('./mail-template.service.js');

module.exports = {
    get: function(req, res) {
        var query = coreQueryBuilderService.buildQuery(req);
        mailTemplateService.get(query, function(err, mailTemplates) {
            if (err) return res.status(400).send(err);
            return res.send(mailTemplates);
        });
    },
    dataTable: function(req, res) {
        var query = coreQueryBuilderService.buildQuery(req);
        mailTemplateService.dataTable(query, function(err, mailTemplates) {
            if (err) return res.status(400).send(err);
            return res.send(mailTemplates);
        });
    },
    add: function(req, res) {
        mailTemplateService.add(req.body, function(err, mailTemplate) {
            if (err) return res.status(400).send(err);
            return res.send(mailTemplate);
        });
    },
    update: function(req, res) {
        mailTemplateService.update(req.body, function(err, mailTemplate) {
            if (err) return res.status(400).send(err);
            return res.send(mailTemplate);
        });
    },
    remove: function(req, res) {
        mailTemplateService.remove(req.body, function(err, mailTemplate) {
            if (err) return res.status(400).send(err);
            return res.send(mailTemplate);
        });
    }
};
