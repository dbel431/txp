var Mailbox = require('./mailbox.model.js');
var Audit = require('../core/audit.log.service.js');
var Exception = require('../core/exception.log.service.js');

module.exports = {
    get: function(query, callback) {
        if (typeof query.deleted == 'undefined') query.deleted = false;
        Mailbox
            .find(query)
            .populate({
                path: 'from',
                match: {
                    deleted: false
                }
            })
            .populate({
                path: 'to',
                match: {
                    deleted: false
                }
            })
            .populate({
                path: 'cc',
                match: {
                    deleted: false
                }
            })
            .populate({
                path: 'bcc',
                match: {
                    deleted: false
                }
            })
            .sort({ 'sent': -1 })
            .exec(callback);
    },
    add: function(mailboxData, callback) {
        var by = { by: undefined };
        if (mailboxData.from) by.by = mailboxData.from || undefined;
        Mailbox.create(mailboxData, function(err, mailbox) {
            if (err) {
                console.log(err);
                Exception.log('MAILBOX', 'ADD', 'Mailbox add Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            if (!mailbox) return callback(null, false, { message: 'Mailbox not found' });
            Audit.log('MAILBOX', 'ADD', 'Mailbox added', mailbox, by.by, function(err) { if(err) console.log(err); });

            return callback(null, mailbox);
        });
    },
    update: function(mailboxData, callback) {
        var by = { by: undefined };
        if (mailboxData.updated) by.by = mailboxData.updated.by || undefined;
        Mailbox.findByIdAndUpdate(mailboxData._id, mailboxData, function(err, mailbox) {
            if (err) {
                console.log(err);
                Exception.log('MAILBOX', 'UPDATE', 'Mailbox Update Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            Audit.log('MAILBOX', 'UPDATE', 'Mailbox Updated', mailbox, by.by, function(err) { if(err) console.log(err); });
            return callback(null, mailbox);
        });
    },
    remove: function(mailboxData, callback) {
        var by = { by: undefined };
        if (mailboxData.updated) by.by = mailboxData.updated.by || undefined;
        mailboxData.deleted = true;
        Mailbox.findByIdAndUpdate(mailboxData._id, mailboxData, function(err, mailbox) {
            if (err) {
                console.log(err);
                Exception.log('MAILBOX', 'DELETE', 'Mailbox Delete Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            Audit.log('MAILBOX', 'DELETE', 'Mailbox Deleted', mailbox, by.by, function(err) { if(err) console.log(err); });
            return callback(null, mailbox);
        });
    }
};
