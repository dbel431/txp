var express = require('express');
var mailbox = express.Router();

var mailboxController = require('./mailbox.controller.js');

mailbox.get('/', mailboxController.get);
mailbox.post('/', mailboxController.add);
mailbox.put('/', mailboxController.update);
mailbox.put('/delete', mailboxController.remove);
module.exports = mailbox;