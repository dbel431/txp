var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TXN_MailboxSchema = new Schema({
    from: {
        type: Schema.Types.ObjectId,
        ref: 'TXN_User'
    },
    to: [{
        type: Schema.Types.ObjectId,
        ref: 'TXN_User'
    }],
    cc: [{
        type: Schema.Types.ObjectId,
        ref: 'TXN_User'
    }],
    bcc: [{
        type: Schema.Types.ObjectId,
        ref: 'TXN_User'
    }],
    subject: String,
    body: String,
    sent: {
        type: Date,
        default: Date.now
    },
    read: [{
        type: Schema.Types.ObjectId,
        ref: 'TXN_User'
    }],
    deleted: {
        type: Boolean,
        default: false
    },
    updated: {
        by: {
            type: Schema.Types.ObjectId,
            ref: 'TXN_User'
        },
        at: {
            type: Date,
            default: Date.now
        }
    }
});

module.exports = mongoose.model('TXN_Mailbox', TXN_MailboxSchema);
