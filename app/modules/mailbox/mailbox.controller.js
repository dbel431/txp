var mailboxService = require('./mailbox.service.js');

module.exports = {
    get: function(req, res) {
        var query = {};
        if (req.query) {
            if (req.query.id) {
                query._id = req.query.id;
                delete req.query.id;
            }
            for (var i in req.query) {
                query[i] = req.query[i];
            }
        }
        if (req.body) {
            if (req.body.id) {
                query._id = req.body.id;
                delete req.body.id;
            }
            for (var i in req.body) {
                query[i] = req.body[i];
            }
        }
        mailboxService.get(query, function(err, mails) {
            if (err) return res.status(400).send(err);
            return res.send(mails);
        });
    },
    add: function(req, res) {
        mailboxService.add(req.body, function(err, mail) {
            if (err) return res.status(400).send(err);
            return res.send(mail);
        });
    },
    update: function(req, res) {
        mailboxService.update(req.body, function(err, mailbox) {
            if (err) return res.status(400).send(err);
            return res.send(mailbox);
        });
    },
    remove: function(req, res) {
        mailboxService.remove(req.body, function(err, mail) {
            if (err) return res.status(400).send(err);
            return res.send(mail);
        });
    }
};
