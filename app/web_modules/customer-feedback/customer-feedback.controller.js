var coreQueryBuilderService = require('../../modules/core/core.query-builder.service.js');
var customerFeedbackService = require('./customer-feedback.service.js');

module.exports = {
    get: function(req, res) {
       var query = coreQueryBuilderService.buildQuery(req);
        customerFeedbackService.get(query, function(err, customerFeedbacks) {
            if (err) return res.status(400).send(err);
            return res.send(customerFeedbacks);
        });
       //return res.send({"s":"helooo"});
    },
    dataTable: function(req, res) {
        var query = coreQueryBuilderService.buildQuery(req);
        customerFeedbackService.dataTable(query, function(err, customerFeedbacks) {
            if (err) return res.status(400).send(err);
            return res.send(customerFeedbacks);
        });
    },
    add: function(req, res) {
        
        customerFeedbackService.add(req.body, function(err, customerFeedback) {
            if (err) return res.status(400).send(err);
            return res.send(customerFeedback);
        });
    },
    update: function(req, res) {
        customerFeedbackService.update(req.body, function(err, customerFeedback) {
            if (err) return res.status(400).send(err);
            return res.send(customerFeedback);
        });
    },
    remove: function(req, res) {
        customerFeedbackService.remove(req.body, function(err, customerFeedback) {
            if (err) return res.status(400).send(err);
            return res.send(customerFeedback);
        });
    }
};
