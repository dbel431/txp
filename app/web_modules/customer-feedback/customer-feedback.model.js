var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var txn_customer_feedback = new Schema({
   
    "directoryId": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "direcoryName" : String,
    "name" : {
        "prefix" : String, 
        "first" : String, 
        "last" : String, 
        "middle" : String, 
        "suffix" : String
    },
    "customerPhoto" : String,
    "customerDetails1": String,
    "customerDetails2": String,
    "comments" : String,
    "commentsDate" : Date,
    "sequenceNo" : Number,
    "showHome" : Boolean,
    "active": Boolean,
    "deleted": Boolean,

    "created": {
        "by": {
            "type": Schema.Types.ObjectId,
            "ref": 'TXN_WebUser'
        },
        "at": {
            "type": Date,
            "default": Date.now
        }
    },
    "updated": {
        "by": {
            "type": Schema.Types.ObjectId,
            "ref": 'TXN_WebUser'
        },
        "at": {
            "type": Date,
            "default": Date.now
        }
    }

});

module.exports = mongoose.model('txn_customer_feedback', txn_customer_feedback);
