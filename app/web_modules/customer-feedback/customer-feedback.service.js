var CustomerFeedback = require('./customer-feedback.model.js');
var Audit = require('../../modules/core/audit.log.service.js');
var Exception = require('../../modules/core/exception.log.service.js');

module.exports = {
    get: function(query, callback) {
       if (typeof query.deleted == 'undefined') query.deleted = false;
        CustomerFeedback.find(query)
        .populate({ path: 'directoryId', match: { active: true, deleted: false } })
        .exec(function(err, customerFeedbackData) {
               // var customerFeedbackData = sortOrgs(customerFeedbackData);
                callback(err, customerFeedbackData);
        });
        //return callback(null, {customerFeedback:"sss"});
    },
    add: function(customerFeedbackData, callback) {
        var by = { by: undefined };
       
        if (customerFeedbackData.created) by.by = customerFeedbackData.created.by || undefined;
        CustomerFeedback.create(customerFeedbackData, function(err, customerFeedback) {
            if (err) {
                console.log(err);
                Exception.log('Customer Feedback', 'ADD', 'Customer Feedback add Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            if (!customerFeedback) return callback(null, false, { message: 'Customer Feedback not found' });
            Audit.log('Customer Feedback', 'ADD', 'Customer Feedback added', customerFeedback, by.by, function(err) { if(err) console.log(err); });
            return callback(null, customerFeedback);
        });
    },
    dataTable: function(query, callback) {
        var options = { conditions: query.conditions || {} };
        if (!options.conditions.deleted) options.conditions.deleted = false;
        CustomerFeedback.dataTable(query, options, callback);
    },
    update: function(customerFeedbackData, callback) {
        var by = { by: undefined };
        if (customerFeedbackData.updated) by.by = customerFeedbackData.updated.by || undefined;
        CustomerFeedback.findByIdAndUpdate(customerFeedbackData._id, customerFeedbackData, function(err, customerFeedback) {
            if (err) {
                console.log(err);
                Exception.log('Customer Feedback', 'UPDATE', 'Customer Feedback Update Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            Audit.log('Customer Feedback', 'UPDATE', 'Customer Feedback Updated', customerFeedback, by.by, function(err) { if(err) console.log(err); });
            return callback(null, customerFeedback);
        });
    },
    remove: function(customerFeedbackData, callback) {
        var by = { by: undefined };
        if (customerFeedbackData.updated) by.by = customerFeedbackData.updated.by || undefined;
        customerFeedbackData.deleted = true;
        CustomerFeedback.findByIdAndUpdate(customerFeedbackData._id, customerFeedbackData, function(err, customerFeedback) {
            if (err) {
                console.log(err);
                Exception.log('Customer Feedback', 'DELETE', 'CustomerFeedback Delete Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            Audit.log('Customer Feedback', 'DELETE', 'Customer Feedback Deleted', customerFeedback, by.by, function(err) { if(err) console.log(err); });
            return callback(null, customerFeedback);
        });
    }
};
