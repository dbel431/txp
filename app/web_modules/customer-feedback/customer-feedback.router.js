var express = require('express');
var customerFeedback = express.Router();

var customerFeedbackController = require('./customer-feedback.controller.js');
customerFeedback.get('/', customerFeedbackController.get);
customerFeedback.get('/', customerFeedbackController.get);
customerFeedback.post('/data-table', customerFeedbackController.dataTable);
customerFeedback.post('/', customerFeedbackController.add);
customerFeedback.put('/', customerFeedbackController.update);
customerFeedback.put('/delete', customerFeedbackController.remove);
module.exports = customerFeedback;