var coreQueryBuilderService = require('../../modules/core/core.query-builder.service.js');
var productsServicesService = require('./products-services.service.js');

module.exports = {
    get: function(req, res) {

        var query = coreQueryBuilderService.buildQuery(req);
         productsServicesService.get(query, function(err, productsServices) {
            if (err) return res.status(400).send(err);
            return res.send(productsServices);
        });
    },
    getGuide: function(req, res) {
        var d = new Date();
        req.body['startDate'] = {"$lte" : d.toISOString()};
        req.body['endDate'] = {"$gte" : d.toISOString()};
        var query = coreQueryBuilderService.buildQuery(req);
       
        productsServicesService.getGuide(query, function(err, productsServices) {
            console.log(productsServices);
            if (err) return res.status(400).send(err);
            return res.send(productsServices);
        });
    },
    getProductCategory: function(req, res) {
      
        var query = coreQueryBuilderService.buildQuery(req);
        productsServicesService.getProductCategory(query, function(err, productsServices) {
            if (err) return res.status(400).send(err);
            return res.send(productsServices);
        });
    },
    postProductSubCategory: function(req, res) {
       
        var query = req.body;//coreQueryBuilderService.buildQuery(req);
       //  console.log(query);
        productsServicesService.postProductSubCategory(query, function(err, productsServices) {
            if (err) return res.status(400).send(err);
            return res.send(productsServices);
        });
    },
    getProductCategoryHome: function(req, res) {
           
        var query = coreQueryBuilderService.buildQuery(req);
        productsServicesService.getProductCategoryHome(query, function(err, productsServices) {
            if (err) return res.status(400).send(err);
            return res.send(productsServices);
        });
    }
};
