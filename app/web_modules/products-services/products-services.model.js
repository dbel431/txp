var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/*var ProductsServicesSchema = new Schema({
    "ProductCategory": [{
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    }],
    "ProductCategoryName": String,
    "ProductCategoryIcon": String,

    "ProductSubCategory": [{
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    }],
    "ProductSubCategoryName": String,
    "CompanyListLogo": String,
    "CompanyDetailsLogo": String,
    "CompanyName": String,
    "CompanyAddress": {
        "Street1": String,
        "Street2": String,
        "City": String,
        "State": String,
        "Country": String,
        "Zip": String,
        "Telephone": String,
        "Fax": String
    },
    "CompanyEmail ": String,
    "CompanyWebsite": String,
    "CompanyTypeHeader": String,
    "CompanyHeaderDescription": String,
    "CompanyDescription": String,
    "CompanyContactHeader": String,
    "CompanyContactDetails": String,
    "sequenceNo": Number,
    "ProductSubCategoryNames": [String],
    "Personnel": [{
        "PersonnelHeader": String
    }, {
        "PersonnelImage": String,
        "PersonName": String,
        "PersonDesignation": String
    }],
    "CompanyAlsoAppearsIn": {
        "CompanyAlsoAppearsInHeader": String,
        "AppearSubCatogery": String
    }
});*/
var ProductsServicesSchema = new Schema({

    "org_id": Number,
    "entryId": Number,
    "directoryName": String,
    "directoryId": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "sequenceNo": Number,
    "adsTypeId": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "adsTypeName": String,
    "categories": [{
        "ProductCategory": {
            "type": Schema.Types.ObjectId,
            "ref": 'MST_RefCodeType'
        },
        "ProductCategoryName": String, 
        "ProductSubCategory": {
            "type": Schema.Types.ObjectId,
            "ref": 'MST_RefCodeType'
        },
        "ProductSubCategoryName": String,
        "ordering": Number
    }],
    "name": String,
    "companyName": String,
    "companyLocalTitle": String,
    "companyListLogo": String,
    "companyDetailsLogo": String,
    "status": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "adsEditId": Number,
    "userId": Number,
    "statusNote": String,
    "businessType": String,
    "sortName": String,
    "established": {
        "detail": String
    },
    "attention": String,
    "companyAddress": {
        "street1": String,
        "street2": String,
        "cityId": {
            "type": Schema.Types.ObjectId,
            "ref": 'MST_RefCodeValue'
        },
        "city": String,
        "stateId": {
            "type": Schema.Types.ObjectId,
            "ref": 'MST_RefCodeValue'
        },
        "stateAbbreviation": String,
        "state": String,
        "countryId": {
            "type": Schema.Types.ObjectId,
            "ref": 'MST_RefCodeValue'
        },
        "country": String,
        "zip": String
    },
    "location": [Number],
    "startDate": Date,
    "endDate": Date,
    "telephone": String,
    "companyFreePhone": String,
    "fax": String,
    "companyFreeFax": String,
    "companyEmail": String,
    "companyWebsite": String,
    "companyTypeHeader": String, // Default- "Type of Business:" 
    "companyHeaderDescription": String,
    "companyDescription": String,
    "metaDescription": String,
    "metaKeyword": String,
    "logoCaption": String,
    "companyContactHeader": String,
    "companyContactDetails": String,
    "flags": {
        "firstMail": Boolean,
        "nrm": Boolean,
        "otherSource": Boolean,
        "secondMail": Boolean,
        "mailer": String,
        "hotEmailUrl": Boolean,
        "paidIndex": Boolean
    }
    //done
    ,
    "adTrackNumber": Number,
    "personnel": [{
        "personId": Number,
        "personnel_id": Number,
        "adsId": Number,
        "responseCode": {
            "type": Schema.Types.ObjectId,
            "ref": 'MST_RefCodeValue'
        }, // same as OMD personnel title
        "responseCodeName": String,
        "personPrefix": String,
        "personName": String,
        "personLastName": String,
        "PersonNameSuffix": String,
        "personDesignation": String,
        "personEmail": String,
        "personPhone": String,
        "personFax": String,
    }]
    //done
    ,
    "state":  {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
     "cost": Number,
     "pay": Number,
    "lastStep": Number,
    "checkedOut": Number,
    "published": Number,
    "changed": Number,
    "approveDate": Date,
    "hits": Number,
    "logoHits": Number,
    "showHome": Boolean
});
module.exports = mongoose.model('products_service', ProductsServicesSchema);
