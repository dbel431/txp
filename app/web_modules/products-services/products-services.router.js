
var express = require('express');
var productsServices = express.Router();

var productsServicesController = require('./products-services.controller.js');

productsServices.get('/', productsServicesController.get);
productsServices.get('/getGuide', productsServicesController.getGuide);
productsServices.get('/getProductCategory', productsServicesController.getProductCategory);
productsServices.get('/postProductSubCategory', productsServicesController.postProductSubCategory);
productsServices.get('/getProductCategoryHome', productsServicesController.getProductCategoryHome);

module.exports = productsServices;


