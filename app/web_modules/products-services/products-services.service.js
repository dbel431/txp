var ProductsServices = require('./products-services.model.js');

var Exception = require('../../modules/core/exception.log.service.js');

module.exports = {

    get: function(query, callback) {
        // if (typeof query.deleted == 'undefined') query.deleted = false;
        ProductsServices.find({}, callback);

    },
    getGuide: function(query, callback) {
        //console.log(query);
        //console.log('sssss', query.ProductSubCategoryNames);
        /*  if (typeof query.$stateParams === 'undefined' || query.$stateParams === {}) {
              ProductsServices.find(query, callback);
          } else*/
        //query['$and']=[{ "startDate": { $lte:new Date().toISOString() } }, { $and: [ { "endDate": { $gte:new Date().toISOString() } } ] } ] ;
        query.published=1;
        if (query.ProductSubCategoryNames) {

            
            var qr={ 'directoryName': query.directoryName.toUpperCase(), 
                'categories': { $elemMatch: { 'ProductSubCategoryName': query.ProductSubCategoryNames } } };
                var d = new Date();
                qr.startDate={"$lte" : d.toISOString()};;
                qr.endDate = {"$gte" : d.toISOString()};

            ProductsServices.find(qr, callback);
        } else if (query.CompanyName) {            

                var d = new Date();
                var qr={ 'companyName': query.CompanyName, 'directoryName': query.directoryName};
                qr.startDate={"$lte" : d.toISOString()};;
                qr.endDate = {"$gte" : d.toISOString()};
                ProductsServices.find(qr, callback);
            var qr={ 'companyName': query.CompanyName, 'directoryName': query.directoryName};

        } else {
            
           //  var obj={};
           // // obj['$and']=[{ "startDate": { $lte:new Date().toISOString() } }, { $and: [ { "endDate": { $gte:new Date().toISOString() } } ] } ] ;
           //  obj.directoryName=query.directoryName.toUpperCase();
           //  obj['$or']=[{'categories.ProductSubCategoryName':query.companyName},{'companyName':query.companyName}];
           //  //query['categories.ProductSubCategoryName']

        ProductsServices.find(query, callback);
        }

        // console.log(query);
        // if (query.directoryId) {
        //     console.log(query);
        //     if (query.ProductSubCategoryName != 'undefined' && query.ProductCategoryName != 'undefined') {
        //         //ProductsServices.find({'directoryId':query.directoryId, 'categories': { $elemMatch: { 'ProductSubCategoryName': query.ProductSubCategoryNames } } }, callback);
        //         ProductsServices.find({ 'directoryId': query.directoryId, 'categories': { $elemMatch: { 'ProductSubCategoryName': query.ProductSubCategoryNames  } } }, callback);
        //     } else {

        //         ProductsServices.find({ 'directoryId': query.directoryId }, callback);

        //     }
        // } else if (query.ProductSubCategoryNames) {


        //     ProductsServices.find({ 'directoryName': query.directoryName, 'categories': { $elemMatch: { 'ProductSubCategoryName': query.ProductSubCategoryNames } } }, callback);

        // } else if (query.CompanyName) {
        //     // console.log({ 'directoryName': query.directoryName, 'categories': { $elemMatch: { 'ProductSubCategoryName': query.ProductSubCategoryNames } } });
        //     //, 'categories': { $elemMatch: { 'ProductSubCategoryName': query.ProductSubCategoryNames } }

        //     ProductsServices.find({ 'companyName': query.CompanyName, 'directoryName': query.directoryName }, callback);

        // }

    },
    getProductCategory: function(query, callback) {
        // if (typeof query.deleted == 'undefined') query.deleted = false;
        //ProductsServices.find(query, callback);
        // console.log(query);

        ProductsServices.aggregate(
            [
                { $match: query },
                { "$group": { "_id": { ProductCategory: "$ProductCategory", ProductCategoryName: "$ProductCategoryName", ProductCategoryIcon: "$ProductCategoryIcon" } } }
            ], callback);

    },
    postProductSubCategory: function(query, callback) {
        // if (typeof query.deleted == 'undefined') query.deleted = false;
        console.log(query);
        if (typeof query === 'undefined') {
            ProductsServices.aggregate(
                [
                    //{$match: query.$stateParams},
                    { "$group": { "_id": { ProductCategory: "$ProductCategory", ProductCategoryName: "$ProductCategoryName", ProductSubCategoryName: "$ProductSubCategoryName", ProductSubCategory: "$ProductSubCategory", ProductCategoryIcon: "$ProductCategoryIcon" } } }
                ], callback);
        } else {

            ProductsServices.aggregate(
                [
                    { $match: query },
                    { "$group": { "_id": { ProductCategory: "$ProductCategory", ProductCategoryName: "$ProductCategoryName", ProductSubCategoryName: "$ProductSubCategoryName", ProductSubCategory: "$ProductSubCategory", ProductCategoryIcon: "$ProductCategoryIcon" } } }
                ], callback);
        }


    },
    getProductCategoryHome: function(query, callback) {

        if (query.showHome) query.showHome = (query.showHome == 'true');
        ProductsServices.aggregate(
            [
                { $match: query },
                { "$group": { "_id": { ProductCategoryName: "$ProductCategoryName", sequenceNo:"$sequenceNo"  } } }
            ], callback);

    },
};
