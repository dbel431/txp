var packageService = require('./package.service.js');
var coreQueryBuilderService = require('../../modules/core/core.query-builder.service.js');

function sanitizeUser(package) {
    if (package.constructor === Array) {
        for (var i in package) {
            package[i] = sanitizeUser(package[i]);
        }
    } else {
        if (package.package) {
            package.package = sanitizeUser(package.package);
        }
        if (package.created && ('by' in package.created) && package.created.by) {
            package.created.by = sanitizeUser(package.created.by);
        }
        if (package.updated && ('by' in package.updated) && package.updated.by) {
            package.updated.by = sanitizeUser(package.updated.by);
        }
        
    }
  
    return package;
}

module.exports = {

      get: function(req, res) {
        var query = coreQueryBuilderService.buildQuery(req);
        packageService.get(query, function(err, package, info) {
            if (err) return res.status(400).send(err);
            return res.send(sanitizeUser(package));
        });
    },
    add: function(req, res) {
        packageService.add(req.body, function(err, package, info) {
            if (err) return res.status(400).send(err);
            if (!package) return res.status(403).send(info);
            return res.send(sanitizeUser(package));
        });
    },
    update: function(req, res) {
        packageService.update(req.body, function(err, package, info) {
            if (err) return res.status(400).send(err);
            if (!package) return res.status(403).send(info);
            return res.send(sanitizeUser(package));
        });
    },
    remove: function(req, res) {
        packageService.remove(req.body, function(err, package, info) {
            if (err) return res.status(400).send(err);
            if (!package) return res.status(403).send(info);
            return res.send(sanitizeUser(package));
        });
    }
};
