var express = require('express');
var package = express.Router();
var packageController = require('./package.controller.js');

package.get('/', packageController.get);
package.post('/', packageController.add);
package.put('/', packageController.update);
package.put('/delete', packageController.remove);
module.exports = package;
