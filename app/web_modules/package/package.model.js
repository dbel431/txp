var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TXN_PackageSchema = new Schema({

    "packageId": Number,
    "packageName": String,
    "packageDescription": String,
    "packageCost": Number,
    "sequenceNo": Number,
    "packageLink": String,
    "showHome": Boolean,
    "startDate": Date,
    "endDate": Date,
    "active": Boolean,
    "deleted": Boolean,
    "directoryId": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "direcoryName": String,
    "type": String,
    "searDataFileds": [{
        "label": String,
        "base": String,
        "ngModel": String,
        "name": String,
        "dataType": String
    }],

    "created": {
        "by": {
            "type": Schema.Types.ObjectId,
            "ref": 'TXN_WebUser'
        },
        "at": {
            "type": Date,
            "default": Date.now
        }
    },
    "updated": {
        "by": {
            "type": Schema.Types.ObjectId,
            "ref": 'TXN_WebUser'
        },
        "at": {
            "type": Date,
            "default": Date.now
        }
    }
});

module.exports = mongoose.model('TXN_Package', TXN_PackageSchema);
