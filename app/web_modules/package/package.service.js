var Package = require('./package.model.js');
var Audit = require('../../modules/core/audit.log.service.js');
var Exception = require('../../modules/core/exception.log.service.js');
var AccessLog = require('../../modules/core/access.log.service.js');

module.exports = {

    get: function(query, callback) {
        // if (typeof query.deleted == 'undefined') query.deleted = false;
        Package.find(query)
            .populate('created.by')
            .exec(callback);
    },
    add: function(packageData, callback) {
        var by = { by: undefined };
        if (packageData.created) by.by = packageData.created.by || undefined;
        var package = new Package(packageData);
        package.save(function(err, package) {
            if (err) {
                console.log(err);
                Exception.log('PACKAGE', 'ADD', 'Package add Error', err, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            if (!package) return callback(null, false, { message: 'No package found' })
            Audit.log('PACKAGE', 'ADD', 'Package added', package, by.by, function(err) {
                if (err) console.log(err);
            });
            console.log(null, package);
            return callback(null, package);
        });
    },
    update: function(packageData, callback) {
        var by = { by: undefined };
        if (packageData.updated) by.by = packageData.updated.by || undefined;
        Package.findById(packageData._id, function(err, package) {
            if (err) {
                console.log(err);
                Exception.log('PACKAGE', 'GET', 'Package get Error', err, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            if (!package) return callback(null, false, { message: 'No package found' });

            if (packageData.packageId) package.packageId = packageData.packageId;
            if (packageData.packageName) package.packageName = packageData.packageName;
            if (packageData.startDate) package.startDate = packageData.startDate;
            if (packageData.endDate) package.endDate = packageData.endDate;
            
            if (by.by) package.updated.by = by.by;
            package.updated.at = new Date();
            package.save(function(err, package) {
                if (err) {
                    console.log(err);
                    Exception.log('PACKAGE', 'UPDATE', 'Package update Error', err, by.by, function(err) {
                        if (err) console.log(err);
                    });
                    return callback(err);
                }
                Audit.log('PACKAGE', 'UPDATE', 'Package updated', package, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(null, package);

            });
        });
    },
    remove: function(packageData, callback) {
        var by = { by: undefined };
        if (packageData.updated) by.by = packageData.updated.by || undefined;
        packageData.deleted = true;
        Package.findByIdAndUpdate(packageData._id, packageData, function(err, package) {
            if (err) {
                console.log(err);
                Exception.log('PACKAGE', 'DELETE', 'Package delete Error', err, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            Audit.log('PACKAGE', 'DELETE', 'Package deleted', package, by.by, function(err) {
                if (err) console.log(err);
            });
            return callback(null, package);
        });
    }
};
