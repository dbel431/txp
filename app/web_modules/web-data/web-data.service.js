var webData = require('./web-data.model.js');
var Audit = require('../../modules/core/audit.log.service.js');
var Exception = require('../../modules/core/exception.log.service.js');
var PackagesData = require('../package/package.model.js');
var advertiseInquirie = require('./advertise-inquirie.model.js');
var util = require('util');
var Mailer = require('../../modules/core/mailer.service.js');
module.exports = {
    get: function(query, callback) {
      // console.log(query);
       if (typeof query.deleted == 'undefined') query.deleted = false;
        webData.find(query)
       // .populate({ path: 'directoryId', match: { active: true, deleted: false } })
        .populate({ path: 'package' ,match: { showHome: true,active: true, deleted: false } }).exec(function(err, webData) {
               // var menuData = sortOrgs(menuData);
                callback(err, webData);
        });
       //  return callback(null, {webData:"sss"});
    },
    addAdvertiseInquirie: function(advertiseInquirieData, callback) {
      //  console.log(advertiseInquirieData.data);
        var by = { by: undefined };
        if (advertiseInquirieData.created) by.by = advertiseInquirieData.created.by || undefined;
        advertiseInquirie.create(advertiseInquirieData, function(err, advertiseInquirie) {
            if (err) {
               
                Exception.log('Advertise Inquirie', 'ADD', 'Advertise Inquirie add Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            if (!advertiseInquirie) return callback(null, false, { message: 'Advertise Inquirie not found' });
            Audit.log('Advertise Inquirie', 'ADD', 'Advertise Inquirie added', advertiseInquirie, by.by, function(err) { if(err) console.log(err); });
            if (advertiseInquirieData.type == "Comment") {
                //Mailer.sendMail("Treasured Works: Directory Management System", advertiseInquirieData.email, 'Treasured Works', '', '<h2><p>Thanks for your Comments and Feedbacks!</p></h2><p>Regards,<br>Treasured Works.</p>');
                 var name=advertiseInquirieData.name.first;
                 var email="<h2>NRP: Comments and Feedback Submission</h2>Greetings "+name+"! <br><br> You recently gave us some really helpful comments about our product and Services. We really appreciate the time you <br>took to help us to improve our product/services.<br><br> If required our representative will contact you soon.  <br><br> Thanks again!!<br><br> Best wishes, <br> National Register Publishing<br> <i>Customer Service <br> Treasured Works, LLC / National Register Publishing, <br> 430 Mountain Avenue Suite 403, New Providence, NJ 07974.<br> Phone - 800-473-7020 <br> Mobile - 908-673-1189<br> Email -NRPsales@nationalregisterpublishing.com<br></i>";
                 Mailer.sendMail("Treasured Works: Directory Management System", advertiseInquirieData.email, 'Comments and Feedback', '',email);
            }
            else if(advertiseInquirieData.type == "Email A Friend") {
                 Mailer.sendMail("Treasured Works: Directory Management System",advertiseInquirieData.friendsEmail, advertiseInquirieData.subject, '', advertiseInquirieData.details +' <h2><p>Thanks for joining us!</p></h2><p>Regards,<br>Treasured Works.</p>');
            }
            else if(advertiseInquirieData.type == "Special Offers") {
                var name =advertiseInquirieData.email;
                var email="<h2>NRP: Special Offer Subscription</h2>Greetings "+name+"! <br><br> You have been successfully added to our mailing list, keeping you up-to-date with our latest news.<br> You can also hear about our latest developments by following NRPsales@nationalregisterpublishing.com <br><br> Regards, <br> National Register Publishing<br> <i>Customer Service <br> Treasured Works, LLC / National Register Publishing, <br> 430 Mountain Avenue Suite 403, New Providence, NJ 07974.<br> Phone - 800-473-7020<br> Mobile - 908-673-1189<br> Email -NRPsales@nationalregisterpublishing.com<br></i>";
                 //Mailer.sendMail("Treasured Works: Directory Management System", advertiseInquirieData.email, 'Treasured Works – Special Offer Subscription', '', '<h2><p>Thanks for joining us!</p></h2><p>Regards,<br>Treasured Works.</p>');
                 Mailer.sendMail("Treasured Works: Directory Management System", advertiseInquirieData.email, 'Special Offers', '',email);
                //Mailer.sendMail("Treasured Works: Directory Management System", advertiseInquirieData.email, 'Treasured Works', '', '<h2><p>Your request has been submitted!</p></h2><p>Regards,<br>Treasured Works.</p>');
            }
            else if(advertiseInquirieData.type == "Request A Datacard") {
                 //Mailer.sendMail("Treasured Works: Directory Management System", advertiseInquirieData.email, 'Treasured Works', '', '<h2><p>Thank you for your inquiry!</p></h2><p>Regards,<br>Treasured Works.</p>');
                   var name=advertiseInquirieData.name.first;
                 var email="<h2>NRP: Datacard Request Submission Confirmation</h2>Greetings "+name+"! <br><br> Thank you for your inquiry! <br><br> A list representative will contact you soon. <br><br> Mean while if you have any question about our other product services, we invite you to call us immediately at 555-5555-5555. <br><br> Regards, <br> National Register Publishing<br> <i>Customer Service <br> Treasured Works, LLC / National Register Publishing, <br> 430 Mountain Avenue Suite 403, New Providence, NJ 07974.<br> Phone - 800-473-7020 <br> Mobile - 908-673-1189<br> Email -agnes.orlowska@nationalregisterpublishing.com<br></i>";
                 Mailer.sendMail("Treasured Works: Directory Management System", advertiseInquirieData.email, 'Request A Datacard', '',email);

            }
            else if(advertiseInquirieData.type == "Create Request For Information") {
              for (var i = 0; i < advertiseInquirieData.data.length; i++) {
               console.log(advertiseInquirieData.data[i].companyEmail); 
              // Mailer.sendMail("Treasured Works: Directory Management System", advertiseInquirieData.email, 'Treasured Works', '', '<h2><p>Your request has been submitted!</p></h2><p>Regards,<br>Treasured Works.</p>');
              };
                 Mailer.sendMail("Treasured Works: Directory Management System", advertiseInquirieData.email, 'Treasured Works', '', '<h2><p>Your request has been submitted!</p></h2><p>Regards,<br>Treasured Works.</p>');
            }
            else if(advertiseInquirieData.type == "NRP Inquirie") {
                 //Mailer.sendMail("Treasured Works: Directory Management System", advertiseInquirieData.email, 'Treasured Works', '', '<h2><p>Your request has been submitted!</p></h2><p>Regards,<br>Treasured Works.</p>');
                var name=advertiseInquirieData.name.first;
                var email="<h2>NRP: Comments and Feedbacks Submission</h2>Greetings "+name+"! <br><br> You recently gave us some really helpful comments about our product and Services. We really appreciate the time you <br>took to help us to improve our product/services.<br><br> If required our representative will contact you soon.  <br><br> Thanks again!!<br><br> Best wishes, <br> National Register Publishing<br> <i>Customer Service <br> Treasured Works, LLC / National Register Publishing, <br> 430 Mountain Avenue Suite 403, New Providence, NJ 07974.<br> Phone - 908-517-0780 <br> Fax - 908-608-3012<br> Email -Sales@nrpdirect.com<br></i>";
                 Mailer.sendMail("Treasured Works: Directory Management System", advertiseInquirieData.email, 'NRP Inquiry', '',email);
                 
            }
            else{
                  //Mailer.sendMail("Treasured Works: Directory Management System", advertiseInquirieData.email, 'Treasured Works', '', '<h2><p>We will get back to you!</p></h2><p>Regards,<br>Treasured Works.</p>');
                   var name=advertiseInquirieData.name.first;
                 var email="<h2>NRP: Thanks for showing interest to Advertise with Us</h2>Greetings "+name+"! <br><br> Thanks for choosing us to advertise your product on our website/print media. Our representative will <br>contact you soon. <br><br> Mean while if you have any question about our product/advertisement services, we invite you to call us <br> immediately at 555-5555-5555. <br><br> Once again Thank you for business opportunity. <br><br> Regards, <br> National Register Publishing<br> <i>Customer Service <br> Treasured Works, LLC / National Register Publishing, <br> 430 Mountain Avenue Suite 403, New Providence, NJ 07974.<br> Phone - 800-473-7020 <br> Mobile - 908-673-1189<br> Email -NRPsales@nationalregisterpublishing.com<br></i>";
                 Mailer.sendMail("Treasured Works: Directory Management System", advertiseInquirieData.email, 'Advertise Inquiry', '',email);
            }

             

            return callback(null, advertiseInquirie);
        });

        
    },
    add: function(webDataData, callback) {
        var by = { by: undefined };
        if (webDataData.created) by.by = webDataData.created.by || undefined;
        webData.create(webDataData, function(err, webData) {
            if (err) {
                console.log(err);
                Exception.log('WebData', 'ADD', 'webData add Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            if (!webData) return callback(null, false, { message: 'webData not found' });
            Audit.log('WebData', 'ADD', 'webData added', webData, by.by, function(err) { if(err) console.log(err); });
            return callback(null, webData);
        });
    },
    dataTable: function(query, callback) {
        var options = { conditions: query.conditions || {} };
        if (!options.conditions.deleted) options.conditions.deleted = false;
        webData.dataTable(query, options, callback);
    },
    update: function(webDataData, callback) {
        var by = { by: undefined };
        if (webDataData.updated) by.by = webDataData.updated.by || undefined;
        webData.findByIdAndUpdate(webDataData._id, webDataData, function(err, webData) {
            if (err) {
                console.log(err);
                Exception.log('WebData', 'UPDATE', 'webData Update Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            Audit.log('WebData', 'UPDATE', 'webData Updated', webData, by.by, function(err) { if(err) console.log(err); });
            return callback(null, webData);
        });
    },
    remove: function(webDataData, callback) {
        var by = { by: undefined };
        if (webDataData.updated) by.by = webDataData.updated.by || undefined;
        webDataData.deleted = true;
        webData.findByIdAndUpdate(webDataData._id, webDataData, function(err, webData) {
            if (err) {
                console.log(err);
                Exception.log('WebData', 'DELETE', 'webData Delete Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            Audit.log('WebData', 'DELETE', 'webData Deleted', webData, by.by, function(err) { if(err) console.log(err); });
            return callback(null, webData);
        });
    }
};
