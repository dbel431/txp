var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Txn_AdvertiseInquirie = new Schema({

    "directoryId": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "direcoryName": String,

  //  "sequenceNo": Number,
    "name" : {
        "prefix" : String, 
        "first" : String, 
        "last" : String, 
        "middle" : String, 
        "suffix" : String
    },
    "email": String,
    "title": String,
    "organization": String,
    "organizationtype": String,
    "address": String,
    "city": String,
    "state": String,
    "zip": String,
    "needs": String,
    "hearaboutus": String,
    "vendors": String,
    "comments": String,
    "subject":String,
    "details":String,
    "type" : String,
    "reasons" : String,
    "phone" : String,
    "friendsEmail": String,
    "emailStatus" : Boolean,
    "data": Schema.Types.Mixed,
    "created": {
        "by": {
            type: Schema.Types.ObjectId,
            ref: 'TXN_User',
        },
        "at": {
            type: Date,
            default: Date.now
        }
    },
    "updated": {
        "by": {
            type: Schema.Types.ObjectId,
            ref: 'TXN_User'
        },
        "at": {
            type: Date,
            default: Date.now
        }
    }
});

module.exports = mongoose.model('Txn_Advertise_Inquirie', Txn_AdvertiseInquirie);
