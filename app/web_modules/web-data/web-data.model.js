var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Txn_WebData = new Schema({

    "directoryId": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "package": [{
        "type": Schema.Types.ObjectId,
        "ref": 'TXN_SubscriptionPackage'
    }],

    "direcoryName": String,
    "sequenceNo": Number,
    "pageName": String,
    "subSection": String,
    "sectionName": String,
    "sectionHeader": String,
    "sectionDescription1": String,
    "sectionDescription2": String,
    "sectionDescription3": String,
    "sectionDescription4": String,
    "subData": [{
        "name": String,
        "count": String
    }],
    "buttonTitle": String,
    "buttonLink": String,
    "sectionImage": String,
    "active": Boolean,
    "deleted": Boolean,
    "created": {
        "by": {
            type: Schema.Types.ObjectId,
            ref: 'TXN_User',
        },
        "at": {
            type: Date,
            default: Date.now
        }
    },
    "updated": {
        "by": {
            type: Schema.Types.ObjectId,
            ref: 'TXN_User'
        },
        "at": {
            type: Date,
            default: Date.now
        }
    }
});

module.exports = mongoose.model('Txn_WebData', Txn_WebData);
