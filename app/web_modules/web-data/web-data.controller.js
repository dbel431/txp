var coreQueryBuilderService = require('../../modules/core/core.query-builder.service.js');
var webDataService = require('./web-data.service.js');

module.exports = {
    get: function(req, res) {
       var query = coreQueryBuilderService.buildQuery(req);
        webDataService.get(query, function(err, webData) {
            if (err) return res.status(400).send(err);
            return res.send(webData);
        });
       //return res.send({"s":"helooo"});
    },
    dataTable: function(req, res) {
        var query = coreQueryBuilderService.buildQuery(req);
        webDataService.dataTable(query, function(err, webData) {
            if (err) return res.status(400).send(err);
            return res.send(webData);
        });
    },
    add: function(req, res) {
        webDataService.add(req.body, function(err, webData) {
            if (err) return res.status(400).send(err);
            return res.send(webData);
        });
    },  
    addAdvertiseInquirie: function(req, res) {
      // console.log(req.body);
        webDataService.addAdvertiseInquirie(req.body, function(err, advertiseInquirieData) {
            if (err) return res.status(400).send(err);
            //console.log('App advertiseInquirieData',advertiseInquirieData);
            return res.send(advertiseInquirieData);
        });
    },
    update: function(req, res) {
        webDataService.update(req.body, function(err, webData) {
            if (err) return res.status(400).send(err);
            return res.send(webData);
        });
    },
    remove: function(req, res) {
        webDataService.remove(req.body, function(err, webData) {
            if (err) return res.status(400).send(err);
            return res.send(webData);
        });
    }
};
