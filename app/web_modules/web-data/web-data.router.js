var express = require('express');
var webData = express.Router();

var webDataController = require('./web-data.controller.js');
webData.get('/', webDataController.get);
webData.get('/', webDataController.get);
webData.post('/data-table', webDataController.dataTable);
webData.post('/', webDataController.add);
webData.post('/addAdvertiseInquirie', webDataController.addAdvertiseInquirie);
webData.put('/', webDataController.update);
webData.put('/delete', webDataController.remove);
module.exports = webData;