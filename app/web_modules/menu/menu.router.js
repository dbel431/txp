var express = require('express');
var menu = express.Router();

var menuController = require('./menu.controller.js');
menu.get('/', menuController.get);
menu.get('/', menuController.get);
menu.post('/data-table', menuController.dataTable);
menu.post('/', menuController.add);
menu.put('/', menuController.update);
menu.put('/delete', menuController.remove);
module.exports = menu;