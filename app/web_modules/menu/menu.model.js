var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MST_Menu = new Schema({
   
    "directoryId": {
        "type": Schema.Types.ObjectId,
        "ref": 'MST_RefCodeValue'
    },
    "direcoryName" : String,
    "caption" : String,          //"Home"
    "sequenceNo": Number,
    "icon" : String,
    "showIcon": Boolean,
    "toolTip": String,
    "link" : String,
    "class" : String,
    "parentId" : Number,
    "subMenu" : Boolean,
    "subMenus" : [
        {
           
            "caption" : String,          
            "sequenceNo": Number,
            "icon" : String,
            "showIcon": Boolean,
            "toolTip": String,
            "link" : String,
            "class" : String,
            "active" :Boolean,
            "deleted": Boolean
        }
    ],
    "active": Boolean,
    "deleted": Boolean,
    created: {
        by: {
            type: Schema.Types.ObjectId,
            ref: 'TXN_User',
        },
        at: {
            type: Date,
            default: Date.now
        }
    },
    updated: {
        by: {
            type: Schema.Types.ObjectId,
            ref: 'TXN_User'
        },
        at: {
            type: Date,
            default: Date.now
        }
    }
});

module.exports = mongoose.model('MST_Menu', MST_Menu);
