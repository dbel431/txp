var coreQueryBuilderService = require('../../modules/core/core.query-builder.service.js');
var menuService = require('./menu.service.js');

module.exports = {
    get: function(req, res) {
       var query = coreQueryBuilderService.buildQuery(req);
        menuService.get(query, function(err, menus) {
            if (err) return res.status(400).send(err);
            return res.send(menus);
        });
       //return res.send({"s":"helooo"});
    },
    dataTable: function(req, res) {
        var query = coreQueryBuilderService.buildQuery(req);
        menuService.dataTable(query, function(err, menus) {
            if (err) return res.status(400).send(err);
            return res.send(menus);
        });
    },
    add: function(req, res) {
        
        menuService.add(req.body, function(err, menu) {
            if (err) return res.status(400).send(err);
            return res.send(menu);
        });
    },
    update: function(req, res) {
        menuService.update(req.body, function(err, menu) {
            if (err) return res.status(400).send(err);
            return res.send(menu);
        });
    },
    remove: function(req, res) {
        menuService.remove(req.body, function(err, menu) {
            if (err) return res.status(400).send(err);
            return res.send(menu);
        });
    }
};
