var Menu = require('./menu.model.js');
var Audit = require('../../modules/core/audit.log.service.js');
var Exception = require('../../modules/core/exception.log.service.js');

module.exports = {
    get: function(query, callback) {
        
       if (typeof query.deleted == 'undefined') query.deleted = false;
     
        Menu.find(query)
        .populate({ path: 'directoryId', match: { active: true, deleted: false } })
        .exec(function(err, menuData) {
               // var menuData = sortOrgs(menuData);
                callback(err, menuData);
        });
        // return callback(null, {menu:"sss"});
    },
    add: function(menuData, callback) {
        var by = { by: undefined };
       // console.log(menuData);
        if (menuData.created) by.by = menuData.created.by || undefined;
        Menu.create(menuData, function(err, menu) {
            if (err) {
                console.log(err);
                Exception.log('Menu', 'ADD', 'Menu add Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            if (!menu) return callback(null, false, { message: 'Menu not found' });
            Audit.log('Menu', 'ADD', 'Menu added', menu, by.by, function(err) { if(err) console.log(err); });
            return callback(null, menu);
        });
    },
    dataTable: function(query, callback) {
        var options = { conditions: query.conditions || {} };
        if (!options.conditions.deleted) options.conditions.deleted = false;
        Menu.dataTable(query, options, callback);
    },
    update: function(menuData, callback) {
        var by = { by: undefined };
        if (menuData.updated) by.by = menuData.updated.by || undefined;
        Menu.findByIdAndUpdate(menuData._id, menuData, function(err, menu) {
            if (err) {
                console.log(err);
                Exception.log('Menu', 'UPDATE', 'Menu Update Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            Audit.log('Menu', 'UPDATE', 'Menu Updated', menu, by.by, function(err) { if(err) console.log(err); });
            return callback(null, menu);
        });
    },
    remove: function(menuData, callback) {
        var by = { by: undefined };
        if (menuData.updated) by.by = menuData.updated.by || undefined;
        menuData.deleted = true;
        Menu.findByIdAndUpdate(menuData._id, menuData, function(err, menu) {
            if (err) {
                console.log(err);
                Exception.log('Menu', 'DELETE', 'Menu Delete Error', err, by.by, function(err) { if(err) console.log(err); });
                return callback(err);
            }
            Audit.log('Menu', 'DELETE', 'Menu Deleted', menu, by.by, function(err) { if(err) console.log(err); });
            return callback(null, menu);
        });
    }
};
