var express = require('express');
var authController = require('../auth/auth.controller.js');
var userController = require('./user.controller.js');
var user = express.Router();

user.use(authController.identifyToken, authController.authenticateToken);

user.get('/', userController.get);
user.post('/data-table', userController.dataTable);
user.post('/', userController.add);
user.put('/', userController.update);
user.delete('/', userController.remove);

module.exports = user;
