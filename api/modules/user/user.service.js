var Client = require('node-rest-client').Client;

module.exports = {
    get: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/user", args, successCallback);
        client.on('error', errorCallback);
    },
    dataTable: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/user/data-table", args, successCallback);
        client.on('error', errorCallback);
    },
    add: function(user, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: user,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/user", args, successCallback);
        client.on('error', errorCallback);
    },
    update: function(user, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: user,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/user", args, successCallback);
        client.on('error', errorCallback);
    },
    remove: function(user, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: user,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/user/delete", args, successCallback);
        client.on('error', errorCallback);
    },
    verifySecurityQuestion: function(user, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: user,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/user/verify-security-question", args, successCallback);
        client.on('error', errorCallback);
    },
    resetPassword: function(user, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: user,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/user/reset-password-manual", args, successCallback);
        client.on('error', errorCallback);
    }
};
