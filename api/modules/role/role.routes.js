var express = require('express');
var authController = require('../auth/auth.controller.js');
var roleController = require('./role.controller.js');
var role = express.Router();

role.use(authController.identifyToken, authController.authenticateToken);

role.get('/', roleController.get);
role.post('/data-table', roleController.dataTable);
role.post('/', roleController.add);
role.put('/', roleController.update);
role.delete('/', roleController.remove);

module.exports = role;
