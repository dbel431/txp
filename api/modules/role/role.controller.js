var roleService = require('./role.service.js');

module.exports = {
    get: function(req, res) {
        roleService.get(req.query, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    dataTable: function(req, res) {
        roleService.dataTable(req.body, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    add: function(req, res) {
        var role = req.body;
        if (req.userData) role.created = {
            by: req.userData._id,
            at: new Date()
        };
        roleService.add(role, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    update: function(req, res) {
        var role = req.body;
        if (req.userData) role.created = {
            by: req.userData._id,
            at: new Date()
        };
        roleService.update(req.query.id, role, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    remove: function(req, res) {
        var role = { _id: req.query.id };
        if (req.userData) role.updated = {
            by: req.userData._id,
            at: new Date()
        };
        roleService.remove(role, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    }
};
