var express = require('express');
var authController = require('../auth/auth.controller.js');
var todoController = require('./todo.controller.js');
var todo = express.Router();

todo.use(authController.identifyToken, authController.authenticateToken);

todo.get('/', todoController.get);
todo.post('/', todoController.add);
todo.put('/', todoController.update);
todo.delete('/', todoController.remove);

module.exports = todo;