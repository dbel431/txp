var todoService = require('./todo.service.js');

module.exports = {
    get: function(req, res) {
        if (!req.userData && !req.userData._id) return res.status(403).end();
        var query = req.query;
        query['created.by'] = req.userData._id;
        todoService.get(query, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    add: function(req, res) {
        var todo = req.body;
        if (req.userData) todo.created = {
            by: req.userData._id,
            at: new Date()
        };
        todoService.add(todo, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    update: function(req, res) {
        var todo = req.body;
        if (req.userData) todo.updated = {
            by: req.userData._id,
            at: new Date()
        };
        todoService.update(req.query.id, todo, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    remove: function(req, res) {
        var todo = { _id: req.query.id };
        if (req.userData) todo.updated = {
            by: req.userData._id,
            at: new Date()
        };
        todoService.remove(todo, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    }
};
