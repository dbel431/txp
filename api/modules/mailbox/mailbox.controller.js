var mailboxService = require('./mailbox.service.js');

module.exports = {
    get: function(req, res) {
        mailboxService.get(req.query, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    add: function(req, res) {
        var mailbox = req.body;
        mailboxService.add(mailbox, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    update: function(req, res) {
        var mailbox = req.body;
        if (req.userData) mailbox.updated = {
            by: req.userData._id,
            at: new Date()
        };
        mailboxService.update(req.query.id, mailbox, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    remove: function(req, res) {
        var mailbox = { _id: req.query.id };
        if (req.userData) mailbox.updated = {
            by: req.userData._id,
            at: new Date()
        };
        mailboxService.remove(mailbox, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    }
};
