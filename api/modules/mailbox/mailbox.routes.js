var express = require('express');
var authController = require('../auth/auth.controller.js');
var mailboxController = require('./mailbox.controller.js');
var mailbox = express.Router();

mailbox.use(authController.identifyToken, authController.authenticateToken);

mailbox.get('/', mailboxController.get);
mailbox.post('/', mailboxController.add);
mailbox.put('/', mailboxController.update);
mailbox.delete('/', mailboxController.remove);

module.exports = mailbox;
