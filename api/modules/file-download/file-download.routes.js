var express = require('express');
var authController = require('../auth/auth.controller.js');
var fileDownloadController = require('./file-download.controller.js');
var fileDownload = express.Router();

fileDownload.use(authController.identifyToken, authController.authenticateToken);

fileDownload.get('/', fileDownloadController.get);

module.exports = fileDownload;
