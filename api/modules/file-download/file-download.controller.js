var fileDownloadService = require('./file-download.service.js');

module.exports = {
    get: function(req, res) {
        fileDownloadService.download(req.query.filename, function(err, filename) {
            console.log('filename', filename);
            if (err) {
                console.log(err);
                return res.status(500).send(err);
            }
            return res.send({ filename: filename });
        });
    }
};
