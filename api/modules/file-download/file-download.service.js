var http = require('http');
var path = require('path');
var fs = require('fs');

var appPath = process.env.APP_HOST + ":" + process.env.APP_PORT + "/exports";

module.exports = {
    download: function(filename, callback) {
        var file = fs.createWriteStream(path.join(process.env.FILE_DOWNLOAD_PATH, filename));
        var request = http.get(appPath + "/" + filename, function(response) {
            response.pipe(file);
            file.on('finish', function() {
                file.close(function() {
                    callback(null, filename);
                }); // close() is async, call callback after close completes.
            });
        }).on('error', function(err) { // Handle errors
            fs.unlink(process.env.FILE_DOWNLOAD_PATH + "/" + filename); // Delete the file async. (But we don't check the result)
            if (callback) callback(err);
        });
    }
};
