var Client = require('node-rest-client').Client;

module.exports = {
    // get:function(req, res) {
    //      res.send('controller1023');
    // },
    get: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        //client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/email-template", args, successCallback);
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/emailTemplate", args,successCallback);
        client.on('error', errorCallback);
    },
    txn: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        //client.txn(process.env.APP_HOST + ":" + process.env.APP_PORT + "/email-template", args, successCallback);
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/emailTemplate/txn", args,successCallback);
        client.on('error', errorCallback);
    },
    add: function(query, successCallback, errorCallback) {
        console.log("in service",query);
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        //client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/email-template", args, successCallback);
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/emailTemplate", args,successCallback);
        client.on('error', errorCallback);
    },
    addtransaction: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        //client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/email-template", args, successCallback);
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/emailTemplate/addtransaction", args,successCallback);
        client.on('error', errorCallback);
    },
    update: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/emailTemplate", args, successCallback);
        client.on('error', errorCallback);
    }
};


/*
module.exports = {
        get:function(data) {
         return 101;

     },
    get: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
    },
    update: function(id, emailTemplate, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: emailTemplate,
            headers: {
                'Content-Type': 'application/json'
            }
        };
    }
};*/