var emailTemplateService = require('./email-template.service.js');

module.exports = {
    // get: emailTemplateService.get,
    get: function(req, res) {
        emailTemplateService.get(req.query, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    txn: function(req, res) {
        emailTemplateService.txn(req.query, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
     add: function(req, res) {
        // var emailTemplate = req.body;
        // if (req.userData) emailTemplate.created = {
        //     by: req.userData._id,
        //     at: new Date()
        // };
        // emailTemplateService.add(emailTemplate, function(data, result) {
        //     switch (result.statusCode) {
        //         case 200:
        //             return res.send(data);
        //         default:
        //             return res.status(result.statusCode).send(data);
        //     }
        // }, function(err) {
        //     return res.status(500).send(err);
        // });
        
        emailTemplateService.add(req.body, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    addtransaction: function(req, res) {
        emailTemplateService.addtransaction(req.body, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    update: function(req, res) {
        // var emailTemplate = req.body;
        // if (req.userData) emailTemplate.updated = {
        //     by: req.userData._id,
        //     at: new Date()
        // };
    //     emailTemplateService.update(req.query.id, emailTemplate, function(data, result) {
    //         switch (result.statusCode) {
    //             case 200:
    //                 return res.send(data);
    //             default:
    //                 return res.status(result.statusCode).send(data);
    //         }
    //     }, function(err) {
    //         return res.status(500).send(err);
    //     });

    // }
        emailTemplateService.update(req.query._id,function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(result);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    }
};
