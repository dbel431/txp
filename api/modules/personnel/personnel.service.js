var Client = require('node-rest-client').Client;

module.exports = {
    list: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/organization/personnel/list", args, successCallback);
        client.on('error', errorCallback);
    }
};
