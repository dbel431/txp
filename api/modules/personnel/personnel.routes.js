var express = require('express');
var authController = require('../auth/auth.controller.js');
var personnelController = require('./personnel.controller.js');
var personnel = express.Router();

personnel.use(authController.identifyToken, authController.authenticateToken);

personnel.post('/list', personnelController.list);

module.exports = personnel;
