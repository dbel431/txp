var webUserService = require('./web-user.service.js');

module.exports = {
    get: function(req, res) {
        webUserService.get(req.query, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    dataTable: function(req, res) {
        webUserService.dataTable(req.body, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    add: function(req, res) {
        var webUser = req.body;
        if (req.userData) webUser.created = {
            by: req.userData._id,
            at: new Date()
        };
        webUserService.add(webUser, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    update: function(req, res) {
        var webUser = req.body;
        if (req.userData) webUser.updated = {
            by: req.userData._id,
            at: new Date()
        };
        webUserService.update(req.query.id, webUser, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    confirmSubscription: function(req, res) {
        var webUser = req.body;
        if (req.userData) webUser.updated = {
            by: req.userData._id,
            at: new Date()
        };
        webUserService.confirmSubscription(req.query.id, webUser, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    remove: function(req, res) {
        var webUser = { _id: req.query.id };
        if (req.userData) webUser.updated = {
            by: req.userData._id,
            at: new Date()
        };
        webUserService.remove(webUser, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    }
};
