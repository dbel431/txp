var express = require('express');
var authController = require('../auth/auth.controller.js');
var webUserController = require('./web-user.controller.js');
var webUser = express.Router();

webUser.use(authController.identifyToken, authController.authenticateToken);

webUser.get('/', webUserController.get);
webUser.post('/data-table', webUserController.dataTable);
webUser.post('/', webUserController.add);
webUser.put('/', webUserController.update);
webUser.put('/confirm-subscription', webUserController.confirmSubscription);
webUser.delete('/', webUserController.remove);

module.exports = webUser;
