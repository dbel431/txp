var Client = require('node-rest-client').Client;

module.exports = {
    get: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/web-user", args, successCallback);
        client.on('error', errorCallback);
    },
    authenticateToken: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/web-user/token", args, successCallback);
        client.on('error', errorCallback);
    },
    list: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/web-user/list", args, successCallback);
        client.on('error', errorCallback);
    },
    login: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/web-user/login", args, successCallback);
        client.on('error', errorCallback);
    },
    logout: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/web-user/logout", args, successCallback);
        client.on('error', errorCallback);
    },
    dataTable: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/web-user/data-table", args, successCallback);
        client.on('error', errorCallback);
    },
    add: function(webUser, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: webUser,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/web-user", args, successCallback);
        client.on('error', errorCallback);
    },
    update: function(id, webUser, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: webUser,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/web-user", args, successCallback);
        client.on('error', errorCallback);
    },
    confirmSubscription: function(id, webUser, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: webUser,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/web-user/confirm-subscription", args, successCallback);
        client.on('error', errorCallback);
    },
    remove: function(webUser, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: webUser,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/web-user/delete", args, successCallback);
        client.on('error', errorCallback);
    },
    register: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/web-user/register", args, successCallback);
        client.on('error', errorCallback);
    },
    forgotPassword: function(webUser, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: webUser,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/web-user/forgot-password", args, successCallback);
        client.on('error', errorCallback);
    },
    changePassword: function(webUser, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: webUser,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/web-user/change-password", args, successCallback);
        client.on('error', errorCallback);
    }
};
