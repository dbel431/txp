var subscriptionPackageService = require('./subscription-package.service.js');

module.exports = {
    get: function(req, res) {
        subscriptionPackageService.get(req.query, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getSubscriberTypes: function(req, res) {
        subscriptionPackageService.getSubscriberTypes(req.query, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    list: function(req, res) {
        subscriptionPackageService.list(req.body, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    dataTable: function(req, res) {
        subscriptionPackageService.dataTable(req.body, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    add: function(req, res) {
        var subscriptionPackage = req.body;
        subscriptionPackage.created = {
            at: new Date()
        };
        subscriptionPackageService.add(subscriptionPackage, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    update: function(req, res) {
        var subscriptionPackage = req.body;
        subscriptionPackage.created = {
            at: new Date()
        };
        subscriptionPackageService.update(req.query.id, subscriptionPackage, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    remove: function(req, res) {
        var subscriptionPackage = { _id: req.query.id };
        subscriptionPackage.updated = {
            at: new Date()
        };
        subscriptionPackageService.remove(subscriptionPackage, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    }
};
