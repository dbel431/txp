var Client = require('node-rest-client').Client;

module.exports = {
    get: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/subscription-package", args, successCallback);
        client.on('error', errorCallback);
    },
    getSubscriberTypes: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/subscription-package/subscriber-type", args, successCallback);
        client.on('error', errorCallback);
    },
    list: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/subscription-package/list", args, successCallback);
        client.on('error', errorCallback);
    },
    dataTable: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/subscription-package/data-table", args, successCallback);
        client.on('error', errorCallback);
    },
    add: function(subscriptionPackage, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: subscriptionPackage,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/subscription-package", args, successCallback);
        client.on('error', errorCallback);
    },
    update: function(id, subscriptionPackage, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: subscriptionPackage,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/subscription-package", args, successCallback);
        client.on('error', errorCallback);
    },
    remove: function(subscriptionPackage, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: subscriptionPackage,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/subscription-package/delete", args, successCallback);
        client.on('error', errorCallback);
    }
};
