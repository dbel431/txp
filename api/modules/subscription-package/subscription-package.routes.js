var express = require('express');
var subscriptionPackageController = require('./subscription-package.controller.js');
var subscriptionPackage = express.Router();

subscriptionPackage.get('/', subscriptionPackageController.get);
subscriptionPackage.get('/subscriber-type', subscriptionPackageController.getSubscriberTypes);
subscriptionPackage.post('/data-table', subscriptionPackageController.dataTable);
subscriptionPackage.post('/list', subscriptionPackageController.list);
subscriptionPackage.post('/', subscriptionPackageController.add);
subscriptionPackage.put('/', subscriptionPackageController.update);
subscriptionPackage.delete('/', subscriptionPackageController.remove);

module.exports = subscriptionPackage;
