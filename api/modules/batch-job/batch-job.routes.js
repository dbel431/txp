var express = require('express');
var authController = require('../auth/auth.controller.js');
var batchJobController = require('./batch-job.controller.js');
var batchJob = express.Router();

batchJob.use(authController.identifyToken, authController.authenticateToken);

batchJob.get('/', batchJobController.get);
batchJob.post('/', batchJobController.add);
batchJob.put('/', batchJobController.update);
batchJob.delete('/', batchJobController.remove);

module.exports = batchJob;
