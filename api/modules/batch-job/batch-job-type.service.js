var Client = require('node-rest-client').Client;

module.exports = {
    get: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/batch-job-type", args, successCallback);
        client.on('error', errorCallback);
    },
    dataTable: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/batch-job-type/data-table", args, successCallback);
        client.on('error', errorCallback);
    },
    add: function(batchJob, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: batchJob,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/batch-job-type", args, successCallback);
        client.on('error', errorCallback);
    },
    update: function(id, batchJob, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: batchJob,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/batch-job-type", args, successCallback);
        client.on('error', errorCallback);
    },
    remove: function(batchJob, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: batchJob,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/batch-job-type/delete", args, successCallback);
        client.on('error', errorCallback);
    }
};
