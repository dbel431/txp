var batchJobService = require('./batch-job.service.js');

module.exports = {
    get: function(req, res) {
        batchJobService.get(req.query, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    dataTable: function(req, res) {
        batchJobService.dataTable(req.body, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    add: function(req, res) {
        var batchJob = req.body;
        if (req.userData) batchJob.created = {
            by: req.userData._id,
            at: new Date()
        };
        batchJobService.add(batchJob, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    update: function(req, res) {
        var batchJob = req.body;
        if (req.userData) batchJob.created = {
            by: req.userData._id,
            at: new Date()
        };
        batchJobService.update(req.query.id, batchJob, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    remove: function(req, res) {
        var batchJob = { _id: req.query.id };
        if (req.userData) batchJob.updated = {
            by: req.userData._id,
            at: new Date()
        };
        batchJobService.remove(batchJob, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    }
};
