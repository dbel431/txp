var express = require('express');
var authController = require('../web-auth/web-auth.controller.js');
var subscriberController = require('./subscriber.controller.js');
var organizationController = require('../organization/organization.controller.js');
var subscriber = express.Router();

subscriber.get('/', subscriberController.get);
subscriber.get('/profile', authController.identifyToken, authController.authenticateToken, subscriberController.profile);
subscriber.post('/data-table', subscriberController.dataTable);
subscriber.post('/list', subscriberController.list);
subscriber.post('/login', subscriberController.login);
subscriber.post('/logout', authController.identifyToken, subscriberController.logout);
subscriber.post('/forgot-password', subscriberController.forgotPassword);
subscriber.post('/register', subscriberController.register);
subscriber.post('/', subscriberController.add);
subscriber.put('/', authController.identifyToken, authController.authenticateToken, subscriberController.update);
subscriber.put('/change-password', authController.identifyToken, authController.authenticateToken, subscriberController.changePassword);
subscriber.delete('/', subscriberController.remove);

subscriber.get('/search', organizationController.get);
subscriber.post('/search/list', organizationController.list);
subscriber.post('/search/data-table', organizationController.dataTable);
subscriber.post('/search/exhibition/list', organizationController.getExhibitionList);
subscriber.post('/search/personnel/list', organizationController.getOrgIdFromPersonnel);
subscriber.post('/search/org-list', organizationController.getOrganizationList);
subscriber.post('/search/chapter-specification',organizationController.getChapterSpecification);
subscriber.post('/search/chapter-specificationData',organizationController.getChapterSpecificationData);


subscriber.post('/web-request', subscriberController.addWebRequest);

module.exports = subscriber;
