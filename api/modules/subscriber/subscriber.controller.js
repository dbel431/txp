var subscriberService = require('../web-user/web-user.service.js');
var webRequestService = require('../web-request/web-request.service.js');

module.exports = {
    get: function(req, res) {
        subscriberService.get(req.query, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    profile: function(req, res) {
        res.send(req.userData);
    },
    list: function(req, res) {
        subscriberService.list(req.body, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    login: function(req, res) {
        subscriberService.login(req.body, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    logout: function(req, res) {
        subscriberService.logout({ token: req.token, secret: req.secret }, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    dataTable: function(req, res) {
        subscriberService.dataTable(req.body, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    add: function(req, res) {
        var subscriber = req.body;
        subscriber.created = {
            at: new Date()
        };
        subscriberService.add(subscriber, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    addWebRequest: function(req, res) {
        var webRequest = req.body;
        webRequest.created = {
            at: new Date()
        };
        webRequestService.add(webRequest, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    update: function(req, res) {
        var subscriber = req.body;
        subscriber.updated = {
            by: req.userData._id,
            at: new Date()
        };
        subscriberService.update(req.query.id, subscriber, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    remove: function(req, res) {
        var subscriber = { _id: req.query.id };
        subscriber.updated = {
            at: new Date()
        };
        subscriberService.remove(subscriber, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    forgotPassword: function(req, res) {
        var subscriber = req.body;
        subscriberService.forgotPassword(subscriber, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    changePassword: function(req, res) {
        var subscriber = req.body;
        subscriber.updated = {
            by: req.userData._id,
            at: new Date()
        };
        subscriberService.changePassword(subscriber, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    register: function(req, res) {
        subscriberService.register(req.body, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    }
};
