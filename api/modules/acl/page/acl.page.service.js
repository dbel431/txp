var Client = require('node-rest-client').Client;

module.exports = {
    get: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/acl/page", args, successCallback);
        client.on('error', errorCallback);
    },
    // add: function(aclPage, successCallback, errorCallback) {
    //     var client = new Client();
    //     var args = {
    //         data: aclPage,
    //         headers: {
    //             'Content-Type': 'application/json'
    //         }
    //     };
    //     client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/acl/page", args, successCallback);
    //     client.on('error', errorCallback);
    // },
    // update: function(id, aclPage, successCallback, errorCallback) {
    //     var client = new Client();
    //     var args = {
    //         data: aclPage,
    //         headers: {
    //             'Content-Type': 'application/json'
    //         }
    //     };
    //     client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/acl/page", args, successCallback);
    //     client.on('error', errorCallback);
    // }
};
