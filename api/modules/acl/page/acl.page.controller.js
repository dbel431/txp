var aclPageService = require('./acl.page.service.js');

module.exports = {
    get: function(req, res) {
        aclPageService.get(req.query, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    // add: function(req, res) {
    //     var aclPage = req.body;
    //     if (req.userData) aclPage.created = {
    //         by: req.userData._id
    //     };
    //     aclPageService.add(aclPage, function(data, result) {
    //         switch (result.statusCode) {
    //             case 200:
    //                 return res.send(data);
    //             default:
    //                 return res.status(result.statusCode).send(data);
    //         }
    //     }, function(err) {
    //         return res.status(500).send(err);
    //     });
    // },
    // update: function(req, res) {
    //     var aclPage = req.body;
    //     if (req.userData) aclPage.updated = {
    //         by: req.userData._id
    //     };
    //     aclPageService.update(req.query.id, req.body, function(data, result) {
    //         switch (result.statusCode) {
    //             case 200:
    //                 return res.send(data);
    //             default:
    //                 return res.status(result.statusCode).send(data);
    //         }
    //     }, function(err) {
    //         return res.status(500).send(err);
    //     });
    // }
};
