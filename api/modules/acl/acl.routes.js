var express = require('express');
var authController = require('../auth/auth.controller.js');
var aclModuleController = require('./module/acl.module.controller.js');
var aclPageController = require('./page/acl.page.controller.js');
var aclActionController = require('./action/acl.action.controller.js');
var acl = express.Router();

acl.use(authController.identifyToken, authController.authenticateToken);

acl.get('/module', aclModuleController.get);
acl.get('/page', aclPageController.get);
acl.get('/action', aclActionController.get);

module.exports = acl;
