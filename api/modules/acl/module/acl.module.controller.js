var aclModuleService = require('./acl.module.service.js');

module.exports = {
    get: function(req, res) {
        aclModuleService.get(req.query, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    // add: function(req, res) {
    //     var aclModule = req.body;
    //     if (req.userData) aclModule.created = {
    //         by: req.userData._id
    //     };
    //     aclModuleService.add(aclModule, function(data, result) {
    //         switch (result.statusCode) {
    //             case 200:
    //                 return res.send(data);
    //             default:
    //                 return res.status(result.statusCode).send(data);
    //         }
    //     }, function(err) {
    //         return res.status(500).send(err);
    //     });
    // },
    // update: function(req, res) {
    //     var aclModule = req.body;
    //     if (req.userData) aclModule.updated = {
    //         by: req.userData._id
    //     };
    //     aclModuleService.update(req.query.id, req.body, function(data, result) {
    //         switch (result.statusCode) {
    //             case 200:
    //                 return res.send(data);
    //             default:
    //                 return res.status(result.statusCode).send(data);
    //         }
    //     }, function(err) {
    //         return res.status(500).send(err);
    //     });
    // }
};
