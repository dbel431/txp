var Client = require('node-rest-client').Client;

module.exports = {
    get: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/acl/module", args, successCallback);
        client.on('error', errorCallback);
    },
    // add: function(aclModule, successCallback, errorCallback) {
    //     var client = new Client();
    //     var args = {
    //         data: aclModule,
    //         headers: {
    //             'Content-Type': 'application/json'
    //         }
    //     };
    //     client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/acl/module", args, successCallback);
    //     client.on('error', errorCallback);
    // },
    // update: function(id, aclModule, successCallback, errorCallback) {
    //     var client = new Client();
    //     var args = {
    //         data: aclModule,
    //         headers: {
    //             'Content-Type': 'application/json'
    //         }
    //     };
    //     client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/acl/module", args, successCallback);
    //     client.on('error', errorCallback);
    // }
};
