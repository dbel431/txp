var express = require('express');
var authController = require('../auth/auth.controller.js');
var accessLogController = require('./access/log.access.controller.js');
var log = express.Router();

log.use(authController.identifyToken, authController.authenticateToken);

log.get('/access', accessLogController.get);

module.exports = log;