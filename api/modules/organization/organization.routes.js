var express = require('express');
var authController = require('../auth/auth.controller.js');
var directoryController = require('../directory/directory.controller.js');
var organizationController = require('./organization.controller.js');
var organization = express.Router();

organization.use(authController.identifyToken, authController.authenticateToken);

organization.get('/', organizationController.get);
organization.get('/version', organizationController.getOrganizationVersion);
organization.post('/list', organizationController.list);
organization.post('/export-to-excel', organizationController.exportToExcel);
organization.post('/data-table', organizationController.dataTable);

organization.post('/exhibition/list', organizationController.getExhibitionList);
organization.post('/personnel/list', organizationController.getOrgIdFromPersonnel);
organization.post('/org-list',organizationController.getOrganizationList);
organization.post('/chapter-specification',organizationController.getChapterSpecification);

organization.post('/', organizationController.add,
    directoryController.getCodeTypeForDirectoryRecordVersion,
    function(req, res, next) {
        req.query.codeValue = { '$in': ["1"] };
        next();
    }, organizationController.getVersion, organizationController.addVersion,
    organizationController.addComplete);

organization.put('/', organizationController.update,
    directoryController.getCodeTypeForDirectoryRecordVersion,
    function(req, res, next) {
        req.query.codeValue = { '$in': ["3"] };
        next();
    }, organizationController.getVersion, organizationController.addVersion,
    organizationController.addComplete);

organization.post('/send-for-approval',
    directoryController.getCodeTypeForDirectoryRecordVersion,
    function(req, res, next) {
        req.query.codeValue = { '$in': ["2"] };
        next();
    }, organizationController.getVersion, organizationController.updateVersion);

organization.post('/send-for-correction',
    directoryController.getCodeTypeForDirectoryRecordVersion,
    function(req, res, next) {
        req.query.codeValue = { '$in': ["4"] };
        next();
    }, organizationController.getVersion, organizationController.updateVersion);

// organization.post('/request-for-correction',
//     directoryController.getCodeTypeForDirectoryRecordVersion,
//     function(req, res, next) {
//         req.query.codeValue = { '$in': ["5"] };
//         next();
//     }, organizationController.getVersion, organizationController.updateVersion);

organization.post('/approve',
    directoryController.getCodeTypeForDirectoryRecordVersion,
    function(req, res, next) {
        req.query.codeValue = { '$in': ["6"] };
        next();
    }, organizationController.getVersion, organizationController.updateVersion);

organization.get('/summary-report-count', organizationController.getSummaryReportCount);

organization.get('/count',
    organizationController.getExhibitionCount,
    organizationController.getPersonnelCount,
    organizationController.getOrganizationCount,
    organizationController.getActivitiesCount,
    organizationController.getFacilitiesCount,
    organizationController.getCollectionCount,
    organizationController.countComplete);

organization.get('/organization-count', organizationController.getOrganizationCount1);

organization.get('/organization-count-ocd', organizationController.getOrganizationCountOCD);


organization.post('/submit-and-send-for-approval',
    organizationController.add,
    directoryController.getCodeTypeForDirectoryRecordVersion,
    function(req, res, next) {
        req.query.codeValue = { '$in': ["1"] };
        next();
    }, organizationController.getVersion, organizationController.addVersion,
    function(req, res, next) {
        req.query.codeValue = { '$in': ["2"] };
        next();
    }, organizationController.getVersion, organizationController.addVersion,
    organizationController.addComplete);

organization.post('/modify-and-send-for-approval',
    organizationController.update,
    directoryController.getCodeTypeForDirectoryRecordVersion,
    function(req, res, next) {
        req.query.codeValue = { '$in': ["3"] };
        next();
    }, organizationController.getVersion, organizationController.addVersion,
    function(req, res, next) {
        req.query.codeValue = { '$in': ["2"] };
        next();
    }, organizationController.getVersion, organizationController.addVersion,
    organizationController.addComplete);

organization.delete('/', organizationController.remove);

module.exports = organization;
