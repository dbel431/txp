var Client = require('node-rest-client').Client;

module.exports = {
    getChapterSpecificationData:function(query,success,error){
         var client = new Client();
         
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/organization/getChapterSpecificationData", args, success);
        client.on('error', error);
    },
    get: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/organization", args, successCallback);
        client.on('error', errorCallback);
    },
    getExhibitionCount: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/organization/exhibition/count", args, successCallback);
        client.on('error', errorCallback);
    },
    getSummaryReportCount: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/organization/summary-report-count", args, successCallback);
        client.on('error', errorCallback);
    },
    getPersonnelCount: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/organization/personnel/count", args, successCallback);
        client.on('error', errorCallback);
    },
    getActivitiesCount: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/organization/activities/count", args, successCallback);
        client.on('error', errorCallback);
    },

    getFacilitiesCount: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/organization/facilities/count", args, successCallback);
        client.on('error', errorCallback);
    },
    getCollectionCount: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/organization/collection/count", args, successCallback);
        client.on('error', errorCallback);
    },
    getOrganizationCount: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/organization/organization/count", args, successCallback);
        client.on('error', errorCallback);
    },
    getOrganizationCountOCD: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/organization/organization/count-ocd", args, successCallback);
        client.on('error', errorCallback);
    },
    getOrganizationVersion: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/organization/version", args, successCallback);
        client.on('error', errorCallback);
    },
    list: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/organization/list", args, successCallback);
        client.on('error', errorCallback);
    },
    getOrganizationList: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/organization/org-list", args, successCallback);
        client.on('error', errorCallback);
    },
    getExhibitionList: function(query, successCallback, errorCallback) {
        console.log("In Service");
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/organization/exhibition/list", args, successCallback);
        client.on('error', errorCallback);
    },
    getChapterSpecificationInService: function(query, successCallback, errorCallback) {
        console.log("In Service");
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/organization/chapter-specification", args, successCallback);
        client.on('error', errorCallback);
    },
    getOrgIdFromPersonnel: function(query, successCallback, errorCallback) {
        console.log("In API Service");
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/organization/personnel/list", args, successCallback);
        client.on('error', errorCallback);
    },
    exportToExcel: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/organization/export-to-excel", args, successCallback);
        client.on('error', errorCallback);
    },
    dataTable: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/organization/data-table", args, successCallback);
        client.on('error', errorCallback);
    },
    submitAndSendForApproval: function(organization, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: organization,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/organization/submit-and-send-for-approval", args, successCallback);
        client.on('error', errorCallback);
    },
    modifyAndSendForApproval: function(id, organization, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: organization,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/organization", args, successCallback);
        client.on('error', errorCallback);
    },
    add: function(organization, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: organization,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/organization", args, successCallback);
        client.on('error', errorCallback);
    },
    // addVersion: function(organizationVersions, successCallback, errorCallback) {
    //     var client = new Client();
    //     var args = {
    //         data: organizationVersions,
    //         headers: {
    //             'Content-Type': 'application/json'
    //         }
    //     };
    //     client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/organization/version", args, successCallback);
    //     client.on('error', errorCallback);
    // },
    update: function(id, organization, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: organization,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/organization", args, successCallback);
        client.on('error', errorCallback);
    },
    updateVersion: function(id, version, updated, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: {
                id: id,
                version: version,
                updated: updated
            },
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/organization/version", args, successCallback);
        client.on('error', errorCallback);
    },
    remove: function(organization, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: organization,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/organization/delete", args, successCallback);
        client.on('error', errorCallback);
    }
};
