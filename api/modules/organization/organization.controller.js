var refCodeValueService = require('../core/ref-code-value.service.js');
var organizationService = require('./organization.service.js');

module.exports = {
    getChapterSpecificationData:function(req,res){
         organizationService.getChapterSpecificationData(req.body,function(data,result){
            res.send(data);
         },function(err){
              res.send("err");
         });
    },
    get: function(req, res) {
        organizationService.get(req.query, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getExhibitionCount: function(req, res, next) {
        organizationService.getExhibitionCount(req.query, function(data, result) {
            if (!data) return res.status(400).end({});
            // if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.resultData = data.count;
                        req.resultObject = { exhibitions: req.resultData };
                        //  req.resultObject['exhibitions'] = req.resultData;
                        return next();
                    }
                    //return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getSummaryReportCount: function(req, res) {
        organizationService.getSummaryReportCount(req.query, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getPersonnelCount: function(req, res, next) {
        organizationService.getPersonnelCount(req.query, function(data, result) {
            if (!data) return res.status(400).end({});
            // if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.resultData = data.count;
                        req.resultObject.personnels = req.resultData;
                        //  req.resultObject['exhibitions'] = req.resultData;
                        return next();
                    }
                    //return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getActivitiesCount: function(req, res, next) {
        organizationService.getActivitiesCount(req.query, function(data, result) {
            if (!data) return res.status(400).end({});
            // if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.resultData = data.count;
                        req.resultObject.activities = req.resultData;
                        //  req.resultObject['exhibitions'] = req.resultData;
                        return next();
                    }
                    //return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getFacilitiesCount: function(req, res, next) {
        organizationService.getFacilitiesCount(req.query, function(data, result) {
            if (!data) return res.status(400).end({});
            // if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.resultData = data.count;
                        req.resultObject.facilities = req.resultData;
                        //  req.resultObject['exhibitions'] = req.resultData;
                        return next();
                    }
                    //return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }

        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getCollectionCount: function(req, res, next) {
        organizationService.getCollectionCount(req.query, function(data, result) {
            if (!data) return res.status(400).end({});
            // if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.resultData = data.count;
                        req.resultObject.collection = req.resultData;
                        //  req.resultObject['exhibitions'] = req.resultData;
                        return next();
                    }
                    //return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }

        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getOrganizationCount: function(req, res, next) {
        organizationService.getOrganizationCount(req.query, function(data, result) {
            if (!data) return res.status(400).end({});
            // if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.resultData = data.count;
                        req.resultObject.organization = req.resultData;
                        //  req.resultObject['exhibitions'] = req.resultData;
                        return next();
                    }
                    //return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getOrganizationCount1: function(req, res) {
        organizationService.getOrganizationCount(req.query, function(data, result) {
            if (!data) return res.status(400).end({});
            // if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.resultData = data.count;
                        req.resultObject = { organizations: req.resultData };
                        //  req.resultObject['exhibitions'] = req.resultData;
                        return res.status(result.statusCode).send(data);
                    }
                    //return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }

        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getOrganizationCountOCD: function(req, res) {
        organizationService.getOrganizationCountOCD(req.query, function(data, result) {
            if (!data) return res.status(400).end({});
            return res.status(result.statusCode).send(data);
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    countComplete: function(req, res) {
        return res.send(req.resultObject);
    },
    getOrganizationVersion: function(req, res) {
        organizationService.getOrganizationVersion(req.query, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    list: function(req, res) {
        organizationService.list(req.body, function(data, result) {

            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getOrganizationList: function(req, res) {
        organizationService.getOrganizationList(req.body, function(data, result) {

            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getExhibitionList: function(req, res) {
        console.log("In controller");
        organizationService.getExhibitionList(req.body, function(data, result) {

            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getChapterSpecification: function(req, res) {
        console.log("In controller");
        organizationService.getChapterSpecificationInService(req.body, function(data, result) {

            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getOrgIdFromPersonnel: function(req, res) {
        console.log("In API controller");
        organizationService.getOrgIdFromPersonnel(req.body, function(data, result) {

            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    exportToExcel: function(req, res) {
        var excelFields = [{
            label: 'Sr. No.',
            value: 'srno'
        }];
        organizationService.exportToExcel(req.body, function(orgData, result) {
            switch (result.statusCode) {
                case 200:
                    {
                        res.send(orgData);
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(orgData);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getVersion: function(req, res, next) {
        refCodeValueService.get(req.query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.orgRecordStatus = data;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    dataTable: function(req, res) {
        organizationService.dataTable(req.body, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    add: function(req, res, next) {
        var organization = req.body;
        if (req.userData) organization.created = {
            by: req.userData._id,
            at: new Date()
        };
        if (organization.personnel)
            for (var i = organization.personnel.length - 1; i >= 0; i--) {
                if (req.userData) organization.personnel[i].created = {
                    by: req.userData._id,
                    at: new Date()
                };
            }
        if (organization.exhibition)
            for (var i = organization.exhibition.length - 1; i >= 0; i--) {
                if (req.userData) organization.exhibition[i].created = {
                    by: req.userData._id,
                    at: new Date()
                };
            }
        if (organization.chapterSpecification)
            for (var i = organization.chapterSpecification.length - 1; i >= 0; i--) {
                if (req.userData) organization.chapterSpecification[i].created = {
                    by: req.userData._id,
                    at: new Date()
                };
            }
        req.queryErrors = [];
        organizationService.add(organization, function(data, result) {
            req.organization = data;
            switch (result.statusCode) {
                case 200:
                    {
                        req.organization = data.data;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    addVersion: function(req, res, next) {
        var updated;
        if (req.userData) updated = {
            by: req.userData._id,
            at: new Date()
        };
        req.organization = req.organization || req.body;
        var versions = req.orgRecordStatus,
            currentVersion = req.body.currentVersion || {},
            orgId = req.organization ? req.organization._id : req.body._id;
        if (orgId) {
            var recordVersions = [];
            for (var i = versions.length - 1; i >= 0; i--) {
                recordVersions[i] = {};
                recordVersions[i].organization = orgId;
                recordVersions[i].status = versions[i]._id;
                recordVersions[i].comments = currentVersion.comments || "";
                if (req.oldOrgRecord) recordVersions[i].data = req.oldOrgRecord;
                recordVersions[i].created = {
                    by: req.userData._id,
                    at: new Date()
                };
            }
            req.organization.version = recordVersions;
            organizationService.updateVersion([req.organization._id], recordVersions[0], updated, function(data, result) {
                switch (result.statusCode) {
                    case 200:
                        return next();
                    default:
                        return res.status(result.statusCode).send(data);
                }
            }, function(err) {
                return res.status(500).send(err);
            });
        } else
            return res.status(500).send({
                message: 'Could not find associating Organization for this version!'
            });
    },
    addComplete: function(req, res) {
        return res.send(req.organization || {});
    },
    update: function(req, res, next) {
        var organization = req.body;
        req.oldOrgRecord = organization._oldOrgRecord;
        delete organization._oldOrgRecord;
        if (req.userData) organization.updated = {
            by: req.userData._id,
            at: new Date()
        };
        if (organization.personnel)
            for (var i = organization.personnel.length - 1; i >= 0; i--) {
                if (req.userData) organization.personnel[i].updated = {
                    by: req.userData._id,
                    at: new Date()
                };
            }
        if (organization.exhibition)
            for (var i = organization.exhibition.length - 1; i >= 0; i--) {
                if (req.userData) organization.exhibition[i].updated = {
                    by: req.userData._id,
                    at: new Date()
                };
            }
        if (organization.chapterSpecification)
            for (var i = organization.chapterSpecification.length - 1; i >= 0; i--) {
                if (req.userData) organization.chapterSpecification[i].updated = {
                    by: req.userData._id,
                    at: new Date()
                };
            }
        organizationService.update(req.query.id, organization, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    req.organization = data.data;
                    break;
                    //return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    updateVersion: function(req, res, next) {
        var updated;
        if (req.userData) updated = {
            by: req.userData._id,
            at: new Date()
        };
        var version = req.body.version;
        version.status = req.orgRecordStatus[0]._id;
        organizationService.updateVersion(req.body.id, version, updated, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    remove: function(req, res) {
        var organization = { _id: req.query.id };
        if (req.userData) organization.updated = {
            by: req.userData._id,
            at: new Date()
        };
        organizationService.remove(organization, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    }
};
