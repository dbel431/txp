var Client = require('node-rest-client').Client;

module.exports = {
    get: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/ref-code/value", args, successCallback);
        client.on('error', errorCallback);
    },
    getCount: function(query, successCallback, errorCallback) {
        var client = new Client();
        
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/ref-code/count", args, successCallback);
        client.on('error', errorCallback);
    }
};
