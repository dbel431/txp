var response = {
    response: {},
    status: {}
};
module.exports = {
    success: function(dataBuffer, result) {
        var data = JSON.parse(dataBuffer.toString('utf8'));
        switch (result.statusCode) {
            case 200:
                {
                    response.status.success = true;
                    response.response = data;
                }
            default:
                {
                    response.status.success = false;
                    response.status.errorCode = data.error.code;
                    response.status.errorMessage = data.error.message;
                }
        }
        return res.status(result.statusCode).send(response);
    },
    error: function(err) {
        response.status.success = false;
        return res.status(500).send(err);
    }

};
