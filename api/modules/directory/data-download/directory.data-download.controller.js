var batchJobService = require('../../batch-job/batch-job.service.js');
var refCodeTypeService = require('../../core/ref-code-type.service.js');
var refCodeValueService = require('../../core/ref-code-value.service.js');
// var dataDownloadService = require('./directory.data-download.service.js');

module.exports = {
    get: function(req, res) {
        batchJobService.get(req.query, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getCodeTypeForBatchJobType: function(req, res, next) {
        var query = {
            active: true,
            deleted: false,
            codeType: 'BATCH JOB TYPE'
        };
        refCodeTypeService.get(query, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    {
                        req.query = {
                            codeTypeId: data ? data[0]._id : null,
                            active: true,
                            deleted: false,
                        };
                        return next();
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getCodeValueForBatchJobType: function(req, res, next) {
        var query = req.query;
        refCodeValueService.get(query, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    {
                        req.batchJobTypes = data;
                        return next();
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    dataTable: function(req, res) {
        batchJobService.dataTable(req.body, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    forceStartAllJobs: function(req, res) {
        batchJobService.forceStartAllJobs(function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    add: function(req, res) {
        var batchJob = req.body;
        batchJob.type = req.batchJobTypes[0];
        if (!batchJob.name) batchJob.name = req.body.directory.codeValue + ": Data Download Request";
        if (!batchJob.description) batchJob.description = req.body.directory.description + ": Data Download Request generated on " + (new Date()).toString();
        if (req.userData) batchJob.created = {
            by: req.userData._id,
            at: new Date()
        };

        batchJobService.add(batchJob, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    update: function(req, res) {
        var dataDownload = req.body;
        if (req.userData) dataDownload.created = {
            by: req.userData._id,
            at: new Date()
        };
        batchJobService.update(req.query.id, dataDownload, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    remove: function(req, res) {
        var dataDownload = { _id: req.query.id };
        if (req.userData) dataDownload.updated = {
            by: req.userData._id,
            at: new Date()
        };
        batchJobService.remove(dataDownload, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    }
};
