var express = require('express');
var authController = require('../../auth/auth.controller.js');
var dataDownloadController = require('./directory.data-download.controller.js');
var dataDownload = express.Router();

dataDownload.use(authController.identifyToken, authController.authenticateToken);

dataDownload.get('/', dataDownloadController.get);
dataDownload.post('/data-table', dataDownloadController.dataTable);
dataDownload.post('/force-start-all-jobs', dataDownloadController.forceStartAllJobs);
dataDownload.post('/',
    dataDownloadController.getCodeTypeForBatchJobType,
    function(req, res, next) {
        req.query.codeValue = "DIRECTORY DATA DOWNLOADS";
        return next();
    },
    dataDownloadController.getCodeValueForBatchJobType,
    dataDownloadController.add
);
dataDownload.put('/', dataDownloadController.update);
dataDownload.delete('/', dataDownloadController.remove);

module.exports = dataDownload;
