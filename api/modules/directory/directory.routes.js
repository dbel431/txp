var express = require('express');
var authController = require('../auth/auth.controller.js');
var directoryController = require('./directory.controller.js');
var directory = express.Router();

//directory.use(authController.identifyToken, authController.authenticateToken);

directory.get('/', directoryController.getCodeTypeForDirectory, directoryController.get);

directory.post('/city', directoryController.getCodeTypeForDirectoryCity, directoryController.get);

directory.get('/count', directoryController.getCodeTypeForDirectoryCity, directoryController.getCount, function(req, res, next) {
    req.resultObject = { cities: req.resultData };
    next();
}, directoryController.getCodeTypeForDirectoryState, directoryController.getCount, function(req, res, next) {
    req.resultObject['states'] = req.resultData;
    next();
}, directoryController.getCodeTypeForDirectoryCountry, directoryController.getCount, function(req, res, next) {
    req.resultObject['countries'] = req.resultData;
    next();
}, directoryController.countComplete);

directory.post('/state', directoryController.getCodeTypeForDirectoryState, directoryController.get);

directory.post('/country', directoryController.getCodeTypeForDirectoryCountry, directoryController.get);
directory.post('/county', directoryController.getCodeTypeForDirectoryCounty, directoryController.get);

directory.get('/address-type', directoryController.getDirectoryAddressTypes, directoryController.get);

directory.get('/feature-type', directoryController.getDirectoryFeatureTypes);

directory.get('/feature-code', directoryController.getCodeTypeForDirectoryFeatureType, directoryController.get);

directory.get('/record-status', directoryController.getCodeTypeForDirectoryRecordStatus, directoryController.get);

directory.get('/record-version', directoryController.getCodeTypeForDirectoryRecordVersion, directoryController.get);

directory.post('/title', directoryController.getDirectoryTitle, directoryController.get);

directory.post('/getProductCategory', directoryController.getProductCategory, directoryController.get);

directory.post('/getProductSubCategory', directoryController.getProductSubCategory, directoryController.get);

directory.get('/classification', directoryController.getClassificationCode, directoryController.get);

directory.get('/section', directoryController.getSection, directoryController.get);

directory.get('/listing-type', directoryController.getListingType, directoryController.get);

//directory.get('/dmmp-section', directoryController.getDMMPSection, directoryController.get);

directory.get('/pension-report-interval', directoryController.getPensionReportInterval, directoryController.get);

directory.get('/primarymarket', directoryController.getPrimaryMarketCode, directoryController.get);

directory.get('/advance-bill-type', directoryController.getAdvanceBillTypeCode, directoryController.get);

directory.get('/specification-type', directoryController.getSpecificationTypes);

directory.get('/specification-code', directoryController.getCodeTypeForDirectorySpecificationType, directoryController.get);

directory.get('/vendor-type', directoryController.getVendorType, directoryController.get);

directory.post('/vendorcity', directoryController.getCodeTypeForDirectoryVendorCity, directoryController.get);

directory.post('/vendorstate', directoryController.getCodeTypeForDirectoryVendorState, directoryController.get);
directory.get('/section-code', directoryController.getCodeTypeForDirectoryDMMPSectionCode, directoryController.get);

directory.get('/institution-category', directoryController.getCodeTypeForDirectoryInstitutionCategory, directoryController.get);

directory.get('/collection-category', directoryController.getCodeTypeForDirectoryCollectionCategory, directoryController.get);

//for OCD 2/07/2016
directory.get('/diocese-type', directoryController.getCodeTypeForDirectoryDioceseType, directoryController.get);
directory.get('/dioProvince-type', directoryController.getCodeTypeForDirectoryDioProvinceType, directoryController.get);
directory.get('/parish-status-type', directoryController.getCodeTypeForDirectoryParishStatusType, directoryController.get);
directory.get('/ethnicity-type', directoryController.getCodeTypeForDirectoryEthnicityType, directoryController.get);
directory.get('/school-type', directoryController.getCodeTypeForDirectorySchoolType, directoryController.get);
directory.get('/religious-type', directoryController.getCodeTypeForDirectoryReligiousType, directoryController.get);
directory.get('/grade-type', directoryController.getCodeTypeForDirectoryGradeType, directoryController.get);
directory.get('/statistics-type', directoryController.getCodeTypeForDirectoryStatisticsType, directoryController.get);
directory.get('/person-type', directoryController.getCodeTypeForDirectoryPersonType, directoryController.get);
directory.get('/degree-type', directoryController.getCodeTypeForDirectoryDegreeType, directoryController.get);
directory.get('/supplement-type', directoryController.getCodeTypeForDirectorySupplementType, directoryController.get);
directory.get('/organization-type', directoryController.getCodeTypeForDirectoryOrganizationType, directoryController.get);
directory.get('/contact-type', directoryController.getCodeTypeForDirectoryContactType, directoryController.get);
directory.get('/assignment-type', directoryController.getCodeTypeForDirectoryAssignmentType, directoryController.get);
//Direct Marketing Budget Disbursal 
directory.get('/disbursal-type', directoryController.getCodeTypeForDirectoryDisbursalType, directoryController.get);

//For CFS keyPersonnel Officers
directory.get('/officer-type', directoryController.getCodeTypeForDirectoryOfficerType, directoryController.get);

//For DMMP keypersonnel Responsibility /responsibility-type
directory.get('/responsibility-type', directoryController.getCodeTypeForDirectoryResponsibilityType, directoryController.get);

module.exports = directory;
