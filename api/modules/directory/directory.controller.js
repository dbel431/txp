var refCodeTypeService = require('../core/ref-code-type.service.js');
var refCodeValueService = require('../core/ref-code-value.service.js');

module.exports = {
    getCodeTypeForDirectory: function(req, res, next) {
        var query = {
            codeType: 'DIRECTORY',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        // req.query = {};
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getCodeTypeForDirectoryCity: function(req, res, next) {
        var query = {
            codeType: 'CITY',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.query = req.body;
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getCodeTypeForDirectoryState: function(req, res, next) {
        var query = {
            codeType: 'STATE',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.query = req.body;
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getCodeTypeForDirectoryCountry: function(req, res, next) {
        var query = {
            codeType: 'COUNTRY',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.query = req.body;
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getCodeTypeForDirectoryCounty: function(req, res, next) {
        var query = {
            codeType: 'COUNTY',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.query = req.body;
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getCodeTypeForDirectoryFacilityType: function(req, res, next) {
        var query = {
            codeType: 'FACILITY',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.query = req.body;
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getCodeTypeForDirectoryCategoryType: function(req, res, next) {
        var query = {
            codeType: 'CATEGORY',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.query = req.body;
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getDirectoryTitle: function(req, res, next) {
        var query = {
            codeType: 'TITLE',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.query = req.body;
                        req.query.codeTypeId = data[0]._id;
                        req.query['belongsTo.identifier'] = query.codeType;
                        req.query['belongsTo.directory'] = req.query.directoryId;
                        delete req.query.directoryId;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getProductCategory: function(req, res, next) {
        var query = {
            codeType: 'PRODUCT CATEGORY',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.query = req.body;
                        req.query.codeTypeId = data[0]._id;
                        req.query['belongsTo.identifier'] = query.codeType;
                        req.query['belongsTo.directory'] = req.query.directoryId;
                        delete req.query.directoryId;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getProductSubCategory: function(req, res, next) {
        var query = {
            codeType: 'PRODUCT SUB CATEGORY',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.query = req.body;
                        req.query.codeTypeId = data[0]._id;
                        req.query['belongsTo.identifier'] = query.codeType;
                        req.query['belongsTo.directory'] = req.query.directoryId;
                        delete req.query.directoryId;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getDirectoryAddressTypes: function(req, res, next) {
        var query = {
            codeType: 'ADDRESSTYPE',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        // req.query = {};
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getPensionReportInterval: function(req, res, next) {
        var query = {
            codeType: 'INTERVAL_TYPE',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        // req.query = {};
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getClassificationCode: function(req, res, next) {
        var query = {
            codeType: 'CLASSIFICATION',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        // req.query = {};
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getSection: function(req, res, next) {
        var query = {
            codeType: 'BOOK_SECTION',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        // req.query = {};
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getListingType: function(req, res, next) {
        var query = {
            codeType: 'LISTING TYPE',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        // req.query = {};
                        //  req.query.directoryId = req.query.directoryId;
                        req.query.codeTypeId = data[0]._id;
                        req.query.directoryId = req.query.directoryId;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getCodeTypeForDirectoryRecordStatus: function(req, res, next) {
        var query = {
            codeType: 'STATUS',
            'belongsTo.directories': req.query['belongsTo.directories'],
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        // req.query = {};
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getCodeTypeForDirectoryRecordVersion: function(req, res, next) {
        var query = {
            codeType: 'RECORD_VERSION',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        // req.query = {};
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getDirectoryFeatureTypes: function(req, res, next) {
        var query = {
            'belongsTo.identifier': 'FEATURE_TYPE',
            'belongsTo.directories': req.query.directoryId,
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end();
            res.status(result.statusCode).send(data);
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getCodeTypeForDirectoryFeatureType: function(req, res, next) {
        var query = {
            'belongsTo.identifier': 'FEATURE_TYPE',
            'belongsTo.directories': req.query.directoryId,
            active: true,
            deleted: false
        };

        if (req.query && req.query.codeType) query.codeType = req.query.codeType;
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end();
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.query = {};
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getPrimaryMarketCode: function(req, res, next) {
        var query = {
            codeType: 'PRIMARYMARKET',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {

            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        // req.query = {};
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getAdvanceBillTypeCode: function(req, res, next) {

        var query = {
            codeType: 'ADVANCE_BILL',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {

            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        // req.query = {};
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();

        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getSpecificationTypes: function(req, res, next) {
        var query = {
            'belongsTo.identifier': 'SPECIFICATION_TYPE',
            'belongsTo.directories': req.query.directoryId,
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end();
            res.status(result.statusCode).send(data);
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getCodeTypeForDirectorySpecificationType: function(req, res, next) {
        var query = {
            'belongsTo.identifier': 'SPECIFICATION_TYPE',
            'belongsTo.directories': req.query.directoryId,
            active: true,
            deleted: false
        };
        if (req.query && req.query.codeType) query.codeType = req.query.codeType;
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end();
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.query = {};
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getVendorType: function(req, res, next) {
        var query = {
            codeType: 'VENDORTYPE',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        // req.query = {};
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getCodeTypeForDirectoryVendorCity: function(req, res, next) {
        var query = {
            codeType: 'CITY',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.query = req.body;
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getCodeTypeForDirectoryVendorState: function(req, res, next) {
        var query = {
            codeType: 'STATE',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.query = req.body;
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    get: function(req, res) {

        refCodeValueService.get(req.query, function(data, result) {

            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getCount: function(req, res, next) {

        refCodeValueService.getCount(req.query, function(data, result) {

            if (!data) return res.status(400).end({});
            // if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.resultData = data.count;
                        return next();
                    }
                    //return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }

        }, function(err) {
            return res.status(500).send(err);
        });
    },
    countComplete: function(req, res) {
        return res.send(req.resultObject);
    },
    getCodeTypeForDirectoryDMMPSectionCode: function(req, res, next) {
        var query = {
            codeType: 'DMMP_SECTION',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getCodeTypeForDirectoryInstitutionCategory: function(req, res, next) {
        var query = {
            codeType: 'CATEGORY',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        // req.query = {};
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getCodeTypeForDirectoryCollectionCategory: function(req, res, next) {
        var query = {
            codeType: 'COLLECTION',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        // req.query = {};
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },

    //For OCD 2/07/2016

    getCodeTypeForDirectoryDioceseType: function(req, res, next) {
        var query = {
            codeType: 'DIOCESE TYPE',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getCodeTypeForDirectoryDioProvinceType: function(req, res, next) {
        var query = {
            codeType: 'DIO PROVINCE',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getCodeTypeForDirectoryParishStatusType: function(req, res, next) {
        var query = {
            codeType: 'PARISH STATUS',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getCodeTypeForDirectoryEthnicityType: function(req, res, next) {
        var query = {
            codeType: 'ETHNICITY',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getCodeTypeForDirectorySchoolType: function(req, res, next) {
        var query = {
            codeType: 'SCHOOL TYPE',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getCodeTypeForDirectoryReligiousType: function(req, res, next) {
        var query = {
            codeType: 'RELIGIOUS ORDER',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getCodeTypeForDirectoryGradeType: function(req, res, next) {
        var query = {
            codeType: 'GRADE TYPE',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getCodeTypeForDirectoryStatisticsType: function(req, res, next) {
        var query = {
            codeType: 'STATISTIC TYPE',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getCodeTypeForDirectoryPersonType: function(req, res, next) {
        var query = {
            codeType: 'PERSON TYPE',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getCodeTypeForDirectoryDegreeType: function(req, res, next) {
        var query = {
            codeType: 'DEGREE TYPE',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getCodeTypeForDirectorySupplementType: function(req, res, next) {
        var query = {
            codeType: 'SUPPLEMENT TYPE',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getCodeTypeForDirectoryOrganizationType: function(req, res, next) {
        var query = {
            codeType: 'ORGANIZATION TYPE',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getCodeTypeForDirectoryContactType: function(req, res, next) {
        var query = {
            codeType: 'CONTACT TYPE',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getCodeTypeForDirectoryAssignmentType: function(req, res, next) {
        var query = {
            codeType: 'ASSIGN TYPE',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },

    //for direct marketing budget disbursal 
    getCodeTypeForDirectoryDisbursalType: function(req, res, next) {
        var query = {
            codeType: 'MEDIA',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },

    //for CFS keyPersonnel Officers
    getCodeTypeForDirectoryOfficerType: function(req, res, next) {
        var query = {
            codeType: 'OFFICER TYPE',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },

    //For DMMP keyPersonnel responsibility
    getCodeTypeForDirectoryResponsibilityType: function(req, res, next) {
        var query = {
            codeType: 'RESPONSIBILITY CODES',
            active: true,
            deleted: false
        };
        refCodeTypeService.get(query, function(data, result) {
            if (!data) return res.status(400).end({});
            if (data.length == 0) return res.send([]);
            switch (result.statusCode) {
                case 200:
                    {
                        req.query.codeTypeId = data[0]._id;
                        req.query.active = true;
                        req.query.deleted = false;
                        break;
                    }
                default:
                    return res.status(result.statusCode).send(data);
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },


};
