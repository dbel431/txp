var webRequestService = require('./web-request.service.js');

module.exports = {
    get: function(req, res) {
        webRequestService.get(req.query, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    dataTable: function(req, res) {
        webRequestService.dataTable(req.body, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    add: function(req, res) {
        var webRequest = req.body;
        if (req.userData) webRequest.created = {
            by: req.userData._id,
            at: new Date()
        };
        webRequestService.add(webRequest, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    update: function(req, res) {
        var webRequest = req.body;
        if (req.userData) webRequest.created = {
            by: req.userData._id,
            at: new Date()
        };
        webRequestService.update(req.query.id, webRequest, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    remove: function(req, res) {
        var webRequest = { _id: req.query.id };
        if (req.userData) webRequest.updated = {
            by: req.userData._id,
            at: new Date()
        };
        webRequestService.remove(webRequest, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    }
};
