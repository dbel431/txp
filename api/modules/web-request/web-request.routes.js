var express = require('express');
var authController = require('../auth/auth.controller.js');
var webRequestController = require('./web-request.controller.js');
var webRequest = express.Router();

webRequest.use(authController.identifyToken, authController.authenticateToken);

webRequest.get('/', webRequestController.get);
webRequest.post('/data-table', webRequestController.dataTable);
webRequest.post('/', webRequestController.add);
webRequest.put('/', webRequestController.update);
webRequest.delete('/', webRequestController.remove);

module.exports = webRequest;
