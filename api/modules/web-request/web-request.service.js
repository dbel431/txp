var Client = require('node-rest-client').Client;

module.exports = {
    get: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/web-request", args, successCallback);
        client.on('error', errorCallback);
    },
    dataTable: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/web-request/data-table", args, successCallback);
        client.on('error', errorCallback);
    },
    add: function(webRequest, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: webRequest,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/web-request", args, successCallback);
        client.on('error', errorCallback);
    },
    update: function(id, webRequest, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: webRequest,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/web-request", args, successCallback);
        client.on('error', errorCallback);
    },
    remove: function(webRequest, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: webRequest,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/web-request/delete", args, successCallback);
        client.on('error', errorCallback);
    }
};
