var authService = require('../web-user/web-user.service.js');
module.exports = {
    identifyToken: function(req, res, next) {
        var bearerToken;
        var bearerHeader = req.headers["authorization"];
        if (typeof bearerHeader !== 'undefined') {
            var bearer = bearerHeader.split(" ");
            bearerSecret = bearer[1];
            bearerToken = bearer[2];
            req.secret = bearerSecret;
            req.token = bearerToken;
            next();
        } else {
            return res.sendStatus(403).end();
        }
    },
    authenticateToken: function(req, res, next) {
        authService.authenticateToken({
            secret: req.secret,
            token: req.token
        }, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    {
                        req.userData = data;
                        break;
                    }
                default:
                    {
                        return res.status(result.statusCode).send(data);
                    }
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    }
};
