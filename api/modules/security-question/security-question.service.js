var Client = require('node-rest-client').Client;

module.exports = {
    get: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/security-question", args, successCallback);
        client.on('error', errorCallback);
    },
    // add: function(securityQuestion, successCallback, errorCallback) {
    //     var client = new Client();
    //     var args = {
    //         data: securityQuestion,
    //         headers: {
    //             'Content-Type': 'application/json'
    //         }
    //     };
    //     client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/security-question", args, successCallback);
    //     client.on('error', errorCallback);
    // },
    // update: function(id, securityQuestion, successCallback, errorCallback) {
    //     var client = new Client();
    //     var args = {
    //         data: securityQuestion,
    //         headers: {
    //             'Content-Type': 'application/json'
    //         }
    //     };
    //     client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/security-question", args, successCallback);
    //     client.on('error', errorCallback);
    // },
    // remove: function(securityQuestion, successCallback, errorCallback) {
    //     var client = new Client();
    //     var args = {
    //         data: securityQuestion,
    //         headers: {
    //             'Content-Type': 'application/json'
    //         }
    //     };
    //     client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/security-question/delete", args, successCallback);
    //     client.on('error', errorCallback);
    // }
};
