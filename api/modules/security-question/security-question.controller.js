var securityQuestionService = require('./security-question.service.js');

module.exports = {
    get: function(req, res) {
        if (!req.userData && !req.userData._id) return res.status(403).end();
        var query = req.query;
        securityQuestionService.get(query, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    // add: function(req, res) {
    //     var securityQuestion = req.body;
    //     if (req.userData) securityQuestion.created = {
    //         by: req.userData._id,
    //         at: new Date()
    //     };
    //     securityQuestionService.add(securityQuestion, function(data, result) {
    //         switch (result.statusCode) {
    //             case 200:
    //                 return res.send(data);
    //             default:
    //                 return res.status(result.statusCode).send(data);
    //         }
    //     }, function(err) {
    //         return res.status(500).send(err);
    //     });
    // },
    // update: function(req, res) {
    //     var securityQuestion = req.body;
    //     if (req.userData) securityQuestion.updated = {
    //         by: req.userData._id,
    //         at: new Date()
    //     };
    //     securityQuestionService.update(req.query.id, securityQuestion, function(data, result) {
    //         switch (result.statusCode) {
    //             case 200:
    //                 return res.send(data);
    //             default:
    //                 return res.status(result.statusCode).send(data);
    //         }
    //     }, function(err) {
    //         return res.status(500).send(err);
    //     });
    // },
    // remove: function(req, res) {
    //     var securityQuestion = { _id: req.query.id };
    //     if (req.userData) securityQuestion.updated = {
    //         by: req.userData._id,
    //         at: new Date()
    //     };
    //     securityQuestionService.remove(securityQuestion, function(data, result) {
    //         switch (result.statusCode) {
    //             case 200:
    //                 return res.send(data);
    //             default:
    //                 return res.status(result.statusCode).send(data);
    //         }
    //     }, function(err) {
    //         return res.status(500).send(err);
    //     });
    // }
};
