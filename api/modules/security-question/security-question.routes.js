var express = require('express');
var authController = require('../auth/auth.controller.js');
var securityQuestionController = require('./security-question.controller.js');
var securityQuestion = express.Router();

securityQuestion.use(authController.identifyToken, authController.authenticateToken);

securityQuestion.get('/', securityQuestionController.get);
// securityQuestion.post('/', securityQuestionController.add);
// securityQuestion.put('/', securityQuestionController.update);
// securityQuestion.delete('/', securityQuestionController.remove);

module.exports = securityQuestion;
