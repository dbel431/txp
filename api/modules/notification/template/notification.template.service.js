var Client = require('node-rest-client').Client;

module.exports = {
    get: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/notification/template", args, successCallback);
        client.on('error', errorCallback);
    },
    add: function(template, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: template,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/notification/template", args, successCallback);
        client.on('error', errorCallback);
    },
    update: function(template, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: template,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/notification/template", args, successCallback);
        client.on('error', errorCallback);
    },
    remove: function(template, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: template,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/notification/template/delete", args, successCallback);
        client.on('error', errorCallback);
    }
};
