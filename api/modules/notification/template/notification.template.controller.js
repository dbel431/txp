var templateService = require('./notification.template.service.js');

module.exports = {
    get: function(req, res) {
        templateService.get(req.query, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    add: function(req, res) {
        var template = req.body;
        if (req.userData) template.created = {
            by: req.userData._id,
            at: new Date()
        };
        templateService.add(template, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    update: function(req, res) {
        var template = req.body;
        if (req.userData) template.created = {
            by: req.userData._id,
            at: new Date()
        };
        templateService.update(template, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    remove: function(req, res) {
        var template = { _id: req.query.id };
        if (req.userData) template.created = {
            by: req.userData._id,
            at: new Date()
        };
        templateService.remove(template, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    }
};
