var Client = require('node-rest-client').Client;

module.exports = {
    get: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/notification/event", args, successCallback);
        client.on('error', errorCallback);
    },
    add: function(event, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: event,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/notification/event", args, successCallback);
        client.on('error', errorCallback);
    },
    update: function(event, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: event,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/notification/event", args, successCallback);
        client.on('error', errorCallback);
    },
    remove: function(event, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: event,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/notification/event/delete", args, successCallback);
        client.on('error', errorCallback);
    }
};
