var eventService = require('./notification.event.service.js');

module.exports = {
    get: function(req, res) {
        eventService.get(req.query, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    add: function(req, res) {
        var event = req.body;
        if (req.userData) event.created = {
            by: req.userData._id,
            at: new Date()
        };
        eventService.add(event, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    update: function(req, res) {
        var event = req.body;
        if (req.userData) event.updated = {
            by: req.userData._id,
            at: new Date()
        };
        eventService.update(event, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    remove: function(req, res) {
        var event = { _id: req.query.id };
        if (req.userData) event.updated = {
            by: req.userData._id,
            at: new Date()
        };
        eventService.remove(event, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    }
};
