var express = require('express');
 var authController = require('../auth/auth.controller.js');
var notificationEventController = require('./event/notification.event.controller.js');
var notificationTemplateController = require('./template/notification.template.controller.js');

var notification = express.Router();

 notification.use(authController.identifyToken, authController.authenticateToken);

notification.get('/event', notificationEventController.get);
notification.post('/event', notificationEventController.add);
notification.put('/event', notificationEventController.update);
notification.delete('/event', notificationEventController.remove);

notification.get('/template', notificationTemplateController.get);
notification.post('/template', notificationTemplateController.add);
notification.put('/template', notificationTemplateController.update);
notification.delete('/template', notificationTemplateController.remove);

module.exports = notification;
