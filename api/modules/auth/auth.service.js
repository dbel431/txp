var Client = require('node-rest-client').Client;

module.exports = {
    authenticateToken: function(token, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: token,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/user/token", args, successCallback);
        client.on('error', errorCallback);
    },
    authenticateResetPasswordToken: function(token, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: token,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/user/reset-password-token", args, successCallback);
        client.on('error', errorCallback);
    },
    login: function(user, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: user,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/user/login", args, successCallback);
        client.on('error', errorCallback);
    },
    forgot: function(credentials, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: credentials,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/user/reset-password", args, successCallback);
        client.on('error', errorCallback);
    },
    logout: function(token, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: token,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/user/logout", args, successCallback);
        client.on('error', errorCallback);
    }
};
