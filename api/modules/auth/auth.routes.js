var express = require('express');
var authController = require('./auth.controller.js');
var userController = require('../user/user.controller.js');
var auth = express.Router();

auth.get('/validate-email', userController.get);
auth.post('/verify-security-question', authController.identifyToken, userController.verifySecurityQuestion);
auth.post('/login', authController.identifyToken, authController.login);
auth.post('/forgot', authController.forgot);
auth.get('/token', authController.identifyToken, authController.authenticateToken, authController.token);
auth.get('/reset-password-token', authController.identifyResetPasswordToken, authController.authenticateResetPasswordToken, authController.resetPasswordToken);
auth.put('/reset-password', authController.identifyResetPasswordToken, authController.authenticateResetPasswordToken, userController.resetPassword);
auth.post('/logout', authController.identifyToken, authController.logout);

module.exports = auth;
