var authService = require('./auth.service.js');
module.exports = {
    identifyToken: function(req, res, next) {
        var bearerToken;
        var bearerHeader = req.headers["authorization"];
        if (typeof bearerHeader !== 'undefined') {
            var bearer = bearerHeader.split(" ");
            bearerSecret = bearer[1];
            bearerToken = bearer[2];
            req.secret = bearerSecret;
            req.token = bearerToken;
            next();
        } else {
            return res.sendStatus(403).end();
        }
    },
    identifyResetPasswordToken: function(req, res, next) {
        var bearerToken;
        var bearerHeader = req.headers["resetpasswordauth"];
        if (typeof bearerHeader !== 'undefined') {
            var bearer = bearerHeader.split(" ");
            bearerSecret = bearer[1];
            bearerToken = bearer[2];
            req.secret = bearerSecret;
            req.token = bearerToken;
            next();
        } else {
            return res.sendStatus(403).end();
        }
    },
    authenticateToken: function(req, res, next) {
        authService.authenticateToken({
            secret: req.secret,
            token: req.token
        }, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    {
                        req.userData = data;
                        break;
                    }
                default:
                    {
                        return res.status(result.statusCode).send(data);
                    }
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    authenticateResetPasswordToken: function(req, res, next) {
        authService.authenticateResetPasswordToken({
            secret: req.secret,
            token: req.token
        }, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    {
                        req.userData = data;
                        break;
                    }
                default:
                    {
                        return res.status(result.statusCode).send(data);
                    }
            }
            next();
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    login: function(req, res) {
        var credentials = req.body;
        if (req.secret) credentials.secret = req.secret;
        authService.login(credentials, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    forgot: function(req, res) {
        authService.forgot(req.body, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    token: function(req, res) {
        res.status(200).send({ user: req.userData });
    },
    resetPasswordToken: function(req, res) {
        res.status(200).send({ user: req.userData });
    },
    logout: function(req, res) {
        authService.logout({
            token: req.token
        }, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    }
};
