require('events').EventEmitter.prototype._maxListeners = 0;
require('../config.js');
var express = require('express');
var api = express();
var bodyParser = require('body-parser');
// ---------------------------------------------------------
// ---------------------------------------------------------
// Logging
// ---------------------------------------------------------
// ---------------------------------------------------------
var winston = require('winston');
var logger = new winston.Logger({
    transports: [
        new winston.transports.File({
            level: process.env.LOG_LEVEL || 'warning',
            filename: './log/api.log',
            handleExceptions: true,
            json: true,
            maxsize: 5242880, //5MB
            maxFiles: 5,
            colorize: false
        })
    ],
    exitOnError: false
});
logger.stream = {
    write: function(message, encoding) {
        logger.info(message);
    }
};
api.use(require("morgan")("combined", { "stream": logger.stream }));
// ---------------------------------------------------------
// ---------------------------------------------------------
// CORS
// ---------------------------------------------------------
// ---------------------------------------------------------
api.use(require('cors')());
// ---------------------------------------------------------
// ---------------------------------------------------------
// Body Parser
// ---------------------------------------------------------
// ---------------------------------------------------------
api.use(bodyParser.json({ limit: '50mb' }));
api.use(bodyParser.urlencoded({
    limit: '50mb',
    extended: true
}));
// ---------------------------------------------------------
// ---------------------------------------------------------
// Treasured Works API
// ---------------------------------------------------------
// ---------------------------------------------------------
api.use('/exports', express.static(__dirname + '/exports'));
// ---------------------------------------------------------
// ---------------------------------------------------------
// load api/auth
var auth = require('./modules/auth/auth.routes.js');
api.use('/auth', auth);
// load api/log
var log = require('./modules/log/log.routes.js');
api.use('/log', log);
// load api/acl
var acl = require('./modules/acl/acl.routes.js');
api.use('/acl', acl);
// load api/mailbox
var mailbox = require('./modules/mailbox/mailbox.routes.js');
api.use('/mailbox', mailbox);
// load api/user
var user = require('./modules/user/user.routes.js');
api.use('/user', user);
// load api/role
var role = require('./modules/role/role.routes.js');
api.use('/role', role);
// load api/notification
var notification = require('./modules/notification/notification.routes.js');
api.use('/notification', notification);
// load api/todo
var todo = require('./modules/todo/todo.routes.js');
api.use('/todo', todo);
// load api/security-question
var securityQuestion = require('./modules/security-question/security-question.routes.js');
api.use('/security-question', securityQuestion);
// load api/directory
var directory = require('./modules/directory/directory.routes.js');
api.use('/directory', directory);
// load api/directory/data-download
var dataDownload = require('./modules/directory/data-download/directory.data-download.routes.js');
api.use('/directory/data-download', dataDownload);
// load api/organization
var organization = require('./modules/organization/organization.routes.js');
api.use('/organization', organization);
// load api/personnel
var personnel = require('./modules/personnel/personnel.routes.js');
api.use('/personnel', personnel);
// load api/batchJob
var batchJob = require('./modules/batch-job/batch-job.routes.js');
api.use('/batch-job', batchJob);
// load api/batchJob
var fileDownload = require('./modules/file-download/file-download.routes.js');
api.use('/file-download', fileDownload);

var menu = require('./web_modules/menu/menu.routes.js');
api.use('/menu', menu);

var webData = require('./web_modules/web-data/web-data.routes.js');
api.use('/web-data', webData);

var packageData = require('./web_modules/package/package.routes.js');
api.use('/package', packageData);

var productsServices = require('./web_modules/products-services/products-services.routes.js');
api.use('/products-services', productsServices);

//email
 // var emailTemplate = require('./modules/email-template/email-template.routes.js');
 // api.use('/emailTemplate', emailTemplate);

var customerFeedback = require('./web_modules/customer-feedback/customer-feedback.routes.js');
api.use('/customer-feedback', customerFeedback);


// load api/subscription-package
var subscriptionPackage = require('./modules/subscription-package/subscription-package.routes.js');
api.use('/subscription-package', subscriptionPackage);
// load api/web-user
var webUser = require('./modules/web-user/web-user.routes.js');
api.use('/web-user', webUser);
// load api/subscriber
var subscriber = require('./modules/subscriber/subscriber.routes.js');
api.use('/subscriber', subscriber);
// load api/web-request
var webRequest = require('./modules/web-request/web-request.routes.js');
api.use('/web-request', webRequest);

// ---------------------------------------------------------
// listen
api.listen(process.env.API_PORT, function() {
    console.log('Listening...');
});
