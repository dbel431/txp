var productsServicesService = require('./products-services.service.js');

module.exports = {
    //console.log(req.query);
    get: function(req, res) {
        productsServicesService.get(req.query, function(data, result) {

            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getGuide: function(req, res) {
        productsServicesService.getGuide(req.body, function(data, result) {

            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getProductCategory: function(req, res) {
     

        productsServicesService.getProductCategory(req.query, function(data, result) {

            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    postProductSubCategory: function(req, res) {
        productsServicesService.postProductSubCategory(req.query, function(data, result) {

            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    getProductCategoryHome: function(req, res) {
//console.log(req.query);
        productsServicesService.getProductCategoryHome(req.query, function(data, result) {

            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    }


};
