var Client = require('node-rest-client').Client;

module.exports = {
    get: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/products-services", args, successCallback);
        client.on('error', errorCallback);
    },
    getGuide: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/products-services/getGuide", args, successCallback);
        client.on('error', errorCallback);
    },
    getProductCategory: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/products-services/getProductCategory", args, successCallback);
        client.on('error', errorCallback);
    },
    postProductSubCategory: function(query, successCallback, errorCallback) {

      
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/products-services/postProductSubCategory", args, successCallback);
        client.on('error', errorCallback);
    }, 
    getProductCategoryHome: function(query, successCallback, errorCallback) {
            
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/products-services/getProductCategoryHome", args, successCallback);
        client.on('error', errorCallback);
    }
};
