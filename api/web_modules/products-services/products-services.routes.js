var express = require('express');
//var authController = require('../auth/auth.controller.js');
var productsServicesController = require('./products-services.controller.js');
//var directoryController = require('../directory/directory.controller.js');
var productsServices = express.Router();

//productsServices.use(authController.identifyToken, authController.authenticateToken);

productsServices.get('/', productsServicesController.get);
productsServices.post('/getGuide', productsServicesController.getGuide);
productsServices.get('/getProductCategory', productsServicesController.getProductCategory);
productsServices.get('/postProductSubCategory', productsServicesController.postProductSubCategory);
productsServices.get('/getProductCategoryHome', productsServicesController.getProductCategoryHome);


module.exports = productsServices;
