var express = require('express');
var authController = require('../../modules/auth/auth.controller.js');
var packageController = require('./package.controller.js');
var package = express.Router();

//package.use(authController.identifyToken, authController.authenticateToken);

package.get('/', packageController.get);
package.post('/data-table', packageController.dataTable);
package.post('/', packageController.add);
package.put('/', packageController.update);
package.delete('/', packageController.remove);

module.exports = package;
