var Client = require('node-rest-client').Client;

module.exports = {
    get: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/package", args, successCallback);
        client.on('error', errorCallback);
    },
    dataTable: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/package/data-table", args, successCallback);
        client.on('error', errorCallback);
    },
    add: function(package, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: package,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/package", args, successCallback);
        client.on('error', errorCallback);
    },
    update: function(id, package, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: package,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/package", args, successCallback);
        client.on('error', errorCallback);
    },
    remove: function(package, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: package,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/package/delete", args, successCallback);
        client.on('error', errorCallback);
    }
};
