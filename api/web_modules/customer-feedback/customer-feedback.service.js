var Client = require('node-rest-client').Client;

module.exports = {
    get: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/customer-feedback", args, successCallback);
        client.on('error', errorCallback);
    },
    dataTable: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/customer-feedback/data-table", args, successCallback);
        client.on('error', errorCallback);
    },
    add: function(customerFeedback, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: customerFeedback,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/customer-feedback", args, successCallback);
        client.on('error', errorCallback);
    },
    update: function(id, customerFeedback, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: customerFeedback,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/customer-feedback", args, successCallback);
        client.on('error', errorCallback);
    },
    remove: function(customerFeedback, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: customerFeedback,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/customer-feedback/delete", args, successCallback);
        client.on('error', errorCallback);
    }
};
