var express = require('express');
var authController = require('../../modules/auth/auth.controller.js');
var customerFeedbackController = require('./customer-feedback.controller.js');
var customerFeedback = express.Router();

//customerFeedback.use(authController.identifyToken, authController.authenticateToken);

customerFeedback.get('/', customerFeedbackController.get);
customerFeedback.post('/data-table', customerFeedbackController.dataTable);
customerFeedback.post('/', customerFeedbackController.add);
customerFeedback.put('/', customerFeedbackController.update);
customerFeedback.delete('/', customerFeedbackController.remove);

module.exports = customerFeedback;
