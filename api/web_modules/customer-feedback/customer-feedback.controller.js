var customerFeedbackService = require('./customer-feedback.service.js');

module.exports = {
    get: function(req, res) {
        customerFeedbackService.get(req.query, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    dataTable: function(req, res) {
        customerFeedbackService.dataTable(req.body, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    add: function(req, res) {
        var customerFeedback = req.body;
        if (req.userData) customerFeedback.created = {
            by: req.userData._id,
            at: new Date()
        };
        customerFeedbackService.add(customerFeedback, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    update: function(req, res) {
        var customerFeedback = req.body;
        if (req.userData) customerFeedback.created = {
            by: req.userData._id,
            at: new Date()
        };
        customerFeedbackService.update(req.query.id, customerFeedback, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    remove: function(req, res) {
        var customerFeedback = { _id: req.query.id };
        if (req.userData) customerFeedback.updated = {
            by: req.userData._id,
            at: new Date()
        };
        customerFeedbackService.remove(customerFeedback, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    }
};
