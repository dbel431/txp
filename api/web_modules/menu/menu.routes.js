var express = require('express');
var authController = require('../../modules/auth/auth.controller.js');
var menuController = require('./menu.controller.js');
var menu = express.Router();

//menu.use(authController.identifyToken, authController.authenticateToken);

menu.get('/', menuController.get);
menu.post('/data-table', menuController.dataTable);
menu.post('/', menuController.add);
menu.put('/', menuController.update);
menu.delete('/', menuController.remove);

module.exports = menu;
