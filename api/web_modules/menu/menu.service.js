var Client = require('node-rest-client').Client;

module.exports = {
    get: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/menu", args, successCallback);
        client.on('error', errorCallback);
    },
    dataTable: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/menu/data-table", args, successCallback);
        client.on('error', errorCallback);
    },
    add: function(menu, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: menu,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/menu", args, successCallback);
        client.on('error', errorCallback);
    },
    update: function(id, menu, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: menu,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/menu", args, successCallback);
        client.on('error', errorCallback);
    },
    remove: function(menu, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: menu,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/menu/delete", args, successCallback);
        client.on('error', errorCallback);
    }
};
