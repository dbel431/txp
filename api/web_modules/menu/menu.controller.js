var menuService = require('./menu.service.js');

module.exports = {
    get: function(req, res) {
        menuService.get(req.query, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    dataTable: function(req, res) {
        menuService.dataTable(req.body, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    add: function(req, res) {
        var menu = req.body;
        if (req.userData) menu.created = {
            by: req.userData._id,
            at: new Date()
        };
        menuService.add(menu, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    update: function(req, res) {
        var menu = req.body;
        if (req.userData) menu.created = {
            by: req.userData._id,
            at: new Date()
        };
        menuService.update(req.query.id, menu, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    remove: function(req, res) {
        var menu = { _id: req.query.id };
        if (req.userData) menu.updated = {
            by: req.userData._id,
            at: new Date()
        };
        menuService.remove(menu, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    }
};
