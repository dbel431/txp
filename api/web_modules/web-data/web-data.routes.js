var express = require('express');
var authController = require('../../modules/auth/auth.controller.js');
var webDataController = require('./web-data.controller.js');
var webData = express.Router();

//webData.use(authController.identifyToken, authController.authenticateToken);

webData.get('/', webDataController.get);
webData.post('/data-table', webDataController.dataTable);
webData.post('/', webDataController.add);
webData.put('/', webDataController.update);
webData.delete('/', webDataController.remove);

module.exports = webData;
