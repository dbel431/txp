var Client = require('node-rest-client').Client;

module.exports = {
    get: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.get(process.env.APP_HOST + ":" + process.env.APP_PORT + "/web-data", args, successCallback);
        client.on('error', errorCallback);
    },
    dataTable: function(query, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: query,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/web-data/data-table", args, successCallback);
        client.on('error', errorCallback);
    },
    add: function(webData, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: webData,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/web-data", args, successCallback);
        client.on('error', errorCallback);
    }, 
    // addAdvertiseInquirie: function(addAdvertiseInquirieData, successCallback, errorCallback) {
    //     var client = new Client();
    //     var args = {
    //         data: addAdvertiseInquirieData,
    //         headers: {
    //             'Content-Type': 'application/json'
    //         }
    //     };
    //     client.post(process.env.APP_HOST + ":" + process.env.APP_PORT + "/web-data/addAdvertiseInquirie", args, successCallback);
    //     client.on('error', errorCallback);
    // },
    update: function(id, webData, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: webData,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/web-data", args, successCallback);
        client.on('error', errorCallback);
    },
    remove: function(webData, successCallback, errorCallback) {
        var client = new Client();
        var args = {
            data: webData,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        client.put(process.env.APP_HOST + ":" + process.env.APP_PORT + "/web-data/delete", args, successCallback);
        client.on('error', errorCallback);
    }
};
