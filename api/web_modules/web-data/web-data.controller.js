var webData = require('./web-data.service.js');

module.exports = {
    get: function(req, res) {
        webData.get(req.query, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    dataTable: function(req, res) {
        webData.dataTable(req.body, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    add: function(req, res) {
        var menu = req.body;
        if (req.userData) menu.created = {
            by: req.userData._id,
            at: new Date()
        };
        webData.add(menu, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    }, 
    // addAdvertiseInquirie: function(req, res) {
    //     var advertiseInquirieData = req.body;
    //     if (req.userData) advertiseInquirieData.created = {
    //         by: req.userData._id,
    //         at: new Date()
    //     };               
    //     webData.addAdvertiseInquirie(advertiseInquirieData, function(data, result) {
    //         switch (result.statusCode) {
    //             case 200:
    //                 return res.send(data);
    //             default:
    //                return res.status(result.statusCode).send(data);
    //         }
    //     }, function(err) {
    //         return res.status(500).send(err);
    //     });
    // },
    update: function(req, res) {
        var menu = req.body;
        if (req.userData) menu.created = {
            by: req.userData._id,
            at: new Date()
        };
        webData.update(req.query.id, menu, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    },
    remove: function(req, res) {
        var menu = { _id: req.query.id };
        if (req.userData) menu.updated = {
            by: req.userData._id,
            at: new Date()
        };
        webData.remove(menu, function(data, result) {
            switch (result.statusCode) {
                case 200:
                    return res.send(data);
                default:
                    return res.status(result.statusCode).send(data);
            }
        }, function(err) {
            return res.status(500).send(err);
        });
    }
};
