var mongoose = require('mongoose');
var util = require('util');
var firstBy = require('thenby');
var Organization = require('./organization.model.js');
var OrganizationPersonnel = require('./organization.personnel.model.js');
var OrganizationExhibition = require('./organization.exhibition.model.js');
var OrganizationChapterSpecification = require('./organization.chapter-specification.model.js');
var OrganizationVersion = require('./organization.version.model.js');
var Audit = require('../core/audit.log.service.js');
var Exception = require('../core/exception.log.service.js');
var OrganizationSummaryReportCount = require('./organization.summary-report-count.model.js');


var personnelPopulations = [
    { path: 'orgUserId', match: { active: true, deleted: false } },
    { path: 'titleMaster', match: { active: true, deleted: false } },
    { path: 'officers.officerTypeId', match: { active: true, deleted: false } },
    { path: 'officers.listingType', match: { active: true, deleted: false } },
    { path: 'personType', match: { active: true, deleted: false } },
    { path: 'address.addressType', match: { active: true, deleted: false } },
    { path: 'address.city', match: { active: true, deleted: false } },
    { path: 'address.state', match: { active: true, deleted: false } },
    { path: 'address.country', match: { active: true, deleted: false } },
    { path: 'address.county', match: { active: true, deleted: false } },
    { path: 'ocdDegree.degreeType', match: { active: true, deleted: false } },
    { path: 'assignment.assignTypeId', match: { active: true, deleted: false } },
    { path: 'status', match: { active: true, deleted: false } },
    { path: 'ocdContact.contactType', match: { active: true, deleted: false } },
    { path: 'responsibilityCodes.responsibilityCode', match: { active: true, deleted: false } },
];

module.exports = {
    mongoose: mongoose,
    orgModel: Organization,
    persModel: OrganizationPersonnel,
    getChapterSpecificationData: function(query, callback) {
        // OrganizationChapterSpecification
        // .aggregate([
        //     {$match:{"directoryId":mongoose.Types.ObjectId('57189cc224d8bc65f4123bc1')}},
        //     {$group:{_id:1,ids:{$push:"$organizationId"}}}
        //     ]).exec(callback);
        //OrganizationChapterSpecification.aggregate([{$match: {       directoryId:ObjectId("57189cc224d8bc65f4123bc1"),       }     },     {       $unwind: {       path:"$chapterSpecification",       }     },      {       $lookup: {           "from" : "txn_chapterspecifications",           "localField" : "chapterSpecification",           "foreignField" : "_id",           "as" : "chapterSpecification"       }     },      {       $unwind: '$chapterSpecification'     },      {       $match: {        "chapterSpecification.specificationType" : {$in:["GEOGRAPHICAL PREFERENCES"]},        "chapterSpecification.codeName" : {$in:["All regions","Midwest"]},       }     }   ],   {     cursor: {       batchSize: 50     }   } )

        // Organization.aggregate([
        //     {$match:{directoryId:mongoose.Types.ObjectId("57189cc224d8bc65f4123bc1")}},
        //     {$unwind:{path:"$chapterSpecification"}},
        //     {$lookup:{"from":"txn_chapterspecifications","localField":"chapterSpecification","foreignField":"_id","as":"chapterSpecification"}},
        //     {$unwind:'$chapterSpecification'},
        //     {$match:{"chapterSpecification.specificationType":{$in:["GEOGRAPHICALPREFERENCES"]},"chapterSpecification.codeName":{$in:["Allregions","Midwest"]}}}
        //     ],{cursor:{batchSize:5}})
        //     .exec(callback);

        // query2.directoryId=mongoose.Types.ObjectId(query2.directoryId);
        // delete query2.directoryId;
        //  console.log('Organization.aggregate_2',JSON.stringify(query2));
        // //{ directoryId: mongoose.Types.ObjectId("57189cc224d8bc65f4123bc1")} 
        // Organization.aggregate(
        //         [{ $match: query2},
        //         { $unwind: "$chapterSpecification" },
        //         { $lookup: { "from": "txn_chapterspecifications", "localField": "chapterSpecification", "foreignField": "_id", "as": "chapterSpecification" } },
        //         { $unwind: "$chapterSpecification" },
        //         { $match: query},
        //         { $group: { _id: 1, ids: { $push: "$_id" } } }
        //     ]).exec(callback);

        // query.directoryId=mongoose.Types.ObjectId(query.directoryId);
        //         console.log('getChapterSpecificationData',query);
        // Organization.aggregate(
        //         [{ $match: query.directoryId},
        //         { $unwind: "$chapterSpecification" },
        //         { $lookup: { "from": "txn_chapterspecifications", "localField": "chapterSpecification", "foreignField": "_id", "as": "chapterSpecification" } },
        //         { $unwind: "$chapterSpecification" },
        //         { $match: query},
        //         { $group: { _id: 1, ids: { $push: "$_id" } } }
        //     ]).exec(callback);

                var Id=mongoose.Types.ObjectId('57189cc224d8bc65f4123bc1');
                delete query.directoryId;
                var x={"chapterSpecification.specificationType":{"$in":["GEOGRAPHICAL PREFERENCES"]}, 
                "chapterSpecification.codeName":{"$in":["All regions","Midwest"]} 
                };
                Organization.aggregate(
                [{ $match: {"directoryId":Id}},
                { $unwind: "$chapterSpecification" },
                { $lookup: { "from": "txn_chapterspecifications", "localField": "chapterSpecification", "foreignField": "_id", "as": "chapterSpecification" } },
                { $unwind: "$chapterSpecification" },
                { $match: query},
                { $group: { _id: 1, ids: { $push: "$_id" } } }
            ]).exec(callback);



    },
    orgNameMatch:function(query, callback){
    query.directoryId=mongoose.Types.ObjectId(query.directoryId);
      Organization.aggregate(
                [
                 { $match: { directoryId : query.directoryId } },
                 { $project: { name: { $toUpper: "$name" }, _id: 1 } },
                 { $match: { name: query.name}},
                 { $group: { _id: 1, ids: { $push: "$_id" } } }
                ]).exec(callback);
    },
    get: function(query, callback) {
        var select, limit, skip, sort = { name: 1, _id: 1 };
        if (query._select) select = query._select;
        if (query._limit) limit = query._limit;
        if (query._skip) skip = query._skip;
        if (query._sort) sort = query._sort;
        if (query._where) query = query._where;
        if (typeof query.deleted == 'undefined') query.deleted = false;
        var dbQuery = Organization
            .find(query)
            .populate({ path: 'parentId', match: { active: true, deleted: false } })
            .populate({ path: 'directoryId', match: { active: true, deleted: false } })
            .populate({
                path: 'personnel',
                match: { active: true, deleted: false },
                options: { sort: { sequenceNumber: 1, _id: 1 } },
                populate: personnelPopulations
            })
            .populate({
                path: 'exhibition',
                options: { sort: { sequenceNumber: 1, _id: 1 } },
                match: { active: true, deleted: false }
            })
            .populate({
                path: 'chapterSpecification',
                options: { sort: { sequence_number: 1, _id: 1 } },
                match: { active: true, deleted: false },
                populate: [
                    { path: 'listingType', match: { active: true, deleted: false } },
                    { path: 'code', match: { active: true, deleted: false } }
                ]
            })
            .populate({ path: 'address.addressType', match: { active: true, deleted: false } })
            .populate({ path: 'address.country', match: { active: true, deleted: false } })
            .populate({ path: 'address.county', match: { active: true, deleted: false } })
            .populate({ path: 'address.state', match: { active: true, deleted: false } })
            .populate({
                path: 'address.city',
                match: { active: true, deleted: false },
                populate: {
                    path: 'parentId',
                    match: { active: true, deleted: false },
                    populate: {
                        path: 'parentId',
                        match: { active: true, deleted: false }
                    }
                }
            })
            .populate({ path: 'features.code', match: { active: true, deleted: false } })
            .populate({ path: 'classificationCode', match: { active: true, deleted: false } })
            .populate({ path: 'sectionCode', match: { active: true, deleted: false } })
            .populate({ path: 'status', match: { active: true, deleted: false } })
            .populate({ path: 'workflowStatus', match: { active: true, deleted: false } })
            .populate({ path: 'pensionReportInterval', match: { active: true, deleted: false } })
            .populate({ path: 'advanceBillType', match: { active: true, deleted: false } })
            .populate({ path: 'primaryMarket', match: { active: true, deleted: false } })
            .populate({ path: 'directMarketingBudgetDisbursal.code', match: { active: true, deleted: false } })
            .populate({ path: 'listingType.listing', match: { active: true, deleted: false } })
            .populate({ path: 'listingType.listingSectionCode', match: { active: true, deleted: false } })
            .populate({ path: 'members.listingType', match: { active: true, deleted: false } })
            .populate({ path: 'vendors.vendorType', match: { active: true, deleted: false } })
            .populate({ path: 'vendors.state', match: { active: true, deleted: false } })
            .populate({ path: 'vendors.city', match: { active: true, deleted: false } })
            //For OCD
            .populate({ path: 'dioceseType', match: { active: true, deleted: false } })
            .populate({ path: 'dioProvince', match: { active: true, deleted: false } })
            .populate({ path: 'parishShrine.ethnicityType1', match: { active: true, deleted: false } })
            .populate({ path: 'parishShrine.ethnicityType2', match: { active: true, deleted: false } })
            .populate({ path: 'parishShrine.parishStatus', match: { active: true, deleted: false } })
            .populate({ path: 'school.schoolType', match: { active: true, deleted: false } })
            .populate({ path: 'school.gradeTypeStart', match: { active: true, deleted: false } })
            .populate({ path: 'school.gradeTypeEnd', match: { active: true, deleted: false } })
            .populate({ path: 'statistic.statisticType', match: { active: true, deleted: false } })
            .populate({ path: 'religiousOrder.religiousType', match: { active: true, deleted: false } })
            .populate({ path: 'legalTitles.city', match: { active: true, deleted: false } })
            .populate({ path: 'legalTitles.state', match: { active: true, deleted: false } })
            .populate({ path: 'ocdContact.contactType', match: { active: true, deleted: false } })
            .populate({ path: 'mailingList.city', match: { active: true, deleted: false } })
            .populate({ path: 'mailingList.state', match: { active: true, deleted: false } });
        if (select) dbQuery.select(select);
        if (sort) dbQuery.sort(sort);
        if (limit) dbQuery.limit(limit);
        if (skip) dbQuery.skip(skip);
        dbQuery.exec(function(err, organizations) {
            var newOrgs = sortOrgs(organizations);
            callback(err, newOrgs);
        });
        // .exec(callback);
    },
//     getNext:function(query, callback){
//         console.log("getNext");
//         //var qr=Organization.find(query).sort(_id).limit(1);
//         qr.exec(callback);  
// //        var p=Organization.find(query).sort([(_id, -1)]).limit(1);
//     },
    getOrgAggregate:function(query, callback){
           query.org.directoryId=mongoose.Types.ObjectId(query.org.directoryId);
        Organization.aggregate(
          [
            {$match: query.org},
            {$match:query.exhi},
            {$group: {"_id":1, "ids":{$push:"$_id"} } }
          ]
        ).exec(callback);  
    },
    getOrgtoExhi:function(query, callback){
        query.org.directoryId=mongoose.Types.ObjectId(query.org.directoryId);
        Organization.aggregate(
          [
            {$match: query.org},
            {$unwind: "$exhibition"},
            {$lookup: {"from" : "txn_exhibitions", "localField" : "exhibition", "foreignField" : "_id", "as" : "exhibition"} },
            {$unwind: "$exhibition"},
           // {$match: {"exhibition.exhibitName":/Park Centennial/i } },
            {$match:query.exhi},
            {$group: {"_id":1, "ids":{$push:"$_id"} } }
          ]
        ).exec(callback);
    },
        getOrgFromPer:function(query, callback){

        /*
db.txn_organizations.aggregate(
  [
    {$match: {"directoryId": ObjectId("57189c7024d8bc65f4123bc0") } },
    {$unwind: "$personnel"},
    {$lookup: {"from" : "txn_personnels", "localField" : "personnel", "foreignField" : "_id", "as" : "personnel"} },
    {$unwind: "$personnel"},
    {$match: {"personnel.name.first":"Kathi", "personnel.name.last":"Stanley"} }
  ]
);
        */
    query.org.directoryId=mongoose.Types.ObjectId(query.org.directoryId);
    //{"directoryId": ObjectId("57189c7024d8bc65f4123bc0") }
    Organization.aggregate([
    {$match:query.org},
    {$unwind:"$personnel"},
    {$lookup:{"from" : "txn_personnels", "localField" : "personnel", "foreignField" : "_id", "as" : "personnel"} },
    {$unwind:"$personnel"},
    {$match:query.per},
    {$group: {"_id":1, "ids":{$push:"$_id"} } }
    ]).exec(callback);

    },

    getExhibitionListData: function(query, callback) {
         OrganizationExhibition
             .aggregate([
                {$match:{ 
                    'directoryId': mongoose.Types.ObjectId(query.directoryId),
                    'active':true,
                    'deleted' : false,
                    'exhibitName': query.exhibitName
               }},
                {$group: {
                          _id:1,'ids':{$push:'$orgId'}
                        }}
                ])
             .exec(callback);


    },
        getExhibitionList: function(query, callback) {
        var directoryId = mongoose.Types.ObjectId(query.directoryId);
        delete query.directoryId;
        OrganizationExhibition
            .aggregate([
                { $match: { "directoryId": directoryId } },
                { $match: query },
                { $group: { _id: 1, ids: { $push: "$orgId" } } }
            ])
            .exec(callback);
    },
    getChapterSpecificationOnAggregate: function(query, callback) {
        var directoryId = mongoose.Types.ObjectId(query.directoryId);
        delete query.directoryId;
        // OrganizationChapterSpecification.aggregate([
        //         { $match: { "directoryId": directoryId } },
        //         { $match: query },
        //         { $group: { _id: 1, ids: { $push: "$organizationId" } } }
        //     ])
        //     .exec(callback);
        OrganizationChapterSpecification.find(query)
            .exec(callback);
    },
    getOrgIdFromPersonnel: function(query, callback) {
        var directoryId = mongoose.Types.ObjectId(query.directoryId);
        delete query.directoryId;
        OrganizationPersonnel
            .aggregate([
                { $match: { "directoryId": directoryId } },
                { $match: query },
                { $unwind: "$orgId" },
                { $group: { _id: "$directoryId", ids: { $push: "$orgId" } } }
            ])
            .exec(callback);
        // .find(query)
        // .populate({ path: 'orgId', match: { active: true, deleted: false } })
        // .exec(callback);
    },
    getPersonnelList: function(query, callback) {
        var select, limit, skip;
        if (query._select) select = query._select;
        if (query._limit) limit = query._limit;
        if (query._skip) skip = query._skip;
        if (query._sort) sort = query._sort;
        if (query._where) query = query._where;
        if (typeof query.deleted == 'undefined') query.deleted = false;
        var dbQuery = OrganizationPersonnel.find(query);
        for (var i = 0; i < personnelPopulations.length; i++) {
            dbQuery.populate(personnelPopulations[i]);
        }
        if (select) dbQuery.select(select);
        if (sort) dbQuery.sort(sort);
        if (limit) dbQuery.limit(limit);
        if (skip) dbQuery.skip(skip);
        dbQuery.exec(callback);
    },
    getSummaryReportCount: function(query, callback) {
        //OrganizationSummaryReportCount.create(query, callback);
        OrganizationSummaryReportCount.find({}, callback);
    },
    getOrganizationCount: function(query, callback) {
        if (typeof query.deleted == 'undefined') query.deleted = false;
        Organization.count(query, callback);
    },
    getActivitiesCount: function(query, callback) {
        if (typeof query.deleted == 'undefined') query.deleted = false;
        Organization.count(query, callback);
    },
    getCategoriesCount: function(query, callback) {
        if (typeof query.deleted == 'undefined') query.deleted = false;
        Organization.count(query, callback);
    },
    getFacilitiesCount: function(query, callback) {
        var id1 = query.directoryId;
        var id = mongoose.Types.ObjectId(id1);
        if (typeof query.deleted == 'undefined') query.deleted = false;
        Organization.aggregate([
            { $match: { "directoryId": id } },
            { $unwind: '$features' },
            { $match: { "features.deleted": { $ne: true } } },
            { $group: { _id: '$features.featureType', count: { $sum: 1 } } }
        ], callback);

        //Organization.count(query, callback);
    },
    getCollectionCount: function(query, callback) {
        // if (typeof query.deleted == 'undefined') query.deleted = false;
        // Organization.count(query, callback);
        var id1 = query.directoryId;
        var id = mongoose.Types.ObjectId(id1);
        if (typeof query.deleted == 'undefined') query.deleted = false;
        OrganizationChapterSpecification.aggregate([
            { $match: { "directoryId": id, 'deleted': { "$ne": true } } },
            // Unwind the array to de-normalize as documents
            // { "$unwind": "$specificationType" },
            // Group on the key you want and provide other values
            {
                "$group": {
                    "_id": "$specificationType",
                    "count": { $sum: 1 }
                }
            }
        ], callback);
    },
    getExhibitionCount: function(query, callback) {
        if (typeof query.deleted == 'undefined') query.deleted = false;
        OrganizationExhibition.count(query, callback);
    },
    getPersonnelCount: function(query, callback) {
        if (typeof query.deleted == 'undefined') query.deleted = false;
        OrganizationPersonnel.count(query, callback);
    },
    exportToExcel: function(query, callback) {
        if (typeof query.where.deleted == 'undefined') query.where.deleted = false;
        // var selectPersonnel = [],
        //     selectExhibition = [],
        //     selectChapterSpecification = [];
        // for (var i = 0; i < query.select.personnel.length; i++) {
        //     selectPersonnel.push(query.select.personnel[i]);
        //     var items = query.select.personnel[i].split('.');
        //     items.splice(0, 1);
        //     query.select.personnel[i] = items.join('.');
        // }
        // for (var i = 0; i < query.select.exhibition.length; i++) {
        //     selectExhibition.push(query.select.exhibition[i]);
        //     var items = query.select.exhibition[i].split('.');
        //     items.splice(0, 1);
        //     query.select.exhibition[i] = items.join('.');
        // }
        // for (var i = 0; i < query.select.chapterSpecification.length; i++) {
        //     selectChapterSpecification.push(query.select.chapterSpecification[i]);
        //     var items = query.select.chapterSpecification[i].split('.');
        //     items.splice(0, 1);
        //     query.select.chapterSpecification[i] = items.join('.');
        // }
        // var select = ([
        //     query.select.organization.join(' '),
        //     selectPersonnel.join(' '),
        //     selectExhibition.join(' '),
        //     selectChapterSpecification.join(' ')
        // ]).join(' ');
        var dbQuery = Organization
            .find(query.where);
        if (query.select.personnel.length > 0)
            dbQuery.populate({
                path: 'personnel',
                // select: query.select.personnel.join(' '),
                options: { sort: { sequenceNumber: 1, _id: 1 } },
                match: { active: true, deleted: false },
                populate: personnelPopulations
            });
        if (query.select.exhibition.length > 0)
            dbQuery.populate({
                path: 'exhibition',
                // select: query.select.exhibition.join(' '),
                options: { sort: { sequenceNumber: 1, _id: 1 } },
                match: { active: true, deleted: false }
            });
        if (query.select.chapterSpecification.length > 0)
            dbQuery.populate({
                path: 'chapterSpecification',
                // select: query.select.chapterSpecification.join(' '),
                options: { sort: { sequence_number: 1, _id: 1 } },
                match: { active: true, deleted: false },
                populate: [
                    { path: 'listingType', match: { active: true, deleted: false } },
                    { path: 'code', match: { active: true, deleted: false } }
                ]
            });

        dbQuery.populate({ path: 'parentId', match: { active: true, deleted: false } })
            .populate({ path: 'directoryId', match: { active: true, deleted: false } })
            .populate({ path: 'address.addressType', match: { active: true, deleted: false } })
            // .populate({ path: 'address.country', match: { active: true, deleted: false } })
            // .populate({ path: 'address.state', match: { active: true, deleted: false } })
            // .populate({ path: 'address.city', match: { active: true, deleted: false } })
            .populate({ path: 'features.code', match: { active: true, deleted: false } })
            .populate({ path: 'classificationCode', match: { active: true, deleted: false } })
            .populate({ path: 'sectionCode', match: { active: true, deleted: false } })
            .populate({ path: 'status', match: { active: true, deleted: false } })
            .populate({ path: 'workflowStatus', match: { active: true, deleted: false } })
            .populate({ path: 'pensionReportInterval', match: { active: true, deleted: false } })
            .populate({ path: 'advanceBillType', match: { active: true, deleted: false } })
            .populate({ path: 'primaryMarket', match: { active: true, deleted: false } })
            .populate({ path: 'directMarketingBudgetDisbursal.code', match: { active: true, deleted: false } })
            .populate({ path: 'listingType.listing', match: { active: true, deleted: false } })
            .populate({ path: 'listingType.listingSectionCode', match: { active: true, deleted: false } })
            .populate({ path: 'vendors.vendorType', match: { active: true, deleted: false } })
            .populate({ path: 'vendors.state', match: { active: true, deleted: false } })
            .populate({ path: 'vendors.city', match: { active: true, deleted: false } })
            .populate({ path: 'versionId', match: { active: true, deleted: false } })
            //For OCD
            .populate({ path: 'dioceseType', match: { active: true, deleted: false } })
            .populate({ path: 'dioProvince', match: { active: true, deleted: false } })
            .populate({ path: 'parishShrine.ethnicityType1', match: { active: true, deleted: false } })
            .populate({ path: 'parishShrine.ethnicityType2', match: { active: true, deleted: false } })
            .populate({ path: 'parishShrine.parishStatus', match: { active: true, deleted: false } })
            .populate({ path: 'school.schoolType', match: { active: true, deleted: false } })
            .populate({ path: 'school.gradeTypeStart', match: { active: true, deleted: false } })
            .populate({ path: 'school.gradeTypeEnd', match: { active: true, deleted: false } })
            .populate({ path: 'statistic.statisticType', match: { active: true, deleted: false } })
            .populate({ path: 'religiousOrder.religiousType', match: { active: true, deleted: false } })
            .populate({ path: 'legalTitles.city', match: { active: true, deleted: false } })
            .populate({ path: 'legalTitles.state', match: { active: true, deleted: false } })
            .populate({ path: 'ocdContact.contactType', match: { active: true, deleted: false } })
            .populate({ path: 'mailingList.city', match: { active: true, deleted: false } })
            .populate({ path: 'mailingList.state', match: { active: true, deleted: false } })
            // .select(select)
            .limit(parseInt(query.limit))
            .skip(parseInt(query.skip))
            .sort(query.sort)
            .exec(function(err, organizations) {
                var newOrgs = sortOrgs(organizations, true);
                callback(err, newOrgs);
            });
    },
    exportToExcelCount: function(query, callback) {
        if (typeof query.deleted == 'undefined') query.deleted = false;
        Organization.count(query, callback);
    },
    exportToExcelPersonnel: function(query, callback) {
        if (query.select.indexOf('orgId') < 0) query.select.push('orgId');
        var dbQuery = OrganizationPersonnel
            .find(query.where, query.select.join(" "));
        if (query.sort) dbQuery.sort(query.sort);
        dbQuery.populate({ path: 'orgId', match: { active: true, deleted: false } })
            .populate({ path: 'orgUserId', match: { active: true, deleted: false } })
            .populate({ path: 'titleMaster', match: { active: true, deleted: false } })
            .populate({ path: 'officers.officerId', match: { active: true, deleted: false } })
            .populate({ path: 'officers.listingType', match: { active: true, deleted: false } })
            /*.populate({ path: 'verification.by', match: { active: true, deleted: false } })*/
            .exec(callback);
    },
    exportToExcelExhibition: function(query, callback) {
        if (query.select.indexOf('orgId') < 0) query.select.push('orgId');
        var dbQuery = OrganizationExhibition
            .find(query.where, query.select.join(" "));
        if (query.sort) dbQuery.sort(query.sort);
        dbQuery.populate({ path: 'orgId', match: { active: true, deleted: false } })
            .exec(callback);
    },
    exportToExcelChapterSpecification: function(query, callback) {
        if (query.select.indexOf('organizationId') < 0) query.select.push('organizationId');
        var dbQuery = OrganizationChapterSpecification
            .find(query.where, query.select.join(" "));
        if (query.sort) dbQuery.sort(query.sort);
        dbQuery.populate({ path: 'directoryId', match: { active: true, deleted: false } })
            .populate({ path: 'organizationId', match: { active: true, deleted: false } })
            .populate({ path: 'listingType', match: { active: true, deleted: false } })
            .populate({ path: 'code', match: { active: true, deleted: false } })
            .exec(callback);
    },
    getPersonnel: function(query, callback) {
        if (typeof query.where.deleted == 'undefined') query.where.deleted = false;
        var dbQuery = OrganizationPersonnel
            .find(query.where);
        if (query.sort) dbQuery.sort(query.sort);
        dbQuery.populate({ path: 'orgId', match: { active: true, deleted: false } })
            .populate({ path: 'orgUserId', match: { active: true, deleted: false } })
            .populate({ path: 'titleMaster', match: { active: true, deleted: false } })
            .populate({ path: 'officers.officerId', match: { active: true, deleted: false } })
            .populate({ path: 'officers.listingType', match: { active: true, deleted: false } })
            /*.populate({ path: 'verification.by', match: { active: true, deleted: false } })*/
            .exec(callback);
    },
    getExhibition: function(query, callback) {
        if (typeof query.where.deleted == 'undefined') query.where.deleted = false;
        var dbQuery = OrganizationExhibition
            .find(query.where);
        if (query.sort) dbQuery.sort(query.sort);
        dbQuery.populate({ path: 'orgId', match: { active: true, deleted: false } })
            .exec(callback);
    },
    getChapterSpecification: function(query, callback) {
        if (typeof query.where.deleted == 'undefined') query.where.deleted = false;
        var dbQuery = OrganizationChapterSpecification
            .find(query.where);
        if (query.sort) dbQuery.sort(query.sort);
        dbQuery.populate({ path: 'directoryId', match: { active: true, deleted: false } })
            .populate({ path: 'organizationId', match: { active: true, deleted: false } })
            .populate({ path: 'listingType', match: { active: true, deleted: false } })
            .populate({ path: 'code', match: { active: true, deleted: false } })
            .exec(callback);
    },
    getVersion: function(query, callback) {
        if (typeof query.deleted == 'undefined') query.deleted = false;
        var dbQuery = OrganizationVersion
            .find(query)
            .populate({ path: 'status', match: { active: true, deleted: false } })
            .populate({
                path: 'created.by',
                match: { active: true, deleted: false },
                populate: {
                    path: 'role',
                    match: { active: true, deleted: false },
                }
            })
            .sort({ 'created.at': -1 })
            .exec(callback);
    },
    dataTable: function(query, callback) {
        var options = { conditions: query.conditions || {} };
        if (!options.conditions.deleted) options.conditions.deleted = false;

        // options.sort = ;
        // query.dataTableQuery.columns[0].orderable = true;
        // query.dataTableQuery.order.push({
        //     column: 0,
        //     dir: 'asc'
        // });
        if (!options.select) options.select = [];
        options.select.push('address.addressType');
        options.select.push('address.deleted');

        query.dataTableQuery.columns.push({
            data: 'address.zip'
        });
        query.dataTableQuery.columns.push({
            data: 'personnel.title'
        });
        query.dataTableQuery.columns.push({
            data: 'personnel.name.first'
        });
        query.dataTableQuery.columns.push({
            data: 'personnel.name.middle'
        });
        query.dataTableQuery.columns.push({
            data: 'flags.icom'
        });
        query.dataTableQuery.columns.push({
            data: 'flags.accredited'
        });
        query.dataTableQuery.columns.push({
            data: 'flags.aam'
        });
        query.dataTableQuery.columns.push({
            data: 'flags.handicapped'
        });
        query.dataTableQuery.columns.push({
            data: 'personnel.name.last'
        });
        query.dataTableQuery.columns.push({
            data: 'personnel.name.prefix'
        });
        query.dataTableQuery.columns.push({
            data: 'personnel.name.suffix'
        });
        query.dataTableQuery.columns.push({
            data: 'address.state.description'
        });
        query.dataTableQuery.columns.push({
            data: 'address.state.codeValue'
        });
        query.dataTableQuery.columns.push({
            data: '_id',
            orderable: true
        });
        query.dataTableQuery.order.push({
            column: (query.dataTableQuery.columns.length - 1),
            dir: 'asc'
        });
        options.handlers = {
            // '_id': function(field, search, opt) {
            // },
            'orgIdNumber': function(field, search, opt) {
                var value = [];
                search.forEach(function(chunk) {
                    if (util.isRegExp(chunk)) {
                        value.push(chunk);
                    } else {
                        value.push(new RegExp(RegExp.escape(chunk.trim()), 'i'));
                    }
                });
                delete options.conditions['orgIdNumber'];
                if (!(query.dataTableQuery.search && query.dataTableQuery.search.value)) {
                    options.conditions['org_id'] = {
                        $in: value
                    }
                }
            }
        };
        Organization.dataTable(query.dataTableQuery, options, callback);
    },
    add: function(organizationData, callback) {
        var by = { by: undefined };
        if (organizationData.created) by = organizationData.created || undefined;
        var autoidnum = [];
        var myid = function() {
            Organization.aggregate([ /*{ $match: { directoryId: organizationData.directoryId } },*/ { $group: { _id: '', orgIdNumber: { $max: "$orgIdNumber" } } }], function(err, result) {
                if (err) {
                    return;
                }
                autoidnum = result[0].orgIdNumber;
                organizationData.orgIdNumber = (autoidnum + 1);
                organizationData.org_id = (autoidnum + 1);

                // for AAD
                if (organizationData.isAADOrg && organizationData.parentId && (organizationData.parentId.org_id || organizationData.parentId.orgIdNumber)) {
                    var orgId = isNaN(parseInt(organizationData.parentId.orgIdNumber || organizationData.parentId.org_id)) ? "" : parseInt(organizationData.parentId.orgIdNumber || organizationData.parentId.org_id);
                    organizationData.orgIdNumber = organizationData.org_id = orgId;
                }
                Organization.create(organizationData, function(err, organization) {
                    if (err) {
                        Exception.log('ORGANIZATION', 'ADD', 'Organization add Error', err, by.by, function(err) {
                            if (err) console.log(err);
                        });
                        return callback(err);
                    }
                    Audit.log('ORGANIZATION', 'ADD', 'Organization added', organization, by.by, function(err) {
                        if (err) console.log(err);
                    });
                    return callback(null, organization);
                });
            });
        }
        myid();
    },
    addVersion: function(versionData, callback) {
        var by = { by: undefined };
        if (versionData.created) by.by = versionData.created || undefined;
        OrganizationVersion.create(versionData, function(err, version) {
            if (err) {
                Exception.log('ORG_VERSION', 'ADD', 'Organization verision add Error', err, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            // if (!version) return callback(null, false, { message: 'Organization verision not found' });
            Audit.log('ORG_VERSION', 'ADD', 'Organization verision added', version, by.by, function(err) {
                if (err) console.log(err);
            });
            return callback(null, version);
        });
    },
    update: function(organizationData, callback) {
        var by = { by: undefined };
        if (organizationData.updated) by = organizationData.updated.by || undefined;
        Organization.findByIdAndUpdate(organizationData._id, organizationData, function(err, organization) {
            if (err) {
                Exception.log('ORGANIZATION', 'UPDATE', 'Organization Update Error', err, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            Audit.log('ORGANIZATION', 'UPDATE', 'Organization Updated', organization, by.by, function(err) {
                if (err) console.log(err);
            });
            return callback(null, organization);
        });
    },
    upsertPersonnel: function(orgId, personnelData, removePersonnel, by, callback) {
        // mongoose.connection.on('open', function(err, conn) {
        var bulk = OrganizationPersonnel.collection.initializeOrderedBulkOp();
        if (removePersonnel && removePersonnel.length > 0) bulk.find({
            _id: {
                $in: removePersonnel.map(function(o) {
                    return mongoose.Types.ObjectId(o);
                })
            }
        }).update({ $set: { deleted: true } });
        var personnelIds = [];
        for (var i = 0; i < personnelData.length; i++) {
            if (personnelData[i]) {
                if (personnelData[i].assignment && personnelData[i].assignment.length > 0) {
                    for (var j = 0; j < personnelData[i].assignment.length; j++) {
                        if (!personnelData[i].assignment[j].orgId) personnelData[i].assignment[j].orgId = orgId;
                    }
                }
                if (!personnelData[i].orgId) personnelData[i].orgId = [];
                if (personnelData[i].orgId.indexOf(orgId) < 0) personnelData[i].orgId.push(orgId);
                var updateFlag = personnelData[i]._id;
                var personnelRecord = (new OrganizationPersonnel(personnelData[i])).toObject();
                if (!updateFlag) bulk.insert(personnelRecord);
                else {
                    var query = { _id: personnelRecord._id };
                    if (personnelIds.indexOf(personnelRecord._id) < 0) personnelIds.push(personnelRecord._id);
                    bulk.find(query).upsert().updateOne(personnelRecord);
                }
            }
        }
        bulk.execute(function(err, personnel) {
            if (err) {
                Exception.log('ORGANIZATION PERSONNEL', 'UPSERT', 'Organization Personnel Upsert Error', err, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            Audit.log('ORGANIZATION PERSONNEL', 'UPSERT', 'Organization Personnel Upserted', personnel, by.by, function(err) {
                if (err) console.log(err);
            });
            personnel.getInsertedIds().forEach(function(personnel) {
                if (personnelIds.indexOf(personnel._id) < 0) personnelIds.push(personnel._id);
            });
            personnel.getUpsertedIds().forEach(function(personnel) {
                if (personnelIds.indexOf(personnel._id) < 0) personnelIds.push(personnel._id);
            });
            Organization.findByIdAndUpdate(orgId, { $set: { personnel: personnelIds } }, callback);
            // return callback(null, personnel);
        });
        // });
        // @todo: Exception Logs and Audit Logs
    },
    upsertExhibition: function(organizationId, exhibitionData, removeExhibition, by, callback) {
        // mongoose.connection.on('open', function(err, conn) {
        var bulk = OrganizationExhibition.collection.initializeOrderedBulkOp();
        if (removeExhibition && removeExhibition.length > 0) bulk.find({
            _id: {
                $in: removeExhibition.map(function(o) {
                    return mongoose.Types.ObjectId(o);
                })
            }
        }).update({ $set: { deleted: true } });
        var exhibitionIds = [];
        for (var i = 0; i < exhibitionData.length; i++) {
            exhibitionData[i].orgId = organizationId;
            var updateFlag = exhibitionData[i]._id;
            var exhibitionRecord = (new OrganizationExhibition(exhibitionData[i])).toObject();
            if (!updateFlag) bulk.insert(exhibitionRecord);
            else {
                var query = { _id: exhibitionRecord._id };
                if (exhibitionIds.indexOf(exhibitionRecord._id) < 0) exhibitionIds.push(exhibitionRecord._id);
                bulk.find(query).upsert().updateOne(exhibitionRecord);
            }
        }
        bulk.execute(function(err, exhibition) {
            if (err) {
                Exception.log('ORGANIZATION EXHIBITION', 'UPSERT', 'Organization Exhibition Upsert Error', err, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            Audit.log('ORGANIZATION EXHIBITION', 'UPSERT', 'Organization Exhibition Upserted', exhibition, by.by, function(err) {
                if (err) console.log(err);
            });
            exhibition.getInsertedIds().forEach(function(exhibition) {
                if (exhibitionIds.indexOf(exhibition._id) < 0) exhibitionIds.push(exhibition._id);
            });
            exhibition.getUpsertedIds().forEach(function(exhibition) {
                if (exhibitionIds.indexOf(exhibition._id) < 0) exhibitionIds.push(exhibition._id);
            });
            Organization.findByIdAndUpdate(organizationId, { $set: { exhibition: exhibitionIds } }, callback);
            // return callback(null, exhibition);
        });
        // });
        // @todo: Exception Logs and Audit Logs
    },
    upsertChapterSpecification: function(organizationId, chapterSpecificationData, removeChapterSpecification, by, callback) {
        // mongoose.connection.on('open', function(err, conn) {
        var bulk = OrganizationChapterSpecification.collection.initializeOrderedBulkOp();
        if (removeChapterSpecification && removeChapterSpecification.length > 0) bulk.find({
            _id: {
                $in: removeChapterSpecification.map(function(o) {
                    return mongoose.Types.ObjectId(o);
                })
            }
        }).update({ $set: { deleted: true } });
        var chapterSpecificationIds = [];
        for (var i = 0; i < chapterSpecificationData.length; i++) {
            chapterSpecificationData[i].organizationId = organizationId;
            var updateFlag = chapterSpecificationData[i]._id;
            var chapterSpecificationRecord = (new OrganizationChapterSpecification(chapterSpecificationData[i])).toObject();
            if (!updateFlag) bulk.insert(chapterSpecificationRecord);
            else {
                var query = { _id: chapterSpecificationRecord._id };
                if (chapterSpecificationIds.indexOf(chapterSpecificationRecord._id) < 0) chapterSpecificationIds.push(chapterSpecificationRecord._id);
                bulk.find(query).upsert().updateOne(chapterSpecificationRecord);
            }
        }
        bulk.execute(function(err, chapterSpecification) {
            if (err) {
                Exception.log('ORGANIZATION CHAPTER SPECIFICATION', 'UPSERT', 'Organization Chapter Specification Upsert Error', err, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            Audit.log('ORGANIZATION CHAPTER SPECIFICATION', 'UPSERT', 'Organization Chapter Specification Upserted', chapterSpecification, by.by, function(err) {
                if (err) console.log(err);
            });
            chapterSpecification.getInsertedIds().forEach(function(chapterSpecification) {
                if (chapterSpecificationIds.indexOf(chapterSpecification._id) < 0) chapterSpecificationIds.push(chapterSpecification._id);
            });
            chapterSpecification.getUpsertedIds().forEach(function(chapterSpecification) {
                if (chapterSpecificationIds.indexOf(chapterSpecification._id) < 0) chapterSpecificationIds.push(chapterSpecification._id);
            });
            Organization.findByIdAndUpdate(organizationId, { $set: { chapterSpecification: chapterSpecificationIds } }, callback);
            // return callback(null, chapterSpecification);
        });
        // });
        // @todo: Exception Logs and Audit Logs
    },
    upsertVersion: function(id, version, updated, callback) {
        var by = updated;
        var status = version.status;
        // mongoose.connection.on('open', function(err, conn) {
        var bulk = OrganizationVersion.collection.initializeOrderedBulkOp();
        for (var i = id.length - 1; i >= 0; i--) {
            version.organization = id[i];
            version.created = updated;
            var versionRecord = (new OrganizationVersion(version)).toObject();
            if (versionRecord) bulk.insert(versionRecord);
        }
        bulk.execute(function(err, version) {
            if (err) {
                Exception.log('ORGANIZATION VERSION', 'ADD', 'Organization Version Add Error', err, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }

            Audit.log('ORGANIZATION VERSION', 'ADD', 'Organization Version Added', version, by.by, function(err) {
                if (err) console.log(err);
            });

            Organization.update({ _id: { $in: id } }, { $set: { versionId: status } }, { multi: true }, function(err, organizations) {
                if (err) {
                    Exception.log('ORGANIZATION', 'UPDATE', 'Organization Update Error', err, by.by, function(err) {
                        if (err) console.log(err);
                    });
                    return callback(err);
                }
                return callback(null, { version: version, organizations: organizations });
            });
        });
    },
    remove: function(organizationData, callback) {
        var by = { by: undefined };
        if (organizationData.updated) by = organizationData.updated.by || undefined;
        organizationData.deleted = true;
        Organization.findByIdAndUpdate(organizationData._id, organizationData, function(err, organization) {
            if (err) {
                Exception.log('ORGANIZATION', 'DELETE', 'Organization Delete Error', err, by.by, function(err) {
                    if (err) console.log(err);
                });
                return callback(err);
            }
            Audit.log('ORGANIZATION', 'DELETE', 'Organization Deleted', organization, by.by, function(err) {
                if (err) console.log(err);
            });
            return callback(null, organization);
        });
    }
};

function sortOrgs(organizations, manipulateAssignments) {
    var newOrgs = [];
    if (organizations && organizations.length > 0) {
        for (var i = 0; i < organizations.length; i++) {
            var org = organizations[i].toObject();
            // former names
            // if (org.formerNames && org.formerNames.length > 0)
            //     org.formerNames.sort(function(fn1, fn2) {
            //         return (fn1.deleted - fn2.deleted);
            //     });

            // features
            if (org.features && org.features.length > 0)
                org.features.sort(
                    firstBy('featureType', { ignoreCase: true })
                    .thenBy('sequenceNo')
                );
            // publication
            if (org.publication && org.publication.length > 0)
                org.publication.sort(function(fn1, fn2) {
                    return ((isNaN(parseInt(fn1.sequenceNumber)) ? 0 : parseInt(fn1.sequenceNumber)) - (isNaN(parseInt(fn2.sequenceNumber)) ? 0 : parseInt(fn2.sequenceNumber)));
                });
            // notes
            if (org.notes && org.notes.length > 0)
                org.notes.sort(function(fn1, fn2) {
                    return ((isNaN(parseInt(fn1.sequenceNo)) ? 0 : parseInt(fn1.sequenceNo)) - (isNaN(parseInt(fn2.sequenceNo)) ? 0 : parseInt(fn2.sequenceNo)));
                });
            // vendors
            if (org.vendors && org.vendors.length > 0)
                org.vendors.sort(function(fn1, fn2) {
                    return ((isNaN(parseInt(fn1.sequenceNumber)) ? 0 : parseInt(fn1.sequenceNumber)) - (isNaN(parseInt(fn2.sequenceNumber)) ? 0 : parseInt(fn2.sequenceNumber)));
                });
            // legalTitles
            if (org.legalTitles && org.legalTitles.length > 0)
                org.legalTitles.sort(function(fn1, fn2) {
                    return ((isNaN(parseInt(fn1.sequenceNo)) ? 0 : parseInt(fn1.sequenceNo)) - (isNaN(parseInt(fn2.sequenceNo)) ? 0 : parseInt(fn2.sequenceNo)));
                });
            // statistic
            if (org.statistic && org.statistic.length > 0)
                org.statistic.sort(function(fn1, fn2) {
                    return ((isNaN(parseInt(fn1.sequenceNo)) ? 0 : parseInt(fn1.sequenceNo)) - (isNaN(parseInt(fn2.sequenceNo)) ? 0 : parseInt(fn2.sequenceNo)));
                });
            // ocdContact
            if (org.ocdContact && org.ocdContact.length > 0)
                org.ocdContact.sort(function(fn1, fn2) {
                    return ((isNaN(parseInt(fn1.sequenceNo)) ? 0 : parseInt(fn1.sequenceNo)) - (isNaN(parseInt(fn2.sequenceNo)) ? 0 : parseInt(fn2.sequenceNo)));
                });
            // address
            if (org.address && org.address.length > 0)
                org.address.sort(function(fn1, fn2) {
                    return ((isNaN(parseInt(fn1.sequenceNo)) ? 0 : parseInt(fn1.sequenceNo)) - (isNaN(parseInt(fn2.sequenceNo)) ? 0 : parseInt(fn2.sequenceNo)));
                });
            // stathist
            if (org.stathist && org.stathist.length > 0) {
                org.stathist.sort(function(fn1, fn2) {
                    return ((isNaN(parseInt(fn2.sequenceNo)) ? 0 : parseInt(fn2.sequenceNo)) - (isNaN(parseInt(fn1.sequenceNo)) ? 0 : parseInt(fn1.sequenceNo)));
                });
                //org.stathist = org.stathist.slice(0, 5);
            }

            if (org.personnel) {
                for (var j = 0; j < org.personnel.length; j++) {
                    // org.personnel[j].notes
                    if (org.personnel[j].notes && org.personnel[j].notes.length > 0)
                        org.personnel[j].notes.sort(function(fn1, fn2) {
                            return ((isNaN(parseInt(fn1.sequenceNo)) ? 0 : parseInt(fn1.sequenceNo)) - (isNaN(parseInt(fn2.sequenceNo)) ? 0 : parseInt(fn2.sequenceNo)));
                        });
                    // org.personnel[j].ocdDegree
                    if (org.personnel[j].ocdDegree && org.personnel[j].ocdDegree.length > 0)
                        org.personnel[j].ocdDegree.sort(function(fn1, fn2) {
                            return ((isNaN(parseInt(fn1.sequenceNo)) ? 0 : parseInt(fn1.sequenceNo)) - (isNaN(parseInt(fn2.sequenceNo)) ? 0 : parseInt(fn2.sequenceNo)));
                        });
                    // org.personnel[j].assignment
                    if (manipulateAssignments) {
                        var assignments = [];
                        if (org.personnel[j].assignment && org.personnel[j].assignment.length > 0) {
                            for (var k = 0; k < org.personnel[j].assignment.length; k++) {
                                if (org.personnel[j].assignment[k].orgId.toString() == org._id.toString()) assignments.push(org.personnel[j].assignment[k]);
                            }
                            assignments.sort(function(fn1, fn2) {
                                return ((isNaN(parseInt(fn1.sequenceNo)) ? 0 : parseInt(fn1.sequenceNo)) - (isNaN(parseInt(fn2.sequenceNo)) ? 0 : parseInt(fn2.sequenceNo)));
                            });
                            org.personnel[j].assignment = assignments;
                        }
                    } else {
                        if (org.personnel[j].assignment && org.personnel[j].assignment.length > 0) {
                            org.personnel[j].assignment.sort(function(fn1, fn2) {
                                return ((isNaN(parseInt(fn1.sequenceNo)) ? 0 : parseInt(fn1.sequenceNo)) - (isNaN(parseInt(fn2.sequenceNo)) ? 0 : parseInt(fn2.sequenceNo)));
                            });
                        }
                    }
                    // org.personnel[j].ocdContact
                    if (org.personnel[j].ocdContact && org.personnel[j].ocdContact.length > 0)
                        org.personnel[j].ocdContact.sort(function(fn1, fn2) {
                            return ((isNaN(parseInt(fn1.sequenceNo)) ? 0 : parseInt(fn1.sequenceNo)) - (isNaN(parseInt(fn2.sequenceNo)) ? 0 : parseInt(fn2.sequenceNo)));
                        });
                }
            }

            newOrgs.push(org);
        }
    }
    return newOrgs;
}
