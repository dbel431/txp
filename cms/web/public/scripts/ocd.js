$(document).ready(function() {
 if(navigator.userAgent.match(/Trident\/7\./)) { // if IE
        $('body').on("mousewheel", function () {
            // remove default behavior
            event.preventDefault(); 

            //scroll without smoothing
            var wheelDelta = event.wheelDelta;
            var currentScrollPosition = window.pageYOffset;
            window.scrollTo(0, currentScrollPosition - wheelDelta);
        });
};
});


jQuery(document).ready(function($) {  

// site preloader -- also uncomment the div in the header and the css style for #preloader
$(window).load(function(){
	$('#preloader').fadeOut('slow',function(){$(this).remove();});
});

});
 // $(window).on("hashchange", function () {
    // window.scrollTo(window.scrollX, window.scrollY + 200);
// });

// jQuery to collapse the navbar on scroll
$(window).scroll(function() {
    if ($(".navbar").offset().top > 50) {
        $(".navbar-fixed-top").addClass("top-nav-collapse");
    } else {
        $(".navbar-fixed-top").removeClass("top-nav-collapse");
    }
});




// jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
	
    $('a.page-scroll').bind('click', function(event) {
		
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top)
			
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});

// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(function() {
    $('.navbar-toggle:visible').click();
});

$(document).ready(function(e) {
 
    $('div#back_to_top').hide();    // hide button first
     
    $(window).scroll(function () {
        if ($(this).scrollTop() > 150) {
            $('#back_to_top').fadeIn();
        } else {
            $('#back_to_top').fadeOut();
        }
    });
 
    // scroll body to top when the button is clicked
    $('#back_to_top a').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });
     
});

$(function() {
	if($('.responsive-tabs') != undefined && $('.responsive-tabs').length > 0){
		$('.responsive-tabs').responsiveTabs({
           accordionOn: ['xs', 'sm']
         });
	}
});