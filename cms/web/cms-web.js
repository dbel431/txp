var config = require('../../config.js');
const express = require('express');
const path = require('path');
const compression = require('compression');
const errorHandler = require('errorhandler');
const app = express();


// app set
app.set('port', process.env.CMS_PORT || 4004);


app.set('views', __dirname + '/views');
// app.set('view engine', 'html');

// app get

app.use(compression());

app.use('/assets', express.static(__dirname + '/public/scripts/'));
app.use('/assets', express.static(__dirname + '/public/OCD/CategoryLogo'));
app.use('/assets', express.static(__dirname + '/public/OCD/CompanyLogo'));
app.use('/assets', express.static(__dirname + '/public/OCD/PersonnelLogo'));
app.use('/assets', express.static(__dirname + '/public/styles/'));
app.use('/assets', express.static(__dirname + '/public/images/'));
app.use('/assets', express.static(__dirname + '/public/fonts/'));
app.use('/assets', express.static(__dirname + '/../bower_components/'));
app.use('/assets', express.static(__dirname + '/common/'));
app.use('/assets', express.static(__dirname + '/public/'));
app.use('/assets', express.static(__dirname + '/views'));

app.use('/config', express.static(__dirname + "/../../config"));

app.get('/:directoryName/*', function(req, res) {
    res.sendFile(path.join(__dirname, '/views/index.html'));
});
app.get('/NRP/', function(req, res) {
    res.sendFile(path.join(__dirname, '/views/NRP/index.html'));
});

app.listen(app.get('port'), () => {
    console.log('Express server listening on port %d in %s mode', app.get('port'), app.get('env'));
});

module.exports = app;
