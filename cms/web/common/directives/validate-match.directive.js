angular.module('web').directive('validateMatch', [function() {
    return {
        scope: {
            validateMatch: '='
        },
        require: 'ngModel',
        restrict: 'A',
        link: function(scope, iElm, iAttrs, ngModel) {
            ngModel.$validators.validateMatch = function(modelValue) {
                return modelValue == scope.validateMatch;
            };

            scope.$watch("validateMatch", function() {
                ngModel.$validate();
            });
        }
    };
}]);
