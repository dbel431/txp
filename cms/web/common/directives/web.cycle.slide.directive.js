angular.module('web')
  .directive('cycle', function() {
    return {
        restrict: 'A',
        priority: 1001,
        link: function(scope, element, attrs) {
           setTimeout(function(){
              $(element).cycle({
                fx: 'scrollHorz',
                timeout: 0,
                slides : 'div',
                prev: '.prev',
                next: '.next'
              });
           }, 0);
        }
    };
  });