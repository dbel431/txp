angular.module('web')
    .directive('scrollOnClick', function() {
        return {
            restrict: 'A',
            link: function(scope, $elm, attrs) {
                attrs.$observe('params', function(value) {
                    if (value.indexOf("#") != -1) {
                        $elm.attr('href', value);
                        setTimeout(function () {
                            angular.element($elm).attr('ui-sref', '.');
                            angular.element($elm).parent('li').removeAttr('ui-sref-active');
                        }, 1000)
                    }
                });

                $elm.on('click', function(event) {  
                    angular.element("#navbar ul").children('li').removeClass('active');
                    angular.element($elm).parent('li').addClass('active');
                	var idToScroll = attrs.params ? attrs.params : attrs.href;
                    var $target;
                    if (idToScroll) {
                        $target = $(idToScroll);
                    } else {
                        $target = $elm;
                    }
                    $('body, html').stop().animate({
                        scrollTop: ($target.offset().top + 5)
                    }, 1500, 'easeInOutExpo');
                    event.preventDefault();
                });
            }
        }
    });
