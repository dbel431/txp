angular.module('web').directive('validateIsEmailUnique', [
    '$rootScope',
    '$stateParams',
    'SubscriberService',
    function($rootScope, $stateParams, SubscriberService) {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function(scope, iElm, iAttrs, ngModel) {
                // ngModel.$validators.validateIsEmailUnique = function(modelValue) {
                //     if (modelValue == '' || modelValue == undefined) return true;
                //     var auth = new SubscriberService();
                //     return SubscriberService.get({
                //         'directoryId': $rootScope.currentDirectory.directoryId,
                //         'auth.email': modelValue
                //     }, function(users) {
                //         ngModel.$setValidity('validateIsEmailUnique', (users.length <= 0));
                //     }, function(err) {
                //         ngModel.$setValidity('validateIsEmailUnique', false);
                //     });
                // };
                iElm.on('blur', function() {
                    var modelValue = iElm.val();
                    if (modelValue == '' || modelValue == undefined) return true;
                    var auth = new SubscriberService();
                    return SubscriberService.get({
                        'directoryId': $rootScope.currentDirectory.directoryId,
                        'auth.email': modelValue
                    }, function(users) {
                        ngModel.$setValidity('validateIsEmailUnique', (users.length <= 0));
                    }, function(err) {
                        ngModel.$setValidity('validateIsEmailUnique', false);
                    });
                });
            }
        };
    }
]);
