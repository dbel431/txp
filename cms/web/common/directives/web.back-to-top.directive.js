angular.module('web')
    .directive('backToTop', function() {
        return {
            restrict: 'A',
            link: function(scope, $elm, attrs) {

                angular.element('#back_to_top').hide();

                $elm.on('click', function(event) {
                    $('body, html').animate({
                        scrollTop: 0
                    }, 500);
                    event.preventDefault();
                });
            }
        }
    });
