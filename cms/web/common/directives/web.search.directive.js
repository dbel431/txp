angular.module('web').directive('directoryWebUserRecordActions', [
    '$rootScope',
    function($rootScope) {
        return {
            scope: {
                'recordId': '=',
                'openDeleteModal1': '=',
            },
            restrict: 'EA',
            templateUrl: '/assets/search/search.html',
            replace: true,
            link: function(scope, elt, attb) {
                scope.isActionPermitted = $rootScope.isActionPermitted;
            }
        };
    }
]);
