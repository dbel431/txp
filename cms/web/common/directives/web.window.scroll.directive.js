angular.module('web')
.directive("scroll", function ($window) {
    return function(scope, element, attrs) {
        angular.element($window).bind("scroll", function() {
            var scrollPos = angular.element($window).scrollTop();
            var navItem = angular.element('#navbar a.page-scroll');

            if (angular.element($window).scrollTop() > 150) {
                angular.element('#back_to_top').fadeIn();
            } else {
                angular.element('#back_to_top').fadeOut();
            }

            angular.element('#navbar a.page-scroll').each(function() {
                var currLink = angular.element(this);
                var currHash = angular.element(this).attr('href');
                if(currHash.indexOf('/')==-1){
                var refElement = angular.element(currHash);
                if (refElement.length) {
                    if (refElement.offset().top <= scrollPos && refElement.offset().top + refElement.height() > scrollPos) {
                        angular.element('#navbar ul li').removeClass("active");
                        angular.element(currLink).parent('li').addClass("active");
                    }
                    else{
                        angular.element(currLink).parent('li').removeClass("active");
                    }
                }
                }


   
            });
            scope.$apply();
        });
    };
});