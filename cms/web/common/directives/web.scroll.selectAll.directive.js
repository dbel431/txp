angular.module('web')
  .directive('selectAll', function($timeout, $parse) {
  return {
    restrict: 'A',
    link: function (scope, masterElement, attrs) {
      var slaveName = attrs.selectAll;
      var slaveSelector = ':checkbox[rel="' + slaveName + '"]';

      masterElement.on('click', function() {
        angular.element(slaveSelector).each(function(i, elem) {
          var localScope = angular.element(elem).scope();
          var model = $parse(angular.element(elem).attr('ng-model'));
          model.assign(localScope, masterElement.prop('checked'));
          localScope.$apply();
        });
      });

      $timeout(function() {
        var slaveElements = angular.element(slaveSelector);
        var setMasterState = function() {
          var checkedSlaves = slaveElements.filter(function(i, elem) {
            return angular.element(elem).prop('checked');
          });
          var isChecked = (checkedSlaves.length === slaveElements.length);
          var isIndeterminate = (checkedSlaves.length > 0 && checkedSlaves.length < slaveElements.length);
          masterElement.prop('checked', isChecked);
          masterElement.prop('indeterminate', isIndeterminate);
        };
        setMasterState();
        slaveElements.on('click', setMasterState);
      });
    }
  };
})