angular.module('web')
    .directive('loading', ['$http', function($http) {
        return {
            restrict: 'A',
            link: function(scope, elm, attrs) {
                scope._noLoader = false;
                scope.isLoading = function() {
                    if(window.location.pathname.toUpperCase().indexOf('SEARCH')!= -1 )
                    {
                        return false;
                    }
                    else
                    return $http.pendingRequests.length > 0;
                    
                };
                scope.$watch(scope.isLoading, function(v) {
                    if (!scope._noLoader && v) {
                        elm.show();
                    } else {
                        elm.hide();
                    }
                });
            }
        };

    }]);
