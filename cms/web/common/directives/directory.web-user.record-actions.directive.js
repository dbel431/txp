angular.module('web').directive('directoryWebUserRecordActions', [
    '$rootScope',
    function($rootScope) {
        return {
            scope: {
                'recordId': '='
            },
            restrict: 'EA',
            templateUrl: '/assets/search/record-actions.tpl.html',
            replace: true
        };
    }
]);