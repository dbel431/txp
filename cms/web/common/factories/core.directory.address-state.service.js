angular.module('web')
    .service('DirectoryStateService', [
        '$resource',
        function($resource) {
            return $resource(APIBasePath + 'directory/state', {}, {
                list: {
                    method: 'POST',
                    isArray: true
                }
            });
        }
    ]);