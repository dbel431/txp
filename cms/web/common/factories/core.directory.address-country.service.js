angular.module('web')
    .service('DirectoryCountryService', [
        '$resource',
        function($resource) {
            return $resource(APIBasePath + 'directory/country', {}, {
                list: {
                    method: 'POST',
                    isArray: true
                }
            });
        }
    ]);
