angular.module('web')
    .service('DirectoryFeatureCodeService', [
        '$resource',
        function($resource) {
            return $resource(APIBasePath + 'directory/feature-code', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);

