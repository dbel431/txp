angular.module('web')
    .service('DirectorySpecificationCodeService', [
        '$resource',
        function($resource) {
            return $resource(APIBasePath + 'directory/specification-code', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);

