angular.module('web')
    .service('DirectoryInstitutionCategoryService', [
        '$resource',
        function($resource) {
            return $resource(APIBasePath + 'directory/institution-category', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);