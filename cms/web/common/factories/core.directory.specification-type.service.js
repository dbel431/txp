angular.module('web')
    .service('DirectorySpecificationTypeService', [
        '$resource',
        function($resource) {
            return $resource(APIBasePath + 'directory/specification-type', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);
