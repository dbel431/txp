angular.module('web').service('DirectoryWebUserService', [
    '$rootScope',
    '$resource',
    function($rootScope, $resource) {
        return $resource(APIBasePath + 'subscriber/search', {}, {
            get: {
                method: 'GET',
                isArray: true
            },
            list: {
                method: 'POST',
                url: APIBasePath + 'subscriber/search/list',
                isArray: true
            },
            getExhibitionList: {
                method: 'GET',
                url: APIBasePath + 'subscriber/search/exhibition/list',
                isArray: true
            },
            update: {
                method: 'PUT'
            }
        });
    }
]);