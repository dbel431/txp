'use strict';
angular.module('web')
	.filter('html', ['$sce', function ($sce) { 
	    return function (text) {
	        return $sce.trustAsHtml(text);
	    };    
	}])