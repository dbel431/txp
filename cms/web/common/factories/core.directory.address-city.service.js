angular.module('web')
    .service('DirectoryCityService', [
        '$resource',
        function($resource) {
            return $resource(APIBasePath + 'directory/city', {}, {
                list: {
                    method: 'POST',
                    isArray: true
                }
            });
        }
    ]);