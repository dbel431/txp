angular.module('web')
    .service('DirectoryDisbursalTypeService', [
        '$resource',
        function($resource) {
            return $resource(APIBasePath + 'directory/disbursal-type', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);

