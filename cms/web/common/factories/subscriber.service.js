angular.module('web')
    .service('SubscriberService', [
        '$resource',
        function($resource) {
            return $resource(APIBasePath + 'subscriber', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                },
                list: {
                    url: APIBasePath + 'subscriber/list',
                    method: 'POST',
                    isArray: true
                },
                login: {
                    url: APIBasePath + 'subscriber/login',
                    method: 'POST'
                },
                register: {
                    url: APIBasePath + 'subscriber/register',
                    method: 'POST'
                },
                logout: {
                    url: APIBasePath + 'subscriber/logout',
                    method: 'POST'
                },
                profile: {
                    url: APIBasePath + 'subscriber/profile',
                    method: 'GET'
                },
                changePassword: {
                    url: APIBasePath + 'subscriber/change-password',
                    method: 'PUT'
                },
                update: {
                    method: 'PUT'
                }
            });
        }
    ]);
