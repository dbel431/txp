'use strict';

angular.module('web')
    .factory('$dataService', ['$http', '$dataShare','dataCommonService', function($http, $dataShare,dataCommonService) {
        dataCommonService.data=0;
        var directoryDetails = $dataShare.getDir();
        return {
            get: get,
            post: post,
            
        };
        function get(apiURL, successCallback, errorCallback) {
            
           
            var api = APIBasePath + apiURL;
            if (api.indexOf('?') > -1) {
                api += '&directoryName=' + directoryDetails.name;
            } else {
                api += '?directoryName=' + directoryDetails.name;
            }
            dataCommonService.data=1;
            $http
                .get(api)
                .success(function(data, status, headers, config) {
                    if (successCallback != null) {
                        successCallback(data, status, headers, config);
                        dataCommonService.data=0;
                    }
                })
                .error(function(error) {
                    errorCallback(error);
                });
        }

        function post(url, data, successCallback, errorCallback) {
            $http
                .post(APIBasePath + url, data)
                .success(function(response) {
                    if (successCallback != null) {
                        successCallback(response);
                    }
                })
                .error(function(err) {
                    if (errorCallback != null) {
                        errorCallback(err);
                    }
                });
        }
    }])
