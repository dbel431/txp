angular.module('web')
    .service('DirectoryOrganizationService', [
        '$rootScope',
        '$stateParams',
        '$resource',
        function($rootScope, $stateParams, $resource) {
            return $resource(APIBasePath + 'subscriber/search', {}, {
                dataTable: {
                    method: 'POST',
                    url: APIBasePath + 'subscriber/search/data-table',
                    transformRequest: function(data, headerGetter) {
                        var headers = headerGetter();
                        var obj = {
                            directoryId: $rootScope.currentDirectory.directoryId,
                            name: { $exists: true },
                            name:{$ne:null}
                        };
                        if (obj.directoryId == '57189cd924d8bc65f4123bc3')
                            obj['classificationCodeName'] = {
                                $in: [
                                    "US Diocese", "World Diocese", "Religious Order/Miscellaneous",
                                    "Curia", "Curia Organization", "Base/Generic Organization",
                                    "Religious Order Organization", "Parish", "School", "Shrine",
                                    "Church", "Hospital", "Personal Prelature", "Propogation of the Faith",
                                    "Rectory", "Missionary Activities", "Cemetery", "Endowment",
                                    "Diocese Religious Order"
                                ]
                            };
                        var newData = {
                            dataTableQuery: data,
                            conditions: obj
                        }
                        return JSON.stringify(newData);
                    }
                },
                list: {
                    method: 'POST',
                    url: APIBasePath + 'subscriber/search/list',
                    isArray: true
                }
            });
        }
    ]);
