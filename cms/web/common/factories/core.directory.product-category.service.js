angular.module('web')
    .service('DirectoryProductCategoryService', [
        '$resource',
        function($resource) {
            return $resource(APIBasePath + 'directory/getProductCategory', {}, {
                list: {
                    method: 'POST',
                    isArray: true
                },
                getProductSubCategory: {
                    method: 'POST',
                    url: APIBasePath + 'directory/getProductSubCategory',
                    isArray: true
                }
            });
        }
    ]);
