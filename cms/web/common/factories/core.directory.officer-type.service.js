angular.module('web')
    .service('DirectoryOfficerTypeService', [
        '$resource',
        function($resource) {
            return $resource(APIBasePath + 'directory/officer-type', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);
