angular.module('web')
    .factory('$dataShare', function($stateParams) {
        return {
            getDir: getDirectoryDetails
        };

        function getDirectoryDetails() {
            var dirs = [
                { 'name': 'AAD', 'directoryId': '57189c7024d8bc65f4123bc0', 'longname': 'American Art Directory' },
                { 'name': 'CFS', 'directoryId': '57189cc224d8bc65f4123bc1', 'longname': 'Corporate Finance Sourcebook' },
                { 'name': 'DMMP', 'directoryId': '57189ccf24d8bc65f4123bc2', 'longname': 'Direct Marketing Market Place' },
                { 'name': 'OCD', 'directoryId': '57189cd924d8bc65f4123bc3', 'longname': 'Official Catholic Directory' },
                { 'name': 'OMD', 'directoryId': '57189b3e24d8bc65f4123bbf', 'longname': 'Official Museum Directory' },
                { 'name': 'NRP', 'directoryId': '57baa29bc2af752141530848', 'longname': 'National Register Publishing' }
            ];
            var directoryName = $stateParams.directoryName.toUpperCase();
            for (var i = 0; i < dirs.length; i++) {
                if (directoryName === dirs[i].name) {
                    console.log(dirs[i]);
                    // $stateParams.directoryId = dirs[i].directoryId;
                    return dirs[i];
                }
            }
        }
    });
