angular.module('web')
    .service('DirectoryClassificationCodeService', [
        '$resource',
        function($resource) {
            return $resource(APIBasePath + 'directory/classification', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);

