angular.module('web')
    .service('DirectorySectionService', [
        '$resource',
        function($resource) {
            return $resource(APIBasePath + 'directory/section', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);

