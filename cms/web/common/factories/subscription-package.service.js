angular.module('web')
    .service('SubscriptionPackageService', [
        '$resource',
        function($resource) {
            return $resource(APIBasePath + 'subscription-package', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                },
                list: {
                    url: APIBasePath + 'subscription-package/list',
                    method: 'POST',
                    isArray: true
                },
                update: {
                    method: 'PUT'
                }
            });
        }
    ]);
