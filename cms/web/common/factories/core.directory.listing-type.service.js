angular.module('web')
    .service('DirectoryListingTypeService', [
        '$resource',
        function($resource) {
            return $resource(APIBasePath + 'directory/listing-type', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);
