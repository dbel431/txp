angular.module('web')
    .service('DirectoryCollectionCategoryService', [
        '$resource',
        function($resource) {
            return $resource(APIBasePath + 'directory/collection-category', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);