'use strict';
angular.module('web')
    .controller('web.home.products-services.details.controller', ['$scope', '$location', '$dataService', '$sce', '$stateParams', '$rootScope', '$state', 'DirectoryStateService', function($scope, $location, $dataService, $sce, $stateParams, $rootScope, $state, DirectoryStateService) {
        $scope.direcoryName = $rootScope.currentDirectory.name;
        setTimeout(function(){
            $('html,body').animate({scrollTop: $('.products-search-box').offset().top-50},'fast');
        },100);
        var str = document.URL;
        var start =str.indexOf($scope.direcoryName);
        $scope.baseURL=str.slice(0 ,start);
        $scope.bindHtml=function(data){
            return $sce.trustAsHtml(data);

        }
        $dataService.get('web-data?pageName=products-services-details',
            function(data) {
                console.log(data);
                if (data != null) {
                    $scope.sections = data;
                }
            },
            function(error) {
                console.log(error)
            })
        //var x= {directoryName: "OMD", CompanyName: "DORFMAN MUSEUM FIGURES, INC."};
        var x={_id:$stateParams.CompanyName};

        $dataService.post('products-services/getGuide', x,
            function(data) {
                if (data != null) {
                    $scope.getGuide = data;
                    //$scope.getGuide[0].imgSrc=$scope.baseURL
                    if($scope.direcoryName=="OMD")
                    {
                        $scope.getGuide[0].imgSrc=$scope.baseURL+'assets/OMD/OMD_Web_Logos/'+$scope.getGuide[0].companyDetailsLogo;
                    }
                    if($scope.direcoryName=="OCD")
                    {
                        $scope.getGuide[0].imgSrc=$scope.baseURL+'assets/OCD/OCD_Web_Logos/'+$scope.getGuide[0].companyDetailsLogo;
                    }

                }
            },
            function(error) {
                console.log(error)
            })
        $scope.productCategoryList = function(item) {
            debugger;
            var results = { "ProductSubCategoryNames": item }
                //console.log($state);
            $state.go('core.productsList', results);

        }


        $scope.addEmailAFriend = { "directoryId": $rootScope.currentDirectory.directoryId, "direcoryName": "OCD", "requestType": "Email A Friend", "details": "", "friendsEmail": "", "email": "", "name": { "first": "" }, "subject": "" };
        $scope.ClickEmailAFriend = function() {

            var absUrl = $location.absUrl();
            //console.log(absUrl);
            $scope.addEmailAFriend.details = $scope.addEmailAFriend.details;
            $scope.addEmailAFriend.detailsReq = "Hi, I thought you might be interested in this company profile  <a href='" + absUrl + "'>" + absUrl + "</a>";
            $scope.addEmailAFriend.subject = $scope.addEmailAFriend.name.first + " sends you a link to" + " " + $scope.getGuide[0].CompanyName

            $dataService.post('subscriber/web-request', $scope.addEmailAFriend,

                function(data) {
                    // console.log('sasgar',data);
                    if (data != null) {
                        $scope.addEmailAFriend = { "directoryId": $rootScope.currentDirectory.directoryId, "direcoryName": "OCD", "requestType": "Email A Friend", "details": "", "email": "", "name": { "first": "" }, "subject": "" };
                        showMessages([{
                            type: 'success',
                            message: 'Email sent successfully!',
                            header: 'Email Submission'
                        }]);
                        //$scope.addEmailAFriendMsg = "Email sent successfully!";
                    }
                },
                function(error) {
                    $scope.addEmailAFriend = { "directoryId": $rootScope.currentDirectory.directoryId, "direcoryName": "OCD", "requestType": "Advertise Inquiry", "details": "", "email": "", "name": { "first": "" }, "subject": "" };
                    //$scope.addEmailAFriendMsg = "Email not sent successfully!";
                    console.log('addEmailAFriend', error)
                    showMessages([{
                        type: 'error',
                        message: 'Email not sent successfully!',
                        header: 'Email Submission'
                    }]);
                })
        };

        // $dataService.get('products-services/getProductCategory',
        //     function(data) {
        //         if (data != null) {
        //             $scope.getProductCategory = data;
        //         }
        //     },
        //     function(error) {
        //         //console.log(error)
        //     })

        // $dataService.get('products-services/postProductSubCategory',
        //     function(data) {
        //         if (data != null) {
        //             $scope.getProductSubCategory = data;
        //         }
        //     },
        //     function(error) {
        //         //console.log(error)
        //     })


        /*  $scope.printDiv = function(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var popupWin = window.open('', '_blank', 'width=300,height=300');
            popupWin.document.open();
            popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="assets/custom.css" /></head><body onload="window.print()">' + printContents + '</body></html>');
            popupWin.document.close();
        }
*/
        $scope.printDiv = function(divName) {

            if( $scope.direcoryName=='OCD')
            {
                var title="The Official Catholic Directory/Search Suppliers";
            }
            if($scope.direcoryName=='OMD')
            {
             var title="The Official Museum Directory/Search Suppliers";   
            }
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1;
            var yyyy = today.getFullYear();
            var datearr=mm+'/'+dd+'/'+yyyy;
            var printContents = document.getElementById(divName).innerHTML;
            console.log($scope.getGuide);
            var footer="Source:"+$scope.baseURL+"  Date:"+ datearr +"<br>National Register Publishing<br> © 2016 Marquis Who's Who LLC. All rights reserved.";
            

            var popupWin = window.open('', '_blank', 'width=595,height=842');
            popupWin.document.open();

            //popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="{{$scope.baseURL}}styles/custom.css"/> <link rel="stylesheet" type="text/css" href="{{$scope.baseURL}}styles/media-query.css" /><link rel="stylesheet" type="text/css" href="{{$scope.baseURL}}bootstrap/dist/css/bootstrap.min.css"/> <style>@media screen {p.bodyText {font-family:verdana, arial, sans-serif;} } @media print {p.bodyText {font-family:georgia, times, serif;} } @media screen, print {p.bodyText {font-size:10pt} .inline-print{float:left;width: 20%;}.align-print-left{float:right} .dark-orange {color: #d7620c !important;} .dark-orange  b{color: #d7620c !important;} .personnel .col-xs-6{ float:left !important; width: 20% !important; } }</style></head><body onload="window.print()"><h2>'+ title +'</h2><hr>' + printContents + '</body></html>');
            popupWin.document.write('<html><head><style>@media screen {p.bodyText {font-family:verdana, arial, sans-serif;} } @media print {p.bodyText {font-family:georgia, times, serif;} } @media screen, print {p.bodyText {font-size:10pt} .inline-print{float:left;width: 20%;}.dark-orange {color: #d7620c !important;} .dark-orange  b{color: #d7620c !important;} .personnel .col-xs-6{ float:left !important; width: 20% !important; } }</style> <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> </head><body onload="window.print()"><h2>'+ title +'</h2><hr>' + printContents + '<center><br><br><br>'+footer+'</center></body></html>');
            //popupWin.document.write('<img src="/assets/tempPersonal.jpg">');
            popupWin.document.close();

        }
        $dataService.post('directory/state', {},
            function(data) {

                if (data != null) {
                    $scope.StateArray = data;
                }
            },
            function(error) {
                //console.log(error)
            })

        $scope.val = { "KeywordSearch": '', "Zip": '' };

        $scope.Reset = function() {
            $scope.val = { "KeywordSearch": '', "Zip": '' };
            document.getElementById("singleSelect").selectedIndex = "0";

        }


        //State
        $scope.getAddressStates = function(val) {
            $('[ng-show="noResultsState"]').html('<i class="glyphicon glyphicon-remove"></i> No Results Found');
            if (val.length > 2) {
                return DirectoryStateService.list({
                    codeValue: {
                        _eval: 'regex',
                        value: val
                    },
                    active: true
                }).$promise;
            } else $('[ng-show="noResultsState"]').html('Enter minimum 3 characters');
        };
        $scope.initAddressStates = function() {
            $("#auto-complete-state")
                .autocomplete({
                    source: function(req, res) {
                        $scope.getAddressStates(req.term).then(function(data) {
                            res(data);
                        });
                    },
                    focus: function(event, ui) {
                        $("#auto-complete-state").val(ui.item.codeValue + " (" + ui.item.description + ")");
                        return false;
                    },
                    minLength: 3,
                    select: function(evt, ui) {
                        $scope.selectAddressStateTypeAhead(ui.item, ui.item);
                        $('#auto-complete-state').val($scope.val.State);
                        //$('#auto-complete-country').val($scope.addDatacard.country);
                        return false;
                    }
                })
                .autocomplete("instance")._renderItem = function(ul, item) {
                    return $("<li>")
                        .append("<div>" + item.codeValue + " (" + item.description + ")" + "<span>, " + (item.parentId ? item.parentId.codeValue : "") + "</span>" + "</div>")
                        .appendTo(ul);
                };
        };
        $scope.selectAddressStateTypeAhead = function($item, $model, $label, $event) {
            //$scope.val.State = $model.codeValue + " (" + $model.description + ")";
            $scope.val.State = $model.codeValue;
            //$scope.val.zip = $model.zip.codeValue;
        };

        setTimeout(function() {
            $scope.initAddressStates();
        }, 100);

        // $scope.KeyWordSearch = function(val) {



        //     var obj = { "directoryName": $rootScope.currentDirectory.name }; // Do not delete this field
        //     console.log(val.KeywordSearch);
        //     var key = val.KeywordSearch;
        //     var results = { "keywordSearch": val.KeywordSearch };

        //     if (val.chk == true) {
        //         if ((key != undefined) && (key != '')) {
        //             obj['companyName'] = { _eval: 'regex', value: key };
        //             if (!obj['$or']) obj['$or'] = [];
        //             obj['$or'].push({ 'categories.ProductCategoryName': { _eval: 'regex', value: key } });
        //             obj['$or'].push({ 'categories.ProductSubCategoryName': { _eval: 'regex', value: key } });
        //         }

        //     } else {
        //         if ((key != undefined) && (key != '')) {

        //             obj['companyName'] = { _eval: 'regex', value: key };
        //         }
    
        //     }

        //     if ((val.State != undefined) && (val.State != '')) {
                
        //         obj['companyAddress.state'] = { _eval: 'regex', value: val.State };
        //     }
        //     if ((val.Zip != undefined) && (val.Zip != '')) {
        //         console.log(val.Zip.charAt(0));

        //         if (!isNaN(val.Zip.charAt(0))) {

        //             obj['companyAddress.zip'] = { _eval: 'regex', value: val.Zip };

        //         } else {
        //             obj['companyAddress.city'] = { _eval: 'regex', value: val.Zip };

        //         }



        //     }
        //     $rootScope.obj1 = obj;

        //     $state.go('core.productsSearch');

        // }
        $scope.KeyWordSearch = function(val) {

            $rootScope.KeyWordSearch=val;

            var obj = { "directoryName": $rootScope.currentDirectory.name };

             // Do not delete this field
            console.log(val.KeywordSearch);
            
            var key = val.KeywordSearch;
                     
            // console.log('val');
            var results = { "keywordSearch": val.KeywordSearch };
      
            if(val.KeywordSearch)
            {
             obj['$or'] = [];
                // obj['$or'].push({'companyName': { _eval: 'regex', value: val.KeywordSearch} });    
                // obj['$or'].push({'companyName': val.KeywordSearch});   
            obj['$or'].push({'categories.ProductCategoryName': { _eval: 'regex', value: val.KeywordSearch }});
                obj['$or'].push({'categories.ProductCategoryName': val.KeywordSearch});
                obj['$or'].push({'categories.ProductSubCategoryName': { _eval: 'regex', value: val.KeywordSearch }});
                obj['$or'].push({'categories.ProductSubCategoryName': val.KeywordSearch});
                obj['$or'].push({'companyName': { _eval: 'regex', value: val.KeywordSearch} });    
                obj['$or'].push({'companyName': val.KeywordSearch});
                obj['$or'].push({'companyDescription':{ _eval: 'regex', value: val.KeywordSearch}});
            }
            if(val.ProductSubCategoryName)
            {
                obj['$or'] = [];
                
                obj['$or'].push({'categories.ProductCategoryName': { _eval: 'regex', value: val.ProductSubCategoryName }});
                obj['$or'].push({'categories.ProductCategoryName': val.ProductSubCategoryName});
                obj['$or'].push({'categories.ProductSubCategoryName': { _eval: 'regex', value: val.ProductSubCategoryName }});
                obj['$or'].push({'categories.ProductSubCategoryName': val.ProductSubCategoryName});
                obj['$or'].push({'companyName': { _eval: 'regex', value: val.ProductSubCategoryName} });    
                obj['$or'].push({'companyName': val.ProductSubCategoryName});
                obj['$or'].push({'companyDescription':{ _eval: 'regex', value: val.ProductSubCategoryName}});
            }


            

            if ((val.State != undefined) && (val.State != '')) {
                // if(!obj['$or']) obj['$or'] = [];
                obj['companyAddress.state'] = { _eval: 'regex', value: val.State } ; 
            }
            if ((val.Zip != undefined) && (val.Zip != '')) {
                console.log(val.Zip.charAt(0));

                if (!isNaN(val.Zip.charAt(0))) {
                  
                    obj['companyAddress.zip']= { _eval: 'regex', value: val.Zip } ;
                    
                } else {
                    obj['companyAddress.city']= { _eval: 'regex', value: val.Zip } ;
                   
                }



            }
            
            // var obj2 = {};

            // obj2['directoryId'] = "OMD";
            // obj2['categories.ProductCategoryName'] = { _eval: 'regex', value: "Au" };
            // obj2['categories.ProductSubCategoryName'] = { _eval: 'regex', value: "Au" };
            //{"KeywordSearch":"","Zip":"","chk":false}
        
            $rootScope.obj1 = obj;
            $state.go('core.productsSearch');
        }





    }])
