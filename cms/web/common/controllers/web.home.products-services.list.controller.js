'use strict';
angular.module('web')
    .run(function($rootScope, $state) {
        $rootScope.$on('$stateChangeError', function(err) {
            //  console.log('$stateChangeError');
        })
    })
    .controller('web.home.products-services.list.controller', ['$scope', '$location', '$dataService', '$sce', '$stateParams', '$state', 'DTOptionsBuilder', '$rootScope', 'DirectoryStateService', '$localStorage', '$http',function($scope, $location, $dataService, $sce, $stateParams, $state, DTOptionsBuilder, $rootScope, DirectoryStateService, $localStorage,$http) {
        $scope.location={};
        $scope.getGuideData={};
        $scope.radius=0;
        $scope.sections={};
        $scope.dtInstance={};
        $scope.val={'chk':false};
         $scope.bindHtml=function(data){
                        return $sce.trustAsHtml(data);
                };
            $dataService.post('products-services/getGuide', $stateParams,
                function(data) {
                    
                    if (data != null) {
                        $scope.getGuide1 = data;
                    }
                },
                function(error) {
                    
                    //console.log(error)
                })
            $http.get('http://ip-api.com/json').success(function(resp){
                $scope.location=resp;
                }).error(function(){
                });
        $scope.calRadius=function(val){
            var dataRadius=[];
            angular.forEach($scope.getGuide1, function(value, key) {
                  var obj1={lat:value.location[0],long:value.location[1]};
                  var obj2={lat:$scope.location.lat,long:$scope.location.lon};
                  //console.log(findDistance(obj1,obj2));
                  if(findDistance(obj1,obj2) <= val)
                  {
                    dataRadius.push(value);
                  }
                  //else
                  // dataRadius.push(value);
                });

            $scope.getGuide1=dataRadius;
        }
        /*geo start*/
             function findDistance(objLoc1,objLoc2) {
             var Rm = 3961; // mean radius of the earth (miles) at 39 degrees from the equator
             var Rk = 6373; // mean radius of the earth (km) at 39 degrees from the equator
             var t1, n1, t2, n2, lat1, lon1, lat2, lon2, dlat, dlon, a, c, dm, dk, mi, km;
               var obj1=objLoc1;
               var obj2=objLoc2;
              t1 = obj1.lat;
              n1 = obj1.long;
              t2 = obj2.lat;
              n2 = obj2.long;
              lat1 = t1* Math.PI/180;
              lon1 = n1* Math.PI/180;
              lat2 = t2* Math.PI/180;
              lon2 = n2* Math.PI/180;
              dlat = lat2 - lat1;
              dlon = lon2 - lon1;
              a  = Math.pow(Math.sin(dlat/2),2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(dlon/2),2);
              c  = 2 * Math.atan2(Math.sqrt(a),Math.sqrt(1-a)); // great circle distance in radians
              dm = c * Rm; // great circle distance in miles
              dk = c * Rk; // great circle distance in km
              mi = Math.round( dm * 1000) / 1000;
              //km = Math.round( dk * 1000) / 1000;
                return mi;
             }
        /*geo end*/
        $scope.direcoryName = $rootScope.currentDirectory.name;
        $dataService.get('web-data?pageName=products-services-list',
            function(data) {
                //  console.log(data);
                if (data != null) {
                    $scope.sections = data;
                }
            },
            function(error) {
                console.log(error)
            })


            /*$dataService.get('products-services/postProductSubCategory?ProductSubCategoryName=' + encodeURIComponent($stateParams.ProductSubCategoryName),
               function(data) {
                   if (data != null) {
                       $scope.getProductSubCategory = data;
                   }
               },
               function(error) {
                   //console.log(error)
               })*/
            // $dataService.post('directory/state', {},
            //     function(data) {

        //         if (data != null) {
        //             $scope.StateArray = data;
        //         }
        //     },
        //     function(error) {
        //         //console.log(error)
        //     })

        $scope.val = { "KeywordSearch": '', "Zip": '' };
        $scope.radius='RADIUS';
        $scope.Reset = function() {
            $scope.radius='RADIUS';
            $scope.val = { "KeywordSearch": '', "Zip": '' };
            document.getElementById("singleSelect").selectedIndex = "0";

        }
        $scope.select = false;
        // $scope.selected = {};
        $scope.selected = {};
        $scope.getInfo = [];
        $scope.selUser = function(role) {

            $scope.selected_user = role;
            if ($scope.selected[role._id] == false) {
                $scope.selected[role._id] = false;
            } else {
                $scope.selected[role._id] = true;
            }
            // body...
        }
        $scope.updateSelection = function(ev, data, selected) {
            if ($scope.getInfo.indexOf(data) == -1) {
                $scope.getInfo.push(data)
            } else {
                $scope.getInfo.splice($scope.getInfo.indexOf(data), 1);
            }
        }

        /*   scope.$watch('selected', function(newValue, oldValue) {
             console.log(newValue);
           });*/
        $scope.createRequast = function() {
            // $localStorage.createRequast=JSON.stringify($scope.getInfo);
            var data=[];
            angular.forEach( $scope.getGuide1,function(val,key) {
                if(val.data==true)
                {
                    data.push(val);
                }
                // statements
            });
            $rootScope.getInfo =data;

            $state.go("core.create-request({navigateTo:'#createReq'})");
            $scope.getInfo={};
        };

        $scope.selectAll = function() {
            var pageInfo=$scope.dtInstance.DataTable.page.info();
            var getGuideData= angular.copy($scope.getGuide1);
            
                $("#tabledtInstance").hide();
                if($scope.select == false)
                {
                $scope.select=true;
                getGuideData=getGuideData.map(function(obj){ 
                obj.data=true;
                return obj;
                });
                // for(var i=pageInfo.start;i<pageInfo.end;i++)
                // {
                //     getGuideData.push(getGuideData[i]);
                // }
                }else{
                $scope.select=false;
                getGuideData=getGuideData.map(function(obj){ 
                obj.data=false;
                return obj;
                });

                }
                $scope.getGuide1=getGuideData;
                setTimeout(function(){$("#tabledtInstance").show();},10);
                
            }
            // <script src='assets/directives/web.scroll.selectAll.directive.js'></script>



        $scope.KeyWordSearch1 = function(val) {



                var obj = { "directoryName": $rootScope.currentDirectory.name }; // Do not delete this field
                console.log(val.KeywordSearch);
                var key = val.KeywordSearch;
                // console.log('val');
                var results = { "keywordSearch": val.KeywordSearch };

                if (val.chk == true) {
                    if ((key != undefined) && (key != '')) {
                        obj['companyName'] = { _eval: 'regex', value: key };
                        if (!obj['$or']) obj['$or'] = [];
                        obj['$or'].push({ 'categories.ProductCategoryName': { _eval: 'regex', value: key } });
                        obj['$or'].push({ 'categories.ProductSubCategoryName': { _eval: 'regex', value: key } });
                    }

                } else {
                    if ((key != undefined) && (key != '')) {

                        obj['companyName'] = { _eval: 'regex', value: key };
                    }
                    // else
                    // {
                    //      obj['$or'].push({ 'CompanyName': { _eval: 'regex', value: "" } });    
                    // }


                }

                if ((val.State != undefined) && (val.State != '')) {
                    // if(!obj['$or']) obj['$or'] = [];
                    obj['companyAddress.state'] = { _eval: 'regex', value: val.State };
                }
                if ((val.Zip != undefined) && (val.Zip != '')) {
                    console.log(val.Zip.charAt(0));

                    if (!isNaN(val.Zip.charAt(0))) {

                        obj['companyAddress.zip'] = { _eval: 'regex', value: val.Zip };

                    } else {
                        obj['companyAddress.city'] = { _eval: 'regex', value: val.Zip };

                    }



                }

                // var obj2 = {};

                // obj2['directoryId'] = "OMD";
                // obj2['categories.ProductCategoryName'] = { _eval: 'regex', value: "Au" };
                // obj2['categories.ProductSubCategoryName'] = { _eval: 'regex', value: "Au" };
                $rootScope.obj1 = obj;

                $state.go('core.productsSearch');

            }
                    $scope.KeyWordSearch = function(val) {

            $rootScope.KeyWordSearch=val;

            var obj = { "directoryName": $rootScope.currentDirectory.name };

             // Do not delete this field
            console.log(val.KeywordSearch);
            
            var key = val.ProductSubCategoryName;
                     
            // console.log('val');
            var results = { "keywordSearch": val.KeywordSearch };
      
            if (val.chk == true) {
                if(val.ProductSubCategoryName)
                {
                    obj['$or'] = [];
                    
                    obj['$or'].push({'categories.ProductCategoryName': { _eval: 'regex', value: val.ProductSubCategoryName }});
                    obj['$or'].push({'categories.ProductCategoryName': val.ProductSubCategoryName});
                    obj['$or'].push({'categories.ProductSubCategoryName': { _eval: 'regex', value: val.ProductSubCategoryName }});
                    obj['$or'].push({'categories.ProductSubCategoryName': val.ProductSubCategoryName});
                    obj['$or'].push({'companyName': { _eval: 'regex', value: val.ProductSubCategoryName }});
                    obj['$or'].push({'companyName': val.ProductSubCategoryName});
                    //obj['categories.ProductSubCategoryName']=val.ProductSubCategoryName;
                 }  

            } else {
                // if ((key != undefined) && (key != '')) {
                  
                //     obj['$or'] = [];
                //     obj['$or'].push({'categories.ProductCategoryName': { _eval: 'regex', value: key }});
                //     obj['$or'].push({'categories.ProductSubCategoryName': { _eval: 'regex', value: key }});
                //     obj['$or'].push({ 'CompanyName': { _eval: 'regex', value: key } });    
                // }
                // else
                // {
                //      obj['$or'].push({ 'CompanyName': { _eval: 'regex', value: "" } });    
                // }
            if(val.ProductSubCategoryName)
            {
                obj['$or'] = [];
                
                obj['$or'].push({'categories.ProductCategoryName': { _eval: 'regex', value: val.ProductSubCategoryName }});
                obj['$or'].push({'categories.ProductCategoryName': val.ProductSubCategoryName});
                obj['$or'].push({'categories.ProductSubCategoryName': { _eval: 'regex', value: val.ProductSubCategoryName }});
                obj['$or'].push({'categories.ProductSubCategoryName': val.ProductSubCategoryName});
                obj['$or'].push({'companyName': { _eval: 'regex', value: val.ProductSubCategoryName }});
                obj['$or'].push({'companyName': val.ProductSubCategoryName});
                //obj['categories.ProductSubCategoryName']=val.ProductSubCategoryName;
            }


            }

            if ((val.State != undefined) && (val.State != '')) {
                // if(!obj['$or']) obj['$or'] = [];
                obj['companyAddress.state'] = { _eval: 'regex', value: val.State } ; 
            }
            if ((val.Zip != undefined) && (val.Zip != '')) {
                console.log(val.Zip.charAt(0));

                if (!isNaN(val.Zip.charAt(0))) {
                  
                    obj['companyAddress.zip']= { _eval: 'regex', value: val.Zip } ;
                    
                } else {
                    obj['companyAddress.city']= { _eval: 'regex', value: val.Zip } ;
                   
                }



            }
            
            // var obj2 = {};

            // obj2['directoryId'] = "OMD";
            // obj2['categories.ProductCategoryName'] = { _eval: 'regex', value: "Au" };
            // obj2['categories.ProductSubCategoryName'] = { _eval: 'regex', value: "Au" };
            $rootScope.obj1 = obj;
            $state.go('core.productsSearch');

        }
            //   $scope.roles = Roles;

        $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('order', [1, 'asc'])
            .withPaginationType('full_numbers')
            .withOption("aoColumnDefs", [{ "targets": [0], "orderable": false }])
            .withOption("zeroRecords", "No matching records found");;

      

        $scope.postProductDetail = function(item) {

            var results = { CompanyName: item }
                // $rootScope =
                //$stateParams = {navigateTo: '#nav' };
            $state.go("core.productsDetails", results);
        }




        //State
        $scope.getAddressStates = function(val) {
            $('[ng-show="noResultsState"]').html('<i class="glyphicon glyphicon-remove"></i> No Results Found');
            if (val.length > 2) {
                return DirectoryStateService.list({
                    codeValue: {
                        _eval: 'regex',
                        value: val
                    },
                    active: true
                }).$promise;
            } else $('[ng-show="noResultsState"]').html('Enter minimum 3 characters');
        };
        $scope.initAddressStates = function() {
            $("#auto-complete-state")
                .autocomplete({
                    source: function(req, res) {
                        $scope.getAddressStates(req.term).then(function(data) {
                            res(data);
                        });
                    },
                    focus: function(event, ui) {
                        $("#auto-complete-state").val(ui.item.codeValue + " (" + ui.item.description + ")");
                        return false;
                    },
                    minLength: 3,
                    select: function(evt, ui) {
                        $scope.selectAddressStateTypeAhead(ui.item, ui.item);
                        $('#auto-complete-state').val($scope.val.State);
                        //$('#auto-complete-country').val($scope.addDatacard.country);
                        return false;
                    }
                })
                .autocomplete("instance")._renderItem = function(ul, item) {
                    return $("<li>")
                        .append("<div>" + item.codeValue + " (" + item.description + ")" + "<span>, " + (item.parentId ? item.parentId.codeValue : "") + "</span>" + "</div>")
                        .appendTo(ul);
                };
        };
        $scope.selectAddressStateTypeAhead = function($item, $model, $label, $event) {
            //$scope.val.State = $model.codeValue + " (" + $model.description + ")";
            $scope.val.State = $model.codeValue;
            //$scope.val.zip = $model.zip.codeValue;
        };

        setTimeout(function() {
            $scope.initAddressStates();
        }, 100);




    }])
