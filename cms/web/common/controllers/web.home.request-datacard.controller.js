'use strict';
angular.module('web')
	.controller('web.home.request-datacard.controller', ['$scope', '$state', '$location', '$dataService', '$sce','$rootScope','$stateParams','DirectoryCountryService', 'DirectoryStateService', 'DirectoryCityService', function($scope, $state, $location, $dataService, $sce, $rootScope,$stateParams,DirectoryCountryService, DirectoryStateService, DirectoryCityService){

    
        $scope.direcoryName = $rootScope.currentDirectory.name;


               $scope.loading = false;
            // if ($stateParams.navigateTo != '') setTimeout(function() {
            //     angular.element('html, body').stop().animate({
            //         scrollTop: ((angular.element($stateParams.navigateTo).offset().top) - 40)
            //     }, 1500, 'easeInOutExpo');
            // }, 150);
             /*if (!$stateParams.packageName) {
                $state.go('core.ocdSales', { navigateTo: '#mailing-page' });
            }
*/

		$dataService.get('web-data?pageName=request-datacard',
			function(data){
				console.log(data);
				if (data != null) {
					$scope.sections = data;
				}
			},function(error){
				console.log(error)
			})
	

		$scope.addDatacard =  {"directoryId":$rootScope.currentDirectory.directoryId,"direcoryName":$scope.direcoryName,"requestType":"Request A Datacard","title":"","organization":"","organizationtype":"","name":{"first":"","last":""},"address":"","city":"","state":"","zip":"","email":"","needs":"","hearaboutus":""};
        $scope.ClickDatacard = function() {
                        
            $dataService.post('subscriber/web-request',$scope.addDatacard,
                function(data) {
                  
                    if (data != null) {
                        $scope.addDatacard =   {"directoryId":$rootScope.currentDirectory.directoryId,"direcoryName":$scope.direcoryName,"requestType":"Request A Datacard","title":"","organization":"","organizationtype":"","name":{"first":"","last":""},"address":"","city":"","state":"","zip":"","email":"","needs":"","hearaboutus":""};

                    //     $scope.addDatacardMsg = "Thank you for your inquiry! ";
                    //     $scope.addDatacardMsg1 = "A list representative will contact you shortly via email. If you have any questions about the mailing list, please email: ";
                       
                    //     if ($scope.direcoryName == 'OCD') {
                    //         $scope.addDatacardMsg2 = "OCDmailinglist@nationalregisterpublishing.com";
                    //     }
                    //     else if ($scope.direcoryName == 'OMD'){
                    //              $scope.addDatacardMsg2 = "agnes.orlowska@officialmuseumdirectory.com";
                    //     }
                     showMessages([{
                        type: 'success',
                        message: 'Thank you for your inquiry!',
                        header: 'Data Card Submission'
                    }]);
                      $state.go('core.mailing-list');
                    //$state.go("core.home({navigateTo:'#mailingSection1'})");
                    }
                },
                function(error) {
                    $scope.addDatacard =   {"directoryId":$rootScope.currentDirectory.directoryId,"direcoryName":$scope.direcoryName,"requestType":"Request A Datacard","title":"","organization":"","organizationtype":"","name":{"first":"","last":""},"address":"","city":"","state":"","zip":"","email":"","needs":"","hearaboutus":""};
                    //  $scope.addDatacardMsg = "Data Card not submitted successfully! ";
                   console.log('addDatacard',error)
                     showMessages([{
                        type: 'error',
                        message: 'Datacard not submitted successfully! ',
                        header: 'Datacard Submission'
                    }]);
                    
                })
        };


            ////////////City

            $scope.initAddressCities = function() {
                $("#auto-complete-city")
                    .autocomplete({
                        source: function(req, res) {
                            $scope.getAddressCities(req.term).then(function(data) {
                                res(data);
                            });
                        },
                        minLength: 3,
                        focus: function(event, ui) {
                            $("#auto-complete-city").val(ui.item.codeValue);
                            return false;
                        },
                        select: function(evt, ui) {
                            $scope.selectAddressCityTypeAhead(ui.item, ui.item);
                            $('#auto-complete-city').val($scope.addDatacard.city);
                            $('#auto-complete-state').val($scope.addDatacard.state);
                            $('#auto-complete-country').val($scope.addDatacard.country);
                            return false;
                        }
                    })
                    .autocomplete("instance")._renderItem = function(ul, item) {
                        return $("<li>")
                            .append("<div>" + item.codeValue + "<p>" + (item.parentId ? (item.parentId.codeValue + " (" + item.parentId.description + ")") : "") + "<span>, " + (item.parentId && item.parentId.parentId ? item.parentId.parentId.codeValue : "") + "</span>" + "</p>" + "</div>")
                            .appendTo(ul);
                    };
            };
            $scope.getAddressCities = function(val) {
                $('[ng-show="noResultsCity"]').html('<i class="glyphicon glyphicon-remove"></i> No Results Found');
                if (val.length > 2) {
                    return DirectoryCityService.list({
                        codeValue: {
                            _eval: 'regex',
                            value: val
                        },
                        active: true
                    }).$promise;
                } else $('[ng-show="noResultsCity"]').html('Enter minimum 3 characters');
            };
            $scope.selectAddressCityTypeAhead = function($item, $model, $label, $event) {
                $scope.addDatacard.city = $model.codeValue;
                $scope.addDatacard.state = $model.parentId ? $model.parentId.codeValue + " (" + $model.parentId.description + ")" : "";
                $scope.addDatacard.country = $model.parentId && $model.parentId.parentId ? $model.parentId.parentId.codeValue : "";
            };
            $scope.initAddressStates = function() {
                $("#auto-complete-state")
                    .autocomplete({
                        source: function(req, res) {
                            $scope.getAddressStates(req.term).then(function(data) {
                                res(data);
                            });
                        },
                        focus: function(event, ui) {
                            $("#auto-complete-state").val(ui.item.codeValue + " (" + ui.item.description + ")");
                            return false;
                        },
                        minLength: 3,
                        select: function(evt, ui) {
                            $scope.selectAddressStateTypeAhead(ui.item, ui.item);
                            $('#auto-complete-state').val($scope.addDatacard.state);
                            $('#auto-complete-country').val($scope.addDatacard.country);
                            return false;
                        }
                    })
                    .autocomplete("instance")._renderItem = function(ul, item) {
                        return $("<li>")
                            .append("<div>" + item.codeValue + " (" + item.description + ")" + "<span>, " + (item.parentId ? item.parentId.codeValue : "") + "</span>" + "</div>")
                            .appendTo(ul);
                    };
            };
            $scope.getAddressStates = function(val) {
                $('[ng-show="noResultsState"]').html('<i class="glyphicon glyphicon-remove"></i> No Results Found');
                if (val.length > 2) {
                    return DirectoryStateService.list({
                        codeValue: {
                            _eval: 'regex',
                            value: val
                        },
                        active: true
                    }).$promise;
                } else $('[ng-show="noResultsState"]').html('Enter minimum 3 characters');
            };
            $scope.selectAddressStateTypeAhead = function($item, $model, $label, $event) {
                $scope.addDatacard.state = $model.codeValue + " (" + $model.description + ")";
                $scope.addDatacard.country = $model.parentId.codeValue;
            };
            $scope.initAddressCountries = function() {
                $("#auto-complete-country")
                    .autocomplete({
                        source: function(req, res) {
                            $scope.getAddressCountries(req.term).then(function(data) {
                                res(data);
                            });
                        },
                        focus: function(event, ui) {
                            $("#auto-complete-country").val(ui.item.codeValue);
                            return false;
                        },
                        minLength: 3,
                        select: function(evt, ui) {
                            $scope.selectAddressCountryTypeAhead(ui.item, ui.item);
                            $('#auto-complete-country').val($scope.addDatacard.country);
                            return false;
                        }
                    })
                    .autocomplete("instance")._renderItem = function(ul, item) {
                        return $("<li>")
                            .append("<div>" + item.codeValue + "</div>")
                            .appendTo(ul);
                    };
            };
            $scope.getAddressCountries = function(val) {
                $('[ng-show="noResultsCountry"]').html('<i class="glyphicon glyphicon-remove"></i> No Results Found');
                if (val.length > 2) {
                    return DirectoryCountryService.list({
                        codeValue: {
                            _eval: 'regex',
                            value: val
                        },
                        active: true
                    }).$promise;
                } else $('[ng-show="noResultsCountry"]').html('Enter minimum 3 characters');
            };
            $scope.selectAddressCountryTypeAhead = function($item, $model, $label, $event) {
                $scope.addDatacard.country = $model.codeValue;
            };



            setTimeout(function() {
                $scope.initAddressCities();
                $scope.initAddressStates();
                $scope.initAddressCountries();
            }, 100);


            ////////////City



	}])
