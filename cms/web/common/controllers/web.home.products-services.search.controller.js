'use strict';
angular.module('web')
    .controller('web.home.products-services.search.controller', ['$scope', '$rootScope', '$location', '$dataService', '$sce', '$stateParams', '$state', 'DTOptionsBuilder','DirectoryStateService','$http', function($scope, $rootScope, $location, $dataService, $sce, $stateParams, $state, DTOptionsBuilder,DirectoryStateService,$http) {
         $scope.direcoryName = $rootScope.currentDirectory.name;
         $scope.location={};
         $scope.radius='RADIUS';
         $scope.dtInstance={};
        //$scope.val = { "KeywordSearch": '', "Zip": '' };
        //$scope.val = $rootScope.KeyWordSearch;auto-complete-state
        $scope.getGuide1={};
        $scope.val={'chk':false};
        function showAll()
        {
            //window.location.reload();
            if($rootScope.obj1 != undefined)
            {
            // $dataService.post('products-services/getGuide', $rootScope.obj1,
            //         function(data) {
            //             if (data != null) {
            //                 if(data.length==0)
            //                     $scope.KeyWordSearch($scope.val);
            //                     else
            //                 $scope.getGuide1 = data;
            //             }
            //         },
            //         function(error) {
            //             console.log(error)
            //         })    

            
            // }
             getGuide($rootScope.obj1);
         }
        }
        showAll();

        if($rootScope.obj1 == undefined)
        {

            $scope.val = { "KeywordSearch": '', "Zip": '' };
            $scope.val=$rootScope.KeyWordSearch;
        }else
        {
        // if($rootScope.obj1.$or)
        // {

        //     $scope.val=$rootScope.KeyWordSearch;
        //     $scope.val.KeywordSearch=$rootScope.obj1.$or[3]['categories.ProductSubCategoryName'];
        //     delete $rootScope.obj1;

        // }else
        // {
        //     //$scope.val=$rootScope.obj1;
        //     $scope.val=$rootScope.KeyWordSearch;
        // }
        }
        
         $scope.bindHtml=function(data){
                        return $sce.trustAsHtml(data);
                };
        $dataService.get('web-data?pageName=products-services-list',
            function(data) {
                console.log(data);
                if (data != null) {
                    $scope.sections = data;
                }
            },
            function(error) {
                console.log(error)
            })


            $http.get('http://ip-api.com/json').success(function(resp){
                $scope.location=resp;
                }).error(function(){
                });
            

        $dataService.post('directory/state', {},
            function(data) {


                if (data != null) {
                    $scope.StateArray = data;
                }
            },
            function(error) {
                //console.log(error)
            })

        function getGuide(obj1) {
             if ((obj1 != undefined) && (obj1 != '')) {
            $dataService.post('products-services/getGuide', obj1,
                    function(data) {
                        if (data != null) {
                            $scope.getGuide1 = data;
                            $('html,body').animate({scrollTop: $('.products-search-box').offset().top-50},'fast');
                        }
                    },
                    function(error) {
                        console.log(error)
                    })
        }
          
        }
        $scope.calRadius=function(val){
            var dataRadius=[];
            angular.forEach($scope.getGuide1, function(value, key) {
                  var obj1={lat:value.location[0],long:value.location[1]};
                  var obj2={lat:$scope.location.lat,long:$scope.location.lon};
                  //console.log(findDistance(obj1,obj2));
                  if(findDistance(obj1,obj2) <= val)
                  {
                    dataRadius.push(value);
                  }
                  //else
                  // dataRadius.push(value);
                });

            $scope.getGuide1=dataRadius;
        }
        /*geo start*/

              
        function findDistance(objLoc1,objLoc2) {
             var Rm = 3961; // mean radius of the earth (miles) at 39 degrees from the equator
             var Rk = 6373; // mean radius of the earth (km) at 39 degrees from the equator
             var t1, n1, t2, n2, lat1, lon1, lat2, lon2, dlat, dlon, a, c, dm, dk, mi, km;
               var obj1=objLoc1;
               var obj2=objLoc2;
              t1 = obj1.lat;
              n1 = obj1.long;
              t2 = obj2.lat;
              n2 = obj2.long;
              lat1 = t1* Math.PI/180;
              lon1 = n1* Math.PI/180;
              lat2 = t2* Math.PI/180;
              lon2 = n2* Math.PI/180;
              dlat = lat2 - lat1;
              dlon = lon2 - lon1;
              a  = Math.pow(Math.sin(dlat/2),2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(dlon/2),2);
              c  = 2 * Math.atan2(Math.sqrt(a),Math.sqrt(1-a)); // great circle distance in radians
              dm = c * Rm; // great circle distance in miles
              dk = c * Rk; // great circle distance in km
              mi = Math.round( dm * 1000) / 1000;
                return mi;
             }
             
        /*geo end*/
        
          $scope.dtOptions = DTOptionsBuilder.newOptions()
          .withOption('order', [1, 'asc'])
          .withPaginationType('simple_numbers')
          .withOption("aoColumnDefs", [{ "targets": [0], "orderable": false }])
          .withOption("zeroRecords", "No matching records found")
          .withOption("retrieve",true);



        $scope.Reset = function() {
            $scope.radius='RADIUS';
            $scope.val = { "KeywordSearch": '', "Zip": '' };
            document.getElementById("singleSelect").selectedIndex = "0";
        }

        $scope.KeyWordSearch = function(val) {
           var obj = { "directoryName": $rootScope.currentDirectory.name }; // Do not delete this field
            console.log(val.KeywordSearch);
            var key = val.ProductSubCategoryName;
            var results = { "keywordSearch": val.KeywordSearch };

            if (val.chk == true) {
                if ((key != undefined) && (key != '')) {
                obj['$or'] = [];
                
                obj['$or'].push({'categories.ProductCategoryName': { _eval: 'regex', value: val.ProductSubCategoryName }});
                obj['$or'].push({'categories.ProductCategoryName': val.ProductSubCategoryName});
                obj['$or'].push({'categories.ProductSubCategoryName': { _eval: 'regex', value: val.ProductSubCategoryName }});
                obj['$or'].push({'categories.ProductSubCategoryName': val.ProductSubCategoryName});
                obj['$or'].push({'companyName': { _eval: 'regex', value: val.ProductSubCategoryName} });    
                obj['$or'].push({'companyName': val.ProductSubCategoryName});
                obj['$or'].push({'companyDescription':{ _eval: 'regex', value: val.ProductSubCategoryName }});

                }

            } else {
                if ((key != undefined) && (key != '')) {
                obj['$or'] = [];
                obj['$or'].push({'categories.ProductCategoryName': { _eval: 'regex', value: key }});
                obj['$or'].push({'categories.ProductCategoryName': key });
                obj['$or'].push({'categories.ProductSubCategoryName': { _eval: 'regex', value:key  }});
                obj['$or'].push({'categories.ProductSubCategoryName':key });
                obj['$or'].push({'companyName': { _eval: 'regex', value: key } });    
                obj['$or'].push({'companyName':key });
                obj['$or'].push({'companyDescription':{ _eval: 'regex', value:key  }});
                }
            }

            if ((val.State != undefined) && (val.State != '')) {
                obj['companyAddress.state'] = { _eval: 'regex', value: val.State } ; 
            }
            if ((val.Zip != undefined) && (val.Zip != '')) {
                console.log(val.Zip.charAt(0));

                if (!isNaN(val.Zip.charAt(0))) {
                  
                    obj['companyAddress.zip']= { _eval: 'regex', value: val.Zip } ;
                    
                } else {
                    obj['companyAddress.city']= { _eval: 'regex', value: val.Zip } ;
                   
                }

                $rootScope.obj1=val;

            }
            $rootScope.obj1 = obj;
            getGuide($rootScope.obj1);
        }


        
        $scope.postProductDetail = function(item) {

            var results = { CompanyName: item };
            $state.go('core.productsDetails', results);
        }
        $scope.getAddressStates = function(val) {
            $('[ng-show="noResultsState"]').html('<i class="glyphicon glyphicon-remove"></i> No Results Found');
            if (val.length > 2) {
                return DirectoryStateService.list({
                    codeValue: {
                        _eval: 'regex',
                        value: val
                    },
                    active: true
                }).$promise;
            } else $('[ng-show="noResultsState"]').html('Enter minimum 3 characters');
        };
        $scope.initAddressStates = function() {
            $("#auto-complete-state")
                .autocomplete({
                    source: function(req, res) {
                        $scope.getAddressStates(req.term).then(function(data) {
                            res(data);
                        });
                    },
                    focus: function(event, ui) {
                        return false;
                    },
                    minLength: 3,
                    select: function(evt, ui) {
                        $scope.selectAddressStateTypeAhead(ui.item, ui.item);
                        $('#auto-complete-state').val($scope.val.State);
                        return false;
                    }
                })
                .autocomplete("instance")._renderItem = function(ul, item) {
                    return $("<li>")
                        .append("<div>" + item.codeValue + " (" + item.description + ")" + "<span>, " + (item.parentId ? item.parentId.codeValue : "") + "</span>" + "</div>")
                        .appendTo(ul);
                };
        };
        $scope.selectAddressStateTypeAhead = function($item, $model, $label, $event) {
            $scope.val.State = $model.codeValue;
        };
           $scope.select = false;
        $scope.selected = {};
        $scope.getInfo = [];
        $scope.selUser = function(role) {

            $scope.selected_user = role;
            if ($scope.selected[role._id] == false) {
                $scope.selected[role._id] = false;
            } else {
                $scope.selected[role._id] = true;
            }
        }
        $scope.updateSelection = function(ev, data, selected) {

            if ($scope.getInfo.indexOf(data) < 0) {
                $scope.getInfo.push(data)
            } else {
                $scope.getInfo.splice($scope.getInfo.indexOf(data), 1);
            }

            
        }

       $scope.createRequast = function() {
            var data=[];
            angular.forEach( $scope.getGuide1,function(val,key) {
                if(val.data==true)
                {
                    data.push(val);
                }
            });
            $rootScope.getInfo =data;

            $state.go("core.create-request({navigateTo:'#createReq'})");
            $scope.getInfo={};
        };

        $scope.selectAll = function() {
            var pageInfo=$scope.dtInstance.DataTable.page.info();
            var getGuideData= angular.copy($scope.getGuide1);
            
                $("#tabledtInstance").hide();
                if($scope.select == false)
                {
                $scope.select=true;
                getGuideData=getGuideData.map(function(obj){ 
                obj.data=true;
                return obj;
                });
                }else{
                $scope.select=false;
                getGuideData=getGuideData.map(function(obj){ 
                obj.data=false;
                return obj;
                });

                }
                $scope.getGuide1=getGuideData;
                setTimeout(function(){$("#tabledtInstance").show();},10);
                
            }

        setTimeout(function() {
            $scope.initAddressStates();
        }, 100);


    }])
