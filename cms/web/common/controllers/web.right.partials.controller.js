// 'use strict';
// angular.module('web')
//     .controller('web.right.partials.controller', ['$scope', '$location', '$dataService', '$sce', '$http', 'FeedService', '$rootScope', '$state', function($scope, $location, $dataService, $sce, $http, FeedService, $rootScope, $state) {
//             setTimeout(function() {
//             $('.bxslider').bxSlider({
//             default: false,
//             auto: true,
//             autoControls: true,
//             mode: 'horizontal',
//             useCSS: true,
//             infiniteLoop: true,
//             hideControlOnEnd: true,
//             easing: 'easeOutElastic',
//             speed: 1500,
//         });
//         $(".bx-controls-auto").hide();
//         $(".bx-default-pager").hide();
//         $(".bx-prev").hide();
//         $(".bx-next").hide();

//             }, 300);
//         $scope.direcoryName = $rootScope.currentDirectory.name;
//         $scope.stload = function() {
//             //$state.reload($state.current.name);
//             window.location = document.URL.slice(0, document.URL.indexOf($scope.direcoryName)) + $scope.direcoryName + '/news';
//         };

//         $dataService.get('web-data?pageName=museum-news',
//             function(data) {
//                 if (data != null) {
//                     $scope.sections = data
//                 }
//             },
//             function(error) {
//                 console.log(error)
//             })

//         if ($scope.direcoryName == 'OCD') {
//             $scope.feedSrc = 'https://cnsblog.wordpress.com/feed/';
//         } else if ($scope.direcoryName == 'OMD') {
//             $scope.feedSrc = 'http://www.museumsassociation.org/rss/news';
//         }

//         FeedService.parseFeed($scope.feedSrc).then(function(res) {
//             var data = res.data.responseData.feed.entries;

//             if ($scope.direcoryName == 'OCD') {
//                 for (var i = 0; i < data.length; i++) {
//                     var publishDate = new Date(data[i].publishedDate);
//                     data[i].publishDate = publishDate;
//                 }
//             }
//             $scope.feeds = data;
//         });


//     }])

'use strict';
angular.module('web')
    .controller('web.right.partials.controller', ['$scope', '$location', '$dataService', '$sce', '$http', 'FeedService', '$rootScope', '$state', function($scope, $location, $dataService, $sce, $http, FeedService, $rootScope, $state) {
        $('.bxslider').bxSlider({
            default: false,
            auto: true,
            autoControls: true,
            mode: 'horizontal',
            useCSS: true,
            infiniteLoop: true,
            hideControlOnEnd: true,
            easing: 'easeOutElastic',
            speed: 1500,
        });
        $(".bx-controls-auto").hide();
        $(".bx-default-pager").hide();
        $(".bx-prev").hide();
        $(".bx-next").hide();
        $scope.direcoryName = $rootScope.currentDirectory.name;
        $scope.stload = function() {
            //$state.reload($state.current.name);
            window.location = document.URL.slice(0, document.URL.indexOf($scope.direcoryName)) + $scope.direcoryName + '/news';
        };

        $dataService.get('web-data?pageName=museum-news',
            function(data) {
                console.log(data);
                if (data != null) {
                    $scope.sections = data;
                }
            },
            function(error) {
                console.log(error)
            })

        if ($scope.direcoryName == 'OCD') {
            $scope.feedSrc = 'https://cnsblog.wordpress.com/feed/';
        } else if ($scope.direcoryName == 'OMD') {
            $scope.feedSrc = 'http://www.museumsassociation.org/rss/news';
        }

        FeedService.parseFeed($scope.feedSrc).then(function(res) {
            var data = res.data.responseData.feed.entries;

            if ($scope.direcoryName == 'OCD') {
                for (var i = 0; i < data.length; i++) {
                    var publishDate = new Date(data[i].publishedDate);
                    data[i].publishDate = publishDate;
                }
            }
            $scope.feeds = data;
        });


    }])
