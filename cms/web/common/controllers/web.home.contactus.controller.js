'use strict';
angular.module('web')
    .controller('web.home.contactus.controller', ['$scope', '$state', '$location', '$dataService', '$sce', '$rootScope', function($scope, $state, $location, $dataService, $sce, $rootScope) {
        $scope.direcoryName = $rootScope.currentDirectory.name;
        $dataService.get('web-data?pageName=contact-us',
            function(data) {
                console.log(data);
                if (data != null) {
                    $scope.sections = data;
                }
            },
            function(error) {
                console.log(error)
            })
        $scope.addComment = { "directoryId": $rootScope.currentDirectory.directoryId, "direcoryName": $scope.direcoryName, "requestType": "Comment", "phone": "", "email": "", "name": { "first": "" }, "details": "", "reasons": "" };
        $scope.ClickComment = function(formData) {
            $scope.CommentForm = formData;
            $dataService.post('subscriber/web-request', $scope.addComment,
                function(data) {

                    if (data != null) {
                        $scope.addComment = { "directoryId": $rootScope.currentDirectory.directoryId, "direcoryName": $scope.direcoryName, "requestType": "Comment", "phone": "", "email": "", "name": { "first": "" }, "details": "", "reasons": "" };
                        showMessages([{
                            type: 'success',
                            message: 'Comments submitted successfully!',
                            header: 'Comments Submission'
                        }]);
                        $state.go('core.contact-us' /*, { navigateTo: '#contact' }*/ )

                    }
                },
                function(error) {
                    $scope.addComment = { "directoryId": $rootScope.currentDirectory.directoryId, "direcoryName": $scope.direcoryName, "requestType": "Comment", "phone": "", "email": "", "name": { "first": "" }, "details": "", "reasons": "" };
                    // $scope.addCommentMsg = "Comments not submitted successfully! ";
                    showMessages([{
                        type: 'error',
                        message: 'Comments not submitted successfully! ',
                        header: 'Comments Submission'
                    }]);
                })
        };


    }])
