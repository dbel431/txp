'use strict';
angular.module('web')
    .controller('DirectoryWebUserSearchControllerDMMP', ['dataCommonService', '$rootScope', '$scope', '$localStorage', '$location', '$dataService', '$sce', '$state', '$stateParams', '$compile', '$resource', '$uibModal', 'DTOptionsBuilder', 'DTColumnBuilder', 'DirectoryOrganizationService', 'DirectoryInstitutionCategory', 'DirectoryCollectionCategory', 'DirectoryWebUserService', 'DirectoryCityService', 'DirectoryStateService', 'DirectorySpecificationType', 'DirectorySpecificationCodeForIndustryPreference', 'DirectoryOfficerType', 'DirectorySpecificationCodeForGeographicalPreference', 'DirectorySpecificationCodeForPortfolioRelationship', 'DirectorySpecificationCodeForFinancingPreferred', 'DirectorySpecificationCodeForDealType', 'DirectorySpecificationCodeForFirmWillTakeActiveRoleAs', 'ListingType', 'DisbursalType',
    function(dataCommonService,$rootScope, $scope, $localStorage, $location, $dataService, $sce, $state, $stateParams, $compile, $resource, $uibModal, DTOptionsBuilder, DTColumnBuilder, DirectoryOrganizationService, DirectoryInstitutionCategory, DirectoryCollectionCategory, DirectoryWebUserService, DirectoryCityService, DirectoryStateService, DirectorySpecificationType, DirectorySpecificationCodeForIndustryPreference, DirectoryOfficerType, DirectorySpecificationCodeForGeographicalPreference, DirectorySpecificationCodeForPortfolioRelationship, DirectorySpecificationCodeForFinancingPreferred, DirectorySpecificationCodeForDealType, DirectorySpecificationCodeForFirmWillTakeActiveRoleAs, ListingType, DisbursalType) {
            // if (!$localStorage['NRP-USER-TOKEN'])
            // {
            //     localStorage.refineSearch='';
            //     $state.go('core.login');
            // }
            var vm = this;
            /*ASD@123 holders start*/
            $scope.InstitutionCategoryArrayDataSettings={enableSearch: true, scrollableHeight: '250px', scrollable: true, scrollableWidth:'100%', externalIdProp: ''};
            $scope.keywordDropDownSettings={scrollableHeight: 'auto', scrollable: true, scrollableWidth:'100%', externalIdProp: ''};
            $('#searchData').hide();
            $scope.disbursalTypeArray = DisbursalType.map(function(data){return {id:data.codeValue,label:data.codeValue};});
            $scope.listingTypeArray = ListingType.map(function(data){return {id:data.codeValue,label:data.codeValue};});
            function init()
            {
            var arr=[{'type':'scope','name':'firstName','value':''}, {'type':'scope','name':'lastName','value':''}, {'type':'scope','name':'name','value':''}, {'type':'scope','name':'instituteNameAllFlag','value':false}, {'type':'scope','name':'cityName','value':''}, {'type':'scope','name':'establishedYear','value':''}, {'type':'scope','name':'vendorsName','value':''}, {'type':'scope','name':'conductBusiness','value':''}, {'type':'scope','name':'conductBusinessbFlag','value':false}, {'type':'scope','name':'catalogOnline','value':false}, {'type':'scope','name':'onlineSales','value':''}, {'type':'scope','name':'zip','value':''}, {'type':'scope','name':'employees','value':''}, {'type':'scope','name':'listingType1','value':''}, {'type':'scope','name':'listingType','value':[]}, {'type':'scope','name':'disbursalType1','value':''}, {'type':'scope','name':'disbursalType','value':[]}, {'type':'scope','name':'t1','value':''}, {'type':'scope','name':'t2','value':''}, {'type':'scope','name':'galleryDescription','value':''}, {'type':'scope','name':'galleryDescriptionFlag','value':''}, {'type':'scope','name':'stateDatadata','value':[]}, {'type':'vm','name':'totalAdvertisingBudgeMethod','value':'select'}, {'type':'vm','name':'totalAdvertisingBudget1','value':''}, {'type':'vm','name':'totalAdvertisingBudget2','value':''}];
            for(var i=0;i<arr.length;i++) {if(arr[i].type=='scope') {var key=arr[i].name; $scope[key]=arr[i].value; } if(arr[i].type=='vm') {var key=arr[i].name; vm[key]=arr[i].value; }
            }
            }
            //table start
            var xtempname={
                directorCEO:
                DTColumnBuilder.newColumn('name','flags')                
                .renderWith(function(name,type, row) {
                           
                    if($scope.tabHref==2)
                    {
                        console.log("$scope.lastName",$scope.lastName);
                        console.log("$scope.firstName",$scope.firstName);
                        if(($scope.lastName && $scope.firstName==undefined) || ($scope.lastName && $scope.firstName=="")){
                        var getPer=row.personnel.filter(function(val){
                            if((val.name.last.toUpperCase() == $scope.lastName.toUpperCase() )|| val.name.last.toUpperCase().indexOf($scope.lastName.toUpperCase()) != -1 )
                            {
                                return val;
                            }
                        }); 
                        row.personnel= getPer;   
                        }
                        if(($scope.firstName && $scope.lastName==undefined )|| ($scope.firstName && $scope.lastName=="" )){
                        var getPer=row.personnel.filter(function(val){
                            if((val.name.first.toUpperCase() == $scope.firstName.toUpperCase() )|| val.name.first.toUpperCase().indexOf($scope.firstName.toUpperCase()) != -1 )
                            {
                                return val;
                            }
                        }); 
                        row.personnel= getPer;   
                        }
                        if($scope.firstName && $scope.lastName){
                        var getPer=row.personnel.filter(function(val){
                            if(((val.name.first.toUpperCase() == $scope.firstName.toUpperCase() )|| (val.name.first.toUpperCase().indexOf($scope.firstName.toUpperCase()) != -1))&& ((val.name.last.toUpperCase() == $scope.lastName.toUpperCase() )|| (val.name.last.toUpperCase().indexOf($scope.lastName.toUpperCase()) != -1 )) )
                            {
                                return val;
                            }
                        }); 
                        row.personnel= getPer;   
                        }
                    }

                    var str='';
                    if(row.personnel.length > 0)
                        str=row.personnel[0].name.last+', '+row.personnel[0].name.first;
                    return str;
                })
                .withOption('width', '15%'),
                name:
                DTColumnBuilder.newColumn('name','flags')
                .withTitle('Institution Name')
                .renderWith(function(name,type, row) {
                    if(name.split(' ')[0].toUpperCase()=='THE')
                        name=name.replace('The','')+',The';
                    if(row.flags)
                    {
                    if(row.flags.aam)
                        name=name+'        <img src="/assets/aam-icon.png" width=15 height=15>';
                    if(row.flags.icom)
                        name=name+'        <img src="/assets/icom-icon.png" width=20 height=10> ';
                    if(row.flags.accredited)
                        name=name+'        <img src="/assets/star_icon.png" width=15 height=15>';
                     if(row.flags.handicapped)
                         name=name+'      <img src="/assets/Handicapped-Accessible.png" width=15 height=15>';
                     }

                    return name||"";
                })
                .withOption('width', '15%'),
                personnelTitle:
                DTColumnBuilder.newColumn('personnel','flags')
                .withTitle('personnelTitle')
                .renderWith(function(name,type, row) {
                    return 'personnelTitle456';
                })
                .withOption('width', '20%'),
                personnel:DTColumnBuilder.newColumn('personnel.titleMasterName')
                .withTitle('Title')
                .renderWith(function(personnel, type, row) {
                    if($scope.tabHref==2)
                    {
                        console.log("$scope.lastName",$scope.lastName);
                        console.log("$scope.firstName",$scope.firstName);
                        if(($scope.lastName && $scope.firstName==undefined) || ($scope.lastName && $scope.firstName=="")){
                        var getPer=row.personnel.filter(function(val){
                            if((val.name.last.toUpperCase() == $scope.lastName.toUpperCase() )|| val.name.last.toUpperCase().indexOf($scope.lastName.toUpperCase()) != -1 )
                            {
                                return val;
                            }
                        }); 
                        row.personnel= getPer;   
                        }
                        if(($scope.firstName && $scope.lastName==undefined )|| ($scope.firstName && $scope.lastName=="" )){
                        var getPer=row.personnel.filter(function(val){
                            if((val.name.first.toUpperCase() == $scope.firstName.toUpperCase() )|| val.name.first.toUpperCase().indexOf($scope.firstName.toUpperCase()) != -1 )
                            {
                                return val;
                            }
                        }); 
                        row.personnel= getPer;   
                        }
                        if($scope.firstName && $scope.lastName){
                        var getPer=row.personnel.filter(function(val){
                            if(
                                (
                                (val.name.first.toUpperCase() == $scope.firstName.toUpperCase() )|| 
                                (val.name.first.toUpperCase().indexOf($scope.firstName.toUpperCase()
                                ) != -1))&& ((val.name.last.toUpperCase() == $scope.lastName.toUpperCase() )|| (val.name.last.toUpperCase().indexOf($scope.lastName.toUpperCase()) != -1 )) )
                            {
                                
                                return val;
                            }
                        }); 
                        row.personnel= getPer;   
                        }
                    }
                    var str='';
                    if(row.personnel.length > 0 && row.personnel[0].title)
                        return row.personnel[0].title;
                    else
                        return '';
                }).withOption('width', '8%'),
                cityName:DTColumnBuilder.newColumn('address.cityName')
                .withTitle('City')
                .renderWith(function(city, type, row) {
                   var zzz=row.address.filter(function(v){if(v.addressType=='576286d0c19cef300bc720e6'){
                            return 1;
                   }});
//                    return ((row.address && zzz.address[0] && zzz.address[0].cityName) ? (zzz.address[0].cityName || "") : "") || "";
                    return ((row.address && row.address[0] && row.address[0].cityName + '45') ? (row.address[0].cityName  || "") : "") || "";
                }).withOption('width', '10%'),
                statedescription:DTColumnBuilder.newColumn('address.state.description')
                    .withTitle('State/Territory')
                .renderWith(function(data, type, full){
                    var zzz=full.address.filter(function(v){if(v.addressType=='576286d0c19cef300bc720e6'){
                            return 1;
                   }});
                    if(zzz.length > 1)
                    return ((full.address && full.address[0] && full.address[0].state.description) ? (full.address[0].state.description|| "") : "") || "";
                    else
                    return ((full.address && full.address[0] && full.address[0].state &&full.address[0].state.description) ? (full.address[0].state.description|| "") : "") || "";

                })
                .withOption('width', '2%'),
                 zip:DTColumnBuilder.newColumn('address.zip')
                .withTitle('zip')
                .renderWith(function(data, type, full){
                    return full.address[0].zip?full.address[0].zip:'';
                })
                .withOption('width', '8%'),
                actions:DTColumnBuilder.newColumn('_id')
                .withTitle('Actions')
                .notSortable()
                .withClass('text-center')
                .renderWith(function(id,type, full) {
                    return '<div ng-click=vm.advanceSearchResetFunctionDMMP()><directory-web-user-record-actions record-id="\'' + id + '\'"></directory-web-user-record-actions></div>';
                })
                .withOption('width', '4%'),
                personnelName:DTColumnBuilder.newColumn('personnel.titleMasterName')
                .withTitle('Pesonnel')
                .renderWith(function(personnel, type, row) {
                    console.log('Pesonnel',row);
                    if (row.personnel) {
                        for (var i = 0; i < row.personnel.length; i++) {
                            var pers = row.personnel[i];
                            if (($scope.firstName !== undefined) && ($scope.firstName !== '')){

                                var var1 = new RegExp($scope.firstName,"i");
                                if (var1.test(pers.name.first)) {
                                    if(pers.name.prefix)
                                        var director = pers.name ? '<p> ' +pers.name.prefix+" "+ [pers.name.first,pers.name.middle, pers.name.last].join(' ') +'</p>' : '';
                                    if(pers.name.suffix)
                                        var director = pers.name ? '<p> '+ [pers.name.first,pers.name.middle, pers.name.last].join(' ') + " "+pers.name.suffix + '</p>' : '';
                                    if(pers.name.suffix && pers.name.prefix )
                                     var director = pers.name ? '<p> ' +pers.name.prefix+" "+ [pers.name.first,pers.name.middle, pers.name.last].join(' ') + " "+pers.name.suffix +'</p>' : '';
                                    if(pers.name.first && pers.name.last)
                                    {
                                        return pers.name.first+" "+pers.name.last;
                                    }
                                    return director || "";
                                }
                            }
                            else if (($scope.lastName !== undefined) && ($scope.lastName !== '')){

                                var var1 = new RegExp($scope.lastName,"i");
                                if (var1.test(pers.name.last)) {

                                    var director = pers.name ? '<p>' + [pers.name.first,pers.name.middle, pers.name.last].join(' ') + '</p>' : '';
                                    return director || "";
                                }
                            }
                            else{
                                 var director = row.personnel[0].name ? '<p>' + [row.personnel[0].name.first,row.personnel[0].name.middle, row.personnel[0].name.last].join(' ') + '</p>' : '';
                                    return director || "";
                            }
                        }
                
                    }
                    return '';
                }).withOption('width', '9%')
                };
            //table end
            vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
            /*function define start*/
            vm.newSearchAAD=newSearchAAD;
            vm.refinesearchAAD=refinesearchAAD;
            vm.gotoDiv=gotoDiv;
            vm.pSearch=pSearch;
            vm.advanceSearchResetFunctionDMMP=advanceSearchResetFunctionDMMP;
            vm.keywordSearch=keywordSearch;
            vm.institutionalSearch=institutionalSearch;
            vm.init=init;
            /*function define end*/
            if(localStorage.state == undefined)
            {
                console.log("localStorage.state_245");
                init();  
                $('#searchData').hide();
                if(localStorage.refineSearch !== "" && localStorage.refineSearch !== undefined)
                {
                    var temp=JSON.parse(localStorage.refineSearch);
                    if(temp == undefined)
                    {
                  //  $("#searchData").show();
                    $scope.tabHref = 1;
                    vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                    init();    
                    }
                    else
                    {
                         vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                         $scope.tabHref =temp.tabHref.value;
                    }
                }else
                {
                    $scope.tabHref = 1;
                    vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                    init();  
                }                
            }
            if(localStorage.state=='new')
            {
                vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                console.log("localStorage.state_245");
                delete localStorage.state;
                init();  
                $('#searchData').show();
                if(localStorage.refineSearch !== "" && localStorage.refineSearch !== undefined)
                {
                    var temp=JSON.parse(localStorage.refineSearch);
                    delete localStorage.refineSearch;
                    if(temp == undefined)
                    {
                  //  $("#searchData").show();
                    $scope.tabHref = 1;
                    vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                    init();    
                    }
                    else
                    {
                         vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                         $scope.tabHref =temp.tabHref.value;
                    }
                }else
                {
                    $scope.tabHref = 1;
                    vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                    init();  
                }

            }
            if(localStorage.state=='ref')
            {
                delete localStorage.state;
                console.log("localStorage.state_new_278",localStorage.state);
                init();  
                $('#searchData').show();
                if(localStorage.refineSearch !== "" && localStorage.refineSearch !== undefined)
                {
                    var temp=JSON.parse(localStorage.refineSearch);
                    delete localStorage.refineSearch;
                    if(temp == undefined)
                    {
                    $scope.tabHref = 1;
                    vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                   
                    }
                    else
                    {
                         $scope.tabHref =temp.tabHref.value;
                         if(temp.tabHref.value==2)
                         {
                            vm.dtC = [xtempname.directorCEO.withTitle('Name'),xtempname.personnel,xtempname.name,xtempname.cityName, xtempname.statedescription, xtempname.actions ];
                         }
                         if(temp.tabHref.value==1)
                         {
                         vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];   
                         }if(temp.tabHref.value==3)
                         {
                         vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];   
                         }
                         tempToScope(temp);
                         setTimeout(function(){
                            searchFunction($scope.tabHref);
                        },100);
                         
                    }
                }else
                {
                    $scope.tabHref = 1;
                    vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                    init();  
                }
            }
            function institutionalSearch()
            {
                
                setTimeout(function(){
                    gotoDiv('#gotoSearch');    
                },100);
                console.log("institutionalSearch");
                searchFunction(1);
                
            }
            function keywordSearch()
            {
                 console.log("keywordSearch");
                    var qr={
                    "eFlag":true,
                    "org":{"directoryId":$rootScope.currentDirectory.directoryId},
                    "exhi":{"$or":[getQr()]}};
                    keyword(qr);
                    gotoDiv('#gotoSearch');
            }
            function tempToScope(temp)
            {
                //var arr=[{'type':'scope','name':'firstName','value':''}, {'type':'scope','name':'lastName','value':''}, {'type':'scope','name':'name','value':''}, {'type':'scope','name':'instituteNameAllFlag','value':false}, {'type':'scope','name':'cityName','value':''}, {'type':'scope','name':'establishedYear','value':''}, {'type':'scope','name':'vendorsName','value':''}, {'type':'scope','name':'conductBusiness','value':''}, {'type':'scope','name':'conductBusinessbFlag','value':false}, {'type':'scope','name':'catalogOnline','value':false}, {'type':'scope','name':'onlineSales','value':''}, {'type':'scope','name':'zip','value':''}, {'type':'scope','name':'employees','value':''}, {'type':'scope','name':'listingType1','value':''}, {'type':'scope','name':'listingType','value':[]}, {'type':'scope','name':'disbursalType1','value':''}, {'type':'scope','name':'disbursalType','value':[]}, {'type':'scope','name':'t1','value':''}, {'type':'scope','name':'t2','value':''}, {'type':'scope','name':'galleryDescription','value':''}, {'type':'scope','name':'galleryDescriptionFlag','value':''}, {'type':'scope','name':'stateDatadata','value':[]}, {'type':'vm','name':'totalAdvertisingBudgeMethod','value':'select'}, {'type':'vm','name':'totalAdvertisingBudget1','value':''}, {'type':'vm','name':'totalAdvertisingBudget2','value':''}];
                var arr=Object.keys(temp);
                for(var i=0;i<arr.length;i++)
                {
                    var key=arr[i],
                    name=temp[key].name, 
                    type=temp[key].type,
                    value=temp[key].value;
                    if(type=="scope" && value!== " " && value!== undefined)
                    {
                        $scope[name]=value;
                    }
                    if(type=="vm" && value!== " " && value!== undefined)
                    {
                        vm[name]=value;
                    }
                }
            }
            function advanceSearchResetFunctionDMMP()
            {
                console.log("advanceSearchResetFunctionDMMP");
                $scope.cityName=$('#CityAuto').val();
                var arr=[{'type':'scope','name':'firstName','value':''}, {'type':'scope','name':'lastName','value':''}, {'type':'scope','name':'name','value':''}, {'type':'scope','name':'instituteNameAllFlag','value':false}, {'type':'scope','name':'cityName','value':''}, {'type':'scope','name':'establishedYear','value':''}, {'type':'scope','name':'vendorsName','value':''}, {'type':'scope','name':'conductBusiness','value':''}, {'type':'scope','name':'conductBusinessbFlag','value':false}, {'type':'scope','name':'catalogOnline','value':false}, {'type':'scope','name':'onlineSales','value':''}, {'type':'scope','name':'zip','value':''}, {'type':'scope','name':'employees','value':''}, {'type':'scope','name':'listingType1','value':''}, {'type':'scope','name':'listingType','value':[]}, {'type':'scope','name':'disbursalType1','value':''}, {'type':'scope','name':'disbursalType','value':[]}, {'type':'scope','name':'t1','value':''}, {'type':'scope','name':'t2','value':''}, {'type':'scope','name':'galleryDescription','value':''}, {'type':'scope','name':'galleryDescriptionFlag','value':''}, {'type':'scope','name':'stateDatadata','value':[]}, {'type':'vm','name':'totalAdvertisingBudgeMethod','value':'select'}, {'type':'vm','name':'totalAdvertisingBudget1','value':''}, {'type':'vm','name':'totalAdvertisingBudget2','value':''},{'type':'scope','name':'tabHref','value':$scope.tabHref}];
                var temp={};
                for(var i=0;i<arr.length;i++)                
                {
                    if(arr[i].type==='scope')
                    {
                        var type=arr[i].type,name=arr[i].name,val=$scope[name];
                        temp[name]={"type":type,"name":name,"value":val};
                    }
                    if(arr[i].type==='vm')
                    {
                        var type=arr[i].type,name=arr[i].name,val=vm[name];
                        temp[name]={"type":type,"name":name,"value":val};
                    }

                }
                localStorage.refineSearch=JSON.stringify(temp);
            }
            function pSearch()
            {
                console.log("pSearch");
                var qr={org:{"directoryId":$rootScope.currentDirectory.directoryId},
                    per:getQr()
                };
                persone(qr);
                gotoDiv('#gotoSearch');
            }
            function gotoDiv(name)
            {
                 console.log("gotoDiv",name);
                 $('#searchData').show();
                 $('html,body').animate({scrollTop: $(name).offset().top-50},'fast');
            }
            function newSearchAAD()
            {
                console.log("newSearchAAD");
                init();
                vm.gotoDiv('#containerBox');
            }
            function refinesearchAAD()
            {
                console.log("refinesearchAAD");
                vm.gotoDiv('#containerBox');
            }
            function org(qr)
            {
                 console.log("org");
                    var promise = $resource(APIBasePath + 'subscriber/search/org-list', {}, {
                        dataTable: {
                            url: APIBasePath + 'subscriber/search/org-list',
                            method: 'POST',
                            transformRequest: function(data, headerGetter) {
                                var headers = headerGetter();
                                var newData = {
                                    dataTableQuery: data,
                                    conditions: qr
                                    }
                                return JSON.stringify(newData);
                                }
                            }
                        });
                        initDatatable(promise.dataTable);
                        return promise;
            }
            function persone(qr)
            {
                qr.per['personnel.deleted']=false;
                    console.log("persone");
                var promise = $resource(APIBasePath + 'subscriber/search/personnel/list', {}, {
                        dataTable: {
                            url: APIBasePath + 'subscriber/search/personnel/list',
                            method: 'POST',
                            transformRequest: function(data, headerGetter) {
                                var headers = headerGetter();
                                var newData = {
                                    dataTableQuery: data,
                                    conditions: qr,
                                    persone:1
                                    }
                                return JSON.stringify(newData);
                                }
                            }
                        });
                        initDatatable(promise.dataTable);
                        return promise;
            }
            function keyword(qr)
            {
                   console.log("keyword");
                var promise = $resource(APIBasePath + 'subscriber/search/exhibition/list', {}, {
                        dataTable: {
                            url: APIBasePath + 'subscriber/search/exhibition/list',
                            method: 'POST',
                            transformRequest: function(data, headerGetter) {
                                var headers = headerGetter();
                                var newData = {
                                    dataTableQuery: data,
                                    conditions: qr,
                                    persone:1
                                    }
                                return JSON.stringify(newData);
                                }
                            }
                        });
                        initDatatable(promise.dataTable);
                        return promise;
            }
            /*ASD@123 holders end*/
            /*string to number function start*/
            function strtonumber(data)
            {
                console.log("strtonumber");
                var temp=data;
                for(var i=0;i<data.split(',').length;i++)
                {
                    temp=temp.replace(',','');
                }
                return temp;
            }
            /*string to number function end */
            /*ASD@123 watch*/
            $scope.$watch('cityName', function(newValue, oldValue) {
                var z1={
                    _where: {codeTypeId:'57174a5d6778ef1c153aec6f',codeValue: {_eval: 'regex',value: newValue},parentId:{'$ne':null},active: true},
                     _limit: 50
                };
                if(newValue!=="" && newValue!= undefined)
                $dataService.post('directory/city',z1,function(data){
                    var city=data.map(function(val){
                        return val.codeValue;
                    });
                    var city2=[];
                    for(var i=0;i<city.length;i++)
                    {
                            if(city2.indexOf(city[i])== -1)
                            {
                             city2.push(city[i]);
                            }
                    }
                    $scope.cityData=city2
                            .map(function(val){
                                return {id:val,label:val};                      
                                });

                              $( "#CityAuto" ).autocomplete({
                                  source:$scope.cityData,
                                   select: function( event, ui ) { 
                                    $("#CityAuto").val(ui.item.id);
                                    return false;
                                   }
                                });
                              
                            },function(error){
                        });
            
            });
            function stateGetData(newValue)
            {
                var arr=[];
                    arr.push({codeValue:{_eval: 'regex',value: newValue}});
                    arr.push({description:{_eval: 'regex',value: newValue}});

                // var z1={
                //     _where: {codeTypeId:'57174a4b6778ef1c153aec6e','$or':arr,active: true},
                //     _limit: 5000
                // };
                var z1={
                    _where: {"parentId":"5718d7abca75ca39db934831"},
                    _limit: 5000
                };
                 //{ "parentId": ObjectId("5718d7abca75ca39db934831") }
                if(newValue!=="" && newValue!= undefined)
                $dataService.post('directory/state',z1,function(data){
             //       $scope.dataCommonService.data=0;
                   // console.log(data);
                            $scope.stateData=data.map(function(data){
                                 return {id:data.codeValue,label:data.description?data.codeValue+" ("+data.description+")":data.codeValue};
                                    //return data.description?data.codeValue+" ("+data.description+")":data.codeValue;
                                });
                                $( "#StateAuto" ).autocomplete({
                                  source:$scope.stateData
                                });
                                //
                                     $( "#StateAuto" ).autocomplete({
                                  source:$scope.stateData,
                                   select: function( event, ui ) { 
                                 //   console.log('ui',ui);
                                    $("#StateAuto").val(ui.item.id);
                                    return false;
                                   }
                                });
                                //
                            },function(error){
                    //    console.log(error);
                        });

            }
            stateGetData('a');

                $scope.$watch('stateName1', function(newValue, oldValue) {
                 stateGetData(newValue);
 

            });
         /*ASD@123 watch*/
            vm.dtI = null;
            vm.dataOfData={};
            vm.dataOfAnd={};
   
         //   $("#searchData").hide();
            //ASD@123 search
            $scope.searchFunction =searchFunction;
            function searchFunction(data)
            {
                console.log("searchFunction");
                //$("#searchData").show();
                console.log('data',data);
                if(data==1)  {
                var qr=getQr();
                qr['directoryId']=$rootScope.currentDirectory.directoryId;
                org(qr);    
                }
                if(data==2)
                {
                    var qr={org:{"directoryId":$rootScope.currentDirectory.directoryId},
                    per:getQr()
                };
                persone(qr);
                }
                if(data==3)
                {
                    var qr={
                    "eFlag":true,
                    "org":{"directoryId":$rootScope.currentDirectory.directoryId},
                    "exhi":{"$or":[getQr()]}};
                    keyword(qr);
                }
            } 

            function getQr(){
            //    alert("hii");
            console.log('getQr');
                var qrObj={};
                    if (($scope.firstName !== undefined) && ($scope.firstName !== ''))
                    {
                        qrObj['personnel.name.first']={ _eval: 'regex', value: $scope.firstName };
                    }
                    if (($scope.lastName !== undefined) && ($scope.lastName !== ''))
                    {
                        qrObj['personnel.name.last']={ _eval: 'regex', value: $scope.lastName };
                    }

        if ($scope.instituteNameAllFlag == true)
        {
            qrObj['name']={ _eval: 'regex', value: $scope.name };
        }
        if($scope.instituteNameAllFlag == false)
        {
            if($scope.name !== '' && $scope.name!== undefined)
            {
            var obj=[];
            var arrname=$scope.name.split(' ');
            obj.push({ _eval: 'regex', value:$scope.name})
            for(var i=0;i<arrname.length;i++)
            {
                if(arrname[i]!=='')
                {
                    var sname=arrname[i];
                    obj.push({ _eval: 'regex', value:sname})
                }
            }
            qrObj['name']={'$in':obj};
        }
        }
        if($('#CityAuto').val()!=='' && $('#CityAuto').val()!==undefined)
        {
            qrObj['address.cityName']=$('#CityAuto').val();
        }
        if ($scope.stateDatadata.length> 0) {
            qrObj['address.stateName']={'$in':[]};
            for (var i = $scope.stateDatadata.length - 1; i >= 0; i--) {
                qrObj['address.stateName']['$in'].push($scope.stateDatadata[i].id);
            }
        }
        if (($scope.zip !== undefined) && ($scope.zip !== ''))
        {
            qrObj['address.zip']={_eval: 'regex', value: $scope.zip };
        }
        if (($scope.galleryDescription !== undefined) && ($scope.galleryDescription !== ''))
        {
            if($scope.galleryDescriptionFlag==true)
                qrObj['galleryDescription']=$scope.galleryDescription;
            else
                qrObj['galleryDescription']={ _eval: 'regex', value: $scope.galleryDescription }
        }
        if(vm.totalAdvertisingBudgeMethod=='A'){
            qrObj['totalAdvBudgetAmount']={$gte:Number(vm.totalAdvertisingBudget1) };
        }
        if(vm.totalAdvertisingBudgeMethod=='E')
        {
            qrObj['totalAdvBudgetAmount']={$gt:Number(vm.totalAdvertisingBudget1),$lt:Number(vm.totalAdvertisingBudget2)};
        }
        if (($scope.establishedYear != undefined) && ($scope.establishedYear != ''))
        {
            qrObj['established.year']={ _eval: 'regex', value: $scope.establishedYear };
        }
        if (($scope.vendorsName != undefined) && ($scope.vendorsName != ''))
        {
            qrObj['vendors.name']={ _eval: 'regex', value: $scope.vendorsName };
        }
        if (($scope.conductBusiness != undefined) && ($scope.conductBusiness != ''))
        {
            if($scope.conductBusinessbFlag==true)
            {

                qrObj['conductBusiness']=$scope.conductBusiness;
            }
            else
            {
                qrObj['conductBusiness']={ _eval: 'regex', value: $scope.conductBusiness };    
            }                  
        }
        if (($scope.catalogOnline == true) && (($scope.onlineSales != undefined) && ($scope.onlineSales != '')))
        {
            qrObj['onlineSales']=$scope.onlineSales;
        }
        if (($scope.employees != undefined) && ($scope.employees != '')){

            qrObj['employees']={ _eval: 'regex', value: $scope.employees };
        }
        if($scope.listingType.length > 0)
        {
            qrObj['listingType.listingName']={'$in':[]};                   
            for (var i = $scope.listingType.length - 1; i >= 0; i--) 
            {
                qrObj['listingType.listingName']['$in'].push($scope.listingType[i].id);
            }
        }
        if($scope.disbursalType.length > 0)
        {
            qrObj['directMarketingBudgetDisbursal.codeName']={'$in':[]};                          
            for (var i = $scope.disbursalType.length - 1; i >= 0; i--) 
            {
                qrObj['directMarketingBudgetDisbursal.codeName']['$in'].push($scope.disbursalType[i].id);
            }
        }
        if($scope.t1){
            if(qrObj['directMarketingBudget'] === undefined )
            qrObj['directMarketingBudget']={"$in":[]};
            qrObj['directMarketingBudget']['$in'].push({ _eval: 'regex', value: $scope.t1 });
        }
        if($scope.t2){
            if(qrObj['directMarketingBudget'] === undefined )
            qrObj['directMarketingBudget']={"$in":[]};
            qrObj['directMarketingBudget']['$in'].push({ _eval: 'regex', value: $scope.t2 });
        }


            return qrObj;
        }
            //ASD@123 searchFunction end
            $scope.refinesearchAADflag=1;

  
            if (localStorage.url !== $rootScope.currentDirectory.name) {
                $state.go('core.login');
                $localStorage.$reset();
                localStorage.url = $rootScope.currentDirectory.name;
            }
            $scope.dtInstance = {};
            $scope.dtInstanceCallback = function(instance) {
                $scope.dtInstance = instance;
            };
            $scope.dataCommonService=dataCommonService;        
            vm.dtIC = function(instance) {
                vm.dtI = instance;
            };
            $scope.autocompletecity=function(data){
                   
            }
            $scope.arrayOfObjectsForIndex = {};
            $scope.totalDisplayed = 10;
            $scope.loadMore = function() {
                $scope.totalDisplayed += 10;
            };
            $scope.basicSearchResetFunction = function() {
                $scope.name = undefined;
                $scope.firstName = undefined;
                $scope.lastName = undefined;
                $scope.instituteNameAllFlag = false;
                $scope.officerType =[];

            };
            $scope.keywordSearchResetFunction = function() {
                $scope.keyword = undefined;
                $scope.keywordDropDown = undefined;
                $scope.keywordSearchAllFlag = undefined;
            };
            vm.firstLoad = true;

            function initDatatable(promise) {
                vm.dtI.changeData(promise);
                vm.dtI.reloadData();
                vm.dtI.rerender();
                vm.firstLoad = false;
            }
                 
            vm.dtO = DTOptionsBuilder.
            fromSource(DirectoryOrganizationService.dataTable)
                .withPaginationType('full_numbers')
                .withOption('order', [0, 'asc'])
                .withDataProp('data')
                .withOption('oLanguage', {
                    "sEmptyTable": "No matching records found"
                })
                .withOption('fnRowCallback', function(nRow) { $compile(nRow)($scope); })
                .withOption('responsive', true)
                .withOption('processing', true)
                .withOption('serverSide', true);


                    function accInit() {
                        setTimeout(function() {
                            $('#accordion').accordion({
                                collapsible: true,
                                active: false
                            });
                            $compile('#accordion')($scope);
                        }, 100);
                    }
                    $scope.$watch('tabHref', function(newVal) {
                    if(newVal==1){
                        vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                        accInit();  
                    }
                    if(newVal==2){
                        vm.dtC = [xtempname.directorCEO.withTitle('Name'),xtempname.personnel,xtempname.name,xtempname.cityName, xtempname.statedescription, xtempname.actions ];
                         accInit();
                    }
                    if(newVal==3)
                    {
                        $scope.keywordSearchAllFlag=true;
                        vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                    }
            });
        }

    ]);
