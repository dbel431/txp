'use strict';
angular.module('web')
.controller("browsertitle",function($scope,$location,dataCommonService, $rootScope, $localStorage){
        $scope.logo={};
        $scope.dataCommonService=dataCommonService;
        $scope.dataCommonService.data=1;
     //   $scope.=0;

        var path = $location.path();
        path = path.split('/');

        if(path[1].toUpperCase() == "OMD") {
            $scope.logo = {
                image : 'assets/OMD/favicon.ico',
                title : 'Official Museum Directory'
            }; 
            // $rootScope.logout();         
            // delete $localStorage['NRP-USER-TOKEN'];
        }
        if(path[1].toUpperCase() == "OCD") {
            $scope.logo = {
                image : 'assets/ocd_favicon.ico',
                title : 'Official Catholic Directory'
            };
            // $rootScope.logout();
            // delete $localStorage['NRP-USER-TOKEN'];
        }
        if(path[1].toUpperCase() == "CFS") {
            $scope.logo = {
                image : 'assets/CFS/favicon.ico',
                title : 'Corporate Finance Sourcebook'
            };
            // $rootScope.logout();
            // delete $localStorage['NRP-USER-TOKEN'];
        }
        if(path[1].toUpperCase() == "AAD") {
            $scope.logo = {
                image : 'assets/AAD/favicon.ico',
                title : 'American Art Directory'
            };
            // $rootScope.logout();
            // delete $localStorage['NRP-USER-TOKEN'];
        }
        if(path[1].toUpperCase() == "DMMP") {
            $scope.logo = {
                image : 'assets/dmmp_favicon.ico',
                title : 'Direct Marketing Market Place'
            };
            // $rootScope.logout();
            // delete $localStorage['NRP-USER-TOKEN'];
        }
        if(path[1].toUpperCase() == "NRP") {
            $scope.logo = {
                image : 'assets/nrpFavicon.ico',
                title : 'National Register Publishing'
            };
            // $rootScope.logout();
            // delete $localStorage['NRP-USER-TOKEN'];
        }

});