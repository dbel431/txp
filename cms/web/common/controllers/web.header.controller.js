'use strict';
angular.module('web')
	.controller('web.header.controller', ['$scope', '$state', '$location', '$dataService', '$rootScope', '$stateParams','dataCommonService',function($scope, $state, $location, $dataService, $rootScope, $stateParams,dataCommonService){
        $scope.direcoryName = $stateParams.directoryName.name;
		$scope.goToHome=function(){
			$state.go('core.home');
		}
		$scope.dataCommonService=dataCommonService;
		$scope.showHide=0;
		$scope.caption='';
		$scope.stload=function(data){
			console.log("stload");
			console.log(data);
			// if(angular.isObject(value))
			// {
			// 	$scope.dataCommonService.caption='';
			// }
			if(angular.isString(data))
			{
				$scope.dataCommonService.caption=data;
			}
			console.log(data);
			localStorage.menuData=data.insideLink;
			if(data.caption!='More Links')
        	{
         	$state.reload($state.current.name);
        	}
         };
        $scope.toggleShowHide=function(data){
			console.log("toggleShowHide");
        	console.log(data);

        	
			$scope.showHide=!$scope.showHide;
     
        	
        	if(data.toolTip=='More Links')
        	{
        		$scope.showHide=!$scope.showHide;
        	}
        	if(data.subMenus.length==2)
        	{
        		$scope.showHide=!$scope.showHide;
        	}
       /* 	if(data.subMenu)
        	{
        	$scope.showHide=0;	
        	}*/

        	if(data.direcoryName=='AAD')
			{
				if(data.subMenus.length == 0 )
				{
				$scope.showHide=0;		
				}
				else
				$scope.showHide=1;	
			}

        };
		var locationPath = $location.path();
		locationPath = locationPath.split('/');
		
		$rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
			if (toState.name != 'core.home') {
				$scope.pageType = "inner";
			}
			if (toState.name == 'core.home') {
				$scope.pageType = "home";
			}
		});

		if (locationPath[2] === "home") {
			$scope.pageType = "home";
		} else {
			$scope.pageType = "inner";
		}

		function getMenus() {
			$dataService.get('menu', function(data){
				var menus = [],
					logo = {},
					locationPath = $location.path();

				locationPath = locationPath.split('/');

				if (data != null || data.length < 0) {
					for (var i = 0; i < data.length; i++) {
						if (data[i].type == "link") {
							menus.push(data[i]);
						} else if(data[i].type == "logo") {
							logo = data[i];
						}
					}
					$scope.menus = menus;
					$scope.logo = logo;
				}
			}, function(error){
				//console.log(error)
			})			
		}

			// $dataService.get('menu', function(data){
			// 	debugger;
			// 	var menus = [],
			// 		logo = {},
			// 		locationPath = $location.path();

			// 	locationPath = locationPath.split('/');

			// 	if (data != null || data.length < 0) {
			// 		for (var i = 0; i < data.length; i++) {
			// 			if (locationPath[2] === "home") {
			// 				data[i].customLink = data[i].link;
			// 			} else {
			// 				data[i].customLink = data[i].insideLink;
			// 			}
			// 			if (data[i].type == "link") {
			// 				menus.push(data[i]);
			// 			} else if(data[i].type == "logo") {
			// 				logo = data[i];
			// 			}
			// 		}
			// 		// 
			// 		$scope.menus = menus;
			// 		$scope.logo = logo;
			// 	}
			// }, function(error){
			// 	//console.log(error)
			// })

		var path = $location.path();

		// path = path.replace('/', '');

		// console.log(path);


		if (path == 'ocd') {
			$scope.logo = {
				image : 'assets/logo_ocd.png',
				title : 'Official Catholic Directory'
			}
		} else if(path == 'omd'){
			$scope.logo = {
				image : 'assets/logo_omd.png',
				title : 'O M D'
			}
		} else if(path == 'cfs'){
			$scope.logo = {
				image : 'assets/logo_omd.png',
				title : 'C F S'
			}
		}


		function goTop(){
			$location.hash('page-top')
			$anchorScroll();
		};

		getMenus();
	}])