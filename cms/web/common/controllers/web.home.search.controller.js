'use strict';
angular.module('web')
    .controller('DirectoryWebUserSearchControllerOMD', [
        'dataCommonService',
        '$rootScope',
        '$scope',
        '$localStorage',
        '$location',
        '$dataService',
        '$sce',
        '$state',
        '$stateParams',
        '$compile',
        '$resource',
        '$uibModal',
        'DTOptionsBuilder',
        'DTColumnBuilder',
        'DirectoryOrganizationService',
        'DirectoryInstitutionCategory',
        'DirectoryCollectionCategory',
        'DirectoryWebUserService',
        'DirectoryCityService',
        'DirectoryStateService',
        'DirectorySpecificationType',
        'DirectorySpecificationCodeForIndustryPreference',
        'DirectoryOfficerType',
        'DirectorySpecificationCodeForGeographicalPreference',
        'DirectorySpecificationCodeForPortfolioRelationship',
        'DirectorySpecificationCodeForFinancingPreferred',
        'DirectorySpecificationCodeForDealType',
        'DirectorySpecificationCodeForFirmWillTakeActiveRoleAs',
        'ListingType',
        'DisbursalType',
        function(dataCommonService,$rootScope, $scope, $localStorage, $location, $dataService, $sce, $state, $stateParams, $compile, $resource, $uibModal, DTOptionsBuilder, DTColumnBuilder, DirectoryOrganizationService, DirectoryInstitutionCategory, DirectoryCollectionCategory, DirectoryWebUserService, DirectoryCityService, DirectoryStateService, DirectorySpecificationType, DirectorySpecificationCodeForIndustryPreference, DirectoryOfficerType, DirectorySpecificationCodeForGeographicalPreference, DirectorySpecificationCodeForPortfolioRelationship, DirectorySpecificationCodeForFinancingPreferred, DirectorySpecificationCodeForDealType, DirectorySpecificationCodeForFirmWillTakeActiveRoleAs, ListingType, DisbursalType) {
            $scope.tabHref = 1;
            setTimeout(function(){$("#select_id option[value='? undefined:undefined ?']").remove();},50);
            $scope.InstitutionCategoryArrayData1=[];
            $scope.nstitutionCategorySelData1=[];
            $scope.collectionCategorySelData1=[];
            $scope.instituteNameAllFlag=false;
            $scope.keywordSearchsettings = {
                        scrollableHeight: '300px',
                        scrollable: true,
                       // enableSearch: true
                    };
            $scope.onlineSales='D';
            $scope.omdKeywordSearchArray=[{id:'entityType',label:'Institution Description'}, {id:'activities.freeText',label:'Activities'}, {id:'features.name',label:'Collection Description'}, {id:'exhibition.exhibitName',label:'Exhibit Description'}, {id:'facilities',label:'Facilities'}, {id:'governing',label:'Governing Authority'}, {id:'research.description',label:'Research Fields'}];
            $scope.omdKeywordSearchModel=[{id:'entityType',label:'Institution Description'}, {id:'activities.freeText',label:'Activities'}, {id:'features.name',label:'Collection Description'}, {id:'exhibition.exhibitName',label:'Exhibit Description'}, {id:'facilities',label:'Facilities'}, {id:'governing',label:'Governing Authority'}, {id:'research.description',label:'Research Fields'}];
            setTimeout(function(){
                $("#searchData").hide();
            },100);

            $scope.showFlag=function(){
                $("#searchData").show();
                localStorage.tabHrefFlag=1;
            }
            function strtonumber(data)
            {
                var temp=data;
                for(var i=0;i<data.split(',').length;i++)
                {
                    temp=temp.replace(',','');
                }
                return temp;
            }
            $scope.example10001settings = {scrollableHeight: '200px', scrollable: true };
        $scope.asdf=function(){

            
            // var temp=$scope.nstitutionCategorySelData;
            // var arr=$scope.InstitutionCategoryArrayData;
            // var tempArr=[];
            // if(temp.length>0)
            // for(var i=0;i<temp.length;i++)
            // {
            //     if(temp[i].pid==0)
            //     {
            //         for(var j=0;j<arr.length;j++)
            //         {
            //             if(temp[i].id==arr[j].pid)
            //             {
            //                 tempArr.push(arr[j]);
            //             }
            //         }
            //     }
            // }
            // console.log(tempArr);
            // //;
        };
        
            
            $scope.refinesearchOMDflag=1;
            $scope.refinesearchAADflag=1;
            $scope.refinesearchDMMPflag=1;
            $scope.refinesearchCFSflag=1;
            if (localStorage.url != $rootScope.currentDirectory.name) {
                $state.go('core.login');
                $localStorage.$reset();
                localStorage.url = $rootScope.currentDirectory.name;
            }
            $scope.dtInstance = {};
            $scope.dtInstanceCallback = function(instance) {
                $scope.dtInstance = instance;
            };
            $scope.cityName= $('#CityAuto').val();
            $scope.stateName1='';
            $scope.dataCommonService=dataCommonService;
            $scope.dataCommonService=1;
            if (!$localStorage['NRP-USER-TOKEN'])
            {
                localStorage.refineSearch='';
                $state.go('core.login');
            }
            if($localStorage['NRP-USER-TOKEN'] && ($localStorage['PKG']=='57a6c362ebba3d886256dba1') )
            {
              $state.go('core.login');
            }
            /*
            function dis(){      
            var target = $('#herobox');
            if (target.length)
            {
                var top = target.offset().top;
                $('html,body').animate({scrollTop: top}, 1000);
                return false;
            }}
            */
            var vm = this;
            vm.dtI = null;
            vm.dataOfData=[];
            vm.dataOfAnd={};

            //table start
            var xtempname={
                directorCEO:
                DTColumnBuilder.newColumn('name','flags')                
                .renderWith(function(name,type, row) {
                    
                    if($scope.tabHref==2)
                    {
                        console.log("$scope.lastName",$scope.lastName);
                        console.log("$scope.firstName",$scope.firstName);
                        if(($scope.lastName && $scope.firstName==undefined) || ($scope.lastName && $scope.firstName=="")){
                        var getPer=row.personnel.filter(function(val){
                            if((val.name.last.toUpperCase() == $scope.lastName.toUpperCase() )|| val.name.last.toUpperCase().indexOf($scope.lastName.toUpperCase()) != -1 )
                            {
                                return val;
                            }
                        }); 
                        row.personnel= getPer;   
                        }
                        if(($scope.firstName && $scope.lastName==undefined )|| ($scope.firstName && $scope.lastName=="" )){
                        var getPer=row.personnel.filter(function(val){
                            if((val.name.first.toUpperCase() == $scope.firstName.toUpperCase() )|| val.name.first.toUpperCase().indexOf($scope.firstName.toUpperCase()) != -1 )
                            {
                                return val;
                            }
                        }); 
                        row.personnel= getPer;   
                        }
                        if($scope.firstName && $scope.lastName){
                        var getPer=row.personnel.filter(function(val){
                            if(((val.name.first.toUpperCase() == $scope.firstName.toUpperCase() )|| (val.name.first.toUpperCase().indexOf($scope.firstName.toUpperCase()) != -1))&& ((val.name.last.toUpperCase() == $scope.lastName.toUpperCase() )|| (val.name.last.toUpperCase().indexOf($scope.lastName.toUpperCase()) != -1 )) )
                            {
                                return val;
                            }
                        }); 
                        row.personnel= getPer;   
                        }
                    }

                    var str='';
                    if(row.personnel.length > 0)
                        str=row.personnel[0].name.last+', '+row.personnel[0].name.first;
                    return str;
                    
                })
                .withOption('width', '5%'),
                name:
                DTColumnBuilder.newColumn('name','flags')
                .withTitle('Institution Name')
                .renderWith(function(name,type, row) {
                    if(name.split(' ')[0].toUpperCase()=='THE')
                        name=name.replace('The','')+',The';
                    if(row.flags)
                    {
                    if(row.flags.aam)
                        name=name+'<img src="/assets/aam-icon.png" width=15 height=15>';
                    if(row.flags.icom)
                        name=name+'<img src="/assets/icom-icon.png" width=20 height=10> ';
                    if(row.flags.accredited)
                        name=name+'<img src="/assets/star_icon.png" width=15 height=15>';
                     if(row.flags.handicapped)
                         name=name+'<img src="/assets/Handicapped-Accessible.png" width=15 height=15>';
                     }

                    return name||"";
                })
                .withOption('width', '15%'),
                personnelTitle:
                DTColumnBuilder.newColumn('personnel','flags')
                .withTitle('personnelTitle')
                .renderWith(function(name,type, row) {
                    return 'personnelTitle456';
                })
                .withOption('width', '20%'),
                personnel:DTColumnBuilder.newColumn('personnel.titleMasterName')
                .withTitle('Title')
                .renderWith(function(personnel, type, row) {

                    // if (row.personnel) {
                    //     for (var i = 0; i < row.personnel.length; i++) {
                            
                    //         var pers = row.personnel[i];
                    //         if (($scope.firstName != undefined) && ($scope.firstName != '')){

                    //             var var1 = new RegExp($scope.firstName,"i");
                    //             if (var1.test(pers.name.first)) {

                    //                 var director = pers.name ? '<p>' + [pers.titleMasterName].join(' ') + '</p>' : '';
                    //                 return director || "";
                    //             }
                    //         }
                    //         else if (($scope.lastName != undefined) && ($scope.lastName != '')){

                    //             var var1 = new RegExp($scope.lastName,"i");
                    //             if (var1.test(pers.name.last)) {

                    //                 var director = pers.name ? '<p>' + [pers.titleMasterName].join(' ') + '</p>' : '';
                    //                 return director || "";
                    //             }
                    //         }
                    //         else{
                    //              var director = row.personnel[0].name ? '<p>' + [row.personnel[0].title].join(' ') +prefix '</p>' : '';
                    //                 return director || "";
                    //         }
                    //     }
                
                    // }
                    // return '';
                    if($scope.tabHref==2)
                    {
                        console.log("$scope.lastName",$scope.lastName);
                        console.log("$scope.firstName",$scope.firstName);
                        if(($scope.lastName && $scope.firstName==undefined) || ($scope.lastName && $scope.firstName=="")){
                        var getPer=row.personnel.filter(function(val){
                            if((val.name.last.toUpperCase() == $scope.lastName.toUpperCase() )|| val.name.last.toUpperCase().indexOf($scope.lastName.toUpperCase()) != -1 )
                            {
                                return val;
                            }
                        }); 
                        row.personnel= getPer;   
                        }
                        if(($scope.firstName && $scope.lastName==undefined )|| ($scope.firstName && $scope.lastName=="" )){
                        var getPer=row.personnel.filter(function(val){
                            if((val.name.first.toUpperCase() == $scope.firstName.toUpperCase() )|| val.name.first.toUpperCase().indexOf($scope.firstName.toUpperCase()) != -1 )
                            {
                                return val;
                            }
                        }); 
                        row.personnel= getPer;   
                        }
                        if($scope.firstName && $scope.lastName){
                        var getPer=row.personnel.filter(function(val){
                            if(
                                (
                                (val.name.first.toUpperCase() == $scope.firstName.toUpperCase() )|| 
                                (val.name.first.toUpperCase().indexOf($scope.firstName.toUpperCase()
                                ) != -1))&& ((val.name.last.toUpperCase() == $scope.lastName.toUpperCase() )|| (val.name.last.toUpperCase().indexOf($scope.lastName.toUpperCase()) != -1 )) )
                            {
                                
                                return val;
                            }
                        }); 
                        row.personnel= getPer;   
                        }
                    }
                    var str='';
                    if(row.personnel.length > 0 && row.personnel[0].title)
                        return row.personnel[0].title;
                    else
                        return '';


                }).withOption('width', '8%'),
                cityName:DTColumnBuilder.newColumn('address.cityName')
                .withTitle('City')
                .renderWith(function(city, type, row) {
                    if(row.address &&row.address.length >1 && $scope.stateDatadata.length > 0)
                    {

                     var selected=row.address.filter(function(val){
                        for(var i=0;i<$scope.stateDatadata.length;i++)
                        {
//                        if(val.state && val.state.codeValue)
                         if((val.state && val.state.codeValue) && ($scope.stateDatadata[i].id==val.state.codeValue))
                            return val;
                        }
                     
                     });   
                        row.address=selected;
                    }

                   var zzz=row.address.filter(function(v){if(v.addressType=='576286d0c19cef300bc720e6'){
                            return 1;
                   }});
//                    return ((row.address && zzz.address[0] && zzz.address[0].cityName) ? (zzz.address[0].cityName || "") : "") || "";
                    return ((zzz && zzz[0] && zzz[0].cityName + '45') ? (zzz[0].cityName  || "") : "") || "";
                }).withOption('width', '10%'),
                statedescription:DTColumnBuilder.newColumn('address.state.description')
                    .withTitle('State/Territory')
                .renderWith(function(data, type, full){

                    if(full.address &&full.address.length >1 && $scope.stateDatadata.length > 0)
                    {

                     var selected=full.address.filter(function(val){
                        for(var i=0;i<$scope.stateDatadata.length;i++)
                        {
                         if($scope.stateDatadata[i].id==val.state.codeValue)
                            return val;
                        }
                     
                     });   
                        full.address=selected;
                    }
                    var zzz=full.address;
                   //  var zzz=full.address.filter(function(v){if(v.addressType=='576286d0c19cef300bc720e6'){
                   //          return 1;
                   // }});
                    return ((zzz && zzz[0] && zzz[0].state && zzz[0].state.description) ? (zzz[0].state.description|| "") : "") || "";

                })
                .withOption('width', '1%'),
                 zip:DTColumnBuilder.newColumn('address.zip')
                .withTitle('zip')
                .renderWith(function(data, type, full){
                    //return full.address[0].zip?full.address[0].zip:'';
                    var zzz=full.address.filter(function(v){if(v.addressType=='576286d0c19cef300bc720e6'){
                            return 1;
                   }});
                    return ((zzz && zzz[0] && zzz[0].zip) ? (zzz[0].zip || "") : "") || "";
                })
                .withOption('width', '6%'),
                actions:DTColumnBuilder.newColumn('_id')
                .withTitle('Actions')
                .notSortable()
                .withClass('text-center')
                .renderWith(function(id,type, full) {
                    if($rootScope.currentDirectory.directoryId == '57189b3e24d8bc65f4123bbf')
                    return '<div ng-click=advanceSearchResetFunction()><directory-web-user-record-actions record-id="\'' + id + '\'"></directory-web-user-record-actions></div>';
                      if($rootScope.currentDirectory.directoryId == '57189c7024d8bc65f4123bc0')
                    return '<div ng-click=advanceSearchResetFunctionAAD()><directory-web-user-record-actions record-id="\'' + id + '\'"></directory-web-user-record-actions></div>';
                  if($rootScope.currentDirectory.directoryId == '57189ccf24d8bc65f4123bc2')
                    return '<div ng-click=advanceSearchResetFunctionDMMP()><directory-web-user-record-actions record-id="\'' + id + '\'"></directory-web-user-record-actions></div>';
                if($rootScope.currentDirectory.directoryId == '57189cc224d8bc65f4123bc1')
                    return '<div ng-click=advanceSearchResetFunctionCFS()><directory-web-user-record-actions record-id="\'' + id + '\'"></directory-web-user-record-actions></div>';
                
                
                })
                .withOption('width', '4%'),
                personnelName:DTColumnBuilder.newColumn('personnel.titleMasterName')
                .withTitle('Pesonnel')
                .renderWith(function(personnel, type, row) {
                 //   console.log('Pesonnel',row);
                    if (row.personnel) {
                        for (var i = 0; i < row.personnel.length; i++) {
                            var pers = row.personnel[i];
                            if (($scope.firstName != undefined) && ($scope.firstName != '')){

                                var var1 = new RegExp($scope.firstName,"i");
                                if (var1.test(pers.name.first)) {
                                    if(pers.name.prefix)
                                        var director = pers.name ? '<p> ' +pers.name.prefix+" "+ [pers.name.first,pers.name.middle, pers.name.last].join(' ') +'</p>' : '';
                                    if(pers.name.suffix)
                                        var director = pers.name ? '<p> '+ [pers.name.first,pers.name.middle, pers.name.last].join(' ') + " "+pers.name.suffix + '</p>' : '';
                                    if(pers.name.suffix && pers.name.prefix )
                                     var director = pers.name ? '<p> ' +pers.name.prefix+" "+ [pers.name.first,pers.name.middle, pers.name.last].join(' ') + " "+pers.name.suffix +'</p>' : '';
                                    if(pers.name.first && pers.name.last)
                                    {
                                        return pers.name.first+" "+pers.name.last;
                                    }
                                    return director || "";
                                }
                            }
                            else if (($scope.lastName != undefined) && ($scope.lastName != '')){

                                var var1 = new RegExp($scope.lastName,"i");
                                if (var1.test(pers.name.last)) {

                                    var director = pers.name ? '<p>' + [pers.name.first,pers.name.middle, pers.name.last].join(' ') + '</p>' : '';
                                    return director || "";
                                }
                            }
                            else{
                                 var director = row.personnel[0].name ? '<p>' + [row.personnel[0].name.first,row.personnel[0].name.middle, row.personnel[0].name.last].join(' ') + '</p>' : '';
                                    return director || "";
                            }
                        }
                
                    }
                    return '';
                }).withOption('width', '9%')
                };
            //table end
            if('57189cc224d8bc65f4123bc1'==$rootScope.currentDirectory.directoryId)
            {
                vm.dataOfAnd['specificationType']={'$in':[]};
                vm.dataOfAnd['codeName']={'$in':[]};
            }
            vm.dtIC = function(instance) {
                vm.dtI = instance;
            };
             $scope.attendanceCount='';
             $scope.attendanceCounts='';
             $scope.searchFunctionCFS=function(){
              //  $scope.dataCommonService.data=1;
                vm.cfsData={};
                if (($scope.mainSourcesOfCapital != undefined) && ($scope.mainSourcesOfCapital != '')) {
                         if(vm.cfsData['specificationType']==undefined)
                            {
                                vm.cfsData['specificationType']={'$in':[]};
                                vm.cfsData['specificationType']['$in'].push("MAIN SOURCES OF CAPITAL");
                            }
                            else
                            vm.cfsData['specificationType']['$in'].push("MAIN SOURCES OF CAPITAL");
                            if(vm.cfsData['name']==undefined)
                            {
                                vm.cfsData['name']={'$in':[]};
                                vm.cfsData['name']['$in'].push({ _eval: 'regex', value: $scope.mainSourcesOfCapital});
                            }
                            else
                            vm.cfsData['name']['$in'].push({ _eval: 'regex', value: $scope.mainSourcesOfCapital});
                }
                if (($scope.fundsAvailabel != undefined) && ($scope.fundsAvailabel != '')) {
                         if(vm.cfsData['specificationType']==undefined)
                            {
                                vm.cfsData['specificationType']={'$in':[]};
                                vm.cfsData['specificationType']['$in'].push("FUNDS AVAILABLE FOR INVESTMENTS OR LOANS");
                            }
                            else
                            vm.cfsData['specificationType']['$in'].push("FUNDS AVAILABLE FOR INVESTMENTS OR LOANS");
                            if(vm.cfsData['name']==undefined)
                            {
                                vm.cfsData['name']={'$in':[]};
                                vm.cfsData['name']['$in'].push({ _eval: 'regex', value: $scope.fundsAvailabel});
                            }
                            else
                            vm.cfsData['name']['$in'].push({ _eval: 'regex', value: $scope.fundsAvailabel});
                }
                if (($scope.exitCriteria != undefined) && ($scope.exitCriteria != '')) 
                {
                         if(vm.cfsData['specificationType']==undefined)
                            {
                                vm.cfsData['specificationType']={'$in':[]};
                                vm.cfsData['specificationType']['$in'].push("EXIT CRITERIA");
                            }
                            else
                            vm.cfsData['specificationType']['$in'].push("EXIT CRITERIA");
                            if(vm.cfsData['name']==undefined)
                            {
                                vm.cfsData['name']={'$in':[]};
                                vm.cfsData['name']['$in'].push({ _eval: 'regex', value: $scope.exitCriteria});
                            }
                            else
                            vm.cfsData['name']['$in'].push({ _eval: 'regex', value: $scope.exitCriteria});
                }
                if (($scope.averageNumberOfDealsCompletedAnnually != undefined) && ($scope.averageNumberOfDealsCompletedAnnually != '')) 
                {
                         if(vm.cfsData['specificationType']==undefined)
                            {
                                vm.cfsData['specificationType']={'$in':[]};
                                vm.cfsData['specificationType']['$in'].push("AVERAGE NUMBER OF DEALS COMPLETED ANNUALLY");
                            }
                            else
                            vm.cfsData['specificationType']['$in'].push("AVERAGE NUMBER OF DEALS COMPLETED ANNUALLY");
                            if(vm.cfsData['name']==undefined)
                            {
                                vm.cfsData['name']={'$in':[]};
                                vm.cfsData['name']['$in'].push({ _eval: 'regex', value: $scope.averageNumberOfDealsCompletedAnnually});
                            }
                            else
                            vm.cfsData['name']['$in'].push({ _eval: 'regex', value: $scope.averageNumberOfDealsCompletedAnnually});
                }
                if (($scope.averageAmountInvestedAnnually != undefined) && ($scope.averageAmountInvestedAnnually != '')) 
                {
                         if(vm.cfsData['specificationType']==undefined)
                            {
                                vm.cfsData['specificationType']={'$in':[]};
                                vm.cfsData['specificationType']['$in'].push("AVERAGE AMOUNT INVESTED ANNUALLY");
                            }
                            else
                            vm.cfsData['specificationType']['$in'].push("AVERAGE AMOUNT INVESTED ANNUALLY");
                            if(vm.cfsData['name']==undefined)
                            {
                                vm.cfsData['name']={'$in':[]};
                                vm.cfsData['name']['$in'].push({ _eval: 'regex', value: $scope.averageAmountInvestedAnnually});
                            }
                            else
                            vm.cfsData['name']['$in'].push({ _eval: 'regex', value: $scope.averageAmountInvestedAnnually});
                }
                
                    if (($scope.minimumOperatingData != undefined) && ($scope.minimumOperatingData != '')) {
                         if(vm.cfsData['specificationType']==undefined)
                            {
                                vm.cfsData['specificationType']={'$in':[]};
                                vm.cfsData['specificationType']['$in'].push("MINIMUM OPERATING DATA REQUIRED TO CONSIDER FINANCING");
                            }
                            else
                            vm.cfsData['specificationType']['$in'].push("MINIMUM OPERATING DATA REQUIRED TO CONSIDER FINANCING");
                            if(vm.cfsData['name']==undefined)
                            {
                                vm.cfsData['name']={'$in':[]};
                                vm.cfsData['name']['$in'].push({ _eval: 'regex', value: $scope.minimumOperatingData});
                            }
                            else
                            vm.cfsData['name']['$in'].push({ _eval: 'regex', value: $scope.minimumOperatingData});
                }

                if (($scope.firmPrefersNotToInvestIn != undefined) && ($scope.firmPrefersNotToInvestIn != '')) {
                         if(vm.cfsData['specificationType']==undefined)
                            {
                                vm.cfsData['specificationType']={'$in':[]};
                                vm.cfsData['specificationType']['$in'].push("FIRM PREFERS NOT TO INVEST IN");
                            }
                            else
                            vm.cfsData['specificationType']['$in'].push("FIRM PREFERS NOT TO INVEST IN");
                            if(vm.cfsData['name']==undefined)
                            {
                                vm.cfsData['name']={'$in':[]};
                                vm.cfsData['name']['$in'].push({ _eval: 'regex', value: $scope.firmPrefersNotToInvestIn});
                            }
                            else
                            vm.cfsData['name']['$in'].push({ _eval: 'regex', value: $scope.firmPrefersNotToInvestIn});
                }

                
                if (($scope.investmentPortfolioSize != undefined) && ($scope.investmentPortfolioSize != '')) {
                         if(vm.cfsData['specificationType']==undefined)
                            {
                                vm.cfsData['specificationType']={'$in':[]};
                                vm.cfsData['specificationType']['$in'].push("INVESTMENT PORTFOLIO SIZE");
                            }
                            else
                            vm.cfsData['specificationType']['$in'].push("INVESTMENT PORTFOLIO SIZE");
                            if(vm.cfsData['name']==undefined)
                            {
                                vm.cfsData['name']={'$in':[]};
                                vm.cfsData['name']['$in'].push({ _eval: 'regex', value: $scope.investmentPortfolioSize});
                            }
                            else
                            vm.cfsData['name']['$in'].push({ _eval: 'regex', value: $scope.investmentPortfolioSize});
                }
                if (($scope.preferredSizeInvestment != undefined) && ($scope.preferredSizeInvestment != '')) {
                         if(vm.cfsData['specificationType']==undefined)
                            {
                                vm.cfsData['specificationType']={'$in':[]};
                                vm.cfsData['specificationType']['$in'].push("INVESTMENT PORTFOLIO SIZE");
                            }
                            else
                            vm.cfsData['specificationType']['$in'].push("INVESTMENT PORTFOLIO SIZE");
                            if(vm.cfsData['name']==undefined)
                            {
                                vm.cfsData['name']={'$in':[]};
                                vm.cfsData['name']['$in'].push({ _eval: 'regex', value: $scope.preferredSizeInvestment});
                            }
                            else
                            vm.cfsData['name']['$in'].push({ _eval: 'regex', value: $scope.preferredSizeInvestment});
                }
                if ($scope.stateDatadata.length> 0) {
                    vm.cfsData['address.stateName']={'$in':[]};
                    for (var i = $scope.stateDatadata.length - 1; i >= 0; i--) {
                         vm.cfsData['address.stateName']['$in'].push($scope.stateDatadata[i].id);
                    }
                }
                // if($scope.cityDatadata.length>0){
                //        vm.cfsData['address.cityName']={'$in':[]};
                //     for (var i = $scope.cityDatadata.length - 1; i >= 0; i--) {
                //        vm.cfsData['address.cityName']['$in'].push($scope.cityDatadata[i].id);
                //     }
                // }
                if (($scope.zip != undefined) && ($scope.zip != ''))
                {
                      vm.cfsData['address.zip']=$scope.zip;
                }
                if (($scope.establishedYear != undefined) && ($scope.establishedYear != ''))
                {
                      //vm.cfsData['established.year']={ _eval: 'regex', value: $scope.establishedYear };
                      vm.cfsData['established.year']=$scope.establishedYear;
                }
                if($scope.industryPreferences)
                if ($scope.industryPreferences.length>0) 
                {
                    if(vm.cfsData['specificationType']==undefined)
                            {
                                vm.cfsData['specificationType']={'$in':[]};
                            }
                        vm.cfsData['specificationType']['$in'].push("INDUSTRY PREFERENCE");
                        if(vm.cfsData['codeName']==undefined)
                        {
                        vm.cfsData['codeName']={'$in':[]};
                        }
                        for (var i = $scope.industryPreferences.length - 1; i >= 0; i--) 
                        {
                           vm.cfsData['codeName']['$in'].push($scope.industryPreferences[i].id);
                        }
                    }
                    if ($scope.geographicalPreference.length>0) 
                    {
                        if(vm.cfsData['specificationType']==undefined)
                            {
                                vm.cfsData['specificationType']={'$in':[]};
                            }
                         vm.cfsData['specificationType']['$in'].push("GEOGRAPHICAL PREFERENCES");
                         if(vm.cfsData['codeName']==undefined)
                         {
                               vm.cfsData['codeName']={'$in':[]};
                         }
                        for (var i = $scope.geographicalPreference.length - 1; i >= 0; i--) 
                        {
                             vm.cfsData['codeName']['$in'].push($scope.geographicalPreference[i].id);
                        }
                    }
                    if (($scope.geographicalPreferencesAroundTheWorld != undefined) && ($scope.geographicalPreferencesAroundTheWorld != '')) 
                    {
                        if(vm.cfsData['specificationType']==undefined)
                            {
                                vm.cfsData['specificationType']={'$in':[]};
                            }
                         vm.cfsData['specificationType']['$in'].push("INTERNATIONAL GEOGRAPHICAL PREFERENCES");
                         if(vm.cfsData['name']==undefined)
                         {
                               vm.cfsData['name']={'$in':[]};
                         }
                         vm.cfsData['name']['$in'].push({ _eval: 'regex', value: $scope.geographicalPreferencesAroundTheWorld });
                        
                    }
                    if ($scope.firmWillTakeActiveRoleAs.length>0) 
                    {
                        if(vm.cfsData['specificationType'] == undefined)
                        {
                            vm.cfsData['specificationType']={'$in':[]};
                            vm.cfsData['specificationType']['$in'].push("FIRM WILL TAKE ACTIVE ROLE AS");
                        }
                        else
                            vm.cfsData['specificationType']['$in'].push("FIRM WILL TAKE ACTIVE ROLE AS");

                        if(vm.cfsData['codeName'] == undefined)
                        {
                                vm.cfsData['codeName']={'$in':[]};
                        }
                        for (var i = $scope.firmWillTakeActiveRoleAs.length - 1; i >= 0; i--) 
                        {
                           vm.cfsData['codeName']['$in'].push($scope.firmWillTakeActiveRoleAs[i].id);
                        }
                    }
                    if (($scope.minimumSizeInvestment != undefined) && ($scope.minimumSizeInvestment != '')) {
                        if(vm.cfsData['specificationType'] == undefined)
                        {
                            vm.cfsData['specificationType']={'$in':[]};
                            vm.cfsData['specificationType']['$in'].push("MINIMUM SIZE INVESTMENT");
                        }
                        else
                            vm.cfsData['specificationType']['$in'].push("MINIMUM SIZE INVESTMENT");

                        if(vm.cfsData['name'] == undefined)
                        {
                                vm.cfsData['name']={'$in':[]};
                        }
                        vm.cfsData['name']['$in'].push($scope.minimumSizeInvestment );
                }
                    if (($scope.maximumSizeInvestmentInOneCompany != undefined) && ($scope.maximumSizeInvestmentInOneCompany != '')) {
                    if(vm.cfsData['specificationType'] == undefined)
                        {
                            vm.cfsData['specificationType']={'$in':[]};
                            vm.cfsData['specificationType']['$in'].push("MAXIMUM SIZE INVESTMENT IN ONE COMPANY");
                        }
                        else
                            vm.cfsData['specificationType']['$in'].push("MAXIMUM SIZE INVESTMENT IN ONE COMPANY");

                        if(vm.cfsData['name'] == undefined)
                        {
                                vm.cfsData['name']={'$in':[]};
                        }
                        vm.cfsData['name']['$in'].push({ _eval: 'regex', value: $scope.maximumSizeInvestmentInOneCompany });
                    }                
                    if (($scope.portfolioRelationship != undefined) && ($scope.portfolioRelationship != '')) 
                    {
                        if(vm.cfsData['specificationType'] == undefined)
                        {
                            vm.cfsData['specificationType']={'$in':[]};
                            vm.cfsData['specificationType']['$in'].push("PORTFOLIO RELATIONSHIP");
                        }
                        else
                        vm.cfsData['specificationType']['$in'].push("PORTFOLIO RELATIONSHIP");
                        if(vm.cfsData['name'] == undefined)
                        {
                                vm.cfsData['name']={'$in':[]};
                        }
                        vm.cfsData['name']['$in'].push({ _eval: 'regex', value: $scope.portfolioRelationship });
                        
                    }
                    if ($scope.financingPreferred.length>0) 
                    {
                        if(vm.cfsData['specificationType'] == undefined)
                        {
                            vm.cfsData['specificationType']={'$in':[]};
                            vm.cfsData['specificationType']['$in'].push("FINANCING PREFERRED");
                        }
                        else
                            vm.cfsData['specificationType']['$in'].push("FINANCING PREFERRED");

                        if(vm.cfsData['codeName'] == undefined)
                        {
                                vm.cfsData['codeName']={'$in':[]};
                        }
                        for (var i = $scope.financingPreferred.length - 1; i >= 0; i--) 
                        {
                            vm.cfsData['codeName']['$in'].push($scope.financingPreferred[i].id);
                        }
                    }
                    //exe query

                        //$('#searchData').hide()
                     vm.cfsData['directoryId']=$rootScope.currentDirectory.directoryId;
                        var promise = $resource(APIBasePath + 'subscriber/search/chapter-specificationData', {}, {
                                dataTable: {
                                    method: 'POST',
                                    transformRequest: function(data, headerGetter) {
                                        var headers = headerGetter();
                                        var newData = {
                                            dataTableQuery: data,
                                            conditions: vm.cfsData
                                        }
                                        return JSON.stringify(newData);
                                    }
                                }
                            });
                            initDatatable(promise.dataTable);
                            return promise;
             //    console.log(vm.cfsData);

             };
$scope.advanceCFSSearchResetFunction=function(){
$scope.name=undefined;
$scope.instituteNameAllFlag=undefined;
$scope.parentName=undefined;
$scope.instituteParentNameAllFlag=undefined;
$scope.firstName=undefined;
$scope.officerType=[];
$scope.lastName=undefined;
$scope.cityName = undefined;
$scope.stateName = undefined;
$scope.cityDatadata=[];
$scope.stateDatadata=[];
$scope.establishedYear=undefined;
$scope.industryPreferences=[];
$scope.geographicalPreference.codeVal=[];
$scope.geographicalPreferencesAroundTheWorld=undefined;
$scope.firmWillTakeActiveRoleAs.codeVal={ codeVal: '' };;
$scope.minimumSizeInvestment=undefined;
$scope.preferredSizeInvestment=undefined;
$scope.maximumSizeInvestmentInOneCompany=undefined;
$scope.portfolioRelationship.codeVal={ codeVal: '' };
$scope.zip=undefined;
$scope.fundsAvailabel=undefined;
$scope.mainSourcesOfCapital=undefined;
$scope.financingPreferred.codeVal={ codeVal: '' };;
$scope.firmPrefersNotToInvestIn=undefined;
$scope.minimumOperatingData=undefined;
$scope.exitCriteria=undefined;
$scope.investmentPortfolioSize=undefined;
$scope.averageNumberOfDealsCompletedAnnually=undefined;
$scope.averageAmountInvestedAnnually=undefined;

}
$scope.advanceSearchResetFunctionCFS=function(){
        $scope.refinesearchCFSflag+=1;
        if($scope.refinesearchCFSflag % 2 == 0)
        {
    temp={};
    temp.name=$scope.name;
    temp.instituteNameAllFlag=$scope.instituteNameAllFlag;
    temp.cityDatadata=$scope.cityDatadata;
    temp.stateDatadata=$scope.stateDatadata;
    temp.establishedYear=$scope.establishedYear;
    temp.industryPreferences=$scope.industryPreferences;
    temp.geographicalPreference=$scope.geographicalPreference;
    temp.geographicalPreferencesAroundTheWorld=$scope.geographicalPreferencesAroundTheWorld;
    temp.firmWillTakeActiveRoleAs=$scope.firmWillTakeActiveRoleAs;
    temp.minimumSizeInvestment=$scope.minimumSizeInvestment;
    temp.preferredSizeInvestment=$scope.preferredSizeInvestment;
    temp.maximumSizeInvestmentInOneCompany=$scope.maximumSizeInvestmentInOneCompany;
    temp.portfolioRelationship=$scope.portfolioRelationship;
    temp.zip=$scope.zip;
    temp.fundsAvailabel=$scope.fundsAvailabel;
    temp.mainSourcesOfCapital=$scope.mainSourcesOfCapital;
    temp.financingPreferred=$scope.financingPreferred;
    temp.firmPrefersNotToInvestIn=$scope.firmPrefersNotToInvestIn;
    temp.minimumOperatingData=$scope.minimumOperatingData;
    temp.exitCriteria=$scope.exitCriteria;
    temp.investmentPortfolioSize=$scope.investmentPortfolioSize;
    temp.averageNumberOfDealsCompletedAnnually=$scope.averageNumberOfDealsCompletedAnnually;
    temp.averageAmountInvestedAnnually=$scope.averageAmountInvestedAnnually;
    temp.firstName=$scope.firstName;
    temp.lastName=$scope.lastName;
    localStorage.refineSearch=JSON.stringify(temp);
    }
    $scope.cityDatadata=[];
    $scope.stateDatadata=[];
    $scope.industryPreferences=[];
    $scope.geographicalPreference=[];
    $scope.firmWillTakeActiveRoleAs=[];
    $scope.portfolioRelationship=[];
    $scope.financingPreferred=[];
    $scope.name=undefined;
    $scope.instituteNameAllFlag=undefined;
    $scope.establishedYear=undefined;
    $scope.geographicalPreferencesAroundTheWorld=undefined;
    $scope.minimumSizeInvestment=undefined;
    $scope.preferredSizeInvestment=undefined;
    $scope.maximumSizeInvestmentInOneCompany=undefined;
    $scope.zip=undefined;
    $scope.fundsAvailabel=undefined;
    $scope.mainSourcesOfCapital=undefined;
    $scope.firmPrefersNotToInvestIn=undefined;
    $scope.minimumOperatingData=undefined;
    $scope.exitCriteria=undefined;
    $scope.investmentPortfolioSize=undefined;
    $scope.averageNumberOfDealsCompletedAnnually=undefined;
    $scope.averageAmountInvestedAnnually=undefined;
    $scope.firstName=undefined;
    $scope.lastName=undefined;
}
$scope.advanceSearchResetFunctionDMMP=function()
{
        $scope.refinesearchDMMPflag+=1;
        if($scope.refinesearchDMMPflag % 2 == 0)
        {
        var temp={};
        temp.t1=$scope.t1;
        temp.t2=$scope.t2;
        temp.conductBusinessbFlag=$scope.conductBusinessbFlag;
        temp.instituteNameAllFlag=$scope.instituteNameAllFlag;
        temp.totalAdvertisingBudgeMethod=vm.totalAdvertisingBudgeMethod;
        temp.totalAdvertisingBudget1=vm.totalAdvertisingBudget1;
        temp.totalAdvertisingBudget2=vm.totalAdvertisingBudget2;
        temp.directMarketingBudgetFlag=$scope.directMarketingBudgetFlag;
        temp.galleryDescriptionFlag=$scope.galleryDescriptionFlag;
        temp.disbursalType1=$scope.disbursalType1;
        temp.listingType1=$scope.listingType1;
        temp.cityName=$scope.cityName;
        temp.stateName1=$scope.stateName1;
        temp.firstName=$scope.firstName;
        temp.lastName=$scope.lastName;
        temp.name=$scope.name;
        temp.cityDatadata=$scope.cityDatadata;
        temp.stateDatadata=$scope.stateDatadata;
        temp.establishedYear=$scope.establishedYear;
        temp.conductBusiness=$scope.conductBusiness;
        temp.directMarketingBudget=$scope.directMarketingBudget;
        temp.totalAdvertisingBudget=$scope.totalAdvertisingBudget;
        temp.catalogOnline=$scope.catalogOnline;
        temp.onlineSales=$scope.onlineSales;
        temp.zip=$scope.zip;
        temp.employees=$scope.employees;
        temp.listingType=$scope.listingType;
        temp.disbursalType=$scope.disbursalType;
        temp.galleryDescription=$scope.galleryDescription;
        temp.vendorsName=$scope.vendorsName;
        localStorage.refineSearch=JSON.stringify(temp);        
        }
        $scope.conductBusinessbFlag=undefined;
        $scope.instituteNameAllFlag=undefined;
        vm.totalAdvertisingBudgeMethod=undefined;
        vm.totalAdvertisingBudget1=undefined;
        vm.totalAdvertisingBudget2=undefined;
        $scope.t1=undefined;
        $scope.t2=undefined;
        $scope.directMarketingBudgetFlag=undefined;
        $scope.galleryDescriptionFlag=undefined;
        $scope.disbursalType1=undefined;
        $scope.listingType1=undefined;
        $scope.cityName=undefined;
        $scope.stateName1=undefined;
        $scope.firstName=undefined;
        $scope.lastName=undefined;
        $scope.name=undefined;
        $scope.cityDatadata=[];
        $scope.stateDatadata=[];
        $scope.establishedYear=undefined;
        $scope.conductBusiness=undefined;
        $scope.directMarketingBudget= undefined
        $scope.totalAdvertisingBudget= undefined
        $scope.catalogOnline = false;
        $scope.onlineSales=false;
        $scope.zip = undefined;
        $scope.employees = undefined;
        $scope.listingType=[];
        $scope.disbursalType=[];
        $scope.galleryDescription=undefined;
        $scope.vendorsName=undefined;
}
$scope.advanceSearchResetFunctionAAD=function()
{
        $scope.refinesearchAADflag+=1;
        if($scope.refinesearchAADflag % 2 == 0)
        {
        var temp={};
        //$scope.keywordDropDown == 'exhibition.exhibitNameAAD'
        //temp.keyword=$scope.keyword;
        temp.keywordSearchAllFlag=$scope.keywordSearchAllFlag;
        temp.activitiesEducationDeptbFlag=$scope.activitiesEducationDeptbFlag;
        temp.establishedDetailbFlag=$scope.establishedDetailbFlag;
        temp.galleryDescriptionFlag=$scope.galleryDescriptionFlag;
        temp.collectionDescriptionAllFlag=$scope.collectionDescriptionAllFlag;
        temp.attendanceMethod=vm.attendanceMethod;
        temp.attendanceCount=$scope.attendanceCount;
        temp.attendanceCounts=$scope.attendanceCounts;
        temp.keywordDropDown=$scope.keywordDropDown;

        temp.keyword=$scope.keyword;
        temp.cityName=$scope.cityName;
        temp.stateName1=$scope.stateName1;
        temp.shortName=$scope.shortName;
        temp.firstName=$scope.firstName;
        temp.lastName=$scope.lastName;
        temp.name=$scope.name;
        temp.cityDatadata=$scope.cityDatadata;
        temp.stateDatadata=$scope.stateDatadata;
        temp.parentNameForAAD=$scope.parentNameForAAD;
        temp.establishedDetail=$scope.establishedDetail;
        temp.galleryDescription=$scope.galleryDescription;
        temp.exhibitionDescription=$scope.exhibitionDescription;
        temp.zip=$scope.zip;
        temp.attendanceCount=$scope.attendanceCount;
        temp.activitiesEducationDept=$scope.activitiesEducationDept;
        temp.degreeGranted=$scope.degreeGranted;
        temp.collectionDescription=$scope.collectionDescription;
        temp.scholarship=$scope.scholarship;
        temp.fellowship=$scope.fellowship;
        temp.assistantship=$scope.assistantship;
        temp.grants=$scope.grants;
        temp.instituteNameAllFlag=$scope.instituteNameAllFlag;
        temp.minorNameAllFlag=$scope.minorNameAllFlag;
        localStorage.refineSearch=JSON.stringify(temp);
        }
        $scope.keywordSearchAllFlag=undefined;
        $scope.activitiesEducationDeptbFlag=undefined;
        $scope.establishedDetailbFlag=undefined;
        $scope.galleryDescriptionFlag=undefined;
        $scope.collectionDescriptionAllFlag=undefined;
        vm.attendanceMethod=undefined;
        $scope.attendanceCount=undefined;
        $scope.attendanceCounts=undefined;
        $scope.cityName=undefined;
        $scope.stateName1=undefined;
        $scope.shortName=undefined;
        $scope.firstName=undefined;
        $scope.lastName=undefined;
        $scope.name=undefined;
        $scope.cityDatadata=[];
        $scope.stateDatadata=[];
        $scope.parentNameForAAD=undefined;
        $scope.establishedDetail=undefined;
        $scope.galleryDescription=undefined;
        $scope.exhibitionDescription=undefined;
        $scope.zip=undefined;
        $scope.attendanceCount=undefined;
        $scope.activitiesEducationDept=undefined;
        $scope.degreeGranted=undefined;
        $scope.collectionDescription=undefined;
        $scope.scholarship=false;
        $scope.fellowship=false;
        $scope.assistantship=false;
        $scope.grants=false;
        $scope.instituteNameAllFlag=false;
        $scope.minorNameAllFlag=false;    


}
$scope.attendanceCountFun=function(str,data)
{
    

    if(str=='attendanceCount')
    {
       // data=parseFloat(data.replace(',',''));
        $scope.attendanceCount=data;
    }
    if(str=='attendanceCounts')
    {
    //data=parseFloat(data.replace(',',''));
     $scope.attendanceCounts=data;   
    }

}
            $scope.flag = 0;
            $scope.var1 = 1;
            $scope.enable = true;
            $scope.collectionCategory = [];
            $scope.institutionCategory = []
            $scope.industryPreferences =[];
            $scope.geographicalPreference =[];
            $scope.officerType = [];
            $scope.portfolioRelationship =[]
            $scope.financingPreferred = [];
            $scope.dealType = [];
            $scope.firmWillTakeActiveRoleAs = [];
            $scope.listingType = [];
            $scope.disbursalType = [];
            $scope.InstitutionCategoryArray = DirectoryInstitutionCategory;
            vm.mainCat=[];
            $scope.$watch('vm.mainCat',function(n,o){
                $scope.InstitutionCategoryArrayData=[];
                if(vm.mainCat.length==0)
                {
                    $scope.nstitutionCategorySelData=[];
                }
                $scope.nstitutionCategorySelData=$scope.nstitutionCategorySelData.filter(function(val){
                        for(var i=0;i<n.length;i++)
                        {
                            if(val.pid==n[i].id)
                            return val;
                        }
                    });
                var x=[];
                for(var i=0;i<n.length;i++)
                {
                    x[i]=$scope.InstitutionCategoryArrayDataSUB.map(function(d){
                        if(n[i].id==d.pid)
                            return d;
                    }).filter(function(d1){return d1!=null;});
                }
                $scope.nstitutionCategorySelData=[];
                for(var i=0;i<x.length;i++)
                {
                    $scope.InstitutionCategoryArrayData = $scope.InstitutionCategoryArrayData.concat(x[i]);
                    $scope.nstitutionCategorySelData=$scope.nstitutionCategorySelData.concat(x[i]);

                }
            },true);
            $scope.InstitutionCategoryArrayData=DirectoryInstitutionCategory.map(function(data)
                {
                    if(data.parentId)
                    return {id:data._id,label:data.codeValue,pid:data.parentId._id};
                })
            $scope.InstitutionCategoryArrayDataSUB=$scope.InstitutionCategoryArrayData.filter(function(d){return d!=null;})

            $scope.InstitutionCategoryArrayDataMainCat=DirectoryInstitutionCategory.map(function(data)
                {
                    if(!data.parentId)
                        return {id:data._id,label:data.codeValue};
                })
            $scope.InstitutionCategoryArrayDataMainCat=$scope.InstitutionCategoryArrayDataMainCat.filter(function(d){return d!=null;})
            $scope.nstitutionCategorySelData=[];
            vm.collectionCategorySub=[];
            $scope.collectionCategoryArrayDataMain=DirectoryCollectionCategory.map(function(d){
                    if(!d.parentId)
                        return {id:d._id,label:d.codeValue};
            }).filter(function(d) {return d!=null;});
            var DirectoryCollectionCategoryObj=DirectoryCollectionCategory.map(function(d){
                    if(d.parentId)
                        return {id:d._id,label:d.codeValue,pid:d.parentId._id};
            }).filter(function(d) {return d!=null;});
            console.log("coll",DirectoryCollectionCategoryObj);
            $scope.$watch('vm.collectionCategorySub',function(n,o){
                var x=[];
                $scope.collectionCategoryArrayData=[];
                for(var i=0;i<n.length;i++)
                {
                    x[i]=DirectoryCollectionCategoryObj.map(function(d){
                        if(n[i].id==d.pid)
                            return d;
                    }).filter(function(d) {return d!=null;});
                    console.log(x);
                    $scope.collectionCategoryArrayData
         
                }
                $scope.collectionCategorySelData=[];
                for(var i=0;i<x.length;i++)
                {
                    $scope.collectionCategoryArrayData = $scope.collectionCategoryArrayData.concat(x[i]);
                    $scope.collectionCategorySelData=$scope.collectionCategorySelData.concat(x[i]);
                }
                $scope.collectionCategorySelData=$scope.collectionCategorySelData.filter(function(val){
                        for(var i=0;i<n.length;i++)
                        {
                            if(val.pid==n[i].id)
                            return val;
                        }
                    });
                //
            },true);
            $scope.aazzxx=[];
            $scope.$watch('vm.aazzxx',function(oval,nval){
              //  console.log(nval);
            });
            $scope.collectionCategorySelData=[];
            $scope.specificationTypeArray = DirectorySpecificationType;
            //$scope.IndustryPreferenceArray = DirectorySpecificationCodeForIndustryPreference;            
            
            $scope.IndustryPreferenceArray=DirectorySpecificationCodeForIndustryPreference.map(function(data){return {id:data.codeValue,label:data.codeValue}; }); 
//            console.log(" $scope.IndustryPreferenceArray ", $scope.IndustryPreferenceArray );
            $scope.GeographicalPreferenceArrray = DirectorySpecificationCodeForGeographicalPreference.map(function(data){return {id:data.codeValue,label:data.codeValue};});
            $scope.officerTypeArray = DirectoryOfficerType.map(function(data){return {id:data.codeValue,label:data.codeValue};});
            $scope.portfolioRelationshipArray = DirectorySpecificationCodeForPortfolioRelationship.map(function(data){return {id:data.codeValue,label:data.codeValue};});
            $scope.financingPreferredArray = DirectorySpecificationCodeForFinancingPreferred.map(function(data){return {id:data.codeValue,label:data.codeValue};});
            $scope.dealTypeArray = DirectorySpecificationCodeForDealType;
            $scope.firmWillTakeActiveRoleAsArray = DirectorySpecificationCodeForFirmWillTakeActiveRoleAs.map(function(data){return {id:data.codeValue,label:data.codeValue};});
        $scope.listingTypeArray = ListingType.map(function(data){return {id:data.codeValue,label:data.codeValue};});
            $scope.disbursalTypeArray = DisbursalType.map(function(data){return {id:data.codeValue,label:data.codeValue};});
            $scope.example15settings = {
                enableSearch: true,
                scrollableHeight: '250px',
                scrollable: true
            };
            $scope.InstitutionCategoryArrayDataSettings={
                groupByTextProvider: function(groupValue) { 
                    if (groupValue === '5763dc21c19ce14186492dc3') 
                         return 'ART'; 
                    if (groupValue === '5763dc21c19ce14186492ddf') 
                        return 'SCIENCE';
                    if (groupValue === '5763dc21c19ce14186492dcf')
                        return 'HISTORY';
                    if (groupValue === '5763dc21c19ce14186492df2')
                      return 'SPECIALIZED';
                    if(groupValue === '5763dc21c19ce14186492ddd')
                        return "CHILDREN'S MUSEUMS";
                    if(groupValue === '5763dc21c19ce14186492dc2')
                        return "COLLEGE AND UNIVERSITY MUSEUMS";
                    if(groupValue === '5763dc21c19ce14186492dcd')
                        return "EXHIBIT AREAS"
                    if(groupValue === '5763dc21c19ce14186492dce')
                        return "GENERAL MUSEUMS";
                    if(groupValue === '5763dc21c19ce14186492dc8')
                        return "COMPANY MUSEUMS";
                    if(groupValue === '5763dc21c19ce14186492dd9')
                        return "LIBRARIES HAVING COLLECTIONS OF BOOKS";
                    if(groupValue === '5763dc21c19ce14186492dda')
                        return "LIBRARIES HAVING COLLECTIONS OTHER THAN BOOKS";
                    if(groupValue === '5763dc21c19ce14186492ddb')
                        return "NATIONAL AND STATE AGENCIES, COUNCILS AND COMMISSIONS";
                    if(groupValue === '5763dc21c19ce14186492ddc')
                        return "NATURE CENTERS";
                    if(groupValue === '5763dc21c19ce14186492dde')
                        return "PARK MUSEUMS AND VISITOR CENTERS";
                    
                },
                enableSearch: true,
                scrollableHeight: '250px',
                scrollable: true,
                scrollableWidth:'100%',
                externalIdProp: ''
            }
            $scope.collectionCategoryArrayDatasettings={
                groupByTextProvider: function(groupValue) { 
                    if (groupValue === '5763dc21c19ce14186492e1e') 
                         return 'ART COLLECTIONS'; 
                    if (groupValue === '5763dc21c19ce14186492e19') 
                        return "ANTHROPOLOGY AND ARCHAEOLOGICAL COLLECTIONS";
                    if (groupValue === '5763dc21c19ce14186492e27')
                        return "HISTORICAL COLLECTIONS";
                    if (groupValue === '5763dc21c19ce14186492e31')
                      return "NATURAL COLLECTIONS";
                    
                },
                enableSearch: true,
                scrollableHeight: '250px',
                scrollable: true,
                scrollableWidth:'100%',
                externalIdProp: ''
            }
            $scope.example11settings = { groupByTextProvider: function(groupValue) { if (groupValue === 'M') { return 'Male'; } else { return 'Female'; } } };
            $scope.cityDatadata=[];
            $scope.customFilter = 'a';
            $scope.cityData = [];
            
            $scope.stateDatadata=[];
            $scope.stateName='';
            $scope.stateData=[];
            
            $scope.autocompletecity=function(data){
                   
            }
            $scope.arrayOfObjectsForIndex = {};
            $scope.totalDisplayed = 10;
            $scope.loadMore = function() {
                $scope.totalDisplayed += 10;
            };
            // $scope.initAddressCities = function() {
            //    // console.log("In initAddressCities");
            //     $("#auto-complete-city")
            //         .autocomplete({
            //             source: function(req, res) {
            //              //   console.log("\nreq :", req);
            //                 $scope.getAddressCities(req.term).then(function(data) {
            //                     //$scope.cityData=data;
            //                     $scope.cityData=data.map(function(data){
            //                         return {id:data.codeValue,label:data.codeValue};
            //                     });
            //                     res(data);
            //                 });
            //             },
            //             minLength: 2,
            //             focus: function(event, ui) {
            //                 $("#auto-complete-city").val(ui.item.codeValue);
            //                 return false;
            //             },
            //             select: function(evt, ui) {
            //                 $scope.selectAddressCityTypeAhead(ui.item, ui.item);
            //                 $('#auto-complete-city').val($scope.cityName);
            //                 $('#auto-complete-state').val($scope.stateName);
                          
            //                 return false;
            //             }
            //         })
            //         .autocomplete("instance")._renderItem = function(ul, item) {
            //             return $("<li>")
            //                 .append("<div ng-click='autocompletecity(item.codeValue)'>" + item.codeValue + "<p>" + (item.parentId ? (item.parentId.codeValue + " (" + item.parentId.description + ")") : "") + "<span>, " + (item.parentId && item.parentId.parentId ? item.parentId.parentId.codeValue : "") + "</span>" + "</p>" + "</div>")
            //                 .appendTo(ul);
            //         };
            // };
            $scope.getAddressCities = function(val) {
               // console.log("In getAddressCities");
                $('[ng-show="noResultsCity"]').html('<i class="glyphicon glyphicon-remove"></i> No Results Found');

                if (val.length > 1) {
                    return DirectoryCityService.list({
                        codeValue: {
                            _eval: 'regex',
                            value: val,
                        },
                        active: true
                    }).$promise;
                } else $('[ng-show="noResultsCity"]').html('Enter minimum 2 characters');
            };
                $scope.getAddressCities1 = function(val) {
                if (val.length > 1) {
                    DirectoryCityService.list({
                        codeValue: {
                            _eval: 'regex',
                            value: val,
                        },
                        active: true
                    }).then(function(data) {
                            $scope.cityData=data
                            .filter(function(v, i, a) {a.indexOf(v) === i})
                            .map(function(data){
                                return {id:data.codeValue,label:data.codeValue};
                                });
                            });
                } };
            $scope.$watch('cityName', function(newValue, oldValue) {
                var z1={
                    _where: {codeTypeId:'57174a5d6778ef1c153aec6f',codeValue: {_eval: 'regex',value: newValue},parentId:{'$ne':null},active: true},
                     _limit: 50
                };
                if(newValue!=="" && newValue!= undefined)
                $dataService.post('directory/city',z1,function(data){
                    var city=data.map(function(val){
                        return val.codeValue;
                    });
                    var city2=[];
                    for(var i=0;i<city.length;i++)
                    {
                            if(city2.indexOf(city[i])== -1)
                            {
                             city2.push(city[i]);
                            }
                    }
                    $scope.cityData=city2
                            .map(function(val){
                                return {id:val,label:val};                      
                                });

                              $( "#CityAuto" ).autocomplete({
                                  source:$scope.cityData,
                                   select: function( event, ui ) { 
                                    $("#CityAuto").val(ui.item.id);
                                    return false;
                                   }
                                });
                              
                            },function(error){
                        });
            
            });
            function stateGetData(newValue)
            {
                var arr=[];
                    arr.push({codeValue:{_eval: 'regex',value: newValue}});
                    arr.push({description:{_eval: 'regex',value: newValue}});

                // var z1={
                //     _where: {codeTypeId:'57174a4b6778ef1c153aec6e','$or':arr,active: true},
                //     _limit: 5000
                // };
                var z1={
                    _where: {"parentId":"5718d7abca75ca39db934831"},
                    _limit: 5000
                };
                 //{ "parentId": ObjectId("5718d7abca75ca39db934831") }
                if(newValue!=="" && newValue!= undefined)
                $dataService.post('directory/state',z1,function(data){
             //       $scope.dataCommonService.data=0;
                   // console.log(data);
                            $scope.stateData=data.map(function(data){
                                 return {id:data.codeValue,label:data.description?data.codeValue+" ("+data.description+")":data.codeValue};
                                    //return data.description?data.codeValue+" ("+data.description+")":data.codeValue;
                                });
                                $( "#StateAuto" ).autocomplete({
                                  source:$scope.stateData
                                });
                                //
                                     $( "#StateAuto" ).autocomplete({
                                  source:$scope.stateData,
                                   select: function( event, ui ) { 
                                 //   console.log('ui',ui);
                                    $("#StateAuto").val(ui.item.id);
                                    return false;
                                   }
                                });
                                //
                            },function(error){
                    //    console.log(error);
                        });

            }
            stateGetData('a');

                $scope.$watch('stateName1', function(newValue, oldValue) {
                 //    $scope.dataCommonService.data=0;
                 stateGetData(newValue);
 

            });
            /*$scope.stateDatadata=[];
            $scope.stateName='';
            $scope.stateData=[];*/
            
            $scope.selectAddressCityTypeAhead = function($item, $model, $label, $event) {
               // console.log("In selectAddressCityTypeAhead");
                $scope.cityName = $model.codeValue;
                $scope.stateName = $model.parentId ? $model.parentId.codeValue : "";
                // $scope.addDatacard.country = $model.parentId && $model.parentId.parentId ? $model.parentId.parentId.codeValue : "";
            };
            $scope.initAddressStates = function() {
               // console.log("In initAddressStates");
                $("#auto-complete-state")
                    .autocomplete({
                        source: function(req, res) {
                            $scope.getAddressStates(req.term).then(function(data) {
                                res(data);
                            });
                        },
                        focus: function(event, ui) {
                            $("#auto-complete-state").val(ui.item.codeValue + " (" + ui.item.description + ")");
                            return false;
                        },
                        minLength: 2,
                        select: function(evt, ui) {
                            $scope.selectAddressStateTypeAhead(ui.item, ui.item);
                            $('#auto-complete-state').val($scope.stateName);
                            // $('#auto-complete-country').val($scope.addDatacard.country);
                            return false;
                        }
                    })
                    .autocomplete("instance")._renderItem = function(ul, item) {
                        return $("<li>")
                            .append("<div>" + item.codeValue + " (" + item.description + ")" + "<span>, " + (item.parentId ? item.parentId.codeValue : "") + "</span>" + "</div>")
                            .appendTo(ul);
                    };
            };
            $scope.getAddressStates = function(val) {
             //   console.log("In getAddressStates");
                $('[ng-show="noResultsState"]').html('<i class="glyphicon glyphicon-remove"></i> No Results Found');
                if (val.length > 1) {
                    return DirectoryStateService.list({
                        codeValue: {
                            _eval: 'regex',
                            value: val
                        },
                        active: true
                    }).$promise;
                } else $('[ng-show="noResultsState"]').html('Enter minimum 2 characters');
            };
            $scope.selectAddressStateTypeAhead = function($item, $model, $label, $event) {
              //  console.log("In selectAddressStateTypeAhead");
                $scope.stateName = $model.codeValue;
                // $scope.addDatacard.country = $model.parentId.codeValue;
            };

            // setTimeout(function() {
            //     $scope.initAddressCities();
            //     $scope.initAddressStates();
            // }, 500);

        
            $scope.searchFunction =searchFunction;
            $scope.searchFunctionOMD =searchFunctionOMD;
            function searchFunctionOMD(){

                searchFunction()   
                $('html,body').animate({scrollTop: $("#gotoSearch").offset().top-50},'slow');
            }
            function searchFunction(){
                if($scope.t1!=undefined || $scope.t1=='')
                {
                    $scope.directMarketingBudget=$scope.t1;
                }
                if($scope.t2!=undefined || $scope.t2=='')
                {
                    $scope.directMarketingBudget+="-"+$scope.t2;
                }
              //  $("#searchData").show();
                $scope.searchData=1;
                var obj = {}; // Do not delete this field
                var obj1 = {};
                var obj2 = {};
                var obj3 = {};
                var obj6 = { $and: [] };

               // console.log("$stateParams.directoryName : ", $stateParams.directoryName);

                obj3 = {
                    "obj": obj,
                    "obj1": obj1,
                    "obj2": obj2,
                    "obj6": obj6
                };

                obj['active'] = true;
                obj['directoryId'] = $rootScope.currentDirectory.directoryId;
                if (obj.directoryId == '57189b3e24d8bc65f4123bbf')
                    obj['parentId'] = null;
                if (obj.directoryId == '57189cd924d8bc65f4123bc3') {
                    obj['name'] = { $exists: true };
                    obj['classificationCodeName'] = {
                        $in: [
                            "US Diocese", "World Diocese", "Religious Order/Miscellaneous",
                            "Curia", "Curia Organization", "Base/Generic Organization",
                            "Religious Order Organization", "Parish", "School", "Shrine",
                            "Church", "Hospital", "Personal Prelature", "Propogation of the Faith",
                            "Rectory", "Missionary Activities", "Cemetery", "Endowment",
                            "Diocese Religious Order"
                        ]
                    };
                }
                if($scope.nstitutionCategorySelData != undefined)
                if (($scope.nstitutionCategorySelData.length > 0 ) && ($scope.collectionCategorySelData.length > 0 )) {
                    var obj4 = {};
                    var obj5 = {};

                    obj = { $and: [] };
                    var  obj61 = { $or: [] };
                    for (var i = $scope.nstitutionCategorySelData.length - 1; i >= 0; i--) {
                        
                        var obj4 = {};
                        obj4['features.featureType'] = "CATEGORY";
                        obj4['features.codeName'] = $scope.nstitutionCategorySelData[i].label;
                        obj61['$or'].push(obj4);
                    }
                    obj['$and'].push(obj61);
                    var obj61 = { $or: [] };
                    for (var i = $scope.collectionCategorySelData.length - 1; i >= 0; i--) {
                        
                        var obj4 = {};
                        obj4['features.featureType'] = "CATEGORY";
                        obj4['features.codeName'] = $scope.collectionCategorySelData[i].label;

                        vm.dataOfData.push(obj4);
                        obj61['$or'].push(obj4);
                    }
                   // obj['$and'].push(obj61);
                    //obj['$or'].push(obj61);
                 //   console.log('obj61',obj61);

                } else {

                  //  console.log("nstitutionCategorySelData.length : ",$scope.nstitutionCategorySelData.length)
                   if ($scope.nstitutionCategorySelData)
                    if ($scope.nstitutionCategorySelData.length > 0 )  {

                        obj = { $or: [] };
                        for (var i = $scope.nstitutionCategorySelData.length - 1; i >= 0; i--) {
                            
                            var obj4 = {};
                            obj4['features.featureType'] = "CATEGORY";
                            obj4['features.codeName'] = $scope.nstitutionCategorySelData[i].label;
                            obj['$or'].push(obj4);
                            vm.dataOfData.push(obj4);
                        }

                        
                     //   console.log("obj",obj);

                    }
                    if ($scope.collectionCategorySelData.length > 0 )  {

                        obj = { $or: [] };
                        for (var i = $scope.collectionCategorySelData.length - 1; i >= 0; i--) {
                            
                            var obj4 = {};
                            obj4['features.featureType'] = "CATEGORY";
                            obj4['features.codeName'] = $scope.collectionCategorySelData[i].label;
                            obj['$or'].push(obj4);
                            vm.dataOfData.push(obj4);
                        }
                    }
                }
                if (($scope.name != undefined) && ($scope.name != '')) {
                    if ($scope.instituteNameAllFlag == true)
                    {
                        obj['name'] = $scope.name;
                        vm.dataOfData.push({name:{ _eval: 'regex', value: $scope.name }});
                        //vm.dataOfData.push({sortMajorName:$scope.name});
                    }
                    if($scope.instituteNameAllFlag == false)
                        {
                        //vm.dataOfData.push({name:{ _eval: 'regex', value: $scope.name }});
                        var obj=[];
                        var arrname=$scope.name.split(' ');
                        obj.push({ _eval: 'regex', value:$scope.name})
                        for(var i=0;i<arrname.length;i++)
                        {
                            if(arrname[i]!='')
                            {
                                var sname=arrname[i]+' ';
                                obj.push({ _eval: 'regex', value:sname})
                            }
                            
                        }
                        
                        vm.dataOfData.push({name:{'$in':obj}});

                        }

                    $scope.enable = false;
                }

                if (($scope.parentName != undefined) && ($scope.parentName != '')) {
                    if ($scope.instituteParentNameAllFlag == true)
                        obj['parentName'] = $scope.parentName;
                    else {
                        obj['parentName'] = { _eval: 'regex', value: $scope.parentName };
                    }

                    $scope.enable = false;
                }
    
              if (($scope.parentNameForAAD != undefined) && ($scope.parentNameForAAD != '')) {
                    obj['sub_nbr'] = '1';
                    obj['name'] = { _eval: 'regex', value: $scope.parentNameForAAD };
                   // vm.dataOfData.push({'name':{ _eval: 'regex', value: $scope.parentNameForAAD }});
                   vm.dataOfAnd['name']={ _eval: 'regex', value: $scope.parentNameForAAD };
                }
    
                if (($scope.entityType != undefined) && ($scope.entityType != '')) {
                    if ($scope.institutionDescriptionAllFlag == true)
                    {
                        obj['entityType'] = $scope.entityType;
                        vm.dataOfData.push({entityType:$scope.entityType})
                    }

                    else
                    {
                        obj['entityType'] = { _eval: 'regex', value: $scope.entityType };
                        vm.dataOfData.push({entityType:{ _eval: 'regex', value: $scope.entityType }})
                    }
                }

                /*if (($scope.cityName != undefined) && ($scope.cityName != '')) {
                    obj['address.cityName'] = { _eval: 'regex', value: $scope.cityName };
                }*/
                if($('#CityAuto').val()!='')
                {
                   vm.dataOfAnd['address.cityName']= $('#CityAuto').val();
                }
                if($('#StateAuto').val()!='')
                {
                   vm.dataOfAnd['address.stateName']= $('#StateAuto').val();
                }
                
                // if($scope.cityDatadata.length>0){

                //     obj = { $or: [] };
                //        vm.dataOfAnd['address.cityName']={'$in':[]};
                //     for (var i = $scope.cityDatadata.length - 1; i >= 0; i--) {
                //         var obj4 = {};
                //         obj4['address.cityName'] = { _eval: 'regex', value: $scope.cityDatadata[i].id};
                //        // vm.dataOfData.push(obj4);
                //        var str=$scope.cityDatadata[i].id;
                //        vm.dataOfAnd['address.cityName']['$in'].push(str.slice(0,str.indexOf('(')));
                //        // codeValue
                //        // vm.dataOfAnd['address.stateName']['$in'].push($scope.stateDatadata[i].id);

                //         obj['$or'].push(obj4);
                //      //   console.log($scope.cityDatadata[i].id); 
                //     }
                // }
                if ($scope.stateDatadata.length> 0) {
                    vm.dataOfAnd['address.stateName']={'$in':[]};
                    obj = { $or: [] };
                    for (var i = $scope.stateDatadata.length - 1; i >= 0; i--) {

                        var obj4 = {};
                        obj4['address.stateName'] = { _eval: 'regex', value: $scope.stateDatadata[i].id};
                        obj['$or'].push(obj4);
                         vm.dataOfAnd['address.stateName']['$in'].push($scope.stateDatadata[i].id);
//                        vm.dataOfData.push(obj4);
                    //    console.log($scope.stateDatadata[i].id); 
                    }
                   // vm.dataOfAnd['$and']=obj['$or'];

                }

                // if (($scope.stateName != undefined) && ($scope.stateName != ''))
                //     obj['address.stateName'] = { _eval: 'regex', value: $scope.stateName };

                if (($scope.freeText != undefined) && ($scope.freeText != '')) {
                    if ($scope.freeTextAllFlag == true)
                    {
                        obj['activities.freeText'] = $scope.freeText;
                        vm.dataOfData.push({'activities.freeText':$scope.freeText});
                    }
                    else
                    {
                        obj['activities.freeText'] = { _eval: 'regex', value: $scope.freeText };
                        vm.dataOfData.push({'activities.freeText':{ _eval: 'regex', value: $scope.freeText}});
                    }
                }

                if (($scope.establishedYear != undefined) && ($scope.establishedYear != ''))
                {
                    obj['established.year'] = { _eval: 'regex', value: $scope.establishedYear };
                     vm.dataOfAnd['established.year']={ _eval: 'regex', value: $scope.establishedYear };
                }

                if (($scope.zip != undefined) && ($scope.zip != ''))
                {
                    obj['address.zip'] = { _eval: 'regex', value: $scope.zip };
                    vm.dataOfAnd['address.zip']={ _eval: 'regex', value: $scope.zip };
                   // vm.dataOfAnd['address.zip']=$scope.zip;
                }

                if (($scope.aamId != undefined) && ($scope.aamId != false))
                {
                    obj['flags.aam'] = $scope.aamId;
                  //  vm.dataOfData.push();
                  vm.dataOfAnd['flags.aam']=true;
                }

                if (($scope.accredited != undefined) && ($scope.accredited != false))
                {
                    obj['flags.accredited'] = $scope.accredited;
                    //vm.dataOfData.push({'flags.accredited':true});
                    
                    vm.dataOfAnd['flags.accredited']=true;
                }

                if (($scope.icom != undefined) && ($scope.icom != false)){
                    obj['flags.icom'] = $scope.icom;
                     //vm.dataOfData.push({'flags.icom':true});
                     
                     vm.dataOfAnd['flags.icom']=true;
                }

                if (($scope.handicappedFlag != undefined) && ($scope.handicappedFlag != false))
        {
                    obj['flags.handicapped'] = $scope.handicappedFlag;
                    vm.dataOfAnd['flags.handicapped']=true;   
                }
    if (($scope.scholarship != undefined) && ($scope.scholarship != false))
    {
        obj['artSchool.scholarship'] = $scope.scholarship;
//        vm.dataOfData.push({'artSchool.scholarship':$scope.scholarship});
        vm.dataOfAnd['artSchool.scholarship']=$scope.scholarship;
    }
    if (($scope.fellowship != undefined) && ($scope.fellowship != false)){
        obj['artSchool.fellowships'] = $scope.fellowship;
      //vm.dataOfData.push({'artSchool.fellowships':$scope.fellowship});
      vm.dataOfAnd['artSchool.fellowships']=$scope.fellowship;
    }
    
    if (($scope.assistantship != undefined) && ($scope.assistantship != false)){
        obj['artSchool.assistantships'] = $scope.assistantship;
      //  vm.dataOfData.push({'artSchool.assistantships':$scope.assistantship});
      vm.dataOfAnd['artSchool.assistantships']=$scope.assistantship;
    }
    if (($scope.grants != undefined) && ($scope.grants != false))
    {
            obj['artSchool.grants'] = $scope.grants;
           // vm.dataOfData.push({'artSchool.grants':$scope.grants});
           vm.dataOfAnd['artSchool.grants']=$scope.grants;
    }

                if (($scope.attendanceCount != undefined) && ($scope.attendanceCount != '' && ($rootScope.currentDirectory.directoryId=='57189c7024d8bc65f4123bc0')))
                    //
                {
                    var num=strtonumber($scope.attendanceCount);
                    obj['attendance.countNumber'] = $scope.attendanceCount;
                 //   vm.dataOfData.push({'attendance.countNumber':$scope.attendanceCount})
               //  vm.dataOfAnd['attendance.countNumber']=$scope.attendanceCount;
                }

                if ((vm.attendanceMethod != undefined) && (vm.attendanceMethod != ''))
                {
                    if(vm.attendanceMethod=='E')
                    {
                        /*'attendance.method':'E',*/
                        var gt=parseFloat(strtonumber(vm.attendanceCount));
                        var lt=parseFloat(strtonumber(vm.attendanceCounts));
                        vm.dataOfData.push({'attendance.countNumber':{'$gt':gt,'$lt':lt}});
                    }
                    if(vm.attendanceMethod=='A')
                    {
                        //obj['attendance.method'] = { _eval: 'regex', value: vm.attendanceMethod };
                        var gt=parseFloat(strtonumber($scope.attendanceCount));

                        vm.dataOfData.push({'attendance.countNumber':{'$gt':gt}});
                    }
                    if(vm.attendanceMethod=='lt')
                    {
                        //obj['attendance.method'] = { _eval: 'regex', value: vm.attendanceMethod };
                        var lt=parseFloat(strtonumber($scope.attendanceCount));
                        vm.dataOfData.push({'attendance.countNumber':{'$lt':lt}});
                    }
                    if(vm.attendanceMethod=='eq')
                    {
                        //obj['attendance.method'] = { _eval: 'regex', value: vm.attendanceMethod };
                        var eq=parseFloat(strtonumber($scope.attendanceCount));
                        vm.dataOfData.push({'attendance.countNumber':{'$eq':eq}});
                    }
                    
                }
                
                

                if (($scope.collectionDescription != undefined) && ($scope.collectionDescription != '')) {
                    obj['features.featureType'] = "COLLECTION";
                    if ($scope.collectionDescriptionAllFlag == true){
                        vm.dataOfData.push({'features.featureType':'COLLECTION','features.name':$scope.collectionDescription});
                        obj['features.name'] = $scope.collectionDescription;
                    }
                    else{
                        vm.dataOfData.push({'features.featureType':'COLLECTION','features.name':{ _eval: 'regex', value: $scope.collectionDescription }});
                        obj['features.name'] = { _eval: 'regex', value: $scope.collectionDescription };
                        vm.dataOfData.push({'features.name':{ _eval: 'regex', value: $scope.collectionDescription }});
                    }
                }
                if (($scope.conductBusiness != undefined) && ($scope.conductBusiness != ''))
                {
                    if($scope.conductBusinessbFlag==true)
                    {
                    obj['conductBusiness'] =$scope.conductBusiness;
                    vm.dataOfAnd['conductBusiness']=$scope.conductBusiness;
                    }
                    else
                    {
                    obj['conductBusiness'] = { _eval: 'regex', value: $scope.conductBusiness };
                    vm.dataOfAnd['conductBusiness']={ _eval: 'regex', value: $scope.conductBusiness };    
                  }                       
                }

                 

                if (($scope.activitiesEducationDept != undefined) && ($scope.activitiesEducationDept != ''))
                {

                    if($scope.activitiesEducationDeptbFlag==true)
                    {
                    obj['activities.educationDept'] =$scope.activitiesEducationDept;
                    vm.dataOfAnd['activities.educationDept']=$scope.activitiesEducationDept;
                    }
                    else
                    {
                    obj['activities.educationDept'] = { _eval: 'regex', value: $scope.activitiesEducationDept };
                    vm.dataOfAnd['activities.educationDept']={ _eval: 'regex', value: $scope.activitiesEducationDept };
                    }

                }

                if (($scope.directMarketingBudget != undefined) && ($scope.directMarketingBudget != ''))
                    
                {
                    if($scope.directMarketingBudgetFlag!=undefined)
                    {
                        vm.dataOfAnd['directMarketingBudget']=$scope.directMarketingBudget;
                    }else
                    {
                          obj['directMarketingBudget'] = { _eval: 'regex', value: $scope.directMarketingBudget };
                  //  vm.dataOfData.push({'directMarketingBudget':{ _eval: 'regex', value: $scope.directMarketingBudget }});
                          vm.dataOfAnd['directMarketingBudget']={ _eval: 'regex', value: $scope.directMarketingBudget };
                    }
                  
                }

                // if (($scope.totalAdvertisingBudget != undefined) && ($scope.totalAdvertisingBudget != ''))                    
                // {
                //     obj['totalAdvertisingBudget'] = { _eval: 'regex', value: $scope.totalAdvertisingBudget };
                //   vm.dataOfAnd['totalAdvertisingBudget']={ _eval: 'regex', value: $scope.totalAdvertisingBudget };
                // }
                if(vm.totalAdvertisingBudgeMethod=='A'){
                    vm.dataOfAnd['totalAdvBudgetAmount']={$gt:Number(vm.totalAdvertisingBudget1) };
                }
                if(vm.totalAdvertisingBudgeMethod=='E')
                {
                    vm.dataOfAnd['totalAdvBudgetAmount']={$gt:Number(vm.totalAdvertisingBudget1),$lt:Number(vm.totalAdvertisingBudget2)};
                }


                if (($scope.establishedDetail != undefined) && ($scope.establishedDetail != ''))
                {
                    if($scope.establishedDetailbFlag==true){
                    obj['established.detail'] =$scope.establishedDetail;
                    vm.dataOfAnd['established.detail']=$scope.establishedDetail;
                    }else{
                    obj['established.detail'] = { _eval: 'regex', value: $scope.establishedDetail };
                    vm.dataOfAnd['established.detail']={ _eval: 'regex', value: $scope.establishedDetail };
                    }
              
                }

                if (($scope.catalogOnline == true) && (($scope.onlineSales != undefined) && ($scope.onlineSales != '')))
                {
                    obj['onlineSales'] = $scope.onlineSales;
                  //  vm.dataOfData.push({'onlineSales':$scope.onlineSales});
                  vm.dataOfAnd['onlineSales']=$scope.onlineSales;
                }

                if (($scope.vendorsName != undefined) && ($scope.vendorsName != ''))
                {
                    obj['vendors.name'] = { _eval: 'regex', value: $scope.vendorsName };
                   // vm.dataOfData.push({'vendors.name':{ _eval: 'regex', value: $scope.vendorsName }});
                   vm.dataOfAnd['vendors.name']={ _eval: 'regex', value: $scope.vendorsName };
                }

                if (($scope.degreeGranted != undefined) && ($scope.degreeGranted != ''))
                {
                    obj['artSchool.degreeGranted'] = { _eval: 'regex', value: $scope.degreeGranted };
                   // vm.dataOfData.push({'artSchool.degreeGranted':{ _eval: 'regex', value: $scope.degreeGranted }});
                   vm.dataOfAnd['artSchool.degreeGranted']={ _eval: 'regex', value: $scope.degreeGranted };
                }

                if (($scope.galleryDescription != undefined) && ($scope.galleryDescription != ''))
                {
                    obj['galleryDescription'] = { _eval: 'regex', value: $scope.galleryDescription };
                   // vm.dataOfData.push({'galleryDescription':{ _eval: 'regex', value: $scope.galleryDescription }});
                   if($scope.galleryDescriptionFlag==true)
                    vm.dataOfAnd['galleryDescription']=$scope.galleryDescription;
                else
                   vm.dataOfAnd['galleryDescription']={ _eval: 'regex', value: $scope.galleryDescription };
                }

                if (($scope.employees != undefined) && ($scope.employees != '')){
                    obj['employees'] = { _eval: 'regex', value: $scope.employees };
                   // vm.dataOfData.push({'employees':{ _eval: 'regex', value: $scope.employees }});
                   vm.dataOfAnd['employees']={ _eval: 'regex', value: $scope.employees };
                }

                // if (($scope.listingType.codeVal.codeValue != undefined) && ($scope.listingType.codeVal.codeValue != "Select")) {
                //     obj['listingType.listingName'] = $scope.listingType.codeVal.codeValue;
                // }
                if($scope.listingType.length > 0)
                {
                       vm.dataOfAnd['listingType.listingName']={'$in':[]};                   
                       for (var i = $scope.listingType.length - 1; i >= 0; i--) 
                       {
                       vm.dataOfAnd['listingType.listingName']['$in'].push($scope.listingType[i].id);
                        }}
            
             // if (($scope.disbursalType.codeVal.codeValue != undefined) && ($scope.disbursalType.codeVal.codeValue != "Select")) {
             //        obj['directMarketingBudgetDisbursal.codeName'] = $scope.disbursalType.codeVal.codeValue;
             //    }
                 if($scope.disbursalType.length > 0)
                {
                       vm.dataOfAnd['directMarketingBudgetDisbursal.codeName']={'$in':[]};                   
                       for (var i = $scope.disbursalType.length - 1; i >= 0; i--) 
                       {
                       vm.dataOfAnd['directMarketingBudgetDisbursal.codeName']['$in'].push($scope.disbursalType[i].id);
                        }
                    }


                /***************************************** Obj 1 for Personnel *******************************************************************/
                if (($scope.firstName != undefined) && ($scope.firstName != '')) {
                    obj1['directoryId'] = $rootScope.currentDirectory.directoryId;
                    obj1['name.first'] = { _eval: 'regex', value: $scope.firstName };

                }

                if (($scope.lastName != undefined) && ($scope.lastName != '')) {
                    obj1['directoryId'] = $rootScope.currentDirectory.directoryId;
                    obj1['name.last'] = { _eval: 'regex', value: $scope.lastName };
                    // obj1['directoryId'] = $rootScope.currentDirectory.directoryId;

                }
                //     if (($scope.officerType.codeVal.codeValue != undefined) && ($scope.officerType.codeVal.codeValue != "Select")) 
                //     {
                //     obj1['directoryId'] = $rootScope.currentDirectory.directoryId;
                //     obj1['officers.OfficerTypeName'] = $scope.officerType.codeVal.codeValue;
                //     vm.dataOfAnd['officers.OfficerTypeName']=$scope.officerType.codeVal.codeValue;
                // }
                    if ($scope.officerType.length>0) 
                    {
                         vm.dataOfAnd['officers.OfficerTypeName']={'$in':[]};
                    
                       for (var i = $scope.officerType.length - 1; i >= 0; i--) 
                       {
                       vm.dataOfAnd['officers.OfficerTypeName']['$in'].push($scope.officerType[i].id);
                        }
                    }

                /***************************************** Obj 2 for Exhibition*********************************************************************/
                if (($scope.travelFlag != undefined) && ($scope.travelFlag != '')) {
                    obj2['travelFlag'] = { _eval: 'regex', value: $scope.travelFlag };
                    obj2['directoryId'] = $rootScope.currentDirectory.directoryId;
                }
             if (($scope.exhibitionDescription != undefined) && ($scope.exhibitionDescription != '')) {
                    obj2['exhibitName'] = { _eval: 'regex', value: $scope.exhibitionDescription };
                    obj2['directoryId'] = $rootScope.currentDirectory.directoryId;
                    //vm.dataOfData.push()
                     vm.dataOfAnd['exhibitName']={ _eval: 'regex', value: $scope.exhibitionDescription };
                }
                /*************************** Obj 6 for Chapter Specification*********************************************************************/

                if (($scope.fundsAvailabel != undefined) && ($scope.fundsAvailabel != '')) {
                    var obj8 = {};
                    obj8['specificationType'] = "FUNDS AVAILABLE FOR INVESTMENTS OR LOANS";
                    obj8['name'] = { _eval: 'regex', value: $scope.fundsAvailabel };
                    obj6['$and'].push(obj8);

                }
                if (($scope.firmPrefersNotToInvestIn != undefined) && ($scope.firmPrefersNotToInvestIn != '')) {
                    var obj8 = {};
                    obj8['specificationType'] = "FIRM PREFERS NOT TO INVEST IN";
                    obj8['name'] = { _eval: 'regex', value: $scope.firmPrefersNotToInvestIn };
                    obj6['$and'].push(obj8);

                }
                if (($scope.minimumOperatingData != undefined) && ($scope.minimumOperatingData != '')) {
                    var obj8 = {};
                    obj8['specificationType'] = "MINIMUM OPERATING DATA REQUIRED TO CONSIDER FINANCING";
                    obj8['name'] = { _eval: 'regex', value: $scope.minimumOperatingData };
                    obj6['$and'].push(obj8);

                }
                if (($scope.geographicalPreferencesAroundTheWorld != undefined) && ($scope.geographicalPreferencesAroundTheWorld != '')) {
                    var obj8 = {};
                    obj8['specificationType'] = "INTERNATIONAL GEOGRAPHICAL PREFERENCES";
                    obj8['name'] = { _eval: 'regex', value: $scope.geographicalPreferencesAroundTheWorld };
                    obj6['$and'].push(obj8);

                }
                if (($scope.exitCriteria != undefined) && ($scope.exitCriteria != '')) {
                    var obj8 = {};
                    obj8['specificationType'] = "EXIT CRITERIA";
                    obj8['name'] = { _eval: 'regex', value: $scope.exitCriteria };
                    obj6['$and'].push(obj8);

                }
                if (($scope.investmentPortfolioSize != undefined) && ($scope.investmentPortfolioSize != '')) {
                    var obj8 = {};
                    obj8['specificationType'] = "INVESTMENT PORTFOLIO SIZE";
                    obj8['name'] = { _eval: 'regex', value: $scope.investmentPortfolioSize };
                    obj6['$and'].push(obj8);

                }
                if (($scope.minimumSizeInvestment != undefined) && ($scope.minimumSizeInvestment != '')) {
                    var obj8 = {};
                    obj8['specificationType'] = "MINIMUM SIZE INVESTMENT";
                    obj8['name'] = { _eval: 'regex', value: $scope.minimumSizeInvestment };
                    obj6['$and'].push(obj8);

                }
                if (($scope.preferredSizeInvestment != undefined) && ($scope.preferredSizeInvestment != '')) {
                    var obj8 = {};
                    obj8['specificationType'] = "PREFERRED SIZE INVESTMENT";
                    obj8['name'] = { _eval: 'regex', value: $scope.preferredSizeInvestment };
                    obj6['$and'].push(obj8);

                }
                if (($scope.maximumSizeInvestmentInOneCompany != undefined) && ($scope.maximumSizeInvestmentInOneCompany != '')) {
                    var obj8 = {};
                    obj8['specificationType'] = "MAXIMUM SIZE INVESTMENT IN ONE COMPANY";
                    obj8['name'] = { _eval: 'regex', value: $scope.maximumSizeInvestmentInOneCompany };
                    obj6['$and'].push(obj8);

                }
                if (($scope.averageNumberOfDealsCompletedAnnually != undefined) && ($scope.averageNumberOfDealsCompletedAnnually != '')) {
                    var obj8 = {};
                    obj8['specificationType'] = "AVERAGE NUMBER OF DEALS COMPLETED ANNUALLY";
                    obj8['name'] = { _eval: 'regex', value: $scope.averageNumberOfDealsCompletedAnnually };
                    obj6['$and'].push(obj8);

                }
                if (($scope.averageAmountInvestedAnnually != undefined) && ($scope.averageAmountInvestedAnnually != '')) {
                    var obj8 = {};
                    obj8['specificationType'] = "AVERAGE AMOUNT INVESTED ANNUALLY";
                    obj8['name'] = { _eval: 'regex', value: $scope.averageAmountInvestedAnnually };
                    obj6['$and'].push(obj8);

                }
                if (($scope.mainSourcesOfCapital != undefined) && ($scope.mainSourcesOfCapital != '')) {
                    var obj8 = {};
                    obj8['specificationType'] = "MAIN SOURCES OF CAPITAL";
                    obj8['name'] = { _eval: 'regex', value: $scope.mainSourcesOfCapital };
                    obj6['$and'].push(obj8);

                }
                // if (($scope.industryPreferences.codeVal.codeValue != undefined) && ($scope.industryPreferences.codeVal.codeValue != "Select")) {
                //     var obj8 = {};
                //     obj8['specificationType'] = "INDUSTRY PREFERENCE";
                //     obj8['codeName'] = $scope.industryPreferences.codeVal.codeValue;
                //     obj6['$and'].push(obj8);

                // }
                    if($scope.industryPreferences)
                    if ($scope.industryPreferences.length>0) 
                    {
                        if(vm.dataOfAnd['specificationType'] == undefined)
                        {
                            vm.dataOfAnd['specificationType']={'$in':[]};
                             vm.dataOfAnd['specificationType']['$in'].push("INDUSTRY PREFERENCE");
                        }
                        else
                            vm.dataOfAnd['specificationType']['$in'].push("INDUSTRY PREFERENCE");

                        if(vm.dataOfAnd['codeName'] == undefined)
                        {
                                vm.dataOfAnd['codeName']={'$in':[]};
                        }
                        for (var i = $scope.industryPreferences.length - 1; i >= 0; i--) 
                        {
                            vm.dataOfAnd['codeName']['$in'].push($scope.industryPreferences[i].id);
                        }
                    }
                // if (($scope.geographicalPreference.codeVal.codeValue != undefined) && ($scope.geographicalPreference.codeVal.codeValue != "Select")) {
                //     var obj8 = {};
                //     obj8['specificationType'] = "GEOGRAPHICAL PREFERENCES";
                //     obj8['codeName'] = $scope.geographicalPreference.codeVal.codeValue;
                //     obj6['$and'].push(obj8);

                // }
                // if ($scope.geographicalPreference.length>0) 
                //     {
                //         if(vm.dataOfAnd['specificationType'] == undefined)
                //         {
                //             vm.dataOfAnd['specificationType']={'$in':[]};
                //             vm.dataOfAnd['specificationType']['$in'].push("GEOGRAPHICAL PREFERENCES");
                //         }
                //         else
                //             vm.dataOfAnd['specificationType']['$in'].push("GEOGRAPHICAL PREFERENCES");

                //         if(vm.dataOfAnd['codeName'] == undefined)
                //         {
                //                 vm.dataOfAnd['codeName']={'$in':[]};
                //         }
                //         for (var i = $scope.geographicalPreference.length - 1; i >= 0; i--) 
                //         {
                //             vm.dataOfAnd['codeName']['$in'].push($scope.geographicalPreference[i].id);
                //         }
                //     }
                // if (($scope.firmWillTakeActiveRoleAs.codeVal.codeValue != undefined) && ($scope.firmWillTakeActiveRoleAs.codeVal.codeValue != "Select")) {
                //     var obj8 = {};
                //     obj8['specificationType'] = "FIRM WILL TAKE ACTIVE ROLE AS";
                //     obj8['codeName'] = $scope.firmWillTakeActiveRoleAs.codeVal.codeValue;
                //     obj6['$and'].push(obj8);

                // }
                    if ($scope.firmWillTakeActiveRoleAs.length>0) 
                    {
                        if(vm.dataOfAnd['specificationType'] == undefined)
                        {
                            vm.dataOfAnd['specificationType']={'$in':[]};
                            vm.dataOfAnd['specificationType']['$in'].push("FIRM WILL TAKE ACTIVE ROLE AS");
                        }
                        else
                            vm.dataOfAnd['specificationType']['$in'].push("FIRM WILL TAKE ACTIVE ROLE AS");

                        if(vm.dataOfAnd['codeName'] == undefined)
                        {
                                vm.dataOfAnd['codeName']={'$in':[]};
                        }
                        for (var i = $scope.firmWillTakeActiveRoleAs.length - 1; i >= 0; i--) 
                        {
                            vm.dataOfAnd['codeName']['$in'].push($scope.firmWillTakeActiveRoleAs[i].id);
                        }
                    }
                // if (($scope.portfolioRelationship.codeVal.codeValue != undefined) && ($scope.portfolioRelationship.codeVal.codeValue != "Select")) {
                //     var obj8 = {};
                //     obj8['specificationType'] = "PORTFOLIO RELATIONSHIP";
                //     obj8['codeName'] = $scope.portfolioRelationship.codeVal.codeValue;
                //     obj6['$and'].push(obj8);

                // }
                    if ($scope.portfolioRelationship.length>0) 
                    {
                        if(vm.dataOfAnd['specificationType'] == undefined)
                        {
                            vm.dataOfAnd['specificationType']={'$in':[]};
                            vm.dataOfAnd['specificationType']['$in'].push("PORTFOLIO RELATIONSHIP");
                        }
                        else
                            vm.dataOfAnd['specificationType']['$in'].push("PORTFOLIO RELATIONSHIP");

                        if(vm.dataOfAnd['codeName'] == undefined)
                        {
                                vm.dataOfAnd['codeName']={'$in':[]};
                        }
                        for (var i = $scope.portfolioRelationship.length - 1; i >= 0; i--) 
                        {
                            vm.dataOfAnd['codeName']['$in'].push($scope.portfolioRelationship[i].id);
                        }
                    }
                // if (($scope.financingPreferred.codeVal.codeValue != undefined) && ($scope.financingPreferred.codeVal.codeValue != "Select")) {
                //     var obj8 = {};
                //     obj8['specificationType'] = "FINANCING PREFERRED";
                //     obj8['codeName'] = $scope.financingPreferred.codeVal.codeValue;
                //     obj6['$and'].push(obj8);
                // }
                    if ($scope.financingPreferred.length>0) 
                    {
                        if(vm.dataOfAnd['specificationType'] == undefined)
                        {
                            vm.dataOfAnd['specificationType']={'$in':[]};
                            vm.dataOfAnd['specificationType']['$in'].push("FINANCING PREFERRED");
                        }
                        else
                            vm.dataOfAnd['specificationType']['$in'].push("FINANCING PREFERRED");

                        if(vm.dataOfAnd['codeName'] == undefined)
                        {
                                vm.dataOfAnd['codeName']={'$in':[]};
                        }
                        for (var i = $scope.financingPreferred.length - 1; i >= 0; i--) 
                        {
                            vm.dataOfAnd['codeName']['$in'].push($scope.financingPreferred[i].id);
                        }
                    }

             //   console.log("obj3 :", obj3);
              //  console.log("obj6 :", obj6);
             //   console.log("Object.keys(obj6) lent :", Object.keys(obj6.$and).length);

            //obj3=vm.dataOfAnd;
                if ($scope.travelFlag == "Y") {

                    if (($scope.firstName != undefined) && ($scope.firstName != '') || ($scope.lastName != undefined) && ($scope.lastName != '')) {

                        obj['personnel'] = true;
                        // start
                        var promise = $resource(APIBasePath + 'subscriber/search/exhibition/list', {}, {
                            dataTable: {
                                url: APIBasePath + 'subscriber/search/exhibition/list',
                                method: 'POST',
                                transformRequest: function(data, headerGetter) {
                                    var headers = headerGetter();
                                    var newData = {
                                        dataTableQuery: data,
                                        conditions: obj3
                                    }
                                    return JSON.stringify(newData);
                                }
                            }
                        });
                        initDatatable(promise.dataTable);
                        return promise;
                        //end

                    } else 
                    {
                        obj['personnel'] = false;
                        var obj3={eFlag:true,
                        org:vm.dataOfAnd,
                        exhi:{"exhibition.travelFlag":'Y'}};
                        obj3.org.directoryId='57189b3e24d8bc65f4123bbf';
                        vm.dataOfAnd={};
                        var promise = $resource(APIBasePath + 'subscriber/search/exhibition/list', {}, {
                            dataTable: {
                                url: APIBasePath + 'subscriber/search/exhibition/list',
                                method: 'POST',
                                transformRequest: function(data, headerGetter) {
                                    var headers = headerGetter();
                                    var newData = {
                                        dataTableQuery: data,
                                        conditions: obj3
                                    }
                                    return JSON.stringify(newData);
                                }
                            }
                        });
                        initDatatable(promise.dataTable);
                        return promise;

                    }
                    // {
                    //     obj['personnel'] = false;

                    //     var promise = $resource(APIBasePath + 'subscriber/search/exhibition/list', {}, {
                    //         dataTable: {
                    //             url: APIBasePath + 'subscriber/search/exhibition/list',
                    //             method: 'POST',
                    //             transformRequest: function(data, headerGetter) {
                    //                 var headers = headerGetter();
                    //                 var newData = {
                    //                     dataTableQuery: data,
                    //                     conditions: obj3
                    //                 }
                    //                 return JSON.stringify(newData);
                    //             }
                    //         }
                    //     });
                    //     initDatatable(promise.dataTable);
                    //     return promise;

                    // }

                } else {

                    if (($scope.firstName != undefined) && ($scope.firstName != '') || ($scope.lastName != undefined) && ($scope.lastName != '')) {

                        if (Object.keys(obj6.$and).length > 0) {
                         //   console.log("In chapter-specification of first name last name");
                            obj['personnel'] = true;
                            obj6['directoryId'] = $rootScope.currentDirectory.directoryId;
                            var promise = $resource(APIBasePath + 'subscriber/search/chapter-specification', {}, {
                                dataTable: {
                                    url: APIBasePath + 'subscriber/search/chapter-specification',
                                    method: 'POST',
                                    transformRequest: function(data, headerGetter) {
                                        var headers = headerGetter();
                                        var newData = {
                                            dataTableQuery: data,
                                            conditions: obj3
                                        }
                                        return JSON.stringify(newData);
                                    }
                                }
                            });
                            initDatatable(promise.dataTable);
                            return promise;

                        } else {   
                      obj3.obj1.deleted=false;                  
                            var promise = $resource(APIBasePath + 'subscriber/search/personnel/list', {}, {
                                dataTable: {
                                    url: APIBasePath + 'subscriber/search/personnel/list',
                                    method: 'POST',
                                    transformRequest: function(data, headerGetter) {
                                        var headers = headerGetter();
                                        var newData = {
                                            dataTableQuery: data,
                                            conditions: obj3
                                        }
                                        return JSON.stringify(newData);
                                    }
                                }
                            }); 
                            initDatatable(promise.dataTable);
                            return promise;
                        }
                    } else {

                        if (Object.keys(obj6.$and).length > 0) {
                         //   console.log("In chapter-specification not in firstName lastName");
                            obj['personnel'] = false;
                            obj6['directoryId'] = $rootScope.currentDirectory.directoryId;
                            var promise = $resource(APIBasePath + 'subscriber/search/chapter-specification', {}, {
                                dataTable: {
                                    url: APIBasePath + 'subscriber/search/chapter-specification',
                                    method: 'POST',
                                    transformRequest: function(data, headerGetter) {
                                        var headers = headerGetter();
                                        var newData = {
                                            dataTableQuery: data,
                                            conditions: obj3
                                        }
                                        return JSON.stringify(newData);
                                    }
                                }
                            });
                            initDatatable(promise.dataTable);
                            return promise;
                        } else {
                      //      $scope.dataCommonService.data=1;
                           //vm.dataOfAnd['directoryId']='57189b3e24d8bc65f4123bbf';
                           vm.dataOfAnd['directoryId']=$rootScope.currentDirectory.directoryId;
                            if(vm.dataOfData.length > 0)
                            vm.dataOfAnd['$or']=vm.dataOfData;
                            var arr =angular.copy(vm.dataOfAnd);
                            vm.dataOfData=[];
                            vm.dataOfAnd={};
                            arr.name={$ne:null};
                            if($scope.instituteNameAllFlag)
                            {
                              // arr.mname=$scope.name;//
                              arr.mname={ _eval: 'regex', value:$scope.name};
                            }
                         //   console.log("In plain Organization",JSON.stringify(arr));

                         if("57189cc224d8bc65f4123bc1"==$rootScope.currentDirectory.directoryId)
                         {
                            if(arr.specificationType!= undefined){
                            if((arr.specificationType.$in.length ==0) && (arr.codeName.$in.length ==0))
                            {
                                arr={};
                                arr.directoryId='57189cc224d8bc65f4123bc1';
                                arr.name={$ne: null};
                            }
                            }
                
                         }
                            var promise = $resource(APIBasePath + 'subscriber/search/org-list', {}, {
                                dataTable: {
                                    url: APIBasePath + 'subscriber/search/org-list',
                                    method: 'POST',
                                    transformRequest: function(data, headerGetter) {
                                        var headers = headerGetter();
                                        var newData = {
                                            dataTableQuery: data,
                                            conditions: arr,
                                        }

                                        return JSON.stringify(newData);
                                    }
                                }
                            });
                            initDatatable(promise.dataTable);
                           
                            return promise;
                        //    $scope.dataCommonService.data=0;
                        }
                    }
                }
             //  $("#searchData").show();

            };
            $scope.keywordSearchFunction =keywordSearchFunction;

            $scope.keywordSearchFunctionOMD=keywordSearchFunctionOMD;

            $scope.keywordSearchFunctionOMDGotoRes=function(){
                    keywordSearchFunctionOMD();
                    $('html,body').animate({scrollTop: $("#gotoSearch").offset().top-50},'slow');
            };

            function keywordSearchFunctionOMD(){
            var searchTempObj=[];
            if($scope.keyword!=undefined && $scope.keyword!="")
            {
                debugger;
                  for(var i=0;i<$scope.omdKeywordSearchModel.length;i++)
            {
            var x=$scope.omdKeywordSearchModel[i].id;
                if($scope.keyword)
                // if($scope.omdKeywordSearchModel[i].id=='name')
                // {
                //     if(!$scope.keywordSearchAllFlag)
                //     {
                //     var y={};
                //     y.name=$scope.keyword;
                //     searchTempObj.push(y);
                //     }    
                //     else
                //     {
                //         var y={};
                //         y.name={ _eval: 'regex', value: $scope.keyword };
                //         searchTempObj.push(y);
                //     }
                // }
                // if($scope.omdKeywordSearchModel[i].id=='membership')
                // {
                //     if(!$scope.keywordSearchAllFlag)
                //     {
                //     var y={};
                //     y.membership=$scope.keyword;
                //     searchTempObj.push(y);
                //     }    
                //     else
                //     {
                //         var y={};
                //         y.membership={ _eval: 'regex', value: $scope.keyword };
                //         searchTempObj.push(y);
                //     }
                // }
                if($scope.omdKeywordSearchModel[i].id!='facilities')
                {
                    if(!$scope.keywordSearchAllFlag)
                    {
                    var y={};
                    y[x]=$scope.keyword;
                    searchTempObj.push(y);
                    }    
                    else
                    {
                        var y={};
                        y[x]={ _eval: 'regex', value: $scope.keyword };
                        searchTempObj.push(y);
                    }
                }
                 if($scope.omdKeywordSearchModel[i].id=='facilities')
                {
                    var y={};
                    y["features.featureType"]="FACILITY";
                    if(!$scope.keywordSearchAllFlag)
                    y['features.name']=$scope.keyword;
                   else
                     y['features.name']={ _eval: 'regex', value: $scope.keyword };
                    searchTempObj.push(y);
                }
            }
           var obj3={eFlag:true,
                        org:{directoryId:'57189b3e24d8bc65f4123bbf'},
                        exhi:{"$or":searchTempObj}
                    };
                    searchTempObj={}
                        var promise = $resource(APIBasePath + 'subscriber/search/exhibition/list', {}, {
                            dataTable: {
                                url: APIBasePath + 'subscriber/search/exhibition/list',
                                method: 'POST',
                                transformRequest: function(data, headerGetter) {
                                    var headers = headerGetter();
                                    var newData = {
                                        dataTableQuery: data,
                                        //conditions: obj3
                                        conditions:obj3
                                    }
                                    return JSON.stringify(newData);
                                }
                            }
                        });
                        initDatatable(promise.dataTable);
                        return promise;

            }          
            }

             function keywordSearchFunction() {
          //     $("#searchData").show();
          
            //    $scope.dataCommonService.data=1;
                var obj = {}; // Do not delete this field
                var obj1 = {};
                var obj2 = {};
                var obj3 = {};

                obj['active'] = true,
                    obj['directoryId'] = $rootScope.currentDirectory.directoryId,
                    obj['parentId'] = null;
                if ($scope.keywordDropDown == "exhibition.exhibitName") {

                    if ($scope.keywordSearchAllFlag == true)
                        obj2["exhibitName"] = $scope.keyword;
                    else
                        obj2["exhibitName"] = { _eval: 'regex', value: $scope.keyword };

                    obj['personnel'] = false;
                    obj3 = {
                        "obj": obj,
                        "obj1": obj1,
                        "obj2": obj2
                    };

                    var promise = $resource(APIBasePath + 'subscriber/search/exhibition/list', {}, {
                        dataTable: {
                            url: APIBasePath + 'subscriber/search/exhibition/list',
                            method: 'POST',
                            transformRequest: function(data, headerGetter) {
                                var headers = headerGetter();
                                var newData = {
                                    dataTableQuery: data,
                                    conditions: obj3
                                }
                                return JSON.stringify(newData);
                            }
                        }
                    });
                    initDatatable(promise.dataTable);
                    return promise;

                }
                if ($scope.keywordDropDown == "facilities") {

                    obj3 = { $or: [] };

                    if (($scope.keyword != undefined) && ($scope.keyword != '')) {
                        if ($scope.keywordSearchAllFlag == true) {
                            obj1['features.name'] = $scope.keyword;
                            obj2['features.codeName'] = $scope.keyword;
                        } else {
                            obj1['features.name'] = { _eval: 'regex', value: $scope.keyword };
                            obj2['features.codeName'] = { _eval: 'regex', value: $scope.keyword };
                        }
                        obj3['active'] = true,
                            obj3['directoryId'] = $rootScope.currentDirectory.directoryId,
                            obj3['parentId'] = null;
                        obj3['features.featureType'] = "FACILITY";

                        obj3['$or'].push(obj1);
                        obj3['$or'].push(obj2);

                        var promise = $resource(APIBasePath + 'subscriber/search/org-list', {}, {
                            dataTable: {
                                url: APIBasePath + 'subscriber/search/org-list',
                                method: 'POST',
                                transformRequest: function(data, headerGetter) {
                                    var headers = headerGetter();
                                    var newData = {
                                        dataTableQuery: data,
                                        //: obj3
                                        conditions:searchTempObj
                                    }
                                    return JSON.stringify(newData);
                                }
                            }
                        });
                        initDatatable(promise.dataTable);
                        return promise;

                    }
                }
                
                if ($scope.keywordDropDown == 'exhibition.exhibitNameAAD') 
                {
                    //var qr=[{$match: {"directoryId":"57189c7024d8bc65f4123bc0", "active":true, "deleted" : false, 'exhibitName':/NCCC Student Exhibition; R/i } }, {$group: {_id:1,'ids':{$push:'$orgId'} } } ];
                    var qr={};
                    qr.directoryId=$rootScope.currentDirectory.directoryId;
                    qr['exhibitName']={_eval: "regex", value: $scope.keyword};
                    var obj3x={data:'exhibitNameAAD',query:qr};
                    var promise = $resource(APIBasePath + 'subscriber/search/exhibition/list', {}, {
                        dataTable: {
                            url: APIBasePath + 'subscriber/search/exhibition/list',
                            method: 'POST',
                            transformRequest: function(data, headerGetter) {
                                var headers = headerGetter();
                                var newData = {
                                    dataTableQuery: data,
                                    conditions: obj3x
                                }
                                return JSON.stringify(newData);
                            }
                        }
                    });
                    initDatatable(promise.dataTable);
                    return promise;
                }
                if ($scope.keywordDropDown == 'features.featureType') 
                {
                    var objx=
                    {'features.featureType': "COLLECTION", 
                    'features.name': {_eval: "regex", value: $scope.keyword},
                    'directoryId':$rootScope.currentDirectory.directoryId
                };
                    var promise = $resource(APIBasePath + 'subscriber/search/org-list', {}, {
                        dataTable: {
                            url: APIBasePath + 'subscriber/search/org-list',
                            method: 'POST',
                            transformRequest: function(data, headerGetter) {
                                var headers = headerGetter();
                                var newData = {
                                    dataTableQuery: data,
                                    conditions: objx
                                }
                                return JSON.stringify(newData);
                            }
                        }
                    });
                    initDatatable(promise.dataTable);
                    return promise;
                }
                if (($scope.keywordDropDown != undefined) && ($scope.keywordDropDown != '')) 
                {

                    if ($scope.keywordSearchAllFlag == true)
                        obj[$scope.keywordDropDown] = $scope.keyword;
                    else
                        obj[$scope.keywordDropDown] = { _eval: 'regex', value: $scope.keyword };

                    var promise = $resource(APIBasePath + 'subscriber/search/org-list', {}, {
                        dataTable: {
                            url: APIBasePath + 'subscriber/search/org-list',
                            method: 'POST',
                            transformRequest: function(data, headerGetter) {
                                var headers = headerGetter();
                                var newData = {
                                    dataTableQuery: data,
                                    conditions: obj
                                }
                                return JSON.stringify(newData);
                            }
                        }
                    });
                    initDatatable(promise.dataTable);
                    return promise;

                }
            };


            $scope.basicSearchResetFunction = function() {
                $scope.name = undefined;
                $scope.firstName = undefined;
                $scope.lastName = undefined;
                $scope.instituteNameAllFlag = false;
                $scope.officerType =[];

            };

            $scope.newSearchOMD=function(){
              //  $scope.advanceSearchResetFunction();
              $scope.advanceSearchResetOMD();
                localStorage.refineSearch='';
                var target = $('#containerBox');
                if (target.length)
                {
                    var top = target.offset().top;
                    $('html,body').animate({scrollTop: top}, 100);
                    return false;
                }
            };
            $scope.newSearchAAD=function(){
                
                $scope.advanceSearchResetFunctionAAD();
                localStorage.refineSearch='';
            };
            $scope.newSearchDMMP=function(){
                $scope.advanceSearchResetFunctionDMMP();
                localStorage.refineSearch='';
            };
            

            $scope.refinesearchAAD=function(){
            if(localStorage.refineSearch == undefined || localStorage.refineSearch == "") return ;
                var temp=JSON.parse(localStorage.refineSearch);
                $scope.collectionDescriptionAllFlag=temp.collectionDescriptionAllFlag;
                if(temp.keywordDropDown=='exhibition.exhibitNameAAD' ||
                    temp.keywordDropDown=='features.featureType')
                {$scope.keywordDropDown=temp.keywordDropDown;
                    $scope.keyword=temp.keyword;
                    $scope.keywordSearchAllFlag=temp.keywordSearchAllFlag;
                    $scope.keywordSearchFunction();
                    $scope.tabHref =3;
                    return ;
                }
                if(temp.firstName!=undefined || temp.lastName!=undefined){
                $scope.firstName=temp.firstName;
                $scope.lastName=temp.lastName;
                
                $scope.tabHref =2;
                searchFunction();
               // $("#searchDataTable")[0].style='';
                return ;
                }
                $scope.activitiesEducationDeptbFlag=temp.activitiesEducationDeptbFlag;
                $scope.establishedDetailbFlag=temp.establishedDetailbFlag;
                $scope.galleryDescriptionFlag=temp.galleryDescriptionFlag;
                vm.attendanceMethod=temp.attendanceMethod;
                $scope.attendanceCount=temp.attendanceCount;
                $scope.attendanceCounts=temp.attendanceCounts;
                $scope.cityName=temp.cityName;
                $scope.stateName1=temp.stateName1;
                $scope.keyword=temp.keyword;
                $scope.name=temp.name;
                $scope.instituteNameAllFlag=temp.instituteNameAllFlag;
             
                $scope.cityDatadata=temp.cityDatadata;
                $scope.stateDatadata=temp.stateDatadata;
                $scope.establishedDetail=temp.establishedDetail;
                $scope.galleryDescription=temp.galleryDescription;
                $scope.exhibitionDescription=temp.exhibitionDescription;
                $scope.scholarship=temp.scholarship;                
                $scope.fellowship=temp.fellowship;
                $scope.assistantship=temp.assistantship;
                $scope.grants=temp.grants;
                $scope.zip=temp.zip;
                $scope.attendanceCount=temp.attendanceCount;
                $scope.activitiesEducationDept=temp.activitiesEducationDept;
                $scope.degreeGranted=temp.degreeGranted;
                $scope.collectionDescription=temp.collectionDescription;
                searchFunction();
            }
            $scope.refineSearchGotoUp =function(){
                var target = $('#containerBox');
                if (target.length)
                {
                    var top = target.offset().top;
                    $('html,body').animate({scrollTop: top}, 100);
                    return false;
                }

            }
            $scope.refinesearchomdMain=function(){
                $scope.refinesearchomd();
                localStorage.refineSearch='';
                var target = $('#containerBox');
                if (target.length)
                {
                    var top = target.offset().top;
                    $('html,body').animate({scrollTop: top}, 100);
                    return false;
                }
            }
            $scope.refinesearchomd=function(){
          
                if(localStorage.refineSearch == undefined || localStorage.refineSearch == "") return ;
                var temp=JSON.parse(localStorage.refineSearch);
                vm.mainCat=temp.mainCat;
                vm.collectionCategorySub=temp.collectionCategorySub;
                $scope.name=temp.name;
                $scope.cityName=temp.cityName;
                $scope.cityName=temp.cityName;
                $('#CityAuto').val(temp.cityName);
                $('#StateAuto').val(temp.stateName1);
                $scope.stateName1=temp.stateName1;
                $scope.stateName5=temp.stateName5;
                $scope.stateName=temp.stateName;
                $scope.cityName=temp.cityName;
                $scope.stateName=temp.stateName;
                $scope.entityType=temp.entityType;
                $scope.institutionDescriptionAllFlag=temp.institutionDescriptionAllFlag;
                $scope.attendanceMethod=temp.attendanceMethod;
                $scope.attendanceCount=temp.attendanceCount;
                $scope.aamId=temp.aamId;
                $scope.icom=temp.icom;
                $scope.accredited=temp.accredited;
                $scope.freeText=temp.freeText;
                $scope.zip=temp.zip;
                $scope.freeText=temp.freeText;
                $scope.freeTextAllFlag=temp.freeTextAllFlag;
                $scope.collectionDescription=temp.collectionDescription;
                $scope.collectionDescriptionAllFlag=temp.collectionDescriptionAllFlag;
                $scope.travelFlag=temp.travelFlag;
                $scope.handicappedFlag=temp.handicappedFlag;
                $scope.tabHref=temp.tabHref;
                vm.attendanceCount=temp.attendanceCount;
                vm.attendanceCounts=temp.attendanceCounts;
                $scope.instituteNameAllFlag=temp.instituteNameAllFlag;
                $scope.collectionCategory=temp.collectionCategory;
                $scope.institutionCategory=temp.institutionCategory;
                $scope.industryPreferences=temp.industryPreferences;
                $scope.geographicalPreference=temp.geographicalPreference;
                $scope.cityDatadata=temp.cityDatadata;
                $scope.stateDatadata=temp.stateDatadata;
                $scope.nstitutionCategorySelData=temp.nstitutionCategorySelData;
                $scope.collectionCategorySelData=temp.collectionCategorySelData;
                $scope.attendanceCount=temp.attendanceCount;
                $scope.attendanceCounts=temp.attendanceCounts;
                vm.attendanceMethod=temp.attendanceMethod;

                //if(temp.firstName!=undefined || temp.lastName!=undefined){
                if(temp.tabHref==2){
                $scope.firstName=temp.firstName;
                $scope.lastName=temp.lastName;
                $scope.tabHref =2;
                searchFunction();
               // $("#searchDataTable")[0].style='';
                return ;
                }
                //if(temp.keyword!=undefined && temp.keywordDropDown!=undefined)
                //if(temp.keyword!=undefined && temp.omdKeywordSearchModel.length > 0)
                if(temp.tabHref==3)
                {
                $scope.tabHref =3;
                $scope.keyword=temp.keyword;
                //$scope.keywordDropDown=temp.keywordDropDown;
                $scope.omdKeywordSearchModel=temp.omdKeywordSearchModel;
                $scope.keywordSearchAllFlag=temp.keywordSearchAllFlag;
                setTimeout(function(){
                    $scope.keywordSearchFunctionOMD();
                },100);
                return ;
                }
                setTimeout(function(){
                    searchFunction();
                },100);
                return ;

                

            };
            $scope.refinesearchDMMP=function(){
                if(localStorage.refineSearch == undefined || localStorage.refineSearch == "") return ;
                var temp=JSON.parse(localStorage.refineSearch);
                $scope.directMarketingBudgetFlag=temp.directMarketingBudgetFlag;
                $scope.galleryDescriptionFlag=temp.galleryDescriptionFlag;
                $scope.disbursalType1=temp.disbursalType1;
                $scope.listingType1=temp.listingType1;
                $scope.cityName=temp.cityName;
                $scope.stateName1=temp.stateName1;
                if(temp.firstName!=undefined || temp.lastName!=undefined){
                $scope.firstName=temp.firstName;
                $scope.lastName=temp.lastName;
                
                $scope.tabHref =2;
                searchFunction();
               // $("#searchDataTable")[0].style='';
                return ;
                }
                $scope.instituteNameAllFlag=temp.instituteNameAllFlag;
                $scope.t1=temp.t1;
                $scope.t2=temp.t2;
                $scope.directMarketingBudget=temp.t1+'-'+temp.t2;
                vm.totalAdvertisingBudgeMethod=temp.totalAdvertisingBudgeMethod;
                vm.totalAdvertisingBudget1=temp.totalAdvertisingBudget1;
                vm.totalAdvertisingBudget2=temp.totalAdvertisingBudget2;
                $scope.name=temp.name;
                $scope.conductBusinessbFlag=temp.conductBusinessbFlag;
                $scope.cityDatadata=temp.cityDatadata;
                $scope.stateDatadata=temp.stateDatadata;
                $scope.establishedYear=temp.establishedYear;
                $scope.conductBusiness=temp.conductBusiness;
                $scope.directMarketingBudget=temp.directMarketingBudget;
                $scope.totalAdvertisingBudget=temp.totalAdvertisingBudget;
                $scope.catalogOnline=temp.catalogOnline;
                $scope.onlineSales=temp.onlineSales;
                $scope.zip=temp.zip;
                $scope.employees=temp.employees;
                $scope.listingType=temp.listingType;
                $scope.disbursalType=temp.disbursalType;
                $scope.galleryDescription=temp.galleryDescription;
                $scope.vendorsName=temp.vendorsName;
                $scope.cityName=temp.cityName;
                $scope.stateName1=temp.stateName1;

                searchFunction();
            }
            $scope.advanceSearchResetOMD=function(){
                vm.mainCat=[];
                vm.collectionCategorySub=[];
                $scope.name=undefined;
                $scope.cityName=undefined;
                $scope.cityName=undefined;
                $scope.stateName1=undefined;
                $scope.stateName5=undefined;
                $scope.stateName=undefined;
                $scope.cityName = undefined;
                $scope.stateName = undefined;
                $scope.entityType = undefined;
                $scope.institutionDescriptionAllFlag = undefined;
                $scope.attendanceMethod = undefined;
                $scope.attendanceCount = undefined;
                $scope.aamId = undefined;
                $scope.icom = undefined;
                $scope.accredited = undefined;
                $scope.freeText = undefined;
                $scope.zip = undefined;
                $scope.freeText = undefined;
                $scope.freeTextAllFlag = undefined;
                $scope.collectionDescription = undefined;
                $scope.collectionDescriptionAllFlag = undefined;
                $scope.travelFlag = undefined;
                $scope.handicappedFlag = undefined;
                $scope.instituteNameAllFlag = false;
                $scope.firstName = undefined;
                $scope.lastName = undefined;
                $scope.collectionCategory = { codeVal: '' };
                $scope.institutionCategory = { codeVal: '' };
                $scope.industryPreferences = [];
                $scope.geographicalPreference =[];
                $scope.cityDatadata=[];
                $scope.stateDatadata=[];
                $scope.nstitutionCategorySelData=[];
                $scope.collectionCategorySelData=[];
                $scope.omdKeywordSearchModel=[];
                $scope.attendanceCount=undefined;
                $scope.attendanceCounts=undefined;
                vm.attendanceCount=undefined;
                vm.attendanceCounts=undefined;
              
                $scope.keywordSearchAllFlag=undefined;
                $scope.keyword=undefined;
                $scope.keywordDropDown=undefined;
                vm.attendanceMethod='';

            }
            $scope.advanceSearchResetFunction = function() {
                // $scope.refinesearchOMDflag+=1;
                // if($scope.refinesearchOMDflag % 2 == 0)
                // {
                var temp={};
                temp.mainCat=vm.mainCat;
                temp.collectionCategorySub=vm.collectionCategorySub;
                temp.firstName=$scope.firstName;
                temp.lastName=$scope.lastName;
                temp.name=$scope.name;
                temp.tabHref=$scope.tabHref;
                // $scope.cityName=$('#CityAuto').val();
                // $scope.stateName1=$('#StateAuto').val();
                // temp.cityName=$('#CityAuto').val();
                // temp.stateName1=$('#StateAuto').val();
                 $scope.cityName=$('#CityAuto').val();
                temp.stateName1=$scope.stateName1;
                // $scope.stateName1=$('#StateAuto').val();
                temp.stateName5=$scope.stateName5;
                temp.stateName=$scope.stateName;
                temp.cityName=$scope.cityName;
                temp.stateName=$scope.stateName;
                temp.entityType=$scope.entityType;
                temp.institutionDescriptionAllFlag=$scope.institutionDescriptionAllFlag;
                temp.attendanceMethod=$scope.attendanceMethod;
                temp.attendanceCount=$scope.attendanceCount;
                temp.aamId=$scope.aamId;
                temp.icom=$scope.icom;
                temp.accredited=$scope.accredited;
                temp.freeText=$scope.freeText;
                temp.zip=$scope.zip;
                temp.freeText=$scope.freeText;
                temp.freeTextAllFlag=$scope.freeTextAllFlag;
                temp.collectionDescription=$scope.collectionDescription;
                temp.collectionDescriptionAllFlag=$scope.collectionDescriptionAllFlag;
                temp.travelFlag=$scope.travelFlag;
                temp.handicappedFlag=$scope.handicappedFlag;
                temp.instituteNameAllFlag=$scope.instituteNameAllFlag;
                temp.firstName=$scope.firstName;
                temp.lastName=$scope.lastName;
                temp.collectionCategory=$scope.collectionCategory;
                temp.institutionCategory=$scope.institutionCategory;
                temp.industryPreferences=$scope.industryPreferences;
                temp.geographicalPreference=$scope.geographicalPreference;
                temp.cityDatadata=$scope.cityDatadata;
                temp.stateDatadata=$scope.stateDatadata;
                temp.nstitutionCategorySelData=$scope.nstitutionCategorySelData;
                temp.collectionCategorySelData=$scope.collectionCategorySelData;
                temp.attendanceCount=$scope.attendanceCount;
                temp.attendanceCounts=$scope.attendanceCounts;
                temp.attendanceMethod=vm.attendanceMethod;
                temp.attendanceCount=vm.attendanceCount;
                temp.attendanceCounts=vm.attendanceCounts;
                temp.keyword=$scope.keyword;
                temp.keywordDropDown=$scope.keywordDropDown;
                temp.omdKeywordSearchModel=$scope.omdKeywordSearchModel;
                temp.keywordSearchAllFlag=$scope.keywordSearchAllFlag;
                localStorage.refineSearch=JSON.stringify(temp);
                //}
            };
            $scope.keywordSearchResetFunction = function() {


                $scope.omdKeywordSearchArray=[{id:'entityType',label:'Institution Description'}, {id:'activities.freeText',label:'Activities'}, {id:'features.name',label:'Collection Description'}, {id:'exhibition.exhibitName',label:'Exhibit Description'}, {id:'facilities',label:'Facilities'}, {id:'governing',label:'Governing Authority'}, {id:'research.description',label:'Research Fields'},{id:'membership',label:'Membership'},{id:'name',label:'Institution Name'},{id:'publication.description',label:'Publication Description'}];                
                $scope.omdKeywordSearchModel=[{id:'entityType',label:'Institution Description'}, {id:'activities.freeText',label:'Activities'}, {id:'features.name',label:'Collection Description'}, {id:'exhibition.exhibitName',label:'Exhibit Description'}, {id:'facilities',label:'Facilities'}, {id:'governing',label:'Governing Authority'}, {id:'research.description',label:'Research Fields'},{id:'membership',label:'Membership'},{id:'name',label:'Institution Name'},{id:'publication.description',label:'Publication Description'}];
                $scope.keyword = undefined;
                $scope.keywordDropDown = undefined;
                $scope.keywordSearchAllFlag = true;
            };




            // DATATABLE CODE

            // $scope.columnDefs = [

                // DTColumnBuilder.newColumn('personnel.titleMasterName')
                // .withTitle('Pesonnel')
                // .renderWith(function(personnel, type, row) {
                //     console.log('Pesonnel',row);
                //     if (row.personnel) {
                //         for (var i = 0; i < row.personnel.length; i++) {
                //             var pers = row.personnel[i];
                //             if (($scope.firstName != undefined) && ($scope.firstName != '')){

                //                 var var1 = new RegExp($scope.firstName,"i");
                //                 if (var1.test(pers.name.first)) {
                //                     if(pers.name.prefix)
                //                         var director = pers.name ? '<p> ' +pers.name.prefix+" "+ [pers.name.first,pers.name.middle, pers.name.last].join(' ') +'</p>' : '';
                //                     if(pers.name.suffix)
                //                         var director = pers.name ? '<p> '+ [pers.name.first,pers.name.middle, pers.name.last].join(' ') + " "+pers.name.suffix + '</p>' : '';
                //                     if(pers.name.suffix && pers.name.prefix )
                //                      var director = pers.name ? '<p> ' +pers.name.prefix+" "+ [pers.name.first,pers.name.middle, pers.name.last].join(' ') + " "+pers.name.suffix +'</p>' : '';
                //                     if(pers.name.first && pers.name.last)
                //                     {
                //                         return pers.name.first+" "+pers.name.last;
                //                     }
                //                     return director || "";
                //                 }
                //             }
                //             else if (($scope.lastName != undefined) && ($scope.lastName != '')){

                //                 var var1 = new RegExp($scope.lastName,"i");
                //                 if (var1.test(pers.name.last)) {

                //                     var director = pers.name ? '<p>' + [pers.name.first,pers.name.middle, pers.name.last].join(' ') + '</p>' : '';
                //                     return director || "";
                //                 }
                //             }
                //             else{
                //                  var director = row.personnel[0].name ? '<p>' + [row.personnel[0].name.first,row.personnel[0].name.middle, row.personnel[0].name.last].join(' ') + '</p>' : '';
                //                     return director || "";
                //             }
                //         }
                
                //     }
                //     return '';
                // }).withOption('width', '9%')
                
                /*.withOption('width', '13%'),
                DTColumnBuilder.newColumn('address[0].state.description')
                .withTitle('State/Territory')
                .renderWith(function(city, type, row) {
                    //return ((row.address && row.address[0] && row.address[0].state.description) ? (row.address[0].state.description || "") : "") || "";
                    if(row.address.length > 0) 
                    if(row.address[0].state.description != undefined && row.address[0].state.description != null)
                        return row.address[0].state.description;
                    else
                        return "";
                    else
                        return "_";

                    
                })*/



                // DTColumnBuilder.newColumn('org_id')
                // .notVisible()
                // .withTitle('Org. ID')
                // .renderWith(function(org_id) {
                //     return org_id || "";
                // })
                // .withOption('width', '10%')
       
            var filterDefs = {
                '0': {
                    type: 'text'
                },
                '1': {
                    type: 'text'
                },
                '2': {
                    type: 'text'
                },
                '3': {
                    type: 'text'
                },
                '4': {
                    type: 'text'
                },
                '6': null
            };

            vm.firstLoad = true;

            function initDatatable(promise) {
                vm.dtI.changeData(promise);
                vm.dtI.reloadData();
                vm.dtI.rerender();
                vm.firstLoad = false;
            }
                 
            vm.dtO = DTOptionsBuilder
            .fromSource(DirectoryOrganizationService.dataTable)
                .withPaginationType('full_numbers')
                .withOption('order', [0, 'asc'])
                .withDataProp('data')
                .withOption('oLanguage', {
                    "sEmptyTable": "No matching records found"
                })
                .withOption('fnRowCallback', function(nRow) { $compile(nRow)($scope); })
                .withOption('fnDrawCallback', function(settings) {
                    if (0) setTimeout(function() {
                        angular.element('html, body').stop().animate({
                            scrollTop: ((angular.element('#searchDataTable').offset().top - 150))
                        }, 1500, 'easeInOutExpo');
                    }, 700);
                })
                .withOption('responsive', true)
                .withOption('processing', true)
                .withOption('serverSide', true);


            function accInit() {
                setTimeout(function() {
                    $('#accordion').accordion({
                        collapsible: true,
                        active: true
                    });
                    $compile('#accordion')($scope);
                }, 100);
            }
//console.log(vm.dtO);

            $scope.$watch('tabHref', function(newVal) {
                    if(newVal==1){
                        vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                        accInit();  
                    }
                    if(newVal==2){
                        vm.dtC = [xtempname.directorCEO.withTitle('Name'),xtempname.personnel,xtempname.name,xtempname.cityName, xtempname.statedescription, xtempname.actions ];
                         accInit();
                    }
                    if(newVal==3)
                    {
                        $scope.keywordSearchAllFlag=true;
                        vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                    }
                    setTimeout(function(){
                        if(JSON.parse(localStorage.tabHrefFlag))
                            $("#searchData").show();
                        else
                            $("#searchData").hide();

                        },100);
            });
            //accInit();
            // var dis=() => 
            // { 
            //     if(localStorage.state=='new') 
            //     {
            //   //  $scope.refinesearchomd();
            //     $scope.searchFunction();
            //     }
            // }
            // dis();
           // searchFunction();

           setTimeout(function() {
            if(localStorage.state=='new') 
            {
               searchFunction();
            }
            if(localStorage.state=='ref' && $rootScope.currentDirectory.directoryId=='57189b3e24d8bc65f4123bbf') 
               $scope.refinesearchomd();
                var target = $('#containerBox');
                if (target.length)
                {
                    var top = target.offset().top;
                    $('html,body').animate({scrollTop: top}, 150);
                    return false;
                }
                //setTimeout(()=>{},100)

            },100)
        }

    ]);