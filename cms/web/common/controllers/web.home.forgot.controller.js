'use strict';
angular.module('web')
    .controller('web.home.forgot.controller', [
        '$rootScope', '$scope', '$state', '$stateParams', '$localStorage', '$location', '$dataService', '$sce',
        function($rootScope, $scope, $state, $stateParams, $localStorage, $location, $dataService, $sce) {
            $scope.forgot = function() {
                $dataService.post('subscriber/forgot-password', {
                        directoryId: $rootScope.currentDirectory.directoryId,
                        auth: $scope.auth
                    },
                    function(data) {
                        showMessages([{
                            header: 'Forgot Password',
                            type: 'success',
                            message: 'Your new password has been successfully mailed to you!'
                        }]);
                        $state.go('core.login');
                    },
                    function(error) {
                        console.log('forgot err', error);
                        var msg = error && error ? error.message : false;
                        showMessages([{
                            header: 'Forgot Password',
                            type: 'error',
                            message: (msg || 'Could not send a new password successfully!')
                        }]);
                    })
            };
        }
    ]);
