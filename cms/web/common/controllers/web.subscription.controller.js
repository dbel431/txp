angular.module('web')
    .controller('SubscriptionController', [
        '$rootScope',
        '$scope',
        '$state',
        '$stateParams',
        'SubscriberService',
        'DirectoryCountryService',
        'DirectoryStateService',
        'DirectoryCityService',
        'SubscriptionPackage',
        '$dataService',
        function($rootScope, $scope, $state, $stateParams, SubscriberService, DirectoryCountryService, DirectoryStateService, DirectoryCityService, SubscriptionPackage, $dataService) {
            $dataService.get('web-data?pageName=subscription',
                function(data) {
                    //console.log(data);
                    if (data != null) {
                        $scope.sections = data;
                        $scope.subscriber.orders.typeName = $scope.sections[0].subData[0].subscriberTypes[0].type;
                        var packageCostType = $scope.sections[0].subData[0].subscriberTypes[0].costPrefix;
                        $scope.subscriber.orders.ppu = $scope.subscriptionPackage[packageCostType + "ackageCost"];
                    }
                },
                function(error) {
                    //console.log(error)
                })
            $scope.loading = false;
            // if ($stateParams.navigateTo != '') setTimeout(function() {
            //     angular.element('html, body').stop().animate({
            //         scrollTop: ((angular.element($stateParams.navigateTo).offset().top) - 50)
            //     }, 1500, 'easeInOutExpo');
            // }, 150);

            if (!$stateParams.packageName) {
                $state.go('core.sales', { navigateTo: '#ways' });
            }

            $scope.initAddressCities = function() {
                $("#auto-complete-city")
                    .autocomplete({
                        source: function(req, res) {
                            $scope.getAddressCities(req.term).then(function(data) {
                                res(data);
                            });
                        },
                        minLength: 3,
                        focus: function(event, ui) {
                            $("#auto-complete-city").val(ui.item.codeValue);
                            return false;
                        },
                        select: function(evt, ui) {
                            $scope.selectAddressCityTypeAhead(ui.item, ui.item);
                            $('#auto-complete-city').val($scope.subscriber.city);
                            $('#auto-complete-state').val($scope.subscriber.state);
                            $('#auto-complete-country').val($scope.subscriber.country);
                            return false;
                        }
                    })
                    .autocomplete("instance")._renderItem = function(ul, item) {
                        return $("<li>")
                            .append("<div>" + item.codeValue + "<p>" + (item.parentId ? (item.parentId.codeValue + " (" + item.parentId.description + ")") : "") + "<span>, " + (item.parentId && item.parentId.parentId ? item.parentId.parentId.codeValue : "") + "</span>" + "</p>" + "</div>")
                            .appendTo(ul);
                    };
            };
            $scope.getAddressCities = function(val) {
                $('[ng-show="noResultsCity"]').html('<i class="glyphicon glyphicon-remove"></i> No Results Found');
                if (val.length > 2) {
                    return DirectoryCityService.list({
                        codeValue: {
                            _eval: 'regex',
                            value: val
                        },
                        active: true
                    }).$promise;
                } else $('[ng-show="noResultsCity"]').html('Enter minimum 3 characters');
            };
            $scope.selectAddressCityTypeAhead = function($item, $model, $label, $event) {
                $scope.subscriber.city = $model.codeValue;
                $scope.subscriber.state = $model.parentId ? $model.parentId.codeValue + " (" + $model.parentId.description + ")" : "";
                $scope.subscriber.country = $model.parentId && $model.parentId.parentId ? $model.parentId.parentId.codeValue : "";
            };
            $scope.initAddressStates = function() {
                $("#auto-complete-state")
                    .autocomplete({
                        source: function(req, res) {
                            $scope.getAddressStates(req.term).then(function(data) {
                                res(data);
                            });
                        },
                        focus: function(event, ui) {
                            $("#auto-complete-state").val(ui.item.codeValue + " (" + ui.item.description + ")");
                            return false;
                        },
                        minLength: 3,
                        select: function(evt, ui) {
                            $scope.selectAddressStateTypeAhead(ui.item, ui.item);
                            $('#auto-complete-state').val($scope.subscriber.state);
                            $('#auto-complete-country').val($scope.subscriber.country);
                            return false;
                        }
                    })
                    .autocomplete("instance")._renderItem = function(ul, item) {
                        return $("<li>")
                            .append("<div>" + item.codeValue + " (" + item.description + ")" + "<span>, " + (item.parentId ? item.parentId.codeValue : "") + "</span>" + "</div>")
                            .appendTo(ul);
                    };
            };
            $scope.getAddressStates = function(val) {
                $('[ng-show="noResultsState"]').html('<i class="glyphicon glyphicon-remove"></i> No Results Found');
                if (val.length > 2) {
                    return DirectoryStateService.list({
                        codeValue: {
                            _eval: 'regex',
                            value: val
                        },
                        active: true
                    }).$promise;
                } else $('[ng-show="noResultsState"]').html('Enter minimum 3 characters');
            };
            $scope.selectAddressStateTypeAhead = function($item, $model, $label, $event) {
                $scope.subscriber.state = $model.codeValue + " (" + $model.description + ")";
                $scope.subscriber.country = $model.parentId.codeValue;
            };
            $scope.initAddressCountries = function() {
                $("#auto-complete-country")
                    .autocomplete({
                        source: function(req, res) {
                            $scope.getAddressCountries(req.term).then(function(data) {
                                res(data);
                            });
                        },
                        focus: function(event, ui) {
                            $("#auto-complete-country").val(ui.item.codeValue);
                            return false;
                        },
                        minLength: 3,
                        select: function(evt, ui) {
                            $scope.selectAddressCountryTypeAhead(ui.item, ui.item);
                            $('#auto-complete-country').val($scope.subscriber.country);
                            return false;
                        }
                    })
                    .autocomplete("instance")._renderItem = function(ul, item) {
                        return $("<li>")
                            .append("<div>" + item.codeValue + "</div>")
                            .appendTo(ul);
                    };
            };
            $scope.getAddressCountries = function(val) {
                $('[ng-show="noResultsCountry"]').html('<i class="glyphicon glyphicon-remove"></i> No Results Found');
                if (val.length > 2) {
                    return DirectoryCountryService.list({
                        codeValue: {
                            _eval: 'regex',
                            value: val
                        },
                        active: true
                    }).$promise;
                } else $('[ng-show="noResultsCountry"]').html('Enter minimum 3 characters');
            };
            $scope.selectAddressCountryTypeAhead = function($item, $model, $label, $event) {
                $scope.subscriber.country = $model.codeValue;
            };

            $scope.subscriptionPackage = SubscriptionPackage[0];

            $scope.subscriber = {
                directoryId: $rootScope.currentDirectory.directoryId,
                orders: {
                    packageName: $stateParams.packageName,
                    package: $scope.subscriptionPackage._id,
                    qty: 1
                }
            };

            $scope.changeSubscriberType = function(subscriberType) {
                $scope.subscriber.orders.typeName = subscriberType.type;
                $scope.subscriber.orders.ppu = $scope.subscriptionPackage[subscriberType.costPrefix + 'ackageCost'];
            };
            $scope.subscribe = function subscribe() {
                $scope.loading = true;
                $scope.subscriber.completeName = $scope.subscriber.name;
                var nameSplit = $scope.subscriber.name.split(' ');
                $scope.subscriber.name = {
                    first: nameSplit[0],
                    middle: nameSplit.length == 2 ? undefined : nameSplit[1],
                    last: nameSplit.length == 2 ? nameSplit[1] : nameSplit.slice(2, nameSplit.length).join(' ')
                };
                SubscriberService.save($scope.subscriber, function(res) {
                    showMessages([{
                        type: 'success',
                        message: 'Order placed successfully!',
                        header: 'Order Online'
                    }]);
                    $state.go('core.sales');
                }, function(err) {
                    console.log(err);
                    showMessages([{
                        type: 'error',
                        message: 'Order could not be placed successfully!',
                        header: 'Order Online'
                    }]);
                })
            };

            $scope.checkExistingSubscriber = function() {
                var orderData = $scope.subscriber.orders;
                SubscriberService.get({
                    'directoryId': $rootScope.currentDirectory.directoryId,
                    'auth.email': $scope.subscriber.auth.email,
                    active: true
                }, function(subscribers) {
                    if (subscribers && subscribers.length > 0) {
                        $scope.subscriber = subscribers[0];
                        $scope.subscriber.name = [$scope.subscriber.name.first, $scope.subscriber.name.middle, $scope.subscriber.name.last].join(' ');
                        $scope.subscriber.orders = orderData;
                    }
                }, function(err) {
                    console.log('Existing user check err', err);
                })
            };

            setTimeout(function() {
                $scope.initAddressCities();
                $scope.initAddressStates();
                $scope.initAddressCountries();
            }, 100);

            $scope.cancel = function() {
                // $state.reload();
                $state.go('core.subscription', { navigateTo: '#subscriptionForm' }, { reload: true });
                // $scope.subscriber={};
            };

        }
    ]);
