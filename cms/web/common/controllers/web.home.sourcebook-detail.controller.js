'use strict';
angular.module('web')
    .controller('web.home.sourcebook-detail.controller', ['$scope', '$location', '$dataService', '$sce', '$stateParams', '$rootScope',
        function($scope, $location, $dataService, $sce, $stateParams, $rootScope) {

            console.log("In web.home.sourcebook-detail.controller");
            $scope.direcoryName = $rootScope.currentDirectory.name;
            // if ($stateParams.navigateTo != '') setTimeout(function() {
            //     angular.element('html, body').stop().animate({
            //         scrollTop: ((angular.element($stateParams.navigateTo).offset().top) - 50)
            //     }, 1500, 'easeInOutExpo');
            // }, 200);

            $dataService.get('web-data?pageName=SourcebookDetail',
                function(data) {
                    console.log(data);
                    if (data != null) {
                        $scope.sections = data;
                        console.log(" data:", data);
                        console.log("sections.sectionHeader:", $scope.sections[0].sectionHeader);
                    }
                },
                function(error) {
                    console.log(error)
                })

        }
    ])
