
'use strict';
angular.module('web')
    .controller('DirectoryWebUserSearchControllerCFS', ['dataCommonService', '$rootScope', '$scope', '$localStorage', '$location', '$dataService', '$sce', '$state', '$stateParams', '$compile', '$resource', '$uibModal', 'DTOptionsBuilder', 'DTColumnBuilder', 'DirectoryOrganizationService', 'DirectoryInstitutionCategory', 'DirectoryCollectionCategory', 'DirectoryWebUserService', 'DirectoryCityService', 'DirectoryStateService', 'DirectorySpecificationType', 'DirectorySpecificationCodeForIndustryPreference', 'DirectoryOfficerType', 'DirectorySpecificationCodeForGeographicalPreference', 'DirectorySpecificationCodeForPortfolioRelationship', 'DirectorySpecificationCodeForFinancingPreferred', 'DirectorySpecificationCodeForDealType', 'DirectorySpecificationCodeForFirmWillTakeActiveRoleAs', 'ListingType', 'DisbursalType',
    function(dataCommonService,$rootScope, $scope, $localStorage, $location, $dataService, $sce, $state, $stateParams, $compile, $resource, $uibModal, DTOptionsBuilder, DTColumnBuilder, DirectoryOrganizationService, DirectoryInstitutionCategory, DirectoryCollectionCategory, DirectoryWebUserService, DirectoryCityService, DirectoryStateService, DirectorySpecificationType, DirectorySpecificationCodeForIndustryPreference, DirectoryOfficerType, DirectorySpecificationCodeForGeographicalPreference, DirectorySpecificationCodeForPortfolioRelationship, DirectorySpecificationCodeForFinancingPreferred, DirectorySpecificationCodeForDealType, DirectorySpecificationCodeForFirmWillTakeActiveRoleAs, ListingType, DisbursalType) {
            // if (!$localStorage['NRP-USER-TOKEN'])
            // {
            //     localStorage.refineSearch='';
            //     $state.go('core.login');
            // }
            var vm = this;
            /*
            cfs add
            */
            $scope.IndustryPreferenceArray=DirectorySpecificationCodeForIndustryPreference.map(function(data){return {id:data.codeValue,label:data.codeValue}; }); 
            $scope.GeographicalPreferenceArrray = DirectorySpecificationCodeForGeographicalPreference.map(function(data){return {id:data.codeValue,label:data.codeValue};});
            $scope.firmWillTakeActiveRoleAsArray = DirectorySpecificationCodeForFirmWillTakeActiveRoleAs.map(function(data){return {id:data.codeValue,label:data.codeValue};});
            $scope.financingPreferredArray = DirectorySpecificationCodeForFinancingPreferred.map(function(data){return {id:data.codeValue,label:data.codeValue};});
            $scope.portfolioRelationshipArray = DirectorySpecificationCodeForPortfolioRelationship.map(function(data){return {id:data.codeValue,label:data.codeValue};});
            $scope.officerTypeArray = DirectoryOfficerType.map(function(data){return {id:data.codeValue,label:data.codeValue};});
            // vm.data={
            // 'City':{'key':'address.cityName','value':'','type':'vm'},
            // 'state':{'key':'address.stateName','value':[],'type':'vm'},
            // 'zip':{'key':'address.zip','value':'','type':'vm'},  //zip=regx and name=regx
            // 'industryPreferences': {'key':'_specificationType.codeName', 'value':[],'type':'vm','specificationType':'INDUSTRY PREFERENCE'},//   vm.dataOfAnd['specificationType']['$in'].push("INDUSTRY PREFERENCE");
            // 'geographicalPreference':{'key':'_specificationType.codeName','value':[],'type':'vm','specificationType':'GEOGRAPHICAL PREFERENCES'},//   vm.cfsData['specificationType']['$in'].push("GEOGRAPHICAL PREFERENCES");
            // 'firmWillTakeActiveRoleAs':{'key':'_specificationType.codeName','value':[], 'type':'vm','specificationType':'FIRM WILL TAKE ACTIVE ROLE AS'},//      vm.cfsData['specificationType']['$in'].push("FIRM WILL TAKE ACTIVE ROLE AS");
            // 'financingPreferred':{'key':'_specificationType.codeName', 'value':[],'type':'vm','specificationType':'FINANCING PREFERRED'},//          vm.cfsData['specificationType']['$in'].push("FINANCING PREFERRED");
            // 'portfolioRelationship':{'key':'_specificationType.codeName','value':[],'type':'vm','specificationType':'PORTFOLIO RELATIONSHIP'}, //          vm.dataOfAnd['specificationType']['$in'].push("PORTFOLIO RELATIONSHIP");
            // 'establishedYear':{'key':'established.year','value':'','type':'vm'},
            // 'geographicalPreferencesAroundTheWorld':{'key':'_specificationType.name','value':'','type':'vm','specificationType':'INTERNATIONAL GEOGRAPHICAL PREFERENCES'},//    vm.cfsData['specificationType']['$in'].push("INTERNATIONAL GEOGRAPHICAL PREFERENCES");
            // 'minimumSizeInvestment':{'key':'_specificationType.name','value':'','type':'vm','specificationType':'MINIMUM SIZE INVESTMENT'},//       vm.cfsData['specificationType']['$in'].push("MINIMUM SIZE INVESTMENT");
            // 'preferredSizeInvestment':{'key':'_specificationType.name','value':'','type':'vm','specificationType':'PREFERRED SIZE INVESTMENT'}, //   obj8['specificationType'] = "PREFERRED SIZE INVESTMENT";
            // 'maximumSizeInvestmentInOneCompany':{'key':'_specificationType.name','value':'','type':'vm','specificationType':'MAXIMUM SIZE INVESTMENT IN ONE COMPANY'},//vm.cfsData['specificationType']['$in'].push("MAXIMUM SIZE INVESTMENT IN ONE COMPANY");
            // 'fundsAvailabel':{'key':'_specificationType.name','value':'','type':'vm'}, //vm.cfsData['specificationType']['$in'].push("FUNDS AVAILABLE FOR INVESTMENTS OR LOANS");
            // 'mainSourcesOfCapital':{'key':'_specificationType.name','value':'','type':'vm','specificationType':'MAIN SOURCES OF CAPITAL'},//       obj8['specificationType'] = "MAIN SOURCES OF CAPITAL";
            // 'firmPrefersNotToInvestIn':{'key':'_specificationType.name','value':'','type':'vm','specificationType':'FIRM PREFERS NOT TO INVEST IN'},//   vm.cfsData['specificationType']['$in'].push("FIRM PREFERS NOT TO INVEST IN");
            // 'averageAmountInvestedAnnually':{'key':'_specificationType.name','value':'','type':'vm','specificationType':'AVERAGE AMOUNT INVESTED ANNUALLY'},         //     vm.cfsData['specificationType']['$in'].push("AVERAGE AMOUNT INVESTED ANNUALLY");
            // 'averageNumberOfDealsCompletedAnnually':{'key':'_specificationType.name','value':'','type':'vm','specificationType':'AVERAGE NUMBER OF DEALS COMPLETED ANNUALLY'}, //   vm.cfsData['specificationType']['$in'].push("AVERAGE NUMBER OF DEALS COMPLETED ANNUALLY");
            // 'investmentPortfolioSize':{'key':'_specificationType.name','value':'','type':'vm','specificationType':'INVESTMENT PORTFOLIO SIZE'},// vm.cfsData['specificationType']['$in'].push("INVESTMENT PORTFOLIO SIZE");
            // 'exitCriteria': {'key':'_specificationType.name','value':'','type':'vm','specificationType':'EXIT CRITERIA'}, // vm.cfsData['specificationType']['$in'].push("EXIT CRITERIA");
            // 'minimumOperatingData': {'key':'_specificationType.name','value':'','type':'vm','specificationType':'MINIMUM OPERATING DATA REQUIRED TO CONSIDER FINANCING'},//     vm.cfsData['specificationType']['$in'].push("MINIMUM OPERATING DATA REQUIRED TO CONSIDER FINANCING");
            // };
    vm.data=     {
            'name':{'key':'name','value':'','type':'vm','regx':false},
            'City':{'key':'address.cityName','value':'','type':'vm'},
            'state':{'key':'address.stateName','value':[],'type':'vm'},
            'zip':{'key':'address.zip','value':'','type':'vm','regx':true},
            'industryPreferences': {'key':'_specificationType.codeName', 'value':[],'type':'vm','specificationType':'INDUSTRY PREFERENCE'},
            'geographicalPreference':{'key':'_specificationType.codeName','value':[],'type':'vm','specificationType':'GEOGRAPHICAL PREFERENCES'},
            'firmWillTakeActiveRoleAs':{'key':'_specificationType.codeName','value':[], 'type':'vm','specificationType':'FIRM WILL TAKE ACTIVE ROLE AS'},
            'financingPreferred':{'key':'_specificationType.codeName', 'value':[],'type':'vm','specificationType':'FINANCING PREFERRED'},
            'portfolioRelationship':{'key':'_specificationType.codeName','value':[],'type':'vm','specificationType':'PORTFOLIO RELATIONSHIP'},
            'establishedYear':{'key':'established.year','value':'','type':'vm'},
            'geographicalPreferencesAroundTheWorld':{'key':'_specificationType.name','value':'','type':'vm','specificationType':'INTERNATIONAL GEOGRAPHICAL PREFERENCES','regx':true},
            'minimumSizeInvestment':{'key':'_specificationType.name','value':'','type':'vm','specificationType':'MINIMUM SIZE INVESTMENT','regx':true},
            'preferredSizeInvestment':{'key':'_specificationType.name','value':'','type':'vm','specificationType':'PREFERRED SIZE INVESTMENT','regx':true},
            'maximumSizeInvestmentInOneCompany':{'key':'_specificationType.name','value':'','type':'vm','specificationType':'MAXIMUM SIZE INVESTMENT IN ONE COMPANY','regx':true},
            'fundsAvailabel':{'key':'_specificationType.name','value':'','type':'vm','regx':true,'specificationType':'FUNDS AVAILABLE FOR INVESTMENTS OR LOANS'},
            'mainSourcesOfCapital':{'key':'_specificationType.name','value':'','type':'vm','specificationType':'MAIN SOURCES OF CAPITAL','regx':true},
            'firmPrefersNotToInvestIn':{'key':'_specificationType.name','value':'','type':'vm','specificationType':'FIRM PREFERS NOT TO INVEST IN','regx':true},
            'averageAmountInvestedAnnually':{'key':'_specificationType.name','value':'','type':'vm','specificationType':'AVERAGE AMOUNT INVESTED ANNUALLY','regx':true},
            'averageNumberOfDealsCompletedAnnually':{'key':'_specificationType.name','value':'','type':'vm','specificationType':'AVERAGE NUMBER OF DEALS COMPLETED ANNUALLY','regx':true},
            'investmentPortfolioSize':{'key':'_specificationType.name','value':'','type':'vm','specificationType':'INVESTMENT PORTFOLIO SIZE','regx':true},
            'exitCriteria': {'key':'_specificationType.name','value':'','type':'vm','specificationType':'EXIT CRITERIA','regx':true},
            'minimumOperatingData': {'key':'_specificationType.name','value':'','type':'vm','specificationType':'MINIMUM OPERATING DATA REQUIRED TO CONSIDER FINANCING','regx':true},
            'officerType':{'key':'officers.OfficerTypeName','value':[],'type':'vm','regx':true}// vm.dataOfAnd['officers.OfficerTypeName']['$in'].push($scope.officerType[i].id);
            };
          //  vm.data= {"_specificationType":{"key":"_specificationType","value":[],"type":"array"},"_name":{"key":"_name","value":[],"type":"array"},"City":{"key":"address.cityName","value":"","type":"string"},"state":{"key":"address.stateName","value":[],"type":"array"},"zip":{"key":"address.zip","value":"","type":"string"},"industryPreferences":{"key":"_specificationType.codeName","value":[],"type":"array"},"geographicalPreference":{"key":"_specificationType.codeName","value":[],"type":"array"},"firmWillTakeActiveRoleAs":{"key":"_specificationType.codeName","value":[],"type":"array"},"financingPreferred":{"key":"_specificationType.codeName","value":[],"type":"array"},"portfolioRelationship":{"key":"_specificationType.codeName","value":[],"type":"array"},"establishedYear":{"key":"established.year","value":"","type":"string"},"geographicalPreferencesAroundTheWorld":{"regx":true,"key":"_specificationType.name","value":"","type":"string"},"minimumSizeInvestment":{"regx":true,"key":"_specificationType.name","value":"","type":"string"},"preferredSizeInvestment":{"regx":true,"key":"_specificationType.name","value":"","type":"string"},"maximumSizeInvestmentInOneCompany":{"regx":true,"key":"_specificationType.name","value":"","type":"string"},"fundsAvailabel":{"regx":true,"key":"_specificationType.name","value":"","type":"string"},"mainSourcesOfCapital":{"regx":true,"key":"_specificationType.name","value":"","type":"string"},"firmPrefersNotToInvestIn":{"regx":true,"key":"_specificationType.name","value":"","type":"string"},"averageAmountInvestedAnnually":{"regx":true,"key":"_specificationType.name","value":"","type":"string"},"averageNumberOfDealsCompletedAnnually":{"regx":true,"key":"_specificationType.name","value":"","type":"string"},"investmentPortfolioSize":{"regx":true,"key":"_specificationType.name","value":"","type":"string"},"exitCriteria":{"regx":true,"key":"_specificationType.name","value":"","type":"string"},"minimumOperatingData":{"regx":true,"key":"_specificationType.name","value":"","type":"string"}};

 
            /*
            cfs end*/
            /*ASD@123 holders start*/
            $scope.InstitutionCategoryArrayDataSettings={enableSearch: true, scrollableHeight: '250px', scrollable: true, scrollableWidth:'100%', externalIdProp: ''};
            $scope.keywordDropDownSettings={scrollableHeight: 'auto', scrollable: true, scrollableWidth:'100%', externalIdProp: ''};
            $('#searchData').hide();
            $scope.disbursalTypeArray = DisbursalType.map(function(data){return {id:data.codeValue,label:data.codeValue};});
            $scope.listingTypeArray = ListingType.map(function(data){return {id:data.codeValue,label:data.codeValue};});
            function init()
            {
            var arr=[{'type':'scope','name':'firstName','value':''}, {'type':'scope','name':'lastName','value':''}, {'type':'scope','name':'name','value':''}, {'type':'scope','name':'instituteNameAllFlag','value':false}, {'type':'scope','name':'cityName','value':''}, {'type':'scope','name':'establishedYear','value':''}, {'type':'scope','name':'vendorsName','value':''}, {'type':'scope','name':'conductBusiness','value':''}, {'type':'scope','name':'conductBusinessbFlag','value':false}, {'type':'scope','name':'catalogOnline','value':false}, {'type':'scope','name':'onlineSales','value':''}, {'type':'scope','name':'zip','value':''}, {'type':'scope','name':'employees','value':''}, {'type':'scope','name':'listingType1','value':''}, {'type':'scope','name':'listingType','value':[]}, {'type':'scope','name':'disbursalType1','value':''}, {'type':'scope','name':'disbursalType','value':[]}, {'type':'scope','name':'t1','value':''}, {'type':'scope','name':'t2','value':''}, {'type':'scope','name':'galleryDescription','value':''}, {'type':'scope','name':'galleryDescriptionFlag','value':''}, {'type':'scope','name':'stateDatadata','value':[]}, {'type':'vm','name':'totalAdvertisingBudgeMethod','value':'select'}, {'type':'vm','name':'totalAdvertisingBudget1','value':''}, {'type':'vm','name':'totalAdvertisingBudget2','value':''}];
            for(var i=0;i<arr.length;i++) {if(arr[i].type=='scope') {var key=arr[i].name; $scope[key]=arr[i].value; } if(arr[i].type=='vm') {var key=arr[i].name; vm[key]=arr[i].value; }
            }
            }
            //table start
            var xtempname={
                directorCEO:
                DTColumnBuilder.newColumn('name','flags')                
                .renderWith(function(name,type, row) {
                    var str='';
                    if(row.personnel.length > 0)
                        str=row.personnel[0].name.last+','+row.personnel[0].name.first;
                    return str;
                })
                .withOption('width', '8%'),
                name:
                DTColumnBuilder.newColumn('name','flags')
                .withTitle('Institution Name')
                .renderWith(function(name,type, row) {
                    if(name.split(' ')[0].toUpperCase()=='THE')
                        name=name.replace('The','')+',The';
                    if(row.flags)
                    {
                    if(row.flags.aam)
                        name=name+'        <img src="/assets/aam-icon.png" width=15 height=15>';
                    if(row.flags.icom)
                        name=name+'        <img src="/assets/icom-icon.png" width=20 height=10> ';
                    if(row.flags.accredited)
                        name=name+'        <img src="/assets/star_icon.png" width=15 height=15>';
                     if(row.flags.handicapped)
                         name=name+'      <img src="/assets/Handicapped-Accessible.png" width=15 height=15>';
                     }

                    return name||"";
                })
                .withOption('width', '20%'),
                personnelTitle:
                DTColumnBuilder.newColumn('personnel','flags')
                .withTitle('personnelTitle')
                .renderWith(function(name,type, row) {
                    return 'personnelTitle456';
                })
                .withOption('width', '20%'),
                personnel:DTColumnBuilder.newColumn('personnel.titleMasterName')
                .withTitle('Title')
                .renderWith(function(personnel, type, row) {
          if($scope.tabHref==2)
                    {
                        console.log("$scope.lastName",$scope.lastName);
                        console.log("$scope.firstName",$scope.firstName);
                        if(($scope.lastName && $scope.firstName==undefined) || ($scope.lastName && $scope.firstName=="")){
                        var getPer=row.personnel.filter(function(val){
                            if((val.name.last.toUpperCase() == $scope.lastName.toUpperCase() )|| val.name.last.toUpperCase().indexOf($scope.lastName.toUpperCase()) != -1 )
                            {
                                return val;
                            }
                        }); 
                        row.personnel= getPer;   
                        }
                        if(($scope.firstName && $scope.lastName==undefined )|| ($scope.firstName && $scope.lastName=="" )){
                        var getPer=row.personnel.filter(function(val){
                            if((val.name.first.toUpperCase() == $scope.firstName.toUpperCase() )|| val.name.first.toUpperCase().indexOf($scope.firstName.toUpperCase()) != -1 )
                            {
                                return val;
                            }
                        }); 
                        row.personnel= getPer;   
                        }
                        if($scope.firstName && $scope.lastName){
                        var getPer=row.personnel.filter(function(val){
                            if(
                                (
                                (val.name.first.toUpperCase() == $scope.firstName.toUpperCase() )|| 
                                (val.name.first.toUpperCase().indexOf($scope.firstName.toUpperCase()
                                ) != -1))&& ((val.name.last.toUpperCase() == $scope.lastName.toUpperCase() )|| (val.name.last.toUpperCase().indexOf($scope.lastName.toUpperCase()) != -1 )) )
                            {
                                
                                return val;
                            }
                        }); 
                        row.personnel= getPer;   
                        }
                    }
                    var str='';
                    if(row.personnel.length > 0 && row.personnel[0].title)
                        return row.personnel[0].title;
                    else
                        return '';

                }).withOption('width', '8%'),
                cityName:DTColumnBuilder.newColumn('address.cityName')
                .withTitle('City')
                .renderWith(function(city, type, row) {
                   var zzz=row.address.filter(function(v){if(v.addressType=='576286d0c19cef300bc720e6'){
                            return 1;
                   }});
//                    return ((row.address && zzz.address[0] && zzz.address[0].cityName) ? (zzz.address[0].cityName || "") : "") || "";
                    return ((row.address && row.address[0] && row.address[0].cityName + '45') ? (row.address[0].cityName  || "") : "") || "";
                }).withOption('width', '10%'),
                statedescription:DTColumnBuilder.newColumn('address.state.description')
                    .withTitle('State/Territory')
                .renderWith(function(data, type, full){
                    var zzz=full.address.filter(function(v){if(v.addressType=='576286d0c19cef300bc720e6'){
                            return 1;
                   }});
                    if(zzz.length > 1)
                    return ((full.address && full.address[0] && full.address[0].state.description) ? (full.address[0].state.description|| "") : "") || "";
                    else
                    return ((full.address && full.address[0] && full.address[0].state &&full.address[0].state.description) ? (full.address[0].state.description|| "") : "") || "";

                })
                .withOption('width', '2%'),
                 zip:DTColumnBuilder.newColumn('address.zip')
                .withTitle('zip')
                .renderWith(function(data, type, full){
                    return full.address[0].zip?full.address[0].zip:'';
                })
                .withOption('width', '8%'),
                actions:DTColumnBuilder.newColumn('_id')
                .withTitle('Actions')
                .notSortable()
                .withClass('text-center')
                .renderWith(function(id,type, full) {
                    return '<div ng-click=vm.advanceSearchResetFunctionDMMP()><directory-web-user-record-actions record-id="\'' + id + '\'"></directory-web-user-record-actions></div>';
                })
                .withOption('width', '4%'),
                personnelName:DTColumnBuilder.newColumn('personnel.titleMasterName')
                .withTitle('Pesonnel')
                .renderWith(function(personnel, type, row) {
                    console.log('Pesonnel',row);
                    if (row.personnel) {
                        for (var i = 0; i < row.personnel.length; i++) {
                            var pers = row.personnel[i];
                            if (($scope.firstName !== undefined) && ($scope.firstName !== '')){

                                var var1 = new RegExp($scope.firstName,"i");
                                if (var1.test(pers.name.first)) {
                                    if(pers.name.prefix)
                                        var director = pers.name ? '<p> ' +pers.name.prefix+" "+ [pers.name.first,pers.name.middle, pers.name.last].join(' ') +'</p>' : '';
                                    if(pers.name.suffix)
                                        var director = pers.name ? '<p> '+ [pers.name.first,pers.name.middle, pers.name.last].join(' ') + " "+pers.name.suffix + '</p>' : '';
                                    if(pers.name.suffix && pers.name.prefix )
                                     var director = pers.name ? '<p> ' +pers.name.prefix+" "+ [pers.name.first,pers.name.middle, pers.name.last].join(' ') + " "+pers.name.suffix +'</p>' : '';
                                    if(pers.name.first && pers.name.last)
                                    {
                                        return pers.name.first+" "+pers.name.last;
                                    }
                                    return director || "";
                                }
                            }
                            else if (($scope.lastName !== undefined) && ($scope.lastName !== '')){

                                var var1 = new RegExp($scope.lastName,"i");
                                if (var1.test(pers.name.last)) {

                                    var director = pers.name ? '<p>' + [pers.name.first,pers.name.middle, pers.name.last].join(' ') + '</p>' : '';
                                    return director || "";
                                }
                            }
                            else{
                                 var director = row.personnel[0].name ? '<p>' + [row.personnel[0].name.first,row.personnel[0].name.middle, row.personnel[0].name.last].join(' ') + '</p>' : '';
                                    return director || "";
                            }
                        }
                
                    }
                    return '';
                }).withOption('width', '9%')
                };
            //table end
            vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
            /*function define start*/
            vm.newSearchAAD=newSearchAAD;
            vm.refinesearchAAD=refinesearchAAD;
            vm.gotoDiv=gotoDiv;
            vm.pSearch=pSearch;
            vm.advanceSearchResetFunctionDMMP=advanceSearchResetFunctionDMMP;
            vm.keywordSearch=keywordSearch;
            vm.institutionalSearch=institutionalSearch;
            vm.init=init;
            
            /*function define end*/
            /*
switch('a') {case 'a': {console.log('a'); break;} case b: {console.log('b'); break; } default: console.log('z'); }
*/
            // function xqr()
            // {
            //     var arr=Object.keys(vm.data);
            //     var qrObj={};
            //         //qrObj.stateName={$in:[]}; //qrObj.codeName.$in
            //         qrObj['chapterSpecification.name']={$in:[]}; //qrObj.name.$in
            //         qrObj['chapterSpecification.specificationType']={$in:[]}; //qrObj.specificationType.$in
            //         qrObj['chapterSpecification.codeName']={$in:[]};
            //     for(var i=0;i<arr.length;i++)
            //     {
            //         var xkey=arr[i];
            //         if(vm.data[xkey].value!== ''|| vm.data[xkey].value.length > 1)
            //         switch(vm.data[xkey].key)
            //         {
            //             case 'address.cityName':
            //             {
            //                 vm.data.City.value=$('#CityAuto').val();
            //                 qrObj['address.cityName']=vm.data.City.value;
            //                 break;
            //             }       
            //             case 'name':
            //             {
            //                 if(vm.data[xkey].regx)
            //                 qrObj['name']={ _eval: 'regex', value: vm.data[xkey].value };
            //             else
            //                 qrObj['name']=vm.data.name.value;
            //                 break;
            //             }
            //             case 'address.stateName':
            //             {
            //                 qrObj['address.stateName']={$in:vm.data.state.value};
            //                 break;
            //             }
            //             case 'address.zip':
            //             {
            //                 if(vm.data[xkey].regx)
            //                 qrObj['address.zip']={ _eval: 'regex', value: vm.data[xkey].value };
            //                     else
            //                 qrObj['address.zip']=vm.data.zip.value;
            //                 break;
            //             }
            //             case '_specificationType.codeName':
            //             {   if(vm.data[xkey].value.length > 0)
            //                 qrObj['chapterSpecification.specificationType']['$in'].push(vm.data[xkey].specificationType);
            //                 for(var j=0;j<vm.data[xkey].value.length;j++)
            //                 {
            //                     qrObj['chapterSpecification.codeName']['$in'].push(vm.data[xkey].value[j].id);
            //                 }
            //                 break;
            //             }
            //             case '_specificationType.name':
            //             {
            //                     qrObj['chapterSpecification.specificationType']['$in'].push(vm.data[xkey].specificationType);
            //                        if(vm.data[xkey].regx==true)
            //                         qrObj['chapterSpecification.name']['$in'].push({ _eval: 'regex', value: vm.data[xkey].value });
            //                         else
            //                         qrObj['chapterSpecification.name']['$in'].push(vm.data[xkey].value);
            //                 break;
            //             }
            //             case 'established.year':
            //             {
            //                 qrObj['established.year']=vm.data[xkey].value;
            //                 break;
            //             }
                        
            //         }

            //     }
            //     if(qrObj['address.stateName'].$in.length == 0) {delete qrObj['address.stateName']; } if(qrObj['chapterSpecification.codeName'].$in.length == 0) {delete qrObj['chapterSpecification.codeName']; } if(qrObj['chapterSpecification.name'].$in.length ==0 ) {delete qrObj['chapterSpecification.name']; } if(qrObj['chapterSpecification.specificationType'].$in.length == 0) {delete qrObj['chapterSpecification.specificationType']; }

            // qrObj['directoryId']=$rootScope.currentDirectory.directoryId;
            // console.log(qrObj);
            // var promise = $resource(APIBasePath + 'subscriber/search/chapter-specificationData', {}, {
            //         dataTable: {
            //             method: 'POST',
            //             transformRequest: function(data, headerGetter) {
            //                 var headers = headerGetter();
            //                 var newData = {
            //                     dataTableQuery: data,
            //                     conditions: qrObj
            //                 }
            //                 return JSON.stringify(newData);
            //             }
            //         }
            //     });
            //     initDatatable(promise.dataTable);
            //     return promise;

            // }
            if(localStorage.state == undefined)
            {
                console.log("localStorage.state_245");
                init();  
                $('#searchData').hide();
                if(localStorage.refineSearch !== "" && localStorage.refineSearch !== undefined)
                {
                    var temp=JSON.parse(localStorage.refineSearch);
                    if(temp == undefined)
                    {
                  //  $("#searchData").show();
                    $scope.tabHref = 1;
                    vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                    init();    
                    }
                    else
                    {
                         vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                         $scope.tabHref =temp.tabHref.value;
                    }
                }else
                {
                    $scope.tabHref = 1;
                    vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                    init();  
                }                
            }
            if(localStorage.state=='new')
            {
                console.log("localStorage.state_245");
                delete localStorage.state;
                init();  
                $('#searchData').show();
                if(localStorage.refineSearch !== "" && localStorage.refineSearch !== undefined)
                {
                    var temp=JSON.parse(localStorage.refineSearch);
                    delete localStorage.refineSearch;
                    if(temp == undefined)
                    {
                  //  $("#searchData").show();
                    $scope.tabHref = 1;
                    vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                    init();    
                    }
                    else
                    {
                         vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                         $scope.tabHref =temp.tabHref.value;
                    }
                }else
                {
                    $scope.tabHref = 1;
                    vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                    init();  
                }

            }
            if(localStorage.state=='ref')
            {
                delete localStorage.state;
                console.log("localStorage.state_new_278",localStorage.state);
                init();  
                $('#searchData').show();
                if(localStorage.refineSearch !== "" && localStorage.refineSearch !== undefined)
                {
                    var temp=JSON.parse(localStorage.refineSearch);
                    delete localStorage.refineSearch;
                    if(temp == undefined)
                    {
                    $scope.tabHref = 1;
                    vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                   
                    }
                    else
                    {
                         vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                         $scope.tabHref =temp.tabHref.value;
                         tempToScope(temp);
                         setTimeout(function(){
                            searchFunction($scope.tabHref);
                        },100);
                         
                    }
                }else
                {
                    $scope.tabHref = 1;
                    vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                    init();  
                }
            }
            function institutionalSearch()
            {
                
                setTimeout(function(){
                    gotoDiv('#gotoSearch');    
                },100);
                console.log("institutionalSearch");
                searchFunction(1);
                
            }
            function keywordSearch()
            {
                 console.log("keywordSearch");
                    var qr={
                    "eFlag":true,
                    "org":{"directoryId":$rootScope.currentDirectory.directoryId},
                    "exhi":{"$or":[getQr()]}};
                    keyword(qr);
                    gotoDiv('#gotoSearch');
            }
            function tempToScope(temp)
            {                
                var arr=Object.keys(temp);
                for(var i=0;i<arr.length;i++)
                {
                    var key=arr[i],
                    name=temp[key].name, 
                    type=temp[key].type,
                    value=temp[key].value;
                    if(type=="scope" && value!== " " && value!== undefined)
                    {
                        $scope[name]=value;
                    }
                    if(type=="vm" && value!== " " && value!== undefined)
                    {
                        vm[name]=value;
                    }
                }
            }
            function advanceSearchResetFunctionDMMP()
            {
                console.log("advanceSearchResetFunctionDMMP");
                var arr=[{'type':'scope','name':'firstName','value':''}, {'type':'scope','name':'lastName','value':''}, {'type':'scope','name':'name','value':''}, {'type':'scope','name':'instituteNameAllFlag','value':false}, {'type':'scope','name':'cityName','value':''}, {'type':'scope','name':'establishedYear','value':''}, {'type':'scope','name':'vendorsName','value':''}, {'type':'scope','name':'conductBusiness','value':''}, {'type':'scope','name':'conductBusinessbFlag','value':false}, {'type':'scope','name':'catalogOnline','value':false}, {'type':'scope','name':'onlineSales','value':''}, {'type':'scope','name':'zip','value':''}, {'type':'scope','name':'employees','value':''}, {'type':'scope','name':'listingType1','value':''}, {'type':'scope','name':'listingType','value':[]}, {'type':'scope','name':'disbursalType1','value':''}, {'type':'scope','name':'disbursalType','value':[]}, {'type':'scope','name':'t1','value':''}, {'type':'scope','name':'t2','value':''}, {'type':'scope','name':'galleryDescription','value':''}, {'type':'scope','name':'galleryDescriptionFlag','value':''}, {'type':'scope','name':'stateDatadata','value':[]}, {'type':'vm','name':'totalAdvertisingBudgeMethod','value':'select'}, {'type':'vm','name':'totalAdvertisingBudget1','value':''}, {'type':'vm','name':'totalAdvertisingBudget2','value':''},{'type':'scope','name':'tabHref','value':$scope.tabHref}];
                var temp={};
                for(var i=0;i<arr.length;i++)                
                {
                    if(arr[i].type==='scope')
                    {
                        var type=arr[i].type,name=arr[i].name,val=$scope[name];
                        temp[name]={"type":type,"name":name,"value":val};
                    }
                    if(arr[i].type==='vm')
                    {
                        var type=arr[i].type,name=arr[i].name,val=vm[name];
                        temp[name]={"type":type,"name":name,"value":val};
                    }

                }
                localStorage.refineSearch=JSON.stringify(temp);
            }
            function pSearch()
            {
                console.log("pSearch");
                var qr={org:{"directoryId":$rootScope.currentDirectory.directoryId},
                    per:getQr()
                };
                persone(qr);
                gotoDiv('#gotoSearch');
            }
            function gotoDiv(name)
            {
                 console.log("gotoDiv",name);
                 $('#searchData').show();
                 $('html,body').animate({scrollTop: $(name).offset().top-50},'fast');
            }
            function newSearchAAD()
            {
                console.log("newSearchAAD");
                init();
                vm.gotoDiv('#containerBox');
            }
            function refinesearchAAD()
            {
                console.log("refinesearchAAD");
                vm.gotoDiv('#containerBox');
            }
            function org(qr)
            {
                 console.log("org");
                    var promise = $resource(APIBasePath + 'subscriber/search/org-list', {}, {
                        dataTable: {
                            url: APIBasePath + 'subscriber/search/org-list',
                            method: 'POST',
                            transformRequest: function(data, headerGetter) {
                                var headers = headerGetter();
                                var newData = {
                                    dataTableQuery: data,
                                    conditions: qr
                                    }
                                return JSON.stringify(newData);
                                }
                            }
                        });
                        initDatatable(promise.dataTable);
                        return promise;
            }
            function persone(qr)
            {
                    console.log("persone");
                var promise = $resource(APIBasePath + 'subscriber/search/personnel/list', {}, {
                        dataTable: {
                            url: APIBasePath + 'subscriber/search/personnel/list',
                            method: 'POST',
                            transformRequest: function(data, headerGetter) {
                                var headers = headerGetter();
                                var newData = {
                                    dataTableQuery: data,
                                    conditions: qr,
                                    persone:1
                                    }
                                return JSON.stringify(newData);
                                }
                            }
                        });
                        initDatatable(promise.dataTable);
                        return promise;
            }
            function keyword(qr)
            {
                   console.log("keyword");
                var promise = $resource(APIBasePath + 'subscriber/search/exhibition/list', {}, {
                        dataTable: {
                            url: APIBasePath + 'subscriber/search/exhibition/list',
                            method: 'POST',
                            transformRequest: function(data, headerGetter) {
                                var headers = headerGetter();
                                var newData = {
                                    dataTableQuery: data,
                                    conditions: qr,
                                    persone:1
                                    }
                                return JSON.stringify(newData);
                                }
                            }
                        });
                        initDatatable(promise.dataTable);
                        return promise;
            }
            function organizationstochapterSpecification(qr){
                var promise = $resource(APIBasePath + 'subscriber/search/chapter-specificationData', {}, {
                    dataTable: {
                        method: 'POST',
                        transformRequest: function(data, headerGetter) {
                            var headers = headerGetter();
                            var newData = {
                                dataTableQuery: data,
                                conditions: qr
                            }
                            return JSON.stringify(newData);
                        }
                    }
                });
                initDatatable(promise.dataTable);
                return promise;
            }
            /*ASD@123 holders end*/
            /*string to number function start*/
            function strtonumber(data)
            {
                console.log("strtonumber");
                var temp=data;
                for(var i=0;i<data.split(',').length;i++)
                {
                    temp=temp.replace(',','');
                }
                return temp;
            }
            /*string to number function end */
            /*ASD@123 watch*/
               $scope.$watch('vm.data.City.value', function(newValue, oldValue) {
                console.log(newValue);
                var z1={
                    _where: {codeTypeId:'57174a5d6778ef1c153aec6f',codeValue: {_eval: 'regex',value: newValue},parentId:{'$ne':null},active: true},
                    _limit: 50
                };
                if(newValue!=="" && newValue!== undefined)
                $dataService.post('directory/city',z1,function(data){
                    $scope.cityData=data
                            .map(function(val){
                                if(val.parentId!==null && val.parentId.description!== null || '')
                                return {id:val.codeValue,label:val.codeValue+'('+val.parentId.description+')',data:data};
                             else
                               return val.codeValue;
                                });
                              $( "#CityAuto" ).autocomplete({
                                  source:$scope.cityData,
                                   select: function( event, ui ) { 
                                    console.log('ui',ui);
                                    $("#CityAuto").val(ui.item.id);
                                    return false;
                                   }
                                });
                              
                            },function(error){
                        });
            
            });
            function stateGetData(newValue)
            {
                var arr=[];
                    arr.push({codeValue:{_eval: 'regex',value: newValue}});
                    arr.push({description:{_eval: 'regex',value: newValue}});
                var z1={
                    _where: {codeTypeId:'57174a4b6778ef1c153aec6e','$or':arr,active: true},
                    _limit: 5000
                };
                if(newValue!=="" && newValue!== undefined)
                $dataService.post('directory/state',z1,function(data){
                            $scope.stateData=data.map(function(data){
                                 return {id:data.codeValue,label:data.description?data.codeValue+" ("+data.description+")":data.codeValue};
                                });
                                $( "#StateAuto" ).autocomplete({
                                  source:$scope.stateData
                                });
                                     $( "#StateAuto" ).autocomplete({
                                  source:$scope.stateData,
                                   select: function( event, ui ) { 
                                    $("#StateAuto").val(ui.item.id);
                                    return false;
                                   }
                                });
                            },function(error){
                        });

            }
            stateGetData('a');

                $scope.$watch('stateName1', function(newValue, oldValue) {
                 stateGetData(newValue);
 

            });
         /*ASD@123 watch*/
            vm.dtI = null;
            vm.dataOfData={};
            vm.dataOfAnd={};
   
         //   $("#searchData").hide();
            //ASD@123 search
            $scope.searchFunction =searchFunction;
            function searchFunction(data)
            {
                console.log("searchFunction");
                //$("#searchData").show();
                console.log('data',data);
                if(data==1)  {
                var qr=getQr();
                qr['directoryId']=$rootScope.currentDirectory.directoryId;
                //org(qr);    
                organizationstochapterSpecification(qr);
                }
                if(data==2)
                {
                    var qr={org:{"directoryId":$rootScope.currentDirectory.directoryId},
                    per:getQr()
                };
                persone(qr);
                }
                if(data==3)
                {
                    var qr={
                    "eFlag":true,
                    "org":{"directoryId":$rootScope.currentDirectory.directoryId},
                    "exhi":{"$or":[getQr()]}};
                    keyword(qr);
                }
            } 

            function getQr(){
            //    alert("hii");
            console.log('getQr');
                       var arr=Object.keys(vm.data);
                var qrObj={};
                    //qrObj.stateName={$in:[]}; //qrObj.codeName.$in
                    qrObj['chapterSpecification.name']={$in:[]}; //qrObj.name.$in
                    qrObj['chapterSpecification.specificationType']={$in:[]}; //qrObj.specificationType.$in
                    qrObj['chapterSpecification.codeName']={$in:[]};
                    qrObj['address.stateName']={$in:[]};
                for(var i=0;i<arr.length;i++)
                {
                    var xkey=arr[i];
                    if(vm.data[xkey].value!== ''|| vm.data[xkey].value.length > 1)
                    switch(vm.data[xkey].key)
                    {
                        case 'address.cityName':
                        {
                            vm.data.City.value=$('#CityAuto').val();
                            qrObj['address.cityName']=vm.data.City.value;
                            break;
                        }       
                        case 'name':
                        {
                            if(vm.data[xkey].regx)
                            qrObj['name']={ _eval: 'regex', value: vm.data[xkey].value };
                            //qrObj['name']=vm.data[xkey].value;
                        else
                            {
                            var obj=[];
                            var arrname=vm.data.name.value.split(' ');
                            obj.push({ _eval: 'regex', value:vm.data.name.value})
                            for(var i=0;i<arrname.length;i++)
                            {
                                if(arrname[i]!=='')
                                {
                                    var sname=arrname[i];
                                    obj.push({ _eval: 'regex', value:sname})
                                }
                            }
                            qrObj['name']={'$in':obj};
                            }
                            //qrObj['name']=vm.data.name.value;
                            break;
                        }
                        case 'address.stateName':
                        {
                            qrObj['address.stateName']={$in:vm.data.state.value};
                            break;
                        }
                        case 'address.zip':
                        {
                            if(vm.data[xkey].regx)
                            qrObj['address.zip']={ _eval: 'regex', value: vm.data[xkey].value };
                                else
                            qrObj['address.zip']=vm.data.zip.value;
                            break;
                        }
                        case '_specificationType.codeName':
                        {   if(vm.data[xkey].value.length > 0)
                            qrObj['chapterSpecification.specificationType']['$in'].push(vm.data[xkey].specificationType);
                            for(var j=0;j<vm.data[xkey].value.length;j++)
                            {
                                qrObj['chapterSpecification.codeName']['$in'].push(vm.data[xkey].value[j].id);
                            }
                            break;
                        }
                        case '_specificationType.name':
                        {
                                qrObj['chapterSpecification.specificationType']['$in'].push(vm.data[xkey].specificationType);
                                   if(vm.data[xkey].regx==true)
                                    qrObj['chapterSpecification.name']['$in'].push({ _eval: 'regex', value: vm.data[xkey].value });
                                    else
                                    qrObj['chapterSpecification.name']['$in'].push(vm.data[xkey].value);
                            break;
                        }
                        case 'established.year':
                        {
                            qrObj['established.year']=vm.data[xkey].value;
                            break;
                        }
                        
                    }

                }
                if(qrObj['address.stateName'].$in.length == 0) {delete qrObj['address.stateName']; } if(qrObj['chapterSpecification.codeName'].$in.length == 0) {delete qrObj['chapterSpecification.codeName']; } if(qrObj['chapterSpecification.name'].$in.length ==0 ) {delete qrObj['chapterSpecification.name']; } if(qrObj['chapterSpecification.specificationType'].$in.length == 0) {delete qrObj['chapterSpecification.specificationType']; }
                return qrObj;
        }
            //ASD@123 searchFunction end
            $scope.refinesearchAADflag=1;

  
            if (localStorage.url !== $rootScope.currentDirectory.name) {
                $state.go('core.login');
                $localStorage.$reset();
                localStorage.url = $rootScope.currentDirectory.name;
            }
            $scope.dtInstance = {};
            $scope.dtInstanceCallback = function(instance) {
                $scope.dtInstance = instance;
            };
            $scope.dataCommonService=dataCommonService;        
            vm.dtIC = function(instance) {
                vm.dtI = instance;
            };
            $scope.autocompletecity=function(data){
                   
            }
            $scope.arrayOfObjectsForIndex = {};
            $scope.totalDisplayed = 10;
            $scope.loadMore = function() {
                $scope.totalDisplayed += 10;
            };
            $scope.basicSearchResetFunction = function() {
                $scope.name = undefined;
                $scope.firstName = undefined;
                $scope.lastName = undefined;
                $scope.instituteNameAllFlag = false;
                $scope.officerType =[];

            };
            $scope.keywordSearchResetFunction = function() {
                $scope.keyword = undefined;
                $scope.keywordDropDown = undefined;
                $scope.keywordSearchAllFlag = undefined;
            };
            vm.firstLoad = true;

            function initDatatable(promise) {
                vm.dtI.changeData(promise);
                vm.dtI.reloadData();
                vm.dtI.rerender();
                vm.firstLoad = false;
            }
                 
            vm.dtO = DTOptionsBuilder.
            fromSource(DirectoryOrganizationService.dataTable)
                .withPaginationType('full_numbers')
                .withOption('order', [0, 'asc'])
                .withDataProp('data')
                .withOption('oLanguage', {
                    "sEmptyTable": "No matching records found"
                })
                .withOption('fnRowCallback', function(nRow) { $compile(nRow)($scope); })
                .withOption('responsive', true)
                .withOption('processing', true)
                .withOption('serverSide', true);


                    function accInit() {
                        setTimeout(function() {
                            $('#accordion').accordion({
                                collapsible: true,
                                active: false
                            });
                            $compile('#accordion')($scope);
                        }, 100);
                    }
                    $scope.$watch('tabHref', function(newVal) {
                    if(newVal==1){
                        vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                        accInit();  
                    }
                    if(newVal==2){
                        vm.dtC = [xtempname.directorCEO.withTitle('Name'),xtempname.personnel,xtempname.name,xtempname.cityName, xtempname.statedescription, xtempname.actions ];
                         accInit();
                    }
                    if(newVal==3)
                    {
                        $scope.keywordSearchAllFlag=true;
                        vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                    }
            });
        }

    ]);
