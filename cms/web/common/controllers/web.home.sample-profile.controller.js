'use strict';
angular.module('web')
	.controller('web.home.sample-profile.controller', ['$scope', '$location', '$dataService', '$sce', function($scope, $location, $dataService, $sce){

		$dataService.get('web-data?pageName=sample-listing',
			function(data){
				console.log(data);
				if (data != null) {
					$scope.sections = data;
				}
			},function(error){
				console.log(error)
			})


	}])