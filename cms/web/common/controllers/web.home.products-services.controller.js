'use strict';
angular.module('web')
    .controller('web.home.products-services.controller', ['$scope', '$rootScope', '$location', '$dataService', '$sce', '$state', 'DirectoryStateService', 'DirectoryProductCategoryService', function($scope, $rootScope, $location, $dataService, $sce, $state, DirectoryStateService, DirectoryProductCategoryService) {
        $scope.catFunction=function(data){
            return parseInt(data.length/2);
        }
        $scope.direcoryName = $rootScope.currentDirectory.name;
        $scope.getImg=function(data){
            data.imgSrc=data.codeValue.replace(/[^a-zA-Z ]/g, "").replace(/ /g,'');
        }
        $scope.htmlData=function(data){
            return $sce.trustAsHtml(data);
        };
        $scope.radius='RADIUS';
        DirectoryProductCategoryService.getProductSubCategory({ "directoryId": $rootScope.currentDirectory.directoryId },
            function(data) {

                console.log(data);
                if (data != null) {
                    $scope.getProductSubCategory = data;
                }
            },
            function(error) {

                console.log(error)
            })


        DirectoryProductCategoryService.list({ "directoryId": $rootScope.currentDirectory.directoryId },
            function(data) {

                console.log(data);
                if (data != null) {
                    $scope.getProductCategory = data;
                }
            },
            function(error) {
                console.log(error)
            })



        $dataService.get('web-data?pageName=products-services-guide',
                function(data) {
                    console.log(data);
                    if (data != null) {
                        $scope.sections = data;
                    }
                },
                function(error) {
                    console.log(error)
                })
            // // $dataService.get('products-services/getProductCategory',
            //     function(data) {


        //         if (data != null) {
        //             $scope.getProductCategory = data;
        //         }
        //     },
        //     function(error) {
        //         //console.log(error)
        //     })

        // $dataService.get('products-services/postProductSubCategory',
        //     function(data) {
        //         if (data != null) {
        //             $scope.getProductSubCategory = data;
        //         }
        //     },
        //     function(error) {
        //         //console.log(error)
        //     })

        $scope.getAddressStates = function(val) {
            $('[ng-show="noResultsState"]').html('<i class="glyphicon glyphicon-remove"></i> No Results Found');
            if (val.length > 2) {
                return DirectoryStateService.list({
                    codeValue: {
                        _eval: 'regex',
                        value: val
                    },
                    active: true
                }).$promise;
            } else $('[ng-show="noResultsState"]').html('Enter minimum 3 characters');
        };
            $scope.selectAll = function() {
            var pageInfo=$scope.dtInstance.DataTable.page.info();
            var getGuideData= angular.copy($scope.getGuide1);
            
                $("#tabledtInstance").hide();
                if($scope.select == false)
                {
                $scope.select=true;
                getGuideData=getGuideData.map(function(obj){ 
                obj.data=true;
                return obj;
                });
                // for(var i=pageInfo.start;i<pageInfo.end;i++)
                // {
                //     getGuideData.push(getGuideData[i]);
                // }
                }else{
                $scope.select=false;
                getGuideData=getGuideData.map(function(obj){ 
                obj.data=false;
                return obj;
                });

                }
                $scope.getGuide1=getGuideData;
                setTimeout(function(){$("#tabledtInstance").show();},10);
                
            }
        $scope.initAddressStates = function() {
            $("#auto-complete-state")
                .autocomplete({
                    source: function(req, res) {
                        $scope.getAddressStates(req.term).then(function(data) {
                            res(data);
                        });
                    },
                    focus: function(event, ui) {
                        $("#auto-complete-state").val(ui.item.codeValue + " (" + ui.item.description + ")");
                        return false;
                    },
                    minLength: 3,
                    select: function(evt, ui) {
                        $scope.selectAddressStateTypeAhead(ui.item, ui.item);
                        $('#auto-complete-state').val($scope.val.State);
                        //$('#auto-complete-country').val($scope.addDatacard.country);
                        return false;
                    }
                })
                .autocomplete("instance")._renderItem = function(ul, item) {
                    return $("<li>")
                        .append("<div>" + item.codeValue + " (" + item.description + ")" + "<span>, " + (item.parentId ? item.parentId.codeValue : "") + "</span>" + "</div>")
                        .appendTo(ul);
                };
        };
        $scope.selectAddressStateTypeAhead = function($item, $model, $label, $event) {
            //$scope.val.State = $model.codeValue + " (" + $model.description + ")";
            $scope.val.State = $model.codeValue;
            //$scope.val.zip = $model.zip.codeValue;
        };

        setTimeout(function() {
            $scope.initAddressStates();
        }, 100);

        // $dataService.post('directory/state', {},
        //         function(data) {


        //             if (data != null) {
        //                 $scope.StateArray = data;
        //             }
        //         },
        //         function(error) {
        //             //console.log(error)
        //         })


        $scope.productCategoryList = function(item) {


            var results = { "ProductSubCategoryNames": item.codeValue }
                //console.log($state);
            $state.go('core.productsList', results);

        }

        $scope.val = { "KeywordSearch": '', "Zip": '' ,"chk":false};

        $scope.Reset = function() {
            $scope.val = { "KeywordSearch": '', "Zip": '' };
             $scope.radius="RADIUS";
            document.getElementById("singleSelect").selectedIndex = "0";

        }



        //$scope.val = {};


        $scope.KeyWordSearch = function(val) {

            $rootScope.KeyWordSearch=val;

            var obj = { "directoryName": $rootScope.currentDirectory.name };

             // Do not delete this field
            console.log(val.KeywordSearch);
            
            var key = val.KeywordSearch;
                     
            // console.log('val');
            var results = { "keywordSearch": val.KeywordSearch };
      

            if(val.ProductSubCategoryName)
            {
                obj['$or'] = [];
                
                obj['$or'].push({'categories.ProductCategoryName': { _eval: 'regex', value: val.ProductSubCategoryName }});
                obj['$or'].push({'categories.ProductCategoryName': val.ProductSubCategoryName});
                obj['$or'].push({'categories.ProductSubCategoryName': { _eval: 'regex', value: val.ProductSubCategoryName }});
                obj['$or'].push({'categories.ProductSubCategoryName': val.ProductSubCategoryName});
                obj['$or'].push({'companyName': { _eval: 'regex', value: val.ProductSubCategoryName} });    
                obj['$or'].push({'companyName': val.ProductSubCategoryName});
                obj['$or'].push({'companyDescription':{ _eval: 'regex', value: val.ProductSubCategoryName }});
            }


            

            if ((val.State != undefined) && (val.State != '')) {
                // if(!obj['$or']) obj['$or'] = [];
                obj['companyAddress.state'] = { _eval: 'regex', value: val.State } ; 
            }
            if ((val.Zip != undefined) && (val.Zip != '')) {
                console.log(val.Zip.charAt(0));

                if (!isNaN(val.Zip.charAt(0))) {
                  
                    obj['companyAddress.zip']= { _eval: 'regex', value: val.Zip } ;
                    
                } else {
                    obj['companyAddress.city']= { _eval: 'regex', value: val.Zip } ;
                   
                }



            }
            
            // var obj2 = {};

            // obj2['directoryId'] = "OMD";
            // obj2['categories.ProductCategoryName'] = { _eval: 'regex', value: "Au" };
            // obj2['categories.ProductSubCategoryName'] = { _eval: 'regex', value: "Au" };
            //{"KeywordSearch":"","Zip":"","chk":false}
        
            $rootScope.obj1 = obj;
            $state.go('core.productsSearch');

        }







    }])


/*
SOTHEBY'S
else
{ "directoryName": "OMD", 
"startDate": { $lte: ISODate("2016-11-18T10:07:57.924+0000") }, 
"endDate": { $gte: ISODate("2016-11-18T10:08:21.266+0000") }, 
"published": 1.0, 
"companyName": "SOTHEBY'S"
}
//
{ "directoryName": "OMD", 
"startDate": { $lte: ISODate("2016-11-18T10:07:57.924+0000") }, 
"endDate": { $gte: ISODate("2016-11-18T10:08:21.266+0000") }, 
"published": 1.0, 
$or: [ 
{ "categories.ProductCategoryName": /insurance/i }, 
{ "categories.ProductSubCategoryName": /insurance/i }, 
{ "companyName": /insurance/i },
{ "categories.ProductCategoryName": insurance }, 
{ "categories.ProductSubCategoryName": insurance }, 
{ "companyName": insurance } ,
companyDescription:/insurance/i
] }
*/
