'use strict';
angular.module('web')
    .controller('web.home.register.controller', [
        '$rootScope',
        '$scope',
        '$state',
        '$stateParams',
        '$location',
        'SubscriberService',
        '$localStorage',
        function($rootScope, $scope, $state, $stateParams, $location, SubscriberService, $localStorage) {
            $scope.user = new SubscriberService({
                "directoryId": $rootScope.currentDirectory.directoryId,
                auth: { secret: localStorage.getItem('NRP-FINGERPRINT') }
            });
            $scope.register = function() {
                $scope.regForm.$setUntouched();
                $scope.user.$register(function(data) {
                    var loginPass = [],
                        printPkg = 0;
                    for (var i = 0; i < data.email.orders.length; i++) {
                        if (data.email.orders[i].confirmed.flag == true) {
                            loginPass.push(data.email.orders[i].package);
                        }
                        if (data.email.orders[i].confirmed.flag == true && data.email.orders[i].package == '57a6c362ebba3d886256dba1') {
                            printPkg += 1;
                        }
                    }
                    if (loginPass.length == printPkg) {
                        $localStorage['NRP-USER-TOKEN'] = data.token;
                        $localStorage['PKG'] = "57a6c362ebba3d886256dba1";
                        $state.go('core.profile');
                    } else {
                        $localStorage['NRP-USER-TOKEN'] = data.token;
                        if (data.requestPasswordChange)
                            $state.go('core.profile', { requestPasswordChange: true, navigateTo: '#change-pass-div' }, { reload: true });
                        else
                            $state.go('core.search', { navigateTo: '#search', navigatePosition: 50 }, { reload: true });
                    }
                    showMessages([{
                        header: 'Sign Up',
                        type: 'success',
                        message: 'Sign Up Successful!'
                    }]);
                    $localStorage['NRP-USER-TOKEN'] = data.token;
                    $state.go('core.search', { navigateTo: '#search', navigatePosition: 50 }, { reload: true });
                }, function(error) {
                    console.log('Reg. error', error);
                    var msg = error && error.data ? error.data.message : false;
                    showMessages([{
                        header: 'Sign Up',
                        type: 'error',
                        message: (msg || 'Could not Sign Up successfully!')
                    }]);
                })
            };
        }
    ])
