angular.module('web')
    .controller('ProfileController', [
        '$rootScope',
        '$scope',
        '$state',
        '$stateParams',
        'Profile',
        'DirectoryCountryService',
        'DirectoryStateService',
        'DirectoryCityService',
        '$dataService',
        function($rootScope, $scope, $state, $stateParams, Profile, DirectoryCountryService, DirectoryStateService, DirectoryCityService, $dataService) {

            if (!$rootScope.isUserLoggedIn()) $state.go('core.home');

            $scope.loading = false;
            $scope.tabHref = $stateParams.requestPasswordChange ? 'cp' : 'g';
            $scope.requestPasswordChange = $stateParams.requestPasswordChange;
            $scope.editFlag = false;
            $scope.toggleEditFlag = function() {
                $scope.editFlag = !$scope.editFlag;
                initPage();
            };
                    $scope.subscriber=Profile
                    var mstr={};
                    angular.copy($scope.subscriber,mstr);
            //angular.copy(Profile,$scope.subscriber);
            $scope.cancel = function() {

//                $state.reload();
                $scope.subscriber=angular.copy(mstr);
                $scope.editFlag = !$scope.editFlag;
               
                
            };
            // if ($stateParams.navigateTo != '') setTimeout(function() {
            //     angular.element('html, body').stop().animate({
            //         scrollTop: ((angular.element($stateParams.navigateTo).offset().top) - 50)
            //     }, 1500, 'easeInOutExpo');
            // }, 700);

            $scope.initAddressCities = function() {
                $("#auto-complete-city")
                    .autocomplete({
                        source: function(req, res) {
                            $scope.getAddressCities(req.term).then(function(data) {
                                res(data);
                            });
                        },
                        minLength: 3,
                        focus: function(event, ui) {
                            $("#auto-complete-city").val(ui.item.codeValue);
                            return false;
                        },
                        select: function(evt, ui) {
                            $scope.selectAddressCityTypeAhead(ui.item, ui.item);
                            $('#auto-complete-city').val($scope.subscriber.city);
                            $('#auto-complete-state').val($scope.subscriber.state);
                            $('#auto-complete-country').val($scope.subscriber.country);
                            return false;
                        }
                    })
                    .autocomplete("instance")._renderItem = function(ul, item) {
                        return $("<li>")
                            .append("<div>" + item.codeValue + "<p>" + (item.parentId ? (item.parentId.codeValue + " (" + item.parentId.description + ")") : "") + "<span>, " + (item.parentId && item.parentId.parentId ? item.parentId.parentId.codeValue : "") + "</span>" + "</p>" + "</div>")
                            .appendTo(ul);
                    };
            };
            $scope.getAddressCities = function(val) {
                $('[ng-show="noResultsCity"]').html('<i class="glyphicon glyphicon-remove"></i> No Results Found');
                if (val.length > 2) {
                    return DirectoryCityService.list({
                        codeValue: {
                            _eval: 'regex',
                            value: val
                        },
                        active: true
                    }).$promise;
                } else $('[ng-show="noResultsCity"]').html('Enter minimum 3 characters');
            };
            $scope.selectAddressCityTypeAhead = function($item, $model, $label, $event) {
                $scope.subscriber.city = $model.codeValue;
                $scope.subscriber.state = $model.parentId ? $model.parentId.codeValue + " (" + $model.parentId.description + ")" : "";
                $scope.subscriber.country = $model.parentId && $model.parentId.parentId ? $model.parentId.parentId.codeValue : "";
            };
            $scope.initAddressStates = function() {
                $("#auto-complete-state")
                    .autocomplete({
                        source: function(req, res) {
                            $scope.getAddressStates(req.term).then(function(data) {
                                res(data);
                            });
                        },
                        focus: function(event, ui) {
                            $("#auto-complete-state").val(ui.item.codeValue + " (" + ui.item.description + ")");
                            return false;
                        },
                        minLength: 3,
                        select: function(evt, ui) {
                            $scope.selectAddressStateTypeAhead(ui.item, ui.item);
                            $('#auto-complete-state').val($scope.subscriber.state);
                            $('#auto-complete-country').val($scope.subscriber.country);
                            return false;
                        }
                    })
                    .autocomplete("instance")._renderItem = function(ul, item) {
                        return $("<li>")
                            .append("<div>" + item.codeValue + " (" + item.description + ")" + "<span>, " + (item.parentId ? item.parentId.codeValue : "") + "</span>" + "</div>")
                            .appendTo(ul);
                    };
            };
            $scope.getAddressStates = function(val) {
                $('[ng-show="noResultsState"]').html('<i class="glyphicon glyphicon-remove"></i> No Results Found');
                if (val.length > 2) {
                    return DirectoryStateService.list({
                        codeValue: {
                            _eval: 'regex',
                            value: val
                        },
                        active: true
                    }).$promise;
                } else $('[ng-show="noResultsState"]').html('Enter minimum 3 characters');
            };
            $scope.selectAddressStateTypeAhead = function($item, $model, $label, $event) {
                $scope.subscriber.state = $model.codeValue + " (" + $model.description + ")";
                $scope.subscriber.country = $model.parentId.codeValue;
            };
            $scope.initAddressCountries = function() {
                $("#auto-complete-country")
                    .autocomplete({
                        source: function(req, res) {
                            $scope.getAddressCountries(req.term).then(function(data) {
                                res(data);
                            });
                        },
                        focus: function(event, ui) {
                            $("#auto-complete-country").val(ui.item.codeValue);
                            return false;
                        },
                        minLength: 3,
                        select: function(evt, ui) {
                            $scope.selectAddressCountryTypeAhead(ui.item, ui.item);
                            $('#auto-complete-country').val($scope.subscriber.country);
                            return false;
                        }
                    })
                    .autocomplete("instance")._renderItem = function(ul, item) {
                        return $("<li>")
                            .append("<div>" + item.codeValue + "</div>")
                            .appendTo(ul);
                    };
            };
            $scope.getAddressCountries = function(val) {
                $('[ng-show="noResultsCountry"]').html('<i class="glyphicon glyphicon-remove"></i> No Results Found');
                if (val.length > 2) {
                    return DirectoryCountryService.list({
                        codeValue: {
                            _eval: 'regex',
                            value: val
                        },
                        active: true
                    }).$promise;
                } else $('[ng-show="noResultsCountry"]').html('Enter minimum 3 characters');
            };
            $scope.selectAddressCountryTypeAhead = function($item, $model, $label, $event) {
                $scope.subscriber.country = $model.codeValue;
            };

            $scope.update = function() {
                $scope.loading = true;
                $scope.subscriber.completeName = [$scope.subscriber.name.first, $scope.subscriber.name.middle, $scope.subscriber.name.last].join(" ");
                $scope.subscriber.$update(function(res) {
                    showMessages([{
                        type: 'success',
                        message: 'Saved Successfully!',
                        header: 'Profile'
                    }]);
                    $state.reload();
                }, function(err) {
                    console.log(err);
                    showMessages([{
                        type: 'error',
                        message: 'Could not save! Please try again later!',
                        header: 'Profile'
                    }]);
                })
            };

            $scope.changePassword = function() {
                $scope.loading = true;
                $scope.subscriber.$changePassword(function(data) {
                    showMessages([{
                        type: 'success',
                        message: 'Password changed Successfully!',
                        header: 'Change Password'
                    }]);
                    if ($scope.requestPasswordChange) $state.go('core.search', { navigateTo: '#search' }, { reload: true })
                    else $state.reload();
                }, function(error) {
                    console.log('Change pass. error', error);
                    var msg = error && error.data ? error.data.message : false;
                    showMessages([{
                        type: 'error',
                        message: (msg || 'Password could not be changed! Please try again later!'),
                        header: 'Change Password'
                    }]);
                })
            };

            setTimeout(initPage, 100);

            function initPage() {
                $scope.initAddressCities();
                $scope.initAddressStates();
                $scope.initAddressCountries();
            }

        }
    ]);
