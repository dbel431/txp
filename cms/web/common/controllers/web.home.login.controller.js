'use strict';
angular.module('web')
    .controller('web.home.login.controller', [
        '$rootScope', '$scope', '$state', '$stateParams', '$localStorage', '$location', 'SubscriberService', '$sce',
        function($rootScope, $scope, $state, $stateParams, $localStorage, $location, SubscriberService, $sce) {
            
            localStorage.url = $rootScope.currentDirectory.name;
            $scope.subscriber = new SubscriberService({
                directoryId: $rootScope.currentDirectory.directoryId
            });
            $scope.dirName=$stateParams.directoryName;
            $scope.login = function() {
                localStorage.tabHrefFlag=0;
                $scope.subscriber.auth.secret = localStorage.getItem('NRP-FINGERPRINT');
                $scope.subscriber.$login(function(data, response) {
                    console.log('data.orders',data.email.orders[0].package);
                    //57a6c36debba3d886256dba3
                    //'57a6c362ebba3d886256dba1' print version id
                    var loginPass=[],printPkg=0;
                    for(var i=0;i<data.email.orders.length;i++)
                    {
                        if(data.email.orders[i].confirmed.flag ==true)   
                        {
                            loginPass.push(data.email.orders[i].package);
                        }
                        if(data.email.orders[i].confirmed.flag ==true && data.email.orders[i].package=='57a6c362ebba3d886256dba1')
                        {
                            printPkg+=1;
                        }
                    }
                   if(loginPass.length == printPkg)
                    {
                        $localStorage['NRP-USER-TOKEN'] = data.token;
                        $localStorage['PKG'] ="57a6c362ebba3d886256dba1";
                        $state.go('core.profile');
                    }
                    else
                    {
                        $localStorage['NRP-USER-TOKEN'] = data.token;
                        if (data.requestPasswordChange) 
                        $state.go('core.profile', { requestPasswordChange: true, navigateTo: '#change-pass-div' }, { reload: true });
                    else 
                        $state.go('core.search', { navigateTo: '#search', navigatePosition : 50 }, { reload: true });
                    }
                    showMessages([{
                        header: 'Login',
                        type: 'success',
                        message: 'Login Successful!'
                    }]);

                }, function(error) {
                    console.log('login err', error);
                    var msg = error && error.data ? error.data.message : false;
                    if('Please Sign Up before you Login!'===msg)
                    {
                         $state.go('core.register');
                    }
                    showMessages([{
                        header: 'Login',
                        type: 'error',
                        message: (msg || 'Could not Login successfully!')
                    }]);
                });
            };
        }
    ]);
