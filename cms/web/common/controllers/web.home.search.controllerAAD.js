'use strict';
angular.module('web')
    .controller('DirectoryWebUserSearchControllerAAD', ['dataCommonService', '$rootScope', '$scope', '$localStorage', '$location', '$dataService', '$sce', '$state', '$stateParams', '$compile', '$resource', '$uibModal', 'DTOptionsBuilder', 'DTColumnBuilder', 'DirectoryOrganizationService', 'DirectoryInstitutionCategory', 'DirectoryCollectionCategory', 'DirectoryWebUserService', 'DirectoryCityService', 'DirectoryStateService', 'DirectorySpecificationType', 'DirectorySpecificationCodeForIndustryPreference', 'DirectoryOfficerType', 'DirectorySpecificationCodeForGeographicalPreference', 'DirectorySpecificationCodeForPortfolioRelationship', 'DirectorySpecificationCodeForFinancingPreferred', 'DirectorySpecificationCodeForDealType', 'DirectorySpecificationCodeForFirmWillTakeActiveRoleAs', 'ListingType', 'DisbursalType',
    function(dataCommonService,$rootScope, $scope, $localStorage, $location, $dataService, $sce, $state, $stateParams, $compile, $resource, $uibModal, DTOptionsBuilder, DTColumnBuilder, DirectoryOrganizationService, DirectoryInstitutionCategory, DirectoryCollectionCategory, DirectoryWebUserService, DirectoryCityService, DirectoryStateService, DirectorySpecificationType, DirectorySpecificationCodeForIndustryPreference, DirectoryOfficerType, DirectorySpecificationCodeForGeographicalPreference, DirectorySpecificationCodeForPortfolioRelationship, DirectorySpecificationCodeForFinancingPreferred, DirectorySpecificationCodeForDealType, DirectorySpecificationCodeForFirmWillTakeActiveRoleAs, ListingType, DisbursalType) {
            // if (!$localStorage['NRP-USER-TOKEN'])
            // {
            //     localStorage.refineSearch='';
            //     $state.go('core.login');
            // }
            var vm = this;
            /*ASD@123 holders start*/
            $scope.InstitutionCategoryArrayDataSettings={enableSearch: true, scrollableHeight: '250px', scrollable: true, scrollableWidth:'100%', externalIdProp: ''};
            $scope.keywordDropDownSettings={scrollableHeight: 'auto', scrollable: true, scrollableWidth:'100%', externalIdProp: ''};
            $('#searchData').hide();
            function init()
            {
            console.log("init");
            $scope.keywordDropDownOptions=[{'id':'features.name','label':'Collection Description'}, {'id':'exhibition.exhibitName','label':'Exhibit Description'}];
            $scope.keywordDropDown=[{'id':'features.name','label':'Collection Description'}, {'id':'exhibition.exhibitName','label':'Exhibit Description'}];
            $scope.stateName1='';
            $scope.stateDatadata=[];
            $scope.keyword='';
            $scope.keywordSearchAllFlag=true;
            $scope.firstName=""; // personal
            $scope.lastName=""; // personal
            $scope.name=""; //done
            $scope.cityName=""; // done
            $scope.stateName1="";   //done
            $scope.establishedDetail=""; //done
            $scope.activitiesEducationDept=""; //done
            $scope.galleryDescription=""; //done
            $scope.degreeGranted=""; //done
            $scope.zip="";             //done
            $scope.collectionDescription=""; //done
            $scope.instituteNameAllFlag=false; // done
            $scope.establishedDetailbFlag=false;    //dome
            $scope.galleryDescriptionFlag=false;    //done
            $scope.scholarship=false;           //done
            $scope.fellowship=false;
            $scope.assistantship=false; //done
            $scope.grants=false; //done
            $scope.activitiesEducationDeptbFlag=false; //done flag
            $scope.collectionDescriptionAllFlag=false;  //done flag
            vm.attendanceMethod="";
            vm.attendanceCount="";
            vm.attendanceCounts="";
            }
            //table start
            var xtempname={
                directorCEO:
                DTColumnBuilder.newColumn('name','flags')                
                .renderWith(function(name,type, row) {
                    
                    if($scope.tabHref==2)
                    {
                        console.log("$scope.lastName",$scope.lastName);
                        console.log("$scope.firstName",$scope.firstName);
                        if(($scope.lastName && $scope.firstName==undefined) || ($scope.lastName && $scope.firstName=="")){
                        var getPer=row.personnel.filter(function(val){
                            if((val.name.last.toUpperCase() == $scope.lastName.toUpperCase() )|| val.name.last.toUpperCase().indexOf($scope.lastName.toUpperCase()) != -1 )
                            {
                                return val;
                            }
                        }); 
                        row.personnel= getPer;   
                        }
                        if(($scope.firstName && $scope.lastName==undefined )|| ($scope.firstName && $scope.lastName=="" )){
                        var getPer=row.personnel.filter(function(val){
                            if((val.name.first.toUpperCase() == $scope.firstName.toUpperCase() )|| val.name.first.toUpperCase().indexOf($scope.firstName.toUpperCase()) != -1 )
                            {
                                return val;
                            }
                        }); 
                        row.personnel= getPer;   
                        }
                        if($scope.firstName && $scope.lastName){
                        var getPer=row.personnel.filter(function(val){
                            if(((val.name.first.toUpperCase() == $scope.firstName.toUpperCase() )|| (val.name.first.toUpperCase().indexOf($scope.firstName.toUpperCase()) != -1))&& ((val.name.last.toUpperCase() == $scope.lastName.toUpperCase() )|| (val.name.last.toUpperCase().indexOf($scope.lastName.toUpperCase()) != -1 )) )
                            {
                                return val;
                            }
                        }); 
                        row.personnel= getPer;   
                        }
                    }

                    var str='';
                    if(row.personnel.length > 0)
                        str=row.personnel[0].name.last+', '+row.personnel[0].name.first;
                    return str;
                })
                .withOption('width', '10%'),
                name:
                DTColumnBuilder.newColumn('name','flags')
                .withTitle('Institution Name')
                .renderWith(function(name,type, row) {
                    if(name.split(' ')[0].toUpperCase()=='THE')
                        name=name.replace('The','')+',The';
                    if(row.flags)
                    {
                    if(row.flags.aam)
                        name=name+'        <img src="/assets/aam-icon.png" width=15 height=15>';
                    if(row.flags.icom)
                        name=name+'        <img src="/assets/icom-icon.png" width=20 height=10> ';
                    if(row.flags.accredited)
                        name=name+'        <img src="/assets/star_icon.png" width=15 height=15>';
                     if(row.flags.handicapped)
                         name=name+'      <img src="/assets/Handicapped-Accessible.png" width=15 height=15>';
                     }

                    return name||"";
                })
                .withOption('width', '20%'),
                personnelTitle:
                DTColumnBuilder.newColumn('personnel','flags')
                .withTitle('personnelTitle')
                .renderWith(function(name,type, row) {
                    return 'personnelTitle456';
                })
                .withOption('width', '20%'),
                personnel:DTColumnBuilder.newColumn('personnel.titleMasterName')
                .withTitle('Title')
                .renderWith(function(personnel, type, row) {
                    if($scope.tabHref==2)
                    {
                        console.log("$scope.lastName",$scope.lastName);
                        console.log("$scope.firstName",$scope.firstName);
                        if(($scope.lastName && $scope.firstName==undefined) || ($scope.lastName && $scope.firstName=="")){
                        var getPer=row.personnel.filter(function(val){
                            if((val.name.last.toUpperCase() == $scope.lastName.toUpperCase() )|| val.name.last.toUpperCase().indexOf($scope.lastName.toUpperCase()) != -1 )
                            {
                                return val;
                            }
                        }); 
                        row.personnel= getPer;   
                        }
                        if(($scope.firstName && $scope.lastName==undefined )|| ($scope.firstName && $scope.lastName=="" )){
                        var getPer=row.personnel.filter(function(val){
                            if((val.name.first.toUpperCase() == $scope.firstName.toUpperCase() )|| val.name.first.toUpperCase().indexOf($scope.firstName.toUpperCase()) != -1 )
                            {
                                return val;
                            }
                        }); 
                        row.personnel= getPer;   
                        }
                        if($scope.firstName && $scope.lastName){
                        var getPer=row.personnel.filter(function(val){
                            if(
                                (
                                (val.name.first.toUpperCase() == $scope.firstName.toUpperCase() )|| 
                                (val.name.first.toUpperCase().indexOf($scope.firstName.toUpperCase()
                                ) != -1))&& ((val.name.last.toUpperCase() == $scope.lastName.toUpperCase() )|| (val.name.last.toUpperCase().indexOf($scope.lastName.toUpperCase()) != -1 )) )
                            {
                                
                                return val;
                            }
                        }); 
                        row.personnel= getPer;   
                        }
                    }
                    var str='';
                    if(row.personnel.length > 0 && row.personnel[0].title)
                        return row.personnel[0].title;
                    else
                        return '';
                }).withOption('width', '4%'),
                cityName:DTColumnBuilder.newColumn('address.cityName')
                .withTitle('City')
                .renderWith(function(city, type, row) {
                    if(row.address)
                   var zzz=row.address.filter(function(v){if(v.addressType=='576286d0c19cef300bc720e6'){
                            return 1;
                   }});
               else
                return "";
//                    return ((row.address && zzz.address[0] && zzz.address[0].cityName) ? (zzz.address[0].cityName || "") : "") || "";
                    return ((row.address && row.address[0] && row.address[0].cityName + '45') ? (row.address[0].cityName  || "") : "") || "";
                }).withOption('width', '10%'),
                statedescription:DTColumnBuilder.newColumn('address.state.description')
                    .withTitle('State/Territory')
                .renderWith(function(data, type, full){
                       if(full.address)
                    var zzz=full.address.filter(function(v){if(v.addressType=='576286d0c19cef300bc720e6'){
                            return 1;
                   }}); else return "";
                    if(zzz.length > 1)
                    return ((full.address && full.address[0] && full.address[0].state.description) ? (full.address[0].state.description|| "") : "") || "";
                    else
                    return ((full.address && full.address[0] && full.address[0].state && full.address[0].state.description) ? (full.address[0].state.description|| "") : "") || "";

                })
                .withOption('width', '2%'),
                 zip:DTColumnBuilder.newColumn('address.zip')
                .withTitle('zip')
                .renderWith(function(data, type, full){
                    return (full.address && full.address[0] && full.address[0].zip)?full.address[0].zip:'';
                })
                .withOption('width', '8%'),
                actions:DTColumnBuilder.newColumn('_id')
                .withTitle('Actions')
                .notSortable()
                .withClass('text-center')
                .renderWith(function(id,type, full) {
                if($rootScope.currentDirectory.directoryId == '57189c7024d8bc65f4123bc0')
                    return '<div ng-click=vm.advanceSearchResetFunctionAAD()><directory-web-user-record-actions record-id="\'' + id + '\'"></directory-web-user-record-actions></div>';
                })
                .withOption('width', '4%'),
                personnelName:DTColumnBuilder.newColumn('personnel.titleMasterName')
                .withTitle('Pesonnel')
                .renderWith(function(personnel, type, row) {
                    console.log('Pesonnel',row);
                    if (row.personnel) {
                        for (var i = 0; i < row.personnel.length; i++) {
                            var pers = row.personnel[i];
                            if (($scope.firstName !== undefined) && ($scope.firstName !== '')){

                                var var1 = new RegExp($scope.firstName,"i");
                                if (var1.test(pers.name.first)) {
                                    if(pers.name.prefix)
                                        var director = pers.name ? '<p> ' +pers.name.prefix+" "+ [pers.name.first,pers.name.middle, pers.name.last].join(' ') +'</p>' : '';
                                    if(pers.name.suffix)
                                        var director = pers.name ? '<p> '+ [pers.name.first,pers.name.middle, pers.name.last].join(' ') + " "+pers.name.suffix + '</p>' : '';
                                    if(pers.name.suffix && pers.name.prefix )
                                     var director = pers.name ? '<p> ' +pers.name.prefix+" "+ [pers.name.first,pers.name.middle, pers.name.last].join(' ') + " "+pers.name.suffix +'</p>' : '';
                                    if(pers.name.first && pers.name.last)
                                    {
                                        return pers.name.first+" "+pers.name.last;
                                    }
                                    return director || "";
                                }
                            }
                            else if (($scope.lastName !== undefined) && ($scope.lastName !== '')){

                                var var1 = new RegExp($scope.lastName,"i");
                                if (var1.test(pers.name.last)) {

                                    var director = pers.name ? '<p>' + [pers.name.first,pers.name.middle, pers.name.last].join(' ') + '</p>' : '';
                                    return director || "";
                                }
                            }
                            else{
                                 var director = row.personnel[0].name ? '<p>' + [row.personnel[0].name.first,row.personnel[0].name.middle, row.personnel[0].name.last].join(' ') + '</p>' : '';
                                    return director || "";
                            }
                        }
                
                    }
                    return '';
                }).withOption('width', '9%')
                };
            //table end
            vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
            /*function define start*/
            vm.newSearchAAD=newSearchAAD;
            vm.refinesearchAAD=refinesearchAAD;
            vm.gotoDiv=gotoDiv;
            vm.pSearch=pSearch;
            vm.advanceSearchResetFunctionAAD=advanceSearchResetFunctionAAD;
            vm.keywordSearch=keywordSearch;
            vm.institutionalSearch=institutionalSearch;
            vm.init=init;
            /*function define end*/
            if(localStorage.state == undefined)
            {
                console.log("localStorage.state_245");
                init();  
                $('#searchData').hide();
                if(localStorage.refineSearch !== "" && localStorage.refineSearch !== undefined)
                {
                    var temp=JSON.parse(localStorage.refineSearch);
                    if(temp == undefined)
                    {
                  //  $("#searchData").show();
                    $scope.tabHref = 1;
                    vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                    init();    
                    }
                    else
                    {
                         vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                         $scope.tabHref =temp.tabHref.value;
                    }
                }else
                {
                    $scope.tabHref = 1;
                    vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                    init();  
                }                
            }
            if(localStorage.state=='new')
            {
                console.log("localStorage.state_245");
                delete localStorage.state;
                init();  
                $('#searchData').show();
                if(localStorage.refineSearch !== "" && localStorage.refineSearch !== undefined)
                {
                    var temp=JSON.parse(localStorage.refineSearch);
                    delete localStorage.refineSearch;
                    if(temp == undefined)
                    {
                  //  $("#searchData").show();
                    $scope.tabHref = 1;
                    vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                    init();    
                    }
                    else
                    {
                         vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                         $scope.tabHref =temp.tabHref.value;
                    }
                }else
                {
                    $scope.tabHref = 1;
                    vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                    init();  
                }

            }
            if(localStorage.state=='ref')
            {
                delete localStorage.state;
                console.log("localStorage.state_new_278",localStorage.state);
                init();  
                $('#searchData').show();
                if(localStorage.refineSearch !== "" && localStorage.refineSearch !== undefined)
                {
                    var temp=JSON.parse(localStorage.refineSearch);
                    delete localStorage.refineSearch;
                    if(temp == undefined)
                    {
                    $scope.tabHref = 1;
                    vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                   
                    }
                    else
                    {

                         $scope.tabHref =temp.tabHref.value;
                         if(temp.tabHref.value==2)
                         {
                            vm.dtC = [xtempname.directorCEO.withTitle('Name'),xtempname.personnel,xtempname.name,xtempname.cityName, xtempname.statedescription, xtempname.actions ];
                         }
                         if(temp.tabHref.value==1)
                         {
                         vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];   
                         }if(temp.tabHref.value==3)
                         {
                         vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];   
                         }
                         tempToScope(temp);
                         setTimeout(function(){
                            searchFunction($scope.tabHref);
                        },100);
                         
                    }
                }else
                {
                    $scope.tabHref = 1;
                    vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                    init();  
                }
            }
            function institutionalSearch()
            {
                console.log("institutionalSearch");
                searchFunction(1);
                gotoDiv('#gotoSearch');
            }
            function keywordSearch()
            {
                 console.log("keywordSearch");
                    var qr={
                    "eFlag":true,
                    "org":{"directoryId":"57189c7024d8bc65f4123bc0"},
                    "exhi":{"$or":[getQr()]}};
                    keyword(qr);
                    gotoDiv('#gotoSearch');
            }
            function tempToScope(temp)
            {
                console.log("tempToScope");
                var arr=Object.keys(temp);
                for(var i=0;i<arr.length;i++)
                {
                    var mkey =arr[i];
                    var val=temp[mkey].val;
                    var key=temp[mkey].key;
                    if(temp[val])
                    if(temp[val].value!=='' && temp[val].value!==undefined)
                    {
                        var tempKey=temp[val].key;
                        var tempVal=temp[val].val;
                        var tempValue=temp[val].value;
                        if(tempKey == 'Scope')
                        {
                            $scope[val]=tempValue;
                        }
                        if(tempKey == 'vm')
                        {
                         vm[val]=tempValue;
                        }
                    }
                }

            }
            function advanceSearchResetFunctionAAD()
            {
                console.log("advanceSearchResetFunctionAAD");
                var arr=[{"key":"Scope","val":"firstName"},{"key":"Scope","val":"lastName"},{"key":"Scope","val":"name"},{"key":"Scope","val":"cityName"},{"key":"Scope","val":"stateName1"},{"key":"Scope","val":"establishedDetail"},{"key":"Scope","val":"activitiesEducationDept"},{"key":"Scope","val":"galleryDescription"},{"key":"Scope","val":"degreeGranted"},{"key":"Scope","val":"zip"},{"key":"Scope","val":"collectionDescription"},{"type":"flag","key":"Scope","val":"instituteNameAllFlag"},{"type":"flag","key":"Scope","val":"establishedDetailbFlag"},{"type":"flag","key":"Scope","val":"galleryDescriptionFlag"},{"type":"flag","key":"Scope","val":"scholarship"},{"type":"flag","key":"Scope","val":"fellowship"},{"type":"flag","key":"Scope","val":"assistantship"},{"type":"flag","key":"Scope","val":"grants"},{"type":"flag","key":"Scope","val":"activitiesEducationDeptbFlag"},{"type":"flag","key":"Scope","val":"collectionDescriptionAllFlag"},{"key":"vm","val":"attendanceMethod"},{"key":"vm","val":"attendanceCount"},{"key":"vm","val":"attendanceCounts"},{"key":"Scope","val":"tabHref"},{"key":"Scope","val":"keyword"},{"key":"Scope","val":"keywordDropDown"},{"type":"flag","key":"Scope","val":"keywordSearchAllFlag"},{"key":"Scope","val":"stateName1"},{"key":"Scope","val":"stateDatadata"},{"type":"flag","key":"Scope","val":"keywordSearchAllFlag"}];
                var temp={};
                for(var i=0;i<arr.length;i++)                
                {
                        var val=arr[i].val;
                        var key=arr[i].key;
                    if(arr[i].key=='Scope' && $scope[arr[i].val]!== undefined && $scope[arr[i].val]!== '')
                    {
                        var value=$scope[arr[i].val];
                        temp[val]={'key':key,'val':val,'value':value};
                    }
                    if(arr[i].type=='flag' && arr[i].key =='Scope')
                    {
                        var value=$scope[arr[i].val];
                        temp[val]={'key':key,'val':val,'value':value};
                    }
                    if(arr[i].key=='vm' && vm[arr[i].val]!== undefined && vm[arr[i].val]!== '')
                    {   var value=vm[arr[i].val];
                        temp[val]={'key':key,'val':val,'value':value};
                    }
                }
                localStorage.refineSearch=JSON.stringify(temp);
            }
            function pSearch()
            {
                console.log("pSearch");
                var qr={org:{"directoryId":'57189c7024d8bc65f4123bc0'},
                    per:getQr()
                };
                persone(qr);
                gotoDiv('#gotoSearch');
            }
            function gotoDiv(name)
            {
                 console.log("gotoDiv");
                 $('#searchData').show();
                $('html,body').animate({scrollTop: $(name).offset().top-50},'slow');
            }
            function newSearchAAD()
            {
                  console.log("newSearchAAD");
                init();
                vm.gotoDiv('#containerBox');
            }
            function refinesearchAAD()
            {
                console.log("refinesearchAAD");
                vm.gotoDiv('#containerBox');
            }
            function org(qr)
            {
                 console.log("org");
                    var promise = $resource(APIBasePath + 'subscriber/search/org-list', {}, {
                        dataTable: {
                            url: APIBasePath + 'subscriber/search/org-list',
                            method: 'POST',
                            transformRequest: function(data, headerGetter) {
                                var headers = headerGetter();
                                var newData = {
                                    dataTableQuery: data,
                                    conditions: qr
                                    }
                                return JSON.stringify(newData);
                                }
                            }
                        });
                        initDatatable(promise.dataTable);
                        return promise;
            }
            function persone(qr)
            {
                    console.log("persone");
                var promise = $resource(APIBasePath + 'subscriber/search/personnel/list', {}, {
                        dataTable: {
                            url: APIBasePath + 'subscriber/search/personnel/list',
                            method: 'POST',
                            transformRequest: function(data, headerGetter) {
                                var headers = headerGetter();
                                var newData = {
                                    dataTableQuery: data,
                                    conditions: qr,
                                    persone:1
                                    }
                                return JSON.stringify(newData);
                                }
                            }
                        });
                        initDatatable(promise.dataTable);
                        return promise;
            }
            function keyword(qr)
            {
                   console.log("keyword");
                var promise = $resource(APIBasePath + 'subscriber/search/exhibition/list', {}, {
                        dataTable: {
                            url: APIBasePath + 'subscriber/search/exhibition/list',
                            method: 'POST',
                            transformRequest: function(data, headerGetter) {
                                var headers = headerGetter();
                                var newData = {
                                    dataTableQuery: data,
                                    conditions: qr,
                                    persone:1
                                    }
                                return JSON.stringify(newData);
                                }
                            }
                        });
                        initDatatable(promise.dataTable);
                        return promise;
            }
            /*ASD@123 holders end*/
            /*string to number function start*/
            function strtonumber(data)
            {
                console.log("strtonumber");
                var temp=data;
                for(var i=0;i<data.split(',').length;i++)
                {
                    temp=temp.replace(',','');
                }
                return temp;
            }
            $scope.$watch('cityName', function(newValue, oldValue) {
                var z1={
                    _where: {codeTypeId:'57174a5d6778ef1c153aec6f',codeValue: {_eval: 'regex',value: newValue},parentId:{'$ne':null},active: true},
                     _limit: 50
                };
                if(newValue!=="" && newValue!= undefined)
                $dataService.post('directory/city',z1,function(data){
                    var city=data.map(function(val){
                        return val.codeValue;
                    });
                    var city2=[];
                    for(var i=0;i<city.length;i++)
                    {
                            if(city2.indexOf(city[i])== -1)
                            {
                             city2.push(city[i]);
                            }
                    }
                    $scope.cityData=city2
                            .map(function(val){
                                return {id:val,label:val};                      
                                });

                              $( "#CityAuto" ).autocomplete({
                                  source:$scope.cityData,
                                   select: function( event, ui ) { 
                                    $("#CityAuto").val(ui.item.id);
                                    return false;
                                   }
                                });
                              
                            },function(error){
                        });
            
            });
            function stateGetData(newValue)
            {
                var arr=[];
                    arr.push({codeValue:{_eval: 'regex',value: newValue}});
                    arr.push({description:{_eval: 'regex',value: newValue}});
                var z1={
                    _where: {"parentId":"5718d7abca75ca39db934831"},
                    _limit: 5000
                };
                if(newValue!=="" && newValue!= undefined)
                $dataService.post('directory/state',z1,function(data){
                            $scope.stateData=data.map(function(data){
                                 return {id:data.codeValue,label:data.description?data.codeValue+" ("+data.description+")":data.codeValue};
                                });
                                $( "#StateAuto" ).autocomplete({
                                  source:$scope.stateData
                                });
                                     $( "#StateAuto" ).autocomplete({
                                  source:$scope.stateData,
                                   select: function( event, ui ) { 
                                    $("#StateAuto").val(ui.item.id);
                                    return false;
                                   }
                                });
                            },function(error){
                        });
            }
            stateGetData('a');

                $scope.$watch('stateName1', function(newValue, oldValue) {
                 stateGetData(newValue);
 

            });
         /*ASD@123 watch*/
            vm.dtI = null;
            vm.dataOfData={};
            vm.dataOfAnd={};
   
         //   $("#searchData").hide();
            //ASD@123 search
            $scope.searchFunction =searchFunction;
            function searchFunction(data)
            {
                console.log("searchFunction");
                console.log('data',data);
                if(data==1)  {
                var qr=getQr();
                qr['directoryId']=$rootScope.currentDirectory.directoryId;
                org(qr);    
                }
                if(data==2)
                {
                    var qr={org:{"directoryId":'57189c7024d8bc65f4123bc0'},
                    per:getQr()
                };
                persone(qr);
                }
                if(data==3)
                {
                    var qr={
                    "eFlag":true,
                    "org":{"directoryId":"57189c7024d8bc65f4123bc0"},
                    "exhi":{"$or":[getQr()]}};
                    keyword(qr);
                }
            } 
            function getQr(){
            console.log('getQr');
                    var qrObj={};
                    if($scope.keywordSearchAllFlag==true && ($scope.keyword !== undefined) && ($scope.keyword !== ''))
                    {
                        var _keywordDropDown=[];
                        console.log($scope.keywordDropDown);
                        for(var i=0;i<$scope.keywordDropDown.length;i++)
                        {
                            var k={};
                            k[$scope.keywordDropDown[i].id]=$scope.keyword;
                            _keywordDropDown.push(k);
                        }
                        qrObj['$or']=_keywordDropDown;
                    }
                    if($scope.keywordSearchAllFlag==false && ($scope.keyword !== undefined) && ($scope.keyword !== ''))
                    {
                        var _keywordDropDown=[];
                        console.log($scope.keywordDropDown);
                        for(var i=0;i<$scope.keywordDropDown.length;i++)
                        {
                                    var k={};
                            k[$scope.keywordDropDown[i].id]={ _eval: 'regex', value: $scope.keyword };
                            _keywordDropDown.push(k);
                        }
                        qrObj['$or']=_keywordDropDown;
                    }
                    if (($scope.firstName !== undefined) && ($scope.firstName !== ''))
                    {
                        qrObj['personnel.name.first']={ _eval: 'regex', value: $scope.firstName };
                    }
                    if (($scope.lastName !== undefined) && ($scope.lastName !== ''))
                    {
                        qrObj['personnel.name.last']={ _eval: 'regex', value: $scope.lastName };
                    }

                    if (($scope.name !== undefined) && ($scope.name !== '')) {
                    if ($scope.instituteNameAllFlag == true)
                    {
                            qrObj['name']={ _eval: 'regex', value: $scope.name };
                    }
                    if($scope.instituteNameAllFlag == false)
                        {
                        var obj=[];
                        var arrname=$scope.name.split(' ');
                        obj.push({ _eval: 'regex', value:$scope.name})
                        for(var i=0;i<arrname.length;i++)
                        {
                            if(arrname[i]!=='')
                            {
                                var sname=arrname[i];
                                obj.push({ _eval: 'regex', value:sname})
                            }
                        }
                        qrObj['name']={'$in':obj};
                        }
                    $scope.enable = false;
                }
                if($('#CityAuto').val()!=='' && $('#CityAuto').val()!==undefined)
                {
                   qrObj['address.cityName']=$('#CityAuto').val();
                }
                if ($scope.stateDatadata.length> 0) {
                        qrObj['address.stateName']={'$in':[]};
                    for (var i = $scope.stateDatadata.length - 1; i >= 0; i--) {
                          qrObj['address.stateName']['$in'].push($scope.stateDatadata[i].id);
                    }
                }
                if (($scope.establishedDetail !== undefined) && ($scope.establishedDetail !== ''))
                {
                    if($scope.establishedDetailbFlag==true){
                    qrObj['established.detail']=$scope.establishedDetail;
                    }else{
                    qrObj['established.detail']={ _eval: 'regex', value: $scope.establishedDetail };
                    }
                }
                if (($scope.galleryDescription !== undefined) && ($scope.galleryDescription !== ''))
                {
                   if($scope.galleryDescriptionFlag==true)
                    qrObj['galleryDescription']=$scope.galleryDescription;
                    else
                    qrObj['galleryDescription']={ _eval: 'regex', value: $scope.galleryDescription }
                }
            if (($scope.activitiesEducationDept !== undefined) && ($scope.activitiesEducationDept !== ''))
                {
                    if($scope.activitiesEducationDeptbFlag==true)
                    {
                    qrObj['activities.educationDept']=$scope.activitiesEducationDept;
                    }
                    else
                    {
                    qrObj['activities.educationDept']={ _eval: 'regex', value: $scope.activitiesEducationDept };
                    }
                }
                if (($scope.degreeGranted !== undefined) && ($scope.degreeGranted !== ''))
                    {
                       qrObj['artSchool.degreeGranted']={ _eval: 'regex', value: $scope.degreeGranted };
                    }
                if (($scope.zip !== undefined) && ($scope.zip !== ''))
                {
                    qrObj['address.zip']={_eval: 'regex', value: $scope.zip };
                }
                if (($scope.collectionDescription !== undefined) && ($scope.collectionDescription !== '')) {
                    if ($scope.collectionDescriptionAllFlag == true){
                        qrObj['features.featureType']='COLLECTION';
                        qrObj['features.name']=$scope.collectionDescription;
                    }
                    else{
                        qrObj['features.featureType']='COLLECTION';
                        qrObj['features.name']={ _eval: 'regex', value: $scope.collectionDescription };
                    }
                }
                if (($scope.scholarship !== undefined) && ($scope.scholarship !== false))
                {
                    qrObj['artSchool.scholarship']=$scope.scholarship;
                }
                if (($scope.fellowship !== undefined) && ($scope.fellowship !== false))
                {
                    qrObj['artSchool.fellowship']=$scope.fellowship;
                }
                if (($scope.assistantship !== undefined) && ($scope.assistantship !== false)){
                  qrObj['artSchool.assistantships']=$scope.assistantship;
                }
                if (($scope.grants !== undefined) && ($scope.grants !== false))
                {
                       qrObj['artSchool.grants']=$scope.grants;
                }
                if ((vm.attendanceMethod !== undefined) && (vm.attendanceMethod !== '' && vm.attendanceMethod!==null))
                {
                    if(vm.attendanceMethod=='E')
                    {
                        var gt=parseFloat(strtonumber(vm.attendanceCount));
                        var lt=parseFloat(strtonumber(vm.attendanceCounts));
                        qrObj['attendance.countNumber']={'$gt':gt,'$lt':lt};
                    }
                    if(vm.attendanceMethod=='A')
                    {
                        var gt=parseFloat(strtonumber(vm.attendanceCount));
                        qrObj['attendance.countNumber']={'$gt':gt};
                    }
                    if(vm.attendanceMethod=='lt')
                    {
                        var lt=parseFloat(strtonumber(vm.attendanceCount));
                        qrObj['attendance.countNumber']={'$lt':lt};
                    }
                    if(vm.attendanceMethod=='eq')
                    {
                        var eq=parseFloat(strtonumber(vm.attendanceCount));
                        qrObj['attendance.countNumber']={'$eq':eq};
                    }
                    
                }
                    return qrObj;
            };

            //ASD@123 searchFunction end
   
     
   
            $scope.refinesearchAADflag=1;

  
            if (localStorage.url !== $rootScope.currentDirectory.name) {
                $state.go('core.login');
                $localStorage.$reset();
                localStorage.url = $rootScope.currentDirectory.name;
            }
            $scope.dtInstance = {};
            $scope.dtInstanceCallback = function(instance) {
                $scope.dtInstance = instance;
            };
            $scope.dataCommonService=dataCommonService;        
            vm.dtIC = function(instance) {
                vm.dtI = instance;
            };
            $scope.autocompletecity=function(data){
                   
            }
            $scope.arrayOfObjectsForIndex = {};
            $scope.totalDisplayed = 10;
            $scope.loadMore = function() {
                $scope.totalDisplayed += 10;
            };
            $scope.basicSearchResetFunction = function() {
                $scope.name = undefined;
                $scope.firstName = undefined;
                $scope.lastName = undefined;
                $scope.instituteNameAllFlag = false;
                $scope.officerType =[];

            };
            $scope.keywordSearchResetFunction = function() {
                $scope.keyword = undefined;
                $scope.keywordDropDown = undefined;
                $scope.keywordSearchAllFlag = undefined;
            };
            vm.firstLoad = true;

            function initDatatable(promise) {
                vm.dtI.changeData(promise);
                vm.dtI.reloadData();
                vm.dtI.rerender();
                vm.firstLoad = false;
            }
                 
            vm.dtO = DTOptionsBuilder.
            fromSource(DirectoryOrganizationService.dataTable)
                .withPaginationType('full_numbers')
                .withOption('order', [0, 'asc'])
                .withDataProp('data')
                .withOption('oLanguage', {
                    "sEmptyTable": "No matching records found"
                })
                .withOption('fnRowCallback', function(nRow) { $compile(nRow)($scope); })
                .withOption('responsive', true)
                .withOption('processing', true)
                .withOption('serverSide', true);


                    function accInit() {
                        setTimeout(function() {
                            $('#accordion').accordion({
                                collapsible: true,
                                active: false
                            });
                            $compile('#accordion')($scope);
                        }, 100);
                    }
                    $scope.$watch('tabHref', function(newVal) {
                    if(newVal==1){
                        vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                        accInit();  
                    }
                    if(newVal==2){
                        vm.dtC = [xtempname.directorCEO.withTitle('Name'),xtempname.personnel,xtempname.name,xtempname.cityName, xtempname.statedescription, xtempname.actions ];
                         accInit();
                    }
                    if(newVal==3)
                    {
                        $scope.keywordSearchAllFlag=true;
                        vm.dtC = [xtempname.name,xtempname.directorCEO.withTitle('Director/CEO'), xtempname.cityName, xtempname.statedescription, xtempname.zip, xtempname.actions ];
                    }
            });
        }

    ]);
