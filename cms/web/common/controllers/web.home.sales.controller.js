'use strict';
angular.module('web')
    .controller('web.home.sales.controller', ['$scope', '$location', '$dataService', '$sce', '$stateParams', '$rootScope', function($scope, $location, $dataService, $sce, $stateParams, $rootScope) {
        $scope.direcoryName = $rootScope.currentDirectory.name;
        $scope.oneAtATime = true;
        // if ($stateParams.navigateTo != '') setTimeout(function() {
        //     angular.element('html, body').stop().animate({
        //         scrollTop: ((angular.element($stateParams.navigateTo).offset().top) - 50)
        //     }, 1500, 'easeInOutExpo');
        // },200);
         $scope.showHide = function(data) {
            if (data.toggle == undefined) {
                data.toggle = 1;
            } else {
                data.toggle = !data.toggle;
            }
        };
        $dataService.get('web-data?pageName=sales',
            function(data) {
                if (data != null) {
                    $scope.sections = data;
                }
            },
            function(error) {
                console.log(error)
            })


        /*  $dataService.get('customer-feedback?direcoryName=OCD&showHome=true',
            function(data) {
                console.log(data);
                if (data != null) {
                    $scope.customerFeedbacks = data;
                }
            },
            function(error) {
                //console.log(error)
            })*/


    }])
