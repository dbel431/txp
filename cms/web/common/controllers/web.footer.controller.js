'use strict';
angular.module('web')
	.controller('web.footer.controller', ['$scope', '$location', '$dataService', '$rootScope', '$stateParams','$state','dataCommonService',function($scope, $location, $dataService, $rootScope, $stateParams,$state,dataCommonService){
		$scope.dataCommonService=dataCommonService;
		$scope.footerData=0;
        $scope.direcoryName = $stateParams.directoryName.name;
         $scope.stload=function(){
         	$state.reload();
         };
		var locationPath = $location.path();
		locationPath = locationPath.split('/');
		
		$rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
			if (toState.name != 'core.home') {
				$scope.pageType = "inner";
			}
			if (toState.name == 'core.home') {
				$scope.pageType = "home";
			}
		});

		if (locationPath[2] === "home") {
			$scope.pageType = "home";
		} else {
			$scope.pageType = "inner";
		}

		function getMenus() {
			$dataService.get('menu?menuType=footer', function(data){
				
				if (data != null || data.length < 0) {
					
					
					$scope.footer = data;
					$scope.footerData=1;
					
					
				}
			}, function(error){
				
			})			
		}

			
		var path = $location.path();

		// path = path.replace('/', '');

		// console.log(path);


		if (path == 'OCD') {
			$scope.logo = {
				route:'OCD/home',
				image : 'assets/logo_ocd.png',
				title : 'Official Catholic Directory'
			}
			
		} else if(path == 'OMD'){
			$scope.logo = {
				route:'/home',
				image : 'assets/logo_omd.png',
				title : 'O M D'
			}
			
		}else if(path == 'CFS'){
			$scope.logo = {
				route:'/home',
				image : 'assets/logo_omd.png',
				title : 'C F S'
			}
			
		}
		


		function goTop(){
			$location.hash('page-top')
			$anchorScroll();
		};

		getMenus();
	}])