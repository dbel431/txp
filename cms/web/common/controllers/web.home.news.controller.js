'use strict';
angular.module('web')
    .controller('web.home.news.controller', ['$scope', '$location', '$dataService', '$sce', 'FeedService', '$rootScope', function($scope, $location, $dataService, $sce, FeedService, $rootScope) {
        $scope.direcoryName = $rootScope.currentDirectory.name;


        $dataService.get('web-data?pageName=museum-news',
            function(data) {
                console.log(data);
                if (data != null) {
                    $scope.sections = data;
                }
            },
            function(error) {
                console.log(error)
            })

        if ($scope.direcoryName == 'OCD') {
            $scope.feedSrc = 'https://cnsblog.wordpress.com/feed/';
        } else if ($scope.direcoryName == 'OMD') {
            $scope.feedSrc = 'http://www.museumsassociation.org/rss/news';
        } else if ($scope.direcoryName == 'AAD') {
            $scope.feedSrc = 'http://depts.washington.edu/soanews/online/news-feed/';
        }

        FeedService.parseFeed($scope.feedSrc).then(function(res) {
            var data = res.data.responseData.feed.entries;
            if ($scope.direcoryName == 'OCD') {
                for (var i = 0; i < data.length; i++) {
                    var publishDate = new Date(data[i].publishedDate);
                    data[i].publishDate = publishDate;
                }
            }
            $scope.feeds = data;
        });


    }])
