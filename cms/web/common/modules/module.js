'use strict';
angular.module('web', [
        'ui.router',
        'ngMessages',
        'ngResource',
        'ngStorage',
/*        'ngAnimate',
        'ngSanitize',*/
        'ui.bootstrap',
        //'angularXml2json',
        'datatables',
        'datatables.columnfilter',
        'angularjs-dropdown-multiselect'
        // 'cb.x2js',
    ])
    .config([
        '$locationProvider',
        '$httpProvider',
        function($locationProvider, $httpProvider) {
            $locationProvider.html5Mode(true);
            $httpProvider.interceptors.push([
                '$q',
                '$location',
                '$localStorage',
                '$stateParams',
                function($q, $location, $localStorage, $stateParams) {
                    return {
                        'request': function(config) {
                            config.headers = config.headers || {};
                            if ($localStorage['NRP-USER-TOKEN'])
                                config.headers.Authorization = 'Bearer ' + localStorage.getItem('NRP-FINGERPRINT') + ' ' + $localStorage['NRP-USER-TOKEN'];
                            return config;
                        },
                        'responseError': function(response) {
                            if ((response.status === 401 || response.status === 403)) {
                                if (!$localStorage['NRP-USER-TOKEN']) {
                                    if (arrayIntersect([
                                            ['login', 'forgot', 'register'], $location.path().split('/')
                                        ]).length > 0) return $q.reject(response);
                                } else if (response.data && response.data.expireToken) {
                                    delete $localStorage['NRP-USER-TOKEN'];
                                }
                                return {};
                            }
                            return $q.reject(response);
                        }
                    };
                }
            ]);
        }
    ])
    // to remove template cache
    .run([
        '$state',
        '$rootScope',
        '$templateCache',
        '$localStorage',
        '$stateParams',
        function($state, $rootScope, $templateCache, $localStorage, $stateParams) {
            $rootScope.$on('$viewContentLoaded', function() {
                $templateCache.removeAll();
            });

            function detectIE() {
                var ua = window.navigator.userAgent;

                var msie = ua.indexOf('MSIE ');
                if (msie > 0) {
                    // IE 10 or older => return version number
                    return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
                }

                var trident = ua.indexOf('Trident/');
                if (trident > 0) {
                    // IE 11 => return version number
                    var rv = ua.indexOf('rv:');
                    return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
                }

                var edge = ua.indexOf('Edge/');
                if (edge > 0) {
                   // Edge (IE 12+) => return version number
                   return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
                }

                return false;
            }

            if (detectIE()) {
                angular.element('body').on("mousewheel", function () {
                    event.preventDefault ? event.preventDefault() : (event.returnValue = false);
                    //scroll without smoothing
                    var wheelDelta = event.wheelDelta;
                    var currentScrollPosition = window.pageYOffset;
                    angular.element('body, html').stop().animate({
                        scrollTop: (currentScrollPosition - wheelDelta)
                    }, 50, 'easeInOutExpo');
                });
            }
            $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
                if(toState.name=='core.login' && $localStorage['NRP-USER-TOKEN'] && ($localStorage['PKG']=='57a6c362ebba3d886256dba1'))
                {
                    $state.go('core.profile');
                        showMessages([{
                        header: '',
                        type: 'success',
                        message: '<b>Online Version & Ultimate Package:</b> Call or email us for pricing info at 844-592-4197<br>(toll free) or <a href="mailto:sales@nrpdirect.com">sales@nrpdirect.com</a>'
                    }]);
                    /*Call or email us for pricing info at 844-592-4197(toll free) or sales@nrpdirect.com*/
                }
                if (localStorage.url) {
                    if(localStorage.url.toUpperCase() != toParams.directoryName.toUpperCase())
                    {
                        
                        $localStorage.$reset();
                        localStorage.url=toParams.directoryName;
                    }
                }
                if (toState.url === '/home') {
                    setTimeout(function(){;
                        angular.element("#navbar ul").children('li').removeClass('active');
                        angular.element("#navbar ul").children(':first').addClass('active');
                    }, 1500);
                };
                if ((localStorage.menuData == "core.about-us" && toState.name == "core.about-us") || (localStorage.menuData == "core.sales" && toState.name == "core.sales")) {
                    
                    toParams.navigateTo = "";
                }

                $("html, body").scrollTop(0);
                if (toParams && toParams.navigateTo && toParams.navigateTo != '') setTimeout(function() {
                    if (toParams.navigatePosition && toParams.navigatePosition != '' ) {
                        
                        $('html, body').stop().animate({
                            scrollTop: (($(toParams.navigateTo).offset().top) - toParams.navigatePosition)
                        }, 1500, 'easeInOutExpo');
                    } else {
                        
                        $('html, body').stop().animate({
                            scrollTop: (($(toParams.navigateTo).offset().top) + 5)
						}, 1500, 'easeInOutExpo');
                    }
                }, 700);
            });
            $rootScope.isUserLoggedIn = function() {
                return $localStorage['NRP-USER-TOKEN'];
            };
            $rootScope.logout = function() {
                delete $localStorage['NRP-USER-TOKEN'];
                delete $localStorage['PKG'];
                localStorage.refineSearch='';
                localStorage.clear();
                
                    showMessages([{
                        type: 'success',
                        message: 'Logged out successfully!',
                        header: 'Logout'
                    }]);
                $state.reload();
                // $rootScope.currentUser.$logout(function() {
                //     showMessages([{
                //         type: 'success',
                //         message: 'Logged out successfully!',
                //         header: 'Logout'
                //     }]);
                //     $state.reload();
                // }, function(err) {
                //     console.log('logout err', err);
                // });
            };
        }
    ]);


toastr.options = {
    "closeButton": true,
    "newestOnTop": true,
    "positionClass": "toast-bottom-right"
};

function showMessages(messages) {
    for (var i in messages) {
        var message = messages[i];
        toastr[message.type](message.message, message.header);
    }
}

function getObjectData(object) {
    if (typeof object != 'object') return object;
    var newObj = {};
    for (var i in object) {
        newObj[i] = object[i];
    }
    newObj = JSON.parse(angular.toJson(newObj));
    return newObj;
}

function arrayIntersect(arrays) {
    return arrays.shift().reduce(function(res, v) {
        if (res.indexOf(v) === -1 && arrays.every(function(a) {
                return a.indexOf(v) !== -1;
            })) res.push(v);
        return res;
    }, []);
}
