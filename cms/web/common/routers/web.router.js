angular.module('web')
    .config(['$stateProvider', '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {

            $stateProvider
                .state('core', {
                    url: '/:directoryName',
                    views: {
                        "header": {
                            templateUrl: 'assets/layout/partials/header.html',
                            controller: 'web.header.controller'
                        },
                        "footer": {
                            templateUrl: 'assets/layout/partials/footer.html',
                            controller: 'web.footer.controller'
                        },
                        "": {
                            template: '<ui-view></ui-view>',
                            controller: function($scope, $dataShare, $rootScope, $stateParams, Profile) {
                                console.log($stateParams);
                                console.log('$dataShare.getDir', $dataShare.getDir());
                                $rootScope.currentDirectory = $dataShare.getDir();
                                $rootScope.currentUser = Profile;
                            }
                        }
                    },
                    resolve: {
                        Profile: [
                            '$localStorage',
                            'SubscriberService',
                            function($localStorage, SubscriberService) {
                                if ($localStorage['NRP-USER-TOKEN']) {
                                    return SubscriberService.profile().$promise;
                                }
                                return undefined;
                            }
                        ]
                    },
                    controller: [
                        '$rootScope',
                        'Profile',
                        function($rootScope, Profile) {
                            $rootScope.currentUser = Profile;
                        }
                    ]
                })
                .state('core.home', {
                    url: '/home',
                    templateUrl: '/assets/home/view.html',
                    params: { navigateTo: '', navigatePosition : ''},
                    controller: 'web.home.controller'
                })
                // About page
                .state('core.about-us', {
                    url: '/about-us',
                    params: { navigateTo: '', navigatePosition : ''},
                    templateUrl: '/assets/about/view.html',
                    controller: 'web.home.aboutus.controller'
                        // we'll get to this in a bit       
                })
                // Contact Us page
                .state('core.contact-us', {
                    url: '/contact-us',
                    templateUrl: '/assets/contact/view.html',
                    controller: 'web.home.contactus.controller'
                        // we'll get to this in a bit       
                })
                .state('core.sourcebook-detail', {
                    url: '/sourcebook-detail',
                    params: { navigateTo: '' },
                    templateUrl: '/assets/sourcebook-detail/sourcebook-detail.html',
                    controller: 'web.home.sourcebook-detail.controller'
                    // we'll get to this in a bit       
                })
                .state('core.sales', {
                    url: '/sales',
                    params: { navigateTo: '', navigatePosition : ''},
                    templateUrl: '/assets/sales/view.html',
                    controller: 'web.home.sales.controller'
                        // we'll get to this in a bit       
                })
                // .state('core.news', {
                //     url: '/news',
                //     templateUrl: '/assets/news/view.html',
                //     controller: 'web.home.news.controller'
                //         // we'll get to this in a bit       
                // })
                .state('core.create-request', {
                    url: '/create-request',
                    params: { navigateTo: '' },
                    templateUrl: '/assets/create-request/view.html',
                    controller: 'web.home.create-request.controller'
                        // we'll get to this in a bit       
                })
                .state('core.products-services', {
                    url: '/products-services',
                    templateUrl: '/assets/products-services/view.html',
                    controller: 'web.home.products-services.controller'
                        // we'll get to this in a bit       
                })
                .state('core.productsList', {
                    url: '/products-services/list/:ProductSubCategoryNames',
                    templateUrl: '/assets/products-services/list/list.html',
                    controller: 'web.home.products-services.list.controller'
                        // we'll get to this in a bit       
                })
                .state('core.productsDetails', {
                    url: '/products-services/details/:CompanyName',
                    templateUrl: '/assets/products-services/details/details.html',
                    controller: 'web.home.products-services.details.controller'
                        // we'll get to this in a bit       
                })
                .state('core.productsSearch', {
                    url: '/products-services/search',
                    templateUrl: '/assets/products-services/list/list.html',
                    controller: 'web.home.products-services.search.controller'
                        // we'll get to this in a bit       
                })
                .state('core.mailing-list', {
                    url: '/mailing-list',
                    templateUrl: '/assets/mailing-list/view.html',
                    controller: 'web.home.mailing-list.controller'
                        // we'll get to this in a bit       
                })
                .state('core.advertise', {
                    url: '/advertise',
                    templateUrl: '/assets/advertise/view.html',
                    controller: 'web.home.advertise.controller'
                        // we'll get to this in a bit       
                })
                .state('core.archive-reflections', {
                    url: '/archive-reflections',
                    templateUrl: '/assets/archive-reflections/view.html',
                    controller: 'web.home.archive-reflections.controller'
                        // we'll get to this in a bit       
                })
                .state('core.request-datacard', {
                    url: '/request-datacard',
                    params: { navigateTo: '', navigatePosition : ''},
                    templateUrl: '/assets/request-datacard/view.html',
                    controller: 'web.home.request-datacard.controller'
                        // we'll get to this in a bit       
                })
                .state('core.privacy-policy', {
                    url: '/privacy-policy',
                    templateUrl: '/assets/privacy-policy/view.html',
                    controller: 'web.home.privacyPolicyFooter.controller'
                        // we'll get to this in a bit       
                })
                .state('core.termsofuse', {
                    url: '/termsofuse',
                    templateUrl: '/assets/termsofuse/view.html',
                    controller: 'web.home.termsofuse.controller'
                        // we'll get to this in a bit       
                })
                .state('core.right-partials', {
                    url: '/right-partials',
                    templateUrl: '/assets/right-partials/right-view.html',
                    controller: 'web.right.partials.controller'
                })
                //Register Page to Submit OTP
                .state('core.register', {
                    url: '/register',
                    templateUrl: '/assets/login/register.html',
                    controller: 'web.home.register.controller'
                        //Login Page    
                }).state('core.login', {
                    url: '/login',
                    templateUrl: '/assets/login/login.html',
                    controller: 'web.home.login.controller'
                }).state('core.forgot', {
                    url: '/forgot',
                    templateUrl: '/assets/login/forgot.html',
                    controller: 'web.home.forgot.controller'
                })
                .state('core.faq', {
                    url: '/faq',
                    templateUrl: '/assets/faq/view.html',
                    controller: 'web.home.faq.controller'
                        // we'll get to this in a bit       
                })
                .state('core.omd-termsofuse', {
                    url: '/omd-termsofuse',
                    templateUrl: '/assets/omd-termsofuse/view.html',
                    controller: 'web.home.omd-termsofuse.controller'
                        // we'll get to this in a bit       
                })
                .state('core.omd-faq', {
                    url: '/omd-faq',
                    templateUrl: '/assets/omd-faq/view.html',
                    controller: 'web.home.omd-faq.controller'
                        // we'll get to this in a bit       
                })
                .state('core.sample-profile', {
                    url: '/sample-profile',
                    templateUrl: '/assets/sample-profile/view.html',
                    controller: 'web.home.sample-profile.controller'
                        // we'll get to this in a bit       
                })
                // .state('core.subscription', {
                //    cache: false,
                //     url: '/subscription',
                //     templateUrl: '/assets/subscription/view.html',
                //     controller: 'web.home.subscriptionform.controller'
                //         // we'll get to this in a bit       
                // })   
                .state('core.search', {
                    url: '/search',
                    params: {
                        navigateTo: '',
                        navigatePosition : ''
                    },
                    templateProvider: [
                        '$templateFactory',
                        '$stateParams',
                        function($templateFactory, $stateParams) {
                            var url = '/assets/search/' + $stateParams.directoryName.toLowerCase() + '-' + 'search.html';
                            return $templateFactory.fromUrl(url);
                        }
                    ],
                    resolve: {
                        DirectoryInstitutionCategory: [
                            'DirectoryInstitutionCategoryService',
                            function(DirectoryInstitutionCategoryService) {
                                return DirectoryInstitutionCategoryService.get().$promise;
                            }
                        ],
                        DirectoryCollectionCategory: [
                            'DirectoryCollectionCategoryService',
                            function(DirectoryCollectionCategoryService) {
                                return DirectoryCollectionCategoryService.get().$promise;
                            }
                        ],
                        DirectorySpecificationType: [
                            'DirectorySpecificationTypeService',
                            function(DirectorySpecificationTypeService) {
                                return DirectorySpecificationTypeService.get().$promise;
                            }
                        ],                        
                        DirectorySpecificationCodeForIndustryPreference: [
                            'DirectorySpecificationCodeService',
                            function(DirectorySpecificationCodeService) {
                                return DirectorySpecificationCodeService.get({codeType: "INDUSTRY PREFERENCE"}).$promise;
                            }
                        ],
                         DirectorySpecificationCodeForGeographicalPreference: [
                            'DirectorySpecificationCodeService',
                            function(DirectorySpecificationCodeService) {
                                return DirectorySpecificationCodeService.get({codeType: "GEOGRAPHICAL PREFERENCES"}).$promise;
                            }
                        ],    
                         DirectorySpecificationCodeForPortfolioRelationship: [
                            'DirectorySpecificationCodeService',
                            function(DirectorySpecificationCodeService) {
                                return DirectorySpecificationCodeService.get({codeType: "PORTFOLIO RELATIONSHIP"}).$promise;
                            }
                        ],
                        DirectorySpecificationCodeForFinancingPreferred: [
                            'DirectorySpecificationCodeService',
                            function(DirectorySpecificationCodeService) {
                                return DirectorySpecificationCodeService.get({codeType: "FINANCING PREFERRED"}).$promise;
                            }
                        ],
                        DirectorySpecificationCodeForFirmWillTakeActiveRoleAs: [
                            'DirectorySpecificationCodeService',
                            function(DirectorySpecificationCodeService) {
                                return DirectorySpecificationCodeService.get({codeType: "FIRM WILL TAKE ACTIVE ROLE AS"}).$promise;
                            }
                        ],
                        DirectorySpecificationCodeForDealType: [
                            'DirectorySpecificationCodeService',
                            function(DirectorySpecificationCodeService) {
                                return DirectorySpecificationCodeService.get({codeType: "DEAL TYPE"}).$promise;
                            }
                        ],
                         ListingType: [
                            'DirectoryListingTypeService',
                            function(DirectoryListingTypeService) {
                                return DirectoryListingTypeService.get({"directoryId" : "57189ccf24d8bc65f4123bc2"}).$promise;
                            }
                        ],
                        DisbursalType: [
                            'DirectoryDisbursalTypeService',
                            function(DirectoryDisbursalTypeService) {
                                return DirectoryDisbursalTypeService.get().$promise;
                            }
                        ],
                           DirectoryOfficerType: [
                            'DirectoryOfficerTypeService',
                            function(DirectoryOfficerTypeService) {
                                return DirectoryOfficerTypeService.get().$promise;
                            }
                        ]   
                    },
                    //controller: 'DirectoryWebUserSearchController',
                      controllerProvider: function($stateParams) {
                        var path=$stateParams.directoryName.toLocaleUpperCase();
                          return 'DirectoryWebUserSearchController'+path;
                    },
                    //controller:ctrl,
                    controllerAs: 'vm'
                })
                .state('core.searchView', {
                    url: '/view/:id',
                    // templateUrl: '/assets/search/search-view.html',
                    templateProvider: [
                        '$templateFactory',
                        '$stateParams',
                        function($templateFactory, $stateParams) {
                            var url = '/assets/search/' + $stateParams.directoryName.toLowerCase() + '-' + 'search-view.html';
                            console.log("url :", url);
                            return $templateFactory.fromUrl(url);
                        }
                    ],
                    resolve: {
                        Organization: [
                            '$stateParams',
                            'DirectoryOrganizationService',
                            function($stateParams, DirectoryOrganizationService) {
                                return DirectoryOrganizationService.list({ id: $stateParams.id }).$promise;
                            }
                        ]
                    },
                    controller: [
                        '$scope',
                        'Organization',
                        function($scope, Organization) {
                            $scope.organization = Organization[0];
                            var vm=this;
                            for(var i=0;i < Organization[0].features.length;i++)
                            {
                                if(Organization[0].features[i].featureType=='FACILITY')
                                {
                                    $scope.organization.featureType=true;
                                }
                            }
                            $scope.showAddress=function(data){

                                for(var i=0;i<$scope.organization.publication.length;i++)
                                {
                                console.log("$scope.organization.publication[i]",$scope.organization.publication[i]);
                                if($scope.organization.publication[i].description==null)
                                    {
                                     $scope.organization.publicationFlag = false;
                                    }else
                                    {
                                    $scope.organization.publicationFlag = true;
                                    }
                                }
                                $scope.organization.showAddress=[];
                                for(var i=0;i<data.length;i++)
                                {
                                    if(data[i].addressType.codeValue=='MA')
                                    {
                                        $scope.organization.showAddress.push('MA');
                                    }
                                    if(data[i].addressType.codeValue=='PH')
                                    {
                                     $scope.organization.showAddress.push('PH');   
                                    }
                                    if($scope.organization.showAddress.indexOf('PH') != -1)
                                    {
                                        $scope.organization.showAddressData='PH';
                                    }
                                    else
                                    {
                                     $scope.organization.showAddressData='MA';   
                                    }
                                }
                                $scope.show=show;
                                function show(data){
                                    if(data != 1)
                                    localStorage.state=data;
                                }
                                $scope.printDiv=printDiv;
                                function printDiv()
                                {
                                    var d=document.getElementById('printDiv').innerHTML;
                                    var win= window.open('', '_blank', 'width=595,height=842');
                                    var str='<html><head></head> <body>'+ d +'</body> </html>';
                                    win.document.write(str);
                                    win.document.close();
                                    win.print();
                                    win.close();
                                }
                                
                            }

                        }
                    ] 
                })
                .state('core.subscription', {
                    url: '/subscription/:packageName',
                    templateUrl: '/assets/subscription/view.html',
                    params: {
                        navigateTo: '',
                        navigatePosition : ''
                    },
                    resolve: {
                        SubscriptionPackage: [
                            '$stateParams',
                            'SubscriptionPackageService',
                            function($stateParams, SubscriptionPackageService, toParams) {
                                return SubscriptionPackageService.get({
                                    packageName: $stateParams.packageName
                                }).$promise;
                            }
                        ]
                    },
                    controller: 'SubscriptionController'
                })
                .state('core.profile', {
                    url: '/profile',
                    templateUrl: '/assets/profile/my-profile.html',
                    params: {
                        navigateTo: '',
                        requestPasswordChange: false
                    },
                    resolve: {
                        Profile: [
                            '$localStorage',
                            'SubscriberService',
                            function($localStorage, SubscriberService) {
                                if ($localStorage['NRP-USER-TOKEN']) {
                                    return SubscriberService.profile().$promise;
                                }
                                return undefined;
                            }
                        ]
                    },
                    controller: 'ProfileController'
                })
                .state('core.404', {
                    url: '/404',
                    template:"",
                    controller: function(){ 
                        window.location.href = DEFAULT_DIRECTORY + "/" + DIRECTORY_404; 
                    }
                });
            $urlRouterProvider.otherwise('/' + DEFAULT_DIRECTORY + "/404");
            //console.log($stateProvider);
            // use the HTML5 History API
            // $locationProvider.html5Mode(true);
            // $locationProvider.hashPrefix('!');
        }
    ]);
