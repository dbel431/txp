angular.module('nrp', ['toaster'])
    .controller('nrp.main.controller', function($scope, $http, toaster) {
        $scope.BASE_PATH=BASE_PATH;
        $scope.data = { "directoryId": "57baa29bc2af752141530848", "direcoryName": 'NRP', "requestType": "NRP Inquiry", "phone": "", "comments": "", "email": "", "name": { "first": "" }, "reasons": "" };
        $scope.nrpsend = nrpsend;

        function nrpsend() {
            console.log($scope.data);
            $http({
                method: 'POST',
                url: APIBasePath + 'subscriber/web-request',
                data: $scope.data
            }).then(function successCallback(response) {

                toaster.pop({
                    type: 'success',
                    title: 'Comments Submission',
                    body: 'Comments Submitted Successfully!',
                    showCloseButton: true,
                    timeout: 3000,
                });
                $scope.data = $scope.data = { "directoryId": "57baa29bc2af752141530848", "direcoryName": 'NRP', "requestType": "NRP Inquiry", "phone": "", "comments": "", "email": "", "name": { "first": "" }, "reasons": "" };
            }, function errorCallback(response) {
                toaster.pop({
                    type: 'error',
                    title: 'Comments Submission',
                    body: 'Comments not Submitted Successfully!',
                    showCloseButton: true,
                    timeout: 3000,
                });
            });
        }
    });
