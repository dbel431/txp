angular.module('directory').service('DirectoryOrganizationVersionService', [
    '$rootScope',
    '$localStorage',
    '$resource',
    function($rootScope, $localStorage, $resource) {
        return $resource(API_PATH + '/organization/version', {}, {
            get: {
                method: 'GET',
                isArray: true
            }
        });
    }
]);
