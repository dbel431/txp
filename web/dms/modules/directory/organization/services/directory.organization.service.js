angular.module('directory').service('DirectoryOrganizationService', [
    '$rootScope',
    '$localStorage',
    '$resource',
    function($rootScope, $localStorage, $resource) {
        return $resource(API_PATH + '/organization', {}, {
            list: {
                method: 'POST',
                url: API_PATH + '/organization/list',
                isArray: true
            },
            getExhibitionCount: {
                method: 'GET',
                url: API_PATH + '/organization/count'
            },
            getOrganizationCount: {
                method: 'GET',
                url: API_PATH + '/organization/organization-count'
            },
            getOrganizationCountOCD: {
                method: 'GET',
                url: API_PATH + '/organization/organization-count-ocd'
            },
            getSummaryReportCount: {
                method: 'GET',
                url: API_PATH + '/organization/summary-report-count',
                isArray: true
            },
            exportToExcel: {
                method: 'POST',
                url: API_PATH + '/organization/export-to-excel'
            },
            submitAndSendForApproval: {
                method: 'POST',
                url: API_PATH + '/organization/submit-and-send-for-approval',
            },
            modifyAndSendForApproval: {
                method: 'POST',
                url: API_PATH + '/organization/modify-and-send-for-approval',
            },
            sendForApproval: {
                method: 'POST',
                url: API_PATH + '/organization/send-for-approval',
            },
            approve: {
                method: 'POST',
                url: API_PATH + '/organization/approve',
            },
            sendForCorrection: {
                method: 'POST',
                url: API_PATH + '/organization/send-for-correction',
            },
            requestForCorrection: {
                method: 'POST',
                url: API_PATH + '/organization/request-for-correction',
            },
            dataTable: {
                method: 'POST',
                url: API_PATH + '/organization/data-table',
                transformRequest: function(data, headerGetter) {
                    var headers = headerGetter();
                    var newData = {
                        dataTableQuery: data,
                        conditions: {
                            directoryId: $localStorage['DMS-DIRECTORY']._id
                        }
                    }
                    return JSON.stringify(newData);
                }
            },
            update: {
                method: 'PUT'
            }
        });
    }
]);
