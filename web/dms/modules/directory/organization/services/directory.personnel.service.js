angular.module('directory').service('DirectoryPersonnelService', [
    '$rootScope',
    '$localStorage',
    '$resource',
    function($rootScope, $localStorage, $resource) {
        return $resource(API_PATH + '/personnel', {}, {
            list: {
                method: 'POST',
                url: API_PATH + '/personnel/list',
                isArray: true
            }
        });
    }
]);
