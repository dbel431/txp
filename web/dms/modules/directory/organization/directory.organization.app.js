var currentDirectoryChosen = null;
angular.module('directory.organization', [
        'directory'
    ])
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $stateProvider.state('portal.directory.organization', {
                url: '/organization',
                templateUrl: '/views/directory.organization.tpl.html'
            }).state('portal.directory.organization.list', {
                url: '/list',
                templateProvider: [
                    '$rootScope',
                    '$localStorage',
                    '$templateFactory',
                    function($rootScope, $localStorage, $templateFactory) {
                        var url = '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.list.html';
                        return $templateFactory.fromUrl(url);
                    }
                ],
                resolve: {
                    DirectoryRecordVersion: [
                        'DirectoryRecordVersionService',
                        function(DirectoryRecordVersionService) {
                            return DirectoryRecordVersionService.get().$promise;
                        }
                    ],
                    "AddressType": [
                        'DirectoryAddressTypeService',
                        function(DirectoryAddressTypeService) {
                            return DirectoryAddressTypeService.get().$promise;
                        }
                    ],
                    OrganizationType: [
                        'DirectoryOrganizationTypeService',
                        function(DirectoryOrganizationTypeService) {
                            return DirectoryOrganizationTypeService.get().$promise;
                        }
                    ],
                    DirectoryRecordStatus: [
                        'DirectoryRecordStatusService',
                        '$localStorage',
                        function(DirectoryRecordStatusService, $localStorage) {
                            return DirectoryRecordStatusService.get({ "belongsTo.directories": $localStorage['DMS-DIRECTORY']._id }).$promise;
                        }
                    ]
                },
                controller: 'DirectoryOrganizationListController'
            }).state('portal.directory.organization.view', {
                url: '/view/:id',
                templateProvider: [
                    '$rootScope',
                    '$localStorage',
                    '$templateFactory',
                    function($rootScope, $localStorage, $templateFactory) {
                        var url = '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.view.tpl.html';
                        return $templateFactory.fromUrl(url);
                    }
                ],
                resolve: {
                    'Organization': [
                        '$stateParams',
                        'DirectoryOrganizationService',
                        function($stateParams, DirectoryOrganizationService) {
                            return DirectoryOrganizationService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ],
                    "AddressType": [
                        'DirectoryAddressTypeService',
                        function(DirectoryAddressTypeService) {
                            return DirectoryAddressTypeService.get().$promise;
                        }
                    ],
                    VendorType: [
                        'DirectoryVendorTypeService',
                        function(DirectoryVendorTypeService) {
                            return DirectoryVendorTypeService.get().$promise;
                        }
                    ],
                    DirectoryRecordStatus: [
                        'DirectoryRecordStatusService',
                        '$localStorage',
                        function(DirectoryRecordStatusService, $localStorage) {
                            return DirectoryRecordStatusService.get({ "belongsTo.directories": $localStorage['DMS-DIRECTORY']._id }).$promise;
                        }
                    ]
                },

                controller: 'DirectoryOrganizationViewController',
            }).state('portal.directory.organization.view.general', {
                url: '/general',
                templateProvider: [
                    '$rootScope',
                    '$localStorage',
                    '$templateFactory',
                    function($rootScope, $localStorage, $templateFactory) {
                        var url = '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.partial.general.view.html';
                        return $templateFactory.fromUrl(url);
                    }
                ]

            }).state('portal.directory.organization.view.communication', {
                url: '/communication',
                templateProvider: [
                    '$rootScope',
                    '$localStorage',
                    '$templateFactory',
                    function($rootScope, $localStorage, $templateFactory) {
                        var url = '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.partial.communication.view.html';
                        return $templateFactory.fromUrl(url);
                    }
                ]

            }).state('portal.directory.organization.view.personnel', {
                url: '/personnel',
                templateProvider: [
                    '$rootScope',
                    '$localStorage',
                    '$templateFactory',
                    function($rootScope, $localStorage, $templateFactory) {
                        var url = '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.partial.personnel.view.html';
                        return $templateFactory.fromUrl(url);
                    }
                ]

            }).state('portal.directory.organization.view.other', {
                url: '/other',
                templateProvider: [
                    '$rootScope',
                    '$localStorage',
                    '$templateFactory',
                    function($rootScope, $localStorage, $templateFactory) {
                        var url = '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.partial.other.view.html';
                        return $templateFactory.fromUrl(url);
                    }
                ]

            }).state('portal.directory.organization.diff', {
                url: '/diff/:id/:versionId',
                templateProvider: [
                    '$rootScope',
                    '$localStorage',
                    '$templateFactory',
                    function($rootScope, $localStorage, $templateFactory) {
                        var url = '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.diff.tpl.html';
                        return $templateFactory.fromUrl(url);
                    }
                ],
                resolve: {
                    'Organization': [
                        '$stateParams',
                        'DirectoryOrganizationService',
                        function($stateParams, DirectoryOrganizationService) {
                            return DirectoryOrganizationService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ],
                    'OrganizationVersions': [
                        '$stateParams',
                        'DirectoryOrganizationVersionService',
                        function($stateParams, DirectoryOrganizationVersionService) {
                            return DirectoryOrganizationVersionService.get({
                                organization: $stateParams.id
                            }).$promise;
                        }
                    ],
                    "AddressType": [
                        'DirectoryAddressTypeService',
                        function(DirectoryAddressTypeService) {
                            return DirectoryAddressTypeService.get().$promise;
                        }
                    ],
                    VendorType: [
                        'DirectoryVendorTypeService',
                        function(DirectoryVendorTypeService) {
                            return DirectoryVendorTypeService.get().$promise;
                        }
                    ],
                    DirectoryRecordStatus: [
                        'DirectoryRecordStatusService',
                        '$localStorage',
                        function(DirectoryRecordStatusService, $localStorage) {
                            return DirectoryRecordStatusService.get({ "belongsTo.directories": $localStorage['DMS-DIRECTORY']._id }).$promise;
                        }
                    ]
                },
                controller: 'DirectoryOrganizationDiffController',
            }).state('portal.directory.organization.diff.general', {
                url: '/general',
                templateProvider: [
                    '$rootScope',
                    '$localStorage',
                    '$templateFactory',
                    function($rootScope, $localStorage, $templateFactory) {
                        var url = '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.partial.general.diff.html';
                        return $templateFactory.fromUrl(url);
                    }
                ]
            }).state('portal.directory.organization.diff.communication', {
                url: '/communication',
                templateProvider: [
                    '$rootScope',
                    '$localStorage',
                    '$templateFactory',
                    function($rootScope, $localStorage, $templateFactory) {
                        var url = '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.partial.communication.diff.html';
                        return $templateFactory.fromUrl(url);
                    }
                ]

            }).state('portal.directory.organization.diff.personnel', {
                url: '/personnel',
                templateProvider: [
                    '$rootScope',
                    '$localStorage',
                    '$templateFactory',
                    function($rootScope, $localStorage, $templateFactory) {
                        var url = '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.partial.personnel.diff.html';
                        return $templateFactory.fromUrl(url);
                    }
                ]

            }).state('portal.directory.organization.diff.other', {
                url: '/other',
                templateProvider: [
                    '$rootScope',
                    '$localStorage',
                    '$templateFactory',
                    function($rootScope, $localStorage, $templateFactory) {
                        var url = '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.partial.other.diff.html';
                        return $templateFactory.fromUrl(url);
                    }
                ]

            }).state('portal.directory.organization.add', {
                url: '/add',
                templateProvider: [
                    '$rootScope',
                    '$localStorage',
                    '$templateFactory',
                    function($rootScope, $localStorage, $templateFactory) {
                        var url = '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.add.tpl.html';
                        return $templateFactory.fromUrl(url);
                    }
                ],
                resolve: {
                    ClassificationCode: [
                        'DirectoryClassificationCodeService',
                        function(DirectoryClassificationCodeService) {
                            return DirectoryClassificationCodeService.get().$promise;
                        }
                    ],
                    Section: [
                        'DirectorySectionService',
                        function(DirectorySectionService) {
                            return DirectorySectionService.get().$promise;
                        }
                    ],
                    ListingType: [
                        'DirectoryListingTypeService',
                        '$localStorage',
                        function(DirectoryListingTypeService, $localStorage) {
                            return DirectoryListingTypeService.get({ directoryId: $localStorage['DMS-DIRECTORY']._id }).$promise;
                        }
                    ],
                    AddressType: [
                        'DirectoryAddressTypeService',
                        function(DirectoryAddressTypeService) {
                            return DirectoryAddressTypeService.get().$promise;
                        }
                    ],
                    VendorType: [
                        'DirectoryVendorTypeService',
                        function(DirectoryVendorTypeService) {
                            return DirectoryVendorTypeService.get().$promise;
                        }
                    ],
                    PrimaryMarket: [
                        'DirectoryPrimaryMarketService',
                        function(DirectoryPrimaryMarketService) {
                            return DirectoryPrimaryMarketService.get().$promise;
                        }
                    ],
                    DirectoryRecordStatus: [
                        'DirectoryRecordStatusService',
                        '$localStorage',
                        function(DirectoryRecordStatusService, $localStorage) {
                            return DirectoryRecordStatusService.get({ "belongsTo.directories": $localStorage['DMS-DIRECTORY']._id }).$promise;
                        }
                    ],
                    DirectoryPensionReportInterval: [
                        'DirectoryPensionReportIntervalService',
                        function(DirectoryPensionReportIntervalService) {
                            return DirectoryPensionReportIntervalService.get().$promise;
                        }
                    ],
                    AdvanceBillType: [
                        'DirectoryAdvanceBillTypeService',
                        function(DirectoryAdvanceBillTypeService) {
                            return DirectoryAdvanceBillTypeService.get().$promise;
                        }
                    ],
                    //FOR OCD
                    OrganizationType: [
                        'DirectoryOrganizationTypeService',
                        function(DirectoryOrganizationTypeService) {
                            return DirectoryOrganizationTypeService.get().$promise;
                        }
                    ],
                    DioceseType: [
                        'DirectoryDioceseTypeService',
                        function(DirectoryDioceseTypeService) {
                            return DirectoryDioceseTypeService.get().$promise;
                        }
                    ],
                    DioProvince: [
                        'DirectoryDioProvinceService',
                        function(DirectoryDioProvinceService) {
                            return DirectoryDioProvinceService.get().$promise;
                        }
                    ],
                    ParishStatus: [
                        'DirectoryParishStatusService',
                        function(DirectoryParishStatusService) {
                            return DirectoryParishStatusService.get().$promise;
                        }
                    ],
                    EthnicityType: [
                        'DirectoryEthnicityTypeService',
                        function(DirectoryEthnicityTypeService) {
                            return DirectoryEthnicityTypeService.get().$promise;
                        }
                    ],
                    SchoolType: [
                        'DirectorySchoolTypeService',
                        function(DirectorySchoolTypeService) {
                            return DirectorySchoolTypeService.get().$promise;
                        }
                    ],
                    ReligiousType: [
                        'DirectoryReligiousTypeService',
                        function(DirectoryReligiousTypeService) {
                            return DirectoryReligiousTypeService.get().$promise;
                        }
                    ],
                    GradeType: [
                        'DirectoryGradeTypeService',
                        function(DirectoryGradeTypeService) {
                            return DirectoryGradeTypeService.get().$promise;
                        }
                    ],
                    Supplement: [
                        'DirectorySupplementTypeService',
                        function(DirectorySupplementTypeService) {
                            return DirectorySupplementTypeService.get().$promise;
                        }
                    ]

                },
                controller: 'DirectoryOrganizationAddController',
            }).state('portal.directory.organization.add.general', {
                url: '/general',
                templateProvider: [
                    '$rootScope',
                    '$state',
                    '$localStorage',
                    '$templateFactory',
                    function($rootScope, $state, $localStorage, $templateFactory) {
                        var url = '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.partial.general.html';
                        return $templateFactory.fromUrl(url);
                    }
                ]
            }).state('portal.directory.organization.add.communication', {
                url: '/communication',
                templateProvider: [
                    '$rootScope',
                    '$localStorage',
                    '$templateFactory',
                    function($rootScope, $localStorage, $templateFactory) {
                        var url = '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.partial.communication.html';
                        return $templateFactory.fromUrl(url);
                    }
                ]
            }).state('portal.directory.organization.add.personnel', {
                url: '/personnel',
                templateProvider: [
                    '$rootScope',
                    '$localStorage',
                    '$templateFactory',
                    function($rootScope, $localStorage, $templateFactory) {
                        var url = '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.partial.personnel.html';
                        return $templateFactory.fromUrl(url);
                    }
                ]
            }).state('portal.directory.organization.add.other', {
                url: '/other',
                templateProvider: [
                    '$rootScope',
                    '$localStorage',
                    '$templateFactory',
                    function($rootScope, $localStorage, $templateFactory) {
                        var url = '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.partial.other.html';
                        return $templateFactory.fromUrl(url);
                    }
                ]
            }).state('portal.directory.organization.edit', {
                url: '/edit/:id',
                templateProvider: [
                    '$rootScope',
                    '$localStorage',
                    '$templateFactory',
                    function($rootScope, $localStorage, $templateFactory) {
                        var url = '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.edit.tpl.html';
                        return $templateFactory.fromUrl(url);
                    }
                ],
                resolve: {
                    'Organization': [
                        '$stateParams',
                        'DirectoryOrganizationService',
                        function($stateParams, DirectoryOrganizationService) {
                            return DirectoryOrganizationService.get({
                                id: $stateParams.id
                            }).$promise;
                        }

                    ],
                    OrganizationType: [
                        'DirectoryOrganizationTypeService',
                        function(DirectoryOrganizationTypeService) {
                            return DirectoryOrganizationTypeService.get().$promise;
                        }
                    ],
                    'ClassificationCode': [
                        'DirectoryClassificationCodeService',
                        function(DirectoryClassificationCodeService) {
                            return DirectoryClassificationCodeService.get().$promise;
                        }
                    ],
                    Section: [
                        'DirectorySectionService',
                        function(DirectorySectionService) {
                            return DirectorySectionService.get().$promise;
                        }
                    ],
                    AddressType: [
                        'DirectoryAddressTypeService',
                        function(DirectoryAddressTypeService) {
                            return DirectoryAddressTypeService.get().$promise;
                        }
                    ],
                    VendorType: [
                        'DirectoryVendorTypeService',
                        function(DirectoryVendorTypeService) {
                            return DirectoryVendorTypeService.get().$promise;
                        }
                    ],
                    ListingType: [
                        'DirectoryListingTypeService',
                        '$localStorage',
                        function(DirectoryListingTypeService, $localStorage) {
                            return DirectoryListingTypeService.get({ directoryId: $localStorage['DMS-DIRECTORY']._id }).$promise;
                        }
                    ],
                    DirectoryRecordStatus: [
                        'DirectoryRecordStatusService',
                        '$localStorage',
                        function(DirectoryRecordStatusService, $localStorage) {
                            return DirectoryRecordStatusService.get({ "belongsTo.directories": $localStorage['DMS-DIRECTORY']._id }).$promise;
                        }
                    ],
                    DirectoryPensionReportInterval: [
                        'DirectoryPensionReportIntervalService',
                        function(DirectoryPensionReportIntervalService) {
                            return DirectoryPensionReportIntervalService.get().$promise;
                        }
                    ],
                    DirectoryRecordVersion: [
                        'DirectoryRecordVersionService',
                        function(DirectoryRecordVersionService) {
                            return DirectoryRecordVersionService.get().$promise;
                        }
                    ],
                    PrimaryMarket: [
                        'DirectoryPrimaryMarketService',
                        function(DirectoryPrimaryMarketService) {
                            return DirectoryPrimaryMarketService.get().$promise;
                        }
                    ],
                    AdvanceBillType: [
                        'DirectoryAdvanceBillTypeService',
                        function(DirectoryAdvanceBillTypeService) {
                            return DirectoryAdvanceBillTypeService.get().$promise;
                        }
                    ],
                    DioceseType: [
                        'DirectoryDioceseTypeService',
                        function(DirectoryDioceseTypeService) {
                            return DirectoryDioceseTypeService.get().$promise;
                        }
                    ],
                    DioProvince: [
                        'DirectoryDioProvinceService',
                        function(DirectoryDioProvinceService) {
                            return DirectoryDioProvinceService.get().$promise;
                        }
                    ],
                    ParishStatus: [
                        'DirectoryParishStatusService',
                        function(DirectoryParishStatusService) {
                            return DirectoryParishStatusService.get().$promise;
                        }
                    ],
                    EthnicityType: [
                        'DirectoryEthnicityTypeService',
                        function(DirectoryEthnicityTypeService) {
                            return DirectoryEthnicityTypeService.get().$promise;
                        }
                    ],
                    SchoolType: [
                        'DirectorySchoolTypeService',
                        function(DirectorySchoolTypeService) {
                            return DirectorySchoolTypeService.get().$promise;
                        }
                    ],
                    ReligiousType: [
                        'DirectoryReligiousTypeService',
                        function(DirectoryReligiousTypeService) {
                            return DirectoryReligiousTypeService.get().$promise;
                        }
                    ],
                    GradeType: [
                        'DirectoryGradeTypeService',
                        function(DirectoryGradeTypeService) {
                            return DirectoryGradeTypeService.get().$promise;
                        }
                    ],
                    Supplement: [
                        'DirectorySupplementTypeService',
                        function(DirectorySupplementTypeService) {
                            return DirectorySupplementTypeService.get().$promise;
                        }
                    ]
                },
                controller: 'DirectoryOrganizationEditController'
            }).state('portal.directory.organization.edit.general', {
                url: '/general',
                templateProvider: [
                    '$rootScope',
                    '$localStorage',
                    '$templateFactory',
                    function($rootScope, $localStorage, $templateFactory) {
                        var url = '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.partial.general.html';
                        return $templateFactory.fromUrl(url);
                    }
                ]
            }).state('portal.directory.organization.edit.communication', {
                url: '/communication',
                templateProvider: [
                    '$rootScope',
                    '$localStorage',
                    '$templateFactory',
                    function($rootScope, $localStorage, $templateFactory) {
                        var url = '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.partial.communication.html';
                        return $templateFactory.fromUrl(url);
                    }
                ]
            }).state('portal.directory.organization.edit.personnel', {
                url: '/personnel',
                templateProvider: [
                    '$rootScope',
                    '$localStorage',
                    '$templateFactory',
                    function($rootScope, $localStorage, $templateFactory) {
                        var url = '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.partial.personnel.html';
                        return $templateFactory.fromUrl(url);
                    }
                ]
            }).state('portal.directory.organization.edit.other', {
                url: '/other',
                templateProvider: [
                    '$rootScope',
                    '$localStorage',
                    '$templateFactory',
                    function($rootScope, $localStorage, $templateFactory) {
                        var url = '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.partial.other.html';
                        return $templateFactory.fromUrl(url);
                    }
                ]
            });
        }
    ])
    .run(['$rootScope',
        '$localStorage', '$state',
        function($rootScope, $localStorage, $state) {
            // $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
            //     if (currentDirectoryChosen == null) currentDirectoryChosen = $localStorage['DMS-DIRECTORY'];
            //     else if (currentDirectoryChosen._id != $localStorage['DMS-DIRECTORY']._id) {
            //         $localStorage['DMS-DIRECTORY'] = currentDirectoryChosen;
            //     }
            // });
            $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
                var state = toState.name.split('.');
                if (state.indexOf('organization') == (state.length - 2) && state.indexOf('add') == (state.length - 1)) {
                    event.preventDefault();
                    $state.go('portal.directory.organization.add.general');
                }
                if (state.indexOf('organization') == (state.length - 2) && state.indexOf('edit') == (state.length - 1)) {
                    event.preventDefault();
                    $state.go('portal.directory.organization.edit.general');
                }
                if (state.indexOf('organization') == (state.length - 2) && state.indexOf('view') == (state.length - 1)) {
                    event.preventDefault();
                    $state.go('portal.directory.organization.view.general');
                }
                if (state.indexOf('organization') == (state.length - 2) && state.indexOf('diff') == (state.length - 1)) {
                    event.preventDefault();
                    $state.go('portal.directory.organization.diff.general');
                }
            });
        }
    ]);
