angular.module('directory').directive('validateIsNameUnique', [
    '$rootScope',
    '$localStorage',
    'DirectoryOrganizationService',
    function($rootScope, $localStorage, DirectoryOrganizationService) {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function(scope, iElm, iAttrs, ngModel) {
                ngModel.$validators.validateIsNameUnique = function(modelValue) {
                    if (modelValue == '' || modelValue == undefined) return true;
                    var name = new DirectoryOrganizationService();
                    return DirectoryOrganizationService.list({
                        'name': modelValue,
                        parentId: null,
                        directoryId: $localStorage['DMS-DIRECTORY']._id
                    }, function(name) {
                        var valid = true;
                        if (iAttrs.parentName) {
                            valid = valid && (modelValue == iAttrs.parentName);
                            //$rootScope.validFlagParent = valid;

                        } else if (iAttrs.exceptOrganizationId) {
                            valid = valid && (name.length == 0 || ((iAttrs.parentName && name.length >= 1) || (!iAttrs.parentName && name.length == 1) && (name[0]._id == iAttrs.exceptOrganizationId)));
                            $rootScope.validFlagEdit = valid;
                        } else {
                            valid = valid && (name.length == 0);
                            $rootScope.validFlag = valid;

                        }
                        ngModel.$setValidity('validateIsNameUnique', valid);
                    }, function(err) {
                        ngModel.$setValidity('validateIsNameUnique', false);
                    });
                };
            }
        };
    }
]);
