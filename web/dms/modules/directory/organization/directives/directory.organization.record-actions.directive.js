angular.module('directory').directive('directoryOrganizationRecordActions', [
    '$rootScope',
    '$localStorage',
    function($rootScope, $localStorage) {
        return {
            scope: {
                'recordId': '=',
                'recordVersionId': '=',
                'rules': '=',
                'showVersionHistory': '=',
                'openVersionHistoryModal': '=',
            },
            restrict: 'EA',
            templateUrl: '/views/directory.organization.record-actions.tpl.html',
            replace: true,
            link: function(scope, elt, attb) {
                scope.isActionPermitted = $rootScope.isActionPermitted;
            }
        };
    }
]);
