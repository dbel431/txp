angular.module('security').directive('validateIsRoleUnique', [
    'RoleService',
    function(RoleService) {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function(scope, iElm, iAttrs, ngModel) {
                ngModel.$validators.validateIsRoleUnique = function(modelValue) {
                    if (modelValue == '' || modelValue == undefined) return true;
                    var role = new RoleService();
                    return RoleService.get({
                        'name': modelValue
                    }, function(role) {
                        if (role.length == 1 && iAttrs.exceptRoleId) {
                            ngModel.$setValidity('validateIsRoleUnique', (role[0]._id == iAttrs.exceptRoleId));
                        } else {
                            ngModel.$setValidity('validateIsRoleUnique', (role.length <= 0));
                        }
                    }, function(err) {
                        ngModel.$setValidity('validateIsRoleUnique', false);
                    });
                };
            }
        };
    }
]);
