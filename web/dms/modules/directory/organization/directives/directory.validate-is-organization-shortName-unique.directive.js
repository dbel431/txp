angular.module('directory').directive('validateIsShortNameUnique', [
    '$rootScope',
    '$localStorage',
    'DirectoryOrganizationService',
    function($rootScope, $localStorage, DirectoryOrganizationService) {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function(scope, iElm, iAttrs, ngModel) {
                ngModel.$validators.validateIsShortNameUnique = function(modelValue) {
                    if (modelValue == '' || modelValue == undefined) return true;
                    var shortName = new DirectoryOrganizationService();
                    return DirectoryOrganizationService.list({
                        'shortName': modelValue,
                        directoryId: $localStorage['DMS-DIRECTORY']._id
                    }, function(shortName) {

                        if (shortName.length == 1 && iAttrs.exceptOrganizationId) {
                            ngModel.$setValidity('validateIsShortNameUnique', (shortName[0]._id == iAttrs.exceptOrganizationId));
                        } else {
                            ngModel.$setValidity('validateIsShortNameUnique', (shortName.length <= 0));
                        }
                    }, function(err) {
                        ngModel.$setValidity('validateIsShortNameUnique', false);
                    });
                };
            }
        };
    }
]);
