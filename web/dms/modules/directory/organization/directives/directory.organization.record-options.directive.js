angular.module('directory').directive('directoryOrganizationRecordOptions', function() {
    return {
        scope: {
            'selectedRecordList': '=',
            'toggleAllRecords': '=',
            'isActionPermitted': '=',
            'openApproveModal': '=',
            'openSendForApprovalModal': '=',
            'openSendForCorrectionModal': '=',
            'openDeleteModal': '=',
            'allChecked': '=',
        },
        restrict: 'EA',
        templateUrl: '/views/directory.organization.record-options.tpl.html',
        replace: true
    };
});
