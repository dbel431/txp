angular.module('directory.organization')
    .controller('DirectoryOrganizationSendForApprovalController', [
        '$rootScope',
        '$localStorage',
        '$scope',
        '$uibModalInstance',
        'Organizations',
        function($rootScope, $localStorage, $scope, $uibModalInstance, Organizations) {
            $scope.version = {};
            $scope.organizations = Organizations;
            for (var i = $scope.organizations.length - 1; i >= 0; i--) {
                $scope.organizations[i]._oldOrgRecord = getObjectData($scope.organizations[i]);
            }
            $scope.cancel = function() {
                $uibModalInstance.dismiss('cancel');
            };
            var messages = [];
            $scope.sendForApproval = function() {
                for (var i = $scope.organizations.length - 1; i >= 0; i--) {
                    $scope.organizations[i].currentVersion = $scope.version;
                    $scope.organizations[i].$sendForApproval(function(org) {
                        messages.push({
                            type: 'success',
                            message: 'Record named: <b>' + org.name + '</b> sent for approval successfully!',
                            header: 'Send for approval'
                        });
                        syncXhr();
                    }, function(err) {
                        messages.push({
                            type: 'error',
                            message: 'Record named: <b>' + org.name + '</b> could not be sent for approval successfully!',
                            header: 'Send for approval'
                        });
                        syncXhr();
                    });
                }
            };
            var count = 0;

            function syncXhr() {
                count++;
                if ($scope.loading = (count == $scope.organizations.length))
                    $uibModalInstance.close(messages);
            }
        }
    ]);
