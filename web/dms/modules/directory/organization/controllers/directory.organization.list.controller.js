angular.module('directory.organization')
    .controller('DirectoryOrganizationListController', [
        '$rootScope',
        '$localStorage',
        '$scope',
        '$state',
        '$compile',
        '$uibModal',
        'DTOptionsBuilder',
        'DTColumnBuilder',
        'DirectoryOrganizationService',
        'DirectoryRecordVersion',
        'DirectoryOrganizationVersionService',
        'AddressType',
        'OrganizationType',
        function($rootScope, $localStorage, $scope, $state, $compile, $uibModal, DTOptionsBuilder, DTColumnBuilder, DirectoryOrganizationService, DirectoryRecordVersion, DirectoryOrganizationVersionService, AddressType, OrganizationType) {
            $rootScope.page = {
                name: $localStorage['DMS-DIRECTORY'].description + ': Record List',
                actions: {
                    add: {
                        object: 'directory.organization.add',
                        allowed: function() {
                            return $rootScope.isActionPermitted(this.object);
                        }
                    },
                    export: {
                        object: 'directory.organization.exportToExcel',
                        allowed: function() {
                            return $rootScope.isActionPermitted(this.object);
                        },
                        ngClick: function() {
                            $scope.openExportToExcelModal();
                        }
                    }
                }
            };
            $scope.rules = {
                versionVsAction: {
                    '1': ['directory.organization.edit', 'directory.organization.sendForApproval'],
                    '2': ['directory.organization.approve', 'directory.organization.sendForCorrection'],
                    '3': ['directory.organization.edit', 'directory.organization.sendForApproval'],
                    '4': ['directory.organization.edit'],
                    '6': ['directory.organization.edit'],
                },
                actionVsVersion: {
                    'directory.organization.edit': ['', '1', '3', '4', '6'],
                    'directory.organization.sendForApproval': ['1', '3'],
                    'directory.organization.sendForCorrection': ['2', '6'],
                    'directory.organization.approve': ['2'],
                }
            };
            $scope.directoryRecordVersion = [];
            for (var i = DirectoryRecordVersion.length - 1; i >= 0; i--) {
                $scope.directoryRecordVersion[DirectoryRecordVersion[i]._id] = {
                    label: DirectoryRecordVersion[i].description,
                    value: DirectoryRecordVersion[i]._id,
                    codeValue: DirectoryRecordVersion[i].codeValue
                };
            }
            $scope.selectedOrganizations = [];
            $scope.selectedOrganizationRecords = {};
            $scope.dtData = [];
            $scope.toggleAllRecords = function(select) {
                for (var i = $scope.dtData.length - 1; i >= 0; i--) {
                    $scope.dtData[i].checked = select || false;
                    if (select) {
                        if ($scope.selectedOrganizations.indexOf($scope.dtData[i]._id) < 0) {
                            $scope.selectedOrganizations.push($scope.dtData[i]._id);
                            $scope.selectedOrganizationRecords[$scope.dtData[i]._id] = $scope.dtData[i];
                        }
                    } else if ($scope.selectedOrganizations.indexOf($scope.dtData[i]._id) >= 0) {
                        if ($scope.selectedOrganizations.indexOf($scope.dtData[i]._id) >= 0) {
                            $scope.selectedOrganizations.splice($scope.selectedOrganizations.indexOf($scope.dtData[i]._id), 1);
                            delete $scope.selectedOrganizationRecords[$scope.dtData[i]._id];
                        }
                    }
                }
            };
            $scope.toggleRecord = function(id) {
                for (var i = $scope.dtData.length - 1; i >= 0; i--) {
                    if ($scope.dtData[i]._id == id) {
                        if ($scope.dtData[i].checked) {
                            if ($scope.selectedOrganizations.indexOf(id) < 0) {
                                $scope.selectedOrganizations.push(id);
                                $scope.selectedOrganizationRecords[id] = $scope.dtData[i];
                            }
                        } else {
                            if ($scope.selectedOrganizations.indexOf(id) >= 0) {
                                $scope.selectedOrganizations.splice($scope.selectedOrganizations.indexOf(id), 1);
                                delete $scope.selectedOrganizationRecords[id];
                            }
                        }
                        break;
                    }
                }
            };
            $scope.allChecked = function() {
                for (var i = $scope.dtData.length - 1; i >= 0; i--) {
                    if (!$scope.dtData[i].checked) return false;
                }
                return true;
            };
            $scope.isActionPermitted = $rootScope.isActionPermitted;

            var addressTypesMap = {};
            for (var i = 0; i < AddressType.length; i++) {
                addressTypesMap[AddressType[i]._id] = [AddressType[i].codeValue, AddressType[i].description].join(' ');
            }
            var organizationTypesMap = {},
                organizationTypesDDLValues = [];
            for (var i = 0; i < OrganizationType.length; i++) {
                organizationTypesMap[OrganizationType[i]._id] = OrganizationType[i].codeValue;
                organizationTypesDDLValues.push({
                    label: OrganizationType[i].codeValue,
                    value: OrganizationType[i]._id
                });
            }

            // *****************
            // BUILD Column Defs
            // *****************

            var columnDefs = [{
                title: '<directory-organization-record-options is-action-permitted="isActionPermitted" open-approve-modal="openApproveModal" open-send-for-approval-modal="openSendForApprovalModal" open-send-for-correction-modal="openSendForCorrectionModal" open-delete-modal="openDeleteModal" selected-record-list="selectedOrganizations" dt-data="dtData" all-checked="allChecked" toggle-all-records="toggleAllRecords"></directory-organization-record-options>',
                orderable: false,
                class: 'text-center',
                width: '2%',
                data: '_id',
                render: function(id) {
                    return '<directory-organization-record-select dt-data="dtData" selected-record-list="selectedOrganizations" record-id="' + id + '" toggle-record="toggleRecord"></directory-organization-record-select>';
                }
            }, {
                title: 'Organization ID',
                width: '2%',
                name: 'orgIdNumber',
                data: 'orgIdNumber',
                render: function(orgIdNumber) {
                    return (isNaN(parseFloat(orgIdNumber)) ? "" : parseFloat(orgIdNumber)) || "";
                }
            }, {
                title: 'Name',
                width: '20%',
                name: 'name',
                data: 'name',
                render: function(name) {
                    return name || "";
                }
            }];

            if ((/AAD/i).test($localStorage['DMS-DIRECTORY'].codeValue) || (/OMD/i).test($localStorage['DMS-DIRECTORY'].codeValue))
                columnDefs.push({
                    title: 'Minor Name',
                    width: '9%',
                    name: 'shortName',
                    data: 'shortName',
                    render: function(shortName) {
                        return shortName || "";
                    }
                });
            if ((/AAD/i).test($localStorage['DMS-DIRECTORY'].codeValue))
                columnDefs.push({
                    title: 'Sub Number',
                    width: '9%',
                    name: 'sub_nbr',
                    data: 'sub_nbr',
                    render: function(sub_nbr) {
                        return sub_nbr || "";
                    }
                });

            if ((/OCD/i).test($localStorage['DMS-DIRECTORY'].codeValue))
                columnDefs.push({
                    title: 'Classification Code',
                    width: '9%',
                    name: 'classificationCode',
                    data: 'classificationCode',
                    render: function(classificationCode) {
                        return organizationTypesMap[classificationCode] || "";
                    }
                });

            columnDefs.push({
                title: 'Address Street 1',
                width: '15%',
                name: 'street1',
                data: 'address.street1',
                render: function(city, type, row) {
                    var phyAddr = getPhysicalAddress(addressTypesMap, row.address);
                    return (phyAddr ? (phyAddr.street1 || "") : "") || "";
                }
            });
            columnDefs.push({
                title: 'City',
                width: '10%',
                name: 'city',
                data: 'address.cityName',
                render: function(city, type, row) {
                    var phyAddr = getPhysicalAddress(addressTypesMap, row.address);
                    return (phyAddr ? (phyAddr.cityName || "") : "") || "";
                }
            });
            columnDefs.push({
                title: 'State/Territory',
                width: '10%',
                name: 'state',
                data: 'address.stateName',
                render: function(city, type, row) {
                    var phyAddr = getPhysicalAddress(addressTypesMap, row.address);
                    return (phyAddr ? (phyAddr.stateName || "") : "") || "";
                }
            });
            columnDefs.push({
                title: 'Migration Status',
                width: '20%',
                orderable: false,
                data: 'versionId',
                render: function(versionId) {
                    return versionId ? $scope.directoryRecordVersion[versionId].label : "";
                }
            });
            columnDefs.push({
                title: 'Actions',
                class: 'text-center',
                width: '9%',
                orderable: false,
                data: 'versionId',
                render: function(versionId, type, row) {
                    return '<directory-organization-record-actions rules="rules" record-id="\'' + row._id + '\'" record-version-id="\'' + (versionId ? $scope.directoryRecordVersion[versionId].codeValue : '1') + '\'" open-version-history-modal="openVersionHistoryModal" show-version-history="showVersionHistory"></directory-organization-record-actions>';
                }
            });
            columnDefs.push({
                title: 'Org. ID',
                visible: false,
                width: '10%',
                name: 'org_id',
                data: 'org_id',
                render: function(org_id) {
                    return org_id || "";
                }
            });
            // *****************

            // *****************
            // BUILD Filter Defs
            // *****************

            var filterDefs = {
                '1': { type: 'text', time: 600 },
                '2': { type: 'text', time: 600 },
                '3': { type: 'text', time: 600 },
                '4': { type: 'text', time: 600 },
                '5': { type: 'text', time: 600 },
                '6': { type: 'select', values: (arrayValues($scope.directoryRecordVersion)).reverse(), time: 600 },
                '8': null
            };

            if ((/AAD/i).test($localStorage['DMS-DIRECTORY'].codeValue))
                var filterDefs = {
                    '1': { type: 'text', time: 600 },
                    '2': { type: 'text', time: 600 },
                    '3': { type: 'text', time: 600 },
                    '4': { type: 'text', time: 600 },
                    '5': { type: 'text', time: 600 },
                    '6': { type: 'text', time: 600 },
                    '7': { type: 'text', time: 600 },
                    '8': { type: 'select', values: (arrayValues($scope.directoryRecordVersion)).reverse(), time: 600 },
                    '10': null
                };
            if ((/OMD/i).test($localStorage['DMS-DIRECTORY'].codeValue))
                var filterDefs = {
                    '1': { type: 'text', time: 600 },
                    '2': { type: 'text', time: 600 },
                    '3': { type: 'text', time: 600 },
                    '4': { type: 'text', time: 600 },
                    '5': { type: 'text', time: 600 },
                    '6': { type: 'text', time: 600 },
                    '7': { type: 'select', values: (arrayValues($scope.directoryRecordVersion)).reverse(), time: 600 },
                    '9': null
                };

            if ((/OCD/i).test($localStorage['DMS-DIRECTORY'].codeValue))
                var filterDefs = {
                    '1': { type: 'text', time: 600 },
                    '2': { type: 'text', time: 600 },
                    '3': { type: 'select', values: organizationTypesDDLValues, time: 600 },
                    '4': { type: 'text', time: 600 },
                    '5': { type: 'text', time: 600 },
                    '6': { type: 'text', time: 600 },
                    '7': { type: 'select', values: (arrayValues($scope.directoryRecordVersion)).reverse(), time: 600 },
                    '9': null
                };
            // *****************

            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withPaginationType('full_numbers')
                // .withOption('scrollY', '500px')
                // .withOption('scrollCollapse', true)
                // .withOption('lengthMenu', [100, 500, 1000, 2000, 3000, 5000, 10000, "All"])
                .withOption('order', [1, 'asc'])
                .withOption('stateSave', true)
                // .withOption('orderFixed', [0, 'asc'])
                .withOption('ajax', DirectoryOrganizationService.dataTable)
                .withDataProp('data')
                .withOption('oLanguage', {
                    "sEmptyTable": "No matching records found"
                })
                .withOption('responsive', true)
                // .withOption('sDom', '<fl>rt<"clear"i><"F"p>')
                .withOption('processing', true)
                .withOption('serverSide', true)
                // .withFixedHeader({
                //   header: true
                // })
                .withOption('drawCallback', function(settings) {
                    var api = this.api(),
                        listOfRecords = api.data(),
                        state = api.state(),
                        filterRow = angular.element(api.table().header()).children()[1];
                    $(filterRow).children().each(function(index, child) {
                        if ($(child).has('.form-control'))
                            $(child).children('.form-control').val(state.columns[index].search.search);
                    });
                    $scope.dtData = [];
                    for (var i = listOfRecords.length - 1; i >= 0; i--) {
                        $scope.dtData[i] = (listOfRecords[i]);
                        $scope.dtData[i].checked = ($scope.selectedOrganizations.indexOf($scope.dtData[i]._id) >= 0);
                    }
                    $(window).scrollTop(0); // $('body').scrollTop(0);
                    $('[datatable]').css('width', '100%');
                    $compile('directory-organization-record-select')($scope);
                    $compile('directory-organization-record-options')($scope);
                    $compile('directory-organization-record-actions')($scope);
                    $('[datatable] thead>tr>th:nth-child(2)').css('width', '150px');
                    $('[datatable] thead>tr>th:last-child').css('width', '120px');
                })
                .withOption('aoColumns', columnDefs)
                .withLightColumnFilter(filterDefs);

            $rootScope.openDeleteModal = function() {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.remove.html',
                    resolve: {
                        Organizations: [
                            'DirectoryOrganizationService',
                            function(DirectoryOrganizationService) {
                                return DirectoryOrganizationService.list({
                                    id: $scope.selectedOrganizations
                                }).$promise;
                            }
                        ]
                    },
                    controller: 'DirectoryOrganizationRemoveController'
                });
                modalInstance.result.then(function(messages) {
                    if (messages.length > 0) showMessages(messages);
                    $state.reload();
                });
            };
            $rootScope.openSendForApprovalModal = function() {
                for (var i = $scope.selectedOrganizations.length - 1; i >= 0; i--) {
                    var versionId = $scope.selectedOrganizationRecords[$scope.selectedOrganizations[i]].versionId ? $scope.directoryRecordVersion[$scope.selectedOrganizationRecords[$scope.selectedOrganizations[i]].versionId].codeValue : '1';
                    if ($scope.rules.actionVsVersion['directory.organization.sendForApproval'].indexOf(versionId) < 0) {
                        var modalInstance = $uibModal.open({
                            backdrop: 'static',
                            animation: true,
                            templateUrl: '/views/directory.organization.popup.error.tpl.html',
                            controller: [
                                '$scope',
                                '$uibModalInstance',
                                function($modalScope, $uibModalInstance) {
                                    $modalScope.title = "Send for Approval";
                                    $modalScope.message = "You cannot perform this action on the selected record(s). Please select the appropriate records for this action!";
                                    $modalScope.cancel = function() {
                                        $uibModalInstance.dismiss('cancel');
                                    };
                                }
                            ]
                        });
                        return false;
                    }
                }
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/directory.organization.send-for-approval.tpl.html',
                    controller: [
                        '$rootScope',
                        '$localStorage',
                        '$scope',
                        '$uibModalInstance',
                        'DirectoryOrganizationService',
                        function($rootScope, $localStorage, $modalScope, $uibModalInstance, DirectoryOrganizationService) {
                            $modalScope.version = {};
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                            var init = new Date();
                            $modalScope.sendForApproval = function() {
                                DirectoryOrganizationService.sendForApproval({
                                    id: $scope.selectedOrganizations,
                                    version: $modalScope.version
                                }, function(res) {
                                    $uibModalInstance.close();
                                }, function(err) {
                                    console.log(err);
                                    showMessages([{
                                        type: 'error',
                                        message: 'Failed!',
                                        header: 'Send for Approval'
                                    }]);
                                })
                            }
                        }
                    ]
                });
                modalInstance.result.then(function() {
                    showMessages([{
                        type: 'success',
                        header: 'Send for Approval',
                        message: $scope.selectedOrganizations.length + " record(s) Sent for Approval successfully!"
                    }]);
                    $state.reload();
                });
            };
            $rootScope.openSendForCorrectionModal = function() {
                for (var i = $scope.selectedOrganizations.length - 1; i >= 0; i--) {
                    var versionId = $scope.selectedOrganizationRecords[$scope.selectedOrganizations[i]].versionId ? $scope.directoryRecordVersion[$scope.selectedOrganizationRecords[$scope.selectedOrganizations[i]].versionId].codeValue : '1'
                    if ($scope.rules.actionVsVersion['directory.organization.sendForCorrection'].indexOf(versionId) < 0) {
                        var modalInstance = $uibModal.open({
                            backdrop: 'static',
                            animation: true,
                            templateUrl: '/views/directory.organization.popup.error.tpl.html',
                            controller: [
                                '$scope',
                                '$uibModalInstance',
                                function($modalScope, $uibModalInstance) {
                                    $modalScope.title = "Send for Correction";
                                    $modalScope.message = "You cannot perform this action on the selected record(s). Please select the appropriate records for this action!";
                                    $modalScope.cancel = function() {
                                        $uibModalInstance.dismiss('cancel');
                                    };
                                }
                            ]
                        });
                        return false;
                    }
                }
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/directory.organization.send-for-correction.tpl.html',
                    controller: [
                        '$rootScope',
                        '$localStorage',
                        '$scope',
                        '$uibModalInstance',
                        'DirectoryOrganizationService',
                        function($rootScope, $localStorage, $modalScope, $uibModalInstance, DirectoryOrganizationService) {
                            $modalScope.version = {};
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                            var init = new Date();
                            $modalScope.sendForCorrection = function() {
                                DirectoryOrganizationService.sendForCorrection({
                                    id: $scope.selectedOrganizations,
                                    version: $modalScope.version
                                }, function(res) {
                                    $uibModalInstance.close();
                                }, function(err) {
                                    console.log(err);
                                    showMessages([{
                                        type: 'error',
                                        message: 'Failed!',
                                        header: 'Send for Correction'
                                    }]);
                                })
                            }
                        }
                    ]
                });
                modalInstance.result.then(function() {
                    showMessages([{
                        type: 'success',
                        header: 'Send for Correction',
                        message: $scope.selectedOrganizations.length + " record(s) Sent for Correction successfully!"
                    }]);
                    $state.reload();
                });
            };
            $rootScope.openApproveModal = function() {
                for (var i = $scope.selectedOrganizations.length - 1; i >= 0; i--) {
                    var versionId = $scope.selectedOrganizationRecords[$scope.selectedOrganizations[i]].versionId ? $scope.directoryRecordVersion[$scope.selectedOrganizationRecords[$scope.selectedOrganizations[i]].versionId].codeValue : '1'
                    if ($scope.rules.actionVsVersion['directory.organization.approve'].indexOf(versionId) < 0) {
                        var modalInstance = $uibModal.open({
                            backdrop: 'static',
                            animation: true,
                            templateUrl: '/views/directory.organization.popup.error.tpl.html',
                            controller: [
                                '$scope',
                                '$uibModalInstance',
                                function($modalScope, $uibModalInstance) {
                                    $modalScope.title = "Approve";
                                    $modalScope.message = "You cannot perform this action on the selected record(s). Please select the appropriate records for this action!";
                                    $modalScope.cancel = function() {
                                        $uibModalInstance.dismiss('cancel');
                                    };
                                }
                            ]
                        });
                        return false;
                    }
                }
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/directory.organization.approve.tpl.html',
                    controller: [
                        '$rootScope',
                        '$localStorage',
                        '$scope',
                        '$uibModalInstance',
                        'DirectoryOrganizationService',
                        function($rootScope, $localStorage, $modalScope, $uibModalInstance, DirectoryOrganizationService) {
                            $modalScope.version = {};
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                            var init = new Date();
                            $modalScope.approve = function() {
                                DirectoryOrganizationService.approve({
                                    id: $scope.selectedOrganizations,
                                    version: $modalScope.version
                                }, function(res) {
                                    $uibModalInstance.close();
                                }, function(err) {
                                    console.log(err);
                                    showMessages([{
                                        type: 'error',
                                        message: 'Failed!',
                                        header: 'Approve Record'
                                    }]);
                                })
                            }
                        }
                    ]
                });
                modalInstance.result.then(function() {
                    showMessages([{
                        type: 'success',
                        header: 'Approve Record',
                        message: $scope.selectedOrganizations.length + " record(s) Approved successfully!"
                    }]);
                    $state.reload();
                });
            };
            $rootScope.openExportToExcelModal = function() {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    // size: 'lg',
                    templateUrl: '/views/directory.organization.export-to-excel.tpl.html',
                    resolve: {
                        DirectoryOrganizationCount: [
                            'DirectoryOrganizationService',
                            function(DirectoryOrganizationService) {
                                return DirectoryOrganizationService.getOrganizationCount({
                                    directoryId: $localStorage['DMS-DIRECTORY']._id
                                }).$promise;
                            }
                        ],
                    },
                    controller: 'DirectoryOrganizationExportToExcelController'
                });
                modalInstance.result.then(function(messages) {
                    $state.reload();
                });
            };
            $scope.showVersionHistory = function(recordId) {
                DirectoryOrganizationVersionService.get({
                    organization: recordId
                }, function(versions) {
                    var recordVersions = [];
                    for (var i = 0; i <= (versions.length - 1); i++) {
                        if (versions[i].data) recordVersions.push(versions[i]);
                    }
                    if (recordVersions && recordVersions.length > 0) {
                        $state.go('portal.directory.organization.diff', {
                            id: recordId,
                            versionId: recordVersions[0]._id
                        }, { reload: true });
                    } else {
                        showMessages([{
                            type: 'error',
                            message: 'There is no history for this record!',
                            header: 'Record History'
                        }]);
                    }
                }, function(err) {
                    console.log(err);
                    showMessages([{
                        type: 'error',
                        message: 'Failed to retrive history for this record!',
                        header: 'Record History'
                    }]);
                });
            };
            $scope.openVersionHistoryModal = function(recordId) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    size: 'lg',
                    templateUrl: '/views/directory.organization.version-history.tpl.html',
                    resolve: {
                        VersionHistory: [
                            'DirectoryOrganizationVersionService',
                            function(DirectoryOrganizationVersionService) {
                                return DirectoryOrganizationVersionService.get({
                                    organization: recordId
                                }).$promise;
                            }
                        ]
                    },
                    controller: [
                        '$rootScope',
                        '$localStorage',
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        'VersionHistory',
                        function($rootScope, $localStorage, $modalScope, $state, $uibModalInstance, VersionHistory) {
                            $modalScope.versions = VersionHistory;
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    ]
                });
            };
        }
    ]);
