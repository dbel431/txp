angular.module('directory.organization')
    .controller('DirectoryOrganizationRequestForCorrectionController', [
        '$rootScope',
        '$localStorage',
        '$scope',
        '$uibModalInstance',
        'Organizations',
        function($rootScope, $localStorage, $scope, $uibModalInstance, Organizations) {
            $scope.version = {};
            $scope.organizations = Organizations;
            for (var i = $scope.organizations.length - 1; i >= 0; i--) {
                $scope.organizations[i]._oldOrgRecord = getObjectData($scope.organizations[i]);
            }
            $scope.cancel = function() {
                $uibModalInstance.dismiss('canc<div></div>el');
            };
            var messages = [];
            $scope.requestForCorrection = function() {
                for (var i = $scope.organizations.length - 1; i >= 0; i--) {
                    $scope.organizations[i].currentVersion = $scope.version;
                    $scope.organizations[i].$requestForCorrection(function(org) {
                        messages.push({
                            type: 'success',
                            message: 'Record named: <b>' + org.name + '</b> Request Send For Correction successfully!',
                            header: 'Record Send For Correction  '
                        });
                        syncXhr();
                    }, function(err) {
                        messages.push({
                            type: 'error',
                            message: 'Record named: <b>' + org.name + '</b> could not be Request Send For Correction successfully!',
                            header: 'Record Send For Correction  '
                        });
                        syncXhr();
                    });
                }
            };
            var count = 0;

            function syncXhr() {
                count++;
                if ($scope.loading = (count == $scope.organizations.length))
                    $uibModalInstance.close(messages);
            }
        }
    ]);
