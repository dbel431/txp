angular.module('directory.organization')
    .controller('DirectoryOrganizationAddController', [
        '$rootScope',
        '$localStorage',
        '$scope',
        '$state',
        '$uibModal',
        'DirectoryOrganizationService',
        'ClassificationCode',
        'Section',
        'AddressType',
        'ListingType',
        'DirectoryRecordStatus',
        'DirectoryPensionReportInterval',
        'PrimaryMarket',
        'AdvanceBillType',
        'DioceseType',
        'DioProvince',
        'ParishStatus',
        'EthnicityType',
        'SchoolType',
        'ReligiousType',
        'GradeType',
        'Supplement',
        'OrganizationType',
        'DirectoryCityService',
        'DirectoryStateService',
        'DirectoryCountyService',
        'DTOptionsBuilder',
        function($rootScope, $localStorage, $scope, $state, $uibModal, DirectoryOrganizationService, ClassificationCode, Section, AddressType, ListingType, DirectoryRecordStatus, DirectoryPensionReportInterval, PrimaryMarket, AdvanceBillType, DioceseType, DioProvince, ParishStatus, EthnicityType, SchoolType, ReligiousType, GradeType, Supplement, OrganizationType, DirectoryCityService, DirectoryStateService, DirectoryCountyService, DTOptionsBuilder) {
            $scope.editState = true;
            $scope.loading = false;
            $scope.ethnicityText1Active = true;
            $scope.ethnicityText2Active = true;
            $scope.orgTypeActivedDio = false;
            $scope.orgTypeActiveReligious = false;
            $scope.orgTypeActiveSchool = false;
            $scope.orgTypeActiveParish = false;
            $rootScope.page = {
                name: $localStorage['DMS-DIRECTORY'].description + ': Add new Record',
                actions: {
                    back: {
                        object: 'directory.organization.list',
                        allowed: function() {
                            return $rootScope.isActionPermitted(this.object);
                        }
                    },
                    submit: {
                        ngClick: function() {
                            $scope.submit();
                        },
                        label: 'Submit',
                        ngDisabled: function() {
                            if ($rootScope.currentDirectory.description == "Official Catholic Directory") {
                                var flag = !($scope.addForm.$invalid || $scope.loading);
                                flag = flag && $scope.organization.name && $scope.organization.name != '';
                                return !flag
                            } else if ($rootScope.currentDirectory.description == "American Art Directory") {
                                var flag = !($scope.loading);
                                flag = flag && $scope.organization.name && $scope.organization.name != '';
                                return !flag

                            } else {
                                var flag = !($scope.addForm.$invalid || $scope.loading);
                                flag = flag && $scope.organization.name && $scope.organization.name != '';
                                return !$rootScope.validFlag;
                            }

                        }
                    },
                    send: {
                        object: 'directory.organization.sendForApproval',
                        allowed: function() {
                            return $rootScope.isActionPermitted(this.object);
                        },
                        ngClick: function() { $scope.submitAndSendForApproval(); },
                        label: 'Submit & Send for Approval',
                        ngDisabled: function() {
                            if ($rootScope.currentDirectory.description == "Official Catholic Directory") {
                                var flag = !($scope.addForm.$invalid || $scope.loading);
                                flag = flag && $scope.organization.name && $scope.organization.name != '';
                                return !flag
                            } else if ($rootScope.currentDirectory.description == "American Art Directory") {
                                var flag = !($scope.loading);
                                flag = flag && $scope.organization.name && $scope.organization.name != '';
                                return !flag

                            } else {
                                var flag = !($scope.addForm.$invalid || $scope.loading);
                                flag = flag && $scope.organization.name && $scope.organization.name != '';
                                return !$rootScope.validFlag;
                            }
                        }
                    }
                }
            };
            $scope.organization = {
                // established: [],
                membership: [],
                formerNames: [],
                address: [],
                contact: {},
                features: [],
                research: [],
                publication: [],
                // activities: [],
                artSchool: [],
                bookEdition: [],
                members: [],
                // numberOfOffices: [],
                // annualRevenues: [],
                // feePercentage: [],
                // professionalStaff: [],
                comments: [],
                directMarketingBudgetDisbursal: [],
                notes: [],
                sectionCode: [],
                listingType: [],
                /* vendors: [],*/
                vendorType: [],
                statistic: [],
                stathist: [],
                //for OCD
                school: {},
                religiousOrder: {},
                parishShrine: {},
            };

            // For OCD Classification Validation
            $scope.clearOCDDataAsPerClassificationCode = function($item) {
                $scope.organization = clearOCDDataAsPerClassificationCode($scope.organization);
                $scope.organization.classificationCodeName = $item.codeValue;
            };
            OCDClassificationCodeCache.cacheCurrentValues($scope.organization);

            $scope.classificationCodeArray = ClassificationCode;
            $scope.recordStatusArray = DirectoryRecordStatus;
            $scope.pensionReportIntervalArray = DirectoryPensionReportInterval;
            $scope.sectionArray = Section;
            $scope.listingTypeArray = ListingType;
            $scope.primaryMarketArray = PrimaryMarket;
            $scope.advanceBillTypeArray = AdvanceBillType;
            //for OCD
            $scope.dioceseTypeArray = DioceseType;
            $scope.dioProvinceArray = DioProvince;
            $scope.parishStatusArray = ParishStatus;
            $scope.ethnicityTypeArray = EthnicityType;
            $scope.schoolTypeArray = SchoolType;
            $scope.religiousTypeArray = ReligiousType;
            $scope.gradeTypeArray = GradeType;
            $scope.supplimentTypeArray = Supplement;
            $scope.organizationTypeArray = OrganizationType;
            // $scope.statisticsTypeArray = StatisticsType;
            $scope.validate = function() {
                showMessages([{
                    type: 'success',
                    message: 'Information validated and saved!',
                    header: 'Add new Record'
                }]);
                $scope.addForm.$setUntouched();
            };
            $scope.submit = function() {
                $scope.loading = true;
                var newOrg = new DirectoryOrganizationService($scope.organization);
                newOrg.isAADOrg = (/AAD/i).test($localStorage['DMS-DIRECTORY'].codeValue);
                if (newOrg.isAADOrg) {
                    if (newOrg.attendance === undefined) {
                        newOrg.orgIdNumber = isNaN(parseInt(newOrg.org_id)) ? null : parseInt(newOrg.org_id);
                        newOrg.directoryId = $localStorage['DMS-DIRECTORY']._id;
                        newOrg.$save(function(organization) {
                            showMessages([{
                                header: 'New Record',
                                message: 'Record saved successfully!',
                                type: 'success'
                            }]);
                            $scope.loading = false;
                            $state.go('portal.directory.organization.list');
                        }, function(err) {
                            showMessages([{
                                header: 'New Record',
                                message: 'Record could not be saved successfully!',
                                type: 'error'
                            }]);
                            $scope.loading = false;
                        });
                    } else {
                        newOrg.attendance.countNumber = parseFloat(newOrg.attendance.count.replace(',', ''));
                        newOrg.orgIdNumber = isNaN(parseInt(newOrg.org_id)) ? null : parseInt(newOrg.org_id);
                        newOrg.directoryId = $localStorage['DMS-DIRECTORY']._id;
                        newOrg.$save(function(organization) {
                            showMessages([{
                                header: 'New Record',
                                message: 'Record saved successfully!',
                                type: 'success'
                            }]);
                            $scope.loading = false;
                            $state.go('portal.directory.organization.list');
                        }, function(err) {
                            showMessages([{
                                header: 'New Record',
                                message: 'Record could not be saved successfully!',
                                type: 'error'
                            }]);
                            $scope.loading = false;
                        });
                    }
                } else {
                    if (newOrg.totalAdvertisingBudget != undefined)
                        newOrg.totalAdvBudgetAmount = parseFloat(newOrg.totalAdvertisingBudget.replace(',', ''));
                    newOrg.orgIdNumber = isNaN(parseInt(newOrg.org_id)) ? null : parseInt(newOrg.org_id);
                    newOrg.directoryId = $localStorage['DMS-DIRECTORY']._id;
                    newOrg.$save(function(organization) {
                        showMessages([{
                            header: 'New Record',
                            message: 'Record saved successfully!',
                            type: 'success'
                        }]);
                        $scope.loading = false;
                        $state.go('portal.directory.organization.list');
                    }, function(err) {
                        showMessages([{
                            header: 'New Record',
                            message: 'Record could not be saved successfully!',
                            type: 'error'
                        }]);
                        $scope.loading = false;
                    });
                }
            };
            $scope.submitAndSendForApproval = function() {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/directory.organization.save-and-send-for-approval.tpl.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            // $scope.organizationRecordStatusArray = OrganizationRecordStatus;
                            $modalScope.organization = $scope.organization;
                            $modalScope.editFlag = $modalScope.organization._id || false;
                            $modalScope.cancel = function() {
                                $scope.organization.currentVersion = {};
                                $uibModalInstance.dismiss('cancel');
                            };
                            $modalScope.save = function() {
                                $uibModalInstance.close($modalScope.version);
                            };
                        }
                    ]
                });
                modalInstance.result.then(function(version, $modalScope) {
                    $scope.loading = true;
                    var newOrg = new DirectoryOrganizationService($scope.organization);
                    newOrg.isAADOrg = (/AAD/i).test($localStorage['DMS-DIRECTORY'].codeValue);
                    if (newOrg.isAADOrg) {
                        if (newOrg.attendance === undefined) {
                            newOrg.orgIdNumber = isNaN(parseInt(newOrg.org_id)) ? null : parseInt(newOrg.org_id);
                            newOrg.directoryId = $localStorage['DMS-DIRECTORY']._id;
                            newOrg.$submitAndSendForApproval(function(organization) {
                                showMessages([{
                                    header: 'New Record',
                                    message: 'Record saved and sent for approval successfully!',
                                    type: 'success'
                                }]);
                                $scope.loading = false;
                                $state.go('portal.directory.organization.list');
                            }, function(err) {
                                showMessages([{
                                    header: 'New Record',
                                    message: 'Record could not be saved and sent for approval successfully!',
                                    type: 'error'
                                }]);
                                $scope.loading = false;
                            });
                        } else {
                            newOrg.attendance.countNumber = parseFloat(newOrg.attendance.count.replace(',', ''));
                            newOrg.orgIdNumber = isNaN(parseInt(newOrg.org_id)) ? null : parseInt(newOrg.org_id);
                            newOrg.directoryId = $localStorage['DMS-DIRECTORY']._id;
                            newOrg.$submitAndSendForApproval(function(organization) {
                                showMessages([{
                                    header: 'New Record',
                                    message: 'Record saved and sent for approval successfully!',
                                    type: 'success'
                                }]);
                                $scope.loading = false;
                                $state.go('portal.directory.organization.list');
                            }, function(err) {
                                showMessages([{
                                    header: 'New Record',
                                    message: 'Record could not be saved and sent for approval successfully!',
                                    type: 'error'
                                }]);
                                $scope.loading = false;
                            });
                        }
                    } else {
                        if (newOrg.totalAdvertisingBudget != undefined)
                            newOrg.totalAdvBudgetAmount = parseFloat(newOrg.totalAdvertisingBudget.replace(',', ''));
                        newOrg.orgIdNumber = isNaN(parseInt(newOrg.org_id)) ? null : parseInt(newOrg.org_id);
                        newOrg.directoryId = $localStorage['DMS-DIRECTORY']._id;
                        newOrg.$submitAndSendForApproval(function(organization) {
                            showMessages([{
                                header: 'New Record',
                                message: 'Record saved and sent for approval successfully!',
                                type: 'success'
                            }]);
                            $scope.loading = false;
                            $state.go('portal.directory.organization.list');
                        }, function(err) {
                            showMessages([{
                                header: 'New Record',
                                message: 'Record could not be saved and sent for approval successfully!',
                                type: 'error'
                            }]);
                            $scope.loading = false;
                        });
                    }
                });
            };

            //for OCD Ethnicity Validation
            $scope.changeethnicityType1 = function(ethnicityType1) {
                $scope.organization.parishShrine.ethnicityType1Name = ethnicityType1.codeValue;
                if (ethnicityType1.codeValue != "Other") {
                    $scope.organization.parishShrine.ethnicityText1 = "";
                    $scope.ethnicityText1Active = true;
                }
                if (ethnicityType1.codeValue == "Other") {
                    $scope.ethnicityText1Active = false;
                }
            }
            $scope.changeethnicityType2 = function(ethnicityType2) {
                $scope.organization.parishShrine.ethnicityType2Name = ethnicityType2.codeValue;
                if (ethnicityType2.codeValue != "Other") {
                    $scope.organization.parishShrine.ethnicityText2 = "";
                    $scope.ethnicityText2Active = true;
                }
                if (ethnicityType2.codeValue == "Other") {
                    $scope.ethnicityText2Active = false;
                }
            }

            // -------------
            // Former Names
            // -------------
            $scope.addFormerName = function(index, formerName) {

                $scope.organization.formerNames.push(formerName);

            };
            $scope.editFormerName = function(index, formerName) {
                $scope.organization.formerNames[index] = formerName;
            };
            $scope.removeFormerName = function(index) {
                if (confirm('Are you sure?'))
                    $scope.organization.formerNames.splice(index, 1);
            };
            $scope.openFormerNamesPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.formerNames.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            $modalScope.editFlag = (typeof index != 'undefined');
                            $modalScope.formerName = $modalScope.editFlag ? getObjectData($scope.organization.formerNames[index]) : {};
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                            $modalScope.add = function() {
                                $uibModalInstance.close({
                                    action: 'add',
                                    data: $modalScope.formerName
                                });
                            };
                            $modalScope.update = function() {

                                $uibModalInstance.close({
                                    action: 'edit',
                                    data: $modalScope.formerName
                                });
                            };
                            $modalScope.formerName.startDate = $modalScope.formerName.startDate ? new Date($modalScope.formerName.startDate) : '';
                            $modalScope.formerName.endDate = $modalScope.formerName.endDate ? new Date($modalScope.formerName.endDate) : '';
                            $modalScope.startDateOptions = {
                                minDate: new Date($modalScope.formerName.startDate),
                                maxDate: new Date($modalScope.formerName.endDate)
                            };
                            $modalScope.endDateOptions = {
                                minDate: new Date($modalScope.formerName.startDate),
                                maxDate: new Date($modalScope.formerName.endDate)
                            };
                            $modalScope.$watch('formerName.endDate', function(newVal) {
                                $modalScope.startDateOptions.maxDate = new Date(newVal || '');
                                $modalScope.endDateOptions.maxDate = new Date(newVal || '');
                            });
                            $modalScope.$watch('formerName.startDate', function(newVal) {
                                $modalScope.startDateOptions.minDate = new Date(newVal || '');
                                $modalScope.endDateOptions.minDate = new Date(newVal || '');
                            });
                        }

                    ]
                });
                modalInstance.result.then(function(op) {
                    if (!$scope.organization.formerNames) $scope.organization.formerNames = [];
                    $scope[op.action + 'FormerName'](index, op.data);
                });

            };

            // -------------
            // Address
            // -------------
            // $scope.addressTypes = [];
            // for (var i = AddressType.length - 1; i >= 0; i--) {
            //     $scope.addressTypes[AddressType[i]._id] = AddressType[i].codeValue + ' (' + AddressType[i].description + ')';
            // }
            $scope.addAddress = function(index, address) {
                $scope.organization.address.push(address);
            };
            $scope.editAddress = function(index, address) {
                $scope.organization.address[index] = address;
            };
            $scope.removeAddress = function(index) {
                $scope.organization.address.splice(index, 1);
            };
            $scope.openAddressPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    size: 'lg',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.address.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        'DirectoryCountryService',
                        'DirectoryCountyService',
                        'DirectoryStateService',
                        'DirectoryCityService',
                        function($modalScope, $state, $uibModalInstance, DirectoryCountryService, DirectoryCountyService, DirectoryStateService, DirectoryCityService) {
                            $modalScope.addressTypeArray = AddressType;
                            $modalScope.editFlag = (typeof index != 'undefined');
                            $modalScope.address = $modalScope.editFlag ? getObjectData($scope.organization.address[index]) : {};
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                            $modalScope.add = function() {
                                $uibModalInstance.close({
                                    action: 'add',
                                    data: $modalScope.address
                                });
                            };
                            $modalScope.update = function() {
                                $uibModalInstance.close({
                                    action: 'edit',
                                    data: $modalScope.address
                                });
                            };
                            $modalScope.getAddressCities = function(val) {
                                $('[ng-show="noResultsCity"]').html(val.length > 0 ? '<i class="glyphicon glyphicon-remove"></i> No Results Found' : '');
                                if (val.length > 2) {
                                    return DirectoryCityService.list({
                                        codeValue: {
                                            _eval: 'regex',
                                            value: val
                                        },
                                        active: true
                                    }).$promise;
                                } else $('[ng-show="noResultsCity"]').html('Enter minimum 3 characters');
                            };
                            $modalScope.selectAddressCityTypeAhead = function($item, $model, $label, $event) {
                                $modalScope.address.city = $model;
                                $modalScope.address.cityName = $model.codeValue;
                                $modalScope.address.state = $model.parentId;
                                $modalScope.address.stateName = $model.parentId.codeValue + " (" + $model.parentId.description + ")";
                                $modalScope.address.country = $model.parentId.parentId;
                                $modalScope.address.countryName = $model.parentId.parentId.codeValue;
                            };
                            $modalScope.getAddressStates = function(val) {
                                $('[ng-show="noResultsState"]').html(val.length > 0 ? '<i class="glyphicon glyphicon-remove"></i> No Results Found' : '');
                                if (val.length > 2) {
                                    return DirectoryStateService.list({
                                        codeValue: {
                                            _eval: 'regex',
                                            value: val
                                        },
                                        active: true
                                    }).$promise;
                                } else $('[ng-show="noResultsState"]').html('Enter minimum 3 characters');
                            };
                            $modalScope.selectAddressStateTypeAhead = function($item, $model, $label, $event) {
                                $modalScope.address.state = $model;
                                $modalScope.address.stateName = $model.codeValue + " (" + $model.description + ")";
                                $modalScope.address.country = $model.parentId;
                                $modalScope.address.countryName = $model.parentId.codeValue;
                            };
                            $modalScope.getAddressCountries = function(val) {
                                $('[ng-show="noResultsCountry"]').html(val.length > 0 ? '<i class="glyphicon glyphicon-remove"></i> No Results Found' : '');
                                if (val.length > 2) {
                                    return DirectoryCountryService.list({
                                        codeValue: {
                                            _eval: 'regex',
                                            value: val
                                        },
                                        active: true
                                    }).$promise;
                                } else $('[ng-show="noResultsCountry"]').html('Enter minimum 3 characters');
                            };
                            $modalScope.selectAddressCountryTypeAhead = function($item, $model, $label, $event) {
                                $modalScope.address.country = $model;
                                $modalScope.address.countryName = $model.codeValue;
                            };
                            $modalScope.getAddressCountys = function(val) {
                                $('[ng-show="noResultsCounty"]').html(val.length > 0 ? '<i class="glyphicon glyphicon-remove"></i> No Results Found' : '');
                                if (val.length > 2) {
                                    return DirectoryCountyService.list({
                                        codeValue: {
                                            _eval: 'regex',
                                            value: val
                                        },
                                        active: true
                                    }).$promise;
                                } else $('[ng-show="noResultsCounty"]').html('Enter minimum 3 characters');
                            };
                            $modalScope.selectAddressCountyTypeAhead = function($item, $model, $label, $event) {
                                $modalScope.address.county = $model;
                                $modalScope.address.countyName = $model.codeValue;
                            };
                        }
                    ]
                });
                modalInstance.result.then(function(op) {
                    if (!$scope.organization.address) $scope.organization.address = [];
                    $scope[op.action + 'Address'](index, op.data);
                });
            };

            //for OCD Placement City
            $scope.getAddressCities = function(val) {
                $('[ng-show="noResultsCity"]').html(val.length > 0 ? '<i class="glyphicon glyphicon-remove"></i> No Results Found' : '');
                if (val.length > 2) {
                    return DirectoryCityService.list({
                        codeValue: {
                            _eval: 'regex',
                            value: val
                        },
                        active: true
                    }).$promise;
                } else $('[ng-show="noResultsCity"]').html('Enter minimum 3 characters');
            };
            $scope.formatPlacementTypeAhead = function($model) {
                return $model ? $model : "";
            };
            $scope.selectPlacementTypeAhead = function($item) {
                $scope.organization.placementCity = $item.codeValue;
            };

            //for OCD Header state
            $scope.getheaderStates = function(val) {
                $('[ng-show="noResultsState"]').html(val.length > 0 ? '<i class="glyphicon glyphicon-remove"></i> No Results Found' : '');
                if (val.length > 2) {
                    return DirectoryStateService.list({
                        codeValue: {
                            _eval: 'regex',
                            value: val
                        },
                        active: true
                    }).$promise;
                } else $('[ng-show="noResultsState"]').html('Enter minimum 3 characters');
            };
            $scope.formatStateTypeAhead = function($model) {
                return $model ? $model : "";
            };
            $scope.selectStateTypeAhead = function($item) {
                $scope.organization.header.state = $item.codeValue;
            };

            //for OCD header County
            $scope.getHeaderCountys = function(val) {
                $('[ng-show="noResultsCounty"]').html(val.length > 0 ? '<i class="glyphicon glyphicon-remove"></i> No Results Found' : '');
                if (val.length > 2) {
                    return DirectoryCountyService.list({
                        codeValue: {
                            _eval: 'regex',
                            value: val
                        },
                        active: true
                    }).$promise;
                } else $('[ng-show="noResultsCounty"]').html('Enter minimum 3 characters');
            };
            $scope.formatHeaderTypeAhead = function($model) {
                return $model ? $model : "";
            };
            $scope.selectHeaderCountyTypeAhead = function($item) {
                $scope.organization.header.county = $item.codeValue;
            };


            //Please do not uncomment this code
            // Last modified by Swapnil Sonawane
            //Date: 6/06/2016
            // -------------
            // Contact
            // -------------
            /*  $scope.addContacts = function(index, contact) {
                $scope.organization.contact.push(contact);
            };
            $scope.editContacts = function(index, contact) {
                $scope.organization.contact[index] = contact;
            };
            $scope.removeContacts = function(index) {
                $scope.organization.contact.splice(index, 1);
            };
            $scope.openContactsPopup = function(index) {
                var modalInstance = $uibModal.open({
backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.contacts.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            $modalScope.editFlag = (typeof index != 'undefined');
                            $modalScope.contact = $modalScope.editFlag ? getObjectData($scope.organization.contact[index]) : {};
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                            $modalScope.add = function() {
                                $uibModalInstance.close({
                                    action: 'add',
                                    data: $modalScope.contact
                                });
                            };
                            $modalScope.update = function() {
                                $uibModalInstance.close({
                                    action: 'edit',
                                    data: $modalScope.contact
                                });
                            };
                        }
                    ]
                });
                modalInstance.result.then(function(op) {
                    if (!$scope.organization.contact) $scope.organization.contact = [];
                    $scope[op.action + 'Contacts'](index, op.data);
                });
            };
*/
            // -------------
            // Key Personnel
            // -------------
            $scope.addKeyPersonnel = function(index, personnel) {
                personnel.directoryId = $localStorage['DMS-DIRECTORY']._id;
                $scope.organization.personnel.push(personnel);
            };
            $scope.editKeyPersonnel = function(index, personnel) {
                personnel.directoryId = $localStorage['DMS-DIRECTORY']._id;
                $scope.organization.personnel[index] = personnel;
            };
            $scope.removeKeyPersonnel = function(index) {
                $scope.organization.personnel.splice(index, 1);
            };
            $scope.showPersonnelName = function(personnel) {
                return [(personnel.name.prefix || ""), (personnel.name.first || ""), (personnel.name.middle || ""), (personnel.name.last || ""), (personnel.name.suffix || "")].join(' ');
            };
            $scope.openKeyPersonnelPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    size: 'lg',
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        'DirectoryKeyPersonnelTitleService',
                        function($modalScope, $state, $uibModalInstance, DirectoryKeyPersonnelTitleService) {
                            $modalScope.getKeyPersonnelTitles = function(val) {
                                $('[ng-show="noResultsTitle"]').html(val.length > 0 ? '<i class="glyphicon glyphicon-remove"></i> No Results Found' : '');
                                if (val.length > 2) {
                                    return DirectoryKeyPersonnelTitleService.list({
                                        directoryId: $localStorage['DMS-DIRECTORY']._id,
                                        codeValue: {
                                            _eval: 'regex',
                                            value: val
                                        }
                                    }).$promise;
                                } else $('[ng-show="noResultsTitle"]').html('Enter minimum 3 characters');
                            };
                            $modalScope.formatKeypersonnelTypeAhead = function($model) {
                                return $model ? $model.codeValue : "";
                            };
                            $modalScope.selectKeypersonnelTypeAhead = function($item) {
                                $modalScope.keyPersonnel.titleMaster = $item;
                                $modalScope.keyPersonnel.titleMasterName = $item.codeName;
                            };
                            $modalScope.editFlag = (typeof index != 'undefined');
                            $modalScope.keyPersonnel = $modalScope.editFlag ? getObjectData($scope.organization.personnel[index]) : {};
                            if (!$modalScope.editFlag) $modalScope.keyPersonnel.active = true;
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                            $modalScope.add = function() {
                                $uibModalInstance.close({
                                    action: 'add',
                                    data: $modalScope.keyPersonnel
                                });
                            };
                            $modalScope.update = function() {
                                $uibModalInstance.close({
                                    action: 'edit',
                                    data: $modalScope.keyPersonnel
                                });
                            };
                        }
                    ]
                });
                modalInstance.result.then(function(op) {
                    if (!$scope.organization.personnel) $scope.organization.personnel = [];
                    $scope[op.action + 'KeyPersonnel'](index, op.data);
                });
            };



            //KeyPersonnel For OCD
            $scope.addOCDKeyPersonnel = function(index, personnel) {
                personnel.directoryId = $localStorage['DMS-DIRECTORY']._id;
                $scope.organization.personnel.push(personnel);
            };
            $scope.editOCDKeyPersonnel = function(index, personnel) {
                personnel.directoryId = $localStorage['DMS-DIRECTORY']._id;
                $scope.organization.personnel[index] = personnel;
            };
            $scope.removeOCDKeyPersonnel = function(index) {
                $scope.organization.personnel.splice(index, 1);
            };
            $scope.showOCDPersonnelName = function(personnel) {
                return [(personnel.name.prefix || ""), (personnel.name.first || ""), (personnel.name.middle || ""), (personnel.name.last || ""), (personnel.name.suffix || "")].join(' ');
            };
            $scope.openOCDKeyPersonnelPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    size: 'lg',
                    windowClass: 'full-window',
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.html',
                    resolve: {
                        PersonTypes: [
                            'DirectoryPersonTypeService',
                            function(DirectoryPersonTypeService) {
                                return DirectoryPersonTypeService.get().$promise;
                            }
                        ],
                        AddressType: [
                            'DirectoryAddressTypeService',
                            function(DirectoryAddressTypeService) {
                                return DirectoryAddressTypeService.get().$promise;
                            }
                        ],
                        DegreeType: [
                            'DirectoryDegreeTypeService',
                            function(DirectoryDegreeTypeService) {
                                return DirectoryDegreeTypeService.get().$promise;
                            }
                        ],
                        AssignmentType: [
                            'DirectoryAssignmentTypeService',
                            function(DirectoryAssignmentTypeService) {
                                return DirectoryAssignmentTypeService.get().$promise;
                            }
                        ],
                        ContactType: [
                            'DirectoryContactTypeService',
                            function(DirectoryContactTypeService) {
                                return DirectoryContactTypeService.get().$promise;
                            }
                        ],
                        DirectoryRecordStatus: [
                            'DirectoryRecordStatusService',
                            function(DirectoryRecordStatusService) {
                                return DirectoryRecordStatusService.get().$promise;
                            }
                        ]
                    },
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        'DirectoryKeyPersonnelTitleService',
                        'PersonTypes',
                        'AddressType',
                        'DegreeType',
                        'DirectoryPersonnelService',
                        'DirectoryCountryService',
                        'DirectoryCountyService',
                        'DirectoryStateService',
                        'DirectoryCityService',
                        'AssignmentType',
                        'ContactType',
                        'DirectoryRecordStatus',
                        function($modalScope, $state, $uibModalInstance, DirectoryKeyPersonnelTitleService, PersonTypes, AddressType, DegreeType, DirectoryPersonnelService, DirectoryCountryService, DirectoryCountyService, DirectoryStateService, DirectoryCityService, AssignmentType, ContactType, DirectoryRecordStatus) {
                            $modalScope.personTypeArray = PersonTypes;
                            $modalScope.addressTypeArray = AddressType;
                            $modalScope.addressTypes = $scope.addressTypes;
                            $modalScope.degreeTypeArray = DegreeType;
                            $modalScope.assignmentTypeArray = AssignmentType;
                            $modalScope.showOCDPersonnelName = $scope.showOCDPersonnelName;
                            $modalScope.contactTypeArray = ContactType;
                            $modalScope.recordStatusArray = DirectoryRecordStatus;
                            $modalScope.editFlag = (typeof index != 'undefined');
                            $modalScope.keyPersonnel = $modalScope.editFlag ? getObjectData($scope.organization.personnel[index]) : {
                                address: [],
                                ocdDegree: [],
                                notes: [],
                                assignment: [],
                                ocdContact: []
                            };
                            if (!$modalScope.editFlag) $modalScope.keyPersonnel.active = true;
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                            $modalScope.add = function() {
                                $uibModalInstance.close({
                                    action: 'add',
                                    data: $modalScope.keyPersonnel
                                });
                            };
                            $modalScope.update = function() {
                                $uibModalInstance.close({
                                    action: 'edit',
                                    data: $modalScope.keyPersonnel
                                });
                            };
                            $modalScope.personnelIndex = index;
                            $modalScope.openRemovePopup = $scope.openRemovePopup;
                            $modalScope.getOCDPersonnel = function(val, prefix) {
                                $('[ng-show="noResultsPersonnel' + prefix + 'Name"]').html(val.length > 0 ? '<i class="glyphicon glyphicon-remove"></i> No Results Found' : '');
                                if (val.length > 2) {
                                    var query = {
                                        directoryId: $localStorage['DMS-DIRECTORY']._id,
                                        active: true
                                    };
                                    if ($modalScope.keyPersonnel && $modalScope.keyPersonnel.name) {
                                        query['name.prefix'] = $modalScope.keyPersonnel.name.prefix ? {
                                            _eval: 'regex',
                                            value: $modalScope.keyPersonnel.name.prefix
                                        } : undefined;
                                        query['name.first'] = $modalScope.keyPersonnel.name.first ? {
                                            _eval: 'regex',
                                            value: $modalScope.keyPersonnel.name.first
                                        } : undefined;
                                        query['name.middle'] = $modalScope.keyPersonnel.name.middle ? {
                                            _eval: 'regex',
                                            value: $modalScope.keyPersonnel.name.middle
                                        } : undefined;
                                        query['name.last'] = $modalScope.keyPersonnel.name.last ? {
                                            _eval: 'regex',
                                            value: $modalScope.keyPersonnel.name.last
                                        } : undefined;
                                        query['name.suffix'] = $modalScope.keyPersonnel.name.suffix ? {
                                            _eval: 'regex',
                                            value: $modalScope.keyPersonnel.name.suffix
                                        } : undefined;
                                    }
                                    return DirectoryPersonnelService.list(query).$promise;
                                } else $('[ng-show="noResultsPersonnel' + prefix + 'Name"]').html('Enter minimum 3 characters');
                            };
                            $modalScope.selectPersonnel = function($item, $model) {
                                $modalScope.keyPersonnel = $model;
                                ['Prefix', 'First', 'Middle', 'Last', 'Suffix'].forEach(function(prefix) {
                                    $('[ng-show="noResultsPersonnel' + prefix + 'Name"]').html('');
                                })
                            };
                            $modalScope.getAddressCities = function(val) {
                                $('[ng-show="noResultsCity"]').html(val.length > 0 ? '<i class="glyphicon glyphicon-remove"></i> No Results Found' : '');
                                if (val.length > 2) {
                                    return DirectoryCityService.list({
                                        codeValue: {
                                            _eval: 'regex',
                                            value: val
                                        },
                                        active: true
                                    }).$promise;
                                } else $('[ng-show="noResultsCity"]').html('Enter minimum 3 characters');
                            };
                            $modalScope.selectAddressCityTypeAhead = function($item, $model, $label, $event) {
                                $modalScope.newContact.city = $model;
                                $modalScope.newContact.cityName = $model.codeValue;
                                $modalScope.newContact.state = $model.parentId;
                                $modalScope.newContact.stateName = $model.parentId.codeValue + " (" + $model.parentId.description + ")";
                                $modalScope.newContact.country = $model.parentId.parentId;
                                $modalScope.newContact.countryName = $model.parentId.parentId.codeValue;
                            };
                            $modalScope.getAddressStates = function(val) {
                                $('[ng-show="noResultsState"]').html(val.length > 0 ? '<i class="glyphicon glyphicon-remove"></i> No Results Found' : '');
                                if (val.length > 2) {
                                    return DirectoryStateService.list({
                                        codeValue: {
                                            _eval: 'regex',
                                            value: val
                                        },
                                        active: true
                                    }).$promise;
                                } else $('[ng-show="noResultsState"]').html('Enter minimum 3 characters');
                            };
                            $modalScope.selectAddressStateTypeAhead = function($item, $model, $label, $event) {
                                $modalScope.newContact.state = $model;
                                $modalScope.newContact.stateName = $model.codeValue + " (" + $model.description + ")";
                                $modalScope.newContact.country = $model.parentId;
                                $modalScope.newContact.countryName = $model.parentId.codeValue;
                            };
                            $modalScope.getAddressCountries = function(val) {
                                $('[ng-show="noResultsCountry"]').html(val.length > 0 ? '<i class="glyphicon glyphicon-remove"></i> No Results Found' : '');
                                if (val.length > 2) {
                                    return DirectoryCountryService.list({
                                        codeValue: {
                                            _eval: 'regex',
                                            value: val
                                        },
                                        active: true
                                    }).$promise;
                                } else $('[ng-show="noResultsCountry"]').html('Enter minimum 3 characters');
                            };
                            $modalScope.selectAddressCountryTypeAhead = function($item, $model, $label, $event) {
                                $modalScope.newContact.country = $model;
                                $modalScope.newContact.countryName = $model.codeValue;
                            };
                            $modalScope.getAddressCountys = function(val) {
                                $('[ng-show="noResultsCounty"]').html(val.length > 0 ? '<i class="glyphicon glyphicon-remove"></i> No Results Found' : '');
                                if (val.length > 2) {
                                    return DirectoryCountyService.list({
                                        codeValue: {
                                            _eval: 'regex',
                                            value: val
                                        },
                                        active: true
                                    }).$promise;
                                } else $('[ng-show="noResultsCounty"]').html('Enter minimum 3 characters');
                            };
                            $modalScope.selectAddressCountyTypeAhead = function($item, $model, $label, $event) {
                                $modalScope.newContact.county = $model;
                                $modalScope.newContact.countyName = $model.codeValue;
                            };

                            //ocdContact Details
                            $modalScope.newOCDContact = {};
                            $modalScope.newOCDContactIndex = null;
                            $modalScope.editNewOCDContact = function(index) {
                                $modalScope.newOCDContactIndex = index;
                                $modalScope.newOCDContact = getObjectData($modalScope.keyPersonnel.ocdContact[index]);
                            };
                            $modalScope.removeNewOCDContact = function(index) {
                                if ($modalScope.keyPersonnel.ocdContact[index]._id) {
                                    $modalScope.keyPersonnel.ocdContact[index].deleted = true;
                                } else {
                                    $modalScope.keyPersonnel.ocdContact.splice(index, 1);
                                }
                                $modalScope.resetNewOCDContact();
                            };
                            $modalScope.resetNewOCDContact = function() {
                                $modalScope.newOCDContact = {};
                                $modalScope.newOCDContactIndex = null;
                            };
                            $modalScope.saveNewOCDContact = function() {
                                $modalScope.newOCDContactIndex = $modalScope.newOCDContactIndex != null ? $modalScope.newOCDContactIndex : $modalScope.keyPersonnel.ocdContact.length;
                                $modalScope.keyPersonnel.ocdContact[$modalScope.newOCDContactIndex] = $modalScope.newOCDContact;
                                $modalScope.resetNewOCDContact();
                            };
                            // Address Details
                            $modalScope.newContact = {};
                            $modalScope.newContactIndex = null;
                            $modalScope.editNewContact = function(index) {
                                $modalScope.newContactIndex = index;
                                $modalScope.newContact = getObjectData($modalScope.keyPersonnel.address[index]);
                            };
                            $modalScope.removeNewContact = function(index) {
                                if ($modalScope.keyPersonnel.address[index]._id) {
                                    $modalScope.keyPersonnel.address[index].deleted = true;
                                } else {
                                    $modalScope.keyPersonnel.address.splice(index, 1);
                                }
                                $modalScope.resetNewContact();
                            };
                            $modalScope.resetNewContact = function() {
                                $modalScope.newContact = {};
                                $modalScope.newContactIndex = null;
                            };
                            $modalScope.saveNewContact = function() {
                                $modalScope.newContactIndex = $modalScope.newContactIndex != null ? $modalScope.newContactIndex : $modalScope.keyPersonnel.address.length;
                                $modalScope.keyPersonnel.address[$modalScope.newContactIndex] = $modalScope.newContact;
                                $modalScope.resetNewContact();
                            };
                            // Degree Details
                            $modalScope.newDegree = {};
                            $modalScope.newDegreeIndex = null;
                            $modalScope.editNewDegree = function(index) {
                                $modalScope.newDegreeIndex = index;
                                $modalScope.newDegree = getObjectData($modalScope.keyPersonnel.ocdDegree[index]);
                            };
                            $modalScope.removeNewDegree = function(index) {
                                if ($modalScope.keyPersonnel.ocdDegree[index]._id) {
                                    $modalScope.keyPersonnel.ocdDegree[index].deleted = true;
                                } else {
                                    $modalScope.keyPersonnel.ocdDegree.splice(index, 1);
                                }
                                $modalScope.resetNewDegree();
                            };
                            $modalScope.resetNewDegree = function() {
                                $modalScope.newDegree = {};
                                $modalScope.newDegreeIndex = null;
                            };
                            $modalScope.saveNewDegree = function() {
                                $modalScope.newDegreeIndex = $modalScope.newDegreeIndex != null ? $modalScope.newDegreeIndex : $modalScope.keyPersonnel.ocdDegree.length;
                                $modalScope.keyPersonnel.ocdDegree[$modalScope.newDegreeIndex] = $modalScope.newDegree;
                                $modalScope.resetNewDegree();
                            };
                            // Note Details
                            $modalScope.newNote = {};
                            $modalScope.newNoteIndex = null;
                            $modalScope.editNewNote = function(index) {
                                $modalScope.newNoteIndex = index;
                                $modalScope.newNote = getObjectData($modalScope.keyPersonnel.notes[index]);
                            };
                            $modalScope.removeNewNote = function(index) {
                                if ($modalScope.keyPersonnel.notes[index]._id) {
                                    $modalScope.keyPersonnel.notes[index].deleted = true;
                                } else {
                                    $modalScope.keyPersonnel.notes.splice(index, 1);
                                }
                                $modalScope.resetNewNote();
                            };
                            $modalScope.resetNewNote = function() {
                                $modalScope.newNote = {};
                                $modalScope.newNoteIndex = null;
                            };
                            $modalScope.saveNewNote = function() {
                                $modalScope.newNoteIndex = $modalScope.newNoteIndex != null ? $modalScope.newNoteIndex : $modalScope.keyPersonnel.notes.length;
                                $modalScope.keyPersonnel.notes[$modalScope.newNoteIndex] = $modalScope.newNote;
                                $modalScope.resetNewNote();
                            };
                            // Assignment Details
                            $modalScope.assngOrgId = $scope.organization._id;
                            $modalScope.newAssignment = {};
                            $modalScope.newAssignmentIndex = null;
                            $modalScope.editNewAssignment = function(index) {
                                $modalScope.newAssignmentIndex = index;
                                $modalScope.newAssignment = getObjectData($modalScope.keyPersonnel.assignment[index]);
                            };
                            $modalScope.removeNewAssignment = function(index) {
                                if ($modalScope.keyPersonnel.assignment[index]._id) {
                                    $modalScope.keyPersonnel.assignment[index].deleted = true;
                                } else {
                                    $modalScope.keyPersonnel.assignment.splice(index, 1);
                                }
                                $modalScope.resetNewAssignment();
                            };
                            $modalScope.resetNewAssignment = function() {
                                $modalScope.newAssignment = {};
                                $modalScope.newAssignmentIndex = null;
                            };
                            $modalScope.saveNewAssignment = function() {
                                $modalScope.newAssignmentIndex = $modalScope.newAssignmentIndex != null ? $modalScope.newAssignmentIndex : $modalScope.keyPersonnel.assignment.length;
                                $modalScope.keyPersonnel.assignment[$modalScope.newAssignmentIndex] = $modalScope.newAssignment;
                                $modalScope.resetNewAssignment();
                            };
                            //ORg Type For Assignment
                            $modalScope.getOrganizationForAssignment = function(val) {
                                $('[ng-show="noResultsOrganization"]').html(val.length > 0 ? '<i class="glyphicon glyphicon-remove"></i> No Results Found' : '');
                                if (val.length > 2) {
                                    return DirectoryOrganizationService.list({
                                        active: true,
                                        directoryId: $localStorage['DMS-DIRECTORY']._id,
                                        parentId: null,
                                        name: {
                                            _eval: 'regex',
                                            value: val
                                        }
                                    }).$promise;
                                } else $('[ng-show="noResultsOrganization"]').html('Enter minimum 3 characters');
                            };
                            $modalScope.formatOrgTypeAhead = function($model) {
                                return $model ? $model.name : "";
                            };
                            $modalScope.selectOrgTypeAhead = function($item) {
                                $modalScope.newAssignment.orgName = $item.name;
                            };

                            // $modalScope.getOrganizationForTypeAhead = $scope.getOrganizationForTypeAhead;

                        }
                    ]
                });
                modalInstance.result.then(function(op) {
                    if (!$scope.organization.personnel) $scope.organization.personnel = [];
                    $scope[op.action + 'OCDKeyPersonnel'](index, op.data);
                });
            };

            //Key Personnel For CFS 23/08/2016
            $scope.addCFSKeyPersonnel = function(index, personnel) {
                personnel.directoryId = $localStorage['DMS-DIRECTORY']._id;
                $scope.organization.personnel.push(personnel);
            };
            $scope.editCFSKeyPersonnel = function(index, personnel) {
                personnel.directoryId = $localStorage['DMS-DIRECTORY']._id;
                $scope.organization.personnel[index] = personnel;
            };
            $scope.removeCFSKeyPersonnel = function(index) {
                $scope.organization.personnel.splice(index, 1);
            };
            $scope.openCFSKeyPersonnelPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    size: 'lg',
                    windowClass: 'full-window',
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.html',
                    resolve: {
                        ListingType: [
                            'DirectoryListingTypeService',
                            '$localStorage',
                            function(DirectoryListingTypeService, $localStorage) {
                                return DirectoryListingTypeService.get({ directoryId: $localStorage['DMS-DIRECTORY']._id }).$promise;
                            }
                        ],
                        OfficerType: [
                            'DirectoryOfficerTypeService',
                            function(DirectoryOfficerTypeService) {
                                return DirectoryOfficerTypeService.get().$promise;
                            }
                        ],
                    },
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        'DirectoryKeyPersonnelTitleService',
                        'OfficerType',
                        function($modalScope, $state, $uibModalInstance, DirectoryKeyPersonnelTitleService, OfficerType) {
                            $modalScope.getKeyPersonnelTitles = function(val) {
                                $('[ng-show="noResultsTitle"]').html(val.length > 0 ? '<i class="glyphicon glyphicon-remove"></i> No Results Found' : '');
                                if (val.length > 2) {
                                    return DirectoryKeyPersonnelTitleService.list({
                                        directoryId: $localStorage['DMS-DIRECTORY']._id,
                                        codeValue: {
                                            _eval: 'regex',
                                            value: val
                                        }
                                    }).$promise;
                                } else $('[ng-show="noResultsTitle"]').html('Enter minimum 3 characters');
                            };
                            $modalScope.formatKeypersonnelTypeAhead = function($model) {
                                return $model ? $model.codeValue : "";
                            };
                            $modalScope.selectKeypersonnelTypeAhead = function($item) {
                                $modalScope.keyPersonnel.titleMaster = $item;
                                $modalScope.keyPersonnel.titleMasterName = $item.codeName;
                            };
                            $modalScope.listingTypeArray = ListingType;
                            $modalScope.officerTypeArray = OfficerType;
                            $modalScope.editFlag = (typeof index != 'undefined');
                            $modalScope.keyPersonnel = $modalScope.editFlag ? getObjectData($scope.organization.personnel[index]) : {
                                officers: [],
                            };
                            if (!$modalScope.editFlag) $modalScope.keyPersonnel.active = true;
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                            $modalScope.add = function() {
                                $uibModalInstance.close({
                                    action: 'add',
                                    data: $modalScope.keyPersonnel
                                });
                            };
                            $modalScope.update = function() {
                                $uibModalInstance.close({
                                    action: 'edit',
                                    data: $modalScope.keyPersonnel
                                });
                            };
                            $modalScope.personnelIndex = index;
                            $modalScope.openRemovePopup = $scope.openRemovePopup;
                            // Officer Details
                            $modalScope.newOfficer = {};
                            $modalScope.newOfficerIndex = null;
                            $modalScope.editNewCFSOfficer = function(index) {
                                $modalScope.newOfficerIndex = index;
                                $modalScope.newOfficer = getObjectData($modalScope.keyPersonnel.officers[index]);
                            };
                            $modalScope.removeNewCFSOfficer = function(index) {
                                if ($modalScope.keyPersonnel.officers[index]._id) {
                                    $modalScope.keyPersonnel.officers[index].deleted = true;
                                } else {
                                    $modalScope.keyPersonnel.officers.splice(index, 1);
                                }
                                $modalScope.resetNewCFSOfficer();
                            };
                            $modalScope.resetNewCFSOfficer = function() {
                                $modalScope.newOfficer = {};
                                $modalScope.newOfficerIndex = null;
                            };
                            $modalScope.saveNewCFSOfficer = function() {
                                $modalScope.newOfficerIndex = $modalScope.newOfficerIndex != null ? $modalScope.newOfficerIndex : $modalScope.keyPersonnel.officers.length;
                                $modalScope.keyPersonnel.officers[$modalScope.newOfficerIndex] = $modalScope.newOfficer;
                                $modalScope.resetNewCFSOfficer();
                            };

                        }
                    ]
                });
                modalInstance.result.then(function(op) {
                    if (!$scope.organization.personnel) $scope.organization.personnel = [];
                    $scope[op.action + 'CFSKeyPersonnel'](index, op.data);
                });
            };

            //Ker Personnel DMMP 24/08/2016

            $scope.addDMMPKeyPersonnel = function(index, personnel) {
                personnel.directoryId = $localStorage['DMS-DIRECTORY']._id;
                $scope.organization.personnel.push(personnel);
            };
            $scope.editDMMPKeyPersonnel = function(index, personnel) {
                personnel.directoryId = $localStorage['DMS-DIRECTORY']._id;
                $scope.organization.personnel[index] = personnel;
            };
            $scope.removeDMMPKeyPersonnel = function(index) {
                $scope.organization.personnel.splice(index, 1);
            };
            $scope.openDMMPKeyPersonnelPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    size: 'lg',
                    windowClass: 'full-window',
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.html',
                    resolve: {
                        ResponsibilityType: [
                            'DirectoryResponsibilityTypeService',
                            function(DirectoryResponsibilityTypeService) {
                                return DirectoryResponsibilityTypeService.get().$promise;
                            }
                        ],
                    },
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        'DirectoryKeyPersonnelTitleService',
                        'ResponsibilityType',
                        function($modalScope, $state, $uibModalInstance, DirectoryKeyPersonnelTitleService, ResponsibilityType) {
                            $modalScope.getKeyPersonnelTitles = function(val) {
                                $('[ng-show="noResultsTitle"]').html(val.length > 0 ? '<i class="glyphicon glyphicon-remove"></i> No Results Found' : '');
                                if (val.length > 2) {
                                    return DirectoryKeyPersonnelTitleService.list({
                                        directoryId: $localStorage['DMS-DIRECTORY']._id,
                                        codeValue: {
                                            _eval: 'regex',
                                            value: val
                                        }
                                    }).$promise;
                                } else $('[ng-show="noResultsTitle"]').html('Enter minimum 3 characters');
                            };
                            $modalScope.formatKeypersonnelTypeAhead = function($model) {
                                return $model ? $model.codeValue : "";
                            };
                            $modalScope.selectKeypersonnelTypeAhead = function($item) {
                                $modalScope.keyPersonnel.titleMaster = $item;
                                $modalScope.keyPersonnel.titleMasterName = $item.codeName;
                            };
                            $modalScope.responsibilityTypeArray = ResponsibilityType;
                            $modalScope.editFlag = (typeof index != 'undefined');
                            $modalScope.keyPersonnel = $modalScope.editFlag ? getObjectData($scope.organization.personnel[index]) : {
                                responsibilityCodes: [],
                            };
                            if (!$modalScope.editFlag) $modalScope.keyPersonnel.active = true;
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                            $modalScope.add = function() {
                                $uibModalInstance.close({
                                    action: 'add',
                                    data: $modalScope.keyPersonnel
                                });
                            };
                            $modalScope.update = function() {
                                $uibModalInstance.close({
                                    action: 'edit',
                                    data: $modalScope.keyPersonnel
                                });
                            };
                            $modalScope.personnelIndex = index;
                            $modalScope.openRemovePopup = $scope.openRemovePopup;

                            // Responsibility Details
                            $modalScope.newResponsibility = {};
                            $modalScope.newResponsibilityIndex = null;
                            $modalScope.editNewDMMPResponsibility = function(index) {
                                $modalScope.newResponsibilityIndex = index;
                                $modalScope.newResponsibility = getObjectData($modalScope.keyPersonnel.responsibilityCodes[index]);
                            };
                            $modalScope.removeNewDMMPResponsibility = function(index) {
                                if ($modalScope.keyPersonnel.responsibilityCodes[index]._id) {
                                    $modalScope.keyPersonnel.responsibilityCodes[index].deleted = true;
                                } else {
                                    $modalScope.keyPersonnel.responsibilityCodes.splice(index, 1);
                                }
                                $modalScope.resetNewDMMPResponsibility();
                            };
                            $modalScope.resetNewDMMPResponsibility = function() {
                                $modalScope.newResponsibility = {};
                                $modalScope.newResponsibilityIndex = null;
                            };
                            $modalScope.saveNewDMMPResponsibility = function() {
                                $modalScope.newResponsibilityIndex = $modalScope.newResponsibilityIndex != null ? $modalScope.newResponsibilityIndex : $modalScope.keyPersonnel.responsibilityCodes.length;
                                $modalScope.keyPersonnel.responsibilityCodes[$modalScope.newResponsibilityIndex] = $modalScope.newResponsibility;
                                $modalScope.resetNewDMMPResponsibility();
                            };

                        }
                    ]
                });
                modalInstance.result.then(function(op) {
                    if (!$scope.organization.personnel) $scope.organization.personnel = [];
                    $scope[op.action + 'DMMPKeyPersonnel'](index, op.data);
                });
            };

            //----------
            //OCD General Details View
            //----------
            $scope.openOCDKeyPersonnelGeneralDetailsPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    size: 'lg',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.ocdGeneral.view.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            $modalScope.addressTypes = $scope.addressTypes;
                            $modalScope.keyPersonnel = $scope.organization.personnel[index];
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    ]
                });
            };

            //-----------
            //OCD Contact Details View
            //-------------
            $scope.openOCDKeyPersonnelOCDContactDetailsPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    size: 'lg',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.ocdContact.view.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            $modalScope.addressTypes = $scope.addressTypes;
                            $modalScope.keyPersonnel = $scope.organization.personnel[index];
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    ]
                });
            };

            // -------------
            // OCD Address Details View
            // -------------
            $scope.openOCDKeyPersonnelContactDetailsPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    size: 'lg',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.contact.view.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            $modalScope.addressTypes = $scope.addressTypes;
                            $modalScope.keyPersonnel = $scope.organization.personnel[index];
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    ]
                });
            };
            // -------------
            // OCD Degree Details View
            // -------------
            $scope.openOCDKeyPersonnelDegreeDetailsPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    size: 'lg',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.degree.view.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            $modalScope.keyPersonnel = $scope.organization.personnel[index];
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    ]
                });
            };
            // -------------
            // OCD Notes Details View
            // -------------
            $scope.openOCDKeyPersonnelNotesPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    size: 'lg',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.notes.view.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            $modalScope.keyPersonnel = $scope.organization.personnel[index];
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    ]
                });
            };
            // -------------
            // OCD Assignment Details View
            // -------------
            $scope.openOCDKeyPersonnelAssignmentsPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    size: 'lg',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.assignment.view.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            $modalScope.assngOrgId = $scope.organization._id;
                            $modalScope.keyPersonnel = $scope.organization.personnel[index];
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    ]
                });
            };

            // -------------
            // Features
            // -------------
            $scope.addFeatures = function(index, feature) {
                $scope.organization.features.push(feature);
            };
            $scope.editFeatures = function(index, feature) {
                $scope.organization.features[index] = feature;
            };
            $scope.removeFeatures = function(index) {
                $scope.organization.features.splice(index, 1);
            };
            $scope.openFeaturePopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.features.html',
                    resolve: {
                        FeatureTypes: [
                            'DirectoryFeatureTypeService',
                            '$localStorage',
                            function(DirectoryFeatureTypeService, $localStorage) {
                                return DirectoryFeatureTypeService.get({ directoryId: $localStorage['DMS-DIRECTORY']._id }).$promise;
                            }
                        ]
                    },
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        'DirectoryFeatureCodeService',
                        'FeatureTypes',
                        function($modalScope, $state, $uibModalInstance, DirectoryFeatureCodeService, FeatureTypes) {
                            $modalScope.featureTypeArray = FeatureTypes;
                            $modalScope.editFlag = (typeof index != 'undefined');
                            $modalScope.feature = $modalScope.editFlag ? getObjectData($scope.organization.features[index]) : {};
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                            $modalScope.add = function() {
                                $uibModalInstance.close({
                                    action: 'add',
                                    data: $modalScope.feature
                                });
                            };
                            $modalScope.changeFeatureType = function(featureType, dontDelete) {
                                $modalScope.featureCodeArray = [];
                                if (!dontDelete)
                                    delete $modalScope.feature.code;
                                else
                                    featureType = { codeType: featureType };
                                if (featureType && featureType.codeType) {
                                    $modalScope.loading = true;
                                    DirectoryFeatureCodeService.get({ directoryId: $localStorage['DMS-DIRECTORY']._id, codeType: featureType.codeType }, function(featureCodes) {
                                        $modalScope.featureCodeArray = featureCodes;
                                        $modalScope.loading = false;
                                    }, function(err) {
                                        showMessages([{
                                            header: 'Feature Codes',
                                            message: 'Could not fetch Feature Codes',
                                            type: 'error'
                                        }]);
                                        $modalScope.loading = false;
                                    });
                                }
                            };
                            $modalScope.update = function() {
                                $uibModalInstance.close({
                                    action: 'edit',
                                    data: $modalScope.feature
                                });
                            };
                            if ($modalScope.feature.featureType) $modalScope.changeFeatureType($modalScope.feature.featureType, true);
                        }
                    ]
                });
                modalInstance.result.then(function(op) {

                    if (!$scope.organization.features) $scope.organization.features = [];
                    $scope[op.action + 'Features'](index, op.data);
                });

            };

            // -------------
            // Chapter Specifiaction chapterSpecification
            // -------------chapterSpecification
            $scope.addChapterSpecification = function(index, chapterSpecification) {
                chapterSpecification.directoryId = $localStorage['DMS-DIRECTORY']._id;
                $scope.organization.chapterSpecification.push(chapterSpecification);
            };
            $scope.editChapterSpecification = function(index, chapterSpecification) {
                chapterSpecification.directoryId = $localStorage['DMS-DIRECTORY']._id;
                $scope.organization.chapterSpecification[index] = chapterSpecification;
            };
            $scope.removeChapterSpecification = function(index) {
                $scope.organization.chapterSpecification.splice(index, 1);
            };
            $scope.openChapterSpecificationPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.chapterSpecifications.html',
                    resolve: {
                        ListingType: [
                            'DirectoryListingTypeService',
                            '$localStorage',
                            function(DirectoryListingTypeService, $localStorage) {
                                return DirectoryListingTypeService.get({ directoryId: $localStorage['DMS-DIRECTORY']._id }).$promise;
                            }
                        ],
                        SpecificationType: [
                            'DirectorySpecificationTypeService',
                            function(DirectorySpecificationTypeService) {
                                return DirectorySpecificationTypeService.get({ directoryId: $localStorage['DMS-DIRECTORY']._id }).$promise;
                            }
                        ]
                    },
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        'ListingType',
                        'SpecificationType',
                        'DirectorySpecificationCodeService',
                        /*  'DirectorySpecificationTypeService',*/
                        function($modalScope, $state, $uibModalInstance, ListingType, SpecificationType, DirectorySpecificationCodeService) {

                            $modalScope.editFlag = (typeof index != 'undefined');
                            $modalScope.listingTypeArray = ListingType;

                            $modalScope.specificationTypeArray = SpecificationType;
                            $modalScope.chapterSpecification = $modalScope.editFlag ? getObjectData($scope.organization.chapterSpecification[index]) : {};
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                            $modalScope.add = function() {
                                $uibModalInstance.close({
                                    action: 'add',
                                    data: $modalScope.chapterSpecification
                                });
                            };
                            $modalScope.changeSpecificationType = function(specificationType, dontDelete) {
                                $modalScope.specificationCodeArray = [];
                                if (!dontDelete) {
                                    delete $modalScope.chapterSpecification.code;
                                } else if (!specificationType.codeType) specificationType = { codeType: specificationType };
                                if (specificationType) {
                                    $modalScope.loading = true;
                                    DirectorySpecificationCodeService.get({
                                        directoryId: $localStorage['DMS-DIRECTORY']._id,
                                        codeType: specificationType.codeType
                                    }, function(specificationCodes) {
                                        $modalScope.specificationCodeArray = specificationCodes;
                                        $modalScope.loading = false;
                                    }, function(err) {
                                        showMessages([{
                                            header: 'Specification  Codes',
                                            message: 'Could not fetch ChapterSpecification Codes',
                                            type: 'error'
                                        }]);
                                        $modalScope.loading = false;
                                    });
                                }
                            };

                            if ($modalScope.chapterSpecification.specificationType) $modalScope.changeSpecificationType($modalScope.chapterSpecification.specificationType, true);
                            $modalScope.update = function() {
                                $uibModalInstance.close({
                                    action: 'edit',
                                    data: $modalScope.chapterSpecification
                                });
                            };
                        }
                    ]
                });
                modalInstance.result.then(function(op) {

                    if (!$scope.organization.chapterSpecification) $scope.organization.chapterSpecification = [];
                    $scope[op.action + 'ChapterSpecification'](index, op.data);
                });
            };
            // -------------
            // Listing Type
            // -------------
            $scope.addListingType = function(index, listingType) {
                $scope.organization.listingType.push(listingType);
            };
            $scope.editListingType = function(index, listingType) {
                $scope.organization.listingType[index] = listingType;
            };
            $scope.removeListingType = function(index) {
                $scope.organization.listingType.splice(index, 1);
            };
            $scope.openListingTypePopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.listingType.html',
                    resolve: {
                        ListingType: [
                            'DirectoryListingTypeService',
                            '$localStorage',
                            function(DirectoryListingTypeService, $localStorage) {
                                return DirectoryListingTypeService.get({ directoryId: $localStorage['DMS-DIRECTORY']._id }).$promise;
                            }
                        ]
                    },
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        'ListingType',
                        /*  'DirectorySpecificationTypeService',*/
                        function($modalScope, $state, $uibModalInstance, ListingType) {
                            $modalScope.editFlag = (typeof index != 'undefined');
                            $modalScope.listingTypeArray = ListingType;
                            $modalScope.listingType = $modalScope.editFlag ? getObjectData($scope.organization.listingType[index]) : {};
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                            $modalScope.add = function() {
                                $modalScope.listingType.listingName = $modalScope.listingType.listing.codeValue;
                                /*comment becoz propero value bind when add $modalScope.listingType.listing = $modalScope.listingType.listing._id;*/
                                $uibModalInstance.close({
                                    action: 'add',
                                    data: $modalScope.listingType
                                });
                            };
                            /* $modalScope.selectListinc = function($item, $model, $label, $event) {
                             
                                 $modalScope.address.state = $model.parentId;
                                 $modalScope.address.stateName = $model.parentId.codeValue + " (" + $model.parentId.description + ")";
                                 $modalScope.address.country = $model.parentId.parentId;
                                 $modalScope.address.countryName = $model.parentId.parentId.codeValue;
                             };*/
                            // $modalScope.changeSpecificationType = function(specificationType, dontDelete) {
                            //     $modalScope.specificationCodeArray = [];
                            //     if (!dontDelete)
                            //         delete $modalScope.chapterSpecification.code;
                            //     if (specificationType && specificationType.codeType) {
                            //         $modalScope.loading = true;
                            //         DirectorySpecificationCodeService.get({ directoryId: $localStorage['DMS-DIRECTORY']._id,codeType: specificationType.codeType }, function(specificationCodes) {
                            //             $modalScope.specificationCodeArray = specificationCodes;
                            //             $modalScope.loading = false;
                            //         }, function(err) {
                            //             showMessages([{
                            //                 header: 'Specification  Codes',
                            //                 message: 'Could not fetch ChapterSpecification Codes',
                            //                 type: 'error'
                            //             }]);
                            //             $modalScope.loading = false;
                            //         });
                            //     }
                            // };

                            ///



                            $modalScope.update = function() {
                                $modalScope.listingType.listingName = $modalScope.listingType.listing.codeValue;
                                $modalScope.listingType.listing = $modalScope.listingType.listing._id;
                                $uibModalInstance.close({
                                    action: 'edit',
                                    data: $modalScope.listingType
                                });
                            };
                        }
                    ]
                });
                modalInstance.result.then(function(op) {
                    if (!$scope.organization.listingType) $scope.organization.listingType = [];
                    $scope[op.action + 'ListingType'](index, op.data);
                });
            };


            //Listing For DMMP//
            $scope.addListingTypeDMMP = function(index, listingType) {
                $scope.organization.listingType.push(listingType);
            };
            $scope.editListingTypeDMMP = function(index, listingType) {
                $scope.organization.listingType[index] = listingType;
            };
            $scope.removeListingTypeDMMP = function(index) {
                $scope.organization.listingType.splice(index, 1);
            };
            $scope.openListingTypeDMMPPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.listingType.html',
                    resolve: {
                        ListingType: [
                            'DirectoryListingTypeService',
                            '$localStorage',
                            function(DirectoryListingTypeService, $localStorage) {
                                return DirectoryListingTypeService.get({ directoryId: $localStorage['DMS-DIRECTORY']._id }).$promise;
                            }
                        ]
                    },
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        'ListingType',
                        'DirectoryDMMPSectionCodeService',
                        function($modalScope, $state, $uibModalInstance, ListingType, DirectoryDMMPSectionCodeService) {
                            $modalScope.editFlag = (typeof index != 'undefined');
                            $modalScope.listingTypeArray = ListingType;
                            $modalScope.listingType = $modalScope.editFlag ? getObjectData($scope.organization.listingType[index]) : {};
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                            $modalScope.add = function() {
                                $modalScope.listingType.listingName = $modalScope.listingType.listing.codeValue;
                                if ($modalScope.listingType.listingSectionCode) $modalScope.listingType.listingSectionName = $modalScope.listingType.listingSectionCode.codeValue;
                                $uibModalInstance.close({
                                    action: 'add',
                                    data: $modalScope.listingType
                                });
                            };
                            $modalScope.changeSectionType = function(listing, dontDelete) {
                                $modalScope.sectionCodeArray = [];
                                if (!dontDelete) {
                                    delete $modalScope.listingType.listingSectionCode;
                                    delete $modalScope.listingType.listingSectionName;
                                }
                                if (listing) {
                                    $modalScope.loading = true;
                                    DirectoryDMMPSectionCodeService.get({ parentId: listing._id }, function(sectionCodes) {
                                        $modalScope.sectionCodeArray = sectionCodes;
                                        $modalScope.loading = false;
                                    }, function(err) {
                                        showMessages([{
                                            header: 'Section Codes',
                                            message: 'Could not fetch Section Codes',
                                            type: 'error'
                                        }]);
                                        $modalScope.loading = false;
                                    });
                                }
                            };
                            if ($modalScope.listingType.listing) $modalScope.changeSectionType($modalScope.listingType.listing, true);
                            $modalScope.update = function() {
                                $modalScope.listingType.listingName = $modalScope.listingType.listing.codeValue;
                                if ($modalScope.listingType.listingSectionCode) $modalScope.listingType.listingSectionName = $modalScope.listingType.listingSectionCode.codeValue;
                                $uibModalInstance.close({
                                    action: 'edit',
                                    data: $modalScope.listingType
                                });
                            };
                        }
                    ]
                });
                modalInstance.result.then(function(op) {
                    if (!$scope.organization.listingType) $scope.organization.listingType = [];
                    $scope[op.action + 'ListingType'](index, op.data);
                });
            };




            // -------------
            // Publications
            // -------------
            $scope.addPublications = function(index, publication) {
                $scope.organization.publication.push(publication);
            };
            $scope.editPublications = function(index, publication) {
                $scope.organization.publication[index] = publication;
            };
            $scope.removePublications = function(index) {
                $scope.organization.publication.splice(index, 1);
            };
            $scope.openPublicationsPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.publications.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            $modalScope.editFlag = (typeof index != 'undefined');
                            $modalScope.publication = $modalScope.editFlag ? getObjectData($scope.organization.publication[index]) : {};
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                            $modalScope.add = function() {
                                $uibModalInstance.close({
                                    action: 'add',
                                    data: $modalScope.publication
                                });
                            };
                            $modalScope.update = function() {
                                $uibModalInstance.close({
                                    action: 'edit',
                                    data: $modalScope.publication
                                });
                            };
                        }
                    ]
                });
                modalInstance.result.then(function(op) {
                    if (!$scope.organization.publication) $scope.organization.publication = [];
                    $scope[op.action + 'Publications'](index, op.data);
                });
            };

            // -------------
            // Research
            // -------------
            $scope.addResearch = function(index, research) {
                $scope.organization.research.push(research);
            };
            $scope.editResearch = function(index, research) {
                $scope.organization.research[index] = research;
            };
            $scope.removeResearch = function(index) {
                $scope.organization.research.splice(index, 1);
            };
            $scope.openResearchPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.research.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            $modalScope.editFlag = (typeof index != 'undefined');
                            $modalScope.research = $modalScope.editFlag ? getObjectData($scope.organization.research[index]) : "";
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                            $modalScope.add = function() {
                                $uibModalInstance.close({
                                    action: 'add',
                                    data: $modalScope.research
                                });
                            };

                            $modalScope.update = function() {
                                $uibModalInstance.close({
                                    action: 'edit',
                                    data: $modalScope.research
                                });
                            };
                        }
                    ]
                });
                modalInstance.result.then(function(op) {
                    if (!$scope.organization.research) $scope.organization.research = [];
                    $scope[op.action + 'Research'](index, op.data);
                });
            };

            // -------------
            // Exhibitions
            // -------------
            $scope.addExhibition = function(index, exhibition) {
                exhibition.directoryId = $localStorage['DMS-DIRECTORY']._id;
                $scope.organization.exhibition.push(exhibition);
            };
            $scope.editExhibition = function(index, exhibition) {
                exhibition.directoryId = $localStorage['DMS-DIRECTORY']._id;
                $scope.organization.exhibition[index] = exhibition;
            };
            $scope.removeExhibition = function(index) {
                $scope.organization.exhibition.splice(index, 1);
            };
            $scope.openExhibitionPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.exhibition.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            $modalScope.editFlag = (typeof index != 'undefined');
                            $modalScope.exhibition = $modalScope.editFlag ? getObjectData($scope.organization.exhibition[index]) : {};
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                            $modalScope.add = function() {
                                $uibModalInstance.close({
                                    action: 'add',
                                    data: $modalScope.exhibition
                                });
                            };
                            $modalScope.update = function() {
                                $uibModalInstance.close({
                                    action: 'edit',
                                    data: $modalScope.exhibition
                                });
                            };
                            // $modalScope.startDateOptions = {
                            //     minDate: new Date($modalScope.exhibition.startDate || $modalScope.exhibition.startDateText),
                            //     maxDate: new Date($modalScope.exhibition.endDate || $modalScope.exhibition.endDateText)
                            // };
                            // $modalScope.endDateOptions = {
                            //     minDate: new Date($modalScope.exhibition.startDate || $modalScope.exhibition.startDateText),
                            //     maxDate: new Date($modalScope.exhibition.endDate || $modalScope.exhibition.endDateText)
                            // };
                            // $modalScope.$watch('exhibition.endDate', function(newVal) {
                            //     $modalScope.startDateOptions.maxDate = new Date(newVal || '');
                            //     $modalScope.endDateOptions.maxDate = new Date(newVal || '');
                            // });
                            // $modalScope.$watch('exhibition.startDate', function(newVal) {
                            //     $modalScope.startDateOptions.minDate = new Date(newVal || '');
                            //     $modalScope.endDateOptions.minDate = new Date(newVal || '');
                            // });
                            $modalScope.exhibition.startDate = $modalScope.exhibition.startDate ? new Date($modalScope.exhibition.startDate) : "";
                            $modalScope.exhibition.endDate = $modalScope.exhibition.endDate ? new Date($modalScope.exhibition.endDate) : "";
                            $modalScope.startDateOptions = {
                                minDate: new Date($modalScope.exhibition.startDate),
                                maxDate: new Date($modalScope.exhibition.endDate)
                            };
                            $modalScope.endDateOptions = {
                                minDate: new Date($modalScope.exhibition.startDate),
                                maxDate: new Date($modalScope.exhibition.endDate)
                            };
                            $modalScope.$watch('exhibition.endDate', function(newVal) {
                                $modalScope.startDateOptions.maxDate = new Date(newVal || '');
                                $modalScope.endDateOptions.maxDate = new Date(newVal || '');
                                $modalScope.exhibition.endDateText = newVal;
                            });
                            $modalScope.$watch('exhibition.startDate', function(newVal) {
                                $modalScope.startDateOptions.minDate = new Date(newVal || '');
                                $modalScope.endDateOptions.minDate = new Date(newVal || '');
                                $modalScope.exhibition.startDateText = newVal;
                            });
                        }
                    ]
                });
                modalInstance.result.then(function(op) {
                    if (!$scope.organization.exhibition) $scope.organization.exhibition = [];
                    $scope[op.action + 'Exhibition'](index, op.data);
                });
            };

            // -------------
            // Activities
            // -------------
            $scope.addActivities = function(index, activity) {
                $scope.organization.activities.push(activity);
            };
            $scope.editActivities = function(index, activity) {
                $scope.organization.activities[index] = activity;
            };
            $scope.removeActivities = function(index) {
                $scope.organization.activities.splice(index, 1);
            };
            $scope.openActivitiesPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.activities.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            $modalScope.editFlag = (typeof index != 'undefined');
                            $modalScope.activity = $modalScope.editFlag ? getObjectData($scope.organization.activities[index]) : {};
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                            $modalScope.add = function() {
                                $uibModalInstance.close({
                                    action: 'add',
                                    data: $modalScope.activity
                                });
                            };
                            $modalScope.update = function() {
                                $uibModalInstance.close({
                                    action: 'edit',
                                    data: $modalScope.activity
                                });
                            };
                        }
                    ]
                });
                modalInstance.result.then(function(op) {
                    if (!$scope.organization.activities) $scope.organization.activities = [];
                    $scope[op.action + 'Activities'](index, op.data);
                });
            };

            // -------------
            // Notes
            // -------------
            $scope.addNotes = function(index, note) {
                $scope.organization.notes.push(note);
            };
            $scope.editNotes = function(index, note) {
                $scope.organization.notes[index] = note;
            };
            $scope.removeNotes = function(index) {
                $scope.organization.notes.splice(index, 1);
            };
            $scope.openNotesPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.notes.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            $modalScope.editFlag = (typeof index != 'undefined');
                            $modalScope.notes = $modalScope.editFlag ? getObjectData($scope.organization.notes[index]) : {};
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                            $modalScope.add = function() {
                                $uibModalInstance.close({
                                    action: 'add',
                                    data: $modalScope.notes
                                });
                            };
                            $modalScope.update = function() {
                                $uibModalInstance.close({
                                    action: 'edit',
                                    data: $modalScope.notes
                                });
                            };
                        }
                    ]
                });
                modalInstance.result.then(function(op) {
                    if (!$scope.organization.notes) $scope.organization.notes = [];
                    $scope[op.action + 'Notes'](index, op.data);
                });
            };

            //---------Vendors--------------//

            $scope.addVendors = function(index, vendor) {
                $scope.organization.vendors.push(vendor);
            };
            $scope.editVendors = function(index, vendor) {
                $scope.organization.vendors[index] = vendor;
            };
            $scope.removeVendors = function(index) {
                $scope.organization.vendors.splice(index, 1);
            };
            $scope.openVendorsPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.vendors.html',
                    resolve: {
                        VendorType: [
                            'DirectoryVendorTypeService',
                            function(DirectoryVendorTypeService) {
                                return DirectoryVendorTypeService.get().$promise;
                            }
                        ]
                    },
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        'VendorType',
                        'DirectoryStateService',
                        'DirectoryCityService',
                        function($modalScope, $state, $uibModalInstance, VendorType, DirectoryStateService, DirectoryCityService) {
                            $modalScope.editFlag = (typeof index != 'undefined');
                            $modalScope.vendorTypeArray = VendorType;
                            $modalScope.vendors = $modalScope.editFlag ? getObjectData($scope.organization.vendors[index]) : {};
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                            $modalScope.add = function() {
                                $uibModalInstance.close({
                                    action: 'add',
                                    data: $modalScope.vendors
                                });
                            };
                            $modalScope.update = function() {
                                $uibModalInstance.close({
                                    action: 'edit',
                                    data: $modalScope.vendors
                                });
                            };
                            $modalScope.getAddressCitiesVendors = function(val) {
                                $('[ng-show="noResultsCity"]').html(val.length > 0 ? '<i class="glyphicon glyphicon-remove"></i> No Results Found' : '');
                                if (val.length > 2) {
                                    return DirectoryCityService.list({
                                        codeValue: {
                                            _eval: 'regex',
                                            value: val
                                        },
                                        active: true
                                    }).$promise;
                                } else $('[ng-show="noResultsCity"]').html('Enter minimum 3 characters');
                            };
                            $modalScope.selectAddressCityTypeAhead = function($item, $model, $label, $event) {
                                $modalScope.vendors.city = $model;
                                $modalScope.vendors.cityName = $model.codeValue;
                                $modalScope.vendors.state = $model.parentId;
                                $modalScope.vendors.stateName = $model.parentId.codeValue + " (" + $model.parentId.description + ")";

                            };
                            $modalScope.getAddressStatesVendors = function(val) {
                                $('[ng-show="noResultsState"]').html(val.length > 0 ? '<i class="glyphicon glyphicon-remove"></i> No Results Found' : '');
                                if (val.length > 2) {
                                    return DirectoryStateService.list({
                                        codeValue: {
                                            _eval: 'regex',
                                            value: val
                                        },
                                        active: true
                                    }).$promise;
                                } else $('[ng-show="noResultsState"]').html('Enter minimum 3 characters');
                            };
                            $modalScope.selectAddressStateTypeAhead = function($item, $model, $label, $event) {
                                $modalScope.vendors.state = $model;
                                $modalScope.vendors.stateName = $model.codeValue + " (" + $model.description + ")";
                                $modalScope.address.country = $model.parentId;
                                $modalScope.address.countryName = $model.parentId.codeValue;
                            };
                        }
                    ]
                });
                modalInstance.result.then(function(op) {

                    if (!$scope.organization.vendors) $scope.organization.vendors = [];
                    $scope[op.action + 'Vendors'](index, op.data);
                });
            };

            $scope.getOrganizationForTypeAhead = function(val) {
                $('[ng-show="noResultsOrganization"]').html(val.length > 0 ? '<i class="glyphicon glyphicon-remove"></i> No Results Found' : '');
                if (val.length > 2) {
                    return DirectoryOrganizationService.list({
                        active: true,
                        directoryId: $localStorage['DMS-DIRECTORY']._id,
                        parentId: null,
                        name: {
                            _eval: 'regex',
                            value: val
                        }
                    }).$promise;
                } else $('[ng-show="noResultsOrganization"]').html('Enter minimum 3 characters');

            };
            $scope.formatOrgTypeAhead = function($model) {
                return $model ? $model.name : "";
            };
            $scope.selectOrgTypeAhead = function($item) {
                $scope.organization.parentId = $item;
            };


            //Remove null
            // $scope.function sanitizeMethod(organization) {
            //     $scope.organization = organization.data;

            //     for (i = 0; i < formerNames.length; i++) {
            //         if (formerNames[i].name == null) {
            //             $scope.organization.formerNames[i].splice($index, 1);
            //         }
            //     }
            //     return organization;
            //     console.log('hello');
            // }
            $scope.openRemovePopup = function($index, popupName, callback) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/directory.organization.remove.tpl.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            $modalScope.popupName = popupName;
                            $modalScope.index1 = $index;
                            $modalScope.editFlag = (typeof index != 'undefined');
                            $modalScope.removes = {};
                            $modalScope.removeRecord = function() {
                                if (popupName == "FormerName(s)") {
                                    $scope.organization.formerNames.splice($index, 1);
                                } else if (popupName == "Book Edition(s)") {
                                    $scope.organization.bookEdition.splice($index, 1);
                                } else if (popupName == "Address") {
                                    $scope.organization.address.splice($index, 1);
                                } else if (popupName == "Key Personnel") {
                                    $scope.removeKeyPersonnel($index);
                                } else if (popupName == "Key Personnel") {
                                    $scope.removeOCDKeyPersonnel($index);
                                } else if (popupName == "Features") {
                                    $scope.organization.features.splice($index, 1);
                                } else if (popupName == "Exhibition") {
                                    $scope.removeExhibition($index);
                                } else if (popupName == "Research") {
                                    $scope.organization.research.splice($index, 1);
                                } else if (popupName == "Publications") {
                                    $scope.organization.publication.splice($index, 1);
                                } else if (popupName == "Contacts") {
                                    $scope.organization.ocdContact.splice($index, 1);
                                } else if (popupName == "Chapter Specification") {
                                    $scope.removeChapterSpecification($index);
                                } else if (popupName == "Notes") {
                                    $scope.organization.notes.splice($index, 1);
                                } else if (popupName == "Stathist") {
                                    $scope.organization.stathist.splice($index, 1);
                                } else if (popupName == "Direct Marketing Budget Disbursal") {
                                    $scope.organization.directMarketingBudgetDisbursal.splice($index, 1);
                                } else if (popupName == "Vendors") {
                                    $scope.organization.vendors.splice($index, 1);
                                } else if (popupName == "Listing Types") {
                                    $scope.organization.listingType.splice($index, 1);
                                } else if (popupName == "Members") {
                                    $scope.organization.members.splice($index, 1);
                                } else if (popupName == "DMMP Listing Types") {
                                    $scope.organization.listingType.splice($index, 1);
                                } else if (popupName == "Legal Titles") {
                                    $scope.organization.legalTitles.splice($index, 1);
                                } else if (popupName == "Statistics") {
                                    $scope.organization.statistic.splice($index, 1);
                                } else if (popupName == "Cross Reference") {
                                    $scope.organization.crossReference.splice($index, 1);
                                } else if (popupName == "Key Personnel: Contact") {
                                    callback($index);
                                } else if (popupName == "Key Personnel: Address") {
                                    callback($index);
                                } else if (popupName == "Key Personnel: Degree") {
                                    callback($index);
                                } else if (popupName == "Key Personnel: Notes") {
                                    callback($index);
                                } else if (popupName == "Key Personnel: Assignments") {
                                    callback($index);
                                } else if (popupName == "Key Personnel: Officer") {
                                    callback($index);
                                } else if (popupName == "Key Personnel: Responsibility") {
                                    callback($index);
                                }
                                $uibModalInstance.close({
                                    action: 'removeRecord',
                                    data: $modalScope.removes
                                });
                            };
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    ]
                });
            };

            // For OCD (30/06/2016)


            //Legal Titles PopUp
            $scope.addlegalTitle = function(index, legalTitles) {
                $scope.organization.legalTitles.push(legalTitles);
            };
            $scope.editlegalTitle = function(index, legalTitles) {
                $scope.organization.legalTitles[index] = legalTitles;
            };
            $scope.removelegalTitle = function(index) {
                $scope.organization.legalTitles.splice(index, 1);
            };
            $scope.openlegalTitlesPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.legalTitles.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            $modalScope.editFlag = (typeof index != 'undefined');
                            $modalScope.legalTitles = $modalScope.editFlag ? getObjectData($scope.organization.legalTitles[index]) : {};
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                            $modalScope.add = function() {
                                $uibModalInstance.close({
                                    action: 'add',
                                    data: $modalScope.legalTitles
                                });
                            };
                            $modalScope.update = function() {
                                $uibModalInstance.close({
                                    action: 'edit',
                                    data: $modalScope.legalTitles
                                });
                            };
                            $modalScope.getLegalCities = function(val) {
                                $('[ng-show="noResultsCity"]').html(val.length > 0 ? '<i class="glyphicon glyphicon-remove"></i> No Results Found' : '');
                                if (val.length > 2) {
                                    return DirectoryCityService.list({
                                        codeValue: {
                                            _eval: 'regex',
                                            value: val
                                        },
                                        active: true
                                    }).$promise;
                                } else $('[ng-show="noResultsCity"]').html('Enter minimum 3 characters');
                            };
                            $modalScope.selectLegalCityTypeAhead = function($item, $model, $label, $event) {
                                $modalScope.legalTitles.cityName = $model.codeValue;
                                $modalScope.legalTitles.state = $model.parentId;
                                $modalScope.legalTitles.stateName = $model.parentId.codeValue + " (" + $model.parentId.description + ")";

                            };
                            $modalScope.getLegalStates = function(val) {
                                $('[ng-show="noResultsState"]').html(val.length > 0 ? '<i class="glyphicon glyphicon-remove"></i> No Results Found' : '');
                                if (val.length > 2) {
                                    return DirectoryStateService.list({
                                        codeValue: {
                                            _eval: 'regex',
                                            value: val
                                        },
                                        active: true
                                    }).$promise;
                                } else $('[ng-show="noResultsState"]').html('Enter minimum 3 characters');
                            };
                            $modalScope.selectLegalStateTypeAhead = function($item, $model, $label, $event) {
                                $modalScope.legalTitles.stateName = $model.codeValue + " (" + $model.description + ")";
                            };
                        }
                    ]
                });
                modalInstance.result.then(function(op) {
                    if (!$scope.organization.legalTitles) $scope.organization.legalTitles = [];
                    $scope[op.action + 'legalTitle'](index, op.data);
                });
            };
            //Statistics PopUp
            $scope.addStatistic = function(index, statistic) {
                $scope.organization.statistic.push(statistic);
            };
            $scope.editStatistic = function(index, statistic) {
                $scope.organization.statistic[index] = statistic;
            };
            $scope.removeStatistic = function(index) {
                $scope.organization.statistic.splice(index, 1);
            };
            $scope.openStatisticsPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.statistic.html',
                    resolve: {
                        StatisticsType: [
                            'DirectoryStatisticsTypeService',
                            function(DirectoryStatisticsTypeService) {
                                return DirectoryStatisticsTypeService.get().$promise;
                            }
                        ],
                    },
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        'StatisticsType',
                        function($modalScope, $state, $uibModalInstance, StatisticsType) {
                            $modalScope.statisticArray = StatisticsType;
                            $modalScope.editFlag = (typeof index != 'undefined');
                            $modalScope.statistic = $modalScope.editFlag ? getObjectData($scope.organization.statistic[index]) : {};
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                            $modalScope.add = function() {
                                $uibModalInstance.close({
                                    action: 'add',
                                    data: $modalScope.statistic
                                });
                            };
                            $modalScope.update = function() {
                                $uibModalInstance.close({
                                    action: 'edit',
                                    data: $modalScope.statistic
                                });
                            };
                        }
                    ]
                });
                modalInstance.result.then(function(op) {
                    if (!$scope.organization.statistic) $scope.organization.statistic = [];
                    $scope[op.action + 'Statistic'](index, op.data);
                });
            };
            //Cross Reference
            $scope.addCrossReference = function(index, crossReference) {
                $scope.organization.crossReference.push(crossReference);
            };
            $scope.editCrossReference = function(index, crossReference) {
                $scope.organization.crossReference[index] = crossReference;
            };
            $scope.removecrossReference = function(index) {
                $scope.organization.crossReference.splice(index, 1);
            };
            $scope.opencrossReferencesPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.crossReference.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            $modalScope.editFlag = (typeof index != 'undefined');
                            $modalScope.crossReference = $modalScope.editFlag ? getObjectData($scope.organization.crossReference[index]) : {};
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                            $modalScope.add = function() {
                                $uibModalInstance.close({
                                    action: 'add',
                                    data: $modalScope.crossReference
                                });
                            };
                            $modalScope.update = function() {
                                $uibModalInstance.close({
                                    action: 'edit',
                                    data: $modalScope.crossReference
                                });
                            };
                        }
                    ]
                });
                modalInstance.result.then(function(op) {
                    if (!$scope.organization.crossReference) $scope.organization.crossReference = [];
                    $scope[op.action + 'CrossReference'](index, op.data);
                });
            };


            // For OCD Contact
            //OCD Conatct PopUp
            $scope.addContact = function(index, ocdContact) {
                $scope.organization.ocdContact.push(ocdContact);
            };
            $scope.editContact = function(index, ocdContact) {
                $scope.organization.ocdContact[index] = ocdContact;
            };
            $scope.removeContact = function(index) {
                $scope.organization.ocdContact.splice(index, 1);
            };
            $scope.openContactPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    size: 'lg',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.contact.html',
                    resolve: {
                        ContactType: [
                            'DirectoryContactTypeService',
                            function(DirectoryContactTypeService) {
                                return DirectoryContactTypeService.get().$promise;
                            }
                        ]
                    },
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        'ContactType',
                        'DirectoryContactTypeService',
                        function($modalScope, $state, $uibModalInstance, ContactType, DirectoryContactTypeService) {
                            $modalScope.contactTypeArray = ContactType;
                            $modalScope.editFlag = (typeof index != 'undefined');
                            $modalScope.ocdContact = $modalScope.editFlag ? getObjectData($scope.organization.ocdContact[index]) : {};
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                            $modalScope.add = function() {
                                $uibModalInstance.close({
                                    action: 'add',
                                    data: $modalScope.ocdContact
                                });
                            };
                            $modalScope.update = function() {
                                $uibModalInstance.close({
                                    action: 'edit',
                                    data: $modalScope.ocdContact
                                });
                            };
                        }
                    ]
                });
                modalInstance.result.then(function(op) {
                    if (!$scope.organization.ocdContact) $scope.organization.ocdContact = [];
                    $scope[op.action + 'Contact'](index, op.data);
                });
            };
            //Stathist For OMD
            $scope.stathistDtOptions = DTOptionsBuilder.newOptions()
                .withOption("aoColumnDefs", [{ "targets": [2], "orderable": false }]);
            $scope.omdStathist = {};
            $scope.editValue = 0;
            $scope.addStathist = function() {
                $scope.newStathistIndex;
                // $scope.organization.stathist.push($scope.omdStathist);
                if ($scope.editValue == 1) {
                    $scope.organization.stathist[$scope.newStathistIndex] = $scope.omdStathist;
                    $scope.editValue = 0;
                } else {
                    $scope.organization.stathist.push($scope.omdStathist);
                }

                $scope.resetStathist();
            };
            $scope.resetStathist = function() {
                $scope.omdStathist = {};
                $scope.organization.stathist.omdStathist = null;
            };
            // $scope.omdStathist = {};
            // $scope.newStathistIndex = null;
            $scope.editStathist = function(index) {
                $scope.editValue = 1;
                $scope.newStathistIndex = index;
                $scope.omdStathist = getObjectData($scope.organization.stathist[index]);

            };

            //Direct Marketing Budget Disbrusal 22/08/1989
            $scope.addDisbursal = function(index, directMarketingBudgetDisbursal) {
                $scope.organization.directMarketingBudgetDisbursal.push(directMarketingBudgetDisbursal);
            };
            $scope.editDisbursal = function(index, directMarketingBudgetDisbursal) {
                $scope.organization.directMarketingBudgetDisbursal[index] = directMarketingBudgetDisbursal;
            };
            $scope.removeDisbursal = function(index) {
                $scope.organization.directMarketingBudgetDisbursal.splice(index, 1);
            };
            $scope.openDirectMarketingBudgetDisbrusalPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.budgetDisbursal.html',
                    resolve: {
                        DisbursalType: [
                            'DirectoryDisbursalTypeService',
                            function(DirectoryDisbursalTypeService) {
                                return DirectoryDisbursalTypeService.get().$promise;
                            }
                        ],
                    },
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        'DisbursalType',
                        function($modalScope, $state, $uibModalInstance, DisbursalType) {
                            $modalScope.disbursalcArray = DisbursalType;
                            $modalScope.editFlag = (typeof index != 'undefined');
                            $modalScope.directMarketingBudgetDisbursal = $modalScope.editFlag ? getObjectData($scope.organization.directMarketingBudgetDisbursal[index]) : {};
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                            $modalScope.add = function() {
                                $uibModalInstance.close({
                                    action: 'add',
                                    data: $modalScope.directMarketingBudgetDisbursal
                                });
                            };
                            $modalScope.update = function() {
                                $uibModalInstance.close({
                                    action: 'edit',
                                    data: $modalScope.directMarketingBudgetDisbursal
                                });
                            };
                        }
                    ]
                });
                modalInstance.result.then(function(op) {
                    if (!$scope.organization.directMarketingBudgetDisbursal) $scope.organization.directMarketingBudgetDisbursal = [];
                    $scope[op.action + 'Disbursal'](index, op.data);
                });
            };

            //City and state for Mailing List
            $scope.getMailingCities = function(val) {
                $('[ng-show="noResultsCity"]').html(val.length > 0 ? '<i class="glyphicon glyphicon-remove"></i> No Results Found' : '');
                if (val.length > 2) {
                    return DirectoryCityService.list({
                        codeValue: {
                            _eval: 'regex',
                            value: val
                        },
                        active: true
                    }).$promise;
                } else $('[ng-show="noResultsCity"]').html('Enter minimum 3 characters');
            };
            $scope.selectMailingCityTypeAhead = function($item, $model, $label, $event) {
                $scope.organization.mailingList.cityName = $model.codeValue;
                $scope.organization.mailingList.state = $model.parentId;
                $scope.organization.mailingList.stateName = $model.parentId.codeValue + " (" + $model.parentId.description + ")";

            };
            $scope.getMailingStates = function(val) {
                $('[ng-show="noResultsState"]').html(val.length > 0 ? '<i class="glyphicon glyphicon-remove"></i> No Results Found' : '');
                if (val.length > 2) {
                    return DirectoryStateService.list({
                        codeValue: {
                            _eval: 'regex',
                            value: val
                        },
                        active: true
                    }).$promise;
                } else $('[ng-show="noResultsState"]').html('Enter minimum 3 characters');
            };
            // $scope.selectMailingStateTypeAhead = function($item, $model, $label, $event) {
            //     $scope.organization.mailingList.stateName = $model.codeValue + " (" + $model.description + ")";
            // };


            //CFS keyPersonnel General View
            $scope.openCFSKeyPersonnelGeneralDetailsPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    size: 'lg',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.general.view.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            //$modalScope.addressTypes = $scope.addressTypes;
                            $modalScope.keyPersonnel = $scope.organization.personnel[index];
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    ]
                });
            };

            //CFS  keyPersonnel Officer View 
            $scope.openCFSKeyPersonnelOfficersDetailsPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    size: 'lg',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.officer.view.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            //$modalScope.addressTypes = $scope.addressTypes;
                            $modalScope.keyPersonnel = $scope.organization.personnel[index];
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    ]
                });
            };

            //DMMP Key Personnel General Details 
            $scope.openDMMPKeyPersonnelGeneralDetailsPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    size: 'lg',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.general.view.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            //$modalScope.addressTypes = $scope.addressTypes;
                            $modalScope.keyPersonnel = $scope.organization.personnel[index];
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    ]
                });
            };
            //DMMP Key Personnel Responsibility Details
            $scope.openDMMPKeyPersonnelResponsibilityDetailsPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    size: 'lg',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.responsibility.view.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            //$modalScope.addressTypes = $scope.addressTypes;
                            $modalScope.keyPersonnel = $scope.organization.personnel[index];
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    ]
                });
            };

            //AAD Book Edition PopUp 25/08/2016
            $scope.addBookEditions = function(index, bookEditions) {
                $scope.organization.bookEdition.push(bookEditions);
            };
            $scope.editBookEditions = function(index, bookEditions) {
                $scope.organization.bookEdition[index] = bookEditions;
            };
            $scope.removeBookEditions = function(index) {
                $scope.organization.bookEdition.splice(index, 1);
            };
            $scope.openBookEditionPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.bookEdition.html',
                    resolve: {
                        StatusType: [
                            'DirectoryRecordStatusService',
                            function(DirectoryRecordStatusService) {
                                return DirectoryRecordStatusService.get().$promise;
                            }
                        ],

                    },
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        'StatusType',
                        function($modalScope, $state, $uibModalInstance, StatusType) {
                            $modalScope.statusTypeArray = StatusType;
                            $modalScope.editFlag = (typeof index != 'undefined');
                            $modalScope.bookEditions = $modalScope.editFlag ? getObjectData($scope.organization.bookEdition[index]) : {};
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                            $modalScope.add = function() {
                                $uibModalInstance.close({
                                    action: 'add',
                                    data: $modalScope.bookEditions
                                });
                            };
                            $modalScope.update = function() {
                                $uibModalInstance.close({
                                    action: 'edit',
                                    data: $modalScope.bookEditions
                                });
                            };

                        }
                    ]
                });
                modalInstance.result.then(function(op) {

                    if (!$scope.organization.bookEdition) $scope.organization.bookEdition = [];
                    $scope[op.action + 'BookEditions'](index, op.data);
                });

            };
            // -------------
            // Memebers
            // -------------
            $scope.addMembers = function(index, members) {
                $scope.organization.members.push(members);
            };
            $scope.editMembers = function(index, members) {
                $scope.organization.members[index] = members;
            };
            $scope.removeMembers = function(index) {
                $scope.organization.members.splice(index, 1);
            };
            $scope.openMemeberPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.members.html',
                    resolve: {
                        ListingType: [
                            'DirectoryListingTypeService',
                            '$localStorage',
                            function(DirectoryListingTypeService, $localStorage) {
                                return DirectoryListingTypeService.get({ directoryId: $localStorage['DMS-DIRECTORY']._id }).$promise;
                            }
                        ]
                    },
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        'ListingType',
                        function($modalScope, $state, $uibModalInstance, ListingType) {
                            $modalScope.editFlag = (typeof index != 'undefined');
                            $modalScope.listingTypeArray = ListingType;
                            $modalScope.members = $modalScope.editFlag ? getObjectData($scope.organization.members[index]) : {};
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                            $modalScope.add = function() {
                                $modalScope.members.listingName = $modalScope.members.listingType.codeValue;
                                $uibModalInstance.close({
                                    action: 'add',
                                    data: $modalScope.members
                                });
                            };
                            $modalScope.update = function() {
                                $modalScope.members.listingName = $modalScope.members.listingType.codeValue;
                                $uibModalInstance.close({
                                    action: 'edit',
                                    data: $modalScope.members
                                });
                            };
                        }
                    ]
                });
                modalInstance.result.then(function(op) {
                    if (!$scope.organization.members) $scope.organization.members = [];
                    $scope[op.action + 'Members'](index, op.data);
                });
            };

        }
    ]);
