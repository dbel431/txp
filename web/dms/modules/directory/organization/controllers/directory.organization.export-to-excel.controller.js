    angular.module('directory.organization')
        .controller('DirectoryOrganizationExportToExcelController', [
            '$rootScope',
            '$localStorage',
            '$scope',
            '$interval',
            '$compile',
            '$uibModalInstance',
            'DirectoryOrganizationService',
            'DirectoryOrganizationCount',
            'DirectoryDataDownloadService',
            'CoreFileDownloadService',
            function($rootScope, $localStorage, $scope, $interval, $compile, $uibModalInstance, DirectoryOrganizationService, DirectoryOrganizationCount, DirectoryDataDownloadService, CoreFileDownloadService) {
                $scope.cancel = function() {
                    $uibModalInstance.dismiss('cancel');
                    $('.overlay>.centered').remove();
                };
                $scope.currentDirectory = $localStorage['DMS-DIRECTORY'].codeValue;

                $scope.fields = getExportFields();
                $scope.allSelected = false;
                $scope.selectedFields = [];
                $scope.sort = { order: "1" };
                $scope.toggleAll = function() {
                    for (var i = $scope.fields[$scope.currentDirectory].length - 1; i >= 0; i--) {
                        $scope.fields[$scope.currentDirectory][i].checked = $scope.allSelected;
                    }
                    if ($scope.allSelected) {
                        for (var i in $scope.fields[$scope.currentDirectory]) {
                            $scope.selectedFields.push($scope.fields[$scope.currentDirectory][i]);
                        }
                    } else $scope.selectedFields = [];
                };
                $scope.toggleSelection = function toggleSelection(field) {
                    if (field.checked) {
                        if ($scope.selectedFields.indexOf(field) < 0)
                            $scope.selectedFields.push(field);
                        $scope.allSelected = ($scope.selectedFields.length == $scope.fields[$scope.currentDirectory].length);
                    } else {
                        if ($scope.selectedFields.indexOf(field) >= 0)
                            $scope.selectedFields.splice($scope.selectedFields.indexOf(field), 1);
                        $scope.allSelected = false;
                    }
                };
                $scope.excelData = [];
                $scope.excelFields = { srno: "#" };
                $scope.Orgcount = DirectoryOrganizationCount;
                $scope.count = 0;
                $scope.exportToExcel = function() {
                    DirectoryOrganizationService.exportToExcel({
                        select: $scope.selectedFields,
                        where: $rootScope.getExportToExcelWhereClause(),
                        limit: $scope.limit,
                        skip: $scope.skip,
                        sort: $scope.sort,
                    }, function(res) {
                        $scope.stopFetching(true);
                        $('.overlay>.centered>.lead').html('Preparing file for download... This may take some time!');
                        CoreFileDownloadService.get({
                            filename: res.csvFile
                        }, function(res) {
                            downloadFile(API_PATH + '/exports/' + res.filename);
                            $scope.stopFetching();
                        });
                    }, function(err) {
                        console.log(err);
                        showMessages([{
                            header: 'Export to Excel',
                            message: 'Could not fetch Records',
                            type: 'error'
                        }]);
                        $scope.cancel();
                    });
                    // $scope.getOrganizationCount: function(req, res) {
                    //         var query = coreQueryBuilderService.buildQuery(req);
                    //         query.active = true;
                    //         query.deleted = false;
                    //         organizationService.getOrganizationCount(query, function(err, count) {
                    //             // console.log("count aagya " + count);
                    //             if (err) return res.status(400).send(err);
                    //             return res.send({ count: count });
                    //         });
                    //     };

                    setTimeout(function() {
                        $scope.startFetching();
                    }, 500);
                    $scope.counter = 1;
                    $scope.stop = null;
                    $scope.startFetching = function() {
                        $('.overlay>.centered').remove();
                        $('.overlay').append($('<div class="centered"><p class="lead text-center">Please wait...</p></div>'));
                        $scope.stop = $interval(function() {
                            if ($scope.counter <= 100) {
                                var count = $scope.counter;
                                $('.overlay>.centered>.lead').html('Fetching record(s)... ' + count + '%');
                                $scope.counter++;
                            } else if ($scope.counter <= 200) {
                                var count = $scope.counter - 100;
                                $('.overlay>.centered>.lead').html('Proccessing record(s)... ' + count + '%');
                                $scope.counter++;
                            } else {
                                $('.overlay>.centered>.lead').html('Preparing file for download... This may take some time!');
                                $scope.stopFetching();
                            }
                        }, 400);
                    };
                    $scope.stopFetching = function(flag) {
                        if (angular.isDefined($scope.stop)) {
                            $interval.cancel($scope.stop);
                            $scope.stop = undefined;
                        }
                        if (flag) $('.overlay>.centered').remove();
                        setTimeout(function() {
                            $('.overlay>.centered').remove();
                        }, 5000);
                    };
                };

                $scope.sendDataDownloadRequest = function() {
                    var obj = {
                        data: {
                            directoryId: $localStorage['DMS-DIRECTORY']._id,
                            selectedFields: $scope.selectedFields
                        }
                    };
                    DirectoryDataDownloadService.save(obj, function(res) {
                        showMessages([{
                            type: 'success',
                            message: 'A new request has been acknowledged',
                            header: 'Data Download Request'
                        }]);
                        $uibModalInstance.close();
                    }, function(err) {
                        console.log(err);
                        showMessages([{
                            type: 'error',
                            message: 'A new request could not be acknowledged.',
                            header: 'Data Download Request'
                        }]);
                    })
                };
            }
        ]);
