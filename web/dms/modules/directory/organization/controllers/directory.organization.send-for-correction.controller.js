angular.module('directory.organization')
    .controller('DirectoryOrganizationSendForCorrectionController', [
        '$rootScope',
        '$localStorage',
        '$scope',
        '$uibModalInstance',
        'Organizations',
        function($rootScope, $localStorage, $scope, $uibModalInstance, Organizations) {
            $scope.version = {};
            $scope.organizations = Organizations;
            for (var i = $scope.organizations.length - 1; i >= 0; i--) {
                $scope.organizations[i]._oldOrgRecord = getObjectData($scope.organizations[i]);
            }
            $scope.cancel = function() {
                $uibModalInstance.dismiss('cancel');
            };
            var messages = [];
            $scope.sendForCorrection = function() {
                for (var i = $scope.organizations.length - 1; i >= 0; i--) {
                    $scope.organizations[i].currentVersion = $scope.version;
                    $scope.organizations[i].$sendForCorrection(function(org) {
                        messages.push({
                            type: 'success',
                            message: 'Record named: <b>' + org.name + '</b> Send For Correction successfully!',
                            header: 'Record Send For Correction '
                        });
                        syncXhr();
                    }, function(err) {
                        messages.push({
                            type: 'error',
                            message: 'Record named: <b>' + org.name + '</b> could not be Send For Correction successfully!',
                            header: 'Record Send For Correction '
                        });
                        syncXhr();
                    });
                }
            };
            var count = 0;

            function syncXhr() {
                count++;
                if ($scope.loading = (count == $scope.organizations.length))
                    $uibModalInstance.close(messages);
            }
        }
    ]);
