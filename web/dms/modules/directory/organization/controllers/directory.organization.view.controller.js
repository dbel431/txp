angular.module('directory.organization')
    .controller('DirectoryOrganizationViewController', [
        '$rootScope',
        '$localStorage',
        '$scope',
        '$state',
        '$uibModal',
        'Organization',
        'AddressType',
        'DirectoryRecordStatus',
        function($rootScope, $localStorage, $scope, $state, $uibModal, Organization, AddressType, DirectoryRecordStatus) {
            $rootScope.page = {
                name: $localStorage['DMS-DIRECTORY'].description + ': View Record Details',
                actions: {
                    back: {
                        object: 'directory.organization.list',
                        allowed: function() {
                            return $rootScope.isActionPermitted(this.object);
                        }
                    }
                }
            };
            $scope.organization = Organization.data;
            $scope.directoryRecordStatus = [];
            for (var i = DirectoryRecordStatus.length - 1; i >= 0; i--) {
                $scope.directoryRecordStatus[DirectoryRecordStatus[i]._id] = {
                    label: DirectoryRecordStatus[i].codeValue,
                    value: DirectoryRecordStatus[i]._id
                };
            }

            $scope.showPersonnelName = function(personnel) {
                return [(personnel.name.prefix || ""), (personnel.name.first || ""), (personnel.name.middle || ""), (personnel.name.last || ""), (personnel.name.suffix || "")].join(' ');
            };
            $scope.addressTypes = [];
            for (var i = AddressType.length - 1; i >= 0; i--) {
                $scope.addressTypes[AddressType[i]._id] = AddressType[i].codeValue + ' (' + AddressType[i].description + ')';
            }

            //----------
            //OCD General Details View
            //----------
            $scope.openOCDKeyPersonnelGeneralDetailsPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    size: 'lg',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.ocdGeneral.view.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            $modalScope.addressTypes = $scope.addressTypes;
                            $modalScope.keyPersonnel = $scope.organization.personnel[index];
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    ]
                });
            };

            //-----------
            //OCD Contact Details View
            //-------------
            $scope.openOCDKeyPersonnelOCDContactDetailsPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    size: 'lg',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.ocdContact.view.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            $modalScope.addressTypes = $scope.addressTypes;
                            $modalScope.keyPersonnel = $scope.organization.personnel[index];
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    ]
                });
            };

            // -------------
            // Address Details View
            // -------------
            $scope.openOCDKeyPersonnelContactDetailsPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    size: 'lg',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.contact.view.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            $modalScope.addressTypes = $scope.addressTypes;
                            $modalScope.keyPersonnel = $scope.organization.personnel[index];
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    ]
                });
            };
            // -------------
            // OCD Degree Details View
            // -------------
            $scope.openOCDKeyPersonnelDegreeDetailsPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    size: 'lg',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.degree.view.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            $modalScope.keyPersonnel = $scope.organization.personnel[index];
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    ]
                });
            };
            // -------------
            // OCD Notes Details View
            // -------------
            $scope.openOCDKeyPersonnelNotesPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    size: 'lg',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.notes.view.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            $modalScope.keyPersonnel = $scope.organization.personnel[index];
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    ]
                });
            };
            // -------------
            // OCD Assignment Details View
            // -------------
            $scope.openOCDKeyPersonnelAssignmentsPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    size: 'lg',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.assignment.view.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            $modalScope.keyPersonnel = $scope.organization.personnel[index];
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                            $modalScope.assngOrgId = $scope.organization._id;
                        }
                    ]
                });
            };

            //CFS keyPersonnel General View
            $scope.openCFSKeyPersonnelGeneralDetailsPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    size: 'lg',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.general.view.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            //$modalScope.addressTypes = $scope.addressTypes;
                            $modalScope.keyPersonnel = $scope.organization.personnel[index];
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    ]
                });
            };

            //CFS  keyPersonnel Officer View 
            $scope.openCFSKeyPersonnelOfficersDetailsPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    size: 'lg',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.officer.view.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            //$modalScope.addressTypes = $scope.addressTypes;
                            $modalScope.keyPersonnel = $scope.organization.personnel[index];
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    ]
                });
            };

            //DMMP Key Personnel General Details 
            $scope.openDMMPKeyPersonnelGeneralDetailsPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    size: 'lg',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.general.view.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            //$modalScope.addressTypes = $scope.addressTypes;
                            $modalScope.keyPersonnel = $scope.organization.personnel[index];
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    ]
                });
            };
            //DMMP Key Personnel Responsibility Details
            $scope.openDMMPKeyPersonnelResponsibilityDetailsPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    size: 'lg',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.responsibility.view.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            //$modalScope.addressTypes = $scope.addressTypes;
                            $modalScope.keyPersonnel = $scope.organization.personnel[index];
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    ]
                });
            };
        }
    ]);
