angular.module('directory.organization')
    .controller('DirectoryOrganizationApproveController', [
        '$rootScope',
        '$localStorage',
        '$scope',
        '$uibModalInstance',
        'Organizations',
        function($rootScope, $localStorage, $scope, $uibModalInstance, Organizations) {
            $scope.version = {};
            $scope.organizations = Organizations;
            for (var i = $scope.organizations.length - 1; i >= 0; i--) {
                $scope.organizations[i]._oldOrgRecord = getObjectData($scope.organizations[i]);
            }
            $scope.cancel = function() {
                $uibModalInstance.dismiss('cancel');
            };
            var messages = [];
            $scope.approve = function() {
                for (var i = $scope.organizations.length - 1; i >= 0; i--) {
                    $scope.organizations[i].currentVersion = $scope.version;
                    $scope.organizations[i].$approve(function(org) {
                        messages.push({
                            type: 'success',
                            message: 'Record named: <b>' + org.name + '</b> Approved successfully!',
                            header: 'Approve Record'
                        });
                        syncXhr();
                    }, function(err) {
                        messages.push({
                            type: 'error',
                            message: 'Record named: <b>' + org.name + '</b> could not be Approved successfully!',
                            header: 'Approve Record'
                        });
                        syncXhr();
                    });
                }
            };
            var count = 0;

            function syncXhr() {
                count++;
                if ($scope.loading = (count == $scope.organizations.length))
                    $uibModalInstance.close(messages);
            }
        }
    ]);
