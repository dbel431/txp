angular.module('directory')
    .controller('DirectoryOrganizationDiffController', [
        '$rootScope',
        '$localStorage',
        '$scope',
        '$state',
        '$uibModal',
        '$stateParams',
        'Organization',
        'OrganizationVersions',
        'AddressType',
        'VendorType',
        'DirectoryRecordStatus',

        function($rootScope, $localStorage, $scope, $state, $uibModal, $stateParams, Organization, OrganizationVersions, AddressType, VendorType, DirectoryRecordStatus) {

            $rootScope.page = {

                name: $localStorage['DMS-DIRECTORY'].description + ': Record Difference',
                actions: {
                    back: {
                        // object: 'directory.organization.list',
                        object: 'directory.recordHistory.list',
                        allowed: function() {
                            return $rootScope.isActionPermitted(this.object);
                        }
                    }
                }
            };
            $scope.organization = Organization.data;
            $scope.originalOrg = {};
            $scope.versions = [];
            for (var i = 0; i <= (OrganizationVersions.length - 1); i++) {
                if (OrganizationVersions[i].data) $scope.versions.push(OrganizationVersions[i]);
            };
            for (var i = $scope.versions.length - 1; i >= 0; i--) {
                if ($stateParams.versionId == $scope.versions[i]._id) {
                    $scope.originalOrg = $scope.versions[i].data;
                    if (i > 0) $scope.organization = $scope.versions[i - 1].data;
                    $scope.deletedPers = [];
                    var origPersMap = {},
                        origPers = getObjectData($scope.originalOrg.personnel);
                    for (var p in $scope.originalOrg.personnel) {
                        origPersMap[$scope.originalOrg.personnel[p]._id] = p;
                    }
                    var delPersMap = origPersMap;
                    for (var p in $scope.organization.personnel) {
                        if ($scope.originalOrg.personnel && $scope.originalOrg.personnel.length > 0) {
                            $scope.originalOrg.personnel[p] = origPers[origPersMap[$scope.organization.personnel[p]._id]];
                            delete delPersMap[$scope.organization.personnel[p]._id];
                        }
                    }
                    for (var p in delPersMap) $scope.deletedPers.push(origPers[delPersMap[p]]);

                    $scope.deletedExhib = [];
                    var origExhibMap = {},
                        origExhib = getObjectData($scope.originalOrg.exhibition);
                    for (var p in $scope.originalOrg.exhibition) {
                        origExhibMap[$scope.originalOrg.exhibition[p]._id] = p;
                    }
                    var delExhibMap = origExhibMap;
                    for (var p in $scope.organization.exhibition) {
                        if ($scope.originalOrg.exhibition && $scope.originalOrg.exhibition.length > 0) {
                            $scope.originalOrg.exhibition[p] = origExhib[origExhibMap[$scope.organization.exhibition[p]._id]];
                            delete delExhibMap[$scope.organization.exhibition[p]._id];
                        }
                    }
                    for (var p in delExhibMap) $scope.deletedExhib.push(origExhib[delExhibMap[p]]);

                    $scope.deletedChapSpec = [];
                    var origChapSpecMap = {},
                        origChapSpec = getObjectData($scope.originalOrg.chapterSpecification);
                    for (var p in $scope.originalOrg.chapterSpecification) {
                        origChapSpecMap[$scope.originalOrg.chapterSpecification[p]._id] = p;
                    }
                    var delChapSpecMap = origChapSpecMap;
                    for (var p in $scope.organization.chapterSpecification) {
                        if ($scope.originalOrg.chapterSpecification && $scope.originalOrg.chapterSpecification.length > 0) {
                            $scope.originalOrg.chapterSpecification[p] = origChapSpec[origChapSpecMap[$scope.organization.chapterSpecification[p]._id]];
                            delete delChapSpecMap[$scope.organization.chapterSpecification[p]._id];
                        }
                    }
                    for (var p in delChapSpecMap) $scope.deletedChapSpec.push(origChapSpec[delChapSpecMap[p]]);

                    $scope.deletedFormerNames = [];
                    var origFormerNamesMap = {},
                        origFormerNames = getObjectData($scope.originalOrg.formerNames);
                    for (var p in $scope.originalOrg.formerNames) {
                        origFormerNamesMap[$scope.originalOrg.formerNames[p]._id] = p;
                    }
                    var delFormerNamesMap = origFormerNamesMap;
                    for (var p in $scope.organization.formerNames) {
                        if ($scope.originalOrg.formerNames && $scope.originalOrg.formerNames.length > 0) {
                            $scope.originalOrg.formerNames[p] = origFormerNames[origFormerNamesMap[$scope.organization.formerNames[p]._id]];
                            if (!$scope.organization.formerNames[p].deleted) delete delFormerNamesMap[$scope.organization.formerNames[p]._id];
                        }
                    }
                    for (var p in delFormerNamesMap) $scope.deletedFormerNames.push(origFormerNames[delFormerNamesMap[p]]);

                    $scope.deletedListingType = [];
                    var origListingTypeMap = {},
                        origListingType = getObjectData($scope.originalOrg.listingType);
                    for (var p in $scope.originalOrg.listingType) {
                        origListingTypeMap[$scope.originalOrg.listingType[p]._id] = p;
                    }
                    var delListingTypeMap = origListingTypeMap;
                    for (var p in $scope.organization.listingType) {
                        if ($scope.originalOrg.listingType && $scope.originalOrg.listingType.length > 0) {
                            $scope.originalOrg.listingType[p] = origListingType[origListingTypeMap[$scope.organization.listingType[p]._id]];
                            if (!$scope.organization.listingType[p].deleted) delete delListingTypeMap[$scope.organization.listingType[p]._id];
                        }
                    }
                    for (var p in delListingTypeMap) $scope.deletedListingType.push(origListingType[delListingTypeMap[p]]);

                    $scope.deletedAddress = [];
                    var origAddressMap = {},
                        origAddress = getObjectData($scope.originalOrg.address);
                    for (var p in $scope.originalOrg.address) {
                        origAddressMap[$scope.originalOrg.address[p]._id] = p;
                    }
                    var delAddressMap = origAddressMap;
                    for (var p in $scope.organization.address) {
                        if ($scope.originalOrg.address && $scope.originalOrg.address.length > 0) {
                            $scope.originalOrg.address[p] = origAddress[origAddressMap[$scope.organization.address[p]._id]];
                            if (!$scope.organization.address[p].deleted) delete delAddressMap[$scope.organization.address[p]._id];
                        }
                    }
                    for (var p in delAddressMap) $scope.deletedAddress.push(origAddress[delAddressMap[p]]);

                    $scope.deletedFeatures = [];
                    var origFeaturesMap = {},
                        origFeatures = getObjectData($scope.originalOrg.features);
                    for (var p in $scope.originalOrg.features) {
                        origFeaturesMap[$scope.originalOrg.features[p]._id] = p;
                    }
                    var delFeaturesMap = origFeaturesMap;
                    for (var p in $scope.organization.features) {
                        if ($scope.originalOrg.features && $scope.originalOrg.features.length > 0) {
                            $scope.originalOrg.features[p] = origFeatures[origFeaturesMap[$scope.organization.features[p]._id]];
                            if (!$scope.organization.features[p].deleted) delete delFeaturesMap[$scope.organization.features[p]._id];
                        }
                    }
                    for (var p in delFeaturesMap) $scope.deletedFeatures.push(origFeatures[delFeaturesMap[p]]);

                    $scope.deletedResearch = [];
                    var origResearchMap = {},
                        origResearch = getObjectData($scope.originalOrg.research);
                    for (var p in $scope.originalOrg.research) {
                        origResearchMap[$scope.originalOrg.research[p]._id] = p;
                    }
                    var delResearchMap = origResearchMap;
                    for (var p in $scope.organization.research) {
                        if ($scope.originalOrg.research && $scope.originalOrg.research.length > 0) {
                            $scope.originalOrg.research[p] = origResearch[origResearchMap[$scope.organization.research[p]._id]];

                            if (!$scope.organization.research[p].deleted) delete delResearchMap[$scope.organization.research[p]._id];
                        }
                    }
                    for (var p in delResearchMap) $scope.deletedResearch.push(origResearch[delResearchMap[p]]);

                    $scope.deletedPublication = [];
                    var origPublicationMap = {},
                        origPublication = getObjectData($scope.originalOrg.publication);
                    for (var p in $scope.originalOrg.publication) {
                        origPublicationMap[$scope.originalOrg.publication[p]._id] = p;
                    }
                    var delPublicationMap = origPublicationMap;
                    for (var p in $scope.organization.publication) {
                        if ($scope.originalOrg.publication && $scope.originalOrg.publication.length > 0) {
                            $scope.originalOrg.publication[p] = origPublication[origPublicationMap[$scope.organization.publication[p]._id]];

                            if (!$scope.organization.publication[p].deleted) delete delPublicationMap[$scope.organization.publication[p]._id];
                        }
                    }
                    for (var p in delPublicationMap) $scope.deletedPublication.push(origPublication[delPublicationMap[p]]);

                    $scope.deletedNotes = [];
                    var origNotesMap = {},
                        origNotes = getObjectData($scope.originalOrg.notes);
                    for (var p in $scope.originalOrg.notes) {
                        origNotesMap[$scope.originalOrg.notes[p]._id] = p;
                    }
                    var delNotesMap = origNotesMap;
                    for (var p in $scope.organization.notes) {
                        if ($scope.originalOrg.notes && $scope.originalOrg.notes.length > 0) {
                            $scope.originalOrg.notes[p] = origNotes[origNotesMap[$scope.organization.notes[p]._id]];

                            if (!$scope.organization.notes[p].deleted) delete delNotesMap[$scope.organization.notes[p]._id];
                        }
                    }
                    for (var p in delNotesMap) $scope.deletedNotes.push(origNotes[delNotesMap[p]]);

                    $scope.deletedVendors = [];
                    var origVendorsMap = {},
                        origVendors = getObjectData($scope.originalOrg.vendors);
                    for (var p in $scope.originalOrg.vendors) {
                        origVendorsMap[$scope.originalOrg.vendors[p]._id] = p;
                    }
                    var delVendorsMap = origVendorsMap;
                    for (var p in $scope.organization.vendors) {
                        if ($scope.originalOrg.vendors && $scope.originalOrg.vendors.length > 0) {
                            $scope.originalOrg.vendors[p] = origVendors[origVendorsMap[$scope.organization.vendors[p]._id]];

                            if (!$scope.organization.vendors[p].deleted) delete delVendorsMap[$scope.organization.vendors[p]._id];
                        }
                    }
                    for (var p in delVendorsMap) $scope.deletedVendors.push(origVendors[delVendorsMap[p]]);


                    //For OCD Legal Title

                    $scope.deletedLegalTitles = [];
                    var origLegalTitlesMap = {},
                        origLegalTitles = getObjectData($scope.originalOrg.legalTitles);
                    for (var p in $scope.originalOrg.legalTitles) {
                        origLegalTitlesMap[$scope.originalOrg.legalTitles[p]._id] = p;
                    }
                    var delLegalTitlesMap = origLegalTitlesMap;
                    for (var p in $scope.organization.legalTitles) {
                        if ($scope.originalOrg.legalTitles && $scope.originalOrg.legalTitles.length > 0) {
                            $scope.originalOrg.legalTitles[p] = origLegalTitles[origLegalTitlesMap[$scope.organization.legalTitles[p]._id]];

                            if (!$scope.organization.legalTitles[p].deleted) delete delLegalTitlesMap[$scope.organization.legalTitles[p]._id];
                        }
                    }
                    for (var p in delLegalTitlesMap) $scope.deletedLegalTitles.push(origLegalTitles[delLegalTitlesMap[p]]);

                    //For OCD Statistics
                    $scope.deletedStatistics = [];
                    var origStatisticsMap = {},
                        origStatistics = getObjectData($scope.originalOrg.statistic);
                    for (var p in $scope.originalOrg.statistic) {
                        origStatisticsMap[$scope.originalOrg.statistic[p]._id] = p;
                    }
                    var delStatisticsMap = origStatisticsMap;
                    for (var p in $scope.organization.statistic) {
                        if ($scope.originalOrg.statistic && $scope.originalOrg.statistic.length > 0) {
                            $scope.originalOrg.statistic[p] = origStatistics[origStatisticsMap[$scope.organization.statistic[p]._id]];
                            if (!$scope.organization.statistic[p].deleted) delete delStatisticsMap[$scope.organization.statistic[p]._id];
                        }
                    }
                    for (var p in delStatisticsMap) $scope.deletedStatistics.push(origStatistics[delStatisticsMap[p]]);

                    //for Direct marketing budget disbrusal
                    $scope.deletedDisbursalType = [];
                    var origDisbursalMap = {},
                        origDisbursal = getObjectData($scope.originalOrg.directMarketingBudgetDisbursal);
                    for (var p in $scope.originalOrg.directMarketingBudgetDisbursal) {
                        origDisbursalMap[$scope.originalOrg.directMarketingBudgetDisbursal[p]._id] = p;
                    }
                    var delDisbursalMap = origDisbursalMap;
                    for (var p in $scope.organization.directMarketingBudgetDisbursal) {
                        if ($scope.originalOrg.directMarketingBudgetDisbursal && $scope.originalOrg.directMarketingBudgetDisbursal.length > 0) {
                            $scope.originalOrg.directMarketingBudgetDisbursal[p] = origDisbursal[origDisbursalMap[$scope.organization.directMarketingBudgetDisbursal[p]._id]];
                            if (!$scope.organization.directMarketingBudgetDisbursal[p].deleted) delete delDisbursalMap[$scope.organization.directMarketingBudgetDisbursal[p]._id];
                        }
                    }
                    for (var p in delDisbursalMap) $scope.deletedDisbursalType.push(origDisbursal[delDisbursalMap[p]]);

                    //For AAD Book Edition
                    $scope.deletedBookEdition = [];
                    var origBookEditionMap = {},
                        origBookEdition = getObjectData($scope.originalOrg.bookEdition);
                    for (var p in $scope.originalOrg.bookEdition) {
                        origBookEditionMap[$scope.originalOrg.bookEdition[p]._id] = p;
                    }
                    var delBookEditionMap = origBookEditionMap;
                    for (var p in $scope.organization.bookEdition) {
                        if ($scope.originalOrg.bookEdition && $scope.originalOrg.bookEdition.length > 0) {
                            $scope.originalOrg.bookEdition[p] = origBookEdition[origBookEditionMap[$scope.organization.bookEdition[p]._id]];
                            if (!$scope.organization.bookEdition[p].deleted) delete delBookEditionMap[$scope.organization.bookEdition[p]._id];
                        }
                    }
                    for (var p in delBookEditionMap) $scope.deletedBookEdition.push(origBookEdition[delBookEditionMap[p]]);


                    //For OCD Contact
                    $scope.deletedContact = [];
                    var origContactMap = {},
                        origContacts = getObjectData($scope.originalOrg.ocdContact);
                    for (var p in $scope.originalOrg.ocdContact) {
                        origContactMap[$scope.originalOrg.ocdContact[p]._id] = p;
                    }
                    var delContactsMap = origContactMap;
                    for (var p in $scope.organization.ocdContact) {
                        if ($scope.originalOrg.ocdContact && $scope.originalOrg.ocdContact.length > 0) {
                            $scope.originalOrg.ocdContact[p] = origContacts[origContactMap[$scope.organization.ocdContact[p]._id]];
                            if (!$scope.organization.ocdContact[p].deleted) delete delContactsMap[$scope.organization.ocdContact[p]._id];
                        }
                    }
                    for (var p in delContactsMap) $scope.deletedContact.push(origContacts[delContactsMap[p]]);



                    $scope.indexPos = i;
                    break;
                }
            }
            $scope.directoryRecordStatus = [];
            for (var i = DirectoryRecordStatus.length - 1; i >= 0; i--) {
                $scope.directoryRecordStatus[DirectoryRecordStatus[i]._id] = {
                    label: DirectoryRecordStatus[i].codeValue,
                    value: DirectoryRecordStatus[i]._id
                };
            }

            $scope.showPersonnelName = function(personnel) {
                return personnel ? [(personnel.name.prefix || ""), (personnel.name.first || ""), (personnel.name.middle || ""), (personnel.name.last || ""), (personnel.name.suffix || "")].join(' ') : "";
            };

            $scope.addressTypes = [];
            for (var i = AddressType.length - 1; i >= 0; i--) {
                $scope.addressTypes[AddressType[i]._id] = AddressType[i].codeValue + ' (' + AddressType[i].description + ')';
            }

            //-------------
            //OCD General diff
            //-----------
            $scope.openOCDKeyPersonnelGeneralDiffPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    size: 'lg',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.ocdGeneral.diff.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            $modalScope.addressTypes = $scope.addressTypes;
                            $modalScope.originalPers = $scope.originalOrg.personnel[index] || {};
                            $modalScope.keyPersonnel = $scope.organization.personnel[index] || {};
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                            $modalScope.showPersonnelName = function(personnel) {
                                return personnel && personnel.name ? [(personnel.name.prefix || ""), (personnel.name.first || ""), (personnel.name.middle || ""), (personnel.name.last || ""), (personnel.name.suffix || "")].join(' ') : "";
                            };

                        }
                    ]
                });
            };

            // -------------
            // OCD Contact Diff
            // -------------
            $scope.openOCDKeyPersonnelOCDContactDiffPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    size: 'lg',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.ocdContact.diff.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            $modalScope.addressTypes = $scope.addressTypes;
                            $modalScope.originalPers = $scope.originalOrg.personnel[index] || {};
                            $modalScope.keyPersonnel = $scope.organization.personnel[index] || {};
                            if (!$modalScope.originalPers.ocdContact) $modalScope.originalPers.ocdContact = [];
                            $modalScope.deletedContacts = [];
                            var origContactsMap = {},
                                origContacts = getObjectData($modalScope.originalPers.ocdContact);
                            for (var p in $modalScope.originalPers.ocdContact) {
                                origContactsMap[$modalScope.originalPers.ocdContact[p]._id] = p;
                            }
                            var delContact = origContactsMap;
                            for (var p in $modalScope.keyPersonnel.ocdContact) {
                                if ($modalScope.originalPers.ocdContact && $modalScope.originalPers.ocdContact.length > 0) {
                                    $modalScope.originalPers.ocdContact[p] = origContacts[origContactsMap[$modalScope.keyPersonnel.ocdContact[p]._id]];

                                    if (!$modalScope.keyPersonnel.ocdContact[p].deleted) delete delContact[$modalScope.keyPersonnel.ocdContact[p]._id];
                                }
                            }
                            for (var p in delContact) $modalScope.deletedContacts.push(origContacts[delContact[p]]);
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    ]
                });
            };





            // -------------
            // OCD Address Diff
            // -------------
            $scope.openOCDKeyPersonnelContactDiffPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    size: 'lg',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.contact.diff.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            $modalScope.addressTypes = $scope.addressTypes;
                            $modalScope.originalPers = $scope.originalOrg.personnel[index] || {};
                            $modalScope.keyPersonnel = $scope.organization.personnel[index] || {};
                            if (!$modalScope.originalPers.address) $modalScope.originalPers.address = [];
                            $modalScope.deletedContacts = [];
                            var origContactsMap = {},
                                origContacts = getObjectData($modalScope.originalPers.address);
                            for (var p in $modalScope.originalPers.address) {
                                origContactsMap[$modalScope.originalPers.address[p]._id] = p;
                            }
                            var delContact = origContactsMap;
                            for (var p in $modalScope.keyPersonnel.address) {
                                if ($modalScope.originalPers.address && $modalScope.originalPers.address.length > 0) {
                                    $modalScope.originalPers.address[p] = origContacts[origContactsMap[$modalScope.keyPersonnel.address[p]._id]];
                                    if (!$modalScope.keyPersonnel.address[p].deleted) delete delContact[$modalScope.keyPersonnel.address[p]._id];
                                }
                            }
                            for (var p in delContact) $modalScope.deletedContacts.push(origContacts[delContact[p]]);
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    ]
                });
            };
            // -------------
            // OCD Degree Diff
            // -------------
            $scope.openOCDKeyPersonnelDegreeDiffPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    size: 'lg',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.degree.diff.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            $modalScope.originalPers = $scope.originalOrg.personnel[index] || {};
                            $modalScope.keyPersonnel = $scope.organization.personnel[index] || {};
                            if (!$modalScope.originalPers.ocdDegree) $modalScope.originalPers.ocdDegree = [];
                            $modalScope.deletedDegree = [];
                            var origDegreeMap = {},
                                origdegree = getObjectData($modalScope.originalPers.ocdDegree);
                            for (var p in $modalScope.originalPers.ocdDegree) {
                                origDegreeMap[$modalScope.originalPers.ocdDegree[p]._id] = p;
                            }
                            var delDegree = origDegreeMap;
                            for (var p in $modalScope.keyPersonnel.ocdDegree) {
                                if ($modalScope.originalPers.ocdDegree && $modalScope.originalPers.ocdDegree.length > 0) {
                                    $modalScope.originalPers.ocdDegree[p] = origdegree[origDegreeMap[$modalScope.keyPersonnel.ocdDegree[p]._id]];

                                    if (!$modalScope.keyPersonnel.ocdDegree[p].deleted) delete delDegree[$modalScope.keyPersonnel.ocdDegree[p]._id];
                                }
                            }
                            for (var p in delDegree) $modalScope.deletedDegree.push(origdegree[delDegree[p]]);
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    ]
                });
            };

            // -------------
            // OCD Note Diff
            // -------------
            $scope.openOCDKeyPersonnelNotesDiffPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    size: 'lg',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.note.diff.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            $modalScope.originalPers = $scope.originalOrg.personnel[index] || {};
                            $modalScope.keyPersonnel = $scope.organization.personnel[index] || {};
                            if (!$modalScope.originalPers.notes) $modalScope.originalPers.notes = [];
                            $modalScope.deletedNotes = [];
                            var origNoteMap = {},
                                origNote = getObjectData($modalScope.originalPers.notes);
                            for (var p in $modalScope.originalPers.notes) {
                                origNoteMap[$modalScope.originalPers.notes[p]._id] = p;
                            }
                            var delNote = origNoteMap;
                            for (var p in $modalScope.keyPersonnel.notes) {
                                if ($modalScope.originalPers.notes && $modalScope.originalPers.notes.length > 0) {
                                    $modalScope.originalPers.notes[p] = origNote[origNoteMap[$modalScope.keyPersonnel.notes[p]._id]];
                                    if (!$modalScope.keyPersonnel.notes[p].deleted) delete delNote[$modalScope.keyPersonnel.notes[p]._id];
                                }
                            }
                            for (var p in delNote) $modalScope.deletedNotes.push(origNote[delNote[p]]);
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    ]
                });
            };

            // -------------
            // OCD Assignment Diff
            // -------------
            $scope.openOCDKeyPersonnelAssignmentsDiffPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    size: 'lg',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.assignment.diff.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            $modalScope.assngOrgId = $scope.organization._id;
                            $modalScope.originalPers = $scope.originalOrg.personnel[index] || {};
                            $modalScope.keyPersonnel = $scope.organization.personnel[index] || {};
                            if (!$modalScope.originalPers.assignment) $modalScope.originalPers.assignment = [];
                            $modalScope.deletedAssignment = [];
                            var origAssignmentMap = {},
                                origAssignment = getObjectData($modalScope.originalPers.assignment);
                            for (var p in $modalScope.originalPers.assignment) {
                                origAssignmentMap[$modalScope.originalPers.assignment[p]._id] = p;
                            }
                            var delAssignment = origAssignmentMap;
                            for (var p in $modalScope.keyPersonnel.assignment) {
                                if ($modalScope.originalPers.assignment && $modalScope.originalPers.assignment.length > 0) {
                                    $modalScope.originalPers.assignment[p] = origAssignment[origAssignmentMap[$modalScope.keyPersonnel.assignment[p]._id]];
                                    if (!$modalScope.keyPersonnel.assignment[p].deleted) delete delAssignment[$modalScope.keyPersonnel.assignment[p]._id];
                                }
                            }
                            for (var p in delAssignment) $modalScope.deletedAssignment.push(origAssignment[delAssignment[p]]);
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    ]
                });
            };

            //CFS General Details Diff
            $scope.openCFSKeyPersonnelGeneralDiffPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    size: 'lg',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.general.diff.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            $modalScope.originalPers = $scope.originalOrg.personnel[index] || {};
                            $modalScope.keyPersonnel = $scope.organization.personnel[index] || {};
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                            $modalScope.showPersonnelName = function(personnel) {
                                return personnel && personnel.name ? [(personnel.name.prefix || ""), (personnel.name.first || ""), (personnel.name.middle || ""), (personnel.name.last || ""), (personnel.name.suffix || "")].join(' ') : "";
                            };

                        }
                    ]
                });
            };

            //CFS Key Personnel Officers diff openCFSKeyPersonnelOfficersDiffPopup
            $scope.openCFSKeyPersonnelOfficersDiffPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    size: 'lg',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.officer.diff.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            $modalScope.originalPers = $scope.originalOrg.personnel[index] || {};
                            $modalScope.keyPersonnel = $scope.organization.personnel[index] || {};
                            if (!$modalScope.originalPers.officers) $modalScope.originalPers.officers = [];
                            $modalScope.deletedOfficers = [];
                            var origOfficerMap = {},
                                origOfficer = getObjectData($modalScope.originalPers.officers);
                            for (var p in $modalScope.originalPers.officers) {
                                origOfficerMap[$modalScope.originalPers.officers[p]._id] = p;
                            }
                            var delOfficer = origOfficerMap;
                            for (var p in $modalScope.keyPersonnel.officers) {
                                if ($modalScope.originalPers.officers && $modalScope.originalPers.officers.length > 0) {
                                    $modalScope.originalPers.officers[p] = origOfficer[origOfficerMap[$modalScope.keyPersonnel.officers[p]._id]];
                                    if (!$modalScope.keyPersonnel.officers[p].deleted) delete delOfficer[$modalScope.keyPersonnel.officers[p]._id];
                                }
                            }
                            for (var p in delOfficer) $modalScope.deletedOfficers.push(origOfficer[delOfficer[p]]);
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };

                        }
                    ]
                });
            };

            //DMMP Key Personnel General Diff
            $scope.openDMMPKeyPersonnelGeneralDiffPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    size: 'lg',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.general.diff.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            $modalScope.originalPers = $scope.originalOrg.personnel[index] || {};
                            $modalScope.keyPersonnel = $scope.organization.personnel[index] || {};
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                            $modalScope.showPersonnelName = function(personnel) {
                                return personnel && personnel.name ? [(personnel.name.prefix || ""), (personnel.name.first || ""), (personnel.name.middle || ""), (personnel.name.last || ""), (personnel.name.suffix || "")].join(' ') : "";
                            };

                        }
                    ]
                });
            };

            //DMMP Key Personnel Responsibility

            $scope.openDMMPKeyPersonnelResponsibilityDiffPopup = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    size: 'lg',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.popup.keyPersonnel.responsibility.diff.html',
                    controller: [
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        function($modalScope, $state, $uibModalInstance) {
                            $modalScope.originalPers = $scope.originalOrg.personnel[index] || {};
                            $modalScope.keyPersonnel = $scope.organization.personnel[index] || {};
                            // if (!$modalScope.originalPers.responsibilityCodes) $modalScope.originalPers.responsibilityCodes = [];
                            // $modalScope.deletedResponsibility = [];
                            // var origResponsibilityMap = {},
                            //     origResponsibility = getObjectData($modalScope.originalPers.responsibilityCodes);
                            // for (var p in $modalScope.originalPers.responsibilityCodes) {
                            //     origResponsibilityMap[$modalScope.originalPers.responsibilityCodes[p]._id] = p;
                            // }
                            // var delResponsibility = origResponsibilityMap;
                            // for (var p in $modalScope.keyPersonnel.responsibilityCodes) {
                            //     if ($modalScope.originalPers.responsibilityCodes && $modalScope.originalPers.responsibilityCodes.length > 0) {
                            //         $modalScope.originalPers.responsibilityCodes[p] = origResponsibility[origResponsibilityMap[$modalScope.keyPersonnel.responsibilityCodes[p]._id]];
                            //         if (!$modalScope.keyPersonnel.responsibilityCodes[p].deleted) delete delResponsibility[$modalScope.keyPersonnel.responsibilityCodes[p]._id];
                            //     }
                            // }
                            // for (var p in delResponsibility) $modalScope.deletedResponsibility.push(origResponsibility[delResponsibility[p]]);
                            if (!$modalScope.originalPers.responsibilityCodes) $modalScope.originalPers.responsibilityCodes = [];
                            $modalScope.deletedResponsibility = [];
                            var origResponsibilityMap = {},
                                origResponsibility = getObjectData($modalScope.originalPers.responsibilityCodes);
                            for (var p in $modalScope.originalPers.responsibilityCodes) {
                                origResponsibilityMap[$modalScope.originalPers.responsibilityCodes[p]._id] = p;
                            }
                            var delResponsibility = origResponsibilityMap;
                            for (var p in $modalScope.keyPersonnel.responsibilityCodes) {
                                if ($modalScope.originalPers.responsibilityCodes && $modalScope.originalPers.responsibilityCodes.length > 0) {
                                    $modalScope.originalPers.responsibilityCodes[p] = origResponsibility[origResponsibilityMap[$modalScope.keyPersonnel.responsibilityCodes[p]._id]];
                                    if (!$modalScope.keyPersonnel.responsibilityCodes[p].deleted) delete delResponsibility[$modalScope.keyPersonnel.responsibilityCodes[p]._id];
                                }
                            }
                            for (var p in delResponsibility) $modalScope.deletedResponsibility.push(origResponsibility[delResponsibility[p]]);
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };

                        }
                    ]
                });
            };



        }
    ]);
