angular.module('directory.organization')
    .controller('DirectoryOrganizationRemoveController', [
        '$rootScope',
        '$localStorage',
        '$scope',
        '$uibModalInstance',
        'Organizations',
        function($rootScope, $localStorage, $scope, $uibModalInstance, Organizations) {
            $scope.organizations = Organizations;

            $scope.cancel = function() {
                $uibModalInstance.dismiss('cancel');
            };
            var messages = [];
            $scope.remove = function() {


                for (var i = $scope.organizations.length - 1; i >= 0; i--) {


                    $scope.organizations[i].$remove({
                        id: $scope.organizations[i]._id
                    }, function(org) {
                        messages.push({
                            type: 'success',
                            message: 'Record named: <b>' + org.name + '</b> deleted successfully!',
                            header: 'Remove Record'
                        });
                        syncXhr();
                    }, function(err) {
                        messages.push({
                            type: 'error',
                            message: 'Record named: <b>' + org.name + '</b> could not be deleted successfully!',
                            header: 'Remove Record'
                        });
                        syncXhr();
                    });
                }
            };
            var count = 0;

            function syncXhr() {
                count++;
                if ($scope.loading = (count == $scope.organizations.length)) $uibModalInstance.close(messages);
            }
        }
    ]);
