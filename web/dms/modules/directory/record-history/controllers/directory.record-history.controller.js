angular.module('directory.recordHistory')
    .controller('DirectoryRecordHistoryController', [
        '$rootScope',
        '$localStorage',
        function($rootScope, $localStorage) {
            if (!($localStorage['DMS-DIRECTORY'])) $localStorage['DMS-DIRECTORY'] = false;
            $rootScope.page = {
                name: $localStorage['DMS-DIRECTORY'].description + ': Record History'
            };
        }
    ]);
