angular.module('directory.recordHistory')
    .controller('DirectoryRecordHistoryListController', [
        '$rootScope',
        '$localStorage',
        '$scope',
        '$state',
        '$compile',
        '$uibModal',
        'DTOptionsBuilder',
        'DTColumnBuilder',
        'DirectoryOrganizationService',
        'DirectoryRecordVersion',
        'DirectoryOrganizationVersionService',
        'AuthToken',
        'AddressType',
        'OrganizationType',
        function($rootScope, $localStorage, $scope, $state, $compile, $uibModal, DTOptionsBuilder, DTColumnBuilder, DirectoryOrganizationService, DirectoryRecordVersion, DirectoryOrganizationVersionService, AuthToken, AddressType, OrganizationType) {

            $scope.directoryRecordVersion = [];
            for (var i = DirectoryRecordVersion.length - 1; i >= 0; i--) {
                $scope.directoryRecordVersion[DirectoryRecordVersion[i]._id] = {
                    label: DirectoryRecordVersion[i].description,
                    value: DirectoryRecordVersion[i]._id,
                    codeValue: DirectoryRecordVersion[i].codeValue
                };
            }
            $scope.isActionPermitted = $rootScope.isActionPermitted;

            var addressTypesMap = {};
            for (var i = 0; i < AddressType.length; i++) {
                addressTypesMap[AddressType[i]._id] = [AddressType[i].codeValue, AddressType[i].description].join(' ');
            }
            var organizationTypesMap = {},
                organizationTypesDDLValues = [];
            for (var i = 0; i < OrganizationType.length; i++) {
                organizationTypesMap[OrganizationType[i]._id] = OrganizationType[i].codeValue;
                organizationTypesDDLValues.push({
                    label: OrganizationType[i].codeValue,
                    value: OrganizationType[i]._id
                });
            }


            // *****************
            // BUILD Column Defs
            // *****************

            var columnDefs = [{
                title: 'Organization ID',
                width: '2%',
                name: 'orgIdNumber',
                data: 'orgIdNumber',
                render: function(orgIdNumber) {
                    return (isNaN(parseFloat(orgIdNumber)) ? "" : parseFloat(orgIdNumber)) || "";
                }
            }, {
                title: 'Name',
                width: '20%',
                name: 'name',
                data: 'name',
                render: function(name) {
                    return name || "";
                }
            }];

            if ((/AAD/i).test($localStorage['DMS-DIRECTORY'].codeValue) || (/OMD/i).test($localStorage['DMS-DIRECTORY'].codeValue))
                columnDefs.push({
                    title: 'Minor Name',
                    width: '9%',
                    name: 'shortName',
                    data: 'shortName',
                    render: function(shortName) {
                        return shortName || "";
                    }
                });
            if ((/AAD/i).test($localStorage['DMS-DIRECTORY'].codeValue))
                columnDefs.push({
                    title: 'Sub Number',
                    width: '9%',
                    name: 'sub_nbr',
                    data: 'sub_nbr',
                    render: function(sub_nbr) {
                        return sub_nbr || "";
                    }
                });


            if ((/OCD/i).test($localStorage['DMS-DIRECTORY'].codeValue))
                columnDefs.push({
                    title: 'Classification Code',
                    width: '9%',
                    name: 'classificationCode',
                    data: 'classificationCode',
                    render: function(classificationCode) {
                        return organizationTypesMap[classificationCode] || "";
                    }
                });

            columnDefs.push({
                title: 'Address Street 1',
                width: '15%',
                name: 'street1',
                data: 'address.street1',
                render: function(city, type, row) {
                    var phyAddr = getPhysicalAddress(addressTypesMap, row.address);
                    return (phyAddr ? (phyAddr.street1 || "") : "") || "";
                }
            });
            columnDefs.push({
                title: 'City',
                width: '10%',
                name: 'city',
                data: 'address.cityName',
                render: function(city, type, row) {
                    var phyAddr = getPhysicalAddress(addressTypesMap, row.address);
                    return (phyAddr ? (phyAddr.cityName || "") : "") || "";
                }
            });
            columnDefs.push({
                title: 'State/Territory',
                width: '10%',
                name: 'state',
                data: 'address.stateName',
                render: function(city, type, row) {
                    var phyAddr = getPhysicalAddress(addressTypesMap, row.address);
                    return (phyAddr ? (phyAddr.stateName || "") : "") || "";
                }
            });
            columnDefs.push({
                title: 'Migration Status',
                width: '20%',
                orderable: false,
                data: 'versionId',
                render: function(versionId) {
                    return versionId ? $scope.directoryRecordVersion[versionId].label : "";
                }
            });
            columnDefs.push({
                title: 'Actions',
                class: 'text-center',
                width: '9%',
                orderable: false,
                data: 'versionId',
                render: function(versionId, type, row) {
                    return '<directory-record-history-record-actions record-id="\'' + row._id + '\'" record-version-id="\'' + (versionId ? $scope.directoryRecordVersion[versionId].codeValue : '1') + '\'" open-version-history-modal="openVersionHistoryModal" show-version-history="showVersionHistory"></directory-record-history-record-actions>';
                }
            });
            columnDefs.push({
                title: 'Org. ID',
                visible: false,
                width: '10%',
                name: 'org_id',
                data: 'org_id',
                render: function(org_id) {
                    return org_id || "";
                }
            });
            // *****************

            // *****************
            // BUILD Filter Defs
            // *****************

            var filterDefs = {
                '0': { type: 'text', time: 600 },
                '1': { type: 'text', time: 600 },
                '2': { type: 'text', time: 600 },
                '3': { type: 'text', time: 600 },
                '4': { type: 'text', time: 600 },
                '5': { type: 'select', values: (arrayValues($scope.directoryRecordVersion)).reverse() },
                '7': null
            };

            if ((/AAD/i).test($localStorage['DMS-DIRECTORY'].codeValue))
                var filterDefs = {
                    '0': { type: 'text', time: 600 },
                    '1': { type: 'text', time: 600 },
                    '2': { type: 'text', time: 600 },
                    '3': { type: 'text', time: 600 },
                    '4': { type: 'text', time: 600 },
                    '5': { type: 'text', time: 600 },
                    '6': { type: 'text', time: 600 },
                    '7': { type: 'select', values: (arrayValues($scope.directoryRecordVersion)).reverse() },
                    '9': null
                };
            if ((/OMD/i).test($localStorage['DMS-DIRECTORY'].codeValue))
                var filterDefs = {
                    '0': { type: 'text', time: 600 },
                    '1': { type: 'text', time: 600 },
                    '2': { type: 'text', time: 600 },
                    '3': { type: 'text', time: 600 },
                    '4': { type: 'text', time: 600 },
                    '5': { type: 'text', time: 600 },
                    '6': { type: 'select', values: (arrayValues($scope.directoryRecordVersion)).reverse() },
                    '8': null
                };

            if ((/OCD/i).test($localStorage['DMS-DIRECTORY'].codeValue))
                var filterDefs = {
                    '0': { type: 'text', time: 600 },
                    '1': { type: 'text', time: 600 },
                    '2': { type: 'select', values: organizationTypesDDLValues },
                    '3': { type: 'text', time: 600 },
                    '4': { type: 'text', time: 600 },
                    '5': { type: 'text', time: 600 },
                    '6': { type: 'select', values: (arrayValues($scope.directoryRecordVersion)).reverse() },
                    '8': null
                };
            // *****************

            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withPaginationType('full_numbers')
                // .withOption('scrollY', '500px')
                // .withOption('scrollCollapse', true)
                // .withOption('lengthMenu', [100, 500, 1000, 2000, 3000, 5000, 10000, "All"])
                .withOption('order', [0, 'asc'])
                .withOption('stateSave', true)
                // .withOption('orderFixed', [0, 'asc'])
                .withOption('ajax', DirectoryOrganizationService.dataTable)
                .withDataProp('data')
                .withOption('oLanguage', {
                    "sEmptyTable": "No matching records found"
                })
                .withOption('responsive', true)
                // .withOption('sDom', '<fl>rt<"clear"i><"F"p>')
                .withOption('processing', true)
                .withOption('serverSide', true)
                // .withFixedHeader({
                //   header: true
                // })
                .withOption('drawCallback', function(settings) {
                    var api = this.api(),
                        state = api.state(),
                        filterRow = angular.element(api.table().header()).children()[1];
                    $(filterRow).children().each(function(index, child) {
                        if ($(child).has('.form-control'))
                            $(child).children('.form-control').val(state.columns[index].search.search);
                    });
                    $(window).scrollTop(0); // $('body').scrollTop(0);
                    $('[datatable]').css('width', '100%');
                    $compile('directory-record-history-record-actions')($scope);
                    $('[datatable] thead>tr>th:nth-child(1)').css('width', '150px');
                    $('[datatable] thead>tr>th:last-child').css('width', '120px');
                })
                .withOption('aoColumns', columnDefs)
                .withLightColumnFilter(filterDefs);

            $scope.showVersionHistory = function(recordId) {
                DirectoryOrganizationVersionService.get({
                    organization: recordId
                }, function(versions) {
                    var recordVersions = [];
                    for (var i = 0; i <= (versions.length - 1); i++) {
                        if (versions[i].data) recordVersions.push(versions[i]);
                    }
                    if (recordVersions && recordVersions.length > 0) {
                        $state.go('portal.directory.recordHistory.diff', {
                            id: recordId,
                            versionId: recordVersions[0]._id
                        }, { reload: true });
                    } else {
                        showMessages([{
                            type: 'error',
                            message: 'There is no history for this record!',
                            header: 'Record History'
                        }]);
                    }
                }, function(err) {
                    console.log(err);
                    showMessages([{
                        type: 'error',
                        message: 'Failed to retrive history for this record!',
                        header: 'Record History'
                    }]);
                });
            };
            $scope.openVersionHistoryModal = function(recordId) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    size: 'lg',
                    templateUrl: '/views/directory.organization.version-history.tpl.html',
                    resolve: {
                        VersionHistory: [
                            'DirectoryOrganizationVersionService',
                            function(DirectoryOrganizationVersionService) {
                                return DirectoryOrganizationVersionService.get({
                                    organization: recordId
                                }).$promise;
                            }
                        ]
                    },
                    controller: [
                        '$rootScope',
                        '$localStorage',
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        'VersionHistory',
                        function($rootScope, $localStorage, $modalScope, $state, $uibModalInstance, VersionHistory) {
                            $modalScope.versions = VersionHistory;
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    ]
                });
            };
        }
    ]);
