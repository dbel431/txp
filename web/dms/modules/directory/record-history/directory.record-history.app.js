angular.module('directory.recordHistory', [
        'directory.organization'
    ])
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $stateProvider.state('portal.directory.recordHistory', {
                    url: '/record-history',
                    templateUrl: '/views/directory.record-history.tpl.html',
                    controller: 'DirectoryRecordHistoryController'
                }).state('portal.directory.recordHistory.list', {
                    url: '/list',
                    templateUrl: '/views/directory.record-history.list.html',
                    resolve: {
                        "AddressType": [
                            'DirectoryAddressTypeService',
                            function(DirectoryAddressTypeService) {
                                return DirectoryAddressTypeService.get().$promise;
                            }
                        ],
                        OrganizationType: [
                            'DirectoryOrganizationTypeService',
                            function(DirectoryOrganizationTypeService) {
                                return DirectoryOrganizationTypeService.get().$promise;
                            }
                        ],
                        DirectoryRecordVersion: [
                            'DirectoryRecordVersionService',
                            function(DirectoryRecordVersionService) {
                                return DirectoryRecordVersionService.get().$promise;
                            }
                        ]
                    },
                    controller: 'DirectoryRecordHistoryListController'
                })
                .state('portal.directory.recordHistory.diff', {
                    url: '/diff/:id/:versionId',
                    templateUrl: '/views/directory.record-history.diff.tpl.html',
                    resolve: {
                        'Organization': [
                            '$stateParams',
                            'DirectoryOrganizationService',
                            function($stateParams, DirectoryOrganizationService) {
                                return DirectoryOrganizationService.get({
                                    id: $stateParams.id
                                }).$promise;
                            }
                        ],
                        'OrganizationVersions': [
                            '$stateParams',
                            'DirectoryOrganizationVersionService',
                            function($stateParams, DirectoryOrganizationVersionService) {
                                return DirectoryOrganizationVersionService.get({
                                    organization: $stateParams.id
                                }).$promise;
                            }
                        ],
                        "AddressType": [
                            'DirectoryAddressTypeService',
                            function(DirectoryAddressTypeService) {
                                return DirectoryAddressTypeService.get().$promise;
                            }
                        ],
                        VendorType: [
                            'DirectoryVendorTypeService',
                            function(DirectoryVendorTypeService) {
                                return DirectoryVendorTypeService.get().$promise;
                            }
                        ],
                        DirectoryRecordStatus: [
                            'DirectoryRecordStatusService',
                            function(DirectoryRecordStatusService) {
                                return DirectoryRecordStatusService.get().$promise;
                            }
                        ]
                    },
                    controller: 'DirectoryOrganizationDiffController',
                }).state('portal.directory.recordHistory.diff.general', {
                    url: '/general',
                    templateProvider: [
                        '$rootScope',
                        '$localStorage',
                        '$templateFactory',
                        function($rootScope, $localStorage, $templateFactory) {
                            var url = '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.partial.general.diff.html';
                            return $templateFactory.fromUrl(url);
                        }
                    ]
                }).state('portal.directory.recordHistory.diff.communication', {
                    url: '/communication',
                    templateProvider: [
                        '$rootScope',
                        '$localStorage',
                        '$templateFactory',
                        function($rootScope, $localStorage, $templateFactory) {
                            var url = '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.partial.communication.diff.html';
                            return $templateFactory.fromUrl(url);
                        }
                    ]
                }).state('portal.directory.recordHistory.diff.personnel', {
                    url: '/personnel',
                    templateProvider: [
                        '$rootScope',
                        '$localStorage',
                        '$templateFactory',
                        function($rootScope, $localStorage, $templateFactory) {
                            var url = '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.partial.personnel.diff.html';
                            return $templateFactory.fromUrl(url);
                        }
                    ]
                }).state('portal.directory.recordHistory.diff.other', {
                    url: '/other',
                    templateProvider: [
                        '$rootScope',
                        '$localStorage',
                        '$templateFactory',
                        function($rootScope, $localStorage, $templateFactory) {
                            var url = '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.partial.other.diff.html';
                            return $templateFactory.fromUrl(url);
                        }
                    ]
                });
        }
    ])
    .run(['$rootScope',
        '$localStorage', '$state',
        function($rootScope, $localStorage, $state) {
            $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
                var state = toState.name.split('.');
                if (state.indexOf('recordHistory') == (state.length - 2) && state.indexOf('diff') == (state.length - 1)) {
                    event.preventDefault();
                    $state.go('portal.directory.recordHistory.diff.general');
                }
            });
        }
    ]);
