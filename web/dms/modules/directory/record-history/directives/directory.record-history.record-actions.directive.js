angular.module('directory').directive('directoryRecordHistoryRecordActions', [
    '$rootScope',
    '$localStorage',
    function($rootScope, $localStorage) {
        return {
            scope: {
                'recordId': '=',
                'recordVersionId': '=',
                'showVersionHistory': '=',
                'openVersionHistoryModal': '=',
            },
            restrict: 'EA',
            templateUrl: '/views/directory.record-history.record-actions.tpl.html',
            replace: true,
            link: function(scope, elt, attb) {
                scope.isActionPermitted = $rootScope.isActionPermitted;
            }
        };
    }
]);
