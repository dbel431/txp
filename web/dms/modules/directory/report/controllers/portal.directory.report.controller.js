angular.module('directory.report')
    .controller('DirectoryReportController', [
        '$rootScope',
        '$localStorage',
        '$scope',
        '$state',
        'DirectorySummary',
        'DirectoryOrganization',
        'DirectoryOrganizationOCD',
        'SummaryReportCount',
        function($rootScope, $localStorage, $scope, $state, DirectorySummary, DirectoryOrganization, DirectoryOrganizationOCD, SummaryReportCount) {

            $scope.dirID = $localStorage['DMS-DIRECTORY']._id;
            $scope.count = DirectorySummary;
            $scope.exhibitionCount = DirectoryOrganization;
            $scope.personnelCount = DirectoryOrganization;
            $scope.activityCount = DirectoryOrganization;
            $scope.facilityCount = DirectoryOrganization;
            $scope.collectionCount = DirectoryOrganization;
            $scope.organizationCount = DirectoryOrganization;
            $scope.organizationCountOCD = DirectoryOrganizationOCD;
            $scope.isOCD = (/OCD/i).test($localStorage['DMS-DIRECTORY'].codeValue);
            $scope.sourceCount = SummaryReportCount;
            // console.log($scope.facilityCount.length);
            // console.log($scope.facilityCount.facilities.length);
            $rootScope.page = {
                name: $rootScope.page.name + ": Summary Report"
            };
            var regexps = {
                'FACILITY': /^FACILITY$/i,
                'COLLECTION': /^COLLECTION$/i,
                'CATEGORY': /^CATEGORY$/i,
                'CODED SUBJECTS': /^CODED SUBJECTS$/i,
                'COURSES OFFERED': /^COURSES OFFERED$/i,
                'LIBRARY_HOLDING': /^LIBRARY_HOLDING$/i,
                'UNCODED_SUBJECT': /^UNCODED_SUBJECT$/i,
                'service offered': /^service offered$/i,
                'Industry preference': /^Industry preference$/i,
                'Geographical preferences': /^Geographical preferences$/i,
                'financing preferred': /^financing preferred$/i,
                'Fee structure or method of compensation': /^Fee structure or method of compensation$/i,
            };
            // recordType:"Facilities", directoryId:["57189b3e24d8bc65f4123bbf"]
            // console.log(SummaryReportCount);
            if ($scope.facilityCount.facilities)
                for (var i = $scope.facilityCount.facilities.length - 1; i >= 0; i--) {
                    if (regexps['FACILITY'].test($scope.facilityCount.facilities[i]._id)) {
                        angular.forEach(SummaryReportCount, function(key) {
                            if (key.recordType == 'Facilities' && key.directoryId[0] == $localStorage['DMS-DIRECTORY']._id) {
                                key.targetCount = $scope.facilityCount.facilities[i].count
                            }
                        });
                    }
                    if (regexps['COLLECTION'].test($scope.facilityCount.facilities[i]._id)) {
                        angular.forEach(SummaryReportCount, function(key) {
                            if (key.recordType == 'Collections' && key.directoryId[0] == $localStorage['DMS-DIRECTORY']._id) {
                                key.targetCount = $scope.facilityCount.facilities[i].count
                            }
                        });
                    }
                    if (regexps['CATEGORY'].test($scope.facilityCount.facilities[i]._id)) {
                        angular.forEach(SummaryReportCount, function(key) {

                            if (key.recordType == 'Categories' && key.directoryId[0] == $localStorage['DMS-DIRECTORY']._id) {

                                key.targetCount = $scope.facilityCount.facilities[i].count
                            }

                        });
                    }
                    if (regexps['CODED SUBJECTS'].test($scope.facilityCount.facilities[i]._id)) {
                        angular.forEach(SummaryReportCount, function(key) {

                            if (key.recordType == 'Coded Subjects' && key.directoryId[0] == $localStorage['DMS-DIRECTORY']._id) {

                                key.targetCount = $scope.facilityCount.facilities[i].count
                            }

                        });
                    }
                    if (regexps['COURSES OFFERED'].test($scope.facilityCount.facilities[i]._id)) {
                        angular.forEach(SummaryReportCount, function(key) {

                            if (key.recordType == 'Courses Offered' && key.directoryId[0] == $localStorage['DMS-DIRECTORY']._id) {

                                key.targetCount = $scope.facilityCount.facilities[i].count
                            }

                        });
                    }
                    if (regexps['LIBRARY_HOLDING'].test($scope.facilityCount.facilities[i]._id)) {
                        angular.forEach(SummaryReportCount, function(key) {

                            if (key.recordType == 'Library Holdings' && key.directoryId[0] == $localStorage['DMS-DIRECTORY']._id) {

                                key.targetCount = $scope.facilityCount.facilities[i].count
                            }

                        });
                    }
                    if (regexps['UNCODED_SUBJECT'].test($scope.facilityCount.facilities[i]._id)) {
                        angular.forEach(SummaryReportCount, function(key) {

                            if (key.recordType == 'Uncoded Subjects' && key.directoryId[0] == $localStorage['DMS-DIRECTORY']._id) {

                                key.targetCount = $scope.facilityCount.facilities[i].count
                            }

                        });
                    }
                }

            if ($scope.collectionCount.collection)
                for (var i = $scope.collectionCount.collection.length - 1; i >= 0; i--) {

                    if (regexps['service offered'].test($scope.collectionCount.collection[i]._id)) {
                        // console.log($scope.collectionCount.collection[i]._id);
                        angular.forEach(SummaryReportCount, function(key) {

                            if (key.recordType == 'Services Offered' && key.directoryId[0] == $localStorage['DMS-DIRECTORY']._id) {

                                key.targetCount = $scope.collectionCount.collection[i].count
                            }

                        });
                    }
                    if (regexps['Industry preference'].test($scope.collectionCount.collection[i]._id)) {

                        angular.forEach(SummaryReportCount, function(key) {

                            if (key.recordType == 'Industry Preferences' && key.directoryId[0] == $localStorage['DMS-DIRECTORY']._id) {

                                key.targetCount = $scope.collectionCount.collection[i].count
                            }

                        });
                    }
                    if (regexps['Geographical preferences'].test($scope.collectionCount.collection[i]._id)) {
                        angular.forEach(SummaryReportCount, function(key) {

                            if (key.recordType == 'Geographical Preferences' && key.directoryId[0] == $localStorage['DMS-DIRECTORY']._id) {

                                key.targetCount = $scope.collectionCount.collection[i].count
                            }

                        });
                    }
                    if (regexps['financing preferred'].test($scope.collectionCount.collection[i]._id)) {
                        angular.forEach(SummaryReportCount, function(key) {

                            if (key.recordType == 'Financing Preferred' && key.directoryId[0] == $localStorage['DMS-DIRECTORY']._id) {

                                key.targetCount = $scope.collectionCount.collection[i].count
                            }

                        });
                    }
                    if (regexps['Fee structure or method of compensation'].test($scope.collectionCount.collection[i]._id)) {
                        angular.forEach(SummaryReportCount, function(key) {

                            if (key.recordType == 'Fee Structure' && key.directoryId[0] == $localStorage['DMS-DIRECTORY']._id) {

                                key.targetCount = $scope.collectionCount.collection[i].count
                            }

                        });
                    }



                }

        }

    ]);
