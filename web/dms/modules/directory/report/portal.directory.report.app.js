angular.module('directory.report', [
        'directory'
    ])
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $stateProvider.state('portal.directory.report', {
                url: '/report',
                templateUrl: '/views/directory.report.tpl.html',
            }).state('portal.directory.report.migrationSummary', {
                url: '/migration-summary',
                templateUrl: '/views/directory.report.migration-summary.html',
                resolve: {
                    DirectorySummary: [
                        'DirectorySummaryService',
                        function(DirectorySummaryService) {
                            return DirectorySummaryService.get().$promise;
                        }
                    ],
                    DirectoryOrganization: [
                        '$rootScope',
                        '$localStorage',
                        'DirectoryOrganizationService',
                        function($rootScope, $localStorage, DirectoryOrganizationService) {
                            return DirectoryOrganizationService.getExhibitionCount({
                                directoryId: $localStorage['DMS-DIRECTORY']._id
                            }).$promise;
                        }
                    ],
                    DirectoryOrganizationOCD: [
                        '$rootScope',
                        '$localStorage',
                        'DirectoryOrganizationService',
                        function($rootScope, $localStorage, DirectoryOrganizationService) {
                            return DirectoryOrganizationService.getOrganizationCountOCD({
                                directoryId: $localStorage['DMS-DIRECTORY']._id
                            }).$promise;
                        }
                    ],
                    SummaryReportCount: [
                        '$rootScope',
                        '$localStorage',
                        'DirectoryOrganizationService',
                        function($rootScope, $localStorage, DirectoryOrganizationService) {
                            return DirectoryOrganizationService.getSummaryReportCount({
                                directoryId: $localStorage['DMS-DIRECTORY']._id
                            }).$promise;
                        }
                    ]
                },
                controller: 'DirectoryReportController'
            });

        }
    ]);
