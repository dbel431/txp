angular.module('directory').service('DirectorySummaryService', [
    '$rootScope',
    '$localStorage',
    '$resource',
    function($rootScope, $localStorage, $resource) {
        return $resource(API_PATH + '/directory/count', {});
    }
    // function($rootScope, $localStorage, $resource) {
    //     return $resource(API_PATH + '/organization/count', {} );
    // }
]);
