angular.module('directory', [
        'portal'
    ])
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $stateProvider.state('portal.directory', {
                url: '/directory',
                templateUrl: '/views/core.portal.page.tpl.html',
                controller: [
                    '$rootScope',
                    '$localStorage',
                    function($rootScope, $localStorage) {
                        if (!($localStorage['DMS-DIRECTORY'])) $localStorage['DMS-DIRECTORY'] = false;
                        $rootScope.page = {
                            name: $localStorage['DMS-DIRECTORY'].description
                        };
                    }

                ]
            });
        }
    ]).run([
        '$rootScope',
        '$localStorage',
        function($rootScope, $localStorage) {
            $rootScope.getExportToExcelWhereClause = function() {
                return {
                    directoryId: $localStorage['DMS-DIRECTORY']._id,
                    active: true
                };
            }
        }
    ]);

function getPhysicalAddress(addressTypesMap, addresses) {
    if (addresses && addresses.length > 0) {
        for (var i = 0; i < addresses.length; i++) {
            if (addressTypesMap[addresses[i].addressType] && addressTypesMap[addresses[i].addressType].search(/phy/i) > 0 && !addresses[i].deleted)
                return addresses[i];
        }
        for (var i = 0; i < addresses.length; i++) {
            if (addressTypesMap[addresses[i].addressType] && addressTypesMap[addresses[i].addressType].search(/mail/i) > 0 && !addresses[i].deleted)
                return addresses[i];
        }
        return addresses[0].deleted ? {} : addresses[0];
    }
    return {};
}

function getOCDClassificationCodeMap() {
    return {
        'US Diocese': [
            'dioceseType', 'dioProvince', 'dioceseTypeName', 'dioProvinceName'
        ],
        'World Diocese': [
            'dioceseType', 'dioProvince', 'dioceseTypeName', 'dioProvinceName'
        ],
        'Religious Order/Miscellaneous': [
            'religiousOrder'
        ],
        'Religious Order Organization': [
            'religiousOrder'
        ],
        'School': [
            'school'
        ],
        'High School': [
            'school'
        ],
        'Catechesis Religious Program': [
            'school'
        ],
        'College': [
            'school'
        ],
        'Seminary': [
            'school'
        ],
        'Child Care': [
            'school'
        ],
        'Parish': [
            'parishShrine'
        ],
        'Shrine': [
            'parishShrine'
        ],
        'Oratory': [
            'parishShrine'
        ],
        'Mission': [
            'parishShrine'
        ],
        'Station': [
            'parishShrine'
        ],
        'Chapel': [
            'parishShrine'
        ],
        'Header': [
            'header', 'sequenceNo', 'EINumber', 'placementCity', 'purpose', 'crossRefId', 'mailerType', 'sortMajorName', 'abbrevationName', 'latinName', 'squareMiles', 'supplementType', 'akaDbaIndex', 'akaDbaName', 'status', 'established', 'outstandingIssues', 'locale', 'flags', 'formerNames', 'organizationOffice', 'ocdContact', 'address', 'personnel', 'sales', 'legalTitles', 'features', 'notes', 'statistic'
        ],
        'Header w/ assignments only': [
            'header', 'sequenceNo', 'EINumber', 'placementCity', 'purpose', 'crossRefId', 'mailerType', 'sortMajorName', 'abbrevationName', 'latinName', 'squareMiles', 'supplementType', 'akaDbaIndex', 'akaDbaName', 'status', 'established', 'outstandingIssues', 'locale', 'flags', 'formerNames', 'organizationOffice', 'ocdContact', 'address', 'personnel', 'sales', 'legalTitles', 'features', 'notes', 'statistic'
        ],
        'Placement Header': [
            'header', 'sequenceNo', 'EINumber', 'placementCity', 'purpose', 'crossRefId', 'mailerType', 'sortMajorName', 'abbrevationName', 'latinName', 'squareMiles', 'supplementType', 'akaDbaIndex', 'akaDbaName', 'status', 'established', 'outstandingIssues', 'locale', 'flags', 'formerNames', 'organizationOffice', 'ocdContact', 'address', 'personnel', 'sales', 'legalTitles', 'features', 'notes', 'statistic'
        ],
    };
}

var OCDClassificationCodeCache = {
    value: null,
    data: {},
    cacheCurrentValues: function(organization) {
        this.value = this.value || (organization.classificationCode ? organization.classificationCode.codeValue : null);
        this.populate(organization);
    },
    populate: function(organization) {
        var map = getOCDClassificationCodeMap(),
            obj = this;
        if (map[obj.value]) {
            angular.forEach(map[obj.value], function(value, key) {
                if (!obj.data[obj.value]) obj.data[obj.value] = {};
                obj.data[obj.value][value] = organization[value];
            });
            obj.value = (organization.classificationCode ? organization.classificationCode.codeValue : null);
        }
    }
};

function clearOCDDataAsPerClassificationCode(organization) {
    var map = getOCDClassificationCodeMap();
    OCDClassificationCodeCache.cacheCurrentValues(organization);
    for (i in map) {
        if (i != organization.classificationCode.codeValue)
            angular.forEach(map[i], function(value, key) {
                organization[value] = null;
            });
    }
    angular.forEach(map[organization.classificationCode.codeValue], function(value, key) {
        organization[value] = OCDClassificationCodeCache.data[organization.classificationCode.codeValue] ? OCDClassificationCodeCache.data[organization.classificationCode.codeValue][value] : null;
    });
    return organization;
}

function getExportFields() {
    return {
        'OMD': [
            { label: 'Organization ID', base: 'organization', ngModel: 'org_id', name: 'org_id' },
            { label: 'Sequence No.', base: 'organization', ngModel: 'sequenceNo', name: 'sequenceNo' },
            { label: 'Name', base: 'organization', ngModel: 'name', name: 'name' },
            { label: 'Minor Name', base: 'organization', ngModel: 'shortName', name: 'shortname' },
            // { label: 'Established Year', base: 'organization', ngModel: 'established.year', name: 'year', isNested: true },
            { label: 'Established', base: 'organization', ngModel: 'established.year', name: 'year', isNested: true },
            { label: 'Status', base: 'organization', ngModel: 'status', name: 'status', valuePath: 'codeValue' },
            // { label: 'Comments', base: 'organization', ngModel: 'comments', name: 'commentsOMD', arrayPath: 'comments' },
            { label: 'Sort Name', base: 'organization', ngModel: 'sortMajorName', name: 'sortMajorName' },
            { label: 'AAM ID', base: 'organization', ngModel: 'aamId', name: 'aamId' },
            { label: 'AAM', base: 'organization', ngModel: 'flags.aam', name: 'accreditedAAMOMD', isNested: true, hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Institution Type/Description', base: 'organization', ngModel: 'entityType', name: 'entityType' },
            { label: 'Congressional District', base: 'organization', ngModel: 'congressDistrict', name: 'congressDistrict' },
            { label: 'Handicapped Accessible?', base: 'organization', ngModel: 'flags.handicapped', name: 'handicapped', isNested: true, hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Handicap Accessibility Type', base: 'organization', ngModel: 'handicapped', name: 'handicappedType', hasOptions: [{ value: 'P', label: 'Partial' }, { value: 'F', label: 'Full' }] },
            { label: 'Attendance Count', base: 'organization', ngModel: 'attendance.count', name: 'attendanceCount', isNested: true },
            { label: 'Attendance Method', base: 'organization', ngModel: 'attendance.method', name: 'attendanceMethod', isNested: true, hasOptions: [{ value: 'A', label: 'Actual' }, { value: 'E', label: 'Estimated' }] },
            { label: 'Hours & Admission Prices', base: 'organization', ngModel: 'timing', name: 'timing' },
            { label: 'Membership', base: 'organization', ngModel: 'membership', name: 'membership', isNested: true, arrayPath: 'membership', isIndexedArray: true },
            { label: 'Accredited', base: 'organization', ngModel: 'flags.accredited', name: 'accreditedOMD', isNested: true, hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'First Email', base: 'organization', ngModel: 'flags.firstMail', name: 'firstEmailOMD', isNested: true, hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Second Email', base: 'organization', ngModel: 'flags.secondMail', name: 'secondEmailOMD', isNested: true, hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'NRM', base: 'organization', ngModel: 'flags.nrm', name: 'nrmOMD', isNested: true, hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Other', base: 'organization', ngModel: 'flags.otherSrc', name: 'otherOMD', isNested: true, hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Icom', base: 'organization', ngModel: 'flags.icom', name: 'icomOMD', isNested: true, hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Mailer', base: 'organization', ngModel: 'flags.mailer', name: 'mailerOMD', isNested: true },
            { label: 'Former: Names', base: 'organization', ngModel: 'formerNames.name', name: 'formerNames', isNested: true, arrayPath: 'formerNames' },
            { label: 'Former: Start Date', base: 'organization', ngModel: 'formerNames.startDate', name: 'formerStartDate', isNested: true, arrayPath: 'formerNames', isDate: true, dateFormat: 'dS mmmm, yyyy' },
            { label: 'Former: End Date', base: 'organization', ngModel: 'formerNames.endDate', name: 'formerEndDate', isNested: true, arrayPath: 'formerNames', isDate: true, dateFormat: 'dS mmmm, yyyy' },
            //communication Tab
            { label: 'Contact: Phone No.', base: 'organization', ngModel: 'contact.phoneNumbers', name: 'phoneNumbers', isNested: true },
            { label: 'Contact: Fax', base: 'organization', ngModel: 'contact.faxNumbers', name: 'faxNumbers', isNested: true },
            { label: 'Contact: Primary Email', base: 'organization', ngModel: 'contact.emails.primary', name: 'primary', isNested: true },
            { label: 'Contact: Secondary Email', base: 'organization', ngModel: 'contact.emails.secondary', name: 'secondary', isNested: true },
            { label: 'Contact: Websites', base: 'organization', ngModel: 'contact.websites', name: 'websites', isNested: true },
            { label: 'Contact: Social', base: 'organization', ngModel: 'contact.social', name: 'social', isNested: true },
            { label: 'Address: Type', base: 'organization', ngModel: 'address.addressType', name: 'addressType', isNested: true, arrayPath: 'address', valuePath: 'description' },
            { label: 'Address: Street 1', base: 'organization', ngModel: 'address.street1', name: 'street1', isNested: true, arrayPath: 'address' },
            { label: 'Address: Street 2', base: 'organization', ngModel: 'address.street2', name: 'street2', isNested: true, arrayPath: 'address' },
            { label: 'Address: City', base: 'organization', ngModel: 'address.cityName', name: 'city', isNested: true, arrayPath: 'address' /*, valuePath: 'codeValue'*/ },
            { label: 'Address:  State/Territory', base: 'organization', ngModel: 'address.stateName', name: 'state', isNested: true, arrayPath: 'address', /*valuePath: 'codeValue'*/ },
            { label: 'Address: Country', base: 'organization', ngModel: 'address.countryName', name: 'country', isNested: true, arrayPath: 'address' /*, valuePath: 'codeValue'*/ },
            { label: 'Address: Zip', base: 'organization', ngModel: 'address.zip', name: 'zip', isNested: true, arrayPath: 'address' },
            //Personnel
            { label: 'Personnel: Part-Time Paid', base: 'organization', ngModel: 'volunteer.totalPartTimePaid', name: 'totalPartTimePaid', isNested: true },
            { label: 'Personnel: Part-Time Volunteers', base: 'organization', ngModel: 'volunteer.totalPartTimeUnpaid', name: 'totalPartTimeUnpaid', isNested: true },
            { label: 'Personnel: Full-Time Paid', base: 'organization', ngModel: 'volunteer.totalFullTimePaid', name: 'totalFullTimePaid', isNested: true },
            { label: 'Personnel: Full-Time Volunteers', base: 'organization', ngModel: 'volunteer.totalFullTimeUnpaid', name: 'totalFullTimeUnpaid', isNested: true },

            { label: 'Personnel: Interns', base: 'organization', ngModel: 'volunteer.totalIntern', name: 'totalIntern', isNested: true },

            { label: 'Personnel : Volunteers Hours', base: 'organization', ngModel: 'volunteerHours', name: 'volunteerHours' },
            { label: 'Key Personnel: Sequence No.', base: 'personnel', ngModel: 'personnel.sequenceNumber', name: 'personnelsequenceNumber', isNested: true, arrayPath: 'personnel' },
            // { label: 'Key Personnel: Title', base: 'personnel', ngModel: 'personnel.titleMaster', name: 'titleMaster', isNested: true, arrayPath: 'personnel', valuePath: 'codeValue' },
            { label: 'Key Personnel: Title', base: 'personnel', ngModel: 'personnel.titleMaster', name: 'title', isNested: true, arrayPath: 'personnel', valuePath: 'codeValue' },
            { label: 'Key Personnel: Custom Title', base: 'personnel', ngModel: 'personnel.title', name: 'custometitleomd', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Prefix', base: 'personnel', ngModel: 'personnel.name.prefix', name: 'prefix', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: First Name', base: 'personnel', ngModel: 'personnel.name.first', name: 'first', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Middle Name', base: 'personnel', ngModel: 'personnel.name.middle', name: 'middle', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Last Name', base: 'personnel', ngModel: 'personnel.name.last', name: 'last', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Suffix', base: 'personnel', ngModel: 'personnel.name.suffix', name: 'Suffix', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Phone No.', base: 'personnel', ngModel: 'personnel.phoneNumbers', name: 'personnelphoneNumbers', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Fax', base: 'personnel', ngModel: 'personnel.faxNumbers', name: 'personnelfaxNumbers', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Primary Email', base: 'personnel', ngModel: 'personnel.email.primary', name: 'personnelemailprimary', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Secondary Email', base: 'personnel', ngModel: 'personnel.email.secondary', name: 'personnelemailsecondary', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Websites', base: 'personnel', ngModel: 'personnel.website', name: 'personnelwebsite', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Kind Attention', base: 'personnel', ngModel: 'personnel.attention', name: 'personnelattention', isNested: true, arrayPath: 'personnel', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Key Personnel:  Consider for Directory Print', base: 'personnel', ngModel: 'personnel.printFlag', name: 'personnelprintFlag', isNested: true, arrayPath: 'personnel', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            //other Info
            { label: 'Activities', base: 'organization', ngModel: 'activities.freeText', name: 'activitiesFreeText', isNested: true },
            { label: 'Governal Authority', base: 'organization', ngModel: 'governing', name: 'governing' },
            { label: 'Operating Income', base: 'organization', ngModel: 'income', name: 'income' },
            { label: 'Operating Expense ', base: 'organization', ngModel: 'expenses', name: 'expenses' },
            { label: 'Features: Type', base: 'organization', ngModel: 'features.featureType', name: 'featureType', isNested: true, arrayPath: 'features' },
            { label: 'Features: Feature', base: 'organization', ngModel: 'features.code', name: 'featuresCode', isNested: true, arrayPath: 'features', valuePath: 'codeValue' },
            { label: 'Features: Description', base: 'organization', ngModel: 'features.name', name: 'featuresName', isNested: true, arrayPath: 'features' },
            { label: 'Publications: Sequence No.', base: 'organization', ngModel: 'publication.sequenceNumber', name: 'publicationSequence', isNested: true, arrayPath: 'publication' },
            { label: 'Publications: Description', base: 'organization', ngModel: 'publication.description', name: 'publication', isNested: true, arrayPath: 'publication' },
            { label: 'Research Fields: Description', base: 'organization', ngModel: 'research.description', name: 'research', isNested: true, arrayPath: 'research' },
            // Exhibition
            { label: 'Exhibitions: Sequence No.', base: 'exhibition', ngModel: 'exhibition.sequenceNumber', name: 'exhibitionsequenceNumber', isNested: true, arrayPath: 'exhibition' },
            { label: 'Exhibitions: Name', base: 'exhibition', ngModel: 'exhibition.exhibitName', name: 'exhibitName', isNested: true, arrayPath: 'exhibition' },
            { label: 'Exhibitions: Start Date', base: 'exhibition', ngModel: 'exhibition.startDate', name: 'startDate', isNested: true, arrayPath: 'exhibition', isDate: true, dateFormat: 'dS mmmm, yyyy' },

            { label: 'Exhibitions: End Date', base: 'exhibition', ngModel: 'exhibition.endDate', name: 'endDate', isNested: true, arrayPath: 'exhibition', isDate: true, dateFormat: 'dS mmmm, yyyy' },
            { label: 'Exhibitions: Show Date', base: 'exhibition', ngModel: 'exhibition.showDate', name: 'showDate', isNested: true, arrayPath: 'exhibition' },
            { label: 'Exhibitions: Travel', base: 'exhibition', ngModel: 'exhibition.travelFlag', name: 'travelFlag', isNested: true, arrayPath: 'exhibition', hasOptions: [{ value: 'Y', label: 'YES' }, { value: 'N', label: 'NO' }, { value: '', label: '' }] },
        ],
        'AAD': [
            { label: 'Organization ID', base: 'organization', ngModel: 'org_id', name: 'org_id' },
            { label: 'Classification Code', base: 'organization', ngModel: 'classificationCode', name: 'classificationcode', valuePath: 'codeValue' },
            { label: 'Parent Organization/Association', base: 'organization', ngModel: 'parentId', name: 'organization/association', valuePath: 'name' },
            { label: 'Section', base: 'organization', ngModel: 'sectionCode', name: 'section', valuePath: 'description', isNested: true, arrayPath: 'sectionCode' },
            { label: 'Sequence No.', base: 'organization', ngModel: 'sequenceNo', name: 'sequenceno' },
            { label: 'Name', base: 'organization', ngModel: 'name', name: 'name' },
            { label: 'Minor Name', base: 'organization', ngModel: 'shortName', name: 'shortname' },
            { label: 'Name Suffix', base: 'organization', ngModel: 'nameSuffix', name: 'namesuffix' },
            { label: 'Additional Info', base: 'organization', ngModel: 'additionalInformation', name: 'additionalInfo' },
            { label: 'Sub Number', base: 'organization', ngModel: 'sub_nbr', name: 'subNo' },
            { label: 'Status', base: 'organization', ngModel: 'status', name: 'status', valuePath: 'codeValue' },
            // { label: 'Established Year', base: 'organization', ngModel: 'established.year', name: 'year', isNested: true },
            // { label: 'Established Description', base: 'organization', ngModel: 'established.detail', name: 'detail', isNested: true },
            { label: 'Established', base: 'organization', ngModel: 'established.detail', name: 'detail', isNested: true },
            { label: 'Hours Open to the Public', base: 'organization', ngModel: 'timing', name: 'timing' },
            { label: 'Annual Attendance', base: 'organization', ngModel: 'attendance.count', name: 'attendanceCount', isNested: true },
            { label: 'Admission Fees', base: 'organization', ngModel: 'feeStructure', name: 'feestructure' },
            // { label: 'Comments', base: 'organization', ngModel: 'comments', name: 'commentsAAD', arrayPath: 'comments' },
            { label: 'Minor Name Index Indication', base: 'organization', ngModel: 'minorNameIndexIndication', name: 'minornameindexindication', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Sort Major Name', base: 'organization', ngModel: 'sortMajorName', name: 'sortMajorName' },
            { label: 'Sort Minor Name', base: 'organization', ngModel: 'sortMinorName', name: 'sortMinorName' },
            { label: 'Operating Income', base: 'organization', ngModel: 'income', name: 'income' },
            { label: 'Purchases ($)', base: 'organization', ngModel: 'purchases', name: 'purchases' },
            { label: 'Gallery Description', base: 'organization', ngModel: 'galleryDescription', name: 'gallerydescription' },
            { label: 'Membership', base: 'organization', ngModel: 'membership', name: 'membership', isNested: true, arrayPath: 'membership', isIndexedArray: true },
            { label: 'Circulation', base: 'organization', ngModel: 'circulation', name: 'circulation' },

            { label: 'Former: Names', base: 'organization', ngModel: 'formerNames.name', name: 'formerNames', isNested: true, arrayPath: 'formerNames' },
            { label: 'Former: Suffix', base: 'organization', ngModel: 'formerNames.suffix', name: 'formerNamesSuffix', isNested: true, arrayPath: 'formerNames' },

            //Book Edition For AAD
            { label: 'Book Edition: Edition No.', base: 'organization', ngModel: 'bookEdition.edition', name: 'bookEditionNo', isNested: true, arrayPath: 'bookEdition' },
            { label: 'Book Edition: Nom Source', base: 'organization', ngModel: 'bookEdition.nomSource', name: 'bookEditionNomSource', isNested: true, arrayPath: 'bookEdition' },
            { label: 'Book Edition: Edition Respond', base: 'organization', ngModel: 'bookEdition.editionRespond', name: 'bookEditionRespond', isNested: true, arrayPath: 'bookEdition' },
            { label: 'Book Edition: Status', base: 'organization', ngModel: 'bookEdition.statusValue', name: 'bookEditionStatus', isNested: true, arrayPath: 'bookEdition' },
            { label: 'Book Edition: Kill Comment', base: 'organization', ngModel: 'bookEdition.killComment', name: 'bookEditionKillComment', isNested: true, arrayPath: 'bookEdition' },
            { label: 'Book Edition: Editorial Comment', base: 'organization', ngModel: 'bookEdition.editorialComment', name: 'bookEditionEditorialComment', isNested: true, arrayPath: 'bookEdition' },
            { label: 'Book Edition: Research Comment', base: 'organization', ngModel: 'bookEdition.researchComment', name: 'bookEditionResearchComment', isNested: true, arrayPath: 'bookEdition' },
            { label: 'Book Edition: Permanent Comment', base: 'organization', ngModel: 'bookEdition.permanentComment', name: 'bookEditionPermantComment', isNested: true, arrayPath: 'bookEdition' },
            { label: 'Book Edition: Must Field', base: 'organization', ngModel: 'bookEdition.mustField', name: 'bookEditionMustField', isNested: true, arrayPath: 'bookEdition', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },


            //Communication Tab//
            { label: 'Contact: Phone No.', base: 'organization', ngModel: 'contact.phoneNumbers', name: 'phoneNumbers', isNested: true },
            { label: 'Contact: Fax', base: 'organization', ngModel: 'contact.faxNumbers', name: 'faxNumbers', isNested: true },
            { label: 'Contact: Primary Email', base: 'organization', ngModel: 'contact.emails.primary', name: 'primary', isNested: true },
            { label: 'Contact: Secondary Email', base: 'organization', ngModel: 'contact.emails.secondary', name: 'secondary', isNested: true },
            { label: 'Contact: Websites', base: 'organization', ngModel: 'contact.websites', name: 'websites', isNested: true },
            { label: 'Contact: Social', base: 'organization', ngModel: 'contact.social', name: 'social', isNested: true },
            { label: 'Contact: Pager', base: 'organization', ngModel: 'contact.pager', name: 'pager', isNested: true },
            { label: 'Contact: Mobile No.', base: 'organization', ngModel: 'contact.mobile', name: 'mobile', isNested: true },
            { label: 'Contact: Wats', base: 'organization', ngModel: 'contact.wats', name: 'wats', isNested: true },
            { label: 'Contact: FTS', base: 'organization', ngModel: 'contact.fts', name: 'fts', isNested: true },
            { label: 'Contact: TWX', base: 'organization', ngModel: 'contact.twx', name: 'twx', isNested: true },
            { label: 'Contact: Print Flag', base: 'organization', ngModel: 'contact.printFlag', name: 'contactprintFlag', isNested: true, hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Contact: Cable', base: 'organization', ngModel: 'contact.cable', name: 'cable', isNested: true },
            { label: 'Contact: TelexNumber', base: 'organization', ngModel: 'contact.telexNumbers', name: 'telexNumbers', isNested: true },
            { label: 'Contact: Other', base: 'organization', ngModel: 'contact.others', name: 'others', isNested: true },
            { label: 'Address: Type', base: 'organization', ngModel: 'address.addressType', name: 'addressType', isNested: true, arrayPath: 'address', valuePath: 'description' },
            { label: 'Address: Street 1', base: 'organization', ngModel: 'address.street1', name: 'street1', isNested: true, arrayPath: 'address' },
            { label: 'Address: Street 2', base: 'organization', ngModel: 'address.street2', name: 'street2', isNested: true, arrayPath: 'address' },
            { label: 'Address: City', base: 'organization', ngModel: 'address.cityName', name: 'city', isNested: true, arrayPath: 'address' /*, valuePath: 'codeValue'*/ },
            { label: 'Address:  State/Territory', base: 'organization', ngModel: 'address.stateName', name: 'state', isNested: true, arrayPath: 'address', /*valuePath: 'codeValue'*/ },
            { label: 'Address: Country', base: 'organization', ngModel: 'address.countryName', name: 'country', isNested: true, arrayPath: 'address' /*, valuePath: 'codeValue'*/ },
            { label: 'Address: Zip', base: 'organization', ngModel: 'address.zip', name: 'zip', isNested: true, arrayPath: 'address' },
            { label: 'Address: Comment', base: 'organization', ngModel: 'address.comments', name: 'Comments', isNested: true, arrayPath: 'address' },
            { label: 'Address: Nixie Date', base: 'organization', ngModel: 'address.nixieDate', name: 'nixieDate', isNested: true, arrayPath: 'address' },
            { label: 'Address: Nixie Comment', base: 'organization', ngModel: 'address.nixieComments', name: 'nixieComments', isNested: true, arrayPath: 'address' },
            { label: 'Address: Nixie Index', base: 'organization', ngModel: 'address.nixieIndex', name: 'nixieIndex', isNested: true, arrayPath: 'address', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Address:  Mailing Index', base: 'organization', ngModel: 'address.mailingIndex', name: 'mailingIndex', isNested: true, arrayPath: 'address', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            //Personnel Tab
            { label: 'Key Personnel: Sequence No.', base: 'personnel', ngModel: 'personnel.sequenceNumber', name: 'personnelsequenceNumber', isNested: true, arrayPath: 'personnel' },
            // { label: 'Key Personnel: Title', base: 'personnel', ngModel: 'personnel.titleMaster', name: 'titleMaster', isNested: true, arrayPath: 'personnel', valuePath: 'codeValue' },
            // { label: 'Key Personnel: Custom Title', base: 'personnel', ngModel: 'personnel.title', name: 'title', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Title', base: 'personnel', ngModel: 'personnel.title', name: 'title', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Prefix', base: 'personnel', ngModel: 'personnel.name.prefix', name: 'prefix', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: First Name', base: 'personnel', ngModel: 'personnel.name.first', name: 'first', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Middle Name', base: 'personnel', ngModel: 'personnel.name.middle', name: 'middle', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Last Name', base: 'personnel', ngModel: 'personnel.name.last', name: 'last', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Suffix', base: 'personnel', ngModel: 'personnel.name.suffix', name: 'Suffix', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Academic Degree', base: 'personnel', ngModel: 'personnel.degree', name: 'degree', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Phone No.', base: 'personnel', ngModel: 'personnel.phoneNumbers', name: 'personnelphoneNumbers', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Fax', base: 'personnel', ngModel: 'personnel.faxNumbers', name: 'personnelfaxNumbers', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Primary Email', base: 'personnel', ngModel: 'personnel.email.primary', name: 'personnelemailprimary', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Secondary Email', base: 'personnel', ngModel: 'personnel.email.secondary', name: 'personnelemailsecondary', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Websites', base: 'personnel', ngModel: 'personnel.website', name: 'personnelwebsite', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Contact Person Index', base: 'personnel', ngModel: 'personnel.contactPersonIndex', name: 'personnelcontactPersonIndex', isNested: true, arrayPath: 'personnel', hasOptions: [{ value: "Y", label: 'YES' }, { value: "N", label: 'NO' }] },
            { label: 'Key Personnel:  Consider for Directory Print', base: 'personnel', ngModel: 'personnel.printFlag', name: 'personnelprintFlag', isNested: true, arrayPath: 'personnel', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            //Other Tab
            { label: 'Art School: Courses Offered', base: 'organization', ngModel: 'artSchool.coursesOffered', name: 'coursesOffered', isNested: true, arrayPath: 'artSchool' },
            { label: 'Art School: Instructor', base: 'organization', ngModel: 'artSchool.instructors', name: 'instructors', isNested: true, arrayPath: 'artSchool' },
            { label: 'Art School: Adult Hobby Classes', base: 'organization', ngModel: 'artSchool.adultHobbyClass', name: 'adult_hobby_class', isNested: true, arrayPath: 'artSchool' },
            { label: 'Art School: Entrance Required', base: 'organization', ngModel: 'artSchool.entranceRequired', name: 'entranceRequired', isNested: true, arrayPath: 'artSchool' },
            { label: 'Art School: Enrollment', base: 'organization', ngModel: 'artSchool.enrollment', name: 'enrollment', isNested: true, arrayPath: 'artSchool' },
            { label: 'Art School: Control', base: 'organization', ngModel: 'artSchool.control', name: 'control', isNested: true, arrayPath: 'artSchool' },
            { label: 'Art School: Children Classes', base: 'organization', ngModel: 'artSchool.childrenClasses', name: 'Childrens Classes', isNested: true, arrayPath: 'artSchool' },
            { label: 'Art School: Degree Granted', base: 'organization', ngModel: 'artSchool.degreeGranted', name: 'degreeGranted', isNested: true, arrayPath: 'artSchool' },
            { label: 'Art School: Tuition', base: 'organization', ngModel: 'artSchool.tuition', name: 'tuition', isNested: true, arrayPath: 'artSchool' },
            { label: 'Art School: Class Time', base: 'organization', ngModel: 'artSchool.classTime', name: 'classTime', isNested: true, arrayPath: 'artSchool' },
            { label: 'Art School: Summer School', base: 'organization', ngModel: 'artSchool.summerSchool', name: 'summerSchool', isNested: true, arrayPath: 'artSchool' },
            { label: 'Art School: Scholarship Fellowships', base: 'organization', ngModel: 'artSchool.scholarshipFellowships', name: 'scholarshipFellowships', isNested: true, arrayPath: 'artSchool' },
            { label: 'Art School: Scholarship', base: 'organization', ngModel: 'artSchool.scholarship', name: 'scholarship', isNested: true, arrayPath: 'artSchool', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Art School: Grants', base: 'organization', ngModel: 'artSchool.grants', name: 'grants', isNested: true, arrayPath: 'artSchool', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Art School: Financial Aid', base: 'organization', ngModel: 'artSchool.financialAid', name: 'financialAid', isNested: true, arrayPath: 'artSchool', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Art School: Fellowship', base: 'organization', ngModel: 'artSchool.fellowships', name: 'fellowsh', isNested: true, arrayPath: 'artSchool', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Art School: Loans', base: 'organization', ngModel: 'artSchool.loans', name: 'loans', isNested: true, arrayPath: 'artSchool', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Art School: Assistantship', base: 'organization', ngModel: 'artSchool.assistantships', name: 'assistantships', isNested: true, arrayPath: 'artSchool', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Art School: Work Study', base: 'organization', ngModel: 'artSchool.workStudy', name: 'workStudy', isNested: true, arrayPath: 'artSchool', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Activities: Sequence No.', base: 'organization', ngModel: 'activities.sequenceNumber', name: 'sequenceNumbers' },
            { label: 'Activities: Scholarships', base: 'organization', ngModel: 'activities.scholarships', name: 'activitiesScholarships' },
            { label: 'Activities: Junior Museum', base: 'organization', ngModel: 'activities.juniorMuseum', name: 'juniorMuseum' },
            { label: 'Activities: Education Programs', base: 'organization', ngModel: 'activities.educationDept', name: 'educationDept' },
            { label: 'Activities: Book Travel Exhibition', base: 'organization', ngModel: 'activities.bookTravelingExhib', name: 'bookTravelingExhib' },
            { label: 'Activities: Lectures/Awards', base: 'organization', ngModel: 'activities.lecturesAwards', name: 'lecturesAwards' },
            { label: 'Activities: Originates Traveling Exhibitions', base: 'organization', ngModel: 'activities.origiTravelingExhib', name: 'origiTravelingExhib' },
            { label: 'Activities: Extension Programs', base: 'organization', ngModel: 'activities.extensionDept', name: 'extensionDept' },
            { label: 'Activities: Museum Shop', base: 'organization', ngModel: 'activities.museumShop', name: 'museumShop' },
            //Feature
            { label: 'Features: Sequence No.', base: 'organization', ngModel: 'features.sequenceNo', name: 'featureSequenceNo', isNested: true, arrayPath: 'features' },
            { label: 'Features Type', base: 'organization', ngModel: 'features.featureType', name: 'featureType', isNested: true, arrayPath: 'features' },
            { label: 'Features', base: 'organization', ngModel: 'features.code', name: 'featuresCode', isNested: true, arrayPath: 'features', valuePath: 'codeValue' },
            { label: 'Features Description', base: 'organization', ngModel: 'features.name', name: 'featuresName', isNested: true, arrayPath: 'features' },
            { label: 'Features Major Indexed', base: 'organization', ngModel: 'features.majorIndex', name: 'majorIndex', isNested: true, arrayPath: 'features' },
            { label: 'Features Index Indication', base: 'organization', ngModel: 'features.indexIndication', name: 'indexIndication', isNested: true, arrayPath: 'features' },
            // Exhibition
            { label: 'Exhibitions: Sequence No.', base: 'exhibition', ngModel: 'exhibition.sequenceNumber', name: 'exhibitionsequenceNumber', isNested: true, arrayPath: 'exhibition' },
            { label: 'Exhibitions: Name', base: 'exhibition', ngModel: 'exhibition.exhibitName', name: 'exhibitName', isNested: true, arrayPath: 'exhibition' },
            { label: 'Exhibitions: StartDate', base: 'exhibition', ngModel: 'exhibition.startDateText', name: 'startDate', isNested: true, arrayPath: 'exhibition', isDate: true, dateFormat: 'dS mmmm, yyyy' },
            { label: 'Exhibitions: EndDate', base: 'exhibition', ngModel: 'exhibition.endDateText', name: 'endDate', isNested: true, arrayPath: 'exhibition', isDate: true, dateFormat: 'dS mmmm, yyyy' },
            { label: 'Exhibitions: ShowDate', base: 'exhibition', ngModel: 'exhibition.showDate', name: 'showDate', isNested: true, arrayPath: 'exhibition' },
            // { label: 'Exhibitions: Travel', base: 'exhibition', ngModel: 'exhibition.travelFlag', name: 'travelFlag', isNested: true, arrayPath: 'exhibition' },
            { label: 'Publications: Sequence No.', base: 'organization', ngModel: 'publication.sequenceNumber', name: 'publicationsequenceNumber', isNested: true, arrayPath: 'publication' },
            { label: 'Publications: Description', base: 'organization', ngModel: 'publication.description', name: 'publication', isNested: true, arrayPath: 'publication' },
        ],
        'CFS': [
            //General Tab
            { label: 'Organization ID', base: 'organization', ngModel: 'org_id', name: 'org_id' },
            { label: 'Sequence No.', base: 'organization', ngModel: 'sequenceNo', name: 'sequenceNo' },
            { label: 'Established Year', base: 'organization', ngModel: 'established.year', name: 'year', isNested: true },
            { label: 'Total Assets', base: 'organization', ngModel: 'totalAssetsLeadBank', name: 'totalAssetsLeadBank' },
            { label: 'Subsidiary of', base: 'organization', ngModel: 'subsidiaryOf', name: 'subsidiaryOf' },
            { label: 'Areas of Special Strength', base: 'organization', ngModel: 'specialStrength', name: 'specialStrength' },
            { label: 'Number of Countries with Offices', base: 'organization', ngModel: 'officesCountries', name: 'officesCountries' },
            { label: 'Number of Offices in US', base: 'organization', ngModel: 'numberOfOffices.us', name: 'numberOfOfficesus', isNested: true },
            { label: 'Number of Offices Worldwide', base: 'organization', ngModel: 'numberOfOffices.wordwide', name: 'numberOfOfficeswordwide', isNested: true },
            { label: 'Number of Overseas Affiliates', base: 'organization', ngModel: 'overseasAffiliates', name: 'overseasAffiliates' },
            { label: 'Average Private Placement Participant', base: 'organization', ngModel: 'averagePrivatePlacementParticipant', name: 'avgPrivatePlacementParticipant' },
            { label: 'Loan Ceiling', base: 'organization', ngModel: 'loanCeiling', name: 'loanCeiling' },
            { label: 'Annual Contractual Expenses', base: 'organization', ngModel: 'flags.annualContractualExpenses', name: 'annualContractualExpenses', isNested: true, hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Line Access Available', base: 'organization', ngModel: 'flags.lineAccessAvailable', name: 'lineAccessAvailable', isNested: true, hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Joint Commissions', base: 'organization', ngModel: 'flags.jointlyWithCommission', name: 'jointCommision', isNested: true, hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Third Party', base: 'organization', ngModel: 'flags.thirdParty', name: 'thirdParty', isNested: true, hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Lease Participate', base: 'organization', ngModel: 'flags.participateLeaseOthers', name: 'participateLeaseOthers', isNested: true, hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Name', base: 'organization', ngModel: 'name', name: 'name' },
            { label: 'Minor Name', base: 'organization', ngModel: 'shortName', name: 'shortname' },
            { label: 'Sort Name', base: 'organization', ngModel: 'sortMajorName', name: 'sortMajorName' },
            { label: 'Employees', base: 'organization', ngModel: 'employees', name: 'employeesCFS' },
            { label: 'Second Sub Name', base: 'organization', ngModel: 'subName', name: 'subName' },
            { label: 'Lead Bank', base: 'organization', ngModel: 'leadBank', name: 'leadBank' },
            { label: 'Legal Lending Limit in US', base: 'organization', ngModel: 'lendingLimit', name: 'lendingLimit' },
            { label: 'Funds Available for Investment or Loans', base: 'organization', ngModel: 'fundsAvailable', name: 'fundsAvailable' },
            { label: 'Partner', base: 'organization', ngModel: 'partner', name: 'partnerCFS' },
            { label: 'Annual Revenue: US', base: 'organization', ngModel: 'annualRevenues.us', name: 'us', isNested: true },
            { label: 'Annual Revenue: Worldwide', base: 'organization', ngModel: 'annualRevenues.wordwide', name: 'wordwide', isNested: true },
            { label: 'Average Annual Revenue', base: 'organization', ngModel: 'averageAnnualRevenues', name: 'averageAnnualRevenues' },
            { label: 'Percentage of Fees Obtained Through: Professional service fees', base: 'organization', ngModel: 'feePercentage.professionalService', name: 'professionalService', isNested: true },
            { label: 'Percentage of Fees Obtained Through:Commissions', base: 'organization', ngModel: 'feePercentage.commision', name: 'commision', isNested: true },
            { label: 'Parent Name', base: 'organization', ngModel: 'parentName', name: 'parentName' },
            { label: 'Parent Assets', base: 'organization', ngModel: 'parentAssets', name: 'parentAssets' },
            { label: 'Average Annual Corporate Assignment', base: 'organization', ngModel: 'averageAnnualCorporateAssignment', name: 'averageAnnualCorporateAssignment' },
            { label: 'Company Status', base: 'organization', ngModel: 'status', name: 'status', valuePath: 'codeValue' },
            { label: 'Work Flow Status', base: 'organization', ngModel: 'workflowStatus', name: 'workStatus', valuePath: 'codeValue' },
            { label: 'Average Portfolio Maturity', base: 'organization', ngModel: 'averagePortfolioMaturity', name: 'avgportFolioMaturity' },
            { label: 'Intermediary Loan Percentage', base: 'organization', ngModel: 'intermediaryLoanPercentage', name: 'intermediaryLoanPercentage' },
            { label: 'Parent Address', base: 'organization', ngModel: 'parentAddress', name: 'parentAddress' },
            { label: 'Gallery Description', base: 'organization', ngModel: 'galleryDescription', name: 'galleryDescription' },

            { label: 'Listing Type', base: 'organization', ngModel: 'listingType.listing', name: 'listing', valuePath: 'codeValue', isNested: true, arrayPath: 'listingType' }, {
                label: 'Listing Active',
                base: 'organization',
                ngModel: 'listingType.listingActive',
                name: 'listingName',
                isNested: true,
                arrayPath: 'listingType',
                hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }]
            },
            { label: 'Member: Name', base: 'organization', ngModel: 'members.name', name: 'memberName', isNested: true, arrayPath: 'members' },
            { label: 'Member: Listing Type', base: 'organization', ngModel: 'members.listingName', name: 'memeberListingType', isNested: true, arrayPath: 'members' }, {
                label: 'Member: Active',
                base: 'organization',
                ngModel: 'members.active',
                name: 'memberActive',
                isNested: true,
                arrayPath: 'members',
                hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }]
            },

            //Communication Tab
            { label: 'Contact: Phone No.', base: 'organization', ngModel: 'contact.phoneNumbers', name: 'phoneNumbers', isNested: true },
            { label: 'Contact: Fax', base: 'organization', ngModel: 'contact.faxNumbers', name: 'faxNumbers', isNested: true },
            { label: 'Owned By', base: 'organization', ngModel: 'ownedBy', name: 'ownedBy' },
            { label: 'Contact: Primary Email', base: 'organization', ngModel: 'contact.emails.primary', name: 'primary', isNested: true },
            { label: 'Contact: Secondary Email', base: 'organization', ngModel: 'contact.emails.secondary', name: 'secondary', isNested: true },
            { label: 'Contact: Websites', base: 'organization', ngModel: 'contact.websites', name: 'websites', isNested: true },
            { label: 'Contact: Social', base: 'organization', ngModel: 'contact.social', name: 'social', isNested: true },
            { label: 'Address: Type', base: 'organization', ngModel: 'address.addressType', name: 'addressType', isNested: true, arrayPath: 'address', valuePath: 'description' },
            { label: 'Address: Street 1', base: 'organization', ngModel: 'address.street1', name: 'street1', isNested: true, arrayPath: 'address' },
            { label: 'Address: Street 2', base: 'organization', ngModel: 'address.street2', name: 'street2', isNested: true, arrayPath: 'address' },
            { label: 'Address: City', base: 'organization', ngModel: 'address.cityName', name: 'city', isNested: true, arrayPath: 'address' /*, valuePath: 'codeValue'*/ },
            { label: 'Address:  State/Territory', base: 'organization', ngModel: 'address.stateName', name: 'state', isNested: true, arrayPath: 'address', /*valuePath: 'codeValue'*/ },
            { label: 'Address: Country', base: 'organization', ngModel: 'address.countryName', name: 'country', isNested: true, arrayPath: 'address' /*, valuePath: 'codeValue'*/ },
            { label: 'Address: Zip', base: 'organization', ngModel: 'address.zip', name: 'zip', isNested: true, arrayPath: 'address' },


            //Personnel Tab

            { label: 'Portfolio Manager On Staff', base: 'organization', ngModel: 'professionalStaff.portfolioManagersOnStaff', name: 'portfolioManagersOnStaff', isNested: true },
            { label: 'Marketing Department', base: 'organization', ngModel: 'professionalStaff.marketingDepartment', name: 'marketingDepartment', isNested: true },
            { label: ' Property Professionals Data', base: 'organization', ngModel: 'professionalStaff.developmentDepartment', name: 'developmentDepartment', isNested: true },
            { label: 'Technical Department', base: 'organization', ngModel: 'professionalStaff.technicalDepartment', name: 'technicalDepartment', isNested: true },
            { label: 'Key Personnel: Sequence No.', base: 'personnel', ngModel: 'personnel.sequenceNumber', name: 'personnelsequenceNumber', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Personnel ID', base: 'personnel', ngModel: 'personnel.employee_id', name: 'personnelIdNumber', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Title', base: 'personnel', ngModel: 'personnel.titleMaster', name: 'titleMaster', isNested: true, arrayPath: 'personnel', valuePath: 'codeValue' },
            // { label: 'Key Personnel: Custom Title', base: 'personnel', ngModel: 'personnel.title', name: 'title', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Prefix', base: 'personnel', ngModel: 'personnel.name.prefix', name: 'prefix', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: First Name', base: 'personnel', ngModel: 'personnel.name.first', name: 'first', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Middle Name', base: 'personnel', ngModel: 'personnel.name.middle', name: 'middle', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Last Name', base: 'personnel', ngModel: 'personnel.name.last', name: 'last', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Suffix', base: 'personnel', ngModel: 'personnel.name.suffix', name: 'Suffix', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Phone No.', base: 'personnel', ngModel: 'personnel.phoneNumbers', name: 'personnelphoneNumbers', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Fax', base: 'personnel', ngModel: 'personnel.faxNumbers', name: 'personnelfaxNumbers', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Primary Email', base: 'personnel', ngModel: 'personnel.email.primary', name: 'personnelemailprimary', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Secondary Email', base: 'personnel', ngModel: 'personnel.email.secondary', name: 'personnelemailsecondary', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Websites', base: 'personnel', ngModel: 'personnel.website', name: 'personnelwebsite', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel:  Consider for Directory Print', base: 'personnel', ngModel: 'personnel.printFlag', name: 'personnelprintFlag', isNested: true, arrayPath: 'personnel', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            // { label: 'Key Personnel: Active', base: 'personnel', ngModel: 'personnel.activeFlag', name: 'personnelactiveFlag', isNested: true, arrayPath: 'personnel', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Key Personnel: Deleted', base: 'personnel', ngModel: 'personnel.deletedFlag', name: 'personneldeletedFlag', isNested: true, arrayPath: 'personnel', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Key Personnel: Officers: Name', base: 'personnel', ngModel: 'personnel.officers.officerTypeId', name: 'keyPersOfficersName', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'officers', valuePath: 'codeValue' },
            { label: 'Key Personnel: Officers: Listing Type', base: 'personnel', ngModel: 'personnel.officers.listingType', name: 'keyPersOfficersType', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'officers', valuePath: 'codeValue' },
            //Other Tab
            { label: 'Total Corporate Loan Portfolio Outstanding', base: 'organization', ngModel: 'loanPortfolioOutstanding', name: 'loanPortfolioOutstanding' },
            { label: 'Minimum Loan', base: 'organization', ngModel: 'minLoanHandled', name: 'minLoanHandled' },
            { label: 'Size by Original Cost', base: 'organization', ngModel: 'originalCostLeasingPortfolio', name: 'originalCostLeasingPortfolio' },
            { label: 'Size by Gross Receivable Outstanding', base: 'organization', ngModel: 'grossReceivableLeasingPortfolio', name: 'grossReceivableLeasingPortfolio' },
            { label: 'Average Equity Position in Lease', base: 'organization', ngModel: 'avgEquityLease', name: 'avgEquityLease' },
            { label: 'Primary Types of Equipment Leased', base: 'organization', ngModel: 'primaryEquipementLeased', name: 'primaryEquipementLeased' },
            { label: 'Preferred Terms of Lease', base: 'organization', ngModel: 'preferredLeaseTerm', name: 'preferredLeaseTerm' },
            { label: 'Number of Master Trust Client', base: 'organization', ngModel: 'masterTrustClient', name: 'masterTrustClient' },
            { label: ' Master Trust Assets', base: 'organization', ngModel: 'masterTrustAssets', name: 'masterTrustAssets' },
            { label: 'Reports Required by Clients in How Many Ways After Period End', base: 'organization', ngModel: 'clientReceiveReportInterval', name: 'clientReceiveReportInterval' },
            { label: 'Method of Compensation', base: 'organization', ngModel: 'compensationMethod', name: 'compensationMethod' },
            { label: 'Minimum Deal Size Handled', base: 'organization', ngModel: 'minimumDealSizeHandled', name: 'minimumDealSizeHandled' },
            { label: 'Owner Country', base: 'organization', ngModel: 'ownerCountry', name: 'ownerCountry' },
            { label: 'Private Placement Portfolio', base: 'organization', ngModel: 'privatePlacementPortfolio', name: 'privatePlacementPortfolio' },
            { label: 'Funds Available', base: 'organization', ngModel: 'privatePlacementFunds', name: 'privatePlacementFunds' },
            { label: 'Number of Years in Pensions Management', base: 'organization', ngModel: 'pensionManagementYear', name: 'pensionManagementYear' },
            { label: 'Pensions Assets Under Management', base: 'organization', ngModel: 'pensionAssetsManagement', name: 'pensionAssetsManagement' },
            { label: 'Pension Report Interval', base: 'organization', ngModel: 'pensionReportInterval', name: 'pensionReportInterval', valuePath: 'codeValue' },
            { label: 'Average Deals Closed in 3 years', base: 'organization', ngModel: 'averageDealsClosedThreeYear', name: 'averageDealsClosedThreeYear' },
            { label: 'Size of Deal Handled', base: 'organization', ngModel: 'dealSizeHandled', name: 'dealSizeHandled' },
            { label: 'Client mix: Buyers', base: 'organization', ngModel: 'clientMixBuyer', name: 'clientMixBuyer' },
            { label: 'Client mix: Sellers', base: 'organization', ngModel: 'clientMixSeller', name: 'clientMixSeller' },
            { label: 'Client Representation at One Time: Average Buyer Represented', base: 'organization', ngModel: 'averageBuyerRepresented', name: 'averageBuyerRepresented' },
            { label: 'Client Representation at One Time: Average Seller Represented', base: 'organization', ngModel: 'averageSellerRepresented', name: 'averageSellerRepresented' },
            { label: 'Years Leasing', base: 'organization', ngModel: 'yearsLeasing', name: 'yearsLeasing' },
            { label: 'Short Term Cash Fund', base: 'organization', ngModel: 'flags.shortTermCashFund', name: 'shortTermCashFund', isNested: true, hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Zero Cash Balance', base: 'organization', ngModel: 'flags.zeroCashBalance', name: 'zeroCashBalance', isNested: true, hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },

            { label: 'Specification Type', base: 'chapterSpecification', ngModel: 'chapterSpecification.specificationType', name: 'specificationType', isNested: true, arrayPath: 'chapterSpecification' },
            { label: 'Specification Code', base: 'chapterSpecification', ngModel: 'chapterSpecification.code', name: 'code', valuePath: 'codeValue', isNested: true, arrayPath: 'chapterSpecification' },
            { label: 'Specification Name', base: 'chapterSpecification', ngModel: 'chapterSpecification.name', name: 'chapterSpecificationName', isNested: true, arrayPath: 'chapterSpecification' },
            { label: 'Chapter Specification Listing Type', base: 'chapterSpecification', ngModel: 'chapterSpecification.listingType', name: 'listingType1', valuePath: 'codeValue', isNested: true, arrayPath: 'chapterSpecification' },
            { label: 'Chapter Specification: Active', base: 'chapterSpecification', ngModel: 'chapterSpecification.activeFlag', name: 'chapterSpecificationActive', isNested: true, arrayPath: 'chapterSpecification', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Features Type', base: 'organization', ngModel: 'features.featureType', name: 'featureType', isNested: true, arrayPath: 'features' },
            { label: 'Features', base: 'organization', ngModel: 'features.code', name: 'featuresCode', isNested: true, arrayPath: 'features', valuePath: 'codeValue' },
            { label: 'Features Description', base: 'organization', ngModel: 'features.name', name: 'featuresName', isNested: true, arrayPath: 'features' },
        ],
        'DMMP': [
            //General tab
            { label: 'Organization ID', base: 'organization', ngModel: 'org_id', name: 'org_id' },
            { label: 'Sequence No.', base: 'organization', ngModel: 'sequenceNo', name: 'sequenceNo' },

            { label: 'Classification Code', base: 'organization', ngModel: 'classificationCode', name: 'classificationcode', valuePath: 'codeValue' },

            { label: 'Conduct Business', base: 'organization', ngModel: 'conductBusiness', name: 'conductBusiness' },
            { label: 'Advertising Marketing Budget Related to Direct Marketing (%)', base: 'organization', ngModel: 'directMarketingBudget', name: 'directMarketingBudget' },
            { label: ' Business Description ', base: 'organization', ngModel: 'galleryDescription', name: 'galleryDescription' },
            { label: 'Name', base: 'organization', ngModel: 'name', name: 'name' },
            { label: 'Minor Name', base: 'organization', ngModel: 'shortName', name: 'shortname' },
            { label: 'Sort Major Name', base: 'organization', ngModel: 'sortMajorName', name: 'sortMajorName' },
            { label: 'Established Description', base: 'organization', ngModel: 'established.detail', name: 'detail' },
            { label: 'Established Year', base: 'organization', ngModel: 'established.year', name: 'year' },
            { label: 'Primary Market Served', base: 'organization', ngModel: 'primaryMarket', name: 'primaryMarket', valuePath: 'codeValue' },
            { label: 'Name Suffix', base: 'organization', ngModel: 'nameSuffix', name: 'nameSuffix' },
            { label: 'DMA Code', base: 'organization', ngModel: 'dmaCode', name: 'dmaCode' },
            { label: 'DMA ID', base: 'organization', ngModel: 'dmaId', name: 'dmaID' },
            { label: 'Catalog Available Online', base: 'organization', ngModel: 'flags.catalogOnline', name: 'catalogOnline', isNested: true, hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Direct/Indirect Online Sales', base: 'organization', ngModel: 'onlineSales', name: 'onlinesales', hasOptions: [{ value: 'D', label: 'Direct online sales' }, { value: 'I', label: 'Indirect online sales' }] },
            { label: 'Direct Marketing Ad Budget', base: 'organization', ngModel: 'totalAdvertisingBudget', name: 'totalAdvertisingBudget' },

            { label: 'Former: Names', base: 'organization', ngModel: 'formerNames.name', name: 'formerNames', isNested: true, arrayPath: 'formerNames' },
            { label: 'Former: Start Date', base: 'organization', ngModel: 'formerNames.startDate', name: 'formerStartDate', isNested: true, arrayPath: 'formerNames', isDate: true, dateFormat: 'dS mmmm, yyyy' },
            { label: 'Former: End Date', base: 'organization', ngModel: 'formerNames.endDate', name: 'formerEndDate', isNested: true, arrayPath: 'formerNames', isDate: true, dateFormat: 'dS mmmm, yyyy' },
            { label: 'Former: Show Date', base: 'organization', ngModel: 'formerNames.showDate', name: 'formerShowDate', isNested: true, arrayPath: 'formerNames' },
            { label: 'Listing: Type', base: 'organization', ngModel: 'listingType.listing', name: 'listing', isNested: true, arrayPath: 'listingType', valuePath: 'codeValue' },
            { label: 'Listing: Section', base: 'organization', ngModel: 'listingType.listingSectionCode', name: 'listingTypeSectionCode', isNested: true, arrayPath: 'listingType', valuePath: 'codeValue' },
            { label: 'Listing: Section Number', base: 'organization', ngModel: 'listingType.sectionNumber', name: 'listingTypeSectionNumber', isNested: true, arrayPath: 'listingType' },
            { label: 'Listing: Active', base: 'organization', ngModel: 'listingType.listingActive', name: 'listingTypelistingActive', isNested: true, arrayPath: 'listingType', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            //Added 22/08/2016
            { label: 'Direct Marketing Ad Budget : Name', base: 'organization', ngModel: 'directMarketingBudgetDisbursal.code', name: 'directMarketingBudgetDisbursalCode', isNested: true, arrayPath: 'directMarketingBudgetDisbursal', valuePath: 'codeValue' },
            { label: 'Direct Marketing Ad Budget : Value (%)', base: 'organization', ngModel: 'directMarketingBudgetDisbursal.value', name: 'directMarketingBudgetDisbursalValue', isNested: true, arrayPath: 'directMarketingBudgetDisbursal' },


            //Communication tab
            { label: 'Contact: Phone No.', base: 'organization', ngModel: 'contact.phoneNumbers', name: 'phoneNumbers', isNested: true },
            { label: 'Contact: Toll Free', base: 'organization', ngModel: 'contact.officeNumbers', name: 'officeNumbers', isNested: true },
            { label: 'Contact: Fax', base: 'organization', ngModel: 'contact.faxNumbers', name: 'faxNumbers', isNested: true },
            { label: 'Contact: Primary Email', base: 'organization', ngModel: 'contact.emails.primary', name: 'primary', isNested: true },
            { label: 'Contact: Secondary Email', base: 'organization', ngModel: 'contact.emails.secondary', name: 'secondary', isNested: true },
            { label: 'Contact: Websites', base: 'organization', ngModel: 'contact.websites', name: 'websites', isNested: true },
            { label: 'Contact: Social', base: 'organization', ngModel: 'contact.social', name: 'social', isNested: true },


            { label: 'Address: Type', base: 'organization', ngModel: 'address.addressType', name: 'addressType', isNested: true, arrayPath: 'address', valuePath: 'description' },
            { label: 'Address: Street 1', base: 'organization', ngModel: 'address.street1', name: 'street1', isNested: true, arrayPath: 'address' },
            { label: 'Address: Street 2', base: 'organization', ngModel: 'address.street2', name: 'street2', isNested: true, arrayPath: 'address' },
            { label: 'Address: Post Box', base: 'organization', ngModel: 'address.postBox', name: 'postBoxAddress', isNested: true, arrayPath: 'address' },
            { label: 'Address: City', base: 'organization', ngModel: 'address.cityName', name: 'city', isNested: true, arrayPath: 'address' /*, valuePath: 'codeValue'*/ },
            { label: 'Address: State/Territory', base: 'organization', ngModel: 'address.stateName', name: 'state', isNested: true, arrayPath: 'address' /*, valuePath: 'codeValue'*/ }, {
                label: 'Address: Country',
                base: 'organization',
                ngModel: 'address.countryName',
                name: 'country',
                isNested: true,
                arrayPath: 'address'
                    /*, valuePath: 'codeValue'
                     */
            },
            { label: 'Address: Zip', base: 'organization', ngModel: 'address.zip', name: 'zip', isNested: true, arrayPath: 'address' },
            { label: 'Address: Mailing Index', base: 'organization', ngModel: 'address.mailingIndex', name: 'mailingIndex', isNested: true, arrayPath: 'address', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            //Personnel tab
            { label: 'Personnel: Employees', base: 'organization', ngModel: 'employees', name: 'employees' },
            { label: 'Personnel: ID', base: 'personnel', ngModel: 'personnel.personnel_id', name: 'personnelId', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Sequence No.', base: 'personnel', ngModel: 'personnel.sequenceNumber', name: 'personnelsequenceNumber', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Title', base: 'personnel', ngModel: 'personnel.titleMaster', name: 'titleMaster', isNested: true, arrayPath: 'personnel', valuePath: 'codeValue' },
            // { label: 'Key Personnel: Custom Title', base: 'personnel', ngModel: 'personnel.title', name: 'title', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Prefix', base: 'personnel', ngModel: 'personnel.name.prefix', name: 'prefix', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: First Name', base: 'personnel', ngModel: 'personnel.name.first', name: 'first', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Middle Name', base: 'personnel', ngModel: 'personnel.name.middle', name: 'middle', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Last Name', base: 'personnel', ngModel: 'personnel.name.last', name: 'last', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Suffix', base: 'personnel', ngModel: 'personnel.name.suffix', name: 'Suffix', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Phone No.', base: 'personnel', ngModel: 'personnel.phoneNumbers', name: 'personnelphoneNumbers', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Fax', base: 'personnel', ngModel: 'personnel.faxNumbers', name: 'personnelfaxNumbers', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Primary Email', base: 'personnel', ngModel: 'personnel.email.primary', name: 'personnelemailprimary', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Secondary Email', base: 'personnel', ngModel: 'personnel.email.secondary', name: 'personnelemailsecondary', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Websites', base: 'personnel', ngModel: 'personnel.website', name: 'personnelwebsite', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Active', base: 'personnel', ngModel: 'personnel.activeFlag', name: 'active', isNested: true, arrayPath: 'personnel', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Key Personnel: Chairman', base: 'personnel', ngModel: 'personnel.chairman', name: 'chairman', isNested: true, arrayPath: 'personnel', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Key Personnel: DMMP Contact', base: 'personnel', ngModel: 'personnel.dmmpContact', name: 'dmmpContact', isNested: true, arrayPath: 'personnel', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            //for responsibility code
            { label: 'Key Personnel Responsibility Codes: Code', base: 'personnel', ngModel: 'personnel.responsibilityCodes.responsibilityCode', name: 'keyPersRespCode', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'responsibilityCodes', valuePath: 'codeValue' },
            { label: 'Key Personnel Responsibility Codes: Sdaa Index', base: 'personnel', ngModel: 'personnel.responsibilityCodes.sdaaIndex', name: 'respSdaaIndex', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'responsibilityCodes', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Key Personnel Responsibility Codes: Dca Pub Index', base: 'personnel', ngModel: 'personnel.responsibilityCodes.dcaPubIndex', name: 'respDcaPubIndex', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'responsibilityCodes', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Key Personnel Responsibility Codes: Sda Index', base: 'personnel', ngModel: 'personnel.responsibilityCodes.sdaIndex', name: 'respSdaIndex', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'responsibilityCodes', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Key Personnel Responsibility Codes: Dca Ipc Index', base: 'personnel', ngModel: 'personnel.responsibilityCodes.dcaIpcIndex', name: 'respDcaIpcIndex', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'responsibilityCodes', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Key Personnel Responsibility Codes: Acf Index', base: 'personnel', ngModel: 'personnel.responsibilityCodes.acfIndex', name: 'respAcfIndex', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'responsibilityCodes', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Key Personnel Responsibility Codes: Dca Previous Index', base: 'personnel', ngModel: 'personnel.responsibilityCodes.dcaPreviousIndex', name: 'respDcaPreviousIndex', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'responsibilityCodes', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Key Personnel Responsibility Codes: Iad Index', base: 'personnel', ngModel: 'personnel.responsibilityCodes.iadIndex', name: 'respIadIndex', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'responsibilityCodes', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Key Personnel Responsibility Codes: Iag Index', base: 'personnel', ngModel: 'personnel.responsibilityCodes.iagIndex', name: 'respIagIndex', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'responsibilityCodes', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },

            //other tab

            { label: 'Account Preference', base: 'organization', ngModel: 'accountPreference', name: 'accountPreference' },
            { label: 'Assets', base: 'organization', ngModel: 'assets', name: 'assets' },
            { label: 'Advance Bill Type', base: 'organization', ngModel: 'advanceBillType', name: 'advanceBillType', arrayPath: 'advanceBillType', valuePath: 'codeValue' },
            { label: 'Gross Sales or Billing ($)', base: 'organization', ngModel: 'annualRevenues.wordwide', name: 'grossSellorBilling' },
            { label: 'Annual Mailed', base: 'organization', ngModel: 'annualMailed', name: 'annualMailed' },

            //for Mailing list DMMP
            { label: 'Mailing List: Prefix', base: 'organization', ngModel: 'mailingList.prefix', name: 'mailListPrefix', isNested: true },
            { label: 'Mailing List: First', base: 'organization', ngModel: 'mailingList.first', name: 'mailListFirst', isNested: true },
            { label: 'Mailing List: Middle', base: 'organization', ngModel: 'mailingList.middle', name: 'mailListMiddle', isNested: true },
            { label: 'Mailing List: Last', base: 'organization', ngModel: 'mailingList.last', name: 'mailListLast', isNested: true },
            { label: 'Mailing List: Suffix', base: 'organization', ngModel: 'mailingList.suffix', name: 'mailListSuffix', isNested: true },
            { label: 'Mailing List: Mailing Contact', base: 'organization', ngModel: 'mailingList.MailingContact', name: 'mailListContact', isNested: true },
            { label: 'Mailing List: Extension', base: 'organization', ngModel: 'mailingList.extension', name: 'mailListExtension', isNested: true },
            { label: 'Mailing List: Post Box', base: 'organization', ngModel: 'mailingList.postBox', name: 'mailListPost', isNested: true },
            { label: 'Mailing List: Street1', base: 'organization', ngModel: 'mailingList.street1', name: 'mailListStreet1', isNested: true },
            { label: 'Mailing List: City', base: 'organization', ngModel: 'mailingList.cityName', name: 'mailListCity', isNested: true },
            { label: 'Mailing List: State', base: 'organization', ngModel: 'mailingList.stateName', name: 'mailListState', isNested: true },
            { label: 'Mailing List: Zip', base: 'organization', ngModel: 'mailingList.zip', name: 'mailListZip', isNested: true },
            { label: 'Mailing List: Email', base: 'organization', ngModel: 'mailingList.email', name: 'mailListEmail', isNested: true },
            { label: 'Mailing List: Mail List Available', base: 'organization', ngModel: 'mailingList.mailListAvailableIndex', name: 'mailListAvailable', isNested: true, hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Mailing List: Email List Available', base: 'organization', ngModel: 'mailingList.emailListAvailableIndex', name: 'emailListAvailable', isNested: true, hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },

            { label: 'Notes: Sequence No.', base: 'organization', ngModel: 'notes.sequenceNo', name: 'seqNo', isNested: true, arrayPath: 'notes' },
            { label: 'Notes: Note', base: 'organization', ngModel: 'notes.note', name: 'note', isNested: true, arrayPath: 'notes' },
            { label: 'Notes: Note Type', base: 'organization', ngModel: 'notes.noteType', name: 'print', isNested: true, arrayPath: 'notes' },
            { label: 'Vendors: Sequence No.', base: 'organization', ngModel: 'vendors.sequenceNumber', name: 'sequenceNumber', isNested: true, arrayPath: 'vendors' },
            { label: 'Vendors: Vendor Type', base: 'organization', ngModel: 'vendors.vendorType', name: 'vendorType', isNested: true, arrayPath: 'vendors', valuePath: 'codeValue' },
            { label: 'Vendors: Name', base: 'organization', ngModel: 'vendors.name', name: 'vendorsname', isNested: true, arrayPath: 'vendors' },
            { label: 'Vendors: Post Box', base: 'organization', ngModel: 'vendors.postBox', name: 'VendorspostBox', isNested: true, arrayPath: 'vendors' },
            { label: 'Vendors: Street', base: 'organization', ngModel: 'vendors.street', name: 'street', isNested: true, arrayPath: 'vendors' },
            { label: 'Vendors: City', base: 'organization', ngModel: 'vendors.cityName', name: 'cityName', isNested: true, arrayPath: 'vendors' },
            { label: 'Vendors: State', base: 'organization', ngModel: 'vendors.stateName', name: 'stateName', isNested: true, arrayPath: 'vendors' },
            { label: 'Vendors: Zip', base: 'organization', ngModel: 'vendors.zip', name: 'vendorszip', isNested: true, arrayPath: 'vendors' },

        ],
        'OCD': [
            //General Tab
            { label: 'Classification Code', base: 'organization', ngModel: 'classificationCode', name: 'classificationcode', valuePath: 'codeValue' },
            { label: 'Sequence No.', base: 'organization', ngModel: 'sequenceNo', name: 'orgSequenceNo' },
            { label: 'Name', base: 'organization', ngModel: 'name', name: 'name' },
            { label: 'Parent Organization/Header', base: 'organization', ngModel: 'parentId', name: 'parent', valuePath: 'name' },
            { label: 'Abbreviation Name', base: 'organization', ngModel: 'abbrevationName', name: 'abbrName' },
            { label: 'Status', base: 'organization', ngModel: 'status', name: 'status', valuePath: 'codeValue' },
            { label: 'Organization ID', base: 'organization', ngModel: 'org_id', name: 'org_id' },
            { label: 'EI Number', base: 'organization', ngModel: 'EINumber', name: 'EINumber' },
            { label: 'Placement City', base: 'organization', ngModel: 'placementCity', name: 'placementCity' },
            { label: 'Purpose', base: 'organization', ngModel: 'purpose', name: 'Purpose' },
            { label: 'Cross Reference', base: 'organization', ngModel: 'crossRefId', name: 'crossReference' },
            { label: 'Mailer Type', base: 'organization', ngModel: 'mailerType', name: 'mailerType' },
            { label: 'Sort Major Name', base: 'organization', ngModel: 'sortMajorName', name: 'sortMajorName' },
            { label: 'Latin Name', base: 'organization', ngModel: 'latinName', name: 'latinNameOCD' },
            { label: 'Square Miles', base: 'organization', ngModel: 'squareMiles', name: 'squMiles' },
            { label: 'Supplement Type', base: 'organization', ngModel: 'supplementType', name: 'supplementTypeOCD' },
            { label: 'AkaDba Index', base: 'organization', ngModel: 'akaDbaIndex', name: 'akaDbaIndex' },
            { label: 'AkaDba Name', base: 'organization', ngModel: 'akaDbaName', name: 'akaDbaName' },
            { label: 'Established Year', base: 'organization', ngModel: 'established.year', name: 'establishedYear' },
            { label: 'Established Description', base: 'organization', ngModel: 'established.detail', name: 'detail' },
            { label: 'Outstanding Issues', base: 'organization', ngModel: 'outstandingIssues', name: 'outstandingIssues' },
            { label: 'Locale', base: 'organization', ngModel: 'locale', name: 'locale' },
            { label: 'Tax Exempt', base: 'organization', ngModel: 'flags.taxExempt', name: 'taxExempt', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Gift Shop', base: 'organization', ngModel: 'flags.giftShop', name: 'giftShop', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Name Suppress', base: 'organization', ngModel: 'flags.nameSuppress', name: 'nameSuppress', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Wrap Index', base: 'organization', ngModel: 'flags.wrapIndex', name: 'wrapIndex', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Former: Names', base: 'organization', ngModel: 'formerNames.name', name: 'formerNames', isNested: true, arrayPath: 'formerNames' },
            { label: 'Former : Start Date', base: 'organization', ngModel: 'formerNames.startDate', name: 'formerStartDate', isNested: true, arrayPath: 'formerNames', isDate: true, dateFormat: 'dS mmmm, yyyy' },
            { label: 'Former : End Date', base: 'organization', ngModel: 'formerNames.endDate', name: 'formerEndDate', isNested: true, arrayPath: 'formerNames', isDate: true, dateFormat: 'dS mmmm, yyyy' },
            { label: 'Parish Shrine: Ethnicity Type1', base: 'organization', ngModel: 'parishShrine.ethnicityType1', name: 'ethnicityType1', valuePath: 'codeValue', isNested: true },
            { label: 'Parish Shrine: Ethnicity Text1', base: 'organization', ngModel: 'parishShrine.ethnicityText1', name: 'ethnicityText1' },
            { label: 'Parish Shrine: Ethnicity Type2', base: 'organization', ngModel: 'parishShrine.ethnicityType2', name: 'ethnicityType2', valuePath: 'codeValue', isNested: true },
            { label: 'Parish Shrine: Ethnicity Text2', base: 'organization', ngModel: 'parishShrine.ethnicityText2', name: 'ethnicityText2' },
            { label: 'Parish Shrine: Parish Status', base: 'organization', ngModel: 'parishShrine.parishStatus', name: 'parishStatus', valuePath: 'codeValue', isNested: true },
            { label: 'Parish Shrine: Cemeteries', base: 'organization', ngModel: 'parishShrine.cemeteries', name: 'cemeteries' },
            { label: 'Parish Shrine: Joint Cemeteries', base: 'organization', ngModel: 'parishShrine.jointCemeteries', name: 'jointCemeteries', isNested: true },
            { label: 'Parish Shrine:  Spanish Mass', base: 'organization', ngModel: 'parishShrine.spanishMass', name: 'spanishMass' },
            { label: 'Diocese: Diocese Type', base: 'organization', ngModel: 'dioceseType', name: 'dioceseType', valuePath: 'codeValue' },
            { label: 'Diocese: Dio Province', base: 'organization', ngModel: 'dioProvince', name: 'dioProvince', valuePath: 'codeValue' },
            { label: 'Diocese: Archdiocese', base: 'organization', ngModel: 'flags.archdiocese', name: 'archdiocese', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Religious Order: Religious Type', base: 'organization', ngModel: 'religiousOrder.religiousType', name: 'religiousType', isNested: true, valuePath: 'codeValue' },
            { label: 'Religious Order: Secondary Name', base: 'organization', ngModel: 'religiousOrder.secondaryName', name: 'secondaryName', isNested: true },
            { label: 'Religious Order: Tertiary Name', base: 'organization', ngModel: 'religiousOrder.tertiaryName', name: 'tertiaryName', isNested: true },
            { label: 'Religious Order: Kennedy Id', base: 'organization', ngModel: 'religiousOrder.kennedyId', name: 'kennedyId', isNested: true },
            { label: 'Religious Order: Pontifical Or Diocese', base: 'organization', ngModel: 'religiousOrder.pontificalOrDiocese', name: 'religiousOrderDio', isNested: true },
            { label: 'Religious Order: Initials', base: 'organization', ngModel: 'religiousOrder.initials', name: 'initials', isNested: true },
            { label: 'School: School Type', base: 'organization', ngModel: 'school.schoolTypeName', name: 'schoolTypeName', isNested: true },
            { label: 'School: Grade Type Start', base: 'organization', ngModel: 'school.gradeTypeStart', name: 'gradeTypeStart', isNested: true, valuePath: 'codeValue' },
            { label: 'School: Grade Type End', base: 'organization', ngModel: 'school.gradeTypeEnd', name: 'gradeTypeEnd', isNested: true, valuePath: 'codeValue' },
            { label: 'School: Consolidated', base: 'organization', ngModel: 'school.consolidated', name: 'consolidated', isNested: true, hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'School: College', base: 'organization', ngModel: 'school.college', name: 'college', isNested: true, hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Header:  Pre Text', base: 'organization', ngModel: 'header.preText', name: 'headerPreText', isNested: true },
            { label: 'Header:  State', base: 'organization', ngModel: 'header.state', name: 'headerState', isNested: true },
            { label: 'Header:  County', base: 'organization', ngModel: 'header.county', name: 'headerCounty', isNested: true },
            { label: 'Header: Bold', base: 'organization', ngModel: 'header.bold', name: 'headerBold', isNested: true, hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Header: Italic', base: 'organization', ngModel: 'header.italic', name: 'headerItalic', isNested: true, hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Header: Center', base: 'organization', ngModel: 'header.center', name: 'headerCenter', isNested: true, hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Header: Large Font', base: 'organization', ngModel: 'header.largeFont', name: 'headerLargeFont', isNested: true, hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Header: Underline', base: 'organization', ngModel: 'header.underline', name: 'headerUnderLine', isNested: true, hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            //communication Tab
            { label: 'Organization Office: Organization Name', base: 'organization', ngModel: 'organizationOffice.name', name: 'organizationOfficeName', isNested: true },
            { label: 'Organization Office: Additional Information', base: 'organization', ngModel: 'organizationOffice.additionalInfo', name: 'organizationOfficeAdditionalInformation', isNested: true },
            { label: 'Contact: Sequence No.', base: 'organization', isNested: true, arrayPath: 'ocdContact', ngModel: 'ocdContact.sequenceNo', name: 'ocdSeqNo' },
            { label: 'Contact: Address Sequence No.', base: 'organization', isNested: true, arrayPath: 'ocdContact', ngModel: 'ocdContact.addressSequenceNo', name: 'addressSeqNo' },
            { label: 'Contact: Contact Type', base: 'organization', isNested: true, arrayPath: 'ocdContact', ngModel: 'ocdContact.contactType', name: 'contactName', valuePath: 'codeValue' },
            { label: 'Contact: Extension', base: 'organization', isNested: true, arrayPath: 'ocdContact', ngModel: 'ocdContact.extension', name: 'extension' },
            { label: 'Contact: Contact Details', base: 'organization', isNested: true, arrayPath: 'ocdContact', ngModel: 'ocdContact.contactDetails', name: 'contactDetails' },
            { label: 'Contact:   Note', base: 'organization', isNested: true, arrayPath: 'ocdContact', ngModel: 'ocdContact.note', name: 'contactNote' },
            { label: 'Contact: Mail Option ', base: 'organization', isNested: true, arrayPath: 'ocdContact', ngModel: 'ocdContact.mailOptOut', name: 'contactMailOptOut', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Contact:  Nixie Index', base: 'organization', isNested: true, arrayPath: 'ocdContact', ngModel: 'ocdContact.nixieIndex', name: 'contactNixieIndex', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Contact:  Non USA', base: 'organization', isNested: true, arrayPath: 'ocdContact', ngModel: 'ocdContact.nonUSA', name: 'contactNonUSA', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Contact:  Print', base: 'organization', isNested: true, arrayPath: 'ocdContact', ngModel: 'ocdContact.printFlag', name: 'contactPrintFlag', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Address: Sequence No.', base: 'organization', ngModel: 'address.sequenceNo', name: 'sequenceNo', isNested: true, arrayPath: 'address' },
            { label: 'Address: Sort Sequence No.', base: 'organization', ngModel: 'address.sortSequenceNo', name: 'sortSequenceNo', isNested: true, arrayPath: 'address' },
            { label: 'Address: Type', base: 'organization', ngModel: 'address.addressType', name: 'addressType', isNested: true, arrayPath: 'address', valuePath: 'description' },
            { label: 'Address: Organization Name', base: 'organization', ngModel: 'address.organizationName', name: 'addresOrg', isNested: true, arrayPath: 'address' },
            { label: 'Address: Header', base: 'organization', ngModel: 'address.header', name: 'header', isNested: true, arrayPath: 'address' },
            { label: 'Address: Street 1', base: 'organization', ngModel: 'address.street1', name: 'street1', isNested: true, arrayPath: 'address' },
            { label: 'Address: Street 2', base: 'organization', ngModel: 'address.street2', name: 'street2', isNested: true, arrayPath: 'address' },
            { label: 'Address: City', base: 'organization', ngModel: 'address.cityName', name: 'city', isNested: true, arrayPath: 'address' /*, valuePath: 'codeValue'*/ },
            { label: 'Address: State/Territory', base: 'organization', ngModel: 'address.stateName', name: 'state', isNested: true, arrayPath: 'address' },
            { label: 'Address: Country', base: 'organization', ngModel: 'address.countryName', name: 'country', isNested: true, arrayPath: 'address' },
            { label: 'Address: County', base: 'organization', ngModel: 'address.countyName', name: 'county', isNested: true, arrayPath: 'address' },
            { label: 'Address: Zip', base: 'organization', ngModel: 'address.zip', name: 'zip', isNested: true, arrayPath: 'address' },
            { label: 'Address: Contact Person', base: 'organization', ngModel: 'address.contactPerson', name: 'personcontact', isNested: true, arrayPath: 'address' },
            { label: 'Address: Mail Option', base: 'organization', ngModel: 'address.mailOptOut', name: 'mailOptOut', isNested: true, arrayPath: 'address', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Address: Mailing Index', base: 'organization', ngModel: 'address.mailingIndex', name: 'mailingIndex', isNested: true, arrayPath: 'address', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Address: Nixie Index', base: 'organization', ngModel: 'address.nixieIndex', name: 'nixieIndex', isNested: true, arrayPath: 'address', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Address: Non USA', base: 'organization', ngModel: 'address.nonUSA', name: 'nonUSA', isNested: true, arrayPath: 'address', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },

            { label: 'Address: Print', base: 'organization', ngModel: 'address.printFlag', name: 'printFlag', isNested: true, arrayPath: 'address', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },

            //Key personnel
            { label: 'Key Personnel: Person Type', base: 'personnel', ngModel: 'personnel.personType', name: 'personType', isNested: true, arrayPath: 'personnel', valuePath: 'codeValue' },
            { label: 'Key Personnel: Custom Title', base: 'personnel', ngModel: 'personnel.title', name: 'title', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Prefix', base: 'personnel', ngModel: 'personnel.name.prefix', name: 'prefix', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: First Name', base: 'personnel', ngModel: 'personnel.name.first', name: 'fName', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Middle Name', base: 'personnel', ngModel: 'personnel.name.middle', name: 'mName', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Last Name', base: 'personnel', ngModel: 'personnel.name.last', name: 'lName', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Suffix', base: 'personnel', ngModel: 'personnel.name.suffix', name: 'Suffix', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Status', base: 'personnel', ngModel: 'personnel.status', name: 'Status', isNested: true, arrayPath: 'personnel', valuePath: 'codeValue' },
            { label: 'Key Personnel: Home Nation', base: 'personnel', ngModel: 'personnel.homeNation', name: 'homeNation', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Religious Order Initials', base: 'personnel', ngModel: 'personnel.religiousOrderInitials', name: 'religiousOrderInitials', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Home Diocese', base: 'personnel', ngModel: 'personnel.homeDiocese', name: 'homeDiocese', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Websites', base: 'personnel', ngModel: 'personnel.webSite', name: 'keyWebSite', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Retired', base: 'personnel', ngModel: 'personnel.retired', name: 'retired', isNested: true, arrayPath: 'personnel', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Key Personnel: Very', base: 'personnel', ngModel: 'personnel.very', name: 'Very', isNested: true, arrayPath: 'personnel', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Key Personnel: Died-Day', base: 'personnel', ngModel: 'personnel.died.day', name: 'diedDay', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Died-Month', base: 'personnel', ngModel: 'personnel.died.month', name: 'diedMonth', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Died-Year', base: 'personnel', ngModel: 'personnel.died.year', name: 'diedYear', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Ordination-Day', base: 'personnel', ngModel: 'personnel.ordination.ordination_day', name: 'ordinationDate', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Ordination-Month', base: 'personnel', ngModel: 'personnel.ordination.ordination_month', name: 'ordinationMonth', isNested: true, arrayPath: 'personnel' },
            { label: 'Key Personnel: Ordination-Year', base: 'personnel', ngModel: 'personnel.ordination.ordination_year', name: 'ordinationYear', isNested: true, arrayPath: 'personnel' },

            // key Personnel : Contact
            { label: 'Key Personnel: Contact: Sequence No.', base: 'personnel', ngModel: 'personnel.ocdContact.sequenceNo', name: 'keyPersContactSequencNo', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'ocdContact' },
            { label: 'Key Personnel: Contact: Address Sequence No.', base: 'personnel', ngModel: 'personnel.ocdContact.addressSequenceNo', name: 'keyPersContactAddressSequencNo', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'ocdContact' },
            { label: 'Key Personnel: Contact: Contact Type', base: 'personnel', ngModel: 'personnel.ocdContact.contactType', name: 'keyPersContactType', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'ocdContact', valuePath: 'codeValue' },
            { label: 'Key Personnel: Contact: Extension', base: 'personnel', ngModel: 'personnel.ocdContact.extension', name: 'keyPersExtension', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'ocdContact' },
            { label: 'Key Personnel: Contact: Contact Details', base: 'personnel', ngModel: 'personnel.ocdContact.contactDetails', name: 'keyPersContactDetails', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'ocdContact' },
            { label: 'Key Personnel: Contact: Note', base: 'personnel', ngModel: 'personnel.ocdContact.note', name: 'keyPersNote', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'ocdContact' },
            { label: 'Key Personnel: Contact: Mail Option', base: 'personnel', ngModel: 'personnel.ocdContact.mailOptOut', name: 'keyPersMailOpt', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'ocdContact', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Key Personnel: Contact: Nixie Index', base: 'personnel', ngModel: 'personnel.ocdContact.nixieIndex', name: 'keyPersNixieIndex', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'ocdContact', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Key Personnel: Contact: Non USA', base: 'personnel', ngModel: 'personnel.ocdContact.nonUSA', name: 'keyPersNonUsa', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'ocdContact', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Key Personnel: Contact: Print', base: 'personnel', ngModel: 'personnel.ocdContact.printFlag', name: 'keyPersPrint', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'ocdContact', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            // Key Personnel : Address
            { label: 'Key Personnel: Address: Address Type', base: 'personnel', ngModel: 'personnel.address.addressType', name: 'keyPersAddressType', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'address', valuePath: 'description' },
            { label: 'Key Personnel: Address: Street 1', base: 'personnel', ngModel: 'personnel.address.street1', name: 'keyPersStreet1', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'address' },
            { label: 'Key Personnel: Address: Street 2', base: 'personnel', ngModel: 'personnel.address.street2', name: 'keyPersStreet2', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'address' },
            { label: 'Key Personnel: Address: City', base: 'personnel', ngModel: 'personnel.address.cityName', name: 'keyPersCity', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'address' },
            { label: 'Key Personnel: Address: State', base: 'personnel', ngModel: 'personnel.address.stateName', name: 'keyPersState', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'address' },
            { label: 'Key Personnel: Address: Country', base: 'personnel', ngModel: 'personnel.address.countryName', name: 'keyPersCountry', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'address' },
            { label: 'Key Personnel: Address: County', base: 'personnel', ngModel: 'personnel.address.countyName', name: 'keyPersCounty', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'address' },
            { label: 'Key Personnel: Address: Zip', base: 'personnel', ngModel: 'personnel.address.zip', name: 'keyPersZip', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'address' },

            // Key Personnel : Degree
            { label: 'Key Personnel: Degree: Sequence No.', base: 'personnel', ngModel: 'personnel.ocdDegree.sequenceNo', name: 'keyPersSequencNo', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'ocdDegree' },
            { label: 'Key Personnel: Degree: Degree Type', base: 'personnel', ngModel: 'personnel.ocdDegree.degreeType', name: 'keyPersdegreeType', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'ocdDegree', valuePath: 'codeValue' },
            { label: 'Key Personnel: Degree: Other Degree', base: 'personnel', ngModel: 'personnel.ocdDegree.otherDegree', name: 'keyPersOtherdegreeType', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'ocdDegree' },

            //Key Personnel Notes
            { label: 'Key Personnel: Notes: Sequence No.', base: 'personnel', ngModel: 'personnel.notes.sequenceNo', name: 'keyPersNotesSequencNo', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'notes' },
            { label: 'Key Personnel: Notes: Note Type ', base: 'personnel', ngModel: 'personnel.notes.noteType', name: 'keyPersNotesNoyeType', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'notes' },
            { label: 'Key Personnel: Notes: Note', base: 'personnel', ngModel: 'personnel.notes.note', name: 'keyPersNotesNote', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'notes' },
            { label: 'Key Personnel: Notes: Italic', base: 'personnel', ngModel: 'personnel.notes.italic', name: 'keyPersNotesItalic', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'notes', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Key Personnel: Notes: Bold', base: 'personnel', ngModel: 'personnel.notes.bold', name: 'keyPersBold', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'notes', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Key Personnel: Notes: Small Cap', base: 'personnel', ngModel: 'personnel.notes.smallCap', name: 'keyPersSmallCap', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'notes', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },

            // Key personnel Assignment
            { label: 'Key Personnel: Assignment: Sequence No.', base: 'personnel', ngModel: 'personnel.assignment.sequenceNo', name: 'keyPersAssignSequencNo', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'assignment' },
            { label: 'Key Personnel: Assignment: Assign Type', base: 'personnel', ngModel: 'personnel.assignment.assignTypeId', name: 'keyPersAssignType', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'assignment', valuePath: 'codeValue' },
            { label: 'Key Personnel: Assignment: Title', base: 'personnel', ngModel: 'personnel.assignment.title', name: 'keyPersAssignTitle', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'assignment' },
            { label: 'Key Personnel: Assignment: Response', base: 'personnel', ngModel: 'personnel.assignment.response', name: 'keyPersAssignResponse', isNested: true, arrayPath: 'personnel', nestedArrayPath: 'assignment', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            //other Tab
            { label: 'Sales: Name1', base: 'organization', ngModel: 'sales.name1', name: 'name1', isNested: true },
            { label: 'Sales: Name2', base: 'organization', ngModel: 'sales.name2', name: 'name2', isNested: true },
            { label: 'Sales: Nixie', base: 'organization', ngModel: 'sales.nixie', name: 'nixie', isNested: true, hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Legal Titles: Sequence No.', base: 'organization', ngModel: 'legalTitles.sequenceNo', name: 'legalsequenceNo', isNested: true, arrayPath: 'legalTitles' },
            { label: 'Legal Titles: Legal Title', base: 'organization', ngModel: 'legalTitles.legalTitle', name: 'legalTitle', isNested: true, arrayPath: 'legalTitles' },
            { label: 'Legal Titles: City', base: 'organization', ngModel: 'legalTitles.cityName', name: 'legalCity', isNested: true, arrayPath: 'legalTitles' },
            { label: 'Legal Titles: State', base: 'organization', ngModel: 'legalTitles.stateName', name: 'legalState', isNested: true, arrayPath: 'legalTitles' },
            { label: 'Features: Sequence No.', base: 'organization', ngModel: 'features.sequenceNo', name: 'ocdFeaturessequenceNo', isNested: true, arrayPath: 'features' },
            { label: 'Features: Features Type', base: 'organization', ngModel: 'features.featureType', name: 'ocdFeatureType', isNested: true, arrayPath: 'features' },
            { label: 'Features: Name', base: 'organization', ngModel: 'features.name', name: 'ocdFeaturesName', isNested: true, arrayPath: 'features' },
            { label: 'Notes: Sequence No.', base: 'organization', ngModel: 'notes.sequenceNo', name: 'notesequenceNo', isNested: true, arrayPath: 'notes' },
            { label: 'Notes: Note Type', base: 'organization', ngModel: 'notes.noteType', name: 'notenoteType', isNested: true, arrayPath: 'notes' },
            { label: 'Notes: Note ', base: 'organization', ngModel: 'notes.note', name: 'note', isNested: true, arrayPath: 'notes' },
            { label: 'Notes: Italic', base: 'organization', ngModel: 'notes.italic', name: 'italic', isNested: true, arrayPath: 'notes', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Notes: Bold', base: 'organization', ngModel: 'notes.bold', name: 'bold', isNested: true, arrayPath: 'notes', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Notes: Small Cap', base: 'organization', ngModel: 'notes.smallCap', name: 'smallCap', isNested: true, arrayPath: 'notes', hasOptions: [{ value: true, label: 'YES' }, { value: false, label: 'NO' }] },
            { label: 'Statistics: Sequence No. ', base: 'organization', ngModel: 'statistic.sequenceNo', name: 'statisticsequenceNo', isNested: true, arrayPath: 'statistic' },
            { label: 'Statistics: Statistic Type ', base: 'organization', ngModel: 'statistic.statisticTypeName', name: 'statistictype', isNested: true, arrayPath: 'statistic' },
            { label: 'Statistics: Statistic Category ', base: 'organization', ngModel: 'statistic.statisticCategory', name: 'statisticcategory', isNested: true, arrayPath: 'statistic' },
            { label: 'Statistics: Statistic Value ', base: 'organization', ngModel: 'statistic.value', name: 'statisticvalue', isNested: true, arrayPath: 'statistic' },
            { label: 'Statistics: Statistic Text ', base: 'organization', ngModel: 'statistic.text', name: 'statistictext', isNested: true, arrayPath: 'statistic' },
            { label: 'Statistics: Statistic Year ', base: 'organization', ngModel: 'statistic.year', name: 'statisticyear', isNested: true, arrayPath: 'statistic' },
        ]
    };
}
