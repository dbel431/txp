angular.module('directory.dataDownload', [
        'directory'
    ])
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $stateProvider.state('portal.directory.dataDownload', {
                url: '/data-download',
                templateUrl: '/views/directory.data-download.tpl.html',
                controller: 'DirectoryDataDownloadController'
            }).state('portal.directory.dataDownload.list', {
                url: '/list',
                templateUrl: '/views/directory.data-download.list.html',
                controller: 'DirectoryDataDownloadListController'
            }).state('portal.directory.dataDownload.add', {
                url: '/add',
                templateUrl: '/views/directory.data-download.add.html',
                controller: 'DirectoryDataDownloadAddController'
            }).state('portal.directory.dataDownload.edit', {
                url: '/edit/:id',
                templateUrl: '/views/directory.data-download.edit.html',
                resolve: {
                   'DataDownload': [
                        'DirectoryDataDownloadService',
                        '$stateParams',
                        function(DirectoryDataDownloadService, $stateParams) {
                            return DirectoryDataDownloadService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ],
                },
                controller: 'DirectoryDataDownloadEditController'
            })
        }
    ]);
