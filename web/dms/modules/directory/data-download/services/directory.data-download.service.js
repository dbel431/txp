angular.module('directory').service('DirectoryDataDownloadService', [
    '$rootScope',
    '$localStorage',
    '$resource',
    function($rootScope, $localStorage, $resource) {
        return $resource(API_PATH + '/directory/data-download', {}, {
            get: {
                method: 'GET',
                isArray: true
            },
            dataTable: {
                method: 'POST',
                url: API_PATH + '/directory/data-download/data-table',
                transformRequest: function(data, headerGetter) {
                    var headers = headerGetter();
                    var newData = {
                        dataTableQuery: data,
                        conditions: {
                            'created.by': $rootScope.currentUser._id,
                            'data.directoryId': $localStorage['DMS-DIRECTORY']._id
                        }
                    }
                    return JSON.stringify(newData);
                }
            },
            forceStartAllJobs: {
                url: API_PATH + '/directory/data-download/force-start-all-jobs',
                method: 'POST'
            },
            update: {
                method: 'PUT'
            }
        });
    }
]);
