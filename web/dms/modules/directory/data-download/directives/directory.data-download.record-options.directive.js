angular.module('directory').directive('directoryDataDownloadRecordOptions', function() {
    return {
        scope: {
            'selectedRecordList': '=',
            'toggleAllRecords': '=',
            'isActionPermitted': '=',
            'openDeleteModal': '=',
            'allChecked': '=',
        },
        restrict: 'EA',
        templateUrl: '/views/directory.data-download.record-options.tpl.html',
        replace: true
    };
});