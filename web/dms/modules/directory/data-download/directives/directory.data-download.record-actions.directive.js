angular.module('directory').directive('directoryDataDownloadRecordActions', [
    '$rootScope',
    '$localStorage',
    function($rootScope, $localStorage) {
        return {
            scope: {
                'recordId': '=',
                'openDeleteModal1': '=',
            },
            restrict: 'EA',
            templateUrl: '/views/directory.data-download.record-actions.tpl.html',
            replace: true,
            link: function(scope, elt, attb) {
                scope.isActionPermitted = $rootScope.isActionPermitted;
            }
        };
    }
]);
