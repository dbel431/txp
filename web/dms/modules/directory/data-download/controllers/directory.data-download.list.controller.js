angular.module('directory.dataDownload')
    .controller('DirectoryDataDownloadListController', [
        '$rootScope',
        '$localStorage',
        '$scope',
        '$state',
        '$compile',
        '$uibModal',
        'DTOptionsBuilder',
        'DTColumnBuilder',
        'DirectoryDataDownloadService',
        'CoreFileDownloadService',
        function($rootScope, $localStorage, $scope, $state, $compile, $uibModal, DTOptionsBuilder, DTColumnBuilder, DirectoryDataDownloadService, CoreFileDownloadService) {

            $rootScope.page = {
                name: $localStorage['DMS-DIRECTORY'].description + ': Data Downloads Requests',
                actions: {
                    add: {
                        object: 'directory.dataDownload.add',
                        allowed: function() {
                            return $rootScope.isActionPermitted(this.object);
                        },
                        label: 'New Request'
                    },
                    export: {
                        object: 'directory.organization.exportToExcel',
                        allowed: function() {
                            return $rootScope.isActionPermitted(this.object);
                        },
                        ngClick: function() {
                            $scope.openExportToExcelModal();
                        }
                    }
                }
            };

            $scope.forceStartAllJobs = function() {
                DirectoryDataDownloadService.forceStartAllJobs(function() {
                    showMesssages([{
                        type: 'success',
                        message: 'All jobs started',
                        header: 'Batch Jobs'
                    }]);
                });
            };

            $scope.selectedDataDownloadRequests = [];
            $scope.selectedDataDownloadRequestRecords = {};
            $scope.dtData = [];
            $scope.toggleAllRecords = function(select) {
                for (var i = $scope.dtData.length - 1; i >= 0; i--) {
                    $scope.dtData[i].checked = select || false;
                    if (select) {
                        if ($scope.selectedDataDownloadRequests.indexOf($scope.dtData[i]._id) < 0) {
                            $scope.selectedDataDownloadRequests.push($scope.dtData[i]._id);
                            $scope.selectedDataDownloadRequestRecords[$scope.dtData[i]._id] = $scope.dtData[i];
                        }
                    } else if ($scope.selectedDataDownloadRequests.indexOf($scope.dtData[i]._id) >= 0) {
                        if ($scope.selectedDataDownloadRequests.indexOf($scope.dtData[i]._id) >= 0) {
                            $scope.selectedDataDownloadRequests.splice($scope.selectedDataDownloadRequests.indexOf($scope.dtData[i]._id), 1);
                            delete $scope.selectedDataDownloadRequestRecords[$scope.dtData[i]._id];
                        }
                    }
                }
            };
            $scope.toggleRecord = function(id) {
                for (var i = $scope.dtData.length - 1; i >= 0; i--) {
                    if ($scope.dtData[i]._id == id) {
                        if ($scope.dtData[i].checked) {
                            if ($scope.selectedDataDownloadRequests.indexOf(id) < 0) {
                                $scope.selectedDataDownloadRequests.push(id);
                                $scope.selectedDataDownloadRequestRecords[id] = $scope.dtData[i];
                            }
                        } else {
                            if ($scope.selectedDataDownloadRequests.indexOf(id) >= 0) {
                                $scope.selectedDataDownloadRequests.splice($scope.selectedDataDownloadRequests.indexOf(id), 1);
                                delete $scope.selectedDataDownloadRequestRecords[id];
                            }
                        }
                        break;
                    }
                }
            };
            $scope.allChecked = function() {
                for (var i = $scope.dtData.length - 1; i >= 0; i--) {
                    if (!$scope.dtData[i].checked) return false;
                }
                return true;
            };
            $scope.isActionPermitted = $rootScope.isActionPermitted;



            $scope.serviceCallFunction = function(filename) {
                console.log(filename);
                return CoreFileDownloadService.get({
                    filename: filename
                }, function(res) {
                    downloadFile(API_EXPORTS_PATH + "/" + res.filename);
                }, function(err) {
                    console.log(err);
                    showMessages([{
                        type: 'error',
                        message: 'File could not be downloaded!',
                        header: 'File Download'
                    }]);
                });
            };

            var columnDefs = [{
                title: '<directory-data-download-record-options is-action-permitted="isActionPermitted" open-delete-modal="openDeleteModal" selected-record-list="selectedDataDownloadRequests" dt-data="dtData" all-checked="allChecked" toggle-all-records="toggleAllRecords"></directory-data-download-record-options>',
                orderable: false,
                class: 'text-center',
                width: '1%',
                data: '_id',
                render: function(id) {
                    return '<directory-data-download-record-select dt-data="dtData" selected-record-list="selectedDataDownloadRequests" record-id="' + id + '" toggle-record="toggleRecord"></directory-data-download-record-select>';
                }
            }, {
                title: 'Name',
                width: '25%',
                name: 'name',
                data: 'name',
                render: function(name) {
                    return name || "";
                }
            }, {
                title: 'Description',
                width: '25%',
                name: 'description',
                data: 'description',
                render: function(description) {
                    return description || "";
                }
            }, {
                title: 'Request Date',
                width: '10%',
                name: 'created',
                data: 'created.at',
                render: function(created) {
                    return created ? $rootScope.to_date_format(created, 'jS M Y, g:i:s a') : '' || "";
                }
            }, {
                title: 'Completed Date',
                width: '10%',
                name: 'completedAt',
                data: 'completedAt',
                render: function(completedAt) {
                        return completedAt ? $rootScope.to_date_format(completedAt, 'jS M Y, g:i:s a') : '' || "";
                    }
                    // }, {
                    //     title: 'Active',
                    //     width: '3%',
                    //     name: 'active',
                    //     data: 'active',
                    //     render: function(active) {

                //         return active ? "Yes" : "No";
                //     }
                // }, {
                //     title: 'Running',
                //     width: '3%',
                //     name: 'running',
                //     data: 'running',
                //     render: function(running) {
                //         return running ? "Yes" : "No";
                //     }

            }, {
                title: 'Status',
                width: '10%',
                name: 'status',
                data: 'running',
                render: function(running) {
                    return running;
                }
            }, {
                title: 'File for Download',
                width: '10%',
                name: 'result',
                data: 'result',
                render: function(result) {
                    return (result && result.filename) ? '<a href="javascript:void(0)" class="downLink" ng-click="serviceCallFunction(\'' + result.filename + '\')"><i class="fa fa-download"></i> Download File</a>' : '';
                }

            }, {
                title: 'Actions',
                width: '9%',
                class: 'text-center',
                orderable: false,
                data: '_id',
                render: function(id) {
                    return '<directory-data-download-record-actions open-delete-modal1="openDeleteModal1" record-id="\'' + id + '\'"></directory-data-download-record-actions>';
                }
            }];

            var filterDefs = {
                '1': {
                    type: 'text'
                },
                '2': {
                    type: 'text'
                },
                '5': {
                    type: 'select',
                    values: [{
                        label: 'Idle',
                        value: 'idle'
                    }, {
                        label: 'Completed',
                        value: 'completed'
                    }, {
                        label: 'Running',
                        value: 'running'
                    }]
                },
                // '6': {
                //     type: 'select',
                //     values: [{
                //         label: 'Yes',
                //         value: true
                //     }, {
                //         label: 'No',
                //         value: false
                //     }]
                // },
                // '7': {
                //     type: 'select',
                //     values: [{
                //         label: 'Yes',
                //         value: true
                //     }, {
                //         label: 'No',
                //         value: false
                //     }]
                // }
            };

            $rootScope.openDeleteModal = function() {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/directory.data-download.remove.html',
                    resolve: {
                        'DataDownload': [
                            'DirectoryDataDownloadService',
                            function(DirectoryDataDownloadService) {
                                return DirectoryDataDownloadService.get({
                                    id: $scope.selectedDataDownloadRequests
                                }).$promise;
                            }
                        ]
                    },
                    controller: 'DataDownloadRemoveController'
                });
                modalInstance.result.then(function(messages) {
                    var success = 0,
                        error = 0;
                    for (var i in messages) {
                        if (messages[i].type == 'success') success++;
                        if (messages[i].type == 'error') error++;
                    }
                    messages = [];
                    if (success > 0) messages.push({
                        type: 'success',
                        header: 'Remove Data Download Request',
                        message: success + " record(s) removed successfully!"
                    });
                    if (error > 0) messages.push({
                        type: 'error',
                        header: 'Remove Data Download Request',
                        message: error + " record(s) could not be removed successfully!"
                    });
                    if (messages.length > 0) showMessages(messages);
                    $state.reload();
                });
            };
            $scope.openDeleteModal1 = function(recordId) {
                $scope.toggleAllRecords(false);
                $scope.selectedDataDownloadRequests = [recordId];
                $rootScope.openDeleteModal();
            };

            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withPaginationType('full_numbers')
                // .withOption('scrollY', '500px')
                // .withOption('scrollCollapse', true)
                // .withOption('lengthMenu', [100, 500, 1000, 2000, 3000, 5000, 10000, "All"])
                .withOption('order', [1, 'asc'])
                // .withOption('orderFixed', [0, 'asc'])
                .withOption('ajax', DirectoryDataDownloadService.dataTable)
                .withDataProp('data')
                .withOption('oLanguage', {
                    "sEmptyTable": "No matching records found"
                })
                .withOption('responsive', true)
                // .withOption('sDom', '<fl>rt<"clear"i><"F"p>')
                .withOption('processing', true)
                .withOption('serverSide', true)
                // .withFixedHeader({
                //   header: true
                // })
                .withOption('drawCallback', function(settings) {
                    var listOfRecords = this.api().data();
                    $scope.dtData = [];
                    for (var i = listOfRecords.length - 1; i >= 0; i--) {
                        $scope.dtData[i] = (listOfRecords[i]);
                        $scope.dtData[i].checked = ($scope.selectedDataDownloadRequests.indexOf($scope.dtData[i]._id) >= 0);
                    }
                    $(window).scrollTop(0); // $('body').scrollTop(0);
                    $('[datatable]').css('width', '100%');
                    $compile('directory-data-download-record-select')($scope);
                    $compile('directory-data-download-record-options')($scope);
                    $compile('directory-data-download-record-actions')($scope);
                    $compile('.downLink')($scope);

                    $('[datatable] thead>tr>th:nth-child(2)').css('width', '150px');
                    $('[datatable] thead>tr>th:last-child').css('width', '100px');
                })
                .withOption('aoColumns', columnDefs)
                .withLightColumnFilter(filterDefs);
            /*$rootScope.openDeleteModal = function() {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/' + $localStorage['DMS-DIRECTORY'].codeValue + '.directory.organization.remove.html',
                    resolve: {
                        DataDownloadRequests: [
                            'DirectoryDataDownloadRequestService',
                            function(DirectoryDataDownloadRequestService) {
                                return DirectoryDataDownloadRequestService.list({
                                    id: $scope.selectedDataDownloadRequests
                                }).$promise;
                            }
                        ]
                    },
                    controller: 'DirectoryDataDownloadRequestRemoveController'
                });
                modalInstance.result.then(function(messages) {
                    if (messages.length > 0) showMessages(messages);
                    $state.reload();
                });
            };*/

            $rootScope.openExportToExcelModal = function() {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    // size: 'lg',
                    templateUrl: '/views/directory.organization.export-to-excel.tpl.html',
                    controller: 'DirectoryDataDownloadRequestExportToExcelController'
                });
                modalInstance.result.then(function(messages) {
                    $state.reload();
                });
            };

        }


    ]);
