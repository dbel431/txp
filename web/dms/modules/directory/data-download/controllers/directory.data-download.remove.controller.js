angular.module('directory.dataDownload')
    .controller('DataDownloadRemoveController', [
        '$rootScope',
        '$localStorage',
        '$scope',
        '$uibModalInstance',
        'DataDownload',
        function($rootScope, $localStorage, $scope, $uibModalInstance, DataDownload) {
            $scope.dataDownload = DataDownload;
            $scope.cancel = function() {
                $uibModalInstance.dismiss('cancel');
            };
            var messages = [];
            $scope.remove = function() {
                for (var i = $scope.dataDownload.length - 1; i >= 0; i--) {
                    $scope.dataDownload[i].$remove({
                        id: $scope.dataDownload[i]._id
                    }, function(dataDownloads) {
                        messages.push({
                            type: 'success',
                            message: 'Data download request named: <b>' + dataDownloads.name + '</b> deleted successfully!',
                            header: 'Remove data download request'
                        });
                        syncXhr();
                    }, function(err) {
                        messages.push({
                            type: 'error',
                            message: 'Data download request named: <b>' + dataDownloads.name + '</b> could not be deleted successfully!',
                            header: 'Remove data download request'
                        });
                        syncXhr();
                    });
                }
            };
            var count = 0;

            function syncXhr() {
                count++;
                if ($scope.loading = (count == $scope.dataDownload.length)) $uibModalInstance.close(messages);
            }
        }
    ]);
