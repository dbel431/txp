angular.module('directory.organization')
    .controller('DirectoryDataDownloadAddController', [
        '$rootScope',
        '$localStorage',
        '$scope',
        '$state',
        'DirectoryDataDownloadService',
        function($rootScope, $localStorage, $scope, $state, DirectoryDataDownloadService) {
            $scope.editState = true;
            $scope.loading = false;
            $rootScope.page = {
                name: $localStorage['DMS-DIRECTORY'].description + ': Add new Request',
                actions: {
                    back: {
                        object: 'directory.dataDownload.list',
                        allowed: function() {
                            return $rootScope.isActionPermitted(this.object);
                        }
                    },
                }
            };
            $scope.currentDirectory = $localStorage['DMS-DIRECTORY'].codeValue;

            $scope.fields = getExportFields();

            $scope.allSelected = false;
            $scope.selectedFields = [];
            $scope.sort = { order: "1" };
            $scope.toggleAll = function() {
                for (var i = $scope.fields[$scope.currentDirectory].length - 1; i >= 0; i--) {
                    $scope.fields[$scope.currentDirectory][i].checked = $scope.allSelected;
                }
                if ($scope.allSelected) {
                    for (var i in $scope.fields[$scope.currentDirectory]) {
                        $scope.selectedFields.push($scope.fields[$scope.currentDirectory][i]);
                    }
                } else $scope.selectedFields = [];
            };
            $scope.toggleSelection = function toggleSelection(field) {
                if (field.checked) {
                    if ($scope.selectedFields.indexOf(field) < 0)
                        $scope.selectedFields.push(field);
                } else if ($scope.selectedFields.indexOf(field) >= 0)
                    $scope.selectedFields.splice($scope.selectedFields.indexOf(field), 1);
            };

            $scope.sendDataDownloadRequest = function() {
                var obj = {
                    name: $scope.dataDownloadRequest.name,
                    description: $scope.dataDownloadRequest.description,
                    directory: $localStorage['DMS-DIRECTORY']
                };
                obj.data = $rootScope.getExportToExcelWhereClause();
                obj.data.selectedFields = $scope.selectedFields;

                DirectoryDataDownloadService.save(obj, function(res) {
                    showMessages([{
                        type: 'success',
                        message: 'A new request has been acknowledged',
                        header: 'Data Download Request'
                    }]);
                    $state.go('portal.directory.dataDownload.list');
                    $scope.loading = false;
                }, function(err) {
                    console.log(err);
                    showMessages([{
                        type: 'error',
                        message: 'A new request could not be acknowledged.',
                        header: 'Data Download Request'
                    }]);
                    $scope.loading = false;
                })
            };

            $scope.cancel = function() {
                $state.go('portal.directory.dataDownload.list');
            }
        }
    ]);
