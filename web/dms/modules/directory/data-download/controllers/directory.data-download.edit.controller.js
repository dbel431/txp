angular.module('directory.dataDownload')
    .controller('DirectoryDataDownloadEditController', [
        '$rootScope',
        '$localStorage',
        '$scope',
        '$state',
        '$stateParams',
        'DTOptionsBuilder',
        'DataDownload',
        function($rootScope, $localStorage, $scope, $state, $stateParams, DTOptionsBuilder, DataDownload) {
            $rootScope.page = {
                name: 'Edit Data Download Request',
                actions: {
                    back: {
                        object: 'directory.dataDownload.list',
                        allowed: function() {
                            return $rootScope.isActionPermitted(this.object);
                        }
                    }
                }
            };
            $scope.loading = false;
            $scope.dataDownloadRequest = DataDownload[0];
            $scope.currentDirectory = $localStorage['DMS-DIRECTORY'].codeValue;

            $scope.fields = getExportFields();
            $scope.allSelected = false;
            $scope.selectedFields = [];
            if ($scope.dataDownloadRequest.data && $scope.dataDownloadRequest.data.selectedFields) {
                for (var i = $scope.fields[$scope.currentDirectory].length - 1; i >= 0; i--) {
                    for (var j = $scope.dataDownloadRequest.data.selectedFields.length - 1; j >= 0; j--) {
                        if ($scope.fields[$scope.currentDirectory][i].name == $scope.dataDownloadRequest.data.selectedFields[j].name) {
                            $scope.fields[$scope.currentDirectory][i].checked = $scope.dataDownloadRequest.data.selectedFields[j].checked;
                            if ($scope.fields[$scope.currentDirectory][i].checked) {
                                if ($scope.selectedFields.indexOf($scope.fields[$scope.currentDirectory][i]) < 0)
                                    $scope.selectedFields.push($scope.fields[$scope.currentDirectory][i]);
                            }
                            break;
                        }
                    }
                }
                $scope.allSelected = ($scope.selectedFields.length == $scope.fields[$scope.currentDirectory].length);
            }
            $scope.sort = { order: "1" };
            $scope.toggleAll = function() {
                for (var i = $scope.fields[$scope.currentDirectory].length - 1; i >= 0; i--) {
                    $scope.fields[$scope.currentDirectory][i].checked = $scope.allSelected;
                }
                if ($scope.allSelected) {
                    for (var i in $scope.fields[$scope.currentDirectory]) {
                        $scope.selectedFields.push($scope.fields[$scope.currentDirectory][i]);
                    }
                } else $scope.selectedFields = [];
            };
            $scope.toggleSelection = function toggleSelection(field) {
                if (field.checked) {
                    if ($scope.selectedFields.indexOf(field) < 0)
                        $scope.selectedFields.push(field);
                    $scope.allSelected = ($scope.selectedFields.length == $scope.fields[$scope.currentDirectory].length);
                } else {
                    if ($scope.selectedFields.indexOf(field) >= 0)
                        $scope.selectedFields.splice($scope.selectedFields.indexOf(field), 1);
                    $scope.allSelected = false;
                }
            };

            $scope.checkChange = function(aclAction) {
                if (aclAction.selected) {
                    if ($scope.dataDownloadRequest.actions.indexOf(aclAction._id) < 0)
                        $scope.dataDownloadRequest.actions.push(aclAction._id);
                } else {
                    if ($scope.dataDownloadRequest.actions.indexOf(aclAction._id) >= 0)
                        $scope.dataDownloadRequest.actions.splice($scope.dataDownloadRequest.actions.indexOf(aclAction._id), 1);
                }
            }

            $scope.editDataDownloadRequest = function() {
                $scope.loading = true;
                if ($scope.dataDownloadRequest.data) $scope.dataDownloadRequest.data.selectedFields = $scope.selectedFields;
                $scope.dataDownloadRequest.$update({
                    id: $scope.dataDownloadRequest._id
                }, function(dataDownload) {
                    showMessages([{
                        type: 'success',
                        message: 'Details updated successfully!',
                        header: 'Edit Data Download Request'
                    }]);
                    $state.go('portal.directory.dataDownload.list');
                    $scope.loading = false;
                }, function(err) {
                    showMessages([{
                        type: 'error',
                        message: err.data.message,
                        header: 'Edit Data Download Request'
                    }]);
                    $scope.loading = false;
                })
            }
            $scope.cancel = function() {
                $state.go('portal.directory.dataDownload.list');
            }
        }
    ]);
