angular.module('directory.dataDownload')
    .controller('DirectoryDataDownloadController', [
        '$rootScope',
        '$localStorage',
        function($rootScope, $localStorage) {
            if (!($localStorage['DMS-DIRECTORY'])) $localStorage['DMS-DIRECTORY'] = false;
            $rootScope.page = {
                name: $localStorage['DMS-DIRECTORY'].description + ':  Data Downloads'
            };

        }

    ]);
