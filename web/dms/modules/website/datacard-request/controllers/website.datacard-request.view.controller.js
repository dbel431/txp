angular.module('website.datacardRequest')
    .controller('WebDatacardRequestViewController', [
        '$rootScope',
        '$state',
        '$stateParams',
        '$scope',
        'WebDatacardRequest',
        function($rootScope, $state, $stateParams, $scope, WebDatacardRequest) {
            $rootScope.page = {
                name: 'View Datacard Request',
                actions: {
                    back: {
                        object: 'website.datacardRequest.list',
                        allowed: function() {
                            return $rootScope.isActionPermitted(this.object);
                        }
                    }
                }
            };
            $scope.loading = false;
            $scope.datacardRequest = WebDatacardRequest[0];
            $scope.progressMap = {
                1: 'Idle',
                2: 'Under Review',
                3: 'Approved',
                4: 'Rejected'
            };
        }
    ]);
