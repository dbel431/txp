angular.module('website.datacardRequest')
    .controller('WebDatacardRequestController', [
        '$rootScope',
        '$scope',
        '$state',
        function($rootScope, $scope, $state) {
            $rootScope.page = {
                name: 'Datacard Requests'
            };
        }
    ]);
