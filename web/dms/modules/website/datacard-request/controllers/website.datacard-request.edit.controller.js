angular.module('website.datacardRequest')
    .controller('WebDatacardRequestEditController', [
        '$rootScope',
        '$state',
        '$stateParams',
        '$scope',
        'WebDatacardRequest',
        function($rootScope, $state, $stateParams, $scope, WebDatacardRequest) {
            $rootScope.page = {
                name: 'Edit Datacard Request',
                actions: {
                    back: {
                        object: 'website.datacardRequest.list',
                        allowed: function() {
                            return $rootScope.isActionPermitted(this.object);
                        }
                    }
                }
            };
            $scope.loading = false;
            $scope.progressStatusArray = [
                { id: 1, label: 'Idle' },
                { id: 2, label: 'Under Review' },
                { id: 3, label: 'Approved' },
                { id: 4, label: 'Rejected' },
            ];
            $scope.datacardRequest = WebDatacardRequest[0];
            // ha 
            for (var i = 0; i < $scope.progressStatusArray.length; i++) {
                if ($scope.datacardRequest.progress == $scope.progressStatusArray[i].id) {
                    $scope.datacardRequest.progress = $scope.progressStatusArray[i];
                }
            }
            // portion
            $scope.commentIndex = ($scope.datacardRequest.adminComments).length;
            $scope.datacardRequest.adminComments[$scope.commentIndex] = {
                by: $rootScope.currentUser._id,
                prevProgress: $scope.datacardRequest.progress
            };
            $scope.update = function() {
                $scope.datacardRequest.progress = $scope.datacardRequest.progress.id; // hi line
                $scope.datacardRequest.adminComments[$scope.commentIndex].prevProgress = $scope.datacardRequest.adminComments[$scope.commentIndex].prevProgress.id; // hi pan
                $scope.datacardRequest.adminComments[$scope.commentIndex].at = new Date();
                $scope.datacardRequest.$update(function(datacardRequest) {
                    showMessages([{
                        type: 'success',
                        message: 'Request Details updated successfully!',
                        header: 'Edit Datacard Request'
                    }]);
                    $state.go('portal.website.datacardRequest');
                }, function(err) {
                    console.log('update err', err);
                    showMessages([{
                        type: 'error',
                        message: ((err.data ? err.data.message : false) || 'Request Details could not be updated successfully!'),
                        header: 'Edit Datacard Request'
                    }]);
                });
            };
        }
    ]);
