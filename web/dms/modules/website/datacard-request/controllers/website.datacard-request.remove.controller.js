angular.module('website.datacardRequest')
    .controller('WebDatacardRequestRemoveController', [
        '$rootScope',
        '$scope',
        '$uibModalInstance',
        'WebDatacardRequests',
        function($rootScope, $scope, $uibModalInstance, WebDatacardRequests) {
            $scope.webDatacardRequests = WebDatacardRequests;
            $scope.cancel = function() {
                $uibModalInstance.dismiss('cancel');
            };
            var messages = [];
            $scope.remove = function() {
                for (var i = $scope.webDatacardRequests.length - 1; i >= 0; i--) {
                    $scope.webDatacardRequests[i].$remove({
                        id: $scope.webDatacardRequests[i]._id
                    }, function(webDatacardRequest) {
                        messages.push({
                            type: 'success',
                            message: 'Record named: <b>' + webDatacardRequest.name + '</b> deleted successfully!',
                            header: 'Remove Record'
                        });
                        syncXhr();
                    }, function(err) {
                        messages.push({
                            type: 'error',
                            message: 'Record named: <b>' + webDatacardRequest.name + '</b> could not be deleted successfully!',
                            header: 'Remove Record'
                        });
                        syncXhr();
                    });
                }
            };
            var count = 0;

            function syncXhr() {
                count++;
                if ($scope.loading = (count == $scope.webDatacardRequests.length)) $uibModalInstance.close(messages);
            }
        }
    ]);
