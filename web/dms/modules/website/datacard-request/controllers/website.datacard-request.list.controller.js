angular.module('website.datacardRequest')
    .controller('WebDatacardRequestListController', [
        '$rootScope',
        '$scope',
        '$state',
        '$compile',
        '$uibModal',
        'DTOptionsBuilder',
        'WebDatacardRequestService',
        function($rootScope, $scope, $state, $compile, $uibModal, DTOptionsBuilder, WebDatacardRequestService) {
            var progressMap = {
                    1: 'Idle',
                    2: 'Under Review',
                    3: 'Approved',
                    4: 'Rejected'
                },
                progressKeyValMap = [{
                    value: 1,
                    label: 'Idle'
                }, {
                    value: 2,
                    label: 'Under Review'
                }, {
                    value: 3,
                    label: 'Approved'
                }, {
                    value: 4,
                    label: 'Rejected'
                }];
            $rootScope.page = {
                name: 'Datacard Request List',
            };
            $scope.selectedWebDatacardRequests = [];
            $scope.selectedWebDatacardRequestRecords = {};
            $scope.dtData = [];
            $scope.toggleAllRecords = function(select) {
                for (var i = $scope.dtData.length - 1; i >= 0; i--) {
                    $scope.dtData[i].checked = select || false;
                    if (select) {
                        if ($scope.selectedWebDatacardRequests.indexOf($scope.dtData[i]._id) < 0) {
                            $scope.selectedWebDatacardRequests.push($scope.dtData[i]._id);
                            $scope.selectedWebDatacardRequestRecords[$scope.dtData[i]._id] = $scope.dtData[i];
                        }
                    } else if ($scope.selectedWebDatacardRequests.indexOf($scope.dtData[i]._id) >= 0) {
                        if ($scope.selectedWebDatacardRequests.indexOf($scope.dtData[i]._id) >= 0) {
                            $scope.selectedWebDatacardRequests.splice($scope.selectedWebDatacardRequests.indexOf($scope.dtData[i]._id), 1);
                            delete $scope.selectedWebDatacardRequestRecords[$scope.dtData[i]._id];
                        }
                    }
                }
            };
            $scope.toggleRecord = function(id) {
                for (var i = $scope.dtData.length - 1; i >= 0; i--) {
                    if ($scope.dtData[i]._id == id) {
                        if ($scope.dtData[i].checked) {
                            if ($scope.selectedWebDatacardRequests.indexOf(id) < 0) {
                                $scope.selectedWebDatacardRequests.push(id);
                                $scope.selectedWebDatacardRequestRecords[id] = $scope.dtData[i];
                            }
                        } else {
                            if ($scope.selectedWebDatacardRequests.indexOf(id) >= 0) {
                                $scope.selectedWebDatacardRequests.splice($scope.selectedWebDatacardRequests.indexOf(id), 1);
                                delete $scope.selectedWebDatacardRequestRecords[id];
                            }
                        }
                        break;
                    }
                }
            };
            $scope.allChecked = function() {
                for (var i = $scope.dtData.length - 1; i >= 0; i--) {
                    if (!$scope.dtData[i].checked) return false;
                }
                return true;
            };
            $scope.isActionPermitted = $rootScope.isActionPermitted;
            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withPaginationType('full_numbers')
                .withOption('stateSave', true)
                .withOption('ajax', WebDatacardRequestService.dataTable)
                .withDataProp('data')
                .withOption('processing', true)
                .withOption('serverSide', true)
                .withOption('oLanguage', {
                    "sEmptyTable": "No matching records found"
                })
                .withOption('order', [2, 'asc'])
                .withOption('drawCallback', function(settings) {
                    var api = this.api(),
                        listOfRecords = api.data(),
                        state = api.state(),
                        filterRow = angular.element(api.table().header()).children()[1];
                    $(filterRow).children().each(function(index, child) {
                        if ($(child).has('.form-control'))
                            $(child).children('.form-control').val(state.columns[index].search.search);
                    });
                    $scope.dtData = [];
                    for (var i = listOfRecords.length - 1; i >= 0; i--) {
                        $scope.dtData[i] = (listOfRecords[i]);
                        $scope.dtData[i].checked = ($scope.selectedWebDatacardRequests.indexOf($scope.dtData[i]._id) >= 0);
                    }
                    $compile('website-datacard-request-record-select')($scope);
                    $compile('website-datacard-request-record-options')($scope);
                    $compile('website-datacard-request-record-actions')($scope);
                })
                .withPaginationType('full_numbers')
                .withOption('aoColumns', [{
                    title: '<website-datacard-request-record-options is-action-permitted="isActionPermitted" open-delete-modal="openDeleteModal" selected-record-list="selectedWebDatacardRequests" dt-data="dtData" all-checked="allChecked" toggle-all-records="toggleAllRecords"></website-datacard-request-record-options>',
                    orderable: false,
                    class: 'text-center',
                    width: '1%',
                    data: '_id',
                    render: function(id) {
                        return '<website-datacard-request-record-select dt-data="dtData" selected-record-list="selectedWebDatacardRequests" record-id="' + id + '" toggle-record="toggleRecord"></website-datacard-request-record-select>';
                    }
                }, {
                    title: 'Title',
                    width: '10%',
                    name: 'title',
                    data: 'title',
                    render: function(title) {
                        return title || "";
                    }
                }, {
                    title: 'First Name',
                    width: '13%',
                    name: 'fname',
                    data: 'name.first',
                    render: function(fname) {
                        return fname || "";
                    }
                }, {
                    title: 'Last Name',
                    width: '12%',
                    name: 'lname',
                    data: 'name.last',
                    render: function(lname) {
                        return lname || "";
                    }
                }, {
                    title: 'Email',
                    width: '13%',
                    name: 'email',
                    data: 'email',
                    render: function(email) {
                        return email || "";
                    }
                }, {
                    title: 'Organization',
                    width: '13%',
                    name: 'org',
                    data: 'organization',
                    render: function(org) {
                        return org || "";
                    }
                }, {
                    title: 'Progress',
                    width: '10%',
                    orderable: false,
                    name: 'progress',
                    data: 'progress',
                    render: function(progress) {
                        return progressMap[progress];
                    }
                }, {
                    title: 'Status',
                    width: '10%',
                    orderable: false,
                    name: 'status',
                    data: 'active',
                    render: function(active) {
                        return active ? '<span class="">Active</span>' : '<span class="">Inactive</span>';
                    }
                }, {
                    title: 'Actions',
                    orderable: false,
                    class: 'text-center',
                    width: '13%',
                    data: '_id',
                    render: function(id) {
                        return '<website-datacard-request-record-actions open-delete-modal1="openDeleteModal1" open-view-comments-modal="openViewCommentsModal" dt-data="dtData" record-id="\'' + id + '\'"></website-datacard-request-record-actions>';
                    }
                }])
                .withLightColumnFilter({
                    '1': {
                        type: 'text',
                        time: 600
                    },
                    '2': {
                        type: 'text',
                        time: 600
                    },
                    '3': {
                        type: 'text',
                        time: 600
                    },
                    '4': {
                        type: 'text',
                        time: 600
                    },
                    '5': {
                        type: 'text',
                        time: 600
                    },
                    '6': {
                        type: 'select',
                        values: progressKeyValMap
                    },
                    '7': {
                        type: 'select',
                        values: [{
                            value: true,
                            label: 'Active'
                        }, {
                            value: false,
                            label: 'Inactive'
                        }]
                    }
                });
            $rootScope.openDeleteModal = function() {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/website.datacard-request.remove.html',
                    resolve: {
                        WebDatacardRequests: function() {
                            return WebDatacardRequestService.get({
                                id: $scope.selectedWebDatacardRequests
                            }).$promise;
                        }
                    },
                    controller: 'WebDatacardRequestRemoveController'
                });
                modalInstance.result.then(function(messages) {
                    var success = 0,
                        error = 0;
                    for (var i in messages) {
                        if (messages[i].type == 'success') success++;
                        if (messages[i].type == 'error') error++;
                    }
                    messages = [];
                    if (success > 0) messages.push({
                        type: 'success',
                        header: 'Remove Datacard Request',
                        message: success + " record(s) removed successfully!"
                    });
                    if (error > 0) messages.push({
                        type: 'error',
                        header: 'Remove Datacard Request',
                        message: error + " record(s) could not be removed successfully!"
                    });
                    if (messages.length > 0) showMessages(messages);
                    $state.reload();
                });
            };
            $scope.openDeleteModal1 = function(recordId) {
                $scope.toggleAllRecords(false);
                $scope.selectedWebDatacardRequests = [recordId];
                $rootScope.openDeleteModal();
            };
            $scope.openViewCommentsModal = function(id) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/website.datacard-request.comments.html',
                    resolve: {
                        WebDatacardRequest: function() {
                            return WebDatacardRequestService.get({ id: id }).$promise;
                        }
                    },
                    controller: [
                        '$rootScope',
                        '$scope',
                        '$uibModalInstance',
                        'WebDatacardRequest',
                        function($rootScope, $modalScope, $uibModalInstance, WebDatacardRequest) {
                            $modalScope.progressMap = progressMap;
                            $modalScope.comments = WebDatacardRequest[0].adminComments.reverse();
                            for (var i = 0; i < $modalScope.comments.length; i++) {
                                if (i == 0) $modalScope.comments[i].toProgress = WebDatacardRequest[0].progress;
                                else $modalScope.comments[i].toProgress = $modalScope.comments[(i - 1)].prevProgress;
                            }
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    ]
                });
            };
        }
    ]);
