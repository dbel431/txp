angular.module('website.datacardRequest', [
        'website'
    ])
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $stateProvider.state('portal.website.datacardRequest', {
                url: '/datacard-request',
                template: '<ui-view></ui-view>',
                controller: 'WebDatacardRequestController',
            }).state('portal.website.datacardRequest.list', {
                url: '/list',
                templateUrl: '/views/website.datacard-request.list.html',
                controller: 'WebDatacardRequestListController'
            }).state('portal.website.datacardRequest.edit', {
                url: '/edit/:id',
                templateUrl: '/views/website.datacard-request.html',
                resolve: {
                    'WebDatacardRequest': [
                        'WebDatacardRequestService',
                        '$stateParams',
                        function(WebDatacardRequestService, $stateParams) {
                            return WebDatacardRequestService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ]
                },
                controller: 'WebDatacardRequestEditController'
            }).state('portal.website.datacardRequest.view', {
                url: '/view/:id',
                templateUrl: '/views/website.datacard-request.view.html',
                resolve: {
                    'WebDatacardRequest': [
                        'WebDatacardRequestService',
                        '$stateParams',
                        function(WebDatacardRequestService, $stateParams) {
                            return WebDatacardRequestService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ]
                },
                controller: 'WebDatacardRequestViewController'
            }).state('portal.website.datacardRequest.remove', {
                url: '/remove/:id',
                templateUrl: '/views/website.datacard-request.remove.html',
                resolve: {
                    'WebDatacardRequest': [
                        'WebDatacardRequestService',
                        '$stateParams',
                        function(WebDatacardRequestService, $stateParams) {
                            return WebDatacardRequestService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ]
                },
                controller: 'WebDatacardRequestRemoveController'
            });
        }
    ]);
