angular.module('website').directive('websiteDatacardRequestRecordOptions', function() {
    return {
        scope: {
            'selectedRecordList': '=',
            'toggleAllRecords': '=',
            'isActionPermitted': '=',
            'openDeleteModal': '=',
            'allChecked': '=',
        },
        restrict: 'EA',
        templateUrl: '/views/website.datacard-request.record-options.tpl.html',
        replace: true
    };
});
