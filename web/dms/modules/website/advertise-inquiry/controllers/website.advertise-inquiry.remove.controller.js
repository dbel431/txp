angular.module('website.advertiseInquiry')
    .controller('WebAdvertiseInquiryRemoveController', [
        '$rootScope',
        '$scope',
        '$uibModalInstance',
        'WebAdvertiseInquirys',
        function($rootScope, $scope, $uibModalInstance, WebAdvertiseInquirys) {
            $scope.webAdvertiseInquirys = WebAdvertiseInquirys;
            $scope.cancel = function() {
                $uibModalInstance.dismiss('cancel');
            };
            var messages = [];
            $scope.remove = function() {
                for (var i = $scope.webAdvertiseInquirys.length - 1; i >= 0; i--) {
                    $scope.webAdvertiseInquirys[i].$remove({
                        id: $scope.webAdvertiseInquirys[i]._id
                    }, function(webAdvertiseInquiry) {
                        messages.push({
                            type: 'success',
                            message: 'Record named: <b>' + webAdvertiseInquiry.name + '</b> deleted successfully!',
                            header: 'Remove Record'
                        });
                        syncXhr();
                    }, function(err) {
                        messages.push({
                            type: 'error',
                            message: 'Record named: <b>' + webAdvertiseInquiry.name + '</b> could not be deleted successfully!',
                            header: 'Remove Record'
                        });
                        syncXhr();
                    });
                }
            };
            var count = 0;

            function syncXhr() {
                count++;
                if ($scope.loading = (count == $scope.webAdvertiseInquirys.length)) $uibModalInstance.close(messages);
            }
        }
    ]);
