angular.module('website.advertiseInquiry')
    .controller('WebAdvertiseInquiryEditController', [
        '$rootScope',
        '$state',
        '$stateParams',
        '$scope',
        'WebAdvertiseInquiry',
        function($rootScope, $state, $stateParams, $scope, WebAdvertiseInquiry) {
            $rootScope.page = {
                name: 'Edit Advertise Inquiry',
                actions: {
                    back: {
                        object: 'website.advertiseInquiry.list',
                        allowed: function() {
                            return $rootScope.isActionPermitted(this.object);
                        }
                    }
                }
            };
            $scope.loading = false;
            $scope.progressStatusArray = [
                { id: 1, label: 'Idle' },
                { id: 2, label: 'Under Review' },
                { id: 3, label: 'Approved' },
                { id: 4, label: 'Rejected' },
            ];
            $scope.advertiseInquiry = WebAdvertiseInquiry[0];
            // ha 
            for (var i = 0; i < $scope.progressStatusArray.length; i++) {
                if ($scope.advertiseInquiry.progress == $scope.progressStatusArray[i].id) {
                    $scope.advertiseInquiry.progress = $scope.progressStatusArray[i];
                }
            }
            // portion
            $scope.commentIndex = ($scope.advertiseInquiry.adminComments).length;
            $scope.advertiseInquiry.adminComments[$scope.commentIndex] = {
                by: $rootScope.currentUser._id,
                prevProgress: $scope.advertiseInquiry.progress
            };
            $scope.update = function() {
                $scope.advertiseInquiry.progress = $scope.advertiseInquiry.progress.id; // hi line
                $scope.advertiseInquiry.adminComments[$scope.commentIndex].prevProgress = $scope.advertiseInquiry.adminComments[$scope.commentIndex].prevProgress.id; // hi pan
                $scope.advertiseInquiry.adminComments[$scope.commentIndex].at = new Date();
                $scope.advertiseInquiry.$update(function(advertiseInquiry) {
                    showMessages([{
                        type: 'success',
                        message: 'Request Details updated successfully!',
                        header: 'Edit  Advertise Inquiry'
                    }]);
                    $state.go('portal.website.advertiseInquiry');
                }, function(err) {
                    console.log('update err', err);
                    showMessages([{
                        type: 'error',
                        message: ((err.data ? err.data.message : false) || 'Request Details could not be updated successfully!'),
                        header: 'Edit  Advertise Inquiry'
                    }]);
                });
            };
        }
    ]);
