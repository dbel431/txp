angular.module('website.advertiseInquiry')
    .controller('WebAdvertiseInquiryViewController', [
        '$rootScope',
        '$state',
        '$stateParams',
        '$scope',
        'WebAdvertiseInquiry',
        function($rootScope, $state, $stateParams, $scope, WebAdvertiseInquiry) {
            $rootScope.page = {
                name: 'View Advertise Inquiry',
                actions: {
                    back: {
                        object: 'website.advertiseInquiry.list',
                        allowed: function() {
                            return $rootScope.isActionPermitted(this.object);
                        }
                    }
                }
            };
            $scope.loading = false;
            $scope.advertiseInquiry = WebAdvertiseInquiry[0];
            $scope.progressMap = {
                1: 'Idle',
                2: 'Under Review',
                3: 'Approved',
                4: 'Rejected'
            };
        }
    ]);
