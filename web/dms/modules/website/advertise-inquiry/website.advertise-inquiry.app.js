angular.module('website.advertiseInquiry', [
        'website'
    ])
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $stateProvider.state('portal.website.advertiseInquiry', {
                url: '/advertise-inquiry',
                template: '<ui-view></ui-view>',
                controller: 'WebAdvertiseInquiryController',
            }).state('portal.website.advertiseInquiry.list', {
                url: '/list',
                templateUrl: '/views/website.advertise-inquiry.list.html',
                controller: 'WebAdvertiseInquiryListController'
            }).state('portal.website.advertiseInquiry.edit', {
                url: '/edit/:id',
                templateUrl: '/views/website.advertise-inquiry.html',
                resolve: {
                    'WebAdvertiseInquiry': [
                        'WebAdvertiseInquiryService',
                        '$stateParams',
                        function(WebAdvertiseInquiryService, $stateParams) {
                            return WebAdvertiseInquiryService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ]
                },
                controller: 'WebAdvertiseInquiryEditController'
            }).state('portal.website.advertiseInquiry.view', {
                url: '/view/:id',
                templateUrl: '/views/website.advertise-inquiry.view.html',
                resolve: {
                    'WebAdvertiseInquiry': [
                        'WebAdvertiseInquiryService',
                        '$stateParams',
                        function(WebAdvertiseInquiryService, $stateParams) {
                            return WebAdvertiseInquiryService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ]
                },
                controller: 'WebAdvertiseInquiryViewController'
            }).state('portal.website.advertiseInquiry.remove', {
                url: '/remove/:id',
                templateUrl: '/views/website.advertise-inquiry.remove.html',
                resolve: {
                    'WebAdvertiseInquiry': [
                        'WebAdvertiseInquiryService',
                        '$stateParams',
                        function(WebAdvertiseInquiryService, $stateParams) {
                            return WebAdvertiseInquiryService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ]
                },
                controller: 'WebAdvertiseInquiryRemoveController'
            });
        }
    ]);
