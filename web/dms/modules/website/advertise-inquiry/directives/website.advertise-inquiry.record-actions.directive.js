angular.module('website').directive('websiteAdvertiseInquiryRecordActions', [
    '$rootScope',
    function($rootScope) {
        return {
            scope: {
                'dtData': '=',
                'recordId': '=',
                'openDeleteModal1': '=',
                'openViewCommentsModal': '=',
            },
            restrict: 'EA',
            templateUrl: '/views/website.advertise-inquiry.record-actions.tpl.html',
            replace: true,
            link: function(scope, elt, attb) {
                scope.isActionPermitted = $rootScope.isActionPermitted;
                scope.record = {};
                if (scope.dtData) {
                    for (var i = 0; i < scope.dtData.length; i++) {
                        if (scope.dtData[i]._id == scope.recordId) {
                            scope.record = scope.dtData[i];
                            break;
                        }
                    }
                }
            }
        };
    }
]);
