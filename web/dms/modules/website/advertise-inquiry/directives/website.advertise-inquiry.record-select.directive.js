angular.module('website').directive('websiteAdvertiseInquiryRecordSelect', function() {
    return {
        scope: {
            dtData: '=',
            selectedRecordList: '=',
            toggleRecord: '='
        },
        restrict: 'EA',
        templateUrl: '/views/website.advertise-inquiry.record-select.tpl.html',
        replace: true,
        link: function(scope, elt, attb) {
            scope.record = {};
            for (var i = scope.dtData.length - 1; i >= 0; i--) {
                if (scope.dtData[i]._id == attb.recordId) scope.record = scope.dtData[i];
            }
        }
    };
});
