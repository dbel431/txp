angular.module('website').directive('websiteAdvertiseInquiryRecordOptions', function() {
    return {
        scope: {
            'selectedRecordList': '=',
            'toggleAllRecords': '=',
            'isActionPermitted': '=',
            'openDeleteModal': '=',
            'allChecked': '=',
        },
        restrict: 'EA',
        templateUrl: '/views/website.advertise-inquiry.record-options.tpl.html',
        replace: true
    };
});
