angular.module('website.user')
    .controller('WebUserViewController', [
        '$rootScope',
        '$state',
        '$stateParams',
        '$scope',
        'WebUser',
        function($rootScope, $state, $stateParams, $scope, WebUser) {
            $rootScope.page = {
                name: 'View Subscriber',
                actions: {
                    back: {
                        object: 'website.user.list',
                        allowed: function() {
                            return $rootScope.isActionPermitted(this.object);
                        }
                    }
                }
            };
            $scope.loading = false;
            $scope.user = WebUser[0];
        }
    ]);
