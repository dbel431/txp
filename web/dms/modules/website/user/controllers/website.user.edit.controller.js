angular.module('website.user')
    .controller('WebUserEditController', [
        '$rootScope',
        '$state',
        '$stateParams',
        '$scope',
        '$uibModal',
        'WebUser',
        function($rootScope, $state, $stateParams, $scope, $uibModal, WebUser) {
            $rootScope.page = {
                name: 'Edit Subscriber',
                actions: {
                    back: {
                        object: 'website.user.list',
                        allowed: function() {
                            return $rootScope.isActionPermitted(this.object);
                        }
                    }
                }
            };
            $scope.loading = false;
            $scope.user = WebUser[0];
            $scope.openOrderEditModal = function(index) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    size: 'lg',
                    templateUrl: '/views/website.user.order.popup.html',
                    controller: [
                        '$scope',
                        '$uibModalInstance',
                        function($modalScope, $uibModalInstance) {
                            $modalScope.order = getObjectData($scope.user.orders[index]);

                            if (!$modalScope.order.subscription) $modalScope.order.subscription = {};

                            $modalScope.order.subscription.from = $modalScope.order.subscription.from ? new Date($modalScope.order.subscription.from) : '';
                            $modalScope.order.subscription.to = $modalScope.order.subscription.to ? new Date($modalScope.order.subscription.to) : '';
                            $modalScope.accessDateOptions = {
                                minDate: new Date($modalScope.order.subscription.from),
                                maxDate: new Date($modalScope.order.subscription.to)
                            };
                            $modalScope.expiryDateOptions = {
                                minDate: new Date($modalScope.order.subscription.from),
                                maxDate: new Date($modalScope.order.subscription.to)
                            };
                            $modalScope.$watch('order.subscription.to', function(newVal) {
                                $modalScope.accessDateOptions.maxDate = new Date(newVal || '');
                                $modalScope.expiryDateOptions.maxDate = new Date(newVal || '');
                            });
                            $modalScope.$watch('order.subscription.from', function(newVal) {
                                $modalScope.accessDateOptions.minDate = new Date(newVal || '');
                                $modalScope.expiryDateOptions.minDate = new Date(newVal || '');
                            });

                            $modalScope.changeOrderConfirmation = function() {
                                if ($modalScope.order.confirmed.flag) {
                                    $modalScope.order.confirmed = {
                                        flag: $modalScope.order.confirmed.flag,
                                        by: $rootScope.currentUser._id,
                                        at: new Date()
                                    }
                                    $scope.user.updatedOrderIndex = index;
                                } else {
                                    $modalScope.order.confirmed = {
                                        flag: $modalScope.order.confirmed.flag
                                    }
                                    if ($scope.user.updatedOrderIndex) delete $scope.user.updatedOrderIndex;
                                }
                            };

                            $modalScope.edit = function() {
                                $scope.loading = true;
                                $scope.user.orders[index] = $modalScope.order;
                                var userId=angular.copy($scope.user);
                                userId.$update({
                                    id: $scope.user._id
                                }, function(user) {

                                    showMessages([{
                                        type: 'success',
                                        message: 'Order Details updated successfully!',
                                        header: 'Manage Orders'
                                    }]);
                                    $uibModalInstance.close(user);
                                    $scope.loading = false;
                                }, function(err) {
                                    showMessages([{
                                        type: 'error',
                                        message: ((err.data ? err.data.message : false) || 'Order Details could not be updated successfully!'),
                                        header: 'Manage Orders'
                                    }]);
                                    $scope.loading = false;
                                })
                            };

                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    ]
                });
                modalInstance.result.then(function(user) {
                    $state.reload();
                });
            };
        }
    ]);
