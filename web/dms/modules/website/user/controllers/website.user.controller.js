angular.module('website.user')
    .controller('WebUserController', [
        '$rootScope',
        '$scope',
        '$state',
        function($rootScope, $scope, $state) {
            $rootScope.page = {
                name: 'Subscribers'
            };
        }
    ]);
