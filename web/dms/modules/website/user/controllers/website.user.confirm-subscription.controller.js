angular.module('website.user')
    .controller('WebUserConfirmSubscriptionController', [
        '$rootScope',
        '$state',
        '$stateParams',
        '$scope',
        'WebUser',
        function($rootScope, $state, $stateParams, $scope, WebUser) {
            $rootScope.page = {
                name: 'Confirm Subscription',
                actions: {
                    back: {
                        object: 'website.user.list',
                        allowed: function() {
                            return $rootScope.isActionPermitted(this.object);
                        }
                    }
                }
            };
            $scope.loading = false;
            $scope.user = WebUser[0];

            $scope.user.accessDate = $scope.user.accessDate ? new Date($scope.user.accessDate) : '';
            $scope.user.expiryDate = $scope.user.expiryDate ? new Date($scope.user.expiryDate) : '';
            $scope.accessDateOptions = {
                minDate: new Date($scope.user.accessDate),
                maxDate: new Date($scope.user.expiryDate)
            };
            $scope.expiryDateOptions = {
                minDate: new Date($scope.user.accessDate),
                maxDate: new Date($scope.user.expiryDate)
            };
            $scope.$watch('user.expiryDate', function(newVal) {
                $scope.accessDateOptions.maxDate = new Date(newVal || '');
                $scope.expiryDateOptions.maxDate = new Date(newVal || '');
            });
            $scope.$watch('user.accessDate', function(newVal) {
                $scope.accessDateOptions.minDate = new Date(newVal || '');
                $scope.expiryDateOptions.minDate = new Date(newVal || '');
            });

            $scope.confirmSubscription = function() {
                $scope.loading = true;
                $scope.user.$confirmSubscription({
                    id: $scope.user._id
                }, function(user) {
                    showMessages([{
                        type: 'success',
                        message: 'User Account created successfully!',
                        header: 'User Account'
                    }]);
                    $state.go('portal.website.user.list');
                    $scope.loading = false;
                }, function(err) {
                    showMessages([{
                        type: 'error',
                        message: (err.data ? (err.data.message || "User Account could not be created successfully!") : ""),
                        header: 'User Account'
                    }]);
                    $scope.loading = false;
                })
            }
        }
    ]);