angular.module('website.user')
    .controller('WebUserRemoveController', [
        '$rootScope',
        '$scope',
        '$uibModalInstance',
        'WebUsers',
        function($rootScope, $scope, $uibModalInstance, WebUsers) {
            $scope.webUsers = WebUsers;
            $scope.cancel = function() {
                $uibModalInstance.dismiss('cancel');
            };
            var messages = [];
            $scope.remove = function() {
                for (var i = $scope.webUsers.length - 1; i >= 0; i--) {
                    $scope.webUsers[i].$remove({
                        id: $scope.webUsers[i]._id
                    }, function(webUser) {
                        messages.push({
                            type: 'success',
                            message: 'Record named: <b>' + webUser.name + '</b> deleted successfully!',
                            header: 'Remove Record'
                        });
                        syncXhr();
                    }, function(err) {
                        messages.push({
                            type: 'error',
                            message: 'Record named: <b>' + webUser.name + '</b> could not be deleted successfully!',
                            header: 'Remove Record'
                        });
                        syncXhr();
                    });
                }
            };
            var count = 0;

            function syncXhr() {
                count++;
                if ($scope.loading = (count == $scope.webUsers.length)) $uibModalInstance.close(messages);
            }
        }
    ]);
