angular.module('website.user')
    .controller('WebUserListController', [
        '$rootScope',
        '$scope',
        '$state',
        '$compile',
        '$uibModal',
        'DTOptionsBuilder',
        'WebUserService',
        function($rootScope, $scope, $state, $compile, $uibModal, DTOptionsBuilder, WebUserService) {
            $rootScope.page = {
                name: 'Subscriber List',
                actions: {
                    add: {
                        object: 'website.user.add',
                        allowed: function() {
                            return $rootScope.isActionPermitted(this.object);
                        }
                    }
                }
            };
            $scope.selectedWebUsers = [];
            $scope.selectedWebUserRecords = {};
            $scope.dtData = [];
            $scope.toggleAllRecords = function(select) {
                for (var i = $scope.dtData.length - 1; i >= 0; i--) {
                    $scope.dtData[i].checked = select || false;
                    if (select) {
                        if ($scope.selectedWebUsers.indexOf($scope.dtData[i]._id) < 0) {
                            $scope.selectedWebUsers.push($scope.dtData[i]._id);
                            $scope.selectedWebUserRecords[$scope.dtData[i]._id] = $scope.dtData[i];
                        }
                    } else if ($scope.selectedWebUsers.indexOf($scope.dtData[i]._id) >= 0) {
                        if ($scope.selectedWebUsers.indexOf($scope.dtData[i]._id) >= 0) {
                            $scope.selectedWebUsers.splice($scope.selectedWebUsers.indexOf($scope.dtData[i]._id), 1);
                            delete $scope.selectedWebUserRecords[$scope.dtData[i]._id];
                        }
                    }
                }
            };
            $scope.toggleRecord = function(id) {
                for (var i = $scope.dtData.length - 1; i >= 0; i--) {
                    if ($scope.dtData[i]._id == id) {
                        if ($scope.dtData[i].checked) {
                            if ($scope.selectedWebUsers.indexOf(id) < 0) {
                                $scope.selectedWebUsers.push(id);
                                $scope.selectedWebUserRecords[id] = $scope.dtData[i];
                            }
                        } else {
                            if ($scope.selectedWebUsers.indexOf(id) >= 0) {
                                $scope.selectedWebUsers.splice($scope.selectedWebUsers.indexOf(id), 1);
                                delete $scope.selectedWebUserRecords[id];
                            }
                        }
                        break;
                    }
                }
            };
            $scope.allChecked = function() {
                for (var i = $scope.dtData.length - 1; i >= 0; i--) {
                    if (!$scope.dtData[i].checked) return false;
                }
                return true;
            };
            $scope.isActionPermitted = $rootScope.isActionPermitted;
            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withPaginationType('full_numbers')
                .withOption('stateSave', true)
                .withOption('ajax', WebUserService.dataTable)
                .withDataProp('data')
                .withOption('processing', true)
                .withOption('serverSide', true)
                .withOption('oLanguage', {
                    "sEmptyTable": "No matching records found"
                })
                .withOption('order', [1, 'asc'])
                .withOption('drawCallback', function(settings) {
                    var api = this.api(),
                        listOfRecords = api.data(),
                        state = api.state(),
                        filterRow = angular.element(api.table().header()).children()[1];
                    $(filterRow).children().each(function(index, child) {
                        if ($(child).has('.form-control'))
                            $(child).children('.form-control').val(state.columns[index].search.search);
                    });
                    $scope.dtData = [];
                    for (var i = listOfRecords.length - 1; i >= 0; i--) {
                        $scope.dtData[i] = (listOfRecords[i]);
                        $scope.dtData[i].checked = ($scope.selectedWebUsers.indexOf($scope.dtData[i]._id) >= 0);
                    }
                    $compile('website-user-record-select')($scope);
                    $compile('website-user-record-options')($scope);
                    $compile('website-user-record-actions')($scope);
                })
                .withPaginationType('full_numbers')
                .withOption('aoColumns', [{
                    title: '<website-user-record-options is-action-permitted="isActionPermitted" open-delete-modal="openDeleteModal" selected-record-list="selectedWebUsers" dt-data="dtData" all-checked="allChecked" toggle-all-records="toggleAllRecords"></website-user-record-options>',
                    orderable: false,
                    class: 'text-center',
                    width: '1%',
                    data: '_id',
                    render: function(id) {
                        return '<website-user-record-select dt-data="dtData" selected-record-list="selectedWebUsers" record-id="' + id + '" toggle-record="toggleRecord"></website-user-record-select>';
                    }
                }, {
                    title: 'Subscriber Name',
                    width: '69%',
                    name: 'name',
                    data: 'completeName',
                    render: function(name) {
                        return name || "";
                    }
                }, {
                    title: 'Status',
                    width: '10%',
                    // class: 'text-center',
                    orderable: false,
                    name: 'status',
                    data: 'active',
                    render: function(active) {
                        return active ? '<span class="">Active</span>' : '<span class="">Inactive</span>';
                    }
                }, {
                    title: 'Registered',
                    width: '10%',
                    // class: 'text-center',
                    orderable: false,
                    name: 'registered',
                    data: 'auth.registered',
                    render: function(active) {
                        return active ? '<span class="">YES</span>' : '<span class="">NO</span>';
                    }
                }, {
                    title: 'Actions',
                    orderable: false,
                    class: 'text-center',
                    width: '10%',
                    data: '_id',
                    render: function(id) {
                        return '<website-user-record-actions open-delete-modal1="openDeleteModal1" dt-data="dtData" record-id="\'' + id + '\'"></website-user-record-actions>';
                    }
                }])
                .withLightColumnFilter({
                    '1': {
                        type: 'text',
                        time: 600
                    },
                    '2': {
                        type: 'select',
                        values: [{
                            value: true,
                            label: 'Active'
                        }, {
                            value: false,
                            label: 'Inactive'
                        }]
                    },
                    '3': {
                        type: 'select',
                        values: [{
                            value: true,
                            label: 'YES'
                        }, {
                            value: false,
                            label: 'NO'
                        }]
                    }
                });
            $rootScope.openDeleteModal = function() {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/website.user.remove.html',
                    resolve: {
                        WebUsers: function() {
                            return WebUserService.get({
                                id: $scope.selectedWebUsers
                            }).$promise;
                        }
                    },
                    controller: 'WebUserRemoveController'
                });
                modalInstance.result.then(function(messages) {
                    var success = 0,
                        error = 0;
                    for (var i in messages) {
                        if (messages[i].type == 'success') success++;
                        if (messages[i].type == 'error') error++;
                    }
                    messages = [];
                    if (success > 0) messages.push({
                        type: 'success',
                        header: 'Remove Subscriber',
                        message: success + " record(s) removed successfully!"
                    });
                    if (error > 0) messages.push({
                        type: 'error',
                        header: 'Remove Subscriber',
                        message: error + " record(s) could not be removed successfully!"
                    });
                    if (messages.length > 0) showMessages(messages);
                    $state.reload();
                });
            };
            $scope.openDeleteModal1 = function(recordId) {
                $scope.toggleAllRecords(false);
                $scope.selectedWebUsers = [recordId];
                $rootScope.openDeleteModal();
            };
        }
    ]);
