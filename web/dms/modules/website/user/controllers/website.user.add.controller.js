angular.module('website.user')
    .controller('WebUserAddController', [
        '$localStorage',
        '$rootScope',
        '$state',
        '$stateParams',
        '$scope',
        '$uibModal',
        'WebUserService',
        'SubscriptionPackages',
        'SubscriberTypes',
        function($localStorage, $rootScope, $state, $stateParams, $scope, $uibModal, WebUserService, SubscriptionPackages, SubscriberTypes) {
            $rootScope.page = {
                name: 'Add Subscriber',
                actions: {
                    back: {
                        object: 'website.user.list',
                        allowed: function() {
                            return $rootScope.isActionPermitted(this.object);
                        }
                    }
                }
            };
            $scope.loading = false;
            $scope.user = {
                orders: {},
                directoryId: $localStorage['DMS-DIRECTORY']._id,
                active: true
            };
            // $scope.accessDateOptions = {
            //     minDate: new Date($scope.user.accessDate),
            //     maxDate: new Date($scope.user.expiryDate)
            // };
            // $scope.expiryDateOptions = {
            //     minDate: new Date($scope.user.accessDate),
            //     maxDate: new Date($scope.user.expiryDate)
            // };
            // $scope.$watch('user.expiryDate', function(newVal) {
            //     $scope.accessDateOptions.maxDate = new Date(newVal || '');
            //     $scope.expiryDateOptions.maxDate = new Date(newVal || '');
            // });
            // $scope.$watch('user.accessDate', function(newVal) {
            //     $scope.accessDateOptions.minDate = new Date(newVal || '');
            //     $scope.expiryDateOptions.minDate = new Date(newVal || '');
            // });
            // $scope.openOrderEditModal = function(index) {
            //     var modalInstance = $uibModal.open({
            //         backdrop: 'static',
            //         animation: true,
            //         size: 'lg',
            //         templateUrl: '/views/website.user.order.add.popup.html',
            //         controller: [
            //             '$scope',
            //             '$uibModalInstance',
            //             function($modalScope, $uibModalInstance) {
            //                 var editFlag = (typeof index != 'undefined');
            //                 $modalScope.order = editFlag ? getObjectData($scope.user.orders[index]) : {
            //                     confirmed: { flag: true }
            //                 };
            //                 $modalScope.packageArray = SubscriptionPackages;
            //                 $modalScope.subscriberTypeArray = SubscriberTypes[0].subData[0].subscriberTypes;
            //                 var stMap = {};
            //                 $modalScope.subscriberTypeArray.forEach(function(st, i) {
            //                     stMap[st.type] = i;
            //                 });

            //                 if (!$modalScope.order.subscription) $modalScope.order.subscription = {};

            //                 $modalScope.order.subscription.from = $modalScope.order.subscription.from ? new Date($modalScope.order.subscription.from) : '';
            //                 $modalScope.order.subscription.to = $modalScope.order.subscription.to ? new Date($modalScope.order.subscription.to) : '';
            //                 $modalScope.accessDateOptions = {
            //                     minDate: new Date($modalScope.order.subscription.from),
            //                     maxDate: new Date($modalScope.order.subscription.to)
            //                 };
            //                 $modalScope.expiryDateOptions = {
            //                     minDate: new Date($modalScope.order.subscription.from),
            //                     maxDate: new Date($modalScope.order.subscription.to)
            //                 };
            //                 $modalScope.$watch('order.subscription.to', function(newVal) {
            //                     $modalScope.accessDateOptions.maxDate = new Date(newVal || '');
            //                     $modalScope.expiryDateOptions.maxDate = new Date(newVal || '');
            //                 });
            //                 $modalScope.$watch('order.subscription.from', function(newVal) {
            //                     $modalScope.accessDateOptions.minDate = new Date(newVal || '');
            //                     $modalScope.expiryDateOptions.minDate = new Date(newVal || '');
            //                 });

            //                 $modalScope.changeOrderConfirmation = function() {
            //                     if ($modalScope.order.confirmed.flag) {
            //                         $modalScope.order.confirmed = {
            //                             flag: $modalScope.order.confirmed.flag,
            //                             by: $rootScope.currentUser._id,
            //                             at: new Date()
            //                         }
            //                         $scope.user.updatedOrderIndex = index;
            //                     } else {
            //                         $modalScope.order.confirmed = {
            //                             flag: $modalScope.order.confirmed.flag
            //                         }
            //                         if ($scope.user.updatedOrderIndex) delete $scope.user.updatedOrderIndex;
            //                     }
            //                 };

            //                 $modalScope.edit = function() {
            //                     if (!$scope.user.orders) $scope.user.orders = [];
            //                     if (editFlag) $scope.user.orders[index] = $modalScope.order;
            //                     else $scope.user.orders.push($modalScope.order);
            //                     $modalScope.cancel();
            //                 };

            //                 $modalScope.cancel = function() {
            //                     $uibModalInstance.dismiss('cancel');
            //                 };
            //             }
            //         ]
            //     });
            //     modalInstance.result.then(function(user) {
            //         $state.reload();
            //     });
            // };

            $scope.packageArray = SubscriptionPackages;
            $scope.subscriberTypeArray = SubscriberTypes[0].subData[0].subscriberTypes;
            var stMap = {};
            $scope.subscriberTypeArray.forEach(function(st, i) {
                stMap[st.type] = i;
            });

            $scope.changePackageDetails = function() {
                $scope.user.orders.packageName = $scope.user.orders.package.packageName;
                if ($scope.user.orders.typeName) {
                    var packageCostType = $scope.subscriberTypeArray[stMap[$scope.user.orders.typeName]].costPrefix;
                    $scope.user.orders.ppu = $scope.user.orders.package[packageCostType + "ackageCost"];
                }
            };

            $scope.add = function add() {
                $scope.loading = true;
                $scope.user.completeName = [$scope.user.name.first, $scope.user.name.middle, $scope.user.name.last].join(' ');
                $scope.user.viaAdmin = true;
                WebUserService.save($scope.user, function(res) {
                    showMessages([{
                        type: 'success',
                        message: 'Subscriber added successfully!',
                        header: 'Add Subscriber'
                    }]);
                    $state.go('portal.website.user.list');
                }, function(err) {
                    console.log(err);
                    showMessages([{
                        type: 'error',
                        message: 'Subscriber could not be added successfully!',
                        header: 'Add Subscriber'
                    }]);
                })
            };

            $scope.checkExistingUser = function() {
                var orderData = $scope.user.orders;
                WebUserService.get({
                    'directoryId': $localStorage['DMS-DIRECTORY']._id,
                    'auth.email': $scope.user.auth.email,
                    active: true
                }, function(users) {
                    if (users && users.length > 0) {
                        $scope.user = users[0];
                        $scope.user.orders = orderData;
                    }
                }, function(err) {
                    console.log('Existing user check err', err);
                })
            };
        }
    ]);
