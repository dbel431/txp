angular.module('website.user').directive('validateIsSubscriberEmailUnique', [
    '$localStorage',
    'WebUserService',
    function($localStorage, WebUserService) {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function(scope, iElm, iAttrs, ngModel) {
                ngModel.$validators.validateIsSubscriberEmailUnique = function(modelValue) {
                    console.log(ngModel.$error);
                    if (modelValue == '' || modelValue == undefined) return true;
                    if (ngModel.$error && Object.keys(ngModel.$error).length == 0) {
                        var auth = new WebUserService();
                        return WebUserService.get({
                            'directoryId': $localStorage['DMS-DIRECTORY']._id,
                            'auth.email': modelValue
                        }, function(users) {
                            if (users.length == 1 && iAttrs.exceptUserId) {
                                ngModel.$setValidity('validateIsSubscriberEmailUnique', (users[0]._id == iAttrs.exceptUserId));
                            } else {
                                ngModel.$setValidity('validateIsSubscriberEmailUnique', (users.length <= 0));
                            }
                        }, function(err) {
                            ngModel.$setValidity('validateIsSubscriberEmailUnique', false);
                        });
                    }

                };
            }
        };
    }
]);
