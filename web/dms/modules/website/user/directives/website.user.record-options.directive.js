angular.module('website').directive('websiteUserRecordOptions', function() {
    return {
    	scope: {
    		'selectedRecordList' : '=',
            'toggleAllRecords': '=',
            'isActionPermitted': '=',
            'openDeleteModal': '=',
            'allChecked': '=',
    	},
        restrict: 'EA',
        templateUrl: '/views/website.user.record-options.tpl.html',
        replace: true
    };
});
