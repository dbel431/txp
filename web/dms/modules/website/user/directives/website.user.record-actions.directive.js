angular.module('website').directive('websiteUserRecordActions', [
    '$rootScope',
    function($rootScope) {
        return {
            scope: {
                'dtData': '=',
                'recordId': '=',
                'openDeleteModal1': '=',
            },
            restrict: 'EA',
            templateUrl: '/views/website.user.record-actions.tpl.html',
            replace: true,
            link: function(scope, elt, attb) {
                scope.isActionPermitted = $rootScope.isActionPermitted;
                scope.record = {};
                if (scope.dtData) {
                    for (var i = 0; i < scope.dtData.length; i++) {
                        if (scope.dtData[i]._id == scope.recordId) {
                            scope.record = scope.dtData[i];
                            break;
                        }
                    }
                }
            }
        };
    }
]);
