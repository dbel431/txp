angular.module('website.user').service('WebUserService', [
    '$rootScope',
    '$localStorage',
    '$resource',
    function($rootScope, $localStorage, $resource) {
        return $resource(API_PATH + '/web-user', {}, {
            get: {
                method: 'GET',
                isArray: true
            },
            dataTable: {
                method: 'POST',
                url: API_PATH + '/web-user/data-table',
                transformRequest: function(data, headerGetter) {
                    var headers = headerGetter();
                    var newData = {
                        dataTableQuery: data,
                        conditions: {
                            directoryId: $localStorage['DMS-DIRECTORY']._id
                        }
                    }
                    return JSON.stringify(newData);
                }
            },
            update: {
                method: 'PUT'
            },
            confirmSubscription: {
                method: 'PUT',
                url: API_PATH + '/web-user/confirm-subscription'
            }
        });
    }
]);
