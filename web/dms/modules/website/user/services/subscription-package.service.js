angular.module('website')
    .service('SubscriptionPackageService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/subscription-package', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                },
                getSubscriberTypes: {
                    url: API_PATH + '/subscription-package/subscriber-type',
                    method: 'GET',
                    isArray: true
                },
                list: {
                    url: API_PATH + '/subscription-package/list',
                    method: 'POST',
                    isArray: true
                },
                update: {
                    method: 'PUT'
                }
            });
        }
    ]);
