angular.module('website.user', [
        'website'
    ])
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $stateProvider.state('portal.website.user', {
                url: '/user',
                template: '<ui-view></ui-view>',
                controller: 'WebUserController',
            }).state('portal.website.user.list', {
                url: '/list',
                templateUrl: '/views/website.user.list.html',
                controller: 'WebUserListController'
            }).state('portal.website.user.add', {
                url: '/add',
                templateUrl: '/views/website.user.add.html',
                resolve: {
                    'SubscriptionPackages': [
                        '$rootScope',
                        '$localStorage',
                        'SubscriptionPackageService',
                        function($rootScope, $localStorage, SubscriptionPackageService) {
                            return SubscriptionPackageService.get({
                                directoryId: $localStorage['DMS-DIRECTORY']._id,
                                active: true
                            }).$promise;
                        }
                    ],
                    'SubscriberTypes': [
                        '$rootScope',
                        '$localStorage',
                        'SubscriptionPackageService',
                        function($rootScope, $localStorage, SubscriptionPackageService) {
                            return SubscriptionPackageService.getSubscriberTypes({
                                pageName: 'subscription',
                                directoryName: $localStorage['DMS-DIRECTORY'].codeValue
                            }).$promise;
                        }
                    ]
                },
                controller: 'WebUserAddController'
            }).state('portal.website.user.edit', {
                url: '/edit/:id',
                templateUrl: '/views/website.user.html',
                resolve: {
                    'SubscriptionPackages': [
                        '$rootScope',
                        '$localStorage',
                        'SubscriptionPackageService',
                        function($rootScope, $localStorage, SubscriptionPackageService) {
                            return SubscriptionPackageService.get({
                                directoryId: $localStorage['DMS-DIRECTORY']._id,
                                active: true
                            }).$promise;
                        }
                    ],
                    'WebUser': [
                        'WebUserService',
                        '$stateParams',
                        function(WebUserService, $stateParams) {
                            return WebUserService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ]
                },
                controller: 'WebUserEditController'
            }).state('portal.website.user.view', {
                url: '/view/:id',
                templateUrl: '/views/website.user.view.html',
                resolve: {
                    'WebUser': [
                        'WebUserService',
                        '$stateParams',
                        function(WebUserService, $stateParams) {
                            return WebUserService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ]
                },
                controller: 'WebUserViewController'
            }).state('portal.website.user.confirmSubscription', {
                url: '/confirmSubscription/:id',
                templateUrl: '/views/website.user.confirm-subscription.html',
                resolve: {
                    'WebUser': [
                        'WebUserService',
                        '$stateParams',
                        function(WebUserService, $stateParams) {
                            return WebUserService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ]
                },
                controller: 'WebUserConfirmSubscriptionController'
            }).state('portal.website.user.remove', {
                url: '/remove/:id',
                templateUrl: '/views/website.user.remove.html',
                resolve: {
                    'WebUser': [
                        'WebUserService',
                        '$stateParams',
                        function(WebUserService, $stateParams) {
                            return WebUserService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ]
                },
                controller: 'WebUserRemoveController'
            });
        }
    ]);
