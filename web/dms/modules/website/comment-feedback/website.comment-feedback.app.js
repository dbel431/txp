angular.module('website.commentFeedback', [
        'website'
    ])
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $stateProvider.state('portal.website.commentFeedback', {
                url: '/comment-feedback',
                template: '<ui-view></ui-view>',
                controller: 'WebCommentFeedbackController',
            }).state('portal.website.commentFeedback.list', {
                url: '/list',
                templateUrl: '/views/website.comment-feedback.list.html',
                controller: 'WebCommentFeedbackListController'
            }).state('portal.website.commentFeedback.edit', {
                url: '/edit/:id',
                templateUrl: '/views/website.comment-feedback.html',
                resolve: {
                    'WebCommentFeedback': [
                        'WebCommentFeedbackService',
                        '$stateParams',
                        function(WebCommentFeedbackService, $stateParams) {
                            return WebCommentFeedbackService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ]
                },
                controller: 'WebCommentFeedbackEditController'
            }).state('portal.website.commentFeedback.view', {
                url: '/view/:id',
                templateUrl: '/views/website.comment-feedback.view.html',
                resolve: {
                    'WebCommentFeedback': [
                        'WebCommentFeedbackService',
                        '$stateParams',
                        function(WebCommentFeedbackService, $stateParams) {
                            return WebCommentFeedbackService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ]
                },
                controller: 'WebCommentFeedbackViewController'
            }).state('portal.website.commentFeedback.remove', {
                url: '/remove/:id',
                templateUrl: '/views/website.comment-feedback.remove.html',
                resolve: {
                    'WebCommentFeedback': [
                        'WebCommentFeedbackService',
                        '$stateParams',
                        function(WebCommentFeedbackService, $stateParams) {
                            return WebCommentFeedbackService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ]
                },
                controller: 'WebCommentFeedbackRemoveController'
            });
        }
    ]);
