angular.module('website.commentFeedback')
    .controller('WebCommentFeedbackController', [
        '$rootScope',
        '$scope',
        '$state',
        function($rootScope, $scope, $state) {
            $rootScope.page = {
                name: 'Comment Feedbacks'
            };
        }
    ]);
