angular.module('website.commentFeedback')
    .controller('WebCommentFeedbackEditController', [
        '$rootScope',
        '$state',
        '$stateParams',
        '$scope',
        'WebCommentFeedback',
        function($rootScope, $state, $stateParams, $scope, WebCommentFeedback) {
            $rootScope.page = {
                name: 'Edit Comment Feedback',
                actions: {
                    back: {
                        object: 'website.commentFeedback.list',
                        allowed: function() {
                            return $rootScope.isActionPermitted(this.object);
                        }
                    }
                }
            };
            $scope.loading = false;
            $scope.progressStatusArray = [
                { id: 1, label: 'Idle' },
                { id: 2, label: 'Under Review' },
                { id: 3, label: 'Approved' },
                { id: 4, label: 'Rejected' },
            ];
            $scope.commentFeedback = WebCommentFeedback[0];
            // ha 
            for (var i = 0; i < $scope.progressStatusArray.length; i++) {
                if ($scope.commentFeedback.progress == $scope.progressStatusArray[i].id) {
                    $scope.commentFeedback.progress = $scope.progressStatusArray[i];
                }
            }
            // portion
            $scope.commentIndex = ($scope.commentFeedback.adminComments).length;
            $scope.commentFeedback.adminComments[$scope.commentIndex] = {
                by: $rootScope.currentUser._id,
                prevProgress: $scope.commentFeedback.progress
            };
            $scope.update = function() {
                $scope.commentFeedback.progress = $scope.commentFeedback.progress.id; // hi line
                $scope.commentFeedback.adminComments[$scope.commentIndex].prevProgress = $scope.commentFeedback.adminComments[$scope.commentIndex].prevProgress.id; // hi pan
                $scope.commentFeedback.adminComments[$scope.commentIndex].at = new Date();
                $scope.commentFeedback.$update(function(commentFeedback) {
                    showMessages([{
                        type: 'success',
                        message: 'Request Details updated successfully!',
                        header: 'Edit  Comment Feedback'
                    }]);
                    $state.go('portal.website.commentFeedback');
                }, function(err) {
                    console.log('update err', err);
                    showMessages([{
                        type: 'error',
                        message: ((err.data ? err.data.message : false) || 'Request Details could not be updated successfully!'),
                        header: 'Edit  Comment Feedback'
                    }]);
                });
            };
        }
    ]);
