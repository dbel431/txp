angular.module('website.commentFeedback')
    .controller('WebCommentFeedbackListController', [
        '$rootScope',
        '$scope',
        '$state',
        '$compile',
        '$uibModal',
        'DTOptionsBuilder',
        'WebCommentFeedbackService',
        function($rootScope, $scope, $state, $compile, $uibModal, DTOptionsBuilder, WebCommentFeedbackService) {
            var progressMap = {
                    1: 'Idle',
                    2: 'Under Review',
                    3: 'Approved',
                    4: 'Rejected'
                },
                progressKeyValMap = [{
                    value: 1,
                    label: 'Idle'
                }, {
                    value: 2,
                    label: 'Under Review'
                }, {
                    value: 3,
                    label: 'Approved'
                }, {
                    value: 4,
                    label: 'Rejected'
                }];
            $rootScope.page = {
                name: 'Comment Feedback List',
            };
            $scope.selectedWebCommentFeedbacks = [];
            $scope.selectedWebCommentFeedbacksRecords = {};
            $scope.dtData = [];
            $scope.toggleAllRecords = function(select) {
                for (var i = $scope.dtData.length - 1; i >= 0; i--) {
                    $scope.dtData[i].checked = select || false;
                    if (select) {
                        if ($scope.selectedWebCommentFeedbacks.indexOf($scope.dtData[i]._id) < 0) {
                            $scope.selectedWebCommentFeedbacks.push($scope.dtData[i]._id);
                            $scope.selectedWebCommentFeedbacksRecords[$scope.dtData[i]._id] = $scope.dtData[i];
                        }
                    } else if ($scope.selectedWebCommentFeedbacks.indexOf($scope.dtData[i]._id) >= 0) {
                        if ($scope.selectedWebCommentFeedbacks.indexOf($scope.dtData[i]._id) >= 0) {
                            $scope.selectedWebCommentFeedbacks.splice($scope.selectedWebCommentFeedbacks.indexOf($scope.dtData[i]._id), 1);
                            delete $scope.selectedWebCommentFeedbacksRecords[$scope.dtData[i]._id];
                        }
                    }
                }
            };
            $scope.toggleRecord = function(id) {
                for (var i = $scope.dtData.length - 1; i >= 0; i--) {
                    if ($scope.dtData[i]._id == id) {
                        if ($scope.dtData[i].checked) {
                            if ($scope.selectedWebCommentFeedbacks.indexOf(id) < 0) {
                                $scope.selectedWebCommentFeedbacks.push(id);
                                $scope.selectedWebCommentFeedbacksRecords[id] = $scope.dtData[i];
                            }
                        } else {
                            if ($scope.selectedWebCommentFeedbacks.indexOf(id) >= 0) {
                                $scope.selectedWebCommentFeedbacks.splice($scope.selectedWebCommentFeedbacks.indexOf(id), 1);
                                delete $scope.selectedWebCommentFeedbacksRecords[id];
                            }
                        }
                        break;
                    }
                }
            };
            $scope.allChecked = function() {
                for (var i = $scope.dtData.length - 1; i >= 0; i--) {
                    if (!$scope.dtData[i].checked) return false;
                }
                return true;
            };
            $scope.isActionPermitted = $rootScope.isActionPermitted;
            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withPaginationType('full_numbers')
                .withOption('stateSave', true)
                .withOption('ajax', WebCommentFeedbackService.dataTable)
                .withDataProp('data')
                .withOption('processing', true)
                .withOption('serverSide', true)
                .withOption('oLanguage', {
                    "sEmptyTable": "No matching records found"
                })
                .withOption('order', [2, 'asc'])
                .withOption('drawCallback', function(settings) {
                    var api = this.api(),
                        listOfRecords = api.data(),
                        state = api.state(),
                        filterRow = angular.element(api.table().header()).children()[1];
                    $(filterRow).children().each(function(index, child) {
                        if ($(child).has('.form-control'))
                            $(child).children('.form-control').val(state.columns[index].search.search);
                    });
                    $scope.dtData = [];
                    for (var i = listOfRecords.length - 1; i >= 0; i--) {
                        $scope.dtData[i] = (listOfRecords[i]);
                        $scope.dtData[i].checked = ($scope.selectedWebCommentFeedbacks.indexOf($scope.dtData[i]._id) >= 0);
                    }
                    $compile('website-comment-feedback-record-select')($scope);
                    $compile('website-comment-feedback-record-options')($scope);
                    $compile('website-comment-feedback-record-actions')($scope);
                })
                .withPaginationType('full_numbers')
                .withOption('aoColumns', [{
                    title: '<website-comment-feedback-record-options is-action-permitted="isActionPermitted" open-delete-modal="openDeleteModal" selected-record-list="selectedWebCommentFeedbacks" dt-data="dtData" all-checked="allChecked" toggle-all-records="toggleAllRecords"></website-comment-feedback-record-options>',
                    orderable: false,
                    class: 'text-center',
                    width: '1%',
                    data: '_id',
                    render: function(id) {
                        return '<website-comment-feedback-record-select dt-data="dtData" selected-record-list="selectedCommentFeedbacks" record-id="' + id + '" toggle-record="toggleRecord"></website-comment-feedback-record-select>';
                    }
                }, {
                    title: 'Name',
                    width: '13%',
                    name: 'fname',
                    data: 'name.first',
                    render: function(fname) {
                        return fname || "";
                    }
                }, {
                    title: 'Email',
                    width: '13%',
                    name: 'email',
                    data: 'email',
                    render: function(email) {
                        return email || "";
                    }
                }, {
                    title: 'Progress',
                    width: '10%',
                    orderable: false,
                    name: 'progress',
                    data: 'progress',
                    render: function(progress) {
                        return progressMap[progress];
                    }
                }, {
                    title: 'Status',
                    width: '10%',
                    orderable: false,
                    name: 'status',
                    data: 'active',
                    render: function(active) {
                        return active ? '<span class="">Active</span>' : '<span class="">Inactive</span>';
                    }
                }, {
                    title: 'Actions',
                    orderable: false,
                    class: 'text-center',
                    width: '6%',
                    data: '_id',
                    render: function(id) {
                        return '<website-comment-feedback-record-actions open-delete-modal1="openDeleteModal1" open-view-comments-modal="openViewCommentsModal" dt-data="dtData" record-id="\'' + id + '\'"></website-comment-feedback-record-actions>';
                    }
                }])
                .withLightColumnFilter({
                    '1': {
                        type: 'text',
                        time: 600
                    },
                    '2': {
                        type: 'text',
                        time: 600
                    },
                    '3': {
                        type: 'select',
                        values: progressKeyValMap
                    },
                    '4': {
                        type: 'select',
                        values: [{
                            value: true,
                            label: 'Active'
                        }, {
                            value: false,
                            label: 'Inactive'
                        }]
                    }
                });
            $rootScope.openDeleteModal = function() {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/website.comment-feedback.remove.html',
                    resolve: {
                        WebCommentFeedbacks: function() {
                            return WebCommentFeedbackService.get({
                                id: $scope.selectedWebCommentFeedbacks
                            }).$promise;
                        }
                    },
                    controller: 'WebCommentFeedbackRemoveController'
                });
                modalInstance.result.then(function(messages) {
                    var success = 0,
                        error = 0;
                    for (var i in messages) {
                        if (messages[i].type == 'success') success++;
                        if (messages[i].type == 'error') error++;
                    }
                    messages = [];
                    if (success > 0) messages.push({
                        type: 'success',
                        header: 'Remove Comment Feedback',
                        message: success + " record(s) removed successfully!"
                    });
                    if (error > 0) messages.push({
                        type: 'error',
                        header: 'Remove Comment Feedback',
                        message: error + " record(s) could not be removed successfully!"
                    });
                    if (messages.length > 0) showMessages(messages);
                    $state.reload();
                });
            };
            $scope.openDeleteModal1 = function(recordId) {
                $scope.toggleAllRecords(false);
                $scope.selectedWebCommentFeedbacks = [recordId];
                $rootScope.openDeleteModal();
            };
            $scope.openViewCommentsModal = function(id) {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/website.comment-feedback.comments.html',
                    resolve: {
                        CommentFeedback: function() {
                            return WebCommentFeedbackService.get({ id: id }).$promise;
                        }
                    },
                    controller: [
                        '$rootScope',
                        '$scope',
                        '$uibModalInstance',
                        'CommentFeedback',
                        function($rootScope, $modalScope, $uibModalInstance, CommentFeedback) {
                            $modalScope.progressMap = progressMap;
                            $modalScope.comments = CommentFeedback[0].adminComments.reverse();
                            for (var i = 0; i < $modalScope.comments.length; i++) {
                                if (i == 0) $modalScope.comments[i].toProgress = CommentFeedback[0].progress;
                                else $modalScope.comments[i].toProgress = $modalScope.comments[(i - 1)].prevProgress;
                            }
                            $modalScope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    ]
                });
            };
        }
    ]);
