angular.module('website.commentFeedback')
    .controller('WebCommentFeedbackRemoveController', [
        '$rootScope',
        '$scope',
        '$uibModalInstance',
        'WebCommentFeedbacks',
        function($rootScope, $scope, $uibModalInstance, WebCommentFeedbacks) {
            $scope.webCommentFeedbacks = WebCommentFeedbacks;
            $scope.cancel = function() {
                $uibModalInstance.dismiss('cancel');
            };
            var messages = [];
            $scope.remove = function() {
                for (var i = $scope.webCommentFeedbacks.length - 1; i >= 0; i--) {
                    $scope.webCommentFeedbacks[i].$remove({
                        id: $scope.webCommentFeedbacks[i]._id
                    }, function(webCommentFeedback) {
                        messages.push({
                            type: 'success',
                            message: 'Record named: <b>' + webCommentFeedback.name + '</b> deleted successfully!',
                            header: 'Remove Record'
                        });
                        syncXhr();
                    }, function(err) {
                        messages.push({
                            type: 'error',
                            message: 'Record named: <b>' + webCommentFeedback.name + '</b> could not be deleted successfully!',
                            header: 'Remove Record'
                        });
                        syncXhr();
                    });
                }
            };
            var count = 0;

            function syncXhr() {
                count++;
                if ($scope.loading = (count == $scope.webCommentFeedbacks.length)) $uibModalInstance.close(messages);
            }
        }
    ]);
