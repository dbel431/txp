angular.module('website.commentFeedback')
    .controller('WebCommentFeedbackViewController', [
        '$rootScope',
        '$state',
        '$stateParams',
        '$scope',
        'WebCommentFeedback',
        function($rootScope, $state, $stateParams, $scope, WebCommentFeedback) {
            $rootScope.page = {
                name: 'View Comment Feedback',
                actions: {
                    back: {
                        object: 'website.commentFeedback.list',
                        allowed: function() {
                            return $rootScope.isActionPermitted(this.object);
                        }
                    }
                }
            };
            $scope.loading = false;
            $scope.commentFeedback = WebCommentFeedback[0];
            $scope.progressMap = {
                1: 'Idle',
                2: 'Under Review',
                3: 'Approved',
                4: 'Rejected'
            };
        }
    ]);
