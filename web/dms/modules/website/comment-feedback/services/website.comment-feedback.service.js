angular.module('website.commentFeedback').service('WebCommentFeedbackService', [
    '$rootScope',
    '$localStorage',
    '$resource',
    function($rootScope, $localStorage, $resource) {
        return $resource(API_PATH + '/web-request', {}, {
            get: {
                method: 'GET',
                isArray: true
            },
            dataTable: {
                method: 'POST',
                url: API_PATH + '/web-request/data-table',
                transformRequest: function(data, headerGetter) {
                    var headers = headerGetter();
                    var newData = {
                        dataTableQuery: data,
                        conditions: {
                            directoryId: $localStorage['DMS-DIRECTORY']._id,
                            requestType: 'Comment'
                        }
                    }
                    return JSON.stringify(newData);
                }
            },
            update: {
                method: 'PUT'
            }
        });
    }
]);
