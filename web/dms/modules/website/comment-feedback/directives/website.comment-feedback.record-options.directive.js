angular.module('website').directive('websiteCommentFeedbackRecordOptions', function() {
    return {
        scope: {
            'selectedRecordList': '=',
            'toggleAllRecords': '=',
            'isActionPermitted': '=',
            'openDeleteModal': '=',
            'allChecked': '=',
        },
        restrict: 'EA',
        templateUrl: '/views/website.comment-feedback.record-options.tpl.html',
        replace: true
    };
});
