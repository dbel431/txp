angular.module('website', [
        'portal'
    ])
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $stateProvider.state('portal.website', {
                url: '/website',
                templateUrl: '/views/core.portal.page.tpl.html',
                controller: 'WebsiteController'
            });
        }
    ])
    .controller('WebsiteController', [
        '$state',
        '$scope',
        function($state, $scope) {}
    ]);
