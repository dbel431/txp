angular.module('website.nrpInquiry', [
        'website'
    ])
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $stateProvider.state('portal.website.nrpInquiry', {
                url: '/nrp-inquiry',
                template: '<ui-view></ui-view>',
                controller: 'WebNRPInquiryController',
            }).state('portal.website.nrpInquiry.list', {
                url: '/list',
                templateUrl: '/views/website.nrp-inquiry.list.html',
                controller: 'WebNRPInquiryListController'
            }).state('portal.website.nrpInquiry.edit', {
                url: '/edit/:id',
                templateUrl: '/views/website.nrp-inquiry.html',
                resolve: {
                    'WebNRPInquiry': [
                        'WebNRPInquiryService',
                        '$stateParams',
                        function(WebNRPInquiryService, $stateParams) {
                            return WebNRPInquiryService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ]
                },
                controller: 'WebNRPInquiryEditController'
            }).state('portal.website.nrpInquiry.view', {
                url: '/view/:id',
                templateUrl: '/views/website.nrp-inquiry.view.html',
                resolve: {
                    'WebNRPInquiry': [
                        'WebNRPInquiryService',
                        '$stateParams',
                        function(WebNRPInquiryService, $stateParams) {
                            return WebNRPInquiryService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ]
                },
                controller: 'WebNRPInquiryViewController'
            }).state('portal.website.nrpInquiry.remove', {
                url: '/remove/:id',
                templateUrl: '/views/website.nrp-inquiry.remove.html',
                resolve: {
                    'WebNRPInquiry': [
                        'WebNRPInquiryService',
                        '$stateParams',
                        function(WebNRPInquiryService, $stateParams) {
                            return WebNRPInquiryService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ]
                },
                controller: 'WebNRPInquiryRemoveController'
            });
        }
    ]);
