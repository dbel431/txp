angular.module('website.nrpInquiry')
    .controller('WebNRPInquiryEditController', [
        '$rootScope',
        '$state',
        '$stateParams',
        '$scope',
        'WebNRPInquiry',
        function($rootScope, $state, $stateParams, $scope, WebNRPInquiry) {
            $rootScope.page = {
                name: 'Edit NRP Inquiry',
                actions: {
                    back: {
                        object: 'website.nrpInquiry.list',
                        allowed: function() {
                            return $rootScope.isActionPermitted(this.object);
                        }
                    }
                }
            };
            $scope.loading = false;
            $scope.progressStatusArray = [
                { id: 1, label: 'Idle' },
                { id: 2, label: 'Under Review' },
                { id: 3, label: 'Approved' },
                { id: 4, label: 'Rejected' },
            ];
            $scope.nrpInquiry = WebNRPInquiry[0];
            // ha 
            for (var i = 0; i < $scope.progressStatusArray.length; i++) {
                if ($scope.nrpInquiry.progress == $scope.progressStatusArray[i].id) {
                    $scope.nrpInquiry.progress = $scope.progressStatusArray[i];
                }
            }
            // portion
            $scope.commentIndex = ($scope.nrpInquiry.adminComments).length;
            $scope.nrpInquiry.adminComments[$scope.commentIndex] = {
                by: $rootScope.currentUser._id,
                prevProgress: $scope.nrpInquiry.progress
            };
            $scope.update = function() {
                $scope.nrpInquiry.progress = $scope.nrpInquiry.progress.id; // hi line
                $scope.nrpInquiry.adminComments[$scope.commentIndex].prevProgress = $scope.nrpInquiry.adminComments[$scope.commentIndex].prevProgress.id; // hi pan
                $scope.nrpInquiry.adminComments[$scope.commentIndex].at = new Date();
                $scope.nrpInquiry.$update(function(nrpInquiry) {
                    showMessages([{
                        type: 'success',
                        message: 'Request Details updated successfully!',
                        header: 'Edit  NRP Inquiry'
                    }]);
                    $state.go('portal.website.nrpInquiry');
                }, function(err) {
                    console.log('update err', err);
                    showMessages([{
                        type: 'error',
                        message: ((err.data ? err.data.message : false) || 'Request Details could not be updated successfully!'),
                        header: 'Edit  NRP Inquiry'
                    }]);
                });
            };
        }
    ]);
