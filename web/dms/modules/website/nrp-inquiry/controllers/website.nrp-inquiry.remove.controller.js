angular.module('website.nrpInquiry')
    .controller('WebNRPInquiryRemoveController', [
        '$rootScope',
        '$scope',
        '$uibModalInstance',
        'WebNRPInquirys',
        function($rootScope, $scope, $uibModalInstance, WebNRPInquirys) {
            $scope.webNRPInquirys = WebNRPInquirys;
            $scope.cancel = function() {
                $uibModalInstance.dismiss('cancel');
            };
            var messages = [];
            $scope.remove = function() {
                for (var i = $scope.webNRPInquirys.length - 1; i >= 0; i--) {
                    $scope.webNRPInquirys[i].$remove({
                        id: $scope.webNRPInquirys[i]._id
                    }, function(webNRPInquiry) {
                        messages.push({
                            type: 'success',
                            message: 'Record named: <b>' + webNRPInquiry.name + '</b> deleted successfully!',
                            header: 'Remove Record'
                        });
                        syncXhr();
                    }, function(err) {
                        messages.push({
                            type: 'error',
                            message: 'Record named: <b>' + webNRPInquiry.name + '</b> could not be deleted successfully!',
                            header: 'Remove Record'
                        });
                        syncXhr();
                    });
                }
            };
            var count = 0;

            function syncXhr() {
                count++;
                if ($scope.loading = (count == $scope.webNRPInquirys.length)) $uibModalInstance.close(messages);
            }
        }
    ]);
