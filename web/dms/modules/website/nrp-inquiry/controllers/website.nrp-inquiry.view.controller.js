angular.module('website.nrpInquiry')
    .controller('WebNRPInquiryViewController', [
        '$rootScope',
        '$state',
        '$stateParams',
        '$scope',
        'WebNRPInquiry',
        function($rootScope, $state, $stateParams, $scope, WebNRPInquiry) {
            $rootScope.page = {
                name: 'View NRP Inquiry',
                actions: {
                    back: {
                        object: 'website.nrpInquiry.list',
                        allowed: function() {
                            return $rootScope.isActionPermitted(this.object);
                        }
                    }
                }
            };
            $scope.loading = false;
            $scope.nrpInquiry = WebNRPInquiry[0];
            $scope.progressMap = {
                1: 'Idle',
                2: 'Under Review',
                3: 'Approved',
                4: 'Rejected'
            };
        }
    ]);
