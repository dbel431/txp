angular.module('website').directive('websiteNrpInquiryRecordOptions', function() {
    return {
        scope: {
            'selectedRecordList': '=',
            'toggleAllRecords': '=',
            'isActionPermitted': '=',
            'openDeleteModal': '=',
            'allChecked': '=',
        },
        restrict: 'EA',
        templateUrl: '/views/website.nrp-inquiry.record-options.tpl.html',
        replace: true
    };
});
