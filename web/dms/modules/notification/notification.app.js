angular.module('notification', [
        'portal'
    ])
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $stateProvider.state('portal.notification', {
                url: '/notification',
                templateUrl: '/views/core.portal.page.tpl.html',
                controller: 'NotificationController'
            })

        }
    ])
    .controller('NotificationController', [
        '$state',
        '$scope',
        function($state, $scope) {}
    ]);