angular.module('notification.event')
    .controller('EventRemoveController', [
        '$state',
        '$scope',
        'Event',
        function($state, $scope, Event) {
            $scope.loading = false;
            $scope.event = Event[0];
            $scope.remove = function() {
                $scope.loading = true;
                $scope.event.$remove({
                    id: $scope.event._id
                }, function(event) {
                    $scope.loading = false;
                    showMessages([{
                        type: 'success',
                        message: 'Event deleted successfully!',
                        header: 'Delete Event'
                    }]);
                    $state.go('portal.notification.event.list');
                }, function(err) {
                    $scope.loading = false;
                    showMessages([{
                        type: 'error',
                        message: err.data.message,
                        header: 'Remove Event'
                    }]);
                });
            };
        }
    ]);
