angular.module('notification.event')
    .controller('EventAddController', [
        '$state',
        '$scope',
        'EventService',

        function($state, $scope, EventService) {
            $scope.loading = false;
            $scope.event = new EventService();
            $scope.add = function() {
                $scope.loading = true;
                $scope.event.$save(function(event) {
                    showMessages([{
                        type: 'success',
                        message: 'Event : <b>' + event.name + '</b> added successfully!',
                        header: 'Add Event'
                    }]);
                    $state.go('portal.notification.event.list');
                    $scope.loading = false;
                }, function(err) {
                    showMessages([{
                        type: 'error',
                        message: err.data.message,
                        header: 'Add Event'
                    }]);
                    $scope.loading = false;
                })
            }


        }
    ]);
