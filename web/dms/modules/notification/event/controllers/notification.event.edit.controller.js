angular.module('notification.event')
    .controller('EventEditController', [
        '$state',
        '$stateParams',
        '$scope',
        'Event',
        function($state, $stateParams, $scope, Event) {
            $scope.loading = false;
            $scope.event = Event[0];

            $scope.edit = function() {
                $scope.loading = true;
                $scope.event.$update({
                    id: $scope.event._id
                }, function(event) {
                    showMessages([{
                        type: 'success',
                        message: 'Event edited successfully!',
                        header: 'Edit Event'
                    }]);
                    $state.go('portal.notification.event.list');
                    $scope.loading = false;
                }, function(err) {
                    showMessages([{
                        type: 'error',
                        message: err.data.message,
                        header: 'Edit event'
                    }]);
                    $scope.loading = false;
                })
            }
        }
    ]);
