angular.module('notification.event')
    .controller('EventListController', [
        '$scope',
        'DTOptionsBuilder',
        'Events',
        function($scope, DTOptionsBuilder, Events) {
            $scope.events = Events;
            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withPaginationType('full_numbers')
                .withColumnFilter({
                    aoColumns: [{
                        type: 'text',
                        bRegex: true,
                        bSmart: true
                    }, {
                        type: 'text',
                        bRegex: true,
                        bSmart: true
                    }, {
                        type: 'select',
                        bRegex: true,
                        values: [
                            { value: '^Active', label: 'Active' },
                            { value: '^Inactive', label: 'Inactive' }
                        ]
                    }]
                });



        }
    ]);