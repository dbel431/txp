angular.module('notification.event', [
        'notification'
    ])
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $stateProvider.state('portal.notification.event', {
                url: '/event',
                templateUrl: '/views/core.portal.action.tpl.html',
                controller: 'EventController'
            }).state('portal.notification.event.list', {
                url: '/list',
                templateUrl: '/views/notification.event.list.html',
                resolve: {
                    Events: [
                        'EventService',
                        function(EventService) {
                            return EventService.get().$promise;
                        }
                    ]
                },
                controller: 'EventListController'
            }).state('portal.notification.event.add', {
                url: '/add',
                templateUrl: '/views/notification.event.add.html',
                controller: 'EventAddController',
            }).state('portal.notification.event.edit', {
                url: '/edit/:id',
                templateUrl: '/views/notification.event.edit.html',
                resolve: {
                    'Event': [
                        'EventService',
                        '$stateParams',
                        function(EventService, $stateParams) {
                            return EventService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ]
                },
                controller: 'EventEditController'
            }).state('portal.notification.event.remove', {
                url: '/remove/:id',
                templateUrl: '/views/notification.event.remove.html',
                resolve: {
                    'Event': [
                        'EventService',
                        '$stateParams',
                        function(EventService, $stateParams) {
                            return EventService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ]
                },
                controller: 'EventRemoveController'
            });

        }
    ]);
