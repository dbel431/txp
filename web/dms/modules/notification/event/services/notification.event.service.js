angular.module('notification.event').service('EventService', [
    '$resource',
    function($resource) {
        return $resource(API_PATH + '/notification/event', {}, {
            get: {
                method: 'GET',
                isArray: true
            },
            update: {
                method: 'PUT'
            }
        });
    }
]);
