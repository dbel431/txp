angular.module('notification.template')
    .controller('TemplateAddController', [
        '$state',
        '$scope',
        'NotificationTemplateService',
        'NotificationEvents',

        function($state, $scope, NotificationTemplateService, NotificationEvents) {
            $scope.eventArray = NotificationEvents;
            $scope.template = new NotificationTemplateService();
            $scope.options = {
                // language: 'en',
                allowedContent: true
               // entities: false
                // filebrowserUploadUrl: '/upload'
            };
            $scope.selectedEvent = {
                value: $scope.template.event || $scope.eventArray[0]
            };
            $scope.loading = false;
            $scope.onReady = function() {
                // ... 
            };

            $scope.add = function() {
                $scope.template.event = $scope.selectedEvent.value._id;
                $scope.loading = true;
                $scope.template.$save(function(template) {
                    showMessages([{
                        type: 'success',
                        message: 'Template : <b>' + template.name + '</b> added successfully!',
                        header: 'Add Template'
                    }]);
                    $state.go('portal.notification.template.list');
                    $scope.loading = false;
                }, function(err) {
                    showMessages([{
                        type: 'error',
                        message: err.data.message,
                        header: 'Add Template'
                    }]);
                    $scope.loading = false;
                })
            }


        }
    ]);
