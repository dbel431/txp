angular.module('notification.template')
    .controller('TemplateListController', [
        '$scope',
        'DTOptionsBuilder',
        'Templates',
        function($scope, DTOptionsBuilder, Templates) {
            $scope.templates = Templates;
            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withPaginationType('full_numbers')
                .withColumnFilter({
                    aoColumns: [{
                        type: 'text',
                        bRegex: true,
                        bSmart: true
                    }, {
                        type: 'text',
                        bRegex: true,
                        bSmart: true
                    }, {
                        type: 'text',
                        bRegex: true,
                        bSmart: true
                    }, {
                        type: 'select',
                        bRegex: true,
                        values: [
                            { value: '^Active', label: 'Active' },
                            { value: '^Inactive', label: 'Inactive' }
                        ]
                    }]
                });



        }
    ]);