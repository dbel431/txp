angular.module('notification.template')
    .controller('TemplateRemoveController', [
        '$state',
        '$scope',
        'Template',
        function($state, $scope, Template) {
            $scope.loading = false;
            $scope.template = Template[0];
            $scope.remove = function() {
                $scope.loading = true;
                $scope.template.$remove({
                    id: $scope.template._id
                }, function(template) {
                    $scope.loading = false;
                    showMessages([{
                        type: 'success',
                        message: 'Template deleted successfully!',
                        header: 'Delete Template'
                    }]);
                    $state.go('portal.notification.template.list');
                }, function(err) {
                    $scope.loading = false;
                    showMessages([{
                        type: 'error',
                        message: err.data.message,
                        header: 'Remove Template'
                    }]);
                });
            };
        }
    ]);
