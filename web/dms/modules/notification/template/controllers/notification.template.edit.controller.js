angular.module('notification.template')
    .controller('TemplateEditController', [
        '$state',
        '$stateParams',
        '$scope',
        'Template',
        'NotificationEvents',
        function($state, $stateParams, $scope, Template, NotificationEvents) {
            $scope.eventArray = NotificationEvents;
            $scope.loading = false;
            $scope.template = Template[0];
            $scope.selectedEvent = {
                value: $scope.template.event 
            };
            $scope.edit = function() {
                $scope.template.event = $scope.selectedEvent.value._id;
                $scope.loading = true;
                $scope.template.$update({
                    id: $scope.template._id
                }, function(template) {
                    showMessages([{
                        type: 'success',
                        message: 'Template edited successfully!',
                        header: 'Edit Template'
                    }]);
                    $state.go('portal.notification.template.list');
                    $scope.loading = false;
                }, function(err) {
                    showMessages([{
                        type: 'error',
                        message: err.data.message,
                        header: 'Edit Template'
                    }]);
                    $scope.loading = false;
                })
            }
        }
    ]);
