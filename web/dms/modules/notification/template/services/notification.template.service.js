angular.module('notification.template').service('NotificationTemplateService', [
    '$resource',
    function($resource) {
        return $resource(API_PATH + '/notification/template', {}, {
            get: {
                method: 'GET',
                isArray: true
            },
            update: {
                method: 'PUT'
            }
        });
    }
]);
