angular.module('notification.template', [
        'notification'
    ])
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $stateProvider.state('portal.notification.template', {
                url: '/template',
                templateUrl: '/views/core.portal.action.tpl.html',
                controller: 'TemplateController'
            }).state('portal.notification.template.list', {
                url: '/list',
                templateUrl: '/views/notification.template.list.html',
                resolve: {
                    Templates: [
                        'NotificationTemplateService',
                        function(NotificationTemplateService) {
                            return NotificationTemplateService.get().$promise;
                        }
                    ]
                },
                controller: 'TemplateListController'
            }).state('portal.notification.template.add', {
                url: '/add',
                templateUrl: '/views/notification.template.add.html',
                controller: 'TemplateAddController',
                resolve: {
                    NotificationEvents: [
                        'EventService',
                        function(EventService) {
                            return EventService.get({ active: true }).$promise;
                        }
                    ]
                }
            }).state('portal.notification.template.edit', {
                url: '/edit/:id',
                templateUrl: '/views/notification.template.edit.html',
                resolve: {
                    'Template': [
                        'NotificationTemplateService',
                        '$stateParams',
                        function(NotificationTemplateService, $stateParams) {
                            return NotificationTemplateService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ],
                    NotificationEvents: [
                        'EventService',
                        function(EventService) {
                            return EventService.get({ active: true }).$promise;
                        }
                    ]
                },
                controller: 'TemplateEditController'
            }).state('portal.notification.template.remove', {
                url: '/remove/:id',
                templateUrl: '/views/notification.template.remove.html',
                resolve: {
                    'Template': [
                        'NotificationTemplateService',
                        '$stateParams',
                        function(NotificationTemplateService, $stateParams) {
                            return NotificationTemplateService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ]
                },
                controller: 'TemplateRemoveController'
            });

        }
    ]);
