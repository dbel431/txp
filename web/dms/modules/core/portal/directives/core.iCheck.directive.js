angular.module('core').directive('iCheck', [
    '$ocLazyLoad',
    function($ocLazyLoad) {
        return {
            link: function(scope, elt, attb) {
                $ocLazyLoad.load([
                    'admin-theme/plugins/iCheck/icheck.min.js',
                    'admin-theme/plugins/iCheck/square/blue.css'
                ]).then(function() {
                    angular.element(elt).iCheck({
                        checkboxClass: 'icheckbox_square-blue',
                        radioClass: 'iradio_square-blue'
                    });
                });
            }
        };
    }
]);
