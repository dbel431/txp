angular.module('portal')
    .directive('validateIsNumber', function() {
        return {
            require: 'ngModel',
            scope: {
                validateIsNumber: '='
            },
            restrict: 'A',
            link: function(scope, iElm, iAttrs, ngModel) {
                ngModel.$validators.validateIsNumber = function(modelValue) {
                    if (modelValue == '' || modelValue == undefined) return true;
                    var flag = true,
                        options = scope.validateIsNumber;
                    flag = flag && (options.nonZero && parseFloat(modelValue) > 0)
                    return flag;
                };
            }
        };
    });
