angular.module('portal').directive('dmsHeader', function() {
    return {
        restrict: 'E',
        templateUrl: '/views/core.portal.header.tpl.html',
        replace: true
    };
});