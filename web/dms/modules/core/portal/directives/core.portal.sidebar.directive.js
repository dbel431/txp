angular.module('portal').directive('dmsSidebar', ['$rootScope',
    '$localStorage',
    function($rootScope, $localStorage) {
        return {
            restrict: 'E',
            templateUrl: '/views/core.portal.sidebar.tpl.html',
            replace: true
        };
    }
]);
// directive('', ['', function(){
// 	// Runs during compile
// 	return {
// 		// name: '',
// 		// priority: 1,
// 		// terminal: true,
// 		// scope: {}, // {} = isolate, true = child, false/undefined = no change
// 		// controller: function($scope, $element, $attrs, $transclude) {},
// 		// require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
// 		// restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
// 		// template: '',
// 		// templateUrl: '',
// 		// replace: true,
// 		// transclude: true,
// 		// compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
// 		link: function($scope, iElm, iAttrs, controller) {

// 		}
// 	};
// }]);
// angular.module('portal').directive('sidebarLoad', function() {
//     return {
//         restrict: 'A',
//         link: function(scope, elt, attb) {
//             if (scope.$last === true)
//                 $('#side-menu').metisMenu();
//         }
//     };
// });
