angular.module('core').directive('slimScroll', [
    '$ocLazyLoad',
    function($ocLazyLoad) {
        return {
            link: function(scope, elt, attb) {
                $ocLazyLoad.load([
                    'nrp-admin-theme/assets/js/jquery.slimscroll.js',
                ]).then(function() {
                    angular.element(elt).slimScroll({
                        height: attb.slimScroll
                    });
                });
            }
        };
    }
]);
