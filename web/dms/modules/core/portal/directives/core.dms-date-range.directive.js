angular.module('portal')
    .directive('dmsDateRange', function() {
        return {
            require: 'uibDatepickerPopup',
            restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
            link: function($scope, iElm, iAttrs, controller) {
                var type = iAttrs.dmsDateRange,
                    options = iAttrs.datepickerOptions;
                $scope.$watch(iAttrs.ngModel, function(newVal) {
                    $scope[options] = (type == "from") ? {
                        minDate: new Date(newVal || ''),
                        maxDate: new Date(newVal || '')
                    } : {
                        minDate: new Date(newVal || ''),
                        maxDate: new Date(newVal || '')
                    };
                });
            }
        };
    });
