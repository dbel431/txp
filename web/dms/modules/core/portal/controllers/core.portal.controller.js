angular.module('portal')
    .controller('PortalController', [
        '$state',
        '$rootScope',
        '$localStorage',
        '$scope',
        '$uibModal',
        'UserService',
        'UnreadMail',
        'AuthToken',
        'UserACL',
        'Directories',
        function($state, $rootScope, $localStorage, $scope, $uibModal, UserService, UnreadMail, AuthToken, UserACL, Directories) {
            $rootScope.unreadMails = [];
            for (var i = UnreadMail.length - 1; i >= 0; i--) {
                if (UnreadMail[i].read.indexOf(AuthToken.user._id) < 0) {
                    $rootScope.unreadMails.push(UnreadMail[i]);
                }
            }
            $rootScope.currentUser = new UserService(AuthToken.user);
            if (!$localStorage['DMS-DIRECTORY']) $localStorage['DMS-DIRECTORY'] = $rootScope.currentUser.directory;
            $rootScope.currentDirectory = $localStorage['DMS-DIRECTORY'];
            // if ('personalization' in $rootScope.currentUser.details && $rootScope.currentUser.details.personalization.theme)
            //     $rootScope.changeTheme($rootScope.currentUser.details.personalization.theme);
            $rootScope.openDirectorySwitcher = function() {
                var directoryId = $localStorage['DMS-DIRECTORY'] ? $localStorage['DMS-DIRECTORY']._id : '';
                //console.log(AuthToken);
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/core.directory.switcher.tpl.html',
                    size: 'lg',
                    resolve: {
                        AllowedDirectories: function() {
                            var allowedDirectories = [],
                                directoryMap = {};
                            for (var i = Directories.length - 1; i >= 0; i--) {
                                directoryMap[Directories[i]._id] = Directories[i];
                            }
                            for (var i = 0; i < $rootScope.currentUser.role.directories.length; i++) {
                                var directory = directoryMap[$rootScope.currentUser.role.directories[i]];
                                directory.selected = (directory._id == directoryId);
                                allowedDirectories.push(directory);
                            }
                            return allowedDirectories;
                        }
                    },
                    controller: [
                        '$rootScope',
                        '$localStorage',
                        '$scope',
                        '$state',
                        '$uibModalInstance',
                        'AllowedDirectories',
                        'UserService',
                        function($rootScope, $localStorage, $scope, $state, $uibModalInstance, AllowedDirectories, UserService) {
                            $scope.directories = AllowedDirectories;
                            $scope.chooseDirectory = function(directory) {
                                var newUser = new UserService($rootScope.currentUser);
                                $localStorage['DMS-DIRECTORY'] = $rootScope.currentDirectory = directory;
                                newUser.directory = directory._id;
                                newUser.$update(function(currentUser) {
                                    showMessages([{
                                        type: 'success',
                                        message: "Directory switched to <strong>" + directory.codeValue + "</strong>!",
                                        header: 'Switch Directory'
                                    }]);
                                    for (var i in localStorage)
                                        if ((/datatable/i).test(i)) localStorage.removeItem(i);
                                    setTimeout(function() {
                                        if ($rootScope.isCurrentActionExclusive()) $state.go($state.current.name.split('.').slice(0, 3).join('.'), { reload: true });
                                        else window.location.reload();
                                        $uibModalInstance.close();
                                    }, 500);
                                }, function(err) {
                                    showMessages([{
                                        type: 'error',
                                        message: "Directory could not be switched!",
                                        header: 'Switch Directory'
                                    }]);
                                    $uibModalInstance.close();
                                })
                            };
                            $scope.cancel = function() {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    ]
                });
                // modalInstance.result.then(function(selectedItem) {}, function() {});
            };
            $rootScope.isSelectedDirectoryStillValidForCurrentUser = function() {
                if (!(UserACL[0].directories && (UserACL[0].directories.indexOf($localStorage['DMS-DIRECTORY']._id) >= 0))) {
                    $localStorage['DMS-DIRECTORY'] = undefined;
                    return false;
                }
                return true;
            };
        }
    ]);
