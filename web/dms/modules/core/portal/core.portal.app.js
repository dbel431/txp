var Portal = [];
angular.module('portal', [
        'auth', 'acl', 'log'
    ]).config([
        '$stateProvider',
        '$urlRouterProvider',
        '$httpProvider',
        function($stateProvider, $urlRouterProvider, $httpProvider) {
            var redirectTo = (localStorage.getItem('DMS-TOKEN') ? '/portal' : '/login');
            $urlRouterProvider.otherwise(redirectTo);
            $stateProvider
                .state('403', {
                    url: '/403',
                    templateUrl: '/views/core.403.html'
                })
                .state('portal', {
                    url: '/portal',
                    controller: 'PortalController',
                    templateUrl: '/views/core.portal.module.tpl.html',
                    resolve: {
                        AuthToken: [
                            'AuthService',
                            function(AuthService) {
                                return AuthService.token().$promise;
                            }
                        ],
                        UnreadMail: [
                            'AuthToken',
                            'CoreMailboxService',
                            function(AuthToken, CoreMailboxService) {
                                return CoreMailboxService.get({
                                    to: AuthToken.user._id
                                }).$promise;
                            }
                        ],
                        ACLModules: [
                            'ACLModuleService',
                            function(ACLModuleService) {
                                return ACLModuleService.get({ active: true }).$promise;
                            }
                        ],
                        ACLPages: [
                            'ACLPageService',
                            function(ACLPageService) {
                                return ACLPageService.get({ active: true }).$promise;
                            }
                        ],
                        ACLActions: [
                            'ACLActionService',
                            function(ACLActionService) {
                                return ACLActionService.get({ active: true }).$promise;
                            }
                        ],
                        UserACL: [
                            '$location',
                            'AuthToken',
                            'RoleService',
                            function($location, AuthToken, RoleService) {
                                if (AuthToken.user.role) {
                                    return RoleService.get({
                                        id: AuthToken.user.role._id,
                                        active: true
                                    }).$promise;
                                }
                                return [];
                            }
                        ],
                        Portal: [
                            '$state',
                            'ACLModules',
                            'ACLPages',
                            'ACLActions',
                            'UserACL',
                            function($state, ACLModules, ACLPages, ACLActions, UserACL) {
                                Portal = [];
                                if (UserACL.length > 0 && UserACL[0].actions.length > 0) {
                                    var userActions = UserACL[0].actions;
                                    for (var i = 0; i < ACLModules.length; i++) {
                                        var module = ACLModules[i];
                                        module.pages = [];
                                        for (var j = 0; j < ACLPages.length; j++) {
                                            var page = ACLPages[j];
                                            if (page.module && page.module.active && page.module._id == module._id) {
                                                page.actions = [];
                                                for (var k = 0; k < ACLActions.length; k++) {
                                                    var action = ACLActions[k];
                                                    if (action.module && action.module.active && action.page && action.page.active && action.page._id == page._id && userActions.indexOf(action._id) >= 0) {
                                                        page.actions.push(action);
                                                    }
                                                }
                                                if (page.actions.length > 0) module.pages.push(page);
                                            }
                                        }
                                        if (module.pages.length > 0) Portal.push(module);
                                    }
                                }
                                return Portal;
                            }
                        ],
                        Directories: [
                            'DirectoryService',
                            function(DirectoryService) {
                                return DirectoryService.get({ active: true }).$promise;
                            }
                        ]
                    }
                });
            $httpProvider.interceptors.push([
                '$q',
                '$location',
                function($q, $location) {
                    return {
                        'request': function(config) {
                            config.headers = config.headers || {};
                            config.headers.Authorization = 'Bearer ' + localStorage.getItem('DMS-FINGERPRINT') + " " + localStorage.getItem('DMS-TOKEN');
                            config.headers.resetPasswordAuth = 'Bearer ' + localStorage.getItem('DMS-FINGERPRINT') + " " + localStorage.getItem('DMS-RESET-PASSWORD-TOKEN');
                            return config;
                        },
                        'responseError': function(response) {
                            if ((response.status === 401 || response.status === 403)) {
                                localStorage.removeItem('DMS-TOKEN');
                                localStorage.removeItem('DMS-RESET-PASSWORD-TOKEN');
                                if ($location.path().split('/').indexOf('login') < 0 && $location.path().split('/').indexOf('forgot') < 0) {
                                    showMessages([{
                                        type: 'error',
                                        message: 'Unauthorized/Expired token!',
                                        header: 'Authorization'
                                    }]);
                                    $location.path('/login');
                                }

                            }
                            return $q.reject(response);
                        }
                    };
                }
            ]);
        }
    ])
    .run([
        'THEMES',
        '$rootScope',
        '$localStorage',
        '$state',
        'AuthService',
        'UserService',
        function(THEMES, $rootScope, $localStorage, $state, AuthService, UserService) {
            $rootScope.date_format = date_format;
            $rootScope.to_date_format = function(date, format) {
                return $rootScope.date_format(new Date(date), format);
            };
            $rootScope.removeItems = removeItems;
            $rootScope.getNameOfUser = getNameOfUser;
            // portal
            $rootScope.currentUser = {};
            $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams) {
                if (toState.name.split('.').indexOf('portal') >= 0 && localStorage.getItem('DMS-TOKEN') == null) {
                    event.preventDefault();
                    $state.go('auth.login');
                }
            });
            $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
                if (toState.name.split('.').indexOf('portal') >= 0 && localStorage.getItem('DMS-TOKEN') != null) {
                    $rootScope.portalModules = Portal;
                    $rootScope.portalModuleActive = {};
                    $rootScope.portalPageActive = {};
                    var currentPath = removeItems(toState.name.trim().split('.'), [
                        '', 'core', 'portal'
                    ]);
                    var goTo = toState.name;
                    var quickLinkFlag = false;
                    if ( /*Portal.length == 0 && */ currentPath.length == 0) goTo = "portal.dashboard";
                    else {
                        if (toState.name.trim().split('.').indexOf('dashboard') >= 0) {
                            goTo = 'portal.dashboard';
                        } else if (toState.name.trim().split('.').indexOf('profile') >= 0) {
                            switch (currentPath.length) {
                                case 1:
                                    {
                                        goTo = 'portal.profile.personal';
                                        break;
                                    }
                            }
                        } else if (toState.name.trim().split('.').indexOf('quickLinks') >= 0) {
                            goTo = 'portal.quickLinks';
                        } else if (toState.name.trim().split('.').indexOf('mailbox') >= 0) {
                            switch (currentPath.length) {
                                case 1:
                                    {
                                        goTo = 'portal.mailbox.inbox';
                                        break;
                                    }
                            }
                        } else {
                            var flag = false;
                            switch (currentPath.length) {
                                case 0:
                                    {
                                        $rootScope.portalModuleActive = $rootScope.portalModules[0];
                                        flag = true;
                                        goTo = "portal." + $rootScope.portalModules[0].pages[0].actions[0].object;
                                        break;
                                    }
                                case 1:
                                    {
                                        var module = currentPath[0];
                                        for (var i in $rootScope.portalModules) {
                                            if ($rootScope.portalModules[i].object == module) {
                                                $rootScope.portalModuleActive = $rootScope.portalModules[i];
                                                $rootScope.portalPageActive = $rootScope.portalModules[i].pages[0];
                                                flag = true;
                                                goTo = "portal." + $rootScope.portalModules[i].pages[0].actions[0].object;
                                                break;
                                            }
                                        }
                                        break;
                                    }
                                case 2:
                                    {
                                        var module = currentPath[0];
                                        var page = currentPath[1];
                                        for (var i in $rootScope.portalModules) {
                                            if ($rootScope.portalModules[i].object == module) {
                                                for (var j in $rootScope.portalModules[i].pages) {
                                                    if ($rootScope.portalModules[i].pages[j].object == [module, page].join('.')) {
                                                        $rootScope.portalModuleActive = $rootScope.portalModules[i];
                                                        $rootScope.portalPageActive = $rootScope.portalModules[i].pages[j];
                                                        flag = true;
                                                        goTo = "portal." + $rootScope.portalModules[i].pages[j].actions[0].object;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                    }
                                default:
                                    {
                                        var module = currentPath[0];
                                        var page = currentPath[1];
                                        for (var i in $rootScope.portalModules) {
                                            if ($rootScope.portalModules[i].object == module) {
                                                for (var j in $rootScope.portalModules[i].pages) {
                                                    if ($rootScope.portalModules[i].pages[j].object == [module, page].join('.')) {
                                                        flag = true;
                                                        $rootScope.portalModuleActive = $rootScope.portalModules[i];
                                                        $rootScope.portalPageActive = $rootScope.portalModules[i].pages[j];
                                                        quickLinkFlag = (currentPath.length == 3 && !$rootScope.isActionExclusive(currentPath.join('.')));
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        if (flag && !$rootScope.isActionPermitted(currentPath.slice(0, 3).join('.'))) goTo = "403";
                                        break;
                                    }
                            }
                            if (!flag) goTo = "portal";
                        }
                    }
                    // quickLinks
                    if (quickLinkFlag && $rootScope.currentUser && $rootScope.currentUser._id) {
                        if (!$rootScope.currentUser.quickLinks) $rootScope.currentUser.quickLinks = {
                            recent: [],
                            most: []
                        };
                        $rootScope.currentUser.quickLinks.recent.unshift({
                            state: toState.name
                        });
                        $rootScope.currentUser.quickLinks.recent = removeDuplicate($rootScope.currentUser.quickLinks.recent);
                        $rootScope.currentUser.quickLinks.recent = setLinksLimit($rootScope.currentUser.quickLinks.recent);
                        $rootScope.currentUser.quickLinks.most = sortMostViewed(increaseMostCount($rootScope.currentUser.quickLinks.most, toState.name));
                        var newUserObj = new UserService($rootScope.currentUser);
                        newUserObj.$update(function(res) {}, function(err) {});
                    }
                    // ----------
                    event.preventDefault();
                    $state.go(goTo);
                }
            });
            $rootScope.isActionPermitted = function(actionObject) {
                var actionsPermitted = $rootScope.portalPageActive.actions;
                for (var i = actionsPermitted.length - 1; i >= 0; i--) {
                    if (actionObject == actionsPermitted[i].object) return true
                }
                return false;
            };
            $rootScope.isActionExclusive = function(actionObject) {
                var actionsPermitted = $rootScope.portalPageActive.actions;
                for (var i = actionsPermitted.length - 1; i >= 0; i--) {
                    if (actionObject == actionsPermitted[i].object) return actionsPermitted[i].exclusive;
                }
                return false;
            };
            $rootScope.isCurrentActionExclusive = function() {
                var actionObject = removeItems($state.current.name.trim().split('.'), [
                    '', 'core', 'portal'
                ]).slice(0, 3).join('.');
                var actionsPermitted = $rootScope.portalPageActive.actions;
                for (var i = actionsPermitted.length - 1; i >= 0; i--) {
                    if (actionObject == actionsPermitted[i].object) return actionsPermitted[i].exclusive;
                }
                return false;
            };
            $rootScope.logout = function() {
                AuthService.logout(function(res) {
                    localStorage.removeItem('DMS-TOKEN');
                    delete $localStorage['DMS-DIRECTORY'];
                    showMessages([{
                        type: 'success',
                        message: "You have been logged out successfully!",
                        header: 'Logout'
                    }]);
                    $state.go('auth.login');
                });
            };
            // theming
            var theme = (localStorage.getItem('DMS-THEME') || 'light');
            $rootScope.theme = THEMES[theme];
            localStorage.setItem('DMS-THEME', theme);
            $rootScope.changeTheme = function(toTheme) {
                var theme = (toTheme || $rootScope.theme);
                $rootScope.theme = THEMES[theme];
                localStorage.setItem('DMS-THEME', theme);
            };
        }
    ]);

function setLinksLimit(links, limit) {
    return links.length < limit ? links : links.slice(0, 10);
}

function removeDuplicate(recent) {
    var newRecent = [recent[0]];
    for (var i = 1; i < recent.length; i++) {
        if (recent[i].state != recent[0].state) {
            newRecent.push(recent[i]);
        }
    }
    return newRecent;
}

function increaseMostCount(mostViewed, toStateName) {
    var flag = false;
    for (var i in mostViewed) {
        if (mostViewed[i].state == toStateName) {
            mostViewed[i].count++;
            flag = true;
            break;
        }
    }
    if (!flag) mostViewed.push({
        state: toStateName,
        count: 1
    });
    return mostViewed;
}

function sortMostViewed(mostViewed) {
    mostViewed = setLinksLimit(mostViewed);
    for (var i = 0; i < mostViewed.length; i++) {
        for (var j = i + 1; j < mostViewed.length; j++) {
            if (mostViewed[i].count < mostViewed[j].count) {
                var temp = mostViewed[i];
                mostViewed[i] = mostViewed[j];
                mostViewed[j] = temp;
            }
        }
    }
    return mostViewed;
}

function removeItems(arr, exclude) {
    var t = [];
    for (var i = 0; i < arr.length; i++) {
        if (exclude.indexOf(arr[i].trim()) < 0) t.push(arr[i]);
    }
    return t;
}

function getNameOfUser(user) {
    if (user && user.details && user.details.personal && user.details.personal.name)
        return [user.details.personal.name.last, user.details.personal.name.first].join(' ');
    return user.auth.email;
}
