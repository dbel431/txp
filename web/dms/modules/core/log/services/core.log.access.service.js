angular.module('log')
    .service('CoreLogAccessService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/log/access', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);