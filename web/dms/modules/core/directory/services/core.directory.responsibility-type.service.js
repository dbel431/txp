angular.module('directory')
    .service('DirectoryResponsibilityTypeService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/responsibility-type', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);
