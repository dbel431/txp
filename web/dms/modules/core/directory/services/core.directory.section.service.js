angular.module('directory')
    .service('DirectorySectionService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/section', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            })
        }
    ])
