angular.module('directory')
    .service('DirectoryFeatureTypeService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/feature-type', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);
