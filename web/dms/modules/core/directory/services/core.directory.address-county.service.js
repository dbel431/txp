angular.module('directory')
    .service('DirectoryCountyService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/county', {}, {
                list: {
                    method: 'POST',
                    isArray: true
                }
            })
        }
    ])
