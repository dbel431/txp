angular.module('directory')
    .service('DirectoryCityService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/city', {}, {
                list: {
                    method: 'POST',
                    isArray: true
                }
            });
        }
    ]);