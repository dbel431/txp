angular.module('directory')
    .service('DirectoryContactTypeService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/contact-type', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);
