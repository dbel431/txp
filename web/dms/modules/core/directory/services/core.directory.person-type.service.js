angular.module('directory')
    .service('DirectoryPersonTypeService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/person-type', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);
