angular.module('directory')
    .service('DirectoryCollectionCategoryService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/collection-category', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);