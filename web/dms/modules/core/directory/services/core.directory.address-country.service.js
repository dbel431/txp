angular.module('directory')
    .service('DirectoryCountryService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/country', {}, {
                list: {
                    method: 'POST',
                    isArray: true
                }
            });
        }
    ]);
