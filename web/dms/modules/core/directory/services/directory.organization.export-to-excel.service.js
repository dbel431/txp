angular.module('directory')
    .service('ExportToExcelService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/export-to-excel', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            })
        }
    ])
