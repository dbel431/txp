angular.module('directory')
    .service('DirectoryAddressTypeService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/address-type', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);
