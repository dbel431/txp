angular.module('directory')
    .service('DirectoryRecordVersionService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/record-version', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);