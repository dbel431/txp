angular.module('directory')
    .service('DirectoryClassificationCodeService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/classification', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            })
        }
    ])
