angular.module('directory')
    .service('DirectoryParishStatusService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/parish-status-type', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            })
        }
    ])
