angular.module('directory')
    .service('DirectoryDisbursalTypeService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/disbursal-type', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);
