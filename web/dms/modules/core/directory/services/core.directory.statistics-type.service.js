angular.module('directory')
    .service('DirectoryStatisticsTypeService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/statistics-type', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);
