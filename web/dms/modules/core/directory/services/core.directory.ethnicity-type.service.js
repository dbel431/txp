angular.module('directory')
    .service('DirectoryEthnicityTypeService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/ethnicity-type', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            })
        }
    ])
