angular.module('directory')
    .service('DirectoryDegreeTypeService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/degree-type', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);
