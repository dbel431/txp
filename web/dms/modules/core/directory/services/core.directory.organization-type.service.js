angular.module('directory')
    .service('DirectoryOrganizationTypeService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/organization-type', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            })
        }
    ])
