angular.module('directory')
    .service('DirectoryDioceseTypeService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/diocese-type', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            })
        }
    ])
