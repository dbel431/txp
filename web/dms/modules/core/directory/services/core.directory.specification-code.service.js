angular.module('directory')
    .service('DirectorySpecificationCodeService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/specification-code', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);