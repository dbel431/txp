angular.module('directory')
    .service('DirectorySchoolTypeService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/school-type', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);
