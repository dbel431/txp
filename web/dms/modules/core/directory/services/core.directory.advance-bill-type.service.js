angular.module('directory')
    .service('DirectoryAdvanceBillTypeService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/advance-bill-type', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            })
        }
    ])
