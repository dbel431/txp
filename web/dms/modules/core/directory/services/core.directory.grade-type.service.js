angular.module('directory')
    .service('DirectoryGradeTypeService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/grade-type', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);
