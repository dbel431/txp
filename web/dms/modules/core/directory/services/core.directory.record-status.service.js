angular.module('directory')
    .service('DirectoryRecordStatusService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/record-status', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);