angular.module('directory')
    .service('DirectoryStateService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/state', {}, {
                list: {
                    method: 'POST',
                    isArray: true
                }
            });
        }
    ]);