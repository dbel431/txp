angular.module('directory')
    .service('DirectoryDMMPSectionCodeService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/section-code', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);