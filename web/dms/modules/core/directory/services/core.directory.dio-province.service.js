angular.module('directory')
    .service('DirectoryDioProvinceService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/dioProvince-type', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);
