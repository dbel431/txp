angular.module('directory')
    .service('DirectoryReligiousTypeService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/religious-type', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);
