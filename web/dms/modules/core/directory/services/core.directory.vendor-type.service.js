angular.module('directory')
    .service('DirectoryVendorTypeService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/vendor-type', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);
