angular.module('directory')
    .service('DirectoryPrimaryMarketService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/primarymarket', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            })
        }
    ])
