angular.module('directory')
    .service('DirectoryFeatureCodeService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/feature-code', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);