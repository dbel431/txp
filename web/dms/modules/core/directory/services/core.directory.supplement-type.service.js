angular.module('directory')
    .service('DirectorySupplementTypeService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/supplement-type', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);
