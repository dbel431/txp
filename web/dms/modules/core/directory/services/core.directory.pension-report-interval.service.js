angular.module('directory')
    .service('DirectoryPensionReportIntervalService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/pension-report-interval', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);