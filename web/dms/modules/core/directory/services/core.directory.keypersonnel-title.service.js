angular.module('directory')
    .service('DirectoryKeyPersonnelTitleService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/title', {}, {
                list: {
                    method: 'POST',
                    isArray: true
                }
            });
        }
    ]);
