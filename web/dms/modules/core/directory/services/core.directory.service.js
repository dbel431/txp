angular.module('core')
    .service('DirectoryService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);