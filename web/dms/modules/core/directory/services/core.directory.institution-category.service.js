angular.module('directory')
    .service('DirectoryInstitutionCategoryService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/institution-category', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);