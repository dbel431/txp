angular.module('directory')
    .service('DirectoryOfficerTypeService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/officer-type', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);
