angular.module('directory')
    .service('DirectoryAssignmentTypeService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/assignment-type', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);
