angular.module('directory')
    .service('DirectoryListingTypeService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/listing-type', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);
