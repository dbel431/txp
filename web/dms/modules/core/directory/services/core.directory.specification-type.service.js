angular.module('directory')
    .service('DirectorySpecificationTypeService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/directory/specification-type', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);
