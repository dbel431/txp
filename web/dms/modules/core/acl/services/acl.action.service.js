angular.module('acl')
    .service('ACLActionService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/acl/action', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                }
            });
        }
    ]);
