angular.module('core', [
        'ui.router',
        'oc.lazyLoad',
        'ngMessages',
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngSanitize',
        'ngStorage',
        'ngTouch',
        'ui.bootstrap',
        'datatables',
        'datatables.fixedheader',
        'datatables.columnfilter',
        'datatables.light-columnfilter',
        'chart.js',
        'toggle-switch',
        'ui.select',
        'ckeditor',
        'angular-loading-bar',
        'ngJsonExportExcel',
        'ngImgCrop',
    ])
    .constant('THEMES', {
        'dark': {
            '__name': 'Dark Theme',
            'nav': 'navbar-inverse',
            'navAlt': 'navbar-default',
            'sidebar': 'sidebar-nav-dark',
            'body': 'skin-blue'
        },
        'light': {
            '__name': 'Light Theme',
            'nav': 'navbar-default-blue',
            'navAlt': 'navbar-default',
            'sidebar': 'sidebar-nav',
            'body': 'skin-black'
        }
    })
    .config([
        '$locationProvider',
        'uiSelectConfig',
        'cfpLoadingBarProvider',
        '$ocLazyLoadProvider',
        function($locationProvider, uiSelectConfig, cfpLoadingBarProvider, $ocLazyLoadProvider) {
            $locationProvider.html5Mode(true);
            uiSelectConfig.theme = 'bootstrap';
            cfpLoadingBarProvider.includeSpinner = true;
            cfpLoadingBarProvider.spinnerTemplate = '<div class="overlay"><h1><i class="fa fa-spin fa-refresh"></i></h1></div>';
            $ocLazyLoadProvider.config({
                modules: [{
                    name: 'core',
                    files: [
                        // 'admin-theme/plugins/fastclick/fastclick.js',
                        // 'admin-theme/plugins/slimScroll/jquery.slimscroll.min.js',
                        // 'admin-theme/dist/js/app.min.js',
                        'nrp-admin-theme/assets/js/ace-extra.js'
                    ]
                }]
            });
        }
    ]).run([
        '$rootScope',
        '$localStorage',
        '$ocLazyLoad',
        function($rootScope, $localStorage, $ocLazyLoad) {
            $rootScope.$on('$viewContentLoaded', function(event) {
                // asset lazy loading
                $ocLazyLoad.load('core');
            });
        }
    ]);
