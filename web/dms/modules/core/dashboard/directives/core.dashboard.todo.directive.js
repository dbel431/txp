angular.module('dashboard').directive('dmsToDo', [
    'CoreDashboardToDoService',
    function(CoreDashboardToDoService) {
        return {
            templateUrl: '/views/core.dashboard.todo.tpl.html',
            replace: true,
            link: function($scope, iElm, iAttrs, controller) {
                $scope.loading = false;
                $scope.editing = false;
                $scope.todo = new CoreDashboardToDoService({});
                $scope.todoList = [];
                $scope.reloadList = function() {
                    $scope.loading = true;
                    CoreDashboardToDoService.get({}, function(todoList) {
                        $scope.todoList = todoList;
                        $scope.loading = false;
                    }, function(err) {
                        showMessages([{
                            'type': 'error',
                            'message': 'Could not fetch To-do tasks!'
                        }]);
                        $scope.loading = false;
                    });
                };
                $scope.save = function() {
                    $scope.loading = true;
                    var m = $scope.editing ? '$update' : '$save';
                    $scope.todo[m](function(todo) {
                        $scope.editing = false;
                        $scope.todo = new CoreDashboardToDoService({});
                        showMessages([{
                            type: 'success',
                            message: 'To-do item saved successfully!',
                            header: 'To-do List'
                        }]);
                        $scope.reloadList();
                    }, function(err) {
                        showMessages([{
                            type: 'error',
                            message: 'Could not save To-do item!',
                            header: 'To-do List'
                        }]);
                        $scope.loading = false;
                    });
                };
                $scope.edit = function(todo) {
                    $scope.todo = new CoreDashboardToDoService(todo);
                    $scope.editing = true;
                };
                $scope.remove = function(todo) {
                    todo.$remove({
                        id: todo._id
                    }, function(todo) {
                        $scope.todo = new CoreDashboardToDoService({});
                        showMessages([{
                            type: 'success',
                            message: 'To-do item removed successfully!',
                            header: 'To-do List'
                        }]);
                        $scope.reloadList();
                    }, function(err) {
                        showMessages([{
                            type: 'error',
                            message: 'Could not remove To-do item!',
                            header: 'To-do List'
                        }]);
                        $scope.loading = false;
                    });
                };
                $scope.complete = function(todo) {
                    todo.$update(function() {
                        showMessages([{
                            type: 'success',
                            message: 'To-do item status updated successfully!',
                            header: 'To-do List'
                        }]);
                        $scope.reloadList();
                    }, function(err) {
                        showMessages([{
                            type: 'error',
                            message: 'Could not complete To-do item!',
                            header: 'To-do List'
                        }]);
                        $scope.loading = false;
                    })
                }
                $scope.reloadList();
            }
        };
    }
]);
