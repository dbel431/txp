angular.module('dashboard', [
        'portal'
    ])
    .config([
        '$stateProvider',
        function($stateProvider) {
            $stateProvider
                .state('portal.dashboard', {
                    url: '/dashboard',
                    templateUrl: '/views/core.dashboard.html',
                    controller: 'CoreDashboardController'
                });
        }

    ])
