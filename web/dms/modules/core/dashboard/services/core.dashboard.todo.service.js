angular.module('dashboard')
    .service('CoreDashboardToDoService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/todo', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                },
                update: {
                    method: 'PUT'
                }
            });
        }
    ]);
