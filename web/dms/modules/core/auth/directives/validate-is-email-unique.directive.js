angular.module('auth').directive('validateIsEmailUnique', [
    'AuthService',
    function(AuthService) {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function(scope, iElm, iAttrs, ngModel) {
                ngModel.$validators.validateIsEmailUnique = function(modelValue) {
                    if (modelValue == '' || modelValue == undefined) return true;
                    var auth = new AuthService();
                    return AuthService.validateEmail({
                        'auth.email': modelValue
                    }, function(users) {
                        if (users.length == 1 && iAttrs.exceptUserId) {
                            ngModel.$setValidity('validateIsEmailUnique', (users[0]._id == iAttrs.exceptUserId));
                        } else {
                            ngModel.$setValidity('validateIsEmailUnique', (users.length <= 0));
                        }
                    }, function(err) {
                        ngModel.$setValidity('validateIsEmailUnique', false);
                    });
                };
            }
        };
    }
]);
