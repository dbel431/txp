angular.module('auth', [
    'core'
]).config([
    '$stateProvider',
    '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {
        $stateProvider.state('auth', {
            abstract: true,
            templateUrl: '/views/core.auth.tpl.html',
        }).state('auth.login', {
            url: '/login',
            templateUrl: '/views/core.auth.login.html',
            controller: 'LoginController',
        }).state('auth.forgot', {
            abstract: true,
            url: '/forgot',
            template: '<ui-view></ui-view>',
            controller: 'ForgotController'
        }).state('auth.forgot.verifyEmail', {
            url: '/verify-email',
            templateUrl: '/views/core.auth.forgot.verify-email.html',
            controller: 'ForgotVerifyEmailController'
        }).state('auth.forgot.options', {
            url: '/options/:id',
            templateUrl: '/views/core.auth.forgot.options.html',
            resolve: {
                User: [
                    '$stateParams',
                    'AuthService',
                    function($stateParams, AuthService) {
                        return AuthService.validateEmail({ _id: $stateParams.id }).$promise;
                    }
                ]
            },
            controller: 'ForgotOptionsController'
        }).state('auth.forgot.securityQuestion', {
            url: '/security-question/:id',
            templateUrl: '/views/core.auth.forgot.security-question.html',
            resolve: {
                User: [
                    '$stateParams',
                    'AuthService',
                    function($stateParams, AuthService) {
                        return AuthService.validateEmail({ _id: $stateParams.id }).$promise;
                    }
                ]
            },
            controller: 'ForgotSecurityQuestionController'
        }).state('auth.forgot.resetPassword', {
            url: '/reset-password',
            templateUrl: '/views/core.auth.forgot.reset-password.html',
            resolve: {
                User: [
                    'AuthService',
                    function(AuthService) {
                        if (localStorage.getItem('DMS-RESET-PASSWORD-TOKEN')) {
                            return AuthService.validateResetPasswordToken().$promise;
                        }
                        return [];
                    }
                ]
            },
            controller: 'ForgotResetPasswordController'
        });
    }
]);
