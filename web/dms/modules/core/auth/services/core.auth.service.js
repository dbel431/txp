angular.module('auth').service('AuthService', [
    '$resource',
    function($resource) {
        return $resource(API_PATH + '/auth', {}, {
            login: {
                method: 'POST',
                url: API_PATH + '/auth/login'
            },
            forgot: {
                method: 'POST',
                url: API_PATH + '/auth/forgot'
            },
            token: {
                method: 'GET',
                url: API_PATH + '/auth/token'
            },
            logout: {
                method: 'POST',
                url: API_PATH + '/auth/logout'
            },
            validateEmail: {
                method: 'GET',
                url: API_PATH + '/auth/validate-email',
                isArray: true
            },
            verifySecurityQuestion: {
                method: 'POST',
                url: API_PATH + '/auth/verify-security-question'
            },
            validateResetPasswordToken: {
                method: 'GET',
                url: API_PATH + '/auth/reset-password-token'
            },
            resetPassword: {
                method: 'PUT',
                url: API_PATH + '/auth/reset-password'
            }
        });
    }
]);
