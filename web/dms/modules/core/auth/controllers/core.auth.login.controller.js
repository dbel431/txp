angular.module('auth').controller('LoginController', [
    '$rootScope',
    '$localStorage',
    '$scope',
    '$state',
    'AuthService',
    function($rootScope, $localStorage, $scope, $state, AuthService) {
        $rootScope.page = {
            name: "Login"
        };
        $scope.auth = new AuthService();
        $scope.loading = false;
        $scope.login = function() {
            $scope.loading = true;
            $scope.auth.$login(function(user) {
                if (user) {
                    localStorage.setItem('DMS-TOKEN', user.token);
                    $scope.loading = false;
                    $scope.loginForm.$setUntouched();
                    showMessages([{
                        type: 'success',
                        message: "Welcome <strong>" + user.email + "</strong>!",
                        header: 'Login'
                    }]);
                    $state.go('portal');
                }
            }, function(err) {
                $scope.loginForm.$setUntouched();
                $scope.loading = false;
                var msg = "Could not log in! Please try again!";
                if (err.data && err.data.message) msg = err.data.message;
                showMessages([{
                    type: 'error',
                    message: msg,
                    header: 'Login'
                }]);
            });
        }
    }
])
