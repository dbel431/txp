angular.module('auth').controller('ForgotController', [
    '$rootScope',
    '$localStorage',
    '$scope',
    '$state',
    'AuthService',
    function($rootScope, $localStorage, $scope, $state, AuthService) {
        $rootScope.page = {
            name: "Forgot Password"
        };
        $scope.auth = new AuthService();
        $scope.loading = false;
    }
]);

angular.module('auth').controller('ForgotVerifyEmailController', [
    '$rootScope',
    '$localStorage',
    '$scope',
    '$state',
    'AuthService',
    function($rootScope, $localStorage, $scope, $state, AuthService) {
        $scope.verifyEmail = function() {
            $scope.loading = true;
            AuthService.validateEmail({
                'auth.email': $scope.auth.email
            }, function(user) {
                if (!user || user.length == 0) {
                    showMessages([{
                        type: 'error',
                        message: "Unauthorised Email!",
                        header: 'Forgot Password'
                    }]);
                    $scope.loading = false;
                    return;
                }
                $state.go('auth.forgot.options', { id: user[0]._id });
            }, function(err) {
                showMessages([{
                    type: 'error',
                    message: "Could not verify email! Please try again!",
                    header: 'Forgot Password'
                }]);
            });
        };
    }
]);

angular.module('auth').controller('ForgotOptionsController', [
    '$rootScope',
    '$localStorage',
    '$scope',
    '$state',
    'AuthService',
    'User',
    function($rootScope, $localStorage, $scope, $state, AuthService, User) {
        if (!User || User.length == 0) {
            showMessages([{
                type: 'error',
                message: "Could not verify user! Please try again!",
                header: 'Forgot Password'
            }]);
            $state.go('auth.forgot.verifyEmail');
            return;
        }
        $scope.user = User[0];
        $scope.auth = new AuthService({ email: $scope.user.auth.email });
        $scope.mailPassword = function() {
            $scope.loading = true;
            $scope.auth.$forgot(function(res) {
                $scope.loading = false;
                showMessages([{
                    type: 'success',
                    message: "Your password has been reset! Please check your mail!",
                    header: 'Forgot Password'
                }]);
                $state.go('auth.login');
            }, function(err) {
                showMessages([{
                    type: 'error',
                    message: "Could not reset password! Please try again!",
                    header: 'Forgot Password'
                }]);
                $scope.loading = false;
            });
        };
    }
]);

angular.module('auth').controller('ForgotSecurityQuestionController', [
    '$rootScope',
    '$localStorage',
    '$scope',
    '$state',
    'AuthService',
    'User',
    function($rootScope, $localStorage, $scope, $state, AuthService, User) {
        if (!User || User.length == 0 || !(User[0].auth.securityQuestion && User[0].auth.securityQuestion.question)) {
            showMessages([{
                type: 'error',
                message: "Could not verify user! Please try again!",
                header: 'Forgot Password'
            }]);
            $state.go('auth.forgot.verifyEmail');
            return;
        }
        $scope.user = User[0];
        $scope.verifySecurityQuestion = function() {
            $scope.loading = true;
            $scope.user.$verifySecurityQuestion(function(res) {
                $scope.loading = false;
                localStorage.setItem('DMS-RESET-PASSWORD-TOKEN', res.token);
                $state.go('auth.forgot.resetPassword');
            }, function(err) {
                $scope.loading = false;
                var msg = "Could not verify answer! Please try again!";
                if (err.data && err.data.message) msg = err.data.message;
                showMessages([{
                    type: 'error',
                    message: msg,
                    header: 'Forgot Password'
                }]);
            });
        };
    }
]);

angular.module('auth').controller('ForgotResetPasswordController', [
    '$rootScope',
    '$localStorage',
    '$scope',
    '$state',
    'AuthService',
    'User',
    function($rootScope, $localStorage, $scope, $state, AuthService, User) {
        if (!User) {
            showMessages([{
                type: 'error',
                message: "Could not verify user! Please try again!",
                header: 'Forgot Password'
            }]);
            $state.go('auth.forgot.verifyEmail');
            return;
        }
        $scope.user = new AuthService(User.user);
        $scope.resetPassword = function() {
            $scope.loading = true;
            $scope.user.$resetPassword(function(res) {
                $scope.loading = false;
                localStorage.removeItem('DMS-RESET-PASSWORD-TOKEN');
                showMessages([{
                    type: 'success',
                    message: 'Password has been reset successfully!',
                    header: 'Reset Password'
                }]);
                $state.go('auth.login');
            }, function(err) {
                $scope.loading = false;
                var msg = "Password has not been reset successfully!! Please try again!";
                if (err.data && err.data.message) msg = err.data.message;
                showMessages([{
                    type: 'error',
                    message: msg,
                    header: 'Reset Password'
                }]);
            });
        };
    }
]);
