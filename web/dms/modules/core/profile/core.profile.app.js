angular.module('profile', [
        'portal'
    ])
    .config([
        '$stateProvider',
        function($stateProvider) {
            $stateProvider
                .state('portal.profile', {
                    url: '/profile',
                    templateUrl: '/views/core.profile.tpl.html',
                    controller: 'CoreProfileController',
                    resolve: {
                        'Profile': [
                            'AuthToken',
                            'UserService',
                            function(AuthToken, UserService) {
                                return UserService.get({ id: AuthToken.user._id, active: true }).$promise;
                            }
                        ],
                        SecurityQuestions: [
                            'CoreSecurityQuestionService',
                            function(CoreSecurityQuestionService) {
                                return CoreSecurityQuestionService.get({ active: true }).$promise;
                            }
                        ]
                    }
                })
                .state('portal.profile.personal', {
                    url: '/personal',
                    templateUrl: '/views/core.profile.personal.html',
                    controller: 'CoreProfilePersonalController'
                })
                .state('portal.profile.personalization', {
                    url: '/personalization',
                    templateUrl: '/views/core.profile.personalization.html',
                    controller: 'CoreProfilePersonalizationController'
                })
                .state('portal.profile.account', {
                    url: '/account',
                    templateUrl: '/views/core.profile.account.html',
                    controller: 'CoreProfileAccountController'
                })
                .state('portal.profile.security', {
                    url: '/security',
                    templateUrl: '/views/core.profile.security.html',
                    controller: 'CoreProfileAccountController'
                });
        }
    ])
