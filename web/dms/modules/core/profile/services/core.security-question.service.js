angular.module('core')
    .service('CoreSecurityQuestionService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/security-question', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                },
                update: {
                    method: 'PUT'
                }
            });
        }
    ])
