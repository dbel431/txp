angular.module('profile')
    .controller('CoreProfileController', [
        'THEMES',
        '$rootScope',
        '$localStorage',
        '$scope',
        '$uibModal',
        'Profile',
        'SecurityQuestions',
        function(THEMES, $rootScope, $localStorage, $scope, $uibModal, Profile, SecurityQuestions) {
            // inits
            $scope.user = Profile[0];
            $scope.securityQuestionArray = SecurityQuestions;

            // configs
            $scope.dobOptions = {
                maxDate: new Date()
            };

            // masters
            $scope.loading = false;
            $scope.saluteArray = [{
                id: 'mr',
                label: 'Mr.'
            }, {
                id: 'ms',
                label: 'Ms.'
            }, {
                id: 'mrs',
                label: 'Mrs.'
            }, {
                id: 'dr',
                label: 'Dr.'
            }];
            $scope.genderArray = [{
                id: 'male',
                label: 'Male'
            }, {
                id: 'female',
                label: 'Female'
            }, {
                id: 'transgender',
                label: 'Transgender'
            }];
            $scope.themeArray = [];
            $scope.selectedTheme = { value: localStorage.getItem('DMS-THEME') };
            for (var i in THEMES) {
                var theme = {
                    id: i,
                    label: THEMES[i].__name
                };
                $scope.themeArray.push(theme);
            }

            // utils
            $scope.setTheme = function(item, model) {
                $rootScope.changeTheme(item.id);
                $scope.selectedTheme.value = item.id;
            };
            $scope.initUser = function() {
                $scope.user.auth.securityQuestion = {};
                if (!('details' in $scope.user)) $scope.user.details = {};
                if (!('personal' in $scope.user.details)) $scope.user.details.personal = {};
                $scope.user.details.personal.salute = $scope.user.details.personal.salute || $scope.saluteArray[0].id;
                $scope.user.details.personal.gender = $scope.user.details.personal.gender || $scope.genderArray[0].id;
                if (('dob' in $scope.user.details.personal)) $scope.user.details.personal.dob = new Date($scope.user.details.personal.dob);
            };
            $scope.update = function() {
                $scope.loading = true;
                if ($scope.selectedTheme) {
                    if (!$scope.user.details.personalization) $scope.user.details.personalization = {};
                    $scope.user.details.personalization.theme = $scope.selectedTheme.value;
                }
                $scope.user.$update(function(user) {
                    $scope.initUser();
                    showMessages([{
                        type: 'success',
                        message: "Successful",
                        header: 'Details updated successfully!'
                    }]);
                    $scope.userForm.$setUntouched();
                    $scope.loading = false;
                }, function(err) {
                    showMessages([{
                        type: 'error',
                        message: err.data.message || "Could not update details! Please try again!",
                        header: 'Error'
                    }]);
                    $scope.loading = false;
                });
            };

            // runs
            $scope.initUser();

            $scope.showUploadFileDialog = function() {
                var fileUploadDialog = $('<input type="file" />');
                fileUploadDialog.on('change', function(evt) {
                    var file = evt.currentTarget.files[0];
                    var reader = new FileReader();
                    var modalInstance = $uibModal.open({
                        backdrop: 'static',
                        animation: true,
                        templateUrl: '/views/core.image-uploader.crop-zone.html',
                        controller: [
                            '$scope',
                            '$uibModalInstance',
                            function($modalScope, $uibModalInstance) {
                                $modalScope.myImage = '';
                                $modalScope.myCroppedImage = '';
                                $modalScope.cancel = function() {
                                    $uibModalInstance.dismiss('cancel');
                                };
                                $modalScope.crop = function() {
                                    $uibModalInstance.close($modalScope.myCroppedImage);
                                };
                                reader.onload = function(evt) {
                                    $modalScope.$apply(function($modalScope) {
                                        $modalScope.myImage = evt.target.result;
                                    });
                                };
                                reader.readAsDataURL(file);
                            }
                        ]
                    });
                    modalInstance.result.then(function(image) {
                        $scope.user.avatar = image;
                    });
                });
                fileUploadDialog.click();
            };
        }

    ])
    .controller('CoreProfileAccountController', [
        '$scope',
        function($scope) {}
    ])
    .controller('CoreProfilePersonalController', [
        '$scope',
        function($scope) {

        }
    ])
    .controller('CoreProfilePersonalizationController', [
        '$scope',
        function($scope) {}
    ]);
