angular.module('mailbox')
    .service('CoreMailboxService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/mailbox', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                },
                update: {
                    method: 'PUT'
                }
            });
        }
    ]);
