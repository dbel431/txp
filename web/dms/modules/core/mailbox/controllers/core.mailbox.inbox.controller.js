angular.module('mailbox')
    .controller('CoreMailboxInboxController', [
        '$rootScope',
        '$localStorage',
        '$state',
        '$scope',
        'DTOptionsBuilder',
        'CoreMailboxService',
        'Inbox',
        function($rootScope, $localStorage, $state, $scope, DTOptionsBuilder, CoreMailboxService, Inbox) {
            $rootScope.page = {
                name: 'Mailbox : Inbox',
                actions: {
                    add: {
                        object: 'mailbox.inbox',
                        allowed: function() {
                            return $rootScope.isActionPermitted(this.object);
                        }
                    }
                }
            };
            $scope.mails = Inbox;
            $scope.markedMails = [];
            $scope.markMail = function(index) {
                var mail = $scope.mails[index];
                var pos = $scope.markedMails.indexOf(mail._id);
                if (pos >= 0) {
                    $scope.markedMails.splice(pos, 1);
                    $scope.mails[index].marked = false;
                } else {
                    $scope.markedMails.push(mail._id);
                    $scope.mails[index].marked = true;
                }
            };
            $scope.markAsRead = function() {
                var markCount = successCount = 0;
                for (var i = $scope.markedMails.length - 1; i >= 0; i--) {
                    CoreMailboxService.get({ id: $scope.markedMails[i] }, function(mail) {
                        if (mail[0].read.indexOf($scope.currentUser._id) < 0) mail[0].read.push($scope.currentUser._id);
                        mail[0].$update(function(mail) {
                            markedAsRead(true);
                        }, function(err) {
                            markedAsRead(false);
                        })
                    });
                }

                function markedAsRead(successful) {
                    markCount++;
                    if (successful) successCount++;
                    if (markCount == $scope.markedMails.length) {
                        showMessages([{
                            type: 'success',
                            message: markCount + ' mail(s) marked as <b>Read</b> successfully!',
                            header: 'Mailbox'
                        }]);
                        if (markCount > successCount) {
                            showMessages([{
                                type: 'error',
                                message: (markCount - successCount) + ' mail(s) could not be marked as <b>Read</b> successfully!',
                                header: 'Mailbox'
                            }]);
                        }
                        $state.reload();
                    }
                };
            };
            $scope.markAsUnread = function() {
                var markCount = successCount = 0;
                for (var i = $scope.markedMails.length - 1; i >= 0; i--) {
                    CoreMailboxService.get({ id: $scope.markedMails[i] }, function(mail) {
                        if (mail[0].read.indexOf($scope.currentUser._id) >= 0) mail[0].read.splice(mail[0].read.indexOf($scope.currentUser._id), 1);
                        mail[0].$update(function(mail) {
                            markedAsUnread(true);
                        }, function(err) {
                            markedAsUnread(false);
                        })
                    });
                }

                function markedAsUnread(successful) {
                    markCount++;
                    if (successful) successCount++;
                    if (markCount == $scope.markedMails.length) {
                        showMessages([{
                            type: 'success',
                            message: markCount + ' mail(s) marked as <b>Unread</b> successfully!',
                            header: 'Mailbox'
                        }]);
                        if (markCount > successCount) {
                            showMessages([{
                                type: 'error',
                                message: (markCount - successCount) + ' mail(s) could not be marked as <b>Unread</b> successfully!',
                                header: 'Mailbox'
                            }]);
                        }
                        $state.reload();
                    }
                };
            };
            $scope.isRead = function(mail) {
                return (mail.read && mail.read.indexOf($scope.currentUser._id) >= 0);
            };
            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withOption('order', [1, 'asc'])
                .withOption('columnDefs', [{
                    "targets": [0, 5],
                    "orderable": false
                }])
                .withPaginationType('full_numbers')
                .withColumnFilter({
                    aoColumns: [null, null, {
                        type: 'text',
                        bRegex: true,
                        bSmart: true
                    }, {
                        type: 'text',
                        bRegex: true,
                        bSmart: true
                    }, {
                        type: 'text',
                        bRegex: true,
                        bSmart: true
                    }, null]
                });
        }
    ]);
