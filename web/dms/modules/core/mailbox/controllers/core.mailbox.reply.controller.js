angular.module('mailbox')
    .controller('CoreMailboxReplyController', [
        '$state',
        '$rootScope',
        '$localStorage',
        '$scope',
        'CoreMailboxService',
        'Mail',
        'Users',
        function($state, $rootScope, $localStorage, $scope, CoreMailboxService, Mail, Users) {
            $scope.loading = false;
            $scope.oldMail = Mail[0];


            if (!($scope.oldMail.read && $scope.oldMail.read.indexOf($scope.currentUser._id) >= 0)) {
                $scope.oldMail.read.push($scope.currentUser._id);
                $scope.oldMail.$update(function(oldMail) {
                    $rootScope.unreadMailCount--;
                }, function(err) {
                    showMessages([{
                        type: 'error',
                        message: 'Mail could not be read successfully!',
                        header: 'Mailbox'
                    }]);
                });
            }


            $scope.mail = new CoreMailboxService();
            var tos = $scope.oldMail.to;
            $scope.mail.to = [$scope.oldMail.from._id];
            $scope.mail.from = $scope.currentUser._id;
            for (var i = tos.length - 1; i >= 0; i--) {
                if ([$scope.currentUser._id, $scope.mail.to[0]].indexOf(tos[i]._id) < 0) {
                    $scope.mail.to.push(tos[i]._id);
                }
            }
            $scope.mail.subject = "Re: " + $scope.oldMail.subject;
            $scope.mail.body = "<br /><br />" +
                "-------------------------------------------- <br />" +
                "Reply for message: <br />" +
                "-------------------------------------------- <br />" +
                $scope.oldMail.body;
            $scope.userArray = Users;
            $scope.roleGrouping = function(item) {
                return (item.role && item.role.name) ? item.role.name : 'Ungrouped';
            };
            $scope.reply = function() {
                $scope.loading = true;
                $scope.mail.$save(function(mail) {
                    showMessages([{
                        type: 'success',
                        message: 'Reply sent successfully!',
                        header: 'Mailbox'
                    }]);
                    $scope.composeForm.$setUntouched();
                    $state.go('portal.mailbox.inbox', {}, { reload: true });
                    $scope.loading = false;
                }, function(err) {
                    showMessages([{
                        type: 'error',
                        message: 'Reply could not be sent successfully!',
                        header: 'Mailbox'
                    }]);
                    $scope.composeForm.$setUntouched();
                    $scope.loading = false;
                });
            };
        }
    ]);
