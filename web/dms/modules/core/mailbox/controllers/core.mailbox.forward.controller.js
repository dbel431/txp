angular.module('mailbox')
    .controller('CoreMailboxForwardController', [
        '$state',
        '$rootScope',
        '$localStorage',
        '$scope',
        'CoreMailboxService',
        'Mail',
        'Users',
        function($state, $rootScope, $localStorage, $scope, CoreMailboxService, Mail, Users) {
            $rootScope.page = {
                name: "Mailbox :: Forward"
            };
            $scope.loading = false;
            $scope.oldMail = Mail[0];


            if (!($scope.oldMail.read && $scope.oldMail.read.indexOf($scope.currentUser._id) >= 0)) {
                $scope.oldMail.read.push($scope.currentUser._id);
                $scope.oldMail.$update(function(oldMail) {
                    $rootScope.unreadMailCount--;
                }, function(err) {
                    showMessages([{
                        type: 'error',
                        message: 'Mail could not be read successfully!',
                        header: 'Mailbox'
                    }]);
                });
            }


            $scope.mail = new CoreMailboxService();
            $scope.mail.to = [];
            $scope.mail.from = $scope.currentUser._id;
            $scope.mail.subject = "Fw: " + $scope.oldMail.subject;
            $scope.mail.body = "<br /><br />" +
                "-------------------------------------------- <br />" +
                "Forwarded message: <br />" +
                "-------------------------------------------- <br />" +
                $scope.oldMail.body;
            $scope.userArray = Users;
            $scope.roleGrouping = function(item) {
                return (item.role && item.role.name) ? item.role.name : 'Ungrouped';
            };
            $scope.forward = function() {
                $scope.loading = true;
                $scope.mail.$save(function(mail) {
                    showMessages([{
                        type: 'success',
                        message: 'Mail forwarded successfully!',
                        header: 'Mailbox'
                    }]);
                    $scope.composeForm.$setUntouched();
                    $state.go('portal.mailbox.inbox', {}, { reload: true });
                    $scope.loading = false;
                }, function(err) {
                    showMessages([{
                        type: 'error',
                        message: 'Mail could not be forwarded successfully!',
                        header: 'Mailbox'
                    }]);
                    $scope.composeForm.$setUntouched();
                    $scope.loading = false;
                });
            };
        }
    ]);
