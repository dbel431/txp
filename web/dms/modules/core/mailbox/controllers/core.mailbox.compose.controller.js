angular.module('mailbox')
    .controller('CoreMailboxComposeController', [
        '$rootScope',
        '$localStorage',
        '$state',
        '$scope',
        'CoreMailboxService',
        'Users',
        function($rootScope, $localStorage, $state, $scope, CoreMailboxService, Users) {
            $rootScope.page = {
                name: "Mailbox :: Compose"
            };
            $scope.loading = false;
            $scope.mail = new CoreMailboxService();
            $scope.userArray = Users;
            $scope.roleGrouping = function(item) {
                return (item.role && item.role.name) ? item.role.name : 'Ungrouped';
            };
            $scope.send = function() {
                $scope.loading = true;
                $scope.mail.from = $scope.currentUser._id;
                $scope.mail.$save(function(mail) {
                    showMessages([{
                        type: 'success',
                        message: 'Mail sent successfully!',
                        header: 'Mailbox'
                    }]);
                    $scope.composeForm.$setUntouched();
                    $state.go('portal.mailbox.inbox', {}, { reload: true });
                    $scope.loading = false;
                }, function(err) {
                    showMessages([{
                        type: 'error',
                        message: 'Mail could not be sent successfully!',
                        header: 'Mailbox'
                    }]);
                    $scope.composeForm.$setUntouched();
                    $scope.loading = false;
                });
            };
        }
    ]);
