angular.module('mailbox')
    .controller('CoreMailboxReadController', [
        '$state',
        '$rootScope',
        '$localStorage',
        '$scope',
        'CoreMailboxService',
        'Mail',
        function($state, $rootScope, $localStorage, $scope, CoreMailboxService, Mail) {
            $scope.mail = Mail[0];
            if (!($scope.mail.read && $scope.mail.read.indexOf($scope.currentUser._id) >= 0)) {
                $scope.newMail = new CoreMailboxService($scope.mail);
                $scope.newMail.read = $scope.mail.read || [];
                $scope.newMail.read.push($scope.currentUser._id);
                $scope.newMail.$update(function(mail) {
                    $rootScope.unreadMailCount--;
                }, function(err) {
                    showMessages([{
                        type: 'error',
                        message: 'Mail could not be read successfully!',
                        header: 'Mailbox'
                    }]);
                });
            }
        }
    ])
    .controller('CoreMailboxReadSentController', [
        '$state',
        '$scope',
        'Mail',
        function($state, $scope, Mail) {
            $scope.mail = Mail[0];
        }
    ]);
