angular.module('mailbox')
    .controller('CoreMailboxSentController', [
        '$state',
        '$scope',
        'DTOptionsBuilder',
        'Sent',
        function($state, $scope, DTOptionsBuilder, Sent) {
            $scope.mails = Sent;
            /*$scope.markedMails = [];
            $scope.markMail = function(index) {
                var mail = $scope.mails[index];
                var pos = $scope.markedMails.indexOf(mail._id);
                if (pos >= 0) {
                    $scope.markedMails.splice(pos, 1);
                    $scope.mails[index].marked = false;
                } else {
                    $scope.markedMails.push(mail._id);
                    $scope.mails[index].marked = true;
                }
            };*/
            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withOption('order', [0, 'asc'])
                .withOption('columnDefs', [{
                    "targets": [4],
                    "orderable": false
                }])
                .withPaginationType('full_numbers')
                .withColumnFilter({
                    aoColumns: [/*null,*/ null, {
                        type: 'text',
                        bRegex: true,
                        bSmart: true
                    }, {
                        type: 'text',
                        bRegex: true,
                        bSmart: true
                    }, {
                        type: 'text',
                        bRegex: true,
                        bSmart: true
                    }, null]
                });
        }
    ]);
