angular.module('mailbox')
    .controller('CoreMailboxController', [
        '$state',
        '$rootScope',
        '$localStorage',
        '$scope',
        function($state, $rootScope, $localStorage, $scope) {
            $rootScope.page = {
                name: "Mailbox",
            };
        }
    ]);
