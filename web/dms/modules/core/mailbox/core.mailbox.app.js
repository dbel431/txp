angular.module('mailbox', [
        'portal'
    ])
    .config([
        '$stateProvider',
        function($stateProvider) {
            $stateProvider
                .state('portal.mailbox', {
                    url: '/mailbox',
                    templateUrl: '/views/core.mailbox.tpl.html',
                    controller: 'CoreMailboxController'
                })
                .state('portal.mailbox.inbox', {
                    url: '/inbox',
                    templateUrl: '/views/core.mailbox.inbox.html',
                    controller: 'CoreMailboxInboxController',
                    resolve: {
                        'Inbox': [
                            'AuthToken',
                            'CoreMailboxService',
                            function(AuthToken, CoreMailboxService) {
                                return CoreMailboxService.get({
                                    to: AuthToken.user._id,
                                    // cc: AuthToken.user._id,
                                    // bcc: AuthToken.user._id,
                                }).$promise;
                            }
                        ]
                    }
                })
                .state('portal.mailbox.read', {
                    url: '/read/:id',
                    templateUrl: '/views/core.mailbox.read.html',
                    controller: 'CoreMailboxReadController',
                    resolve: {
                        'Mail': [
                            '$stateParams',
                            'CoreMailboxService',
                            function($stateParams, CoreMailboxService) {
                                return CoreMailboxService.get({ id: $stateParams.id }).$promise;
                            }
                        ]
                    }
                })
                .state('portal.mailbox.compose', {
                    url: '/compose',
                    templateUrl: '/views/core.mailbox.compose.html',
                    controller: 'CoreMailboxComposeController',
                    resolve: {
                        'Users': [
                            'UserService',
                            function(UserService) {
                                return UserService.get({
                                    active: true
                                }).$promise;
                            }
                        ]
                    }
                })
                .state('portal.mailbox.reply', {
                    url: '/reply/:id',
                    templateUrl: '/views/core.mailbox.reply.html',
                    controller: 'CoreMailboxReplyController',
                    resolve: {
                        'Mail': [
                            '$stateParams',
                            'CoreMailboxService',
                            function($stateParams, CoreMailboxService) {
                                return CoreMailboxService.get({ id: $stateParams.id }).$promise;
                            }
                        ],
                        'Users': [
                            'UserService',
                            function(UserService) {
                                return UserService.get({
                                    active: true
                                }).$promise;
                            }
                        ]
                    }
                })
                .state('portal.mailbox.forward', {
                    url: '/forward/:id',
                    templateUrl: '/views/core.mailbox.forward.html',
                    controller: 'CoreMailboxForwardController',
                    resolve: {
                        'Mail': [
                            '$stateParams',
                            'CoreMailboxService',
                            function($stateParams, CoreMailboxService) {
                                return CoreMailboxService.get({ id: $stateParams.id }).$promise;
                            }
                        ],
                        'Users': [
                            'UserService',
                            function(UserService) {
                                return UserService.get({
                                    active: true
                                }).$promise;
                            }
                        ]
                    }
                })
                .state('portal.mailbox.sent', {
                    url: '/sent',
                    templateUrl: '/views/core.mailbox.sent.html',
                    controller: 'CoreMailboxSentController',
                    resolve: {
                        'Sent': [
                            'AuthToken',
                            'CoreMailboxService',
                            function(AuthToken, CoreMailboxService) {
                                return CoreMailboxService.get({
                                    from: AuthToken.user._id
                                }).$promise;
                            }
                        ]
                    }
                })
                .state('portal.mailbox.readSent', {
                    url: '/read-sent/:id',
                    templateUrl: '/views/core.mailbox.read-sent.html',
                    controller: 'CoreMailboxReadSentController',
                    resolve: {
                        'Mail': [
                            '$stateParams',
                            'CoreMailboxService',
                            function($stateParams, CoreMailboxService) {
                                return CoreMailboxService.get({ id: $stateParams.id }).$promise;
                            }
                        ]
                    }
                });
        }

    ])
