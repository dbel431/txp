angular.module('quickLinks', [
        'portal'
    ])
    .config([
        '$stateProvider',
        function($stateProvider) {
            $stateProvider
                .state('portal.quickLinks', {
                    url: '/quick-links',
                    templateUrl: '/views/core.portal.quick-links.html',
                    controller: 'CoreQuickLinksController'
                })

        }
    ])