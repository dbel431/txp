angular.module('quickLinks')
    .controller('CoreQuickLinksController', [
        '$rootScope',
        '$localStorage',
        '$scope',
        'ACLActions',
        'UserService',
        function($rootScope, $localStorage, $scope, ACLActions, UserService) {
            $scope.loading = false;
            $scope.aclActions = {};
            for (var i = ACLActions.length - 1; i >= 0; i--) {
                $scope.aclActions[ACLActions[i].object] = ACLActions[i];
            };
            $scope.clearHistory = function() {
                $scope.loading = true;
                $rootScope.currentUser.quickLinks = {};
                var newUserObj = new UserService($rootScope.currentUser);
                newUserObj.$update(function() {
                    showMessages([{
                        type: 'success',
                        message: "History cleared!",
                        header: 'Quick Links'
                    }]);
                    $scope.loading = false;
                }, function(err) {
                    showMessages([{
                        type: 'error',
                        message: "History could not be cleared!",
                        header: 'Quick Links'
                    }]);
                    $scope.loading = false;
                });
            };
        }
    ])
