angular.module('core')
    .service('CoreFileDownloadService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/file-download');
        }
    ]);
