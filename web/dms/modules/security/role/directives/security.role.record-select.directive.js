angular.module('security').directive('securityRoleRecordSelect', function() {
    return {
        scope: {
            dtData: '=',
            selectedRecordList: '=',
            toggleRecord: '='
        },
        restrict: 'EA',
        templateUrl: '/views/security.role.record-select.tpl.html',
        replace: true,
        link: function(scope, elt, attb) {
            scope.record = {};
            for (var i = scope.dtData.length - 1; i >= 0; i--) {
                if (scope.dtData[i]._id == attb.recordId) scope.record = scope.dtData[i];
            }
        }
    };
});
