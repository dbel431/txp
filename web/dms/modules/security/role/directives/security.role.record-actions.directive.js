angular.module('security').directive('securityRoleRecordActions', [
    '$rootScope',
    '$localStorage',
    function($rootScope, $localStorage) {
        return {
            scope: {
                'recordId': '=',
                'openDeleteModal1': '=',
            },
            restrict: 'EA',
            templateUrl: '/views/security.role.record-actions.tpl.html',
            replace: true,
            link: function(scope, elt, attb) {
                scope.isActionPermitted = $rootScope.isActionPermitted;
            }
        };
    }
]);
