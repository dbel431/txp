angular.module('security').directive('securityRoleRecordOptions', function() {
    return {
    	scope: {
    		'selectedRecordList' : '=',
            'toggleAllRecords': '=',
            'isActionPermitted': '=',
            'openDeleteModal': '=',
            'allChecked': '=',
    	},
        restrict: 'EA',
        templateUrl: '/views/security.role.record-options.tpl.html',
        replace: true
    };
});
