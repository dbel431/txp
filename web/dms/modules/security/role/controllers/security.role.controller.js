angular.module('security.role')
    .controller('RoleController', [
        '$rootScope',
        '$localStorage',
        '$scope',
        '$state',
        function($rootScope, $localStorage, $scope, $state) {
            $rootScope.page = {
                name: 'Roles'
            };
        }
    ]);
