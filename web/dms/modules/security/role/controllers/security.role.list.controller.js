angular.module('security.role')
    .controller('RoleListController', [
        '$rootScope',
        '$localStorage',
        '$scope',
        '$state',
        '$compile',
        '$uibModal',
        'DTOptionsBuilder',
        'RoleService',
        function($rootScope, $localStorage, $scope, $state, $compile, $uibModal, DTOptionsBuilder, RoleService) {
            $rootScope.page = {
                name: 'Role List',
                actions: {
                    add: {
                        object: 'security.role.add',
                        allowed: function() {
                            return $rootScope.isActionPermitted(this.object);
                        }
                    }
                }
            };
            $scope.selectedRoles = [];
            $scope.selectedRoleRecords = {};
            $scope.dtData = [];
            $scope.toggleAllRecords = function(select) {
                for (var i = $scope.dtData.length - 1; i >= 0; i--) {
                    $scope.dtData[i].checked = select || false;
                    if (select) {
                        if ($scope.selectedRoles.indexOf($scope.dtData[i]._id) < 0) {
                            $scope.selectedRoles.push($scope.dtData[i]._id);
                            $scope.selectedRoleRecords[$scope.dtData[i]._id] = $scope.dtData[i];
                        }
                    } else if ($scope.selectedRoles.indexOf($scope.dtData[i]._id) >= 0) {
                        if ($scope.selectedRoles.indexOf($scope.dtData[i]._id) >= 0) {
                            $scope.selectedRoles.splice($scope.selectedRoles.indexOf($scope.dtData[i]._id), 1);
                            delete $scope.selectedRoleRecords[$scope.dtData[i]._id];
                        }
                    }
                }
            };
            $scope.toggleRecord = function(id) {
                for (var i = $scope.dtData.length - 1; i >= 0; i--) {
                    if ($scope.dtData[i]._id == id) {
                        if ($scope.dtData[i].checked) {
                            if ($scope.selectedRoles.indexOf(id) < 0) {
                                $scope.selectedRoles.push(id);
                                $scope.selectedRoleRecords[id] = $scope.dtData[i];
                            }
                        } else {
                            if ($scope.selectedRoles.indexOf(id) >= 0) {
                                $scope.selectedRoles.splice($scope.selectedRoles.indexOf(id), 1);
                                delete $scope.selectedRoleRecords[id];
                            }
                        }
                        break;
                    }
                }
            };
            $scope.allChecked = function() {
                for (var i = $scope.dtData.length - 1; i >= 0; i--) {
                    if (!$scope.dtData[i].checked) return false;
                }
                return true;
            };
            $scope.isActionPermitted = $rootScope.isActionPermitted;
            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withPaginationType('full_numbers')
                .withOption('ajax', RoleService.dataTable)
                .withDataProp('data')
                .withOption('processing', true)
                .withOption('serverSide', true)
                .withOption('oLanguage', {
                    "sEmptyTable": "No matching records found"
                })
                .withOption('order', [1, 'asc'])
                .withOption('drawCallback', function(settings) {
                    var listOfRecords = this.api().data();
                    $scope.dtData = [];
                    for (var i = listOfRecords.length - 1; i >= 0; i--) {
                        $scope.dtData[i] = (listOfRecords[i]);
                        $scope.dtData[i].checked = ($scope.selectedRoles.indexOf($scope.dtData[i]._id) >= 0);
                    }
                    $compile('security-role-record-select')($scope);
                    $compile('security-role-record-options')($scope);
                    $compile('security-role-record-actions')($scope);
                })
                .withPaginationType('full_numbers')
                .withOption('aoColumns', [{
                    title: '<security-role-record-options is-action-permitted="isActionPermitted" open-delete-modal="openDeleteModal" selected-record-list="selectedRoles" dt-data="dtData" all-checked="allChecked" toggle-all-records="toggleAllRecords"></security-role-record-options>',
                    orderable: false,
                    class: 'text-center',
                    width: '1%',
                    data: '_id',
                    render: function(id) {
                        return '<security-role-record-select dt-data="dtData" selected-record-list="selectedRoles" record-id="' + id + '" toggle-record="toggleRecord"></security-role-record-select>';
                    }
                }, {
                    title: 'Role Name',
                    width: '40%',
                    name: 'name',
                    data: 'name',
                    render: function(name) {
                        return name || "";
                    }
                }, {
                    title: 'Status',
                    width: '30%',
                    // class: 'text-center',
                    name: 'status',
                    data: 'active',
                    render: function(active) {
                        return active ? '<span class="">Active</span>' : '<span class="">Inactive</span>';
                    }
                }, {
                    title: 'Actions',
                    orderable: false,
                    class: 'text-center',
                    width: '5%',
                    data: '_id',
                    render: function(id) {
                        return '<security-role-record-actions open-delete-modal1="openDeleteModal1" record-id="\'' + id + '\'"></security-role-record-actions>';
                    }
                }])
                .withLightColumnFilter({
                    '1': {
                        type: 'text',
                        time: 600
                    },
                    '2': {
                        type: 'select',
                        values: [{
                            value: true,
                            label: 'Active'
                        }, {
                            value: false,
                            label: 'Inactive'
                        }]
                    }
                });
            $rootScope.openDeleteModal = function() {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/security.role.remove.html',

                    resolve: {
                        Roles: function() {
                            return RoleService.get({
                                id: $scope.selectedRoles
                            }).$promise;
                        }
                    },
                    controller: 'RoleRemoveController'
                });
                modalInstance.result.then(function(messages) {
                    var success = 0,
                        error = 0;
                    for (var i in messages) {
                        if (messages[i].type == 'success') success++;
                        if (messages[i].type == 'error') error++;
                    }
                    messages = [];
                    if (success > 0) messages.push({
                        type: 'success',
                        header: 'Remove Role',
                        message: success + " record(s) removed successfully!"
                    });
                    if (error > 0) messages.push({
                        type: 'error',
                        header: 'Remove Role',
                        message: error + " record(s) could not be removed successfully!"
                    });
                    if (messages.length > 0) showMessages(messages);
                    $state.reload();
                });
            };
            $scope.openDeleteModal1 = function(recordId) {
                $scope.toggleAllRecords(false);
                $scope.selectedRoles = [recordId];
                $rootScope.openDeleteModal();
            };
        }
    ]);
