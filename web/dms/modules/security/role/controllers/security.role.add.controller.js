angular.module('security.role')
    .controller('RoleAddController', [
        '$rootScope',
        '$localStorage',
        '$state',
        '$scope',
        'DTOptionsBuilder',
        'RoleService',
        'ACLPageActions',
        'Directories',
        function($rootScope, $localStorage, $state, $scope, DTOptionsBuilder, RoleService, ACLPageActions, Directories) {
            $rootScope.page = {
                name: 'Add new Role',
                actions: {
                    back: {
                        object: 'security.role.list',
                        allowed: function() {
                            return $rootScope.isActionPermitted(this.object);
                        }
                    }
                }
            };
            $scope.loading = false;
            $scope.directoryArray = Directories;
            $scope.role = new RoleService();
            $scope.role.actions = [];
            $scope.aclPageActions = ACLPageActions;
            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withOption('columnDefs', [{
                    "targets": [2, 3, 4, 5],
                    "orderable": false
                }])
                .withPaginationType('full_numbers')
                .withColumnFilter({
                    aoColumns: [{
                        type: 'text',
                        bRegex: true,
                        bSmart: true
                    }, {
                        type: 'text',
                        bRegex: true,
                        bSmart: true
                    }]
                });
            $scope.checkChange = function(aclAction) {
                if (aclAction.selected) {
                    if ($scope.role.actions.indexOf(aclAction._id) < 0)
                        $scope.role.actions.push(aclAction._id);
                } else {
                    if ($scope.role.actions.indexOf(aclAction._id) >= 0)
                        $scope.role.actions.splice($scope.role.actions.indexOf(aclAction._id), 1);
                }
            }
            $scope.add = function() {
                $scope.loading = true;
                $scope.role.$save(function(role) {
                    showMessages([{
                        type: 'success',
                        message: 'Role named: <b>' + role.name + '</b> added successfully!',
                        header: 'Add Role'
                    }]);
                    $scope.roleForm.$setUntouched();
                    $state.go('portal.security.role.list');
                    $scope.loading = false;
                }, function(err) {
                    showMessages([{
                        type: 'error',
                        message: err.data.message,
                        header: 'Add Role'
                    }]);
                    $scope.roleForm.$setUntouched();
                    $scope.loading = false;
                })
            }
        }
    ]);
