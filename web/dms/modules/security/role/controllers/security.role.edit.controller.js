angular.module('security.role')
    .controller('RoleEditController', [
        '$rootScope',
        '$localStorage',
        '$state',
        '$stateParams',
        '$scope',
        'DTOptionsBuilder',
        'Role',
        'ACLPageActions',
        'Directories',
        function($rootScope, $localStorage, $state, $stateParams, $scope, DTOptionsBuilder, Role, ACLPageActions, Directories) {
            $rootScope.page = {
                name: 'Edit Role',
                actions: {
                    back: {
                        object: 'security.role.list',
                        allowed: function() {
                            return $rootScope.isActionPermitted(this.object);
                        }
                    }
                }
            };
            $scope.loading = false;
            $scope.directoryArray = Directories;
            $scope.role = Role[0];
            $scope.aclPageActions = ACLPageActions;
            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withPaginationType('full_numbers')
                .withColumnFilter({
                    aoColumns: [{
                        type: 'text',
                        bRegex: true,
                        bSmart: true
                    }, {
                        type: 'text',
                        bRegex: true,
                        bSmart: true
                    }]
                });

            $scope.checkChange = function(aclAction) {
                if (aclAction.selected) {
                    if ($scope.role.actions.indexOf(aclAction._id) < 0)
                        $scope.role.actions.push(aclAction._id);
                } else {
                    if ($scope.role.actions.indexOf(aclAction._id) >= 0)
                        $scope.role.actions.splice($scope.role.actions.indexOf(aclAction._id), 1);
                }
            }

            $scope.edit = function() {
                $scope.loading = true;
                $scope.role.$update({
                    id: $scope.role._id
                }, function(role) {
                    showMessages([{
                        type: 'success',
                        message: 'Details updated successfully!',
                        header: 'Edit Role'
                    }]);
                    $state.go('portal.security.role.list');
                    $scope.loading = false;
                }, function(err) {
                    showMessages([{
                        type: 'error',
                        message: err.data.message,
                        header: 'Edit Role'
                    }]);
                    $scope.loading = false;
                })
            }
        }
    ]);
