angular.module('security.role')
    .controller('RoleRemoveController', [
        '$rootScope',
        '$localStorage',
        '$scope',
        '$uibModalInstance',
        'Roles',
        function($rootScope, $localStorage, $scope, $uibModalInstance, Roles) {
            $scope.roles = Roles;
            $scope.cancel = function() {
                $uibModalInstance.dismiss('cancel');
            };
            var messages = [];
            $scope.remove = function() {
                for (var i = $scope.roles.length - 1; i >= 0; i--) {
                    $scope.roles[i].$remove({
                        id: $scope.roles[i]._id
                    }, function(role) {
                        messages.push({
                            type: 'success',
                            message: 'Record named: <b>' + role.name + '</b> deleted successfully!',
                            header: 'Remove Record'
                        });
                        syncXhr();
                    }, function(err) {
                        messages.push({
                            type: 'error',
                            message: 'Record named: <b>' + role.name + '</b> could not be deleted successfully!',
                            header: 'Remove Record'
                        });
                        syncXhr();
                    });
                }
            };
            var count = 0;

            function syncXhr() {
                count++;
                if ($scope.loading = (count == $scope.roles.length)) $uibModalInstance.close(messages);
            }
        }
    ]);
