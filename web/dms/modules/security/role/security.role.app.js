angular.module('security.role', [
        'security'
    ])
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $stateProvider.state('portal.security.role', {
                url: '/role',
                templateUrl: '/views/security.role.tpl.html',
                controller: 'RoleController'
            }).state('portal.security.role.list', {
                url: '/list',
                templateUrl: '/views/security.role.list.html',
                controller: 'RoleListController'
            }).state('portal.security.role.add', {
                url: '/add',
                templateUrl: '/views/security.role.add.html',
                resolve: {
                    ACLActions: [
                        'ACLActionService',
                        function(ACLActionService) {
                            return ACLActionService.get({ active: true }).$promise;
                        }
                    ],
                    ACLPageActions: [
                        'ACLActions',
                        function(ACLActions) {
                            var ACLPageActions = {};
                            for (var i = 0; i < ACLActions.length; i++) {
                                if (ACLActions[i].module && ACLActions[i].module.active && ACLActions[i].page && ACLActions[i].page.active) {
                                    ACLActions[i].selected = false;
                                    if (!ACLPageActions[ACLActions[i].page._id]) ACLPageActions[ACLActions[i].page._id] = ACLActions[i].page;
                                    ACLPageActions[ACLActions[i].page._id].module = ACLActions[i].module;
                                    if (!ACLPageActions[ACLActions[i].page._id].actions) ACLPageActions[ACLActions[i].page._id].actions = {};
                                    if (ACLActions[i].actionType) {
                                        if (!ACLPageActions[ACLActions[i].page._id].actions[ACLActions[i].actionType]) ACLPageActions[ACLActions[i].page._id].actions[ACLActions[i].actionType] = [];
                                        ACLPageActions[ACLActions[i].page._id].actions[ACLActions[i].actionType].push(ACLActions[i]);
                                    }
                                }
                            }
                            return ACLPageActions;
                        }
                    ],
                    Directories: [
                        'DirectoryService',
                        function(DirectoryService) {
                            return DirectoryService.get({ active: true }).$promise;
                        }
                    ]
                },
                controller: 'RoleAddController',
            }).state('portal.security.role.edit', {
                url: '/edit/:id',
                templateUrl: '/views/security.role.edit.html',
                resolve: {
                    ACLActions: [
                        'ACLActionService',
                        function(ACLActionService) {
                            return ACLActionService.get({ active: true }).$promise;
                        }
                    ],
                    'Role': [
                        'RoleService',
                        '$stateParams',
                        function(RoleService, $stateParams) {
                            return RoleService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ],
                    ACLPageActions: [
                        'Role',
                        'ACLActions',
                        function(Role, ACLActions) {
                            var ACLPageActions = {};
                            for (var i = 0; i < ACLActions.length; i++) {
                                if (ACLActions[i].module && ACLActions[i].module.active && ACLActions[i].page && ACLActions[i].page.active) {
                                    ACLActions[i].selected = (Role[0].actions.indexOf(ACLActions[i]._id) >= 0);
                                    if (!ACLPageActions[ACLActions[i].page._id]) ACLPageActions[ACLActions[i].page._id] = ACLActions[i].page;
                                    ACLPageActions[ACLActions[i].page._id].module = ACLActions[i].module;
                                    if (!ACLPageActions[ACLActions[i].page._id].actions) ACLPageActions[ACLActions[i].page._id].actions = {};
                                    if (ACLActions[i].actionType) {
                                        if (!ACLPageActions[ACLActions[i].page._id].actions[ACLActions[i].actionType]) ACLPageActions[ACLActions[i].page._id].actions[ACLActions[i].actionType] = [];
                                        ACLPageActions[ACLActions[i].page._id].actions[ACLActions[i].actionType].push(ACLActions[i]);
                                    }
                                }
                            }
                            return ACLPageActions;
                        }
                    ],
                    Directories: [
                        'DirectoryService',
                        function(DirectoryService) {
                            return DirectoryService.get({ active: true }).$promise;
                        }
                    ]
                },
                controller: 'RoleEditController'
            }).state('portal.security.role.remove', {
                url: '/remove/:id',
                templateUrl: '/views/security.role.remove.html',
                resolve: {
                    'Role': [
                        'RoleService',
                        '$stateParams',
                        function(RoleService, $stateParams) {
                            return RoleService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ]
                },
                controller: 'RoleRemoveController'
            });
        }
    ]);
