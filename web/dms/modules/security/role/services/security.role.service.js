angular.module('security.role').service('RoleService', [
    '$resource',
    function($resource) {
        return $resource(API_PATH + '/role', {}, {
            get: {
                method: 'GET',
                isArray: true
            },
            dataTable: {
                method: 'POST',
                url: API_PATH + '/role/data-table'
            },
            update: {
                method: 'PUT'
            }
        });
    }
]);