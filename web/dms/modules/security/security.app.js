angular.module('security', [
        'portal'
    ])
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $stateProvider.state('portal.security', {
                url: '/security',
                templateUrl: '/views/core.portal.page.tpl.html',
                controller: 'SecurityController'
            });
        }
    ])
    .controller('SecurityController', [
        '$state',
        '$scope',
        function($state, $scope) {}
    ]);
