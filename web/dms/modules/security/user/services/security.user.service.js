angular.module('security.user').service('UserService', [
    '$resource',
    function($resource) {
        return $resource(API_PATH + '/user', {}, {
            get: {
                method: 'GET',
                isArray: true
            },
            dataTable: {
                method: 'POST',
                url: API_PATH + '/user/data-table'
            },
            update: {
                method: 'PUT',
            }
        });
    }
]);
