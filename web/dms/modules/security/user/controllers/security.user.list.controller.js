angular.module('security.user')
    .controller('UserListController', [
        '$rootScope',
        '$localStorage',
        '$scope',
        '$state',
        '$compile',
        '$uibModal',
        'DTOptionsBuilder',
        'UserService',
        'Roles',
        function($rootScope, $localStorage, $scope, $state, $compile, $uibModal, DTOptionsBuilder, UserService, Roles) {
            $rootScope.page = {
                name: 'User List',
                actions: {
                    add: {
                        object: 'security.user.add',
                        allowed: function() {
                            return $rootScope.isActionPermitted(this.object);
                        }
                    }
                }
            };
            $scope.selectedUsers = [];
            $scope.selectedUserRecords = {};
            $scope.dtData = [];
            $scope.roleArray = [];
            $scope.roleMap = {};
            for (var i = Roles.length - 1; i >= 0; i--) {
                $scope.roleArray[i] = {
                    label: Roles[i].name,
                    value: Roles[i]._id
                };
                $scope.roleMap[Roles[i]._id] = Roles[i].name;
            }
            $scope.toggleAllRecords = function(select) {
                for (var i = $scope.dtData.length - 1; i >= 0; i--) {
                    $scope.dtData[i].checked = select || false;
                    if (select) {
                        if ($scope.selectedUsers.indexOf($scope.dtData[i]._id) < 0) {
                            $scope.selectedUsers.push($scope.dtData[i]._id);
                            $scope.selectedUserRecords[$scope.dtData[i]._id] = $scope.dtData[i];
                        }
                    } else if ($scope.selectedUsers.indexOf($scope.dtData[i]._id) >= 0) {
                        if ($scope.selectedUsers.indexOf($scope.dtData[i]._id) >= 0) {
                            $scope.selectedUsers.splice($scope.selectedUsers.indexOf($scope.dtData[i]._id), 1);
                            delete $scope.selectedUserRecords[$scope.dtData[i]._id];
                        }
                    }
                }
            };
            $scope.toggleRecord = function(id) {
                for (var i = $scope.dtData.length - 1; i >= 0; i--) {
                    if ($scope.dtData[i]._id == id) {
                        if ($scope.dtData[i].checked) {
                            if ($scope.selectedUsers.indexOf(id) < 0) {
                                $scope.selectedUsers.push(id);
                                $scope.selectedUserRecords[id] = $scope.dtData[i];
                            }
                        } else {
                            if ($scope.selectedUsers.indexOf(id) >= 0) {
                                $scope.selectedUsers.splice($scope.selectedUsers.indexOf(id), 1);
                                delete $scope.selectedUserRecords[id];
                            }
                        }
                        break;
                    }
                }
            };
            $scope.allChecked = function() {
                for (var i = $scope.dtData.length - 1; i >= 0; i--) {
                    if (!$scope.dtData[i].checked) return false;
                }
                return true;
            };
            $scope.isActionPermitted = $rootScope.isActionPermitted;
            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withPaginationType('full_numbers')
                .withOption('ajax', UserService.dataTable)
                .withDataProp('data')
                .withOption('oLanguage', {
                    "sEmptyTable": "No matching records found"
                })
                .withOption('processing', true)
                .withOption('serverSide', true)
                // .withOption('sDom', '<fl>rt<"clear"i><"F"p>')
                .withOption('order', [2, 'asc'])
                .withOption('drawCallback', function(settings) {
                    var listOfRecords = this.api().data();
                    $scope.dtData = [];
                    for (var i = listOfRecords.length - 1; i >= 0; i--) {
                        $scope.dtData[i] = (listOfRecords[i]);
                        $scope.dtData[i].checked = ($scope.selectedUsers.indexOf($scope.dtData[i]._id) >= 0);
                    }
                    $compile('security-user-record-select')($scope);
                    $compile('security-user-record-options')($scope);
                    $compile('security-user-record-actions')($scope);
                })
                .withPaginationType('full_numbers')
                .withOption('aoColumns', [{
                    title: '<security-user-record-options is-action-permitted="isActionPermitted" open-delete-modal="openDeleteModal" selected-record-list="selectedUsers" dt-data="dtData" all-checked="allChecked" toggle-all-records="toggleAllRecords"></security-user-record-options>',
                    orderable: false,
                    class: 'text-center',
                    width: '2%',
                    data: '_id',
                    render: function(id) {
                        return '<security-user-record-select dt-data="dtData" selected-record-list="selectedUsers" record-id="' + id + '" toggle-record="toggleRecord"></security-user-record-select>';
                    }
                }, {
                    title: 'User Name',
                    name: 'name',
                    width: '20%',
                    data: 'details.personal',
                    render: function(personal, type, row) {
                        var name = personal.name;
                        return name ? [name.first, name.middle, name.last].join(' ') : "";
                    }
                }, {
                    title: 'Email',
                    width: '20%',
                    name: 'email',
                    data: 'auth.email',
                    render: function(email, type, row) {
                        return email || "";
                    }
                }, {
                    title: 'Role',
                    width: '20%',
                    name: 'role',
                    data: 'role',
                    render: function(role, type, row) {
                        return $scope.roleMap[role] || "";
                    }
                }, {
                    title: 'Status',
                    width: '10%',
                    //class: 'text-center',
                    name: 'status',
                    data: 'active',
                    render: function(active) {
                        return active ? '<span class="">Active</span>' : '<span class="">Inactive</span>';
                    }
                }, {
                    title: 'Actions',
                    orderable: false,
                    class: 'text-center',
                    width: '7%',
                    data: '_id',
                    render: function(id) {
                        return '<security-user-record-actions open-delete-modal1="openDeleteModal1" record-id="\'' + id + '\'"></security-user-record-actions>';
                    }
                }])
                .withLightColumnFilter({
                    '1': {
                        type: 'text',
                        time: 600
                    },
                    '2': {
                        type: 'text',
                        time: 600
                    },
                    '3': {
                        type: 'select',
                        values: $scope.roleArray
                    },
                    '4': {
                        type: 'select',
                        values: [{
                            value: 'true',
                            label: 'Active'
                        }, {
                            value: 'false',
                            label: 'Inactive'
                        }]
                    }
                });
            $rootScope.openDeleteModal = function() {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/security.user.remove.html',
                    /*                    size: 'lg',
                     */
                    resolve: {
                        Users: function() {
                            return UserService.get({
                                id: $scope.selectedUsers
                            }).$promise;
                        }
                    },
                    controller: 'UserRemoveController'
                });
                modalInstance.result.then(function(messages) {
                    var success = 0,
                        error = 0;
                    for (var i in messages) {
                        if (messages[i].type == 'success') success++;
                        if (messages[i].type == 'error') error++;
                    }
                    messages = [];
                    if (success > 0) messages.push({
                        type: 'success',
                        header: 'Remove User',
                        message: success + " record(s) removed successfully!"
                    });
                    if (error > 0) messages.push({
                        type: 'error',
                        header: 'Remove User',
                        message: error + " record(s) could not be removed successfully!"
                    });
                    if (messages.length > 0) showMessages(messages);
                    $state.reload();
                });
            };
            $scope.openDeleteModal1 = function(recordId) {
                $scope.toggleAllRecords(false);
                $scope.selectedUsers = [recordId];
                $rootScope.openDeleteModal();
            };

            /*  $rootScope.openVersionHistoryModal = function(recordId) {
                  var modalInstance = $uibModal.open({
                      backdrop: 'static',
                      animation: true,
                      templateUrl: '/views/directory.organization.version-history.tpl.html',
                      size: 'lg',
                      resolve: {
                          Organization: [
                              'DirectoryOrganizationService',
                              function(DirectoryOrganizationService) {
                                  return DirectoryOrganizationService.get({
                                      id: recordId
                                  }).$promise;
                              }
                          ]
                      },
                      controller: [
                          '$rootScope',
'$localStorage',
                          '$scope',
                          '$uibModalInstance',
                          'Organization',
                          function($rootScope, $localStorage, $modalScope, $uibModalInstance, Organization) {
                              $modalScope.organization = Organization.data;
                              $modalScope.cancel = function() {
                                  $uibModalInstance.dismiss('cancel');
                              };
                          }
                      ]
                  });
                  modalInstance.result.then(function(messages) {
                      if (messages.length > 0) showMessages(messages);
                      $state.reload();
                  });
              };*/
        }
    ]);
