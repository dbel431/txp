angular.module('security.user')
    .controller('UserController', [
        '$rootScope',
        '$localStorage',
        '$scope',
        '$state',
        function($rootScope, $localStorage, $scope, $state) {
            $rootScope.page = {
                name: 'Users'
            };
        }
    ]);
