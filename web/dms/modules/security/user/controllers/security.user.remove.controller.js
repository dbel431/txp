angular.module('security.user')
    .controller('UserRemoveController', [
        '$rootScope',
        '$localStorage',
        '$scope',
        '$uibModalInstance',
        'Users',
        function($rootScope, $localStorage, $scope, $uibModalInstance, Users) {
            $scope.users = Users;
            $scope.cancel = function() {
                $uibModalInstance.dismiss('cancel');
            };
            var messages = [];
            $scope.remove = function() {
                for (var i = $scope.users.length - 1; i >= 0; i--) {
                    $scope.users[i].$remove({
                        id: $scope.users[i]._id
                    }, function(user) {
                        messages.push({
                            type: 'success',
                            message: 'Record for <b>' + user.auth.email + '</b> deleted successfully!',
                            header: 'Remove Record'
                        });
                        syncXhr();
                    }, function(err) {
                        messages.push({
                            type: 'error',
                            message: 'Record for <b>' + user.auth.email + '</b> could not be deleted successfully!',
                            header: 'Remove Record'
                        });
                        syncXhr();
                    });
                }
            };
            var count = 0;

            function syncXhr() {
                count++;
                if ($scope.loading = (count == $scope.users.length)) $uibModalInstance.close(messages);
            }
        }
    ]);
