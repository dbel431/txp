angular.module('security.user')
    .controller('UserAddController', [
        '$rootScope',
        '$localStorage',
        '$scope',
        '$state',
        'UserService',
        'Roles',
        function($rootScope, $localStorage, $scope, $state, UserService, Roles) {
            $rootScope.page = {
                name: 'Add new User',
                actions: {
                    back: {
                        object: 'security.user.list',
                        allowed: function() {
                            return $rootScope.isActionPermitted(this.object);
                        }
                    }
                }
            };
            $scope.loading = false;
            $scope.roleArray = Roles;
            $scope.saluteArray = [{
                id: 'mr',
                label: 'Mr.'
            }, {
                id: 'ms',
                label: 'Ms.'
            }, {
                id: 'mrs',
                label: 'Mrs.'
            }, {
                id: 'dr',
                label: 'Dr.'
            }];
            $scope.user = new UserService();
            $scope.accessDateOptions = {
                minDate: new Date($scope.user.accessDate),
                maxDate: new Date($scope.user.expiryDate)
            };
            $scope.expiryDateOptions = {
                minDate: new Date($scope.user.accessDate),
                maxDate: new Date($scope.user.expiryDate)
            };
            $scope.$watch('user.expiryDate', function(newVal) {
                $scope.accessDateOptions.maxDate = new Date(newVal || '');
                $scope.expiryDateOptions.maxDate = new Date(newVal || '');
            });
            $scope.$watch('user.accessDate', function(newVal) {
                $scope.accessDateOptions.minDate = new Date(newVal || '');
                $scope.expiryDateOptions.minDate = new Date(newVal || '');
            });
            $scope.add = function() {
                $scope.loading = true;

                $scope.user.$save(function(res) {
                    showMessages([{
                        type: 'success',
                        message: "Add User",
                        header: 'User Added successfully!'
                    }]);
                    $scope.loading = false;
                    $scope.userForm.$setUntouched();
                    $state.go('portal.security.user.list');
                }, function(err) {
                    showMessages([{
                        type: 'error',
                        message: err.data.message || "Could not add User! Please try again!",
                        header: 'Add User'
                    }]);
                    $scope.userForm.$setUntouched();
                    $scope.loading = false;
                });
            };
        }
    ]);
