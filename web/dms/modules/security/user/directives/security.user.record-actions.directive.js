angular.module('security').directive('securityUserRecordActions', [
    '$rootScope',
    '$localStorage',
    function($rootScope, $localStorage) {
        return {
            scope: {
                'recordId': '=',
                'openDeleteModal1': '=',
            },
            restrict: 'EA',
            templateUrl: '/views/security.user.record-actions.tpl.html',
            replace: true,
            link: function(scope, elt, attb) {
                scope.isActionPermitted = $rootScope.isActionPermitted;
            }
        };
    }
]);
