angular.module('security').directive('securityUserRecordOptions', function() {
    return {
    	scope: {
    		'selectedRecordList' : '=',
            'toggleAllRecords': '=',
            'isActionPermitted': '=',
            'openDeleteModal': '=',
            'allChecked': '=',
    	},
        restrict: 'EA',
        templateUrl: '/views/security.user.record-options.tpl.html',
        replace: true
    };
});
