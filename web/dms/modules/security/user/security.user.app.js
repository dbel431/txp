angular.module('security.user', [
        'security'
    ])
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $stateProvider.state('portal.security.user', {
                url: '/user',
                template: '<ui-view></ui-view>',
                controller: 'UserController',
            }).state('portal.security.user.list', {
                url: '/list',
                templateUrl: '/views/security.user.list.html',
                resolve: {
                    'Roles': [
                        'RoleService',
                        function(RoleService) {
                            return RoleService.get({ active: true }).$promise;
                        }
                    ]
                },
                controller: 'UserListController'
            }).state('portal.security.user.add', {
                url: '/add',
                templateUrl: '/views/security.user.add.html',
                resolve: {
                    'Roles': [
                        'RoleService',
                        function(RoleService) {
                            return RoleService.get({ active: true }).$promise;
                        }
                    ]
                },
                controller: 'UserAddController'
            }).state('portal.security.user.edit', {
                abstract: true,
                url: '/edit/:id',
                templateUrl: '/views/security.user.edit.tpl.html',
                resolve: {
                    'User': [
                        'UserService',
                        '$stateParams',
                        function(UserService, $stateParams) {
                            return UserService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ],
                    'Roles': [
                        'RoleService',
                        function(RoleService) {
                            return RoleService.get({ active: true }).$promise;
                        }
                    ]
                },
                controller: 'UserEditController'
            }).state('portal.security.user.edit.account', {
                url: '/account',
                templateUrl: '/views/security.user.edit.account.html',
                controller: 'UserEditAccountController'
            }).state('portal.security.user.edit.personal', {
                url: '/personal',
                templateUrl: '/views/security.user.edit.personal.html',
                controller: 'UserEditPersonalController'
            }).state('portal.security.user.remove', {
                url: '/remove/:id',
                templateUrl: '/views/security.user.remove.html',
                resolve: {
                    'User': [
                        'UserService',
                        '$stateParams',
                        function(UserService, $stateParams) {
                            return UserService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ]
                },
                controller: 'UserRemoveController'
            }).state('portal.security.user.remove1', {
                url: '/remove/:id',
                templateUrl: '/views/security.user.remove1.html',
                resolve: {
                    'User': [
                        'UserService',
                        '$stateParams',
                        function(UserService, $stateParams) {
                            return UserService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ]
                },
                controller: 'UserRemoveController'
            });
        }
    ]);
