angular.module('operation', [
        'portal'
    ])
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $stateProvider.state('portal.operation', {
                url: '/operation',
                templateUrl: '/views/core.portal.page.tpl.html'
            });
        }
    ]);
