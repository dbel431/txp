angular.module('operation.mailManagement', [
        'operation'
    ])
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $stateProvider.state('portal.operation.mailManagement', {
                url: '/mial-management',
                templateUrl: '/views/core.portal.action.tpl.html'
            }).state('portal.operation.mailManagement.mailLog', {
                url: '/mail-log',
                templateUrl: '/views/operation.mial-management.mail-log.html'
            });
        }
    ]);
