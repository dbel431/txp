angular.module('operation.mailManagement')
    .controller('MailLogListController', [
        '$rootScope',
        '$localStorage',
        '$scope',
        '$state',
        '$compile',
        '$uibModal',
        'DTOptionsBuilder',
        'MailLogService',
        function($rootScope, $localStorage, $scope, $state, $compile, $uibModal, DTOptionsBuilder, MailLogService) {
            $rootScope.page = {
                name: 'Mail Log'
            };
            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withPaginationType('full_numbers')
                .withOption('ajax', MailLogService.dataTable)
                .withDataProp('data')
                .withOption('processing', true)
                .withOption('serverSide', true)
                .withOption('oLanguage', {
                    "sEmptyTable": "No matching records found"
                })
                .withOption('order', [1, 'asc'])
                .withPaginationType('full_numbers')
                .withOption('drawCallback', function(settings) {
                    $compile('operation-mail-management-record-actions')($scope);
                })
                .withOption('aoColumns', [{
                    title: 'Mail Type',
                    width: '20%',
                    name: 'type',
                    data: 'type',
                    render: function(type) {
                        return type || "";
                    }
                }, {
                    title: 'Mail To',
                    width: '20%',
                    name: 'to',
                    data: 'to',
                    render: function(to) {
                        return to || "";
                    }
                }, {
                    title: 'Mail Subject',
                    width: '20%',
                    name: 'subject',
                    data: 'subject',
                    render: function(subject) {
                        return subject || "";
                    }
                }, {
                    title: 'Sent On',
                    width: '40%',
                    name: 'sent',
                    data: 'created.at',
                    render: function(sent) {
                        return (sent ? date_format(sent, 'D, jS M Y, g:i:s a') : null) || "";
                    }
                }, {
                    title: 'Delivered On',
                    width: '40%',
                    name: 'delivered',
                    data: 'delivered',
                    render: function(delivered) {
                        return (delivered ? date_format(delivered, 'D, jS M Y, g:i:s a') : null) || "";
                    }
                }, {
                    title: 'Actions',
                    orderable: false,
                    class: 'text-center',
                    width: '5%',
                    data: '_id',
                    render: function(id) {
                        return '<operation-mail-log-record-actions open-delete-modal1="openDeleteModal1" record-id="\'' + id + '\'"></operation-mail-log-record-actions>';
                    }
                }])
                .withLightColumnFilter({
                    '1': {
                        type: 'text',
                        time: 600
                    },
                    '2': {
                        type: 'select',
                        values: [{
                            value: true,
                            label: 'Active'
                        }, {
                            value: false,
                            label: 'Inactive'
                        }]
                    }
                });
            $rootScope.openDeleteModal = function() {
                var modalInstance = $uibModal.open({
                    backdrop: 'static',
                    animation: true,
                    templateUrl: '/views/operation.mail-management.remove.html',

                    resolve: {
                        MailLogs: function() {
                            return MailLogService.get({
                                id: $scope.selectedMailLogs
                            }).$promise;
                        }
                    },
                    controller: 'MailLogRemoveController'
                });
                modalInstance.result.then(function(messages) {
                    var success = 0,
                        error = 0;
                    for (var i in messages) {
                        if (messages[i].type == 'success') success++;
                        if (messages[i].type == 'error') error++;
                    }
                    messages = [];
                    if (success > 0) messages.push({
                        type: 'success',
                        header: 'Remove MailLog',
                        message: success + " record(s) removed successfully!"
                    });
                    if (error > 0) messages.push({
                        type: 'error',
                        header: 'Remove MailLog',
                        message: error + " record(s) could not be removed successfully!"
                    });
                    if (messages.length > 0) showMessages(messages);
                    $state.reload();
                });
            };
            $scope.openDeleteModal1 = function(recordId) {
                $scope.toggleAllRecords(false);
                $scope.selectedMailLogs = [recordId];
                $rootScope.openDeleteModal();
            };
        }
    ]);
