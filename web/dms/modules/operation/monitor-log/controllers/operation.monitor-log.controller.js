angular.module('operation.monitorLog')
    .controller('OperationMonitorLogAccessLogController', [
        '$rootScope',
        '$localStorage',
        '$scope',
        'DTOptionsBuilder',
        'AccessLogs',
        'Users',
        function($rootScope, $localStorage, $scope, DTOptionsBuilder, AccessLogs, Users) {
            $rootScope.page = {
                name: "Operations: Monitor Logs: Access Logs"
            };

            function getLabels(arrayOfObjects) {
                var labels = [];
                for (var i in arrayOfObjects) {
                    labels.push(arrayOfObjects[i].label);
                }
                return labels;
            }

            function getData(arrayOfObjects) {
                var data = [];
                for (var i in arrayOfObjects) {
                    data = data.concat(arrayOfObjects[i].data);
                }
                return data;
            }

            function getDataCount(arrayOfObjects) {
                var dataCount = [];
                for (var i in arrayOfObjects) {
                    dataCount.push(arrayOfObjects[i].data.length);
                }
                return dataCount;
            }

            function getAccessLogsForDate(date) {
                var accessLogs = {
                    label: date_format(date, 'M jS'),
                    data: []
                };
                for (var i = AccessLogs.length - 1; i >= 0; i--) {
                    var newDate = (new Date(AccessLogs[i].at));
                    var actualDate = new Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate());
                    if (actualDate.getTime() == date.getTime()) {
                        accessLogs.data.push(AccessLogs[i]);
                    }
                }
                return accessLogs;
            }

            function getAccessLogsForDays(days) {
                days = (days - 1);
                var accessLogs = [],
                    dates = [];
                for (var i = days; i >= 0; i--) {
                    var newDate = (i == days) ? new Date() : (new Date(dates[i + 1].valueOf() - (86400 * 1000)));
                    dates[i] = new Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate());
                    accessLogs.push(getAccessLogsForDate(dates[i]));
                }
                return accessLogs;
            }

            function getUsersForRoles() {
                var usersForRoles = {};
                for (var i = Users.length - 1; i >= 0; i--) {
                    var user = Users[i];
                    if (user.role && user.role._id) {
                        if (!usersForRoles[user.role._id]) usersForRoles[user.role._id] = {
                            label: user.role.name,
                            data: []
                        };
                        usersForRoles[user.role._id].data.push(user);
                    }
                }
                return usersForRoles;
            }

            var accessLogs = getAccessLogsForDays(5);
            $scope.lineLabels = getLabels(accessLogs);
            $scope.lineSeries = ['Portal Access'];
            $scope.lineData = [getDataCount(accessLogs)];

            $scope.accessLogs = getData(accessLogs);

            $.fn.dataTable.moment('dddd, Do MMM YYYY, h:m:s a');
            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withPaginationType('full_numbers')
                .withOption('order', [2, 'desc'])
                .withColumnFilter({
                    aoColumns: [{
                        type: 'text',
                        bRegex: true,
                        bSmart: true
                    }, {
                        type: 'select',
                        bRegex: true,
                        values: [
                            { value: '^LOGIN', label: 'LOGIN' },
                            { value: '^LOGOUT', label: 'LOGOUT' }
                        ]
                    }, null]
                });
        }
    ]);
