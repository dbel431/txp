angular.module('operation.monitorLog', [
        'operation'
    ])
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $stateProvider.state('portal.operation.monitorLog', {
                url: '/monitor-log',
                templateUrl: '/views/core.portal.action.tpl.html'
            }).state('portal.operation.monitorLog.accessLog', {
                url: '/access-log',
                templateUrl: '/views/operation.monitor-log.access-log.html',
                resolve: {
                    AccessLogs: [
                        'CoreLogAccessService',
                        function(CoreLogAccessService) {
                            return CoreLogAccessService.get().$promise;
                        }
                    ],
                    Users: [
                        'UserService',
                        function(UserService) {
                            return UserService.get({ active: true }).$promise;
                        }
                    ]
                },
                controller: 'OperationMonitorLogAccessLogController'
            });
        }
    ]);
