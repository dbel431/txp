// ------------------------------------------------------------------
// ----------------------------- CONFIG -----------------------------
(new Fingerprint2()).get(function(result) {
    localStorage.setItem('DMS-FINGERPRINT', result);
});

toastr.options = {
    "closeButton": true,
    "newestOnTop": true,
    "positionClass": "toast-bottom-right"
};

function showMessages(messages) {
    for (var i in messages) {
        var message = messages[i];
        toastr[message.type](message.message, message.header);
    }
}

function getObjectData(object) {
    if (typeof object != 'object') return object;
    var newObj = {};
    for (var i in object) {
        newObj[i] = object[i];
    }
    newObj = JSON.parse(angular.toJson(newObj));
    return newObj;
}

function arrayValues(array) {
    var ret = [];
    for (var i in array) {
        ret.push(array[i]);
    }
    return ret;
};

function arrayKeys(array) {
    var ret = [];
    for (var i in array) {
        ret.push(i);
    }
    return ret;
};

// Source: http://pixelscommander.com/en/javascript/javascript-file-download-ignore-content-type/
window.downloadFile = function(sUrl) {

    //iOS devices do not support downloading. We have to inform user about this.
    if (/(iP)/g.test(navigator.userAgent)) {
        //alert('Your device does not support files downloading. Please try again in desktop browser.');
        window.open(sUrl, '_blank');
        return false;
    }

    //If in Chrome or Safari - download via virtual link click
    if (window.downloadFile.isChrome || window.downloadFile.isSafari) {
        //Creating new link node.
        var link = document.createElement('a');
        link.href = sUrl;
        link.setAttribute('target', '_blank');

        if (link.download !== undefined) {
            //Set HTML5 download attribute. This will prevent file from opening if supported.
            var fileName = sUrl.substring(sUrl.lastIndexOf('/') + 1, sUrl.length);
            link.download = fileName;
        }

        //Dispatching click event.
        if (document.createEvent) {
            var e = document.createEvent('MouseEvents');
            e.initEvent('click', true, true);
            link.dispatchEvent(e);
            return true;
        }
    }

    // Force file download (whether supported by server).
    if (sUrl.indexOf('?') === -1) {
        sUrl += '?download';
    }

    window.open(sUrl, '_blank');
    return true;
}

window.downloadFile.isChrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
window.downloadFile.isSafari = navigator.userAgent.toLowerCase().indexOf('safari') > -1;

// ------------------------------------------------------------------
// -------------------------- Angular Appl --------------------------
angular.module('dms', [
    // core
    'dashboard',
    'profile',
    'mailbox',
    'quickLinks',
    // directory
    'directory.organization',
    'directory.dataDownload',
    'directory.report',
    'directory.recordHistory',
    // operation
    'operation.monitorLog',
    'operation.mailManagement',
    // security
    'security.role',
    'security.user',
    // notification
    'notification.event',
    'notification.template',
    // website
    'website.user',
    'website.datacardRequest',
    'website.advertiseInquiry',
    'website.commentFeedback',
    'website.nrpInquiry'
]);
