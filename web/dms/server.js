var config = require('../../config.js');
var express = require('express');
var app = express();

app.use('/admin-theme', express.static(__dirname + "/../bower_components/AdminLTE"));
app.use('/nrp-admin-theme', express.static(__dirname + "/nrp-admin-theme"));
app.use('/ckeditor', express.static(__dirname + "/../bower_components/ckeditor"));
app.use('/metis', express.static(__dirname + "/public/metis"));

app.use('/scripts', express.static(__dirname + "/public/scripts"));
app.use('/styles', express.static(__dirname + "/public/styles"));
app.use('/fonts', express.static(__dirname + "/public/fonts"));
app.use('/images', express.static(__dirname + "/public/images"));
app.use('/views', express.static(__dirname + "/public/views"));

app.use('/config', express.static(__dirname + "/../../config"));

app.get('/*', function(req, res) {
    res.sendFile(__dirname + '/index.html');
});

app.listen(process.env.WEB_PORT, function() {
    console.log('Listening..');
});
