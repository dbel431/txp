module.exports = function(grunt) {

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            dms: {
                files: {
                    'dms/public/scripts/apps.min.js': [
                        'dms/modules/**/*.app.js',
                        'dms/assets/js/app.js',
                        'dms/assets/js/!(app).js'
                    ],
                    'dms/public/scripts/services.min.js': ['dms/modules/**/*.service.js'],
                    'dms/public/scripts/directives.min.js': ['dms/modules/**/*.directive.js'],
                    'dms/public/scripts/filters.min.js': ['dms/modules/**/*.filter.js'],
                    'dms/public/scripts/controllers.min.js': ['dms/modules/**/*.controller.js']
                }
            },
            dmsDev: {
                files: {
                    'dms-dev/public/scripts/apps.min.js': [
                        'dms-dev/modules/**/*.app.js',
                        'dms-dev/assets/js/app.js',
                        'dms/assets/js/!(app).js'
                    ],
                    'dms-dev/public/scripts/services.min.js': ['dms-dev/modules/**/*.service.js'],
                    'dms-dev/public/scripts/directives.min.js': ['dms-dev/modules/**/*.directive.js'],
                    'dms-dev/public/scripts/filters.min.js': ['dms-dev/modules/**/*.filter.js'],
                    'dms-dev/public/scripts/controllers.min.js': ['dms-dev/modules/**/*.controller.js']
                }
            }
        },
        uglify: {
            dms: {
                files: {
                    'dms/public/scripts/vendor.min.js': [
                        'bower_components/jquery/dist/jquery.js',
                        'bower_components/fingerprintjs2/fingerprint2.js',
                        'bower_components/moment/moment.js',
                        'bower_components/date-format/dist/date-format.js',
                        'bower_components/bootstrap/dist/js/bootstrap.js',
                        'bower_components/metisMenu/dist/metisMenu.js',
                        'bower_components/datatables/media/js/jquery.dataTables.js',
                        'bower_components/datatables/media/js/dataTables.bootstrap.js',
                        'bower_components/angular/angular.js',
                        'bower_components/angular-animate/angular-animate.js',
                        'bower_components/angular-cookies/angular-cookies.js',
                        'bower_components/angular-touch/angular-touch.js',
                        'bower_components/angular-sanitize/angular-sanitize.js',
                        'bower_components/angular-resource/angular-resource.js',
                        'bower_components/angular-ui-router/release/angular-ui-router.js',
                        'bower_components/angular-messages/angular-messages.js',
                        'bower_components/ngstorage/ngStorage.js',
                        'bower_components/angular-bootstrap/ui-bootstrap.js',
                        'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
                        'bower_components/angular-ckeditor/angular-ckeditor.js',
                        'bower_components/oclazyload/dist/ocLazyLoad.js',
                        'bower_components/angular-toggle-switch/angular-toggle-switch.js',
                        'bower_components/ui-select/dist/select.js',
                        'bower_components/angular-datatables/dist/angular-datatables.js',
                        'bower_components/angular-datatables/dist/plugins/**/angular-datatables.!(min).js',
                        'bower_components/datatables-light-columnfilter/dist/dataTables.lightColumnFilter.js',
                        // 'bower_components/datatables-light-columnfilter/dist/dataTables.lcf.datepicker.fr.js',
                        'bower_components/datatables-light-columnfilter/dist/dataTables.lcf.bootstrap3.js',
                        'bower_components/json3/lib/json3.js',
                        'bower_components/angular-loading-bar/build/loading-bar.js',
                        'bower_components/Chart.js/Chart.js',
                        'bower_components/angular-chart.js/dist/angular-chart.js',
                        'bower_components/toastr/toastr.js',
                        'bower_components/file-saver/FileSaver.js',
                        'bower_components/json-export-excel/dest/json-export-excel.js',
                        'bower_components/ng-img-crop/compile/unminified/ng-img-crop.js'
                    ]
                }
            },
            dmsDev: {
                files: {
                    'dms-dev/public/scripts/vendor.min.js': [
                        'bower_components/jquery/dist/jquery.js',
                        'bower_components/fingerprintjs2/fingerprint2.js',
                        'bower_components/moment/moment.js',
                        'bower_components/date-format/dist/date-format.js',
                        'bower_components/bootstrap/dist/js/bootstrap.js',
                        'bower_components/metisMenu/dist/metisMenu.js',
                        'bower_components/datatables/media/js/jquery.dataTables.js',
                        'bower_components/datatables/media/js/dataTables.bootstrap.js',
                        'bower_components/angular/angular.js',
                        'bower_components/angular-animate/angular-animate.js',
                        'bower_components/angular-cookies/angular-cookies.js',
                        'bower_components/angular-touch/angular-touch.js',
                        'bower_components/angular-sanitize/angular-sanitize.js',
                        'bower_components/angular-resource/angular-resource.js',
                        'bower_components/angular-ui-router/release/angular-ui-router.js',
                        'bower_components/angular-messages/angular-messages.js',
                        'bower_components/ngstorage/ngStorage.js',
                        'bower_components/angular-bootstrap/ui-bootstrap.js',
                        'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
                        'bower_components/angular-ckeditor/angular-ckeditor.js',
                        'bower_components/oclazyload/dist/ocLazyLoad.js',
                        'bower_components/angular-toggle-switch/angular-toggle-switch.js',
                        'bower_components/ui-select/dist/select.js',
                        'bower_components/angular-datatables/dist/angular-datatables.js',
                        'bower_components/angular-datatables/dist/plugins/**/angular-datatables.!(min).js',
                        'bower_components/datatables-light-columnfilter/dist/dataTables.lightColumnFilter.js',
                        // 'bower_components/datatables-light-columnfilter/dist/dataTables.lcf.datepicker.fr.js',
                        'bower_components/datatables-light-columnfilter/dist/dataTables.lcf.bootstrap3.js',
                        'bower_components/json3/lib/json3.js',
                        'bower_components/angular-loading-bar/build/loading-bar.js',
                        'bower_components/Chart.js/Chart.js',
                        'bower_components/angular-chart.js/dist/angular-chart.js',
                        'bower_components/toastr/toastr.js',
                        'bower_components/file-saver/FileSaver.js',
                        'bower_components/json-export-excel/dest/json-export-excel.js',
                        'bower_components/ng-img-crop/compile/unminified/ng-img-crop.js'
                    ]
                }
            }
        },
        cssmin: {
            dmsVendor: {
                files: {
                    'dms/public/styles/vendor.min.css': [
                        'bower_components/bootstrap/dist/css/bootstrap.css',
                        'bower_components/font-awesome/css/font-awesome.css',
                        'bower_components/metisMenu/dist/metisMenu.css',
                        'bower_components/angular-loading-bar/build/loading-bar.css',
                        'bower_components/ui-select/dist/select.css',
                        'bower_components/datatables/media/css/dataTables.bootstrap.css',
                        'bower_components/angular-chart.js/dist/angular-chart.css',
                        'bower_components/angular-toggle-switch/angular-toggle-switch.css',
                        'bower_components/toastr/toastr.css',
                        'bower_components/ng-img-crop/compile/unminified/ng-img-crop.css'
                    ]
                }
            },
            dmsDevVendor: {
                files: {
                    'dms-dev/public/styles/vendor.min.css': [
                        'bower_components/bootstrap/dist/css/bootstrap.css',
                        'bower_components/font-awesome/css/font-awesome.css',
                        'bower_components/metisMenu/dist/metisMenu.css',
                        'bower_components/angular-loading-bar/build/loading-bar.css',
                        'bower_components/ui-select/dist/select.css',
                        'bower_components/datatables/media/css/dataTables.bootstrap.css',
                        'bower_components/angular-chart.js/dist/angular-chart.css',
                        'bower_components/angular-toggle-switch/angular-toggle-switch.css',
                        'bower_components/toastr/toastr.css',
                        'bower_components/ng-img-crop/compile/unminified/ng-img-crop.css'
                    ]
                }
            },
            dmsApp: {
                files: {
                    'dms/public/styles/app.min.css': ['dms/assets/css/**/*.css']
                }
            },
            dmsDevApp: {
                files: {
                    'dms-dev/public/styles/app.min.css': ['dms-dev/assets/css/**/*.css']
                }
            }
        },
        copy: {
            dmsFonts: {
                expand: true,
                src: 'bower_components/**/*.{eot,svg,ttf,woff,woff2}',
                dest: 'dms/public/fonts/',
                flatten: true
            },
            dmsDevFonts: {
                expand: true,
                src: 'bower_components/**/*.{eot,svg,ttf,woff,woff2}',
                dest: 'dms-dev/public/fonts/',
                flatten: true
            },
            dmsViews: {
                expand: true,
                src: 'dms/modules/**/views/**/*.html',
                dest: 'dms/public/views/',
                flatten: true
            },
            dmsDevViews: {
                expand: true,
                src: 'dms-dev/modules/**/views/**/*.html',
                dest: 'dms-dev/public/views/',
                flatten: true
            }
        },
        watch: {
            options: {
                livereload: true,
            },
            app: {
                files: [
                    'dms/assets/**/*.js',
                    'dms-dev/assets/**/*.js',
                    'dms/assets/**/*.css',
                    'dms-dev/assets/**/*.css',
                    'dms/modules/**/*.js',
                    'dms-dev/modules/**/*.js',
                    'dms/modules/**/*.css',
                    'dms-dev/modules/**/*.css',
                    'dms/modules/**/*.html',
                    'dms-dev/modules/**/*.html'
                ],
                tasks: [
                    'concat',
                    'cssmin:dmsApp',
                    'cssmin:dmsDevApp',
                    'copy:dmsViews',
                    'copy:dmsDevViews'
                ]
            }
        }
    });

    grunt.registerTask('default', ['concat', 'uglify', 'cssmin', 'copy', 'watch']);

}
