// ------------------------------------------------------------------
// ----------------------------- CONFIG -----------------------------
(new Fingerprint2()).get(function(result) {
    localStorage.setItem('TW-DMS-FINGERPRINT', result);
});

var API_PATH = 'http://' + window.location.hostname + ':4002';

toastr.options = {
    "closeButton": true,
    "newestOnTop": true,
    "positionClass": "toast-bottom-right"
};

function showMessages(messages) {
    for (var i in messages) {
        var message = messages[i];
        toastr[message.type](message.message, message.header);
    }
}
// ------------------------------------------------------------------
// -------------------------- Angular Appl --------------------------
angular.module('dmsDev', [
    'acl',
    'security',
    'policy',
    'master',
]);
