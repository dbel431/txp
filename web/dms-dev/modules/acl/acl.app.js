angular.module('acl', ['core'])
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $stateProvider.state('core.acl', {
                url: '/acl',
                templateUrl: '/views/acl.tpl.html',
                controller: 'ACLController'
            }).state('core.acl.module', {
                url: '/module',
                templateUrl: '/views/acl.module.tpl.html',
                controller: 'ACLModuleController'
            }).state('core.acl.module.list', {
                url: '/list',
                templateUrl: '/views/acl.module.list.html',
                resolve: {
                    ACLModules: [
                        'ACLModuleService',
                        function(ACLModuleService) {
                            return ACLModuleService.query().$promise;
                        }
                    ]
                },
                controller: 'ACLModuleListController'
            }).state('core.acl.module.add', {
                url: '/add',
                templateUrl: '/views/acl.module.edit.html',
                controller: 'ACLModuleAddController'
            }).state('core.acl.module.edit', {
                url: '/edit/:id',
                templateUrl: '/views/acl.module.edit.html',
                resolve: {
                    ACLModule: [
                        'ACLModuleService',
                        '$stateParams',
                        function(ACLModuleService, $stateParams) {
                            if ($stateParams.id)
                                return ACLModuleService.get({
                                    id: $stateParams.id
                                }).$promise;
                            return true;
                        }
                    ]
                },
                controller: 'ACLModuleEditController'
            }).state('core.acl.page', {
                url: '/page',
                templateUrl: '/views/acl.page.tpl.html',
                controller: 'ACLPageController'
            }).state('core.acl.page.list', {
                url: '/list',
                templateUrl: '/views/acl.page.list.html',
                resolve: {
                    ACLPages: [
                        'ACLPageService',
                        function(ACLPageService) {
                            return ACLPageService.get().$promise;
                        }
                    ]
                },
                controller: 'ACLPageListController'
            }).state('core.acl.page.add', {
                url: '/add',
                templateUrl: '/views/acl.page.edit.html',
                controller: 'ACLPageAddController',
                resolve: {
                    ACLModules: [
                        'ACLModuleService',
                        function(ACLModuleService) {
                            return ACLModuleService.get({ active: true }).$promise;
                        }
                    ]
                }
            }).state('core.acl.page.edit', {
                url: '/edit/:id',
                templateUrl: '/views/acl.page.edit.html',
                resolve: {
                    ACLModules: [
                        'ACLModuleService',
                        function(ACLModuleService) {
                            return ACLModuleService.get({ active: true }).$promise;
                        }
                    ],
                    ACLPage: [
                        'ACLPageService',
                        '$stateParams',
                        function(ACLPageService, $stateParams) {
                            if ($stateParams.id)
                                return ACLPageService.get({
                                    id: $stateParams.id
                                }).$promise;
                            return true;
                        }
                    ]
                },
                controller: 'ACLPageEditController'
            }).state('core.acl.action', {
                url: '/action',
                templateUrl: '/views/acl.action.tpl.html',
                controller: 'ACLActionController',
                resolve: {
                    ACLModules: [
                        'ACLModuleService',
                        function(ACLModuleService) {
                            return ACLModuleService.get({ active: true }).$promise;
                        }
                    ],
                    ACLPages: [
                        'ACLPageService',
                        function(ACLPageService) {
                            return ACLPageService.get({ active: true }).$promise;
                        }
                    ],
                }
            }).state('core.acl.action.list', {
                url: '/list',
                templateUrl: '/views/acl.action.list.html',
                resolve: {
                    ACLActions: [
                        'ACLActionService',
                        function(ACLActionService) {
                            return ACLActionService.get().$promise;
                        }
                    ]
                },
                controller: 'ACLActionListController'
            }).state('core.acl.action.add', {
                url: '/add',
                templateUrl: '/views/acl.action.edit.html',
                controller: 'ACLActionAddController'
            }).state('core.acl.action.edit', {
                url: '/edit/:id',
                templateUrl: '/views/acl.action.edit.html',
                resolve: {
                    ACLModules: [
                        'ACLModuleService',
                        function(ACLModuleService) {
                            return ACLModuleService.get({ active: true }).$promise;
                        }
                    ],
                    ACLPages: [
                        'ACLPageService',
                        function(ACLPageService) {
                            return ACLPageService.get({ active: true }).$promise;
                        }
                    ],
                    ACLAction: [
                        'ACLActionService',
                        '$stateParams',
                        function(ACLActionService, $stateParams) {
                            if ($stateParams.id)
                                return ACLActionService.get({
                                    id: $stateParams.id
                                }).$promise;
                            return true;
                        }
                    ]
                },
                controller: 'ACLActionEditController'
            });
        }
    ]);
