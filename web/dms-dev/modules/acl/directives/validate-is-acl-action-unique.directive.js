angular.module('acl')
    .directive('validateIsAclActionUnique', [
        'ACLActionService',
        function(ACLActionService) {
            return {
                require: 'ngModel',
                restrict: 'A',
                link: function(scope, iElm, iAttrs, ngModel) {
                    ngModel.$validators.validateIsAclActionUnique = function(modelValue) {
                        if (modelValue == '' || modelValue == undefined) return true;
                        var aclAction = new ACLActionService();
                        return ACLActionService.get({
                            'object': modelValue,
                            'page': iAttrs.aclPageId
                        }, function(aclActions) {
                            if (aclActions.length == 1 && iAttrs.exceptAclActionId) {
                                ngModel.$setValidity('validateIsAclActionUnique', (aclActions[0]._id == iAttrs.exceptAclActionId));
                            } else {
                                ngModel.$setValidity('validateIsAclActionUnique', (aclActions.length <= 0));
                            }
                        }, function(err) {
                            ngModel.$setValidity('validateIsAclActionUnique', false);
                        });
                    };
                }
            };
        }
    ]);
