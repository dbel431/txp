angular.module('acl')
    .directive('validateIsAclModuleUnique', [
        'ACLModuleService',
        function(ACLModuleService) {
            return {
                require: 'ngModel',
                restrict: 'A',
                link: function(scope, iElm, iAttrs, ngModel) {
                    ngModel.$validators.validateIsAclModuleUnique = function(modelValue) {
                        if (modelValue == '' || modelValue == undefined) return true;
                        var aclModule = new ACLModuleService();
                        return ACLModuleService.get({
                            'object': modelValue
                        }, function(aclModules) {
                            if (aclModules.length == 1 && iAttrs.exceptAclModuleId) {
                                ngModel.$setValidity('validateIsAclModuleUnique', (aclModules[0]._id == iAttrs.exceptAclModuleId));
                            } else {
                                ngModel.$setValidity('validateIsAclModuleUnique', (aclModules.length <= 0));
                            }
                        }, function(err) {
                            ngModel.$setValidity('validateIsAclModuleUnique', false);
                        });
                    };
                }
            };
        }
    ]);
