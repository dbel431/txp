angular.module('acl')
    .directive('validateIsAclPageUnique', [
        'ACLPageService',
        function(ACLPageService) {
            return {
                require: 'ngModel',
                restrict: 'A',
                link: function(scope, iElm, iAttrs, ngModel) {
                    ngModel.$validators.validateIsAclPageUnique = function(modelValue) {
                        if (modelValue == '' || modelValue == undefined) return true;
                        var aclPage = new ACLPageService();
                        return ACLPageService.get({
                            'object': modelValue,
                            'module': iAttrs.aclModuleId
                        }, function(aclPages) {
                            if (aclPages.length == 1 && iAttrs.exceptAclPageId) {
                                ngModel.$setValidity('validateIsAclPageUnique', (aclPages[0]._id == iAttrs.exceptAclPageId));
                            } else {
                                ngModel.$setValidity('validateIsAclPageUnique', (aclPages.length <= 0));
                            }
                        }, function(err) {
                            ngModel.$setValidity('validateIsAclPageUnique', false);
                        });
                    };
                }
            };
        }
    ]);
