angular.module('acl')
    .controller('ACLActionController', [
        '$scope',
        '$state',
        function($scope, $state) {}
    ])
    .controller('ACLActionListController', [
        '$scope',
        'DTOptionsBuilder',
        'ACLActions',
        function($scope, DTOptionsBuilder, ACLActions) {
            $scope.aclActions = [];
            for (var i = ACLActions.length - 1; i >= 0; i--) {
                if (ACLActions[i].module && ACLActions[i].module.active && ACLActions[i].page && ACLActions[i].page.active) $scope.aclActions.push(ACLActions[i]);
            }
            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withPaginationType('full_numbers')
                .withColumnFilter({
                    aoColumns: [{
                        type: 'text',
                        bRegex: true,
                        bSmart: true
                    }, {
                        type: 'text',
                        bRegex: true,
                        bSmart: true
                    }, {
                        type: 'text',
                        bRegex: true,
                        bSmart: true
                    }, {
                        type: 'select',
                        bRegex: true,
                        values: [
                            { value: '^Active', label: 'Active' },
                            { value: '^Inactive', label: 'Inactive' }
                        ]
                    }]
                });
        }
    ])
    .controller('ACLActionAddController', [
        '$scope',
        '$state',
        '$stateParams',
        'ACLActionService',
        'ACLModules',
        'ACLPages',
        function($scope, $state, $stateParams, ACLActionService, ACLModules, ACLPages) {
            $scope.action = new ACLActionService();
            $scope.moduleArray = ACLModules;
            $scope.selectedModule = {
                value: $scope.moduleArray[0]
            };
            $scope.moduleChanged = function(toModule) {
                $scope.pageArray = [];
                for (var i = ACLPages.length - 1; i >= 0; i--) {
                    if (ACLPages[i].module && ACLPages[i].module._id == toModule._id) {
                        $scope.pageArray.push(ACLPages[i]);
                    }
                }
                $scope.selectedPage = {
                    value: $scope.pageArray[0]
                };
            };
            $scope.moduleChanged($scope.selectedModule.value);
            $scope.save = function() {
                $scope.action.module = $scope.selectedModule.value._id;
                $scope.action.page = $scope.selectedPage.value._id;
                $scope.action.$save(function(action) {
                    showMessages([{
                        type: 'success',
                        message: 'Action Added successfully!',
                        header: 'Add Action'
                    }]);
                    $state.go('core.acl.action.list');
                }, function(err) {
                    showMessages([{
                        type: 'error',
                        message: err.message,
                        header: 'Add Action'
                    }]);
                });
            };
        }
    ])
    .controller('ACLActionEditController', [
        '$scope',
        '$state',
        '$stateParams',
        'ACLModules',
        'ACLPages',
        'ACLAction',
        function($scope, $state, $stateParams, ACLModules, ACLPages, ACLAction) {
            $scope.action = ACLAction[0];
            $scope.moduleArray = ACLModules;
            $scope.selectedModule = {
                value: $scope.action.module
            };
            $scope.moduleChanged = function(toModule) {
                $scope.pageArray = [];
                for (var i = ACLPages.length - 1; i >= 0; i--) {
                    if (ACLPages[i].module && ACLPages[i].module._id == toModule._id) {
                        $scope.pageArray.push(ACLPages[i]);
                    }
                }
                $scope.selectedPage = {
                    value: $scope.pageArray[0]
                };
            };
            $scope.moduleChanged($scope.selectedModule.value);
            $scope.save = function() {
                $scope.action.module = $scope.selectedModule.value._id;
                $scope.action.page = $scope.selectedPage.value._id;
                $scope.action.$update({
                    id: $stateParams.id
                }, function(action) {
                    showMessages([{
                        type: 'success',
                        message: 'Action Updated successfully!',
                        header: 'Edit Action'
                    }]);
                    $state.go('core.acl.action.list');
                }, function(err) {
                    showMessages([{
                        type: 'error',
                        message: err.message,
                        header: 'Edit Action'
                    }]);
                });
            };
        }
    ]);
