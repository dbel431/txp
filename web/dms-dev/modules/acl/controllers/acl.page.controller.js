angular.module('acl')
    .controller('ACLPageController', [
        '$scope',
        '$state',
        function($scope, $state) {}
    ])
    .controller('ACLPageListController', [
        '$scope',
        'DTOptionsBuilder',
        'ACLPages',
        function($scope, DTOptionsBuilder, ACLPages) {
            $scope.aclPages = [];
            for (var i = ACLPages.length - 1; i >= 0; i--) {
                if (ACLPages[i].module && ACLPages[i].module.active) $scope.aclPages.push(ACLPages[i]);
            }
            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withPaginationType('full_numbers')
                .withColumnFilter({
                    aoColumns: [{
                        type: 'text',
                        bRegex: true,
                        bSmart: true
                    },{
                        type: 'text',
                        bRegex: true,
                        bSmart: true
                    }, {
                        type: 'select',
                        bRegex: true,
                        values: [
                            { value: '^Active', label: 'Active' }, 
                            { value: '^Inactive', label: 'Inactive' }
                        ]
                    }]
                });
        }
    ])
    .controller('ACLPageAddController', [
        '$scope',
        '$state',
        '$stateParams',
        'ACLPageService',
        'ACLModules',
        function($scope, $state, $stateParams, ACLPageService, ACLModules) {
            $scope.moduleArray = ACLModules;
            $scope.page = new ACLPageService();
            $scope.selectedModule = {
                value: $scope.page.module || $scope.moduleArray[0]
            };
            $scope.save = function() {
                $scope.page.module = $scope.selectedModule.value._id;
                $scope.page.$save(function(page) {
                    showMessages([{
                        type: 'success',
                        message: 'Page Added successfully!',
                        header: 'Add Page'
                    }]);
                    $state.go('core.acl.page.list');
                }, function(err) {
                    showMessages([{
                        type: 'error',
                        message: err.message,
                        header: 'Add Page'
                    }]);
                });
            };
        }
    ])
    .controller('ACLPageEditController', [
        '$scope',
        '$state',
        '$stateParams',
        'ACLModules',
        'ACLPage',
        function($scope, $state, $stateParams, ACLModules, ACLPage) {
            $scope.moduleArray = ACLModules;
            $scope.page = ACLPage[0];
            $scope.selectedModule = {
                value: $scope.page.module
            };
            $scope.save = function() {
                $scope.page.module = $scope.selectedModule.value._id;
                $scope.page.$update({
                    id: $stateParams.id
                }, function(page) {
                    showMessages([{
                        type: 'success',
                        message: 'Page Updated successfully!',
                        header: 'Edit Page'
                    }]);
                    $state.go('core.acl.page.list');
                }, function(err) {
                    showMessages([{
                        type: 'error',
                        message: err.message,
                        header: 'Edit Page'
                    }]);
                });
            };
        }
    ]);
