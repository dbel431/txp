angular.module('acl')
    .controller('ACLModuleController', [
        '$scope',
        '$state',
        function($scope, $state) {}
    ])
    .controller('ACLModuleListController', [
        '$scope',
        'DTOptionsBuilder',
        'ACLModules',
        function($scope, DTOptionsBuilder, ACLModules) {
            $scope.aclModules = ACLModules;
            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withPaginationType('full_numbers')
                .withColumnFilter({
                    aoColumns: [{
                        type: 'text',
                        bRegex: true,
                        bSmart: true
                    }, {
                        type: 'select',
                        bRegex: true,
                        values: [
                            { value: '^Active', label: 'Active' }, 
                            { value: '^Inactive', label: 'Inactive' }
                        ]
                    }]
                });
        }
    ])
    .controller('ACLModuleAddController', [
        '$scope',
        '$state',
        '$stateParams',
        'ACLModuleService',
        function($scope, $state, $stateParams, ACLModuleService) {
            $scope.module = new ACLModuleService();
            $scope.save = function() {
                $scope.module.$save(function(module) {
                    showMessages([{
                        type: 'success',
                        message: 'Module Added successfully!',
                        header: 'Add Module'
                    }]);
                    $state.go('core.acl.module.list');
                }, function(err) {
                    showMessages([{
                        type: 'error',
                        message: err.message,
                        header: 'Add Module'
                    }]);
                });
            };
        }
    ])
    .controller('ACLModuleEditController', [
        '$scope',
        '$state',
        '$stateParams',
        'ACLModule',
        function($scope, $state, $stateParams, ACLModule) {
            $scope.module = ACLModule[0];
            $scope.save = function() {
                $scope.module.$update({
                    id: $stateParams.id
                }, function(module) {
                    showMessages([{
                        type: 'success',
                        message: 'Module Updated successfully!',
                        header: 'Edit Module'
                    }]);
                    $state.go('core.acl.module.list');
                }, function(err) {
                    showMessages([{
                        type: 'error',
                        message: err.message,
                        header: 'Edit Module'
                    }]);
                });
            };
        }
    ]);
