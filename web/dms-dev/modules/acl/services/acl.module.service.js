angular.module('acl')
    .service('ACLModuleService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/acl/module', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                },
                update: {
                    method: 'PUT'
                }
            });
        }
    ])
