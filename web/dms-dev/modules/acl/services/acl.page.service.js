angular.module('acl')
    .service('ACLPageService', [
        '$resource',
        function($resource) {
            return $resource(API_PATH + '/acl/page', {}, {
                get: {
                    method: 'GET',
                    isArray: true
                },
                update: {
                    method: 'PUT'
                }
            });
        }
    ])
