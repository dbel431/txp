angular.module('policy')
    .controller('CorePolicySecurityQuestionController', [
        '$rootScope',
        '$scope',
        '$state',
        function($rootScope, $scope, $state) {
            $scope.loading = false;
        }
    ])
    .controller('CorePolicySecurityQuestionListController', [
        '$rootScope',
        '$scope',
        '$state',
        'DTOptionsBuilder',
        'SecurityQuestions',
        function($rootScope, $scope, $state, DTOptionsBuilder, SecurityQuestions) {
            $scope.securityQuestions = SecurityQuestions;
            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withPaginationType('full_numbers')
                .withColumnFilter({
                    aoColumns: [{
                        type: 'text',
                        bRegex: true,
                        bSmart: true
                    }, {
                        type: 'select',
                        bRegex: true,
                        values: [
                            { value: '^Active', label: 'Active' },
                            { value: '^Inactive', label: 'Inactive' }
                        ]
                    }]
                });
        }
    ])
    .controller('CorePolicySecurityQuestionAddController', [
        '$rootScope',
        '$scope',
        '$state',
        'CorePolicySecurityQuestionService',
        function($rootScope, $scope, $state, CorePolicySecurityQuestionService) {
            $scope.securityQuestion = new CorePolicySecurityQuestionService();
            $scope.add = function() {
                $scope.loading = true;
                $scope.securityQuestion.$save(function(securityQuestion) {
                    $scope.loading = false;
                    showMessages([{
                        type: 'success',
                        message: 'Security Question saved successfully!',
                        header: 'Security Question'
                    }]);
                    $state.go('core.policy.securityQuestion.list');
                }, function(err) {
                    $scope.loading = false;
                    showMessages([{
                        type: 'error',
                        message: 'Could not save Security Question!',
                        header: 'Security Question'
                    }]);
                });
            };
        }
    ])
    .controller('CorePolicySecurityQuestionEditController', [
        '$rootScope',
        '$scope',
        '$state',
        'SecurityQuestion',
        function($rootScope, $scope, $state, SecurityQuestion) {
            $scope.securityQuestion = SecurityQuestion[0];
            $scope.update = function() {
                $scope.loading = true;
                $scope.securityQuestion.$update({
                    id: $scope.securityQuestion._id
                }, function(securityQuestion) {
                    $scope.loading = false;
                    showMessages([{
                        type: 'success',
                        message: 'Security Question saved successfully!',
                        header: 'Security Question'
                    }]);
                    $state.go('core.policy.securityQuestion.list');
                }, function(err) {
                    $scope.loading = false;
                    showMessages([{
                        type: 'error',
                        message: 'Could not save Security Question!',
                        header: 'Security Question'
                    }]);
                });
            };
        }
    ])
    .controller('CorePolicySecurityQuestionRemoveController', [
        '$rootScope',
        '$scope',
        '$state',
        'SecurityQuestion',
        function($rootScope, $scope, $state, SecurityQuestion) {
            $scope.securityQuestion = SecurityQuestion[0];
            $scope.remove = function() {
                $scope.loading = true;
                $scope.securityQuestion.$remove({
                    id: $scope.securityQuestion._id
                }, function(securityQuestion) {
                    $scope.loading = false;
                    showMessages([{
                        type: 'success',
                        message: 'Security Question removed successfully!',
                        header: 'Security Question'
                    }]);
                    $state.go('core.policy.securityQuestion.list');
                }, function(err) {
                    $scope.loading = false;
                    showMessages([{
                        type: 'error',
                        message: 'Could not remove Security Question!',
                        header: 'Security Question'
                    }]);
                });
            };
        }
    ]);
