angular.module('policy').directive('validateIsSecurityQuestionUnique', [
    'CorePolicySecurityQuestionService',
    function(CorePolicySecurityQuestionService) {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function(scope, iElm, iAttrs, ngModel) {
                ngModel.$validators.validateIsSecurityQuestionUnique = function(modelValue) {
                    if (modelValue == '' || modelValue == undefined) return true;
                    var securityQuestion = new CorePolicySecurityQuestionService();
                    return CorePolicySecurityQuestionService.get({
                        'question': modelValue
                    }, function(securityQuestion) {
                        if (securityQuestion.length == 1 && iAttrs.exceptSecurityQuestionId) {
                            ngModel.$setValidity('validateIsSecurityQuestionUnique', (securityQuestion[0]._id == iAttrs.exceptSecurityQuestionId));
                        } else {
                            ngModel.$setValidity('validateIsSecurityQuestionUnique', (securityQuestion.length <= 0));
                        }
                    }, function(err) {
                        ngModel.$setValidity('validateIsSecurityQuestionUnique', false);
                    });
                };
            }
        };
    }
]);
