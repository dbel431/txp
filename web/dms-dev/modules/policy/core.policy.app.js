angular.module('policy', ['core'])
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $stateProvider
                .state('core.policy', {
                    url: '/policy',
                    templateUrl: '/views/policy.tpl.html'
                })
                .state('core.policy.securityQuestion', {
                    url: '/security-question',
                    templateUrl: '/views/policy.security-question.tpl.html',
                    controller: 'CorePolicySecurityQuestionController'
                })
                .state('core.policy.securityQuestion.list', {
                    url: '/list',
                    templateUrl: '/views/policy.security-question.list.html',
                    resolve: {
                        SecurityQuestions: [
                            'CorePolicySecurityQuestionService',
                            function(CorePolicySecurityQuestionService) {
                                return CorePolicySecurityQuestionService.get().$promise;
                            }
                        ]
                    },
                    controller: 'CorePolicySecurityQuestionListController'
                })
                .state('core.policy.securityQuestion.add', {
                    url: '/add',
                    templateUrl: '/views/policy.security-question.add.html',
                    controller: 'CorePolicySecurityQuestionAddController'
                })
                .state('core.policy.securityQuestion.edit', {
                    url: '/edit/:id',
                    templateUrl: '/views/policy.security-question.edit.html',
                    resolve: {
                        SecurityQuestion: [
                            '$stateParams',
                            'CorePolicySecurityQuestionService',
                            function($stateParams, CorePolicySecurityQuestionService) {
                                return CorePolicySecurityQuestionService.get({ id: $stateParams.id }).$promise;
                            }
                        ]
                    },
                    controller: 'CorePolicySecurityQuestionEditController'
                })
                .state('core.policy.securityQuestion.remove', {
                    url: '/remove/:id',
                    templateUrl: '/views/policy.security-question.remove.html',
                    resolve: {
                        SecurityQuestion: [
                            '$stateParams',
                            'CorePolicySecurityQuestionService',
                            function($stateParams, CorePolicySecurityQuestionService) {
                                return CorePolicySecurityQuestionService.get({ id: $stateParams.id }).$promise;
                            }
                        ]
                    },
                    controller: 'CorePolicySecurityQuestionRemoveController'
                });
        }
    ]);
