angular.module('policy').service('CorePolicySecurityQuestionService', [
    '$resource',
    function($resource) {
        return $resource(API_PATH + '/security-question', {}, {
            get: {
                method: 'GET',
                isArray: true
            },
            update: {
                method: 'PUT'
            },
            remove: {
                method: 'PUT',
                url: API_PATH + '/security-question/delete'
            }
        });
    }
]);
