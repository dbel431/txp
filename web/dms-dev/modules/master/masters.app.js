angular.module('master', ['core'])
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $stateProvider
                .state('core.master', {
                    url: '/master',
                    templateUrl: '/views/master.tpl.html'
                })
                .state('core.master.refCodeType', {
                    url: '/ref-code-type',
                    templateUrl: '/views/master.ref-code-type.tpl.html'
                })
                .state('core.master.refCodeType.list', {
                    url: '/list',
                    templateUrl: '/views/master.ref-code-type.list.html',
                    resolve: {
                        RefCodeTypes: [
                            'CoreMasterRefCodeTypeService',
                            function(CoreMasterRefCodeTypeService) {
                                return CoreMasterRefCodeTypeService.get().$promise;
                            }
                        ]
                    },
                    controller: 'CoreMasterRefCodeTypeListController'
                })
                .state('core.master.refCodeType.add', {
                    url: '/add',
                    templateUrl: '/views/master.ref-code-type.add.html',
                    controller: 'CoreMasterRefCodeTypeAddController'
                })
                .state('core.master.refCodeType.edit', {
                    url: '/edit/:id',
                    templateUrl: '/views/master.ref-code-type.edit.html',
                    resolve: {
                        RefCodeType: [
                            '$stateParams',
                            'CoreMasterRefCodeTypeService',
                            function($stateParams, CoreMasterRefCodeTypeService) {
                                return CoreMasterRefCodeTypeService.get({ id: $stateParams.id }).$promise;
                            }
                        ]
                    },
                    controller: 'CoreMasterRefCodeTypeEditController'
                })
                .state('core.master.refCodeType.remove', {
                    url: '/remove/:id',
                    templateUrl: '/views/master.ref-code-type.remove.html',
                    resolve: {
                        RefCodeType: [
                            '$stateParams',
                            'CoreMasterRefCodeTypeService',
                            function($stateParams, CoreMasterRefCodeTypeService) {
                                return CoreMasterRefCodeTypeService.get({ id: $stateParams.id }).$promise;
                            }
                        ]
                    },
                    controller: 'CoreMasterRefCodeTypeRemoveController'
                })
                .state('core.master.refCodeValue', {
                    url: '/ref-code-value',
                    templateUrl: '/views/master.ref-code-value.tpl.html'
                })
                .state('core.master.refCodeValue.list', {
                    url: '/list',
                    templateUrl: '/views/master.ref-code-value.list.html',
                    resolve: {
                        RefCodeValues: [
                            'CoreMasterRefCodeValueService',
                            function(CoreMasterRefCodeValueService) {
                                return CoreMasterRefCodeValueService.get().$promise;
                            }
                        ]
                    },
                    controller: 'CoreMasterRefCodeValueListController'
                })
                .state('core.master.refCodeValue.add', {
                    url: '/add',
                    templateUrl: '/views/master.ref-code-value.add.html',
                    resolve: {
                        RefCodeTypes: [
                            'CoreMasterRefCodeTypeService',
                            function(CoreMasterRefCodeTypeService) {
                                return CoreMasterRefCodeTypeService.get({ active: true }).$promise;
                            }
                        ],
                        RefCodeValues: [
                            'CoreMasterRefCodeValueService',
                            function(CoreMasterRefCodeValueService) {
                                return CoreMasterRefCodeValueService.get({ active: true }).$promise;
                            }
                        ]
                    },
                    controller: 'CoreMasterRefCodeValueAddController'
                })
                .state('core.master.refCodeValue.edit', {
                    url: '/edit/:id',
                    templateUrl: '/views/master.ref-code-value.edit.html',
                    resolve: {
                        RefCodeTypes: [
                            'CoreMasterRefCodeTypeService',
                            function(CoreMasterRefCodeTypeService) {
                                return CoreMasterRefCodeTypeService.get({ active: true }).$promise;
                            }
                        ],
                        RefCodeValues: [
                            'CoreMasterRefCodeValueService',
                            function(CoreMasterRefCodeValueService) {
                                return CoreMasterRefCodeValueService.get({ active: true }).$promise;
                            }
                        ],
                        RefCodeValue: [
                            '$stateParams',
                            'CoreMasterRefCodeValueService',
                            function($stateParams, CoreMasterRefCodeValueService) {
                                return CoreMasterRefCodeValueService.get({ id: $stateParams.id }).$promise;
                            }
                        ]
                    },
                    controller: 'CoreMasterRefCodeValueEditController'
                })
                .state('core.master.refCodeValue.remove', {
                    url: '/remove/:id',
                    templateUrl: '/views/master.ref-code-value.remove.html',
                    resolve: {
                        RefCodeValue: [
                            '$stateParams',
                            'CoreMasterRefCodeValueService',
                            function($stateParams, CoreMasterRefCodeValueService) {
                                return CoreMasterRefCodeValueService.get({ id: $stateParams.id }).$promise;
                            }
                        ]
                    },
                    controller: 'CoreMasterRefCodeValueRemoveController'
                });
        }
    ]);
