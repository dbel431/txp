angular.module('master').service('CoreMasterRefCodeValueService', [
    '$resource',
    function($resource) {
        return $resource(API_PATH + '/ref-code/value', {}, {
            get: {
                method: 'GET',
                isArray: true
            },
            update: {
                method: 'PUT'
            },
            remove: {
                method: 'PUT',
                url: API_PATH + '/ref-code/value/delete'
            }
        });
    }
]);