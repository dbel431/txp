angular.module('master').service('CoreMasterRefCodeTypeService', [
    '$resource',
    function($resource) {
        return $resource(API_PATH + '/ref-code/type', {}, {
            get: {
                method: 'GET',
                isArray: true
            },
            update: {
                method: 'PUT'
            },
            remove: {
                method: 'PUT',
                url: API_PATH + '/ref-code/type/delete'
            }
        });
    }
]);
