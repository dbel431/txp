angular.module('master')
    .controller('CoreMasterRefCodeValueListController', [
        '$rootScope',
        '$scope',
        '$state',
        'DTOptionsBuilder',
        'RefCodeValues',
        function($rootScope, $scope, $state, DTOptionsBuilder, RefCodeValues) {
            $scope.refCodeValues = RefCodeValues;
            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withPaginationType('full_numbers')
                .withColumnFilter({
                    aoColumns: [{
                        type: 'text',
                        bRegex: true,
                        bSmart: true
                    }, {
                        type: 'text',
                        bRegex: true,
                        bSmart: true
                    }, {
                        type: 'select',
                        bRegex: true,
                        values: [
                            { value: '^Active', label: 'Active' },
                            { value: '^Inactive', label: 'Inactive' }
                        ]
                    }]
                });
        }
    ])
    .controller('CoreMasterRefCodeValueAddController', [
        '$rootScope',
        '$scope',
        '$state',
        'CoreMasterRefCodeValueService',
        'RefCodeTypes',
        'RefCodeValues',
        function($rootScope, $scope, $state, CoreMasterRefCodeValueService, RefCodeTypes, RefCodeValues) {
            $scope.refCodeValue = new CoreMasterRefCodeValueService();
            $scope.refCodeTypeArray = RefCodeTypes;
            $scope.parentArray = RefCodeValues;
            $scope.add = function() {
                $scope.loading = true;
                $scope.refCodeValue.$save(function(refCodeValue) {
                    $scope.loading = false;
                    showMessages([{
                        type: 'success',
                        message: 'Code Value saved successfully!',
                        header: 'Code Value'
                    }]);
                    $state.go('core.master.refCodeValue.list');
                }, function(err) {
                    $scope.loading = false;
                    showMessages([{
                        type: 'error',
                        message: 'Could not save Code Value!',
                        header: 'Code Value'
                    }]);
                });
            };
        }
    ])
    .controller('CoreMasterRefCodeValueEditController', [
        '$rootScope',
        '$scope',
        '$state',
        'RefCodeValue',
        'RefCodeTypes',
        'RefCodeValues',
        function($rootScope, $scope, $state, RefCodeValue, RefCodeTypes, RefCodeValues) {
            $scope.refCodeValue = RefCodeValue[0];
            $scope.refCodeTypeArray = RefCodeTypes;
            $scope.parentArray = RefCodeValues;
            $scope.update = function() {
                $scope.loading = true;
                $scope.refCodeValue.$update({
                    id: $scope.refCodeValue._id
                }, function(refCodeValue) {
                    $scope.loading = false;
                    showMessages([{
                        type: 'success',
                        message: 'Code Value saved successfully!',
                        header: 'Code Value'
                    }]);
                    $state.go('core.master.refCodeValue.list');
                }, function(err) {
                    $scope.loading = false;
                    showMessages([{
                        type: 'error',
                        message: 'Could not save Code Value!',
                        header: 'Code Value'
                    }]);
                });
            };
        }
    ])
    .controller('CoreMasterRefCodeValueRemoveController', [
        '$rootScope',
        '$scope',
        '$state',
        'RefCodeValue',
        function($rootScope, $scope, $state, RefCodeValue) {
            $scope.refCodeValue = RefCodeValue[0];
            $scope.remove = function() {
                $scope.loading = true;
                $scope.refCodeValue.$remove({
                    id: $scope.refCodeValue._id
                }, function(refCodeValue) {
                    $scope.loading = false;
                    showMessages([{
                        type: 'success',
                        message: 'Code Value removed successfully!',
                        header: 'Code Value'
                    }]);
                    $state.go('core.master.refCodeValue.list');
                }, function(err) {
                    $scope.loading = false;
                    showMessages([{
                        type: 'error',
                        message: 'Could not remove Code Value!',
                        header: 'Code Value'
                    }]);
                });
            };
        }
    ]);
