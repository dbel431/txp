angular.module('master')
    .controller('CoreMasterRefCodeTypeListController', [
        '$rootScope',
        '$scope',
        '$state',
        'DTOptionsBuilder',
        'RefCodeTypes',
        function($rootScope, $scope, $state, DTOptionsBuilder, RefCodeTypes) {
            $scope.refCodeTypes = RefCodeTypes;
            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withPaginationType('full_numbers')
                .withColumnFilter({
                    aoColumns: [{
                        type: 'text',
                        bRegex: true,
                        bSmart: true
                    }, {
                        type: 'select',
                        bRegex: true,
                        values: [
                            { value: '^Active', label: 'Active' },
                            { value: '^Inactive', label: 'Inactive' }
                        ]
                    }]
                });
        }
    ])
    .controller('CoreMasterRefCodeTypeAddController', [
        '$rootScope',
        '$scope',
        '$state',
        'CoreMasterRefCodeTypeService',
        function($rootScope, $scope, $state, CoreMasterRefCodeTypeService) {
            $scope.refCodeType = new CoreMasterRefCodeTypeService();
            $scope.add = function() {
                $scope.loading = true;
                $scope.refCodeType.$save(function(refCodeType) {
                    $scope.loading = false;
                    showMessages([{
                        type: 'success',
                        message: 'Code Type saved successfully!',
                        header: 'Code Type'
                    }]);
                    $state.go('core.master.refCodeType.list');
                }, function(err) {
                    $scope.loading = false;
                    showMessages([{
                        type: 'error',
                        message: 'Could not save Code Type!',
                        header: 'Code Type'
                    }]);
                });
            };
        }
    ])
    .controller('CoreMasterRefCodeTypeEditController', [
        '$rootScope',
        '$scope',
        '$state',
        'RefCodeType',
        function($rootScope, $scope, $state, RefCodeType) {
            $scope.refCodeType = RefCodeType[0];
            $scope.update = function() {
                $scope.loading = true;
                $scope.refCodeType.$update({
                    id: $scope.refCodeType._id
                }, function(refCodeType) {
                    $scope.loading = false;
                    showMessages([{
                        type: 'success',
                        message: 'Code Type saved successfully!',
                        header: 'Code Type'
                    }]);
                    $state.go('core.master.refCodeType.list');
                }, function(err) {
                    $scope.loading = false;
                    showMessages([{
                        type: 'error',
                        message: 'Could not save Code Type!',
                        header: 'Code Type'
                    }]);
                });
            };
        }
    ])
    .controller('CoreMasterRefCodeTypeRemoveController', [
        '$rootScope',
        '$scope',
        '$state',
        'RefCodeType',
        function($rootScope, $scope, $state, RefCodeType) {
            $scope.refCodeType = RefCodeType[0];
            $scope.remove = function() {
                $scope.loading = true;
                $scope.refCodeType.$remove({
                    id: $scope.refCodeType._id
                }, function(refCodeType) {
                    $scope.loading = false;
                    showMessages([{
                        type: 'success',
                        message: 'Code Type removed successfully!',
                        header: 'Code Type'
                    }]);
                    $state.go('core.master.refCodeType.list');
                }, function(err) {
                    $scope.loading = false;
                    showMessages([{
                        type: 'error',
                        message: 'Could not remove Code Type!',
                        header: 'Code Type'
                    }]);
                });
            };
        }
    ]);
