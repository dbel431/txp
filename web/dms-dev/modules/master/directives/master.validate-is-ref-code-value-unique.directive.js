angular.module('master').directive('validateIsRefCodeValueUnique', [
    'CoreMasterRefCodeValueService',
    function(CoreMasterRefCodeValueService) {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function(scope, iElm, iAttrs, ngModel) {
                ngModel.$validators.validateIsRefCodeValueUnique = function(modelValue) {
                    if (modelValue == '' || modelValue == undefined) return true;
                    var refCodeValue = new CoreMasterRefCodeValueService();
                    return CoreMasterRefCodeValueService.get({
                        'codeValueId': iAttrs.refCodeTypeId,
                        'codeValue': modelValue
                    }, function(refCodeValue) {
                        if (refCodeValue.length == 1 && iAttrs.exceptRefCodeValueId) {
                            ngModel.$setValidity('validateIsRefCodeValueUnique', (refCodeType[0]._id == iAttrs.exceptRefCodeValueId));
                        } else {
                            ngModel.$setValidity('validateIsRefCodeValueUnique', (refCodeType.length <= 0));
                        }
                    }, function(err) {
                        ngModel.$setValidity('validateIsRefCodeValueUnique', false);
                    });
                };
            }
        };
    }
]);
