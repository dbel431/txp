angular.module('master').directive('validateIsRefCodeTypeUnique', [
    'CoreMasterRefCodeTypeService',
    function(CoreMasterRefCodeTypeService) {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function(scope, iElm, iAttrs, ngModel) {
                ngModel.$validators.validateIsRefCodeTypeUnique = function(modelValue) {
                    if (modelValue == '' || modelValue == undefined) return true;
                    var refCodeType = new CoreMasterRefCodeTypeService();
                    return CoreMasterRefCodeTypeService.get({
                        'codeType': modelValue
                    }, function(refCodeType) {
                        if (refCodeType.length == 1 && iAttrs.exceptRefCodeTypeId) {
                            ngModel.$setValidity('validateIsRefCodeTypeUnique', (refCodeType[0]._id == iAttrs.exceptRefCodeTypeId));
                        } else {
                            ngModel.$setValidity('validateIsRefCodeTypeUnique', (refCodeType.length <= 0));
                        }
                    }, function(err) {
                        ngModel.$setValidity('validateIsRefCodeTypeUnique', false);
                    });
                };
            }
        };
    }
]);
