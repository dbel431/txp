angular.module('core', [
    'ui.router',
    'ngMessages',
    'ngResource',
    'ngSanitize',
    'ui.bootstrap',
    // 'ngTable',
    'datatables',
    'datatables.columnfilter',
    'chart.js',
    'ui.select',
    'ckeditor'
]).config([
    '$stateProvider',
    '$urlRouterProvider',
    '$locationProvider',
    function($stateProvider, $urlRouterProvider, $locationProvider) {
        $locationProvider.hashPrefix('!');
        $urlRouterProvider.otherwise('/acl/module/list');
        $stateProvider
            .state('core', {
                abstract: true,
                templateUrl: '/views/core.tpl.html'
            });
    }
]);
