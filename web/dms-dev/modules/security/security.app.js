angular.module('security', ['core'])
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $stateProvider.state('core.security', {
                url: '/security',
                templateUrl: '/views/security.tpl.html'
            }).state('core.security.role', {
                url: '/role',
                templateUrl: '/views/security.role.tpl.html',
                controller: 'RoleController'
            }).state('core.security.role.list', {
                url: '/list',
                templateUrl: '/views/security.role.list.html',
                resolve: {
                    Roles: [
                        'RoleService',
                        function(RoleService) {
                            return RoleService.get().$promise;
                        }
                    ]
                },
                controller: 'RoleListController'
            }).state('core.security.role.add', {
                url: '/add',
                templateUrl: '/views/security.role.add.html',
                controller: 'RoleAddController',
                resolve: {
                    ACLActions: [
                        'ACLActionService',
                        function(ACLActionService) {
                            return ACLActionService.get({ active: true }).$promise;
                        }
                    ]
                }
            }).state('core.security.role.edit', {
                url: '/edit/:id',
                templateUrl: '/views/security.role.edit.html',
                resolve: {
                    ACLActions: [
                        'ACLActionService',
                        function(ACLActionService) {
                            return ACLActionService.get({ active: true }).$promise;
                        }
                    ],
                    'Role': [
                        'RoleService',
                        '$stateParams',
                        function(RoleService, $stateParams) {
                            return RoleService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ]
                },
                controller: 'RoleEditController'
            }).state('core.security.role.remove', {
                url: '/remove/:id',
                templateUrl: '/views/security.role.remove.html',
                resolve: {
                    'Role': [
                        'RoleService',
                        '$stateParams',
                        function(RoleService, $stateParams) {
                            return RoleService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ]
                },
                controller: 'RoleRemoveController'
            }).state('core.security.user', {
                url: '/user',
                templateUrl: '/views/security.user.tpl.html',
                controller: 'UserController',
            }).state('core.security.user.list', {
                url: '/list',
                templateUrl: '/views/security.user.list.html',
                resolve: {
                    Users: [
                        'UserService',
                        function(UserService) {
                            return UserService.get().$promise;
                        }
                    ]
                },
                controller: 'UserListController'
            }).state('core.security.user.add', {
                url: '/add',
                templateUrl: '/views/security.user.add.html',
                controller: 'UserAddController'
            }).state('core.security.user.edit', {
                abstract: true,
                url: '/edit/:id',
                templateUrl: '/views/security.user.edit.tpl.html',
                resolve: {
                    'User': [
                        'UserService',
                        '$stateParams',
                        function(UserService, $stateParams) {
                            return UserService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ],
                    'Roles': [
                        'RoleService',
                        function(RoleService) {
                            return RoleService.get({ active: true }).$promise;
                        }
                    ]
                },
                controller: 'UserEditController'
            }).state('core.security.user.edit.account', {
                url: '/account',
                templateUrl: '/views/security.user.edit.account.html'
            }).state('core.security.user.remove', {
                abstract: true,
                url: '/remove/:id',
                templateUrl: '/views/security.user.remove.tpl.html',
                resolve: {
                    'User': [
                        'UserService',
                        '$stateParams',
                        function(UserService, $stateParams) {
                            return UserService.get({
                                id: $stateParams.id
                            }).$promise;
                        }
                    ],
                    'Roles': [
                        'RoleService',
                        function(RoleService) {
                            return RoleService.get({ active: true }).$promise;
                        }
                    ]
                },
                controller: 'UserEditController'
            });
        }
    ]);
