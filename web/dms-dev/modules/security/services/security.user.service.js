angular.module('security').service('UserService', [
    '$resource',
    function($resource) {
        return $resource(API_PATH + '/user', {}, {
            get: {
                method: 'GET',
                isArray: true
            },
            update: {
                method: 'PUT',
            }
        });
    }
]);
