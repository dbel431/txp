angular.module('security').service('RoleService', [
    '$resource',
    function($resource) {
        return $resource(API_PATH + '/role', {}, {
            get: {
                method: 'GET',
                isArray: true
            },
            update: {
                method: 'PUT'
            },
            remove: {
                method: 'PUT',
                url: API_PATH + '/role/delete'
            }
        });
    }
]);
