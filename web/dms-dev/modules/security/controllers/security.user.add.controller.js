angular.module('security')
    .controller('UserAddController', [
        '$scope',
        '$state',
        'UserService',
        function($scope, $state, UserService) {
            $scope.loading = false;
            $scope.user = new UserService();
            $scope.accessDateOptions = {
                minDate: new Date($scope.user.accessDate),
                maxDate: new Date($scope.user.expiryDate)
            };
            $scope.expiryDateOptions = {
                minDate: new Date($scope.user.accessDate),
                maxDate: new Date($scope.user.expiryDate)
            };
            $scope.$watch('user.expiryDate', function(newVal) {
                $scope.accessDateOptions.maxDate = new Date(newVal || '');
                $scope.expiryDateOptions.maxDate = new Date(newVal || '');
            });
            $scope.$watch('user.accessDate', function(newVal) {
                $scope.accessDateOptions.minDate = new Date(newVal || '');
                $scope.expiryDateOptions.minDate = new Date(newVal || '');
            });
            $scope.add = function() {
                $scope.loading = true;

                $scope.user.$save(function(res) {
                    showMessages([{
                        type: 'success',
                        message: "Successful",
                        header: 'User Added successfully!'
                    }]);
                    $scope.loading = false;
                    $scope.userForm.$setUntouched();
                    $state.go('core.security.user.list');
                }, function(err) {
                    showMessages([{
                        type: 'error',
                        message: err.data.message || "Could not add User! Please try again!",
                        header: 'User Add'
                    }]);
                    $scope.userForm.$setUntouched();
                    $scope.loading = false;
                });
            };
        }
    ]);
