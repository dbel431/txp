angular.module('security')
    .controller('UserEditController', [
        '$scope',
        '$state',
        'User',
        'Roles',
        function($scope, $state, User, Roles) {
            // inits
            $scope.user = User[0];
            $scope.roleArray = Roles;

            // configs
            $scope.dobOptions = {
                maxDate: new Date()
            };

            // masters
            $scope.loading = false;
            $scope.saluteArray = [{
                id: 'mr',
                label: 'Mr.'
            }, {
                id: 'ms',
                label: 'Ms.'
            }, {
                id: 'mrs',
                label: 'Mrs.'
            }, {
                id: 'dr',
                label: 'Dr.'
            }];
            $scope.genderArray = [{
                id: 'male',
                label: 'Male'
            }, {
                id: 'female',
                label: 'Female'
            }, {
                id: 'transgender',
                label: 'Transgender'
            }];
            // utils
            $scope.initUser = function() {
                $scope.user.accessDate = $scope.user.accessDate ? new Date($scope.user.accessDate) : '';
                $scope.user.expiryDate = $scope.user.expiryDate ? new Date($scope.user.expiryDate) : '';
                if (!('details' in $scope.user)) $scope.user.details = {};
                if (!('personal' in $scope.user.details)) $scope.user.details.personal = {};
                $scope.user.details.personal.salute = $scope.user.details.personal.salute || $scope.saluteArray[0].id;
                $scope.user.details.personal.gender = $scope.user.details.personal.gender || $scope.genderArray[0].id;
                if (('dob' in $scope.user.details.personal)) $scope.user.details.personal.dob = new Date($scope.user.details.personal.dob);
            };
            $scope.update = function() {
                $scope.loading = true;
                $scope.user.$update(function(user) {
                    $scope.initUser();
                    showMessages([{
                        type: 'success',
                        message: "Successful",
                        header: 'Details updated successfully!'
                    }]);
                    $scope.userForm.$setUntouched();
                    $scope.loading = false;
                }, function(err) {
                    showMessages([{
                        type: 'error',
                        message: err.data.message || "Could not update details! Please try again!",
                        header: 'Error'
                    }]);
                    $scope.loading = false;
                });
            };

            // runs
            $scope.initUser();
        }
    ])
    .controller('UserEditAccountController', [
        '$scope',
        function($scope) {
            $scope.accessDateOptions = {
                minDate: new Date($scope.user.accessDate),
                maxDate: new Date($scope.user.expiryDate)
            };
            $scope.expiryDateOptions = {
                minDate: new Date($scope.user.accessDate),
                maxDate: new Date($scope.user.expiryDate)
            };
            $scope.$watch('user.expiryDate', function(newVal) {
                $scope.accessDateOptions.maxDate = new Date(newVal || '');
                $scope.expiryDateOptions.maxDate = new Date(newVal || '');
            });
            $scope.$watch('user.accessDate', function(newVal) {
                $scope.accessDateOptions.minDate = new Date(newVal || '');
                $scope.expiryDateOptions.minDate = new Date(newVal || '');
            });

        }
    ])
    .controller('UserEditPersonalController', [
        '$scope',
        function($scope) {}
    ]);
