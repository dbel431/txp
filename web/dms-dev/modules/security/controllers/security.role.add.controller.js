angular.module('security')
    .controller('RoleAddController', [
        '$state',
        '$scope',
        'DTOptionsBuilder',
        'RoleService',
        'ACLActions',
        function($state, $scope, DTOptionsBuilder, RoleService, ACLActions) {
            $scope.loading = false;
            $scope.role = new RoleService();
            $scope.role.actions = [];
            $scope.aclActions = [];
            for (var i = 0; i < ACLActions.length; i++) {
                if (ACLActions[i].module && ACLActions[i].module.active && ACLActions[i].page && ACLActions[i].page.active) {
                    ACLActions[i].selected = false;
                    $scope.aclActions.push(ACLActions[i]);
                }
            }

            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withPaginationType('full_numbers')
                .withColumnFilter({
                    aoColumns: [{
                        type: 'text',
                        bRegex: true,
                        bSmart: true
                    }, {
                        type: 'text',
                        bRegex: true,
                        bSmart: true
                    }, {
                        type: 'text',
                        bRegex: true,
                        bSmart: true
                    }]
                });

            $scope.checkChange = function(aclAction) {
                if (aclAction.selected) {
                    if ($scope.role.actions.indexOf(aclAction._id) < 0)
                        $scope.role.actions.push(aclAction._id);
                } else {
                    if ($scope.role.actions.indexOf(aclAction._id) >= 0)
                        $scope.role.actions.splice($scope.role.actions.indexOf(aclAction._id), 1);
                }
            }
            $scope.add = function() {
                $scope.loading = true;
                $scope.role.$save(function(role) {
                    $scope.loading = false;
                    $scope.roleForm.$setUntouched();
                    showMessages([{
                        type: 'success',
                        message: 'Role named: <b>' + role.name + '</b> added successfully!',
                        header: 'Add Role'
                    }]);
                    $state.go('core.security.role.list');
                }, function(err) {
                    $scope.loading = false;
                    $scope.roleForm.$setUntouched();
                    showMessages([{
                        type: 'error',
                        message: err.data.message,
                        header: 'Add Role'
                    }]);
                })
            }
        }
    ]);
