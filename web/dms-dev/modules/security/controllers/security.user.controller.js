angular.module('security')
    .controller('UserController', [
        '$state',
        '$scope',
        function($state, $scope) {
            $scope.userNavSelected = function(state) {
                return ($state.includes(state)) ? "active" : "";
            };
        }
    ]);
