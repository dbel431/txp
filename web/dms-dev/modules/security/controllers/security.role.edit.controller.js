angular.module('security')
    .controller('RoleEditController', [
        '$state',
        '$stateParams',
        '$scope',
        'DTOptionsBuilder',
        'Role',
        'ACLActions',
        function($state, $stateParams, $scope, DTOptionsBuilder, Role, ACLActions) {
            $scope.loading = false;
            $scope.role = Role[0];
            $scope.aclActions = [];
            for (var i = 0; i < ACLActions.length; i++) {
                if (ACLActions[i].module && ACLActions[i].module.active && ACLActions[i].page && ACLActions[i].page.active) {
                    ACLActions[i].selected = ($scope.role.actions.indexOf(ACLActions[i]._id) >= 0);
                    $scope.aclActions.push(ACLActions[i]);
                }
            }

            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withPaginationType('full_numbers')
                .withColumnFilter({
                    aoColumns: [{
                        type: 'text',
                        bRegex: true,
                        bSmart: true
                    }, {
                        type: 'text',
                        bRegex: true,
                        bSmart: true
                    }, {
                        type: 'text',
                        bRegex: true,
                        bSmart: true
                    }]
                });

            $scope.checkChange = function(aclAction) {
                if (aclAction.selected) {
                    if ($scope.role.actions.indexOf(aclAction._id) < 0)
                        $scope.role.actions.push(aclAction._id);
                } else {
                    if ($scope.role.actions.indexOf(aclAction._id) >= 0)
                        $scope.role.actions.splice($scope.role.actions.indexOf(aclAction._id), 1);
                }
            }

            $scope.edit = function() {
                $scope.loading = true;
                $scope.role.$update({
                    id: $scope.role._id
                }, function(role) {
                    $scope.loading = false;
                    showMessages([{
                        type: 'success',
                        message: 'Details updated successfully!',
                        header: 'Edit Role'
                    }]);

                    $state.go('core.security.role.list');
                }, function(err) {
                    $scope.loading = false;
                    showMessages([{
                        type: 'error',
                        message: err.data.message,
                        header: 'Edit Role'
                    }]);
                })
            }
        }
    ]);
