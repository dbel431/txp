angular.module('security')
    .controller('RoleListController', [
        '$scope',
        'DTOptionsBuilder',
        'Roles',
        function($scope, DTOptionsBuilder, Roles) {
            $scope.roles = Roles;
            $scope.dtOptions = DTOptionsBuilder.newOptions()
                .withPaginationType('full_numbers')
                .withColumnFilter({
                    aoColumns: [{
                        type: 'text',
                        bRegex: true,
                        bSmart: true
                    }, {
                        type: 'select',
                        bRegex: true,
                        values: [
                            { value: '^Active', label: 'Active' }, 
                            { value: '^Inactive', label: 'Inactive' }
                        ]
                    }]
                });
        }
    ]);
