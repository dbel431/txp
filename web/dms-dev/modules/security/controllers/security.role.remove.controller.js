angular.module('security')
    .controller('RoleRemoveController', [
        '$state',
        '$scope',
        'Role',
        function($state, $scope, Role) {
            $scope.loading = false;
            $scope.role = Role[0];
            $scope.remove = function() {
                $scope.loading = true;
                $scope.role.$remove(function(role) {
                    $scope.loading = false;
                    showMessages([{
                        type: 'success',
                        message: 'Role deleted successfully!',
                        header: 'Delete Role'
                    }]);
                    $state.go('core.security.role.list');
                }, function(err) {
                    $scope.loading = false;
                    showMessages([{
                        type: 'error',
                        message: err.data.message,
                        header: 'Remove Role'
                    }]);
                });
            };
        }
    ]);
