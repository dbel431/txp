angular.module('core').directive('validateAs', [function() {
    return {
        require: 'ngModel',
        restrict: 'A',
        link: function(scope, iElm, iAttrs, ngModel) {
            scope.validateAsErrors = scope.validateAsErrors || {};
            scope.validateAsErrors[iAttrs.validateThis] = {};
            scope.validationMap = {
                alphabets: /^[a-zA-Z]*$/,
                digits: /^\d+$/,
                alphanumeric: /^[a-zA-Z0-9]*$/,
                email: /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
            };
            ngModel.$validators.validateAs = function(modelValue) {
                var validations = iAttrs.validateAs.split('|');
                var valid = false;

                for (var i in validations) {
                    var validation = validations[i];
                    if (!(scope.validationMap[validation].test(modelValue))) {
                        scope.validateAsErrors[iAttrs.validateThis][validation] = true;
                    }
                    valid = valid || (scope.validationMap[validation].test(modelValue));
                }

                return valid;
            };

            scope.$watch("validateAs", function() {
                ngModel.$validate();
            });
        }
    };
}]);
