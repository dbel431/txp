angular.module('core').directive('passwordStrength', [function() {
    return {
        scope: {
            password: '=passwordStrength'
        },
        restrict: 'AE',
        template: '<uib-progressbar animate="false" value="strength" type="{{ strengths[strength].type }}"><b>{{ strengths[strength].value }}</b></uib-progressbar>',
        link: function(scope, iElm, iAttrs) {
            scope.strengths = {
                25: {
                    value: 'Weakest',
                    type: 'danger'
                },
                50: {
                    value: 'Weak',
                    type: 'warning'
                },
                75: {
                    value: 'Good',
                    type: 'primary'
                },
                100: {
                    value: 'Strong',
                    type: 'success'
                }
            };

            function calculateStrength(password) {
                scope.valid = [];
                if ((/[a-z]/.test(password))) scope.valid.push('lowercase');
                if ((/[A-Z]/.test(password))) scope.valid.push('uppercase');
                if ((/[0-9]/.test(password))) scope.valid.push('digit');
                if ((/[-!\@\#$%^&*()_+|~=`{}\[\]:";'<>?,.\/]/.test(password))) scope.valid.push('special');
                scope.strength = parseInt((scope.valid.length / 4) * 100);
            }

            scope.$watch('password', calculateStrength);

        }
    };
}]);
