angular.module('core').directive('validatePassword', [function() {
    return {
        require: 'ngModel',
        restrict: 'A',
        link: function(scope, iElm, iAttrs, ngModel) {
            ngModel.$validators.validatePassword = function(modelValue) {
                scope.passwordValidationErrors = []

                if (!(/[a-z]/.test(modelValue))) scope.passwordValidationErrors.push('lowercase');
                if (!(/[A-Z]/.test(modelValue))) scope.passwordValidationErrors.push('uppercase');
                if (!(/[0-9]/.test(modelValue))) scope.passwordValidationErrors.push('digit');
                if (!(/[-!\@\#$%^&*()_+|~=`{}\[\]:";'<>?,.\/]/.test(modelValue))) scope.passwordValidationErrors.push('special');

                return (scope.passwordValidationErrors.length <= 0);
            };
        }
    };
}]);
