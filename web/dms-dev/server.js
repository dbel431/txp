var config = require('../../config.js');
var express = require('express');
var app = express();

app.use('/ckeditor', express.static(__dirname + "/../bower_components/ckeditor"));

app.use('/scripts', express.static(__dirname + "/public/scripts"));
app.use('/styles', express.static(__dirname + "/public/styles"));
app.use('/fonts', express.static(__dirname + "/public/fonts"));
app.use('/images', express.static(__dirname + "/public/images"));
app.use('/views', express.static(__dirname + "/public/views"));

app.use('/config', express.static(__dirname + "/../../config"));

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/index.html');
});

app.listen(process.env.WEB_DEV_PORT, function() {
    console.log('Listening port : ' + process.env.WEB_DEV_PORT);
});
